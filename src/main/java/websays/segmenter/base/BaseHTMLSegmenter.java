/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors: Jorge Valderrama
 * Date: Jul 7, 2014
 */
package websays.segmenter.base;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.htmlcleaner.TagNode;

import websays.types.clipping.SimpleItem;
import websays.types.clipping.SimpleItem.LanguageTag;

/**
 * Basic HTML segmenter: derive your own from here
 * 
 * NOTE: The Asynchornous Crawler can be broken (infinite loop at production) by bad programming of a parser (extends BaseHTMLSegmenter),
 * typically at the momment of generating thread URLS (e.g. comments). In order to validate this it is crucial that any new parser be tested within a
 * local (not production) test on asynchronoush crawler and verify that it terminates and does not entere in an infinite loop.
 *
 */
public class BaseHTMLSegmenter extends HTMLDocumentSegmenter {
  
  private static final Logger logger = Logger.getLogger(BaseHTMLSegmenter.class);
  
  public Boolean timeisReal = false;
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    TagNode rootNode = getRootNode(content);
    SimpleItem mainItem = addSimpleItem(createMainItem(rootNode, url.toString()));
    for (SimpleItem comment : addComments(rootNode, mainItem)) {
      addSimpleItem(comment);
    }
    return getItems();
  }
  
  public List<SimpleItem> addComments(TagNode rootNode, SimpleItem mainItem) {
    List<SimpleItem> comments = new ArrayList<SimpleItem>();
    TagNode[] commentsNode = findComments(rootNode);
    if (commentsNode != null) {
      for (TagNode commentNode : commentsNode) {
        comments.add(createCommentItem(commentNode, mainItem.postUrl, mainItem.threadId, null));
      }
    }
    return comments;
  }
  
  public TagNode[] findComments(TagNode rootNode) {
    return null;
  }
  
  public SimpleItem createMainItem(TagNode rootNode, String postUrl) {
    SimpleItem main = createSimpleItem(postUrl);
    main.title = findMainTitle(rootNode);
    main.body = findMainDescripcion(rootNode);
    main.author = findMainAuthor(rootNode);
    main.authorLocation = findMainAuthorLocation(rootNode);
    main.date = findMainDate(rootNode);
    main.imageLink = findMainImageLink(rootNode);
    Float native_score = findMainScore(rootNode);
    Integer native_max = getNativeMax();
    if (native_score != null && native_max != null) {
      main.setScores(native_score, native_max);
    }
    main.reviews = findMainReviews(rootNode);
    main.likes = findMainLikes(rootNode);
    main.votes = findMainVotes(rootNode);
    main.comments = findMainComments(rootNode);
    main.favorites = findMainFavorites(rootNode);
    main.APIAuthorID = findMainAPIAuthorID(rootNode);
    main.APIObjectID = findMainAPIObjectID(rootNode);
    main.APIToAuthorID = findMainAPIToAuthorID(rootNode);
    main.latitude = findMainLatitude(rootNode);
    main.longitude = findMainLongitude(rootNode);
    main.threadId = findMainThreadId(rootNode);
    main.threadURLs = findMainThreadURLs(rootNode);
    main.timeIsReal = findMainTimeIsReal(rootNode);
    main.language = findMainLanguage(rootNode);
    main.source_extra_metadata = findMainSourceExtraData(rootNode);
    return main;
  }
  
  public SimpleItem createCommentItem(TagNode commentNode, String postUrl, String threadId, List<String> list) {
    SimpleItem comment = createSimpleItem(postUrl);
    comment.APIObjectID = findCommentAPIObjectID(commentNode);
    comment.title = findCommentTitle(commentNode);
    comment.body = findCommentDescripcion(commentNode);
    comment.author = findCommentAuthor(commentNode);
    comment.authorLocation = findCommentAuthorLocation(commentNode);
    comment.date = findCommentDate(commentNode);
    comment.imageLink = findCommentImageLink(commentNode);
    Float native_score = findCommentScore(commentNode);
    Integer native_max = getNativeMax();
    if (native_score != null && native_max != null) {
      comment.setScores(native_score, native_max);
    }
    comment.likes = findCommentLikes(commentNode);
    comment.votes = findCommentVotes(commentNode);
    comment.comments = findCommentNumComments(commentNode);
    comment.threadId = threadId;
    comment.threadURLs = list;
    comment.isComment = true;
    comment.parentID = findCommentParentID(commentNode);
    comment.timeIsReal = findCommentTimeIsReal(commentNode);
    comment.language = findCommentLanguage(commentNode);
    comment.source_extra_metadata = findCommentSourceExtraData(commentNode);
    return comment;
  }
  
  public String findMainTitle(TagNode rootNode) {
    String title = getMetaContent(rootNode, "og:title");
    if (title == null) {
      title = getMetaContent(rootNode, "DC.title");
    }
    return title;
  }
  
  public String findMainDescripcion(TagNode rootNode) {
    return getMetaContent(rootNode, "og:description");
  }
  
  public Date findMainDate(TagNode rootNode) {
    String publishedTime = null;
    Date date = null;
    try {
      publishedTime = getMetaContent(rootNode, "article:published_time");
      if (publishedTime == null) {
        publishedTime = getMetaContent(rootNode, "DC.date");
      }
      if (publishedTime != null) {
        date = DatatypeConverter.parseDateTime(publishedTime).getTime();
        timeisReal = true;
      }
    } catch (Exception e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
      return null;
    }
    return date;
  }
  
  public String findMainAuthor(TagNode rootNode) {
    return getMetaContent(rootNode, "author");
  }
  
  public Integer getNativeMax() {
    return null;
  }
  
  public String findMainAuthorLocation(TagNode rootNode) {
    return null;
  }
  
  public Float findMainScore(TagNode rootNode) {
    return null;
  }
  
  public Integer findMainLikes(TagNode rootNode) {
    return null;
  }
  
  public Integer findMainReviews(TagNode rootNode) {
    return null;
  }
  
  public Integer findMainVotes(TagNode rootNode) {
    return null;
  }
  
  public Integer findMainComments(TagNode rootNode) {
    return null;
  }
  
  public String findMainAPIObjectID(TagNode rootNode) {
    return getMetaContent(rootNode, "fb:app_id");
  }
  
  public String findMainAPIAuthorID(TagNode rootNode) {
    return null;
  }
  
  public String findMainAPIToAuthorID(TagNode rootNode) {
    return null;
  }
  
  public String findMainImageLink(TagNode rootNode) {
    return getMetaContent(rootNode, "og:image");
  }
  
  public Double findMainLatitude(TagNode rootNode) {
    return null;
  }
  
  public Double findMainLongitude(TagNode rootNode) {
    return null;
  }
  
  public String findMainThreadId(TagNode rootNode) {
    return null;
  }
  
  public Integer findMainFavorites(TagNode rootNode) {
    return null;
  }
  
  public List<String> findMainThreadURLs(TagNode rootNode) {
    return null;
  }
  
  public String findMainBody(TagNode rootNode) {
    return null;
  }
  
  public Boolean findMainTimeIsReal(TagNode rootNode) {
    return this.timeisReal;
  }
  
  public LanguageTag findMainLanguage(TagNode rootNode) {
    return null;
  }
  
  public List<String> findMainSourceExtraData(TagNode rootNode) {
    return null;
  }
  
  public Integer findCommentNumComments(TagNode commentNode) {
    return null;
  }
  
  public Integer findCommentVotes(TagNode commentNode) {
    return null;
  }
  
  public Integer findCommentLikes(TagNode commentNode) {
    return null;
  }
  
  public Float findCommentScore(TagNode commentNode) {
    return null;
  }
  
  public String findCommentImageLink(TagNode commentNode) {
    return null;
  }
  
  public Date findCommentDate(TagNode commentNode) {
    return null;
  }
  
  public String findCommentAuthorLocation(TagNode commentNode) {
    return null;
  }
  
  public String findCommentAuthor(TagNode commentNode) {
    return null;
  }
  
  public String findCommentDescripcion(TagNode commentNode) {
    return null;
  }
  
  public String findCommentTitle(TagNode commentNode) {
    return null;
  }
  
  public String findCommentAPIObjectID(TagNode commentNode) {
    return null;
  }
  
  public String findCommentParentID(TagNode commentNode) {
    return null;
  }
  
  public LanguageTag findCommentLanguage(TagNode commentNode) {
    return null;
  }
  
  public Boolean findCommentTimeIsReal(TagNode rootNode) {
    return this.timeisReal;
  }
  
  public List<String> findCommentSourceExtraData(TagNode commentNode) {
    return null;
  }
}
