/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors: Jorge Valdderrama
 * Date: Jul 7, 2014
 */
package websays.segmenter.base;

import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.ContentNode;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import websays.segmenter.common.HTMLEntitiesConverter;
import websays.types.clipping.SimpleItem;

public abstract class HTMLDocumentSegmenter implements MinimalDocumentSegmenter {
  
  private static final Logger logger = Logger.getLogger(HTMLDocumentSegmenter.class);
  
  private ArrayList<SimpleItem> items;
  private int position;
  
  public HTMLDocumentSegmenter() {
    this.items = new ArrayList<SimpleItem>();
    this.setPosition(1);
  }
  
  @Override
  public abstract List<SimpleItem> segment(String content, URL url) throws Exception;
  
  public Date missingDate() { // default missing date is current
    return new Date();
  }
  
  protected String segmentInit(String content, URL url) {
    assert (url != null);
    // recreate the list of items
    setItems(new ArrayList<SimpleItem>());
    setPosition(1);
    content = StringEscapeUtils.unescapeHtml4(content);
    return content;
  }
  
  protected TagNode getRootNode(String content) {
    HtmlCleaner cleaner = createCleaner();
    return cleaner.clean(content);
  }
  
  protected SimpleItem addSimpleItem(SimpleItem item) {
    item.position = position++;
    items.add(item);
    return item;
  }
  
  protected List<SimpleItem> getItems() {
    return items;
  }
  
  protected void setItems(ArrayList<SimpleItem> items) {
    this.items = items;
  }
  
  protected SimpleItem createSimpleItem(String postUrl) {
    SimpleItem simpleItem = new SimpleItem();
    simpleItem.postUrl = postUrl;
    return simpleItem;
  }
  
  protected String getMetaContent(TagNode root, String propertyName) {
    assert (root != null) : "Must have a root element";
    assert (propertyName != null) : "The propertyName can't be null";
    TagNode heading = root.findElementByAttValue("property", propertyName, true, false);
    if (heading == null) {
      heading = root.findElementByAttValue("name", propertyName, true, false);
    }
    
    if (heading != null) {
      String content = StringEscapeUtils.unescapeXml(heading.getAttributeByName("content"));
      return removeWhiteSpaces(content);
    } else {
      return null;
    }
  }
  
  protected String findNodeTextById(TagNode root, String id) {
    return getNodeText(root.findElementByAttValue("id", id, true, false));
  }
  
  protected String findNodeTextByItemprop(TagNode root, String id) {
    return getNodeText(root.findElementByAttValue("itemprop", id, true, false));
  }
  
  protected String findNodeTextByClass(TagNode root, String className) {
    return getNodeText(root.findElementByAttValue("class", className, true, false));
  }
  
  protected String getNodeText(TagNode root) {
    if (root == null) {
      return null;
    }
    String content = StringEscapeUtils.unescapeXml(root.getText().toString());
    return removeWhiteSpaces(content);
  }
  
  protected String getMetaText(TagNode node) {
    if (node == null) {
      return null;
    }
    String content = node.getAttributeByName("content");
    return removeWhiteSpaces(content);
  }
  
  /**
   * Given a tagNode extracts its inner text and only the text. It doesn't extract the node's children's text.
   * 
   * @param parNode
   *          the node the text of which is going to be extracted.
   * @return the inner text of the node. The void String if the node has no inner text or the node itself is null.
   */
  protected String getNodeInnerText(TagNode parNode) {
    
    if (parNode == null) {
      return "";
    }
    
    List children = parNode.getChildren();
    
    if (children != null && children.size() > 0) {
      for (Object child : children) {
        if (child instanceof ContentNode) {
          return child.toString();
        }
      }
    } else {
      return "";
    }
    
    /* If the code gets here there is no inner text, so "" is returned */
    return "";
  }
  
  /**
   * Parses a String that can contain the textual form of a float number.
   * 
   * @param average
   *          a String that contains the textual form of a number
   * @return the parsed float. Or null if the String wasn't parsable.
   */
  protected Float parseFloatSafe(String average) {
    try {
      Float realAverage = new Float(average);
      return realAverage;
    } catch (Exception ex) {
      return null;
    }
  }
  
  public String removeWhiteSpaces(String s) {
    return s == null || s.trim().isEmpty() ? null : HTMLEntitiesConverter.unHTMLEntities(s.replace("&nbsp;", "")).trim();
  }
  
  public String removeLongSpaces(String s) {
    return s == null || s.trim().isEmpty() ? null : HTMLEntitiesConverter.unHTMLEntities(s.replace("&nbsp;", "")).replaceAll("( )+", " ")
        .replaceAll(" ", " ").trim();
  }
  
  /**
   * Looks for a number in a string and then parses it. The parsing can be done from the String's start or looking for the first number in it.
   * 
   * @param text
   *          the text to look numbers inside.
   * @param fromStart
   *          if true the parsing starts at the String's start. If false, there is a search for the first number inside the String.
   * @return A number or null if there was no number in the String.
   */
  public static Integer parseNumbers(String text, boolean fromStart) {
    
    Integer result = null;
    if (text != null) {
      int start = 0;
      if (!fromStart) {
        for (int i = 1; i < text.length(); i++) {
          if (Character.isDigit(text.charAt(i))) {
            start = i;
            break;
          }
        }
      }
      
      for (int i = start + 1; i <= text.length(); i++) {
        try {
          result = Integer.parseInt(text.substring(start, i));
        } catch (Exception e) {
          break;
        }
      }
    }
    
    return result;
  }
  
  public static int getNumberStartIndex(String text) {
    int index = 0;
    if (text != null) {
      for (int i = 0; i < text.length(); i++) {
        if (Character.isDigit(text.charAt(i))) {
          index = i;
          break;
        }
      }
    }
    return index;
  }
  
  public HtmlCleaner createCleaner() {
    HtmlCleaner cleaner = new HtmlCleaner();
    CleanerProperties props = cleaner.getProperties();
    props.setTranslateSpecialEntities(true);
    props.setTransResCharsToNCR(true);
    props.setTransSpecialEntitiesToNCR(true);
    props.setOmitComments(true);
    return cleaner;
  }
  
  /**
   * Given a date, it parses the date known how to handle expressions like "a year ago" in English or "hace un año" in Spanish. as of jul 17 2015 it
   * works for Spanish, English and Italian. If the date is not an "ago" date, this method tries to parse it using the sd parameter.
   * 
   * @param parTextualDate
   *          the string that represents the date to parse
   * @param referenceDate
   *          a reference date from which count backwards. Usually this is the current date, but it needn't to.
   * @param sd
   *          the date parser that will parse the textual date, once adjusted.
   * @return the date parTextualDate represents.
   */
  public static Date parseAgoDate(String parTextualDate, Date referenceDate, SimpleDateFormat sd) {
    if (parTextualDate == null) {
      logger.warn("The ago date to parse is null, so returning null date.");
      return null;
    }
    
    if (logger.isTraceEnabled()) {
      logger.trace("Parsing ago date [" + parTextualDate + "] and present time is [" + referenceDate.toString() + "]");
    }
    
    /*
     * Pattern matcher for Spanish, Italian and English. Should new languages with different structure be parsed, new matchers will be added.
     */
    String strEnESIT = "\\s*(\\d+)\\s+(\\S+)";
    Pattern pEnESIT = Pattern.compile(strEnESIT);
    Matcher m = null;
    
    String textualAmount = null;
    String textualTimeSpan = null;
    int timespan = -200;
    int amount = 0;
    
    Date ret = null;
    
    if (parTextualDate.contains("hace")) {
      /* Spanish */
      parTextualDate = parTextualDate.replace(" un ", " 1 ");
      parTextualDate = parTextualDate.replace(" una ", " 1 ");
      parTextualDate = parTextualDate.replace("hace", "");
      m = pEnESIT.matcher(parTextualDate);
      if (!m.find()) {
        logger
            .warn("The ago date to parse [" + parTextualDate + "] doesn't match the pattern [" + strEnESIT + "], so returning null date.");
        return null;
      }
      textualTimeSpan = m.group(2);
      
      if (textualTimeSpan != null) {
        if (textualTimeSpan.trim().equalsIgnoreCase("día") || textualTimeSpan.trim().equalsIgnoreCase("días")
            || textualTimeSpan.trim().equalsIgnoreCase("dia") || textualTimeSpan.trim().equalsIgnoreCase("dias")) {
          timespan = Calendar.DAY_OF_YEAR;
        } else if (textualTimeSpan.trim().equalsIgnoreCase("semana") || textualTimeSpan.trim().equalsIgnoreCase("semanas")) {
          timespan = Calendar.WEEK_OF_YEAR;
        } else if (textualTimeSpan.trim().equalsIgnoreCase("mes") || textualTimeSpan.trim().equalsIgnoreCase("meses")) {
          timespan = Calendar.MONTH;
        } else if (textualTimeSpan.trim().equalsIgnoreCase("año") || textualTimeSpan.trim().equalsIgnoreCase("años")) {
          timespan = Calendar.YEAR;
        } else if (textualTimeSpan.trim().equalsIgnoreCase("hora") || textualTimeSpan.trim().equalsIgnoreCase("horas")) {
          timespan = Calendar.HOUR_OF_DAY;
        } else if (textualTimeSpan.trim().equalsIgnoreCase("minuto") || textualTimeSpan.trim().equalsIgnoreCase("minutos")) {
          timespan = Calendar.MINUTE;
        } else {
          logger.warn("The timespan [" + textualTimeSpan + "] is not recognized in Spanish. Returning null.");
        }
      } else {
        logger.warn("The timeSpan is null. Maybe the treated textual date [" + parTextualDate + "] is not ok. Returning null. ");
        return ret;
      }
      
    } else if (parTextualDate.contains(" ago") && !parTextualDate.contains("agosto")) {
      /* English */
      parTextualDate = parTextualDate.replace(" a ", " 1 ");
      parTextualDate = parTextualDate.replace("ago", "");
      m = pEnESIT.matcher(parTextualDate);
      if (!m.find()) {
        logger
            .warn("The ago date to parse [" + parTextualDate + "] doesn't match the pattern [" + strEnESIT + "], so returning null date.");
        return null;
      }
      m = pEnESIT.matcher(parTextualDate);
      if (!m.find()) {
        logger
            .warn("The ago date to parse [" + parTextualDate + "] doesn't match the pattern [" + strEnESIT + "], so returning null date.");
        return null;
      }
      textualTimeSpan = m.group(2);
      
      if (textualTimeSpan != null) {
        if (textualTimeSpan.trim().equalsIgnoreCase("day") || textualTimeSpan.trim().equalsIgnoreCase("days")) {
          timespan = Calendar.DAY_OF_YEAR;
        } else if (textualTimeSpan.trim().equalsIgnoreCase("week") || textualTimeSpan.trim().equalsIgnoreCase("weeks")) {
          timespan = Calendar.WEEK_OF_YEAR;
        } else if (textualTimeSpan.trim().equalsIgnoreCase("month") || textualTimeSpan.trim().equalsIgnoreCase("months")) {
          timespan = Calendar.MONTH;
        } else if (textualTimeSpan.trim().equalsIgnoreCase("year") || textualTimeSpan.trim().equalsIgnoreCase("years")) {
          timespan = Calendar.YEAR;
        } else if (textualTimeSpan.trim().equalsIgnoreCase("hour") || textualTimeSpan.trim().equalsIgnoreCase("hours")) {
          timespan = Calendar.HOUR_OF_DAY;
        } else if (textualTimeSpan.trim().equalsIgnoreCase("minute") || textualTimeSpan.trim().equalsIgnoreCase("minutes")) {
          timespan = Calendar.MINUTE;
        } else {
          logger.warn("The timespan [" + textualTimeSpan + "] is not recognized in English. Returning null.");
        }
      } else {
        logger.warn("The timeSpan is null. Maybe the treated textual date [" + parTextualDate + "] is not ok. Returning null. ");
        return ret;
      }
      
    } else if (parTextualDate.contains(" fa")) {
      /* Italian */
      parTextualDate = parTextualDate.replace(" un ", " 1 ");
      parTextualDate = parTextualDate.replace(" una ", " 1 ");
      parTextualDate = parTextualDate.replace("fa", "");
      m = pEnESIT.matcher(parTextualDate);
      if (!m.find()) {
        logger
            .warn("The ago date to parse [" + parTextualDate + "] doesn't match the pattern [" + strEnESIT + "], so returning null date.");
        return null;
      }
      m = pEnESIT.matcher(parTextualDate);
      if (!m.find()) {
        logger
            .warn("The ago date to parse [" + parTextualDate + "] doesn't match the pattern [" + strEnESIT + "], so returning null date.");
        return null;
      }
      textualTimeSpan = m.group(2);
      
      if (textualTimeSpan != null) {
        if (textualTimeSpan.trim().equalsIgnoreCase("giorno") || textualTimeSpan.trim().equalsIgnoreCase("giorni")) {
          timespan = Calendar.DAY_OF_YEAR;
        } else if (textualTimeSpan.trim().equalsIgnoreCase("settimana") || textualTimeSpan.trim().equalsIgnoreCase("settimane")) {
          timespan = Calendar.WEEK_OF_YEAR;
        } else if (textualTimeSpan.trim().equalsIgnoreCase("mese") || textualTimeSpan.trim().equalsIgnoreCase("mesi")) {
          timespan = Calendar.MONTH;
        } else if (textualTimeSpan.trim().equalsIgnoreCase("anno") || textualTimeSpan.trim().equalsIgnoreCase("anni")) {
          timespan = Calendar.YEAR;
        } else {
          logger.warn("The timespan [" + textualTimeSpan + "] is not recognized in Italian. Returning null.");
        }
      } else {
        logger.warn("The timeSpan is null. Maybe the treated textual date [" + parTextualDate + "] is not ok. Returning null. ");
        return ret;
      }
      
    } else {
      /* Language not identified. Maybe is not an ago date */
      if (sd != null) {
        logger.warn("Language not identified for ago date [" + parTextualDate
            + "]. Trying to parse the date as if it wasn't an \"ago\" date.");
        
        try {
          ret = sd.parse(parTextualDate);
        } catch (ParseException e) {
          logger.warn("Date [" + parTextualDate + "] is not an \"ago\" date nor parsable using [" + sd.toPattern() + "]. Reason ["
              + e.getMessage() + "]");
        }
      } else {
        logger.warn("Language not identified for ago date [" + parTextualDate
            + "]. it is not an \"ago\" date and no pattern has been supplied to parse it. Returning null.");
        return ret;
        
      }
      if (logger.isTraceEnabled()) {
        logger.trace("Textual date [" + parTextualDate + "] was not an \"ago\" date but was parsable using [" + sd.toPattern()
            + "] Returning [" + ret + "]");
      }
      
      return ret;
    }
    
    /* Common for Spanish, English and Italian. Beware when adding new languages, maybe the algorithm must be different. */
    textualAmount = m.group(1);
    
    if (textualAmount != null) {
      
      try {
        amount = Integer.parseInt(textualAmount);
        Calendar tempCal = Calendar.getInstance();
        tempCal.setTime(referenceDate);
        tempCal.add(timespan, -(amount));
        ret = tempCal.getTime();
      } catch (NumberFormatException e) {
        logger.warn("TextualAmount [" + textualAmount + "] seems not to be a number. Error [" + e.getMessage() + "]. Returning null date.");
        return ret;
      }
    } else {
      logger.warn("The amount of (days/weeks/months/years) is null. Maybe the treated textual date [" + parTextualDate
          + "] is not ok. Returning null. ");
      return ret;
    }
    
    if (logger.isTraceEnabled()) {
      logger.trace("Textual date [" + parTextualDate + "] Reference date [" + referenceDate + "] Returning [" + ret + "]");
    }
    
    return ret;
  }
  
  /**
   * Given a date, it parses the date known how to handle expressions like "a year ago" in English or "hace un año" in Spanish.
   * 
   * @param parTextualDate
   *          the string that represents the date to parse
   * @param referenceDate
   *          a reference date from which count backwards. Usually this is the current date, but it needn't to.
   * @param sd
   *          the date parser that will parse the textual date, once adjusted.
   * @return the date parTextualDate represents.
   */
  public static Date parseAgoDateOLD(String parTextualDate, Date referenceDate, SimpleDateFormat sd) {
    if (parTextualDate == null) {
      return null;
    }
    Date ret = null;
    parTextualDate = parTextualDate.toLowerCase().trim();
    try {
      int years = 0, months = 0, weeks = 0, days = 0;
      Calendar cal = Calendar.getInstance();
      if (referenceDate != null) {
        cal.setTime(referenceDate);
      }
      
      parTextualDate = parTextualDate.replace(" un ", " 1 ");
      parTextualDate = parTextualDate.replace(" a ", " 1 ");
      
      if (parTextualDate.contains("hace") || (parTextualDate.contains(" ago") && !parTextualDate.contains("agosto"))) {
        
        if (parTextualDate.indexOf("hace") != -1) {
          parTextualDate = parTextualDate.substring(parTextualDate.indexOf("hace") + 4).trim();// remove 'hace'
        }
        
        if (parTextualDate.indexOf(" ago") != -1) {
          parTextualDate = parTextualDate.substring(0, parTextualDate.indexOf(" ago")).trim();// remove 'ago'
        }
        
        if (parTextualDate.endsWith("año") || parTextualDate.endsWith("años") || parTextualDate.endsWith("year")
            || parTextualDate.endsWith("years")) {// Years
          parTextualDate = parTextualDate.replace("años", "").replace("año", "").replace("years", "").replace("year", "").trim();
          parTextualDate = parTextualDate.substring(getNumberStartIndex(parTextualDate)).trim();
          
          years = Integer.parseInt(parTextualDate);
          
          cal.set(Calendar.DATE, 1);// /////////
          cal.set(Calendar.MONTH, 1);// ////////
          // ////////// Set the date of
          // month and the month as '1' when the date is
          // some years ago
          // TODO:
          // Remove these lines if doesn't
          // need it
          // //////////////////////////////////
        } else if (parTextualDate.endsWith("meses") || parTextualDate.endsWith("mes") || parTextualDate.endsWith("months")
            || parTextualDate.endsWith("month")) {// Months
          parTextualDate = parTextualDate.replace("meses", "").replace("mes", "").replace("months", "").replace("month", "").trim();
          parTextualDate = parTextualDate.substring(getNumberStartIndex(parTextualDate)).trim();
          
          months = Integer.parseInt(parTextualDate);
          
          cal.set(Calendar.DATE, 1);// //////////// Set the date of
          // month as '1' when the date is
          // some months ago
          // TODO:
          // Remove this line if doesn't
          // need it
          
        } else if (parTextualDate.endsWith("semana") || parTextualDate.endsWith("semanas") || parTextualDate.endsWith("weeks")
            || parTextualDate.endsWith("week")) {// Weeks
          parTextualDate = parTextualDate.replace("semanas", "").replace("semana", "").replace("weeks", "").replace("week", "").trim();
          parTextualDate = parTextualDate.substring(getNumberStartIndex(parTextualDate)).trim();
          
          weeks = Integer.parseInt(parTextualDate);
        } else if (parTextualDate.endsWith("día") || parTextualDate.endsWith("días") || parTextualDate.endsWith("days")
            || parTextualDate.endsWith("day")) {// Days
          parTextualDate = parTextualDate.replace("días", "").replace("día", "").replace("days", "").replace("day", "").trim();
          parTextualDate = parTextualDate.substring(getNumberStartIndex(parTextualDate)).trim();
          
          days = Integer.parseInt(parTextualDate);
        }
        
      } else if (parTextualDate.contains("la última") || parTextualDate.contains("last ")) {
        
        if (parTextualDate.endsWith("año") || parTextualDate.endsWith("años") || parTextualDate.endsWith("year")
            || parTextualDate.endsWith("years")) {// Years
          years = 1;
          
          cal.set(Calendar.DATE, 1);// /////////
          cal.set(Calendar.MONTH, 1);// ////////
          // ////////// Set the date of
          // month and the month as '1' when the date is
          // some years ago
          // TODO:
          // Remove these lines if doesn't
          // need it
          // //////////////////////////////////
        } else if (parTextualDate.endsWith("meses") || parTextualDate.endsWith("mes") || parTextualDate.endsWith("month")
            || parTextualDate.endsWith("months")) {// Months
        
          months = 1;
          cal.set(Calendar.DATE, 1);// //////////// Set the date of
          // month as '1' when the date is
          // some months ago
          // TODO:
          // Remove this line if doesn't
          // need it
          
        } else if (parTextualDate.endsWith("semana") || parTextualDate.endsWith("semanas") || parTextualDate.endsWith("week")
            || parTextualDate.endsWith("weeks")) {// Weeks
        
          weeks = 1;
        } else if (parTextualDate.endsWith("día") || parTextualDate.endsWith("días") || parTextualDate.endsWith("day")
            || parTextualDate.endsWith("days")) {// Days
        
          days = 1;
        }
        
      } else {
        if (sd != null) {
          ret = sd.parse(parTextualDate);
        }
        return ret;
      }
      
      cal.add(Calendar.YEAR, -years);
      cal.add(Calendar.MONTH, -months);
      cal.add(Calendar.DATE, -weeks * 7);
      cal.add(Calendar.DATE, -days);
      cal.set(Calendar.HOUR_OF_DAY, 12);
      cal.set(Calendar.MINUTE, 0);
      cal.set(Calendar.SECOND, 0);
      cal.set(Calendar.MILLISECOND, 0);
      ret = cal.getTime();
      
    } catch (Exception e) {
      logger.error("Error parsing Date Item: '" + parTextualDate + "'", e);
    }
    return ret;
  }
  
  /**
   * Tests that the comment has author and date
   * 
   * @param item
   *          the comment to be tested
   * @return true if the comment has author and date. False otherwise.
   */
  protected boolean passCommentTest(SimpleItem item) {
    if (item.date == null) {
      return false;
    }
    if (item.author == null) {
      return false;
    }
    return true;
  }
  
  /**
   * Tests that a item have a postUrl and a title.
   * 
   * @param item
   *          the item to be tested
   * @return false if the item doesn't have definite postUrl and definite date. True otherwise
   */
  protected boolean passMainTest(SimpleItem item) {
    if (item.postUrl == null) {
      return false;
    }
    if (item.title == null) {
      return false;
    }
    return true;
  }
  
  public int getPosition() {
    return position;
  }
  
  public void setPosition(int position) {
    this.position = position;
  }
  
}