/**
* Websays Opinion Analytics Engine
*
* (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
*
* Primary Author: Marco Martinez/Hugo Zaragoza
* Contributors:
* Date: Jul 7, 2014
*/
package websays.segmenter.base;

import java.util.HashMap;

public class Layouts {
  
  private static HashMap<String,HashMap<String,String>> layouts = null;
  
  public static HashMap<String,HashMap<String,String>> getLayouts() {
    if (layouts == null) {
      createLayouts();
    }
    
    return layouts;
  }
  
  /**
   * Create differentes layouts. If a node has subitems, separate via ## to get the name node. Get the attribute value via @@
   */
  private static void createLayouts() {
    layouts = new HashMap<String,HashMap<String,String>>();
    
    // feedburner layout one
    HashMap<String,String> layout = new HashMap<String,String>();
    layout.put("body", "description");
    layout.put("title", "title");
    layout.put("author", "dc:creator");
    layout.put("url", "feedburner:origLink");
    layout.put("date", "pubDate");
    layout.put("image", "media:content@@url");
    layout.put("comments_count", "slash:comments");
    layouts.put("feedburner1", layout);
    // feedburner layout two
    layout = new HashMap<String,String>();
    layout.put("body", "content");
    layout.put("title", "title");
    layout.put("author", "author");
    layout.put("date", "published");
    layout.put("url", "feedburner:origLink");
    layout.put("comments_count", "thr:total");
    layout.put("image", "picture##url");
    layouts.put("feedburner2", layout);
    
  }
  
}
