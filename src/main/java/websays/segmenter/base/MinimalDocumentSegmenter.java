package websays.segmenter.base;

import java.net.URL;
import java.util.List;

import websays.types.clipping.SimpleItem;

public interface MinimalDocumentSegmenter {
  
  public List<SimpleItem> segment(String content, URL url) throws Exception;
  
}
