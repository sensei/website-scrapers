package websays.segmenter.base;

import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import websays.types.clipping.SimpleItem;

/**
 * Used to create fake SimpleItems which contains URLs to fetcha and process (in threadUrl). Typically to process search result pages.
 * 
 * Be VERY CAERFUL because some URLs you add will (in their turn) add further URLs, and you may end up creating loops or exponential fan-outs and
 * BREAKING the ASYNCH FETCHER.
 * 
 * How it works: it finds all URLs matching <code>collectors</code> pattern and creates a fake SimpleItem with the URLs as threadURLs.
 * 
 * If <code>urlPrefix</code>!=null all found urls are prefixed with it (useful when urls are relative)
 * 
 * @author hugoz
 *
 */
public abstract class URLCollector extends HTMLDocumentSegmenter {
  
  private static final Logger logger = Logger.getLogger(URLCollector.class);
  
  protected List<Pattern> collectors = null;
  protected String urlPrefix = null;
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    if (collectors == null || collectors.size() == 0) {
      logger.error("No collector patterns defined for " + this.getClass().getName());
      return new ArrayList<>(0);
    }
    if (logger.isTraceEnabled()) {
      logger.trace("<CONTENT>\n" + content + "\n</CONTENT>");
    }
    
    LinkedHashSet<String> urls = new LinkedHashSet<String>(); // will remove duplicates
    for (Pattern p : collectors) {
      Matcher m = p.matcher(content);
      while (m.find()) {
        urls.add((urlPrefix != null ? urlPrefix : "") + m.group(1));
      }
    }
    
    if (logger.isDebugEnabled()) {
      logger.debug("URLS FOUND: [\n  " + StringUtils.join(urls, "\n  ") + "\n]");
    }
    
    SimpleItem si = new SimpleItem();
    si.postUrl = url.toExternalForm();
    si.threadURLs = new ArrayList<>(urls);
    si.fakeItem = true;
    ArrayList<SimpleItem> ret = new ArrayList<>();
    ret.add(si);
    return ret;
  }
  
  public URLCollector() {
    // override and put in collectors and patterns. Each pattern should return matches with one group containing the URL of interest.
    // Exmple:
    collectors = new ArrayList<Pattern>();
    Pattern p = Pattern.compile("\"(http://interesting.site.com/[^\"]*)\"");
    collectors.add(p);
  }
  
}
