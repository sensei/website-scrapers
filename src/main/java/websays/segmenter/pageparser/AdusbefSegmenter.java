/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author:Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class AdusbefSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(AdusbefSegmenter.class);
  
  private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    HtmlCleaner cleaner = createCleaner();
    
    TagNode root = null;
    String site = null;
    if (url != null) {
      site = url.toString();
    }
    root = cleaner.clean(content);
    int position = 1;
    
    if (!site.contains("-int")) {
      
      SimpleItem item = new SimpleItem();
      item.fakeItem = true;
      ArrayList<String> siteURL = new ArrayList<String>(Arrays.asList(site));
      item.threadURLs = siteURL;
      item.position = position;
      items.add(item);
      logger.debug("Fake Clipping with URL " + site);
    } else {
      // /////////// Main Details Item ////////////
      
      TagNode reviewRoot[] = root.getElementsByName("tbody", true);
      if (reviewRoot != null && reviewRoot.length > 0) {
        TagNode threads[] = reviewRoot[0].getAllElements(false);
        
        SimpleItem item = null;
        int counterNodes = 0;
        for (TagNode t : threads) {
          
          if ((counterNodes % 2) == 0) {
            
            item = new SimpleItem();
            getAuthor(item, t);
            getDate(item, t);
            
          } else {
            
            getBody(item, t);
            getThreadId(item, t);
            item.position = position;
            item.postUrl = url.toString();
            position++;
            items.add(item);
            
          }
          counterNodes++;
        }
      }
    }
    
    return items;
  }
  
  private SimpleItem getAuthor(SimpleItem item, TagNode tagNode) {
    // Author
    TagNode nodeAuthor = tagNode.findElementByName("p", true);
    if (nodeAuthor != null) {
      item.author = removeWhiteSpaces(nodeAuthor.findElementByName("b", true).findElementByName("a", true).getText().toString());
    }
    return item;
  }
  
  private SimpleItem getDate(SimpleItem item, TagNode tagNode) throws ParseException {
    // Date
    Date parse = null;
    try {
      TagNode nodeDate[] = tagNode.getElementsByName("tbody", true);
      if (nodeDate != null) {
        String date = removeWhiteSpaces(nodeDate[0].getAllElements(false)[1].findElementByName("p", true).findElementByName("font", true)
            .getText().toString());
        parse = sdf.parse(date.substring(14, 25));
      }
    } catch (Exception e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
      
    }
    item.date = parse;
    return item;
  }
  
  private SimpleItem getBody(SimpleItem item, TagNode tagNode) throws ParseException {
    // Body
    TagNode nodeBody[] = tagNode.getElementsByName("td", true);
    if (nodeBody != null) {
      item.body = removeWhiteSpaces(nodeBody[0].getText().toString());
    }
    return item;
  }
  
  private SimpleItem getThreadId(SimpleItem item, TagNode tagNode) throws ParseException {
    // ThreadId
    TagNode nodeBody[] = tagNode.getElementsByName("td", true);
    if (nodeBody != null) {
      item.threadId = removeWhiteSpaces(nodeBody[0].getAttributeByName("id"));
    }
    return item;
  }
  
}
