/**
 * Websays Opinion Analytics Engine 
 * 
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 * 
 * Primary Author: Jorge Valderrama
 * Contributors: 
 * Date: Jul 14, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

/**
 * Class segmenter for afiliado.com
 **/
public class AfiliadoSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(AfiliadoSegmenter.class);
  
  private static final SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy, HH:MM", new Locale("es", "ES"));
  
  // \d\d [A-Za-z]{3} \d\d\d\d, \d\d:\d\d
  private final static Pattern encodingPatternDate = Pattern.compile("(\\d\\d [A-Za-z]{3} \\d\\d\\d\\d, \\d\\d:\\d\\d)");
  
  private String site = null;
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    HtmlCleaner cleaner = createCleaner();
    
    TagNode root = null;
    if (url != null) {
      site = url.toString();
    }
    root = cleaner.clean(content);
    
    int position = 1;
    // /////////// Main Details Item ////////////
    TagNode mainTop = root.findElementByAttValue("id", "pagecontent", true, false);
    if (mainTop != null) {
      TagNode mainMiddle = root.findElementByAttValue("class", "tablebg", true, false);
      if (mainMiddle != null) {
        TagNode reviewRoot = mainMiddle.findElementByName("tbody", true);
        if (reviewRoot != null) {
          TagNode rev[] = reviewRoot.getElementsByName("tr", false);
          for (TagNode tagNode : rev) {
            if (tagNode.hasAttribute("class")) {
              TagNode node = tagNode.findElementByAttValue("class", "postbody", true, false);
              if (node != null) {
                SimpleItem item = new SimpleItem();
                try {
                  item.body = tagNode.findElementByAttValue("class", "postbody", true, false).getText().toString();
                  item.title = tagNode.findElementByAttValue("class", "postsubject", true, false).getText().toString();
                  item.author = tagNode.findElementByAttValue("class", "postauthor", true, false).getText().toString();
                  String sdate = tagNode.findElementByAttValue("class", "postdetails", true, false).getText().toString();
                  if (sdate != null && !sdate.isEmpty()) {
                    Matcher matcher = encodingPatternDate.matcher(sdate);
                    if (matcher.find()) {
                      sdate = matcher.group(1);
                      item.date = sdf.parse(sdate);
                      item.timeIsReal = true;
                    }
                  }
                  item.postUrl = site;
                  item.position = position++;
                  item.isComment = true;
                  if (position == 2) {
                    item.threadURLs = findMainThreadURLs(mainTop);
                  }
                  items.add(item);
                } catch (Exception e) {
                  logger.error(e.getMessage());
                }
              }
            }
          }
        }
      }
    }
    return items;
  }
  
  @Override
  public List<String> findMainThreadURLs(TagNode node) {
    ArrayList<String> urls = null;
    TagNode[] tabs = node.getAllElements(false);
    TagNode nextPageNode = tabs[tabs.length - 1];
    if (nextPageNode != null) {
      TagNode[] tagsToEvaluate = nextPageNode.getElementsByAttValue("class", "gensmall", true, false);
      if (tagsToEvaluate != null && tagsToEvaluate.length > 0) {
        TagNode rev[] = tagsToEvaluate[1].findElementByName("b", false).getElementsByName("a", false);
        for (TagNode tagNode : rev) {
          if (tagNode.getText().toString().equals("Siguiente")) {
            urls = new ArrayList<String>();
            if (site.contains("start=")) {
              int nextPage = Integer.valueOf(site.substring(site.lastIndexOf("=") + 1, site.length()));
              nextPage = nextPage + 10;
              urls.add(site.substring(0, site.lastIndexOf("=") + 1) + nextPage);
            } else {
              urls.add(site + "&start=10");
            }
          }
        }
      }
    }
    return urls;
  }
}
