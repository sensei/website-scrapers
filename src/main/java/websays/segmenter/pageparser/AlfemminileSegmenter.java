/**
 * Websays Opinion Analytics Engine 
 * 
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 * 
 * Primary Author: joedval
 * Contributors: 
 * Date: Jul 21, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

/**
 * Class segmenter for http://forum.alfemminile.com/
 **/
public class AlfemminileSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(AlfemminileSegmenter.class);
  
  private String site = null;
  
  private ArrayList<SimpleItem> items = null;
  
  private Integer position = null;
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    
    HtmlCleaner cleaner = createCleaner();
    
    TagNode root = null;
    if (url != null) {
      site = url.toString();
    }
    root = cleaner.clean(content);
    
    // /////////// Main Details Item ////////////
    items = new ArrayList<SimpleItem>();
    position = 1;
    if (checkMainDiscussion(root)) {
      TagNode[] nodesTop = root.getAllElements(true);
      for (TagNode tagNode : nodesTop) {
        if (tagNode.hasAttribute("class")) {
          if (tagNode.getAttributeByName("class").equals("aff_mess")) {
            extractItem(root, tagNode.getParent());
          }
        }
      }
    }
    // Replies items
    TagNode mainReplies = root.findElementByAttValue("class", "af_colCentre", true, false);
    if (mainReplies != null) {
      TagNode[] repliesNodes = mainReplies.getAllElements(false);
      for (TagNode replyNode : repliesNodes) {
        if (replyNode.hasAttribute("class")) {
          if (replyNode.getAttributeByName("class").contains("aff_blocRepNiv")) {
            if (replyNode.getAttributes().size() == 2) {
              extractItem(root, replyNode);
            }
          }
        }
      }
    }
    return items;
  }
  
  private boolean checkMainDiscussion(TagNode root) {
    boolean check = true;
    TagNode mainInfo1[] = root.getElementsByAttValue("class", "aff_navHigh", true, false);
    if (mainInfo1.length >= 2) {
      if (mainInfo1[1].findElementByAttValue("align", "right", true, false) != null) {
        TagNode nodeNextPage = mainInfo1[1].findElementByAttValue("align", "right", true, false).findElementByName("a", true);
        if (nodeNextPage != null) {
          if (!nodeNextPage.getText().toString().equals("2")) {
            check = false;
          }
        }
      }
    }
    return check;
  }
  
  private void extractItem(TagNode root, TagNode commentNode) throws ParseException {
    SimpleItem item = new SimpleItem();
    item.APIObjectID = commentNode.getAttributeByName("id");
    if (commentNode.findElementByAttValue("class", "aff_contenu", true, false) != null) {
      item.body = commentNode.findElementByAttValue("class", "aff_contenu", true, false).getText().toString();
    }
    item.title = getMetaContent(root, "description");
    if (commentNode.findElementByAttValue("class", "aff_author", true, false) != null) {
      item.author = commentNode.findElementByAttValue("class", "aff_author", true, false).getText().toString();
    }
    if (commentNode.findElementByAttValue("class", "aff_date", true, false) != null) {
      try {
        int fromComment = commentNode.findElementByAttValue("class", "aff_date", true, false).getText().toString().indexOf("(");
        int toComment = commentNode.findElementByAttValue("class", "aff_date", true, false).getText().toString().indexOf(",");
        String milliseconds = commentNode.findElementByAttValue("class", "aff_date", true, false).getText().toString()
            .substring(fromComment + 1, toComment);
        Date date = new Date(Long.valueOf(milliseconds + "000"));
        item.date = date;
        item.timeIsReal = true;
      } catch (Exception e) {
        logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
        item.date = null;
      }
    }
    item.postUrl = site;
    item.position = position++;
    item.isComment = true;
    if (position == 2) {
      item.threadURLs = findMainThreadURLs(root);
    }
    items.add(item);
  }
  
  @Override
  public List<String> findMainThreadURLs(TagNode root) {
    List<String> urls = null;
    TagNode mainInfo1[] = root.getElementsByAttValue("class", "aff_navHigh", true, false);
    if (mainInfo1.length >= 2) {
      if (mainInfo1[1].findElementByAttValue("align", "right", true, false) != null) {
        TagNode nodeNextPage = mainInfo1[1].findElementByAttValue("align", "right", true, false).findElementByName("a", true);
        if (nodeNextPage != null) {
          if (nodeNextPage.getText().toString() != null && !nodeNextPage.getText().toString().equals("1")) {
            urls = Arrays.asList(nodeNextPage.getAttributeByName("href"));
          }
        }
      }
    }
    return urls;
  }
}
