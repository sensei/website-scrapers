/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class AndroidWorldItSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(AndroidWorldItSegmenter.class);
  
  private static final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy, HH:mm");
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    HtmlCleaner cleaner = createCleaner();
    
    TagNode root = null;
    String site = null;
    if (url != null) {
      site = url.toString();
    }
    root = cleaner.clean(content);
    
    // /////////// Main Details Item ////////////
    
    TagNode[] mainInfo = root.getElementsByAttValue("class", "threadtitle", true, false);
    TagNode mainInfoNode = mainInfo[0];
    String mainTitle = removeLongSpaces(mainInfoNode.getText().toString());
    
    TagNode reviewRoot[] = root.getElementsByAttValue("id", "postlist", true, false);
    // TagNode reviewRoot[] = root.getElementsByAttValue("class", "tborder", true, false);
    // TagNode reviewRoot[] = root.getElementsByAttValue("align", "left", true, false);
    // TagNode reviewRoot[] = root.getElementsByAttValue("class", "page", true, false);
    
    if (reviewRoot != null && reviewRoot.length > 0) {
      TagNode rev[] = reviewRoot[0].getElementsByAttValue("class", "postbitlegacy postbitim postcontainer old", true, false);
      // TagNode rev[] = reviewRoot[0].getChildTags();
      // TagNode rev[] = reviewRoot[0].getAllElements(false);
      
      int position = 1;
      
      for (TagNode tParent : rev) {
        try {
          String postNumber = null;
          SimpleItem item = new SimpleItem();
          
          if (tParent != null) {
            // empty comments
            if (tParent.getAttributeByName("id").equals("post_")) {
              continue;
            }
            
            // main item title
            if (position == 1) {
              item.title = mainTitle;
            } else {
              item.isComment = true;
            }
            
            // item date
            
            try {
              StringBuffer dateText = tParent.getElementsByAttValue("class", "date", true, false)[0].getText();
              String dateString = dateText.toString().replace("&nbsp;", " ").replaceAll(": ", ":").replace('\u00A0', ' ');
              dateString = removeLongSpaces(dateString);
              Date date = sdf.parse(dateString);
              item.date = date;
              item.timeIsReal = true;
            } catch (Exception e) {
              logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
              item.date = null;
            }
            
            // item url
            try {
              TagNode[] elementsByAttValue = tParent.getElementsByAttValue("class", "nodecontrols", true, false);
              String itemURL = null;
              
              TagNode tagNode = elementsByAttValue[0];
              TagNode[] elementsByName = tagNode.getElementsByName("a", false);
              item.itemUrl = elementsByName[0].getAttributeByName("href");
              
            } catch (Exception e) {}
            
            // item author
            try {
              TagNode[] elementsByAttValue = tParent.getElementsByAttValue("class", "username offline ", true, false);
              item.author = elementsByAttValue[0].getText().toString();
            } catch (Exception e) {}
            
            // item body
            try {
              TagNode[] elementsByAttValue = tParent.getElementsByAttValue("class", "postcontent restore ", true, false);
              item.body = removeLongSpaces(elementsByAttValue[0].getText().toString());
            } catch (Exception e) {}
            
            // item title
            try {
              TagNode[] elementsByAttValue = tParent.getElementsByAttValue("class", "title icon", true, false);
              item.title = removeLongSpaces(elementsByAttValue[0].getText().toString());
            } catch (Exception e) {}
            
            item.position = position;
            item.postUrl = url.toString();
            position++;
            
            items.add(item);
          }
          
        } catch (Exception e) {
          logger.error(e.getMessage(), e.getCause());
        }
      }
    }
    
    return items;
  }
}
