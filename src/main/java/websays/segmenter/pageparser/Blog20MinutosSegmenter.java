/**
 * Websays Opinion Analytics Engine 
 * 
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 * 
 * Primary Author: Jorge Valderrama
 * Contributors: 
 * Date: Jul 31, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

/**
 *
 **/
public class Blog20MinutosSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(Blog20MinutosSegmenter.class);
  
  private static final SimpleDateFormat sdfArticle = new SimpleDateFormat("dd MMM yyyy", new Locale("es", "ES"));
  
  private static final SimpleDateFormat sdfComment = new SimpleDateFormat("dd MMM yyyy, hh:mm", new Locale("es", "ES"));
  
  private String site = null;
  
  private final String author = "http://blogs.20minutos.es";
  
  @Override
  public List<SimpleItem> segment(String content, URL url) {
    content = segmentInit(content, url);
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    try {
      HtmlCleaner cleaner = createCleaner();
      
      TagNode root = null;
      if (url != null) {
        site = url.toString();
      }
      root = cleaner.clean(content);
      
      int position = 1;
      // /////////// Main Details Item ////////////
      SimpleItem mainItem = new SimpleItem();
      mainItem.body = findMainBody(root).replaceAll("&nbsp;", "");
      mainItem.date = findMainDate(root);
      mainItem.author = author;
      mainItem.title = findMainTitle(root);
      mainItem.imageLink = findMainImageLink(root);
      mainItem.postUrl = site;
      mainItem.position = position++;
      mainItem.isComment = false;
      items.add(mainItem);
      // Comments of the main detail (Article)
      if (position == 2) {
        TagNode commentlist = root.findElementByAttValue("class", "commentlist", true, false);
        if (commentlist != null) {
          TagNode comments[] = commentlist.getAllElements(false);
          if (comments != null && comments.length > 0) {
            for (TagNode commentNode : comments) {
              SimpleItem commentItem = new SimpleItem();
              commentItem.APIObjectID = commentNode.getAttributeByName("id");
              TagNode nodeAuthor = commentNode.findElementByAttValue("class", "comment-author vcard", true, false);
              if (nodeAuthor != null) {
                if (nodeAuthor.findElementByName("cite", true) != null) {
                  commentItem.author = nodeAuthor.findElementByName("cite", true).getText().toString();
                }
              }
              TagNode nodeBody = commentNode.findElementByAttValue("id", commentItem.APIObjectID.substring(3), true, false);
              TagNode[] paragraphs = nodeBody.getElementsByName("p", false);
              if (paragraphs != null && paragraphs.length > 0) {
                String body = new String();
                for (int i = 0; i < paragraphs.length - 1; i++) {
                  TagNode tagNode = paragraphs[i];
                  body = body.concat(tagNode.getText().toString());
                }
                commentItem.body = body.replace("&#8230", "");
                try {
                  String sdate = paragraphs[paragraphs.length - 1].getText().toString();
                  Date date = sdfComment.parse(sdate.replace(" | ", ", "));
                  commentItem.date = date;
                  commentItem.timeIsReal = true;
                } catch (Exception e) {
                  logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
                  commentItem.date = null;
                }
                
              }
              commentItem.postUrl = site;
              commentItem.position = position++;
              commentItem.isComment = true;
              items.add(commentItem);
            }
          }
        }
      }
    } catch (Exception e) {
      logger.error(e.getMessage());
    }
    return items;
  }
  
  @Override
  public String findMainBody(TagNode rootNode) {
    String body = new String();
    TagNode paragraphsNode = rootNode.findElementByAttValue("class", "entry", true, false);
    if (paragraphsNode != null) {
      TagNode[] paragraphs = paragraphsNode.getElementsByName("p", false);
      if (paragraphs != null && paragraphs.length > 0) {
        for (TagNode tagNode : paragraphs) {
          body = body.concat(tagNode.getText().toString());
        }
      }
    }
    return body;
  }
  
  @Override
  public Date findMainDate(TagNode rootNode) {
    Date date = null;
    try {
      TagNode topNode = rootNode.findElementByAttValue("id", "content", true, false);
      if (topNode != null) {
        TagNode nodes[] = topNode.getAllElements(false);
        if (nodes != null && nodes.length > 2) {
          TagNode topdateNode = nodes[1];
          if (topdateNode != null) {
            TagNode dateNode = topdateNode.findElementByName("small", false);
            if (dateNode != null) {
              String sdate = dateNode.getText().toString();
              try {
                date = sdfArticle.parse(sdate);
              } catch (ParseException e) {
                logger.error(e.getMessage());
              }
            }
          }
        }
      }
    } catch (Exception e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
      return null;
    }
    return date;
  }
  
}
