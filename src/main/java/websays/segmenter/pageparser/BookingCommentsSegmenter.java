/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class BookingCommentsSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(BookingCommentsSegmenter.class);
  
  public static SimpleDateFormat sd = new SimpleDateFormat("dd MMM yyyy", new Locale("en", "US"));
  public static SimpleDateFormat sd2 = new SimpleDateFormat("MMM dd, yyyy", new Locale("en", "US"));
  /* Initialized once the language is detected from the page data */
  public static SimpleDateFormat internationalSD = null;
  
  private final Locale pageLocale = null;
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    
    String baseURL = null; // This is protocol + "://" + host + ":" + port
    
    if (url != null) {
      baseURL = url.getProtocol() + "://" + url.getHost();
      if (url.getPort() != -1) {
        baseURL = baseURL + ":" + url.getPort();
      }
    }
    
    String context = url.getPath();
    String language = null;
    Locale pageLocale = null;
    
    if (context != null) {
      int dotIndex = context.indexOf('.');
      int lastDotIndex = context.lastIndexOf('.');
      
      if (dotIndex == -1) {
        language = "en";
        pageLocale = new Locale(language, "US");
        logger.warn("Page context is [" + context + "] and it shouldn t. Review how this method is called. Using default language ["
            + language + "] and default locale [" + pageLocale.getDisplayName(new Locale("en", "US")) + "].");
        
      } else if (dotIndex != -1 && lastDotIndex == -1) {
        language = context.substring(dotIndex + 1);
        pageLocale = new Locale(language);
      } else if (dotIndex != -1 && lastDotIndex != -1) {
        language = context.substring(dotIndex + 1, lastDotIndex);
        pageLocale = new Locale(language);
      }
      
      if (logger.isTraceEnabled()) {
        logger.trace("Locale [" + pageLocale.getDisplayName(new Locale("en", "US")) + "] got from language [" + language
            + "] got from context [" + context + "]");
      }
    } else {
      language = "en";
      pageLocale = new Locale(language, "US");
      logger.warn("Page context is [" + context + "] and it shouldn t. Review how this method is called. Using default language ["
          + language + "] and default locale [" + pageLocale.getDisplayName(new Locale("en", "US")) + "].");
      
    }
    
    internationalSD = new SimpleDateFormat("dd MMM yyyy", pageLocale);
    
    // recreate the list of items
    setItems(new ArrayList<SimpleItem>());
    super.setPosition(0);
    TagNode rootNode = getRootNode(content);
    
    SimpleItem mainItem = addSimpleItem(createMainItem(rootNode, url.toString()));
    mainItem.isComment = false;
    mainItem.position = super.getPosition();
    mainItem.fakeItem = true;
    mainItem.itemUrl = getMainURLFromComments(url);
    // To be phased out. BookingSegmenter already generates all the comment's pages. No need to generate following pages here.
    // mainItem.threadURLs = getNextComments(rootNode, baseURL, mainItem.itemUrl);
    String facilityName = getFacilityName(url);
    if (logger.isTraceEnabled()) {
      logger.trace("This is the list of urls that point to next comment's blocks [" + mainItem.threadURLs + "]");
    }
    
    for (SimpleItem comment : addComments(rootNode, mainItem)) {
      comment.postUrl = mainItem.itemUrl;
      /* Adding facility's name to the beggining of the title's comment to make it match easier */
      comment.title = facilityName + ": " + comment.title;
      addSimpleItem(comment);
      if (logger.isTraceEnabled()) {
        logger.trace("Comment's position [" + comment.position + "]");
      }
    }
    return getItems();
  }
  
  /**
   * @param url
   * @return
   */
  private String getFacilityName(URL url) {
    String res = null;
    String rawQS = url.getQuery();
    if (rawQS == null) {
      logger.warn("There is not enough info in the url (queryString is null) [" + url.toExternalForm() + "] to get the facility's name.");
      return null;
    }
    
    String[] splittedQS = rawQS.split(";");
    if (splittedQS == null || splittedQS.length < 1) {
      logger.warn("There is not enough info in the url [" + url.toExternalForm() + "] to get the facility's name.");
      return null;
    }
    for (String current : splittedQS) {
      String[] splittedCurr = current.split("=");
      if (splittedCurr != null && splittedCurr.length > 0) {
        switch (splittedCurr[0]) {
          case BookingSegmenter.PAGENAME_PARAM_LABEL:
            res = splittedCurr[1];
            break;
          default:
        }
      }
    }
    
    if (logger.isTraceEnabled()) {
      logger.trace("Facility's name is [" + res + "]");
    }
    return res;
    
  }
  
  /**
   * Given the comments URL, rebuilds the main hotel page URL. It works because, when BookingCommentsSegmenter and BookingSegmenter build the next
   * comment's page url, they add a parameter to the query string (threadURL) the value of which is the main hotel page for the comment's page being
   * processed.
   * 
   * @param url
   *          the url being parsed
   * @return the main hotel's page URL.
   */
  private String getMainURLFromComments(URL url) {
    
    String res = null;
    String rawQS = url.getQuery();
    if (rawQS == null) {
      logger.warn("There is not enough info in the url (queryString is null) [" + url.toExternalForm() + "] to rebuild main page URL.");
      return null;
    }
    
    String[] splittedQS = rawQS.split(";");
    if (splittedQS == null || splittedQS.length < 1) {
      logger.warn("There is not enough info in the url [" + url.toExternalForm() + "] to rebuild main page URL.");
      return null;
    }
    for (String current : splittedQS) {
      String[] splittedCurr = current.split("=");
      if (splittedCurr != null && splittedCurr.length > 0) {
        switch (splittedCurr[0]) {
          case BookingSegmenter.THREADURL_PARAM_LABEL:
            res = splittedCurr[1];
            break;
          default:
        }
      }
    }
    
    if (logger.isTraceEnabled()) {
      logger.trace("Comments URL [" + url.toExternalForm() + "] belongs to facility [" + res + "]");
    }
    return res;
  }
  
  /**
   * Gets the url for the next comment block
   * 
   * @param rootNode
   *          the root node of the page
   * @param baseURL
   *          a textual representation of the url of the page
   * @param mainUrl
   *          the main booking url this comment's page belongs to.
   * @return a String that contains the URL to the next block of comments.
   */
  private List<String> getNextComments(TagNode rootNode, String baseURL, String mainUrl) {
    ArrayList<String> listNextComments = null;
    List nextLinkNodes = rootNode.getElementListByAttValue("id", "review_next_page_link", true, false);
    String urlContext = null;
    
    if (nextLinkNodes != null && nextLinkNodes.size() > 0) {
      urlContext = ((TagNode) nextLinkNodes.get(0)).getAttributeByName("href");
      if (urlContext != null && !"".equalsIgnoreCase(urlContext)) {
        listNextComments = new ArrayList<String>();
        if (urlContext.indexOf("http") == -1 && urlContext.indexOf("https") == -1) {
          urlContext = baseURL + urlContext;
        }
        urlContext = urlContext + ";" + BookingSegmenter.THREADURL_PARAM_LABEL + "=" + mainUrl;
      }
      listNextComments = new ArrayList<String>();
      listNextComments.add(urlContext);
    }
    
    if (logger.isTraceEnabled()) {
      logger.trace("Link to next comment's block page [" + urlContext + "](null means there are no more comment's pages)");
    }
    
    return listNextComments;
  }
  
  @Override
  public TagNode[] findComments(TagNode rootNode) {
    TagNode[] res = null;
    res = rootNode.getElementsByAttValue("class", " review_tr", true, false);
    if (res == null || res.length == 0) {
      logger.trace("The comments list is empty. Trying an alternative method 1.");
      res = rootNode.getElementsByAttValue("class", "review_item r_improve clearfix", true, false);
    }
    
    if (res == null || res.length == 0) {
      logger.trace("The comments list is empty. Trying an alternative method 2.");
      res = rootNode.getElementsByAttValue("class", "review_item clearfix ", true, false);
    }
    
    logger.trace("The comments list has [" + (res == null ? 0 : res.length) + "] comments inside.");
    return res;
  }
  
  @Override
  public Float findCommentScore(TagNode commentNode) {
    String average = findNodeTextByClass(commentNode, "the_score");
    if (average == null) {
      logger.trace("Comment's score is null. Trying method2");
      average = findNodeTextByClass(commentNode, "review_item_review_score");
      if (average == null) {
        logger.trace("Comment's score is null. Trying method3");
        average = findNodeTextByClass(commentNode, "review_item_review_score jq_tooltip high_score_tooltip");
      }
    }
    if (logger.isTraceEnabled()) {
      logger.trace("Raw score String [" + average + "]");
    }
    
    return parseFloatSafe(average);
  }
  
  @Override
  public Date findCommentDate(TagNode commentNode) {
    String date = findNodeTextByClass(commentNode, "date");
    if (date == null) {
      logger.trace("The comment's date is empty. Trying an alternative method.");
      date = findNodeTextByClass(commentNode, "review_item_date");
      if (date != null) {
        // method 2
        try {
          return sd2.parse(date);
        } catch (ParseException e) {
          logger.warn("Error parsing date with method2 for [" + this.getClass().getName() + "] due to [" + e.getMessage()
              + "] will try method3. Using parser [" + sd2.toPattern() + "]");
          
        }
        
        // method 3
        try {
          /* Nasty hack to get rid of "de"s in Spanish and Catalan dates and "." in German dates */
          return internationalSD.parse(date.replace("\\.", "").replaceAll(" de ", "  ").replaceAll("\\s+", " ").trim());
        } catch (ParseException e) {
          logger.warn("Error parsing date with method3 for [" + this.getClass().getName() + "] due to [" + e.getMessage()
              + "] date will be null. Using parser [" + internationalSD.toPattern() + "]");
          return null;
          
        }
      } else {
        logger.warn("No nodes of class [review_item_date] or [date] found. Date will be null.");
      }
      
    }
    // method1
    try {
      return sd.parse(date);
    } catch (ParseException e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
      return null;
    }
  }
  
  @Override
  public String findCommentAuthorLocation(TagNode commentNode) {
    String authorLocation = findNodeTextByClass(commentNode, "location");
    if (authorLocation == null) {
      logger.trace("Comment's authorLocation not found. Using method2");
      authorLocation = findNodeTextByClass(commentNode, "reviewer_country");
    }
    if (logger.isTraceEnabled()) {
      logger.trace("Author location is [" + authorLocation + "]");
    }
    
    return authorLocation;
  }
  
  @Override
  public String findCommentAuthor(TagNode commentNode) {
    String author = findNodeTextByClass(commentNode, "name");
    if (author == null) {
      logger.trace("Comment's author not found. Using method2");
      TagNode[] tempAuthor = commentNode.getElementsByName("h4", true);
      if (tempAuthor != null && tempAuthor.length > 0) {
        author = (tempAuthor[0].getText().toString()).trim();
      }
    }
    if (logger.isTraceEnabled()) {
      logger.trace("Comment's author found [" + author + "]");
    }
    return author;
    
  }
  
  @Override
  public String findCommentTitle(TagNode commentNode) {
    String title = findNodeTextByClass(commentNode, "review_item_header_content");
    if (title != null) {
      int firstQuote = title.indexOf("\"");
      int lastQuote = title.lastIndexOf("\"");
      if (lastQuote != -1) {
        title = title.substring(0, lastQuote);
      }
      if (firstQuote != -1) {
        title = title.substring(firstQuote + 1);
      }
      
    }
    
    if (logger.isTraceEnabled()) {
      logger.trace("Comment's title [" + title + "]");
    }
    
    return title;
  }
  
  @Override
  public String findCommentDescripcion(TagNode commentNode) {
    String good = findNodeTextByClass(commentNode, "comments_good");
    String bad = findNodeTextByClass(commentNode, "comments_bad");
    if (good == null) {
      logger.trace("Comment's description (good) is empty. Trying method2.");
      good = findNodeTextByClass(commentNode, "review_pos");
      if (good == null) {
        logger.trace("Comment's description (good) is empty. Trying method3.");
        good = findNodeTextByClass(commentNode, "review_pos tags235");
      }
    }
    
    if (bad == null) {
      logger.trace("Comment's description (bad) is empty. Trying method2.");
      bad = findNodeTextByClass(commentNode, "review_neg");
      if (bad == null) {
        logger.trace("Comment's description (bad) is empty. Trying method3.");
        bad = findNodeTextByClass(commentNode, "review_neg tags235");
      }
    }
    
    String comments = null;
    if (good == null && bad == null) {
      
      logger.trace("Maybe the comment's inside the review are empty. But the review itself has information.");
      comments = findNodeTextByClass(commentNode, "review_none");
      
      if (comments == null) {
        logger.trace("No method has found comment's description. Returning null. ");
      }
      return null;
    }
    
    comments = "";
    if (good != null) {
      comments += good;
    }
    if (bad != null) {
      if (!comments.isEmpty()) {
        comments += ". ";
      }
      comments += bad;
    }
    return comments;
  }
  
}
