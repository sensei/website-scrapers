/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class BookingSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(BookingSegmenter.class);
  
  public static SimpleDateFormat sd = new SimpleDateFormat("dd MMM yyyy", new Locale("en", "US"));
  public static SimpleDateFormat sd2 = new SimpleDateFormat("MMM dd, yyyy", new Locale("en", "US"));
  /* Initialized once the language is detected from the page data */
  public static SimpleDateFormat internationalSD = null;
  
  private final Pattern patLat = Pattern.compile("booking\\.env\\.b_map_center_latitude = ([0-9.]+);");
  private String _latitude = null;
  private final Pattern patLon = Pattern.compile("booking\\.env\\.b_map_center_longitude = ([0-9.]+);");
  private String _longitude = null;
  
  private Locale _pageLocale = null;
  
  /* Comment's page URL related constants */
  public static final String BASE_COMMENTS_URL = "http://www.booking.com/reviewlist.en-gb.html?";
  public static final String CC1_PARAM_LABEL = "cc1";
  public static final String PAGENAME_PARAM_LABEL = "pagename";
  public static final String LANGUAGE_PARAM_LABEL = "r_lang";
  public static final String OFFSET_PARAM_LABEL = "offset";
  public static final String ROWS_PARAM_LABEL = "rows";
  /**
   * Constant that has the value of the parameter of the querystring that will let other calls to know which is the main page for a comment's page.
   */
  public static final String THREADURL_PARAM_LABEL = "threadURL";
  
  /**
   * Native maximun score for this data source
   */
  public static final Integer MAX_SCORE = 10;
  
  /*
   * TODO: this should be passed to the segmenter as config values. This means to extend the Segmenter interface and make aware the infraestructure
   * about it
   */
  /* Config values used to generate the set of comment's URLs */
  
  /**
   * Set of languages, the comments of which must be retrieved. Should you need to include a new language, add it here.
   */
  private static final String[] languages = {"en", "fr", "es", "it", "de", "nl", "ca"};
  /**
   * Number of pages to be retrieved for each language.
   */
  private static final int numPages = 3;
  
  /**
   * Number of results to be retrieved in each page. Beware, booking.com accepts a undetermined range of results per page. Empirically working amounts
   * are (10,25,50,100). It could change in the future without warning from booking.com
   */
  private static final int resultsPerPage = 100;
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    
    String baseURL = null; // This is protocol + "://" + host + ":" + port
    if (url != null) {
      baseURL = url.getProtocol() + "://" + url.getHost();
      if (url.getPort() != -1) {
        baseURL = baseURL + ":" + url.getPort();
      }
    }
    
    // Getting latitude and longitude. They are get here because are inside a script node that "getRootNode" strips when cleanning.
    Matcher matcher = null;
    matcher = patLat.matcher(content);
    if (matcher.find()) {
      _latitude = matcher.group(1);
    }
    matcher = patLon.matcher(content);
    if (matcher.find()) {
      _longitude = matcher.group(1);
    }
    
    // recreate the list of items
    setItems(new ArrayList<SimpleItem>());
    super.setPosition(1);
    TagNode rootNode = getRootNode(content);
    
    SimpleItem mainItem = addSimpleItem(createMainItem(rootNode, url.toString()));
    mainItem.itemUrl = canonicalizeURL(url);
    // mainItem.threadURLs = getNextComments(rootNode, baseURL);
    mainItem.threadURLs = buildNextComments(mainItem.itemUrl);
    if (logger.isTraceEnabled()) {
      logger.trace("This is the list of urls that point to next comment's blocks [" + mainItem.threadURLs + "]");
    }
    
    List languageNodes = rootNode.getElementListByAttValue("http-equiv", "content-language", true, false);
    String pageLanguage = null;
    if (languageNodes != null && languageNodes.size() > 0) {
      pageLanguage = ((TagNode) languageNodes.get(0)).getAttributeByName("content");
      if (logger.isTraceEnabled()) {
        logger.trace("Got language [" + pageLanguage + "] from page's content.");
      }
    }
    
    if (pageLanguage == null) {
      pageLanguage = "en-us";
      logger.warn("Setting the locale to a default value using language [" + pageLanguage
          + "]. Be warned, this could lead to problems further in the page parsing.");
    }
    
    _pageLocale = Locale.forLanguageTag(pageLanguage);
    if (_pageLocale != null) {
      internationalSD = new SimpleDateFormat("dd MMM yyyy", _pageLocale);
      /* It gets the language for the found locale in English, and then converts it in a SimpleItem.LanguageTag */
      logger.trace("page: [" + url.toExternalForm() + "]");
      logger.trace("pageLanguage: [" + pageLanguage + "]");
      logger.trace("displayName: [" + _pageLocale.getDisplayName(new Locale("en", "US")).toLowerCase() + "]");
      logger.trace("displayLanguage: [" + _pageLocale.getDisplayLanguage(new Locale("en", "US")).toLowerCase() + "]");
      logger.trace("displayCountry: [" + _pageLocale.getDisplayCountry(new Locale("en", "US")).toLowerCase() + "]");
      logger.trace("language: [" + _pageLocale.getLanguage().toLowerCase() + "]");
      logger.trace("country: [" + _pageLocale.getCountry().toLowerCase() + "]");
      mainItem.language = SimpleItem.LanguageTag.valueOf(_pageLocale.getDisplayLanguage(new Locale("en", "US")).toLowerCase());
      
    } else {
      logger.warn("The locale for the page couldn't be generated. This could lead to problems further in the page parsing.");
    }
    
    if (logger.isTraceEnabled()) {
      logger.trace("Page language detection outcome: languageNodes [" + languageNodes + "] pageLanguage [" + pageLanguage
          + "] pageLocale [" + _pageLocale + "]");
    }
    
    for (SimpleItem comment : addComments(rootNode, mainItem)) {
      comment.postUrl = mainItem.itemUrl;
      addSimpleItem(comment);
    }
    return getItems();
  }
  
  @Override
  public Integer getNativeMax() {
    return MAX_SCORE;
  }
  
  /**
   * Builds the url where it is supposed the comments to be.
   * 
   * @param baseURL
   *          the original's page url that lets to build the comments URL.
   * @return a String that is a comments page URL
   */
  private List<String> buildNextComments(String baseURL) {
    ArrayList<String> listNextComments = null;
    String commentsURL = null;
    if (baseURL != null) {
      String[] components = baseURL.split("/");
      if (components != null && components.length > 4) {
        String cc1 = components[4];
        String rawPagename = components[5];
        components = rawPagename.split("\\.");
        if (components != null && components.length > 0) {
          String pagename = components[0];
          listNextComments = new ArrayList<String>();
          /* Using "config" values, a set of comments urls are generated for every "interesting" language */
          for (String currLang : languages) {
            for (int i = 0; i < numPages; i++) {
              int offset = i * resultsPerPage;
              commentsURL = BASE_COMMENTS_URL + CC1_PARAM_LABEL + "=" + cc1 + ";" + PAGENAME_PARAM_LABEL + "=" + pagename + ";"
                  + LANGUAGE_PARAM_LABEL + "=" + currLang + ";" + OFFSET_PARAM_LABEL + "=" + offset + ";" + ROWS_PARAM_LABEL + "="
                  + resultsPerPage + ";" + THREADURL_PARAM_LABEL + "=" + baseURL;
              
              listNextComments.add(commentsURL);
              if (logger.isTraceEnabled()) {
                logger.trace("Added comments URL [" + commentsURL + "] for language [" + currLang + "]");
              }
            }
            
          }
          
          if (logger.isTraceEnabled()) {
            logger.trace("Built the commentsURL [" + commentsURL + "] from the baseURL [" + baseURL + "]");
          }
        } else {
          logger.warn("The base url structure (pageName) [" + baseURL + "] is not suitable to build the comments URL. Returning null.");
        }
      } else {
        logger.warn("The base url structure [" + baseURL + "] is not suitable to build the comments URL. Returning null.");
      }
      
    } else {
      logger.warn("The base url is null [" + baseURL + "] can't build the comments URL. Returning null.");
    }
    
    return listNextComments;
    
  }
  
  /**
   * @param url
   * @return
   */
  private String canonicalizeURL(URL url) {
    if (url == null) {
      logger.warn("The received URL is null. Returning null.");
      return null;
    }
    
    String res = url.toExternalForm();
    String canonPath = url.getPath();
    String canonPort = "";
    
    if (canonPath != null) {
      canonPath = canonPath.substring(0, canonPath.indexOf('.')) + ".html";
    }
    if (url.getPort() != -1) {
      canonPort = ":" + (new Integer(url.getPort())).toString();
    }
    
    res = url.getProtocol() + "://" + url.getHost() + canonPort + canonPath;
    if (logger.isTraceEnabled()) {
      logger.trace("URL [" + url.toExternalForm() + "] canonicalized to [" + res + "]");
    }
    
    return res;
  }
  
  /**
   * Gets the url for the next comment block
   * 
   * @param rootNode
   *          the root node of the page
   * @param baseURL
   *          a textual representation of the url of the page
   * @return a String that contains the URL to the next block of comments.
   */
  private List<String> getNextComments(TagNode rootNode, String baseURL) {
    ArrayList<String> listNextComments = null;
    List nextLinkNodes = rootNode.getElementListByAttValue("id", "review_next_page_link", true, false);
    
    if (nextLinkNodes != null && nextLinkNodes.size() > 0) {
      String urlContext = ((TagNode) nextLinkNodes.get(0)).getAttributeByName("href");
      if (logger.isTraceEnabled()) {
        logger.trace("Link to next comment's block page [" + urlContext + "]");
      }
      if (urlContext != null && !"".equalsIgnoreCase(urlContext)) {
        listNextComments = new ArrayList<String>();
        if (urlContext.indexOf("http") != -1) {
          listNextComments.add(urlContext);
        } else {
          listNextComments.add(baseURL + urlContext);
        }
      }
    }
    
    return listNextComments;
  }
  
  @Override
  public Double findMainLatitude(TagNode rootNode) {
    String latitude = getMetaContent(rootNode, "booking_com:location:latitude");
    if (latitude == null) {
      latitude = _latitude;
    }
    return latitude != null ? Double.parseDouble(latitude) : null;
  }
  
  @Override
  public Double findMainLongitude(TagNode rootNode) {
    String longitude = getMetaContent(rootNode, "booking_com:location:longitude");
    if (longitude == null) {
      longitude = _longitude;
    }
    return longitude != null ? Double.parseDouble(longitude) : null;
  }
  
  @Override
  public String findMainDescripcion(TagNode rootNode) {
    return findNodeTextById(rootNode, "summary");
  }
  
  @Override
  public Float findMainScore(TagNode rootNode) {
    String average = findNodeTextByClass(rootNode, "average");
    
    if (average == null) {
      logger.trace("Comment's score is null. Trying method2");
      List tempNodes = rootNode.getElementListByAttValue("class", "hotel_large_photp_score featured_review_score", true, false);
      if (tempNodes != null && tempNodes.size() > 0) {
        average = findNodeTextByClass(((TagNode) tempNodes.get(0)), "average");
        if (average != null) {
          logger.trace("Method2 got raw score [" + average + "]. It will be rounded.");
        } else {
          logger
              .trace("Method2 failed either. No node with class?\"average\" inside node with class=\"hotel_large_photp_score featured_review_score\". Returning null score");
        }
      } else {
        logger.trace("Method2 failed either. No node with class=\"hotel_large_photp_score featured_review_score\". Returning null score");
      }
    }
    
    return parseFloatSafe(average);
  }
  
  @Override
  public Integer findMainReviews(TagNode rootNode) {
    String reviews = findNodeTextByClass(rootNode, "count");
    return parseFloatSafe(reviews).intValue();
  }
  
  @Override
  public TagNode[] findComments(TagNode rootNode) {
    TagNode[] res = null;
    res = rootNode.getElementsByAttValue("class", " review_tr", true, false);
    if (res == null || res.length == 0) {
      logger.trace("The comments list is empty. Trying an alternative method.");
      res = rootNode.getElementsByAttValue("class", "review_item r_improve clearfix", true, false);
      if (res == null || res.length == 0) {
        logger
            .trace("The comments list is empty using also the alternative method. This is not a problem as booking has a specialized comments segmenter.");
      }
    }
    return res;
  }
  
  @Override
  public Float findCommentScore(TagNode commentNode) {
    String average = findNodeTextByClass(commentNode, "the_score");
    if (average == null) {
      logger.trace("Comment's score is null. Trying method2");
      average = findNodeTextByClass(commentNode, "review_item_review_score jq_tooltip");
      if (average != null) {
        logger.trace("Got raw score using method2 [" + average + "]");
      } else {
        logger.trace("Method2 failed getting the score either. Returning null score.");
      }
    }
    
    return parseFloatSafe(average);
  }
  
  @Override
  public Date findCommentDate(TagNode commentNode) {
    String date = findNodeTextByClass(commentNode, "date");
    if (date == null) {
      logger.trace("The comment's date is empty. Trying an alternative method.");
      date = findNodeTextByClass(commentNode, "review_item_date");
      if (date != null) {
        // method 2
        try {
          return sd2.parse(date);
        } catch (ParseException e) {
          logger.warn("Error parsing date with method2 using parser [" + sd2.toPattern() + "] inside class [" + this.getClass().getName()
              + "] due to [" + e.getMessage() + "] will try method3.");
          
        }
        
        // method 3
        try {
          /* Nasty hack to get rid of "de"s in Spanish and Catalan dates and "." in German dates */
          return internationalSD.parse(date.replace("\\.", "").replaceAll(" de ", "  ").replaceAll("\\s+", " ").trim());
        } catch (ParseException e) {
          logger.warn("Error parsing date with method3 using parser [" + internationalSD.toPattern() + "] with locale["
              + _pageLocale.getDisplayName(new Locale("en", "US")).toLowerCase() + "]  inside [" + this.getClass().getName() + "] due to ["
              + e.getMessage() + "] date will be null.");
          return null;
          
        }
      } else {
        logger.warn("No nodes of class [review_item_date] or [date] found. Date will be null.");
      }
      
    }
    // method1
    try {
      return sd.parse(date);
    } catch (ParseException e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
      return null;
    }
  }
  
  @Override
  public String findCommentAuthorLocation(TagNode commentNode) {
    String authorLocation = findNodeTextByClass(commentNode, "location");
    if (authorLocation == null) {
      logger.trace("Comment's authorLocation not found. Using method2");
      authorLocation = findNodeTextByClass(commentNode, "reviewer_country");
    }
    return authorLocation;
  }
  
  @Override
  public String findCommentAuthor(TagNode commentNode) {
    String author = findNodeTextByClass(commentNode, "name");
    if (author == null) {
      logger.trace("Comment's author not found. Using method2");
      TagNode[] tempAuthor = commentNode.getElementsByName("h4", true);
      if (tempAuthor != null && tempAuthor.length > 0) {
        author = tempAuthor[0].getText().toString();
      }
    }
    return author;
    
  }
  
  @Override
  public String findCommentTitle(TagNode commentNode) {
    String title = findNodeTextByClass(commentNode, "review_item_header_content");
    if (title != null) {
      int firstQuote = title.indexOf("\"");
      int lastQuote = title.lastIndexOf("\"");
      if (lastQuote != -1) {
        title = title.substring(0, lastQuote);
      }
      if (firstQuote != -1) {
        title = title.substring(firstQuote + 1);
      }
      
    }
    
    return title;
  }
  
  @Override
  public String findCommentDescripcion(TagNode commentNode) {
    String good = findNodeTextByClass(commentNode, "comments_good");
    String bad = findNodeTextByClass(commentNode, "comments_bad");
    if (good == null) {
      logger.trace("Comment's description (good) is empty. Trying method2.");
      good = findNodeTextByClass(commentNode, "review_neg");
    }
    
    if (bad == null) {
      logger.trace("Comment's description (bad) is empty. Trying method2.");
      bad = findNodeTextByClass(commentNode, "review_pos");
    }
    
    if (good == null && bad == null) {
      logger.trace("No method has found comment's description. Returning null. ");
      return null;
    }
    
    String comments = "";
    if (good != null) {
      comments += good;
    }
    if (bad != null) {
      if (!comments.isEmpty()) {
        comments += ". ";
      }
      comments += bad;
    }
    return comments;
  }
  
}
