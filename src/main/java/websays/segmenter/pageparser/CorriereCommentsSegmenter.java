/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama  
 * Contributors: Marco Martinez
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import websays.segmenter.base.MinimalDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class CorriereCommentsSegmenter implements MinimalDocumentSegmenter {
  
  static Logger logger = Logger.getLogger(CorriereCommentsSegmenter.class);
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    
    int position = 1;
    String site = null;
    if (url != null) {
      site = url.toString();
    }
    
    JSONObject jsonRoot = new JSONObject(content);
    JSONArray posts = jsonRoot.getJSONArray("commentList");
    for (int i = 0; i < posts.length(); i++) {
      try {
        JSONObject post = posts.getJSONObject(i);
        SimpleItem item = new SimpleItem();
        item.APIObjectID = post.getString("commentId");
        item.body = post.getString("commentText");
        item.APIAuthorID = post.getString("privateUserId");
        // Don't delete this try catch because if to catch and continue the item evaluate if this is not a comment of a comment
        try {
          item.parentID = post.getString("commentReplyId");
        } catch (Exception e) {
          
        }
        
        item.author = post.getString("userName");
        try {
          item.date = new Date(post.getLong("publicationDate"));
          item.timeIsReal = true;
        } catch (Exception e) {
          logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
          item.date = null;
        }
        
        // this is the avatar of the user, not the image of the post
        // item.imageLink = post.getString("avatar");
        item.comments = post.getInt("repliesCount");
        item.postUrl = site;
        item.position = position++;
        item.isComment = true;
        items.add(item);
      } catch (Exception e) {
        logger.error(e.getMessage());
        continue;
      }
      
    }
    return items;
  }
}