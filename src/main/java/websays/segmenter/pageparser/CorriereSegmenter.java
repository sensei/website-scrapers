/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors: Marco Martinez
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.htmlcleaner.TagNode;
import org.json.JSONArray;
import org.json.JSONObject;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class CorriereSegmenter extends BaseHTMLSegmenter {
  
  private final static Pattern encodingPattern = Pattern.compile("uuid_path_articolo = '([^\\\"\\\\]*(?:\\\\.[^\\\"\\\\]*)*)';");
  
  private final static Pattern moodsPattern = Pattern.compile("var emotional = (.*?);");
  
  private static final Logger logger = Logger.getLogger(CorriereSegmenter.class);
  
  private String uuid_articolo = null;
  
  private String urlSite = null;
  
  private enum MoodType1 {
    INDIGNATO, TRISTE, PREOCCUPATO, SODDISFATTO, DIVERTITO
  }
  
  private enum MoodType2 {
    sad, angry, worried, happy, smile
  }
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    // recreate the list of items
    urlSite = url.toString();
    setItems(new ArrayList<SimpleItem>());
    TagNode rootNode = getRootNode(content);
    SimpleItem mainItem = addSimpleItem(createMainItem(rootNode, url.toString()));
    for (SimpleItem comment : addComments(rootNode, mainItem)) {
      addSimpleItem(comment);
    }
    return getItems();
  }
  
  @Override
  public String findMainAPIObjectID(TagNode rootNode) {
    try {
      uuid_articolo = findNodeTextById(rootNode, "uuid_articolo");
      if (uuid_articolo == null) {
        uuid_articolo = findIdArticleByPattern(rootNode);
        if (uuid_articolo != null) {
          uuid_articolo.replace("/", "");
        }
      }
      if (uuid_articolo == null) {
        try {
          uuid_articolo = urlSite.substring(urlSite.length() - 42, urlSite.length() - 6);
          uuid_articolo = uuid_articolo.replace("-", "");
          uuid_articolo = StringUtils.join(uuid_articolo.split("(?<=\\G.{2})"), "/");
        } catch (Exception e) {
          logger.error(e.getMessage());
        }
      }
    } catch (Exception e) {
      logger.error(e.getMessage());
    }
    return uuid_articolo;
  }
  
  @Override
  public List<String> findMainThreadURLs(TagNode rootNode) {
    String host = "http://xml2.temporeale.corriereobjects.it/community-corriere/commenti/articoli/";
    String commentHead = "/final/comment-head.json";
    String commentBody = "/final/comment-body.json";
    uuid_articolo = findNodeTextById(rootNode, "uuid_articolo");
    String articleid;
    ArrayList<String> threadURLs = new ArrayList<String>();
    if (uuid_articolo != null) {
      
      uuid_articolo = uuid_articolo.replace(" ", "");
      articleid = uuid_articolo.replace("-", "");
      if (articleid.length() % 2 != 0) { // controllo pari/dispari
        articleid = '0' + articleid;
      }
      
      String articleidsplit = StringUtils.join(articleid.split("(?<=\\G.{2})"), "/");
      if (articleidsplit != null && !articleidsplit.isEmpty()) {
        threadURLs.add(host + articleidsplit + commentHead);
        threadURLs.add(host + articleidsplit + commentBody);
      }
    } else {
      uuid_articolo = findIdArticleByPattern(rootNode);
      if (uuid_articolo != null) {
        threadURLs.add(host + uuid_articolo + commentHead);
        threadURLs.add(host + uuid_articolo + commentBody);
      } else {
        if (uuid_articolo == null) {
          try {
            uuid_articolo = urlSite.substring(urlSite.length() - 42, urlSite.length() - 6);
            uuid_articolo = uuid_articolo.replace("-", "");
            uuid_articolo = StringUtils.join(uuid_articolo.split("(?<=\\G.{2})"), "/");
            if (uuid_articolo != null) {
              threadURLs.add(host + uuid_articolo + commentHead);
              threadURLs.add(host + uuid_articolo + commentBody);
            }
          } catch (Exception e) {
            logger.error(e.getMessage());
          }
        }
      }
      
    }
    return threadURLs;
    
  }
  
  private String findIdArticleByPattern(TagNode rootNode) {
    Matcher matcher = encodingPattern.matcher(rootNode.getText());
    if (matcher.find()) {
      uuid_articolo = matcher.group(1);
    }
    return uuid_articolo;
  }
  
  @Override
  public Date findMainDate(TagNode rootNode) {
    try {
      TagNode heading = rootNode.findElementByAttValue("name", "DC.date.issued", true, false);
      if (heading != null) {
        String content = StringEscapeUtils.unescapeXml(heading.getAttributeByName("content"));
        String publishedTime = removeWhiteSpaces(content);
        timeisReal = true;
        return DatatypeConverter.parseDateTime(publishedTime).getTime();
      } else {
        return null;
      }
    } catch (Exception e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
      return null;
    }
  }
  
  @Override
  public String findMainDescripcion(TagNode rootNode) {
    String body = new String();
    List<TagNode> paragraphsNode = null;
    paragraphsNode = rootNode.getElementListByAttValue("class", "chapter-paragraph", true, false);
    if (paragraphsNode != null && paragraphsNode.size() > 0) {
      for (TagNode tagNodeP : paragraphsNode) {
        body = body.concat(tagNodeP.getText().toString());
      }
    } else {
      paragraphsNode = rootNode.getElementListByAttValue("id", "content-to-read", true, false);
      if (paragraphsNode != null && paragraphsNode.size() > 0) {
        for (TagNode tagNode : paragraphsNode) {
          TagNode[] paragraphs = tagNode.getElementsByName("p", false);
          if (paragraphs != null && paragraphs.length > 0) {
            for (TagNode tagNodeP : paragraphs) {
              body = body.concat(tagNodeP.getText().toString());
            }
          }
        }
      }
    }
    return body;
  }
  
  @Override
  public Boolean findMainTimeIsReal(TagNode rootNode) {
    findMainDate(rootNode);
    return timeisReal;
  }
  
  @Override
  public Integer findMainComments(TagNode rootNode) {
    Integer numOfComments = 0;
    try {
      String commentsUrl = findMainThreadURLs(rootNode).get(0);
      if (commentsUrl != null) {
        URL website = new URL(commentsUrl);
        URLConnection connection = website.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        
        StringBuilder content = new StringBuilder();
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
          content.append(inputLine);
        }
        in.close();
        
        JSONObject jsonRoot = new JSONObject(content.toString());
        numOfComments = jsonRoot.getInt("totalcount");
      }
    } catch (Exception e) {
      logger.error("Error getting number of comments from Corriere url: " + urlSite + " Error: " + e);
    }
    return numOfComments;
  }
  
  @SuppressWarnings("unchecked")
  @Override
  public List<String> findMainSourceExtraData(TagNode rootNode) {
    MoodCorriere mood = new MoodCorriere();
    int way = 0;
    TagNode emotionsNode = rootNode.findElementByAttValue("id", "box-emotional", true, false);
    if (emotionsNode != null) {
      way = 1;
    } else {
      emotionsNode = rootNode.findElementByAttValue("class", "article-rate", true, false);
      way = 2;
    }
    String sPorcentage = null;
    String moodType = null;
    if ((way == 1 || way == 2) && emotionsNode != null) {
      List<TagNode> emotions;
      emotions = emotionsNode.getElementListByName("li", true);
      for (TagNode tagNode : emotions) {
        sPorcentage = null;
        moodType = null;
        if (way == 1) {
          moodType = tagNode.getText().toString().trim();
          sPorcentage = tagNode.findElementByAttValue("class", "num", true, false).getText().toString();
          if (sPorcentage != null && sPorcentage.equals("")) {
            sPorcentage = new String("0");
          }
          moodEqual(mood, sPorcentage, moodType);
        } else if (way == 2) {
          TagNode tModeType = (TagNode) tagNode.getElementListByName("span", true).get(1);
          String attName = tModeType.getAttributeByName("class");
          moodType = attName.substring(attName.lastIndexOf("-") + 1);
          sPorcentage = tagNode.findElementByAttValue("class", "perce", true, false).getText().toString().trim();
          if (sPorcentage != null && sPorcentage.equals("")) {
            sPorcentage = new String("0");
          }
          if (moodType.equals(MoodType2.angry.toString())) {
            mood.setAngry(new Integer(sPorcentage));
          } else if (moodType.equals(MoodType2.happy.toString())) {
            mood.setWorried(new Integer(sPorcentage));
          } else if (moodType.equals(MoodType2.sad.toString())) {
            mood.setHappy(new Integer(sPorcentage));
          } else if (moodType.equals(MoodType2.worried.toString())) {
            mood.setSad(new Integer(sPorcentage));
          } else if (moodType.equals(MoodType2.smile.toString())) {
            mood.setSmile(new Integer(sPorcentage));
          }
        }
      }
    }
    
    // Last resource any way the dam percentage can not be catch in anyway before
    if (mood.getAngry() == 0 && mood.getHappy() == 0 && mood.getSad() == 0 && mood.getSmile() == 0 && mood.getWorried() == 0) {
      Matcher matcher = moodsPattern.matcher(rootNode.getText());
      if (matcher.find()) {
        String moodJson = matcher.group(1);
        JSONObject obj = new JSONObject(moodJson);
        JSONArray emotionalList = new JSONArray();
        emotionalList = obj.getJSONArray("emotionalList");
        for (int i = 0; i < emotionalList.length(); i++) {
          JSONObject moodItem = new JSONObject();
          moodItem = (JSONObject) emotionalList.get(i);
          moodType = moodItem.getString("emotionalVoteText");
          sPorcentage = String.valueOf(moodItem.getInt("percentage"));
          moodEqual(mood, sPorcentage, moodType.toUpperCase());
        }
      }
    }
    List<String> result = null;
    if (mood.getAngry() > 0 || mood.getHappy() > 0 || mood.getSad() > 0 || mood.getSmile() > 0 || mood.getWorried() > 0) {
      result = new ArrayList<String>();
      result.add(mood.moodToString());
    }
    return result;
  }
  
  private void moodEqual(MoodCorriere mood, String sPorcentage, String moodType) {
    if (moodType.equals(MoodType1.INDIGNATO.toString())) {
      mood.setAngry(new Integer(sPorcentage));
    } else if (moodType.equals(MoodType1.PREOCCUPATO.toString())) {
      mood.setWorried(new Integer(sPorcentage));
    } else if (moodType.equals(MoodType1.SODDISFATTO.toString())) {
      mood.setHappy(new Integer(sPorcentage));
    } else if (moodType.equals(MoodType1.TRISTE.toString())) {
      mood.setSad(new Integer(sPorcentage));
    } else if (moodType.equals(MoodType1.DIVERTITO.toString())) {
      mood.setSmile(new Integer(sPorcentage));
    }
  }
}
