/**
 * Websays Opinion Analytics Engine 
 * 
 * (Websays Copyright © 2010-2015. All rights reserved. http://websays.com )
 * 
 * Primary Author: Jorge Valderrama
 * Contributors: 
 * Date: 27/2/2015
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.segmenter.common.Utils;
import websays.types.clipping.SimpleItem;

/**
 *
 **/
public class CritizenSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(CritizenSegmenter.class);
  
  String site = null;
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    
    int position = 1;
    if (url != null) {
      site = url.toString();
    }
    
    // Convert htmlComments string into HTML to get all comment one by one iterating over their
    HtmlCleaner cleaner = createCleaner();
    TagNode root = null;
    root = cleaner.clean(content);
    @SuppressWarnings("unchecked")
    List<TagNode> comments = root.getElementListByName("article", true);
    if (comments != null && comments.size() > 0) {
      for (TagNode commentNode : comments) {
        SimpleItem item = new SimpleItem();
        item.APIObjectID = commentNode.getAttributeByName("data-critic-id");
        item.threadId = item.APIObjectID;
        item.source_extra_metadata = new ArrayList<String>();
        item.source_extra_metadata.add("sentment:" + commentNode.getAttributeByName("data-critic-score"));
        item.body = removeWhiteSpaces(commentNode.findElementByAttValue("class", "critic-text link-dark", true, false).getText().toString());
        item.itemUrl = commentNode.findElementByAttValue("class", "critic-text link-dark", true, false).findElementByName("a", true)
            .getAttributeByName("href");
        item.author = commentNode.findElementByAttValue("class", "user-name", true, false).getAllElements(true)[5].getText().toString();
        item.postUrl = site;
        item.position = position++;
        if (position == 2 && !Utils.isNumeric(site.substring(site.lastIndexOf("/") + 1, site.length()))) {
          item.threadURLs = new ArrayList<String>();
          item.threadURLs = findMainThreadURLs(root);
        }
        item.isComment = true;
        items.add(item);
      }
    }
    
    return items;
  }
  
  @Override
  public List<String> findMainThreadURLs(TagNode rootNode) {
    ArrayList<String> commentsUrl = new ArrayList<String>();
    TagNode commentsNode = rootNode.findElementByAttValue("class", "pagination pull-right", true, false);
    if (commentsNode != null) {
      TagNode[] commentsPagination = commentsNode.getElementsByName("a", true);
      if (commentsPagination != null && commentsPagination.length > 0) {
        for (TagNode tagNode : commentsPagination) {
          String page = tagNode.getText().toString();
          if (page != null && !page.isEmpty() && Utils.isNumeric(page) && !page.equals("1")) {
            commentsUrl.add(site + "/" + page);
          }
        }
      }
    }
    return commentsUrl;
  }
  
}
