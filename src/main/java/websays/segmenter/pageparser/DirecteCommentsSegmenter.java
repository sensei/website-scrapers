/**
 * Websays Opinion Analytics Engine 
 * 
 * (Websays Copyright © 2010-2015. All rights reserved. http://websays.com )
 * 
 * Primary Author: Jorge Valderrama
 * Contributors: 
 * Date: 26/2/2015
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

/**
 *
 **/
public class DirecteCommentsSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(DirecteCommentsSegmenter.class);
  
  private static final SimpleDateFormat sdf = new SimpleDateFormat("dd  MMM  yyyy HH.mm ", new Locale("ca", "ES"));
  
  private final static Pattern datePattern = Pattern.compile("(\\d\\d de .*)");
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    
    int position = 1;
    String site = null;
    if (url != null) {
      site = url.toString();
    }
    
    // Convert htmlComments string into HTML to get all comment one by one iterating over their
    HtmlCleaner cleaner = createCleaner();
    TagNode root = null;
    root = cleaner.clean(content);
    @SuppressWarnings("unchecked")
    List<TagNode> comments = root.getElementListByName("div", true);
    if (comments != null && comments.size() > 0) {
      for (TagNode commentNode : comments) {
        if (commentNode.hasAttribute("id") && commentNode.getAttributeByName("id").startsWith("comentari_")) {
          SimpleItem item = new SimpleItem();
          item.APIObjectID = commentNode.getAttributeByName("id");
          String nameClassBody = "comentari-cnt-"
              + item.APIObjectID.substring(item.APIObjectID.indexOf("_") + 1, item.APIObjectID.length());
          item.body = commentNode.findElementByAttValue("id", nameClassBody, true, false).getText().toString();
          item.threadId = item.APIObjectID;
          item.author = commentNode.findElementByAttValue("class", "nom", true, false).getAllElements(true)[0].getText().toString();
          item.itemUrl = commentNode.findElementByAttValue("class", "nom", true, false).findElementHavingAttribute("href", false)
              .getAttributeByName("href");
          
          String sDate = commentNode.findElementByAttValue("class", "data_comentari", true, false).getText().toString();
          if (sDate != null) {
            try {
              // 12 de febrer de 2015 15:25 h
              Matcher matcher = datePattern.matcher(sDate);
              if (matcher.find()) {
                sDate = matcher.group(1);
                sDate = sDate.replace("de", "");
                sDate = sDate.replace("h", "");
                sDate = sDate.replace(",", "");
                item.date = sdf.parse(sDate);
                item.timeIsReal = true;
              }
            } catch (Exception e) {
              logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
              item.date = null;
            }
          }
          
          item.postUrl = site;
          item.position = position++;
          item.isComment = true;
          items.add(item);
        }
      }
    }
    return items;
  }
}
