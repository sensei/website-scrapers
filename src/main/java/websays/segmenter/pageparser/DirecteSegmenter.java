/**
 * Websays Opinion Analytics Engine 
 * 
 * (Websays Copyright © 2010-2015. All rights reserved. http://websays.com )
 * 
 * Primary Author: Jorge Valderrama
 * Contributors: 
 * Date: 26/2/2015
 */
package websays.segmenter.pageparser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.segmenter.common.Utils;
import websays.types.clipping.SimpleItem.LanguageTag;


public class DirecteSegmenter extends BaseHTMLSegmenter {
  
  
  private static final Logger logger = Logger.getLogger(DirecteSegmenter.class);
  
  private static final SimpleDateFormat sdf = new SimpleDateFormat("dd  MMM  yyyy HH:mm  ", new Locale("ca", "ES"));
  
  private static final SimpleDateFormat sdf1 = new SimpleDateFormat("dd  MMM  yyyy HH:mm ", new Locale("ca", "ES"));
  
  private final static Pattern datePattern = Pattern.compile("(\\d\\d de .*)");
  
  // http://www.directe.cat/noticia/387079/f
  
  @Override
  public Date findMainDate(TagNode rootNode) {
    String fdate = null;
    Date date = null;
    TagNode dateNode = rootNode.findElementByAttValue("id", "maincontent", true, false);
    if (dateNode != null) {
      dateNode = dateNode.findElementByAttValue("class", "data", true, false);
      if (dateNode != null) {
        try {
          // 12 de febrer de 2015 15:25 h
          fdate = dateNode.getText().toString();
          Matcher matcher = datePattern.matcher(rootNode.getText());
          if (matcher.find()) {
            fdate = matcher.group(1);
            fdate = fdate.replace("de", "");
            fdate = fdate.replace("h", "");
            date = sdf.parse(fdate);
            timeisReal = true;
          }
        } catch (ParseException e) {
          try {
            date = sdf1.parse(fdate);
          } catch (Exception e2) {
            logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
            return null;
          }
        }
      }
    }
    return date;
  }
  
  @Override
  public Boolean findMainTimeIsReal(TagNode rootNode) {
    findMainDate(rootNode);
    return timeisReal;
  }
  
  @Override
  public LanguageTag findMainLanguage(TagNode rootNode) {
    return LanguageTag.catalan;
  }
  
  @Override
  public List<String> findMainThreadURLs(TagNode rootNode) {
    ArrayList<String> commentsUrl = new ArrayList<String>();
    TagNode commentsNode = rootNode.findElementByAttValue("class", "paginacio_comentaris", true, false);
    if (commentsNode != null) {
      TagNode[] commentsPagination = commentsNode.getElementsByName("a", false);
      if (commentsPagination != null && commentsPagination.length > 0) {
        TagNode nodeComentaries = rootNode.findElementByAttValue("id", "comentari-formulari", true, false);
        if (nodeComentaries != null) {
          TagNode nodeIdAriticle = rootNode.findElementByAttValue("id", "id_article", true, false);
          if (nodeIdAriticle != null) {
            String id_article = nodeIdAriticle.getAttributeByName("value");
            if (id_article != null && !id_article.isEmpty()) {
              commentsUrl.add("http://www.directe.cat/ajax/ajax.php/" +id_article + "/" + 1);
              for (TagNode tagNode : commentsPagination) {
                String page = tagNode.getText().toString();
                if (page != null && !page.isEmpty() && Utils.isNumeric(page) ) {
                  commentsUrl.add("http://www.directe.cat/ajax/ajax.php/" +id_article + "/" + page);
                }
              }
            }
          } 
        }
      }
    } 
    return commentsUrl;
  }
  
}
