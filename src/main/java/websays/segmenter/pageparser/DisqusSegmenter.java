/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.json.JSONArray;
import org.json.JSONObject;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class DisqusSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(DisqusSegmenter.class);
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    HtmlCleaner cleaner = createCleaner();
    
    int position = 1;
    TagNode root = null;
    String site = null;
    if (url != null) {
      site = url.toString();
    }
    root = cleaner.clean(content);
    TagNode contents = root.findElementByAttValue("id", "disqus-threadData", true, false);
    JSONObject jsonRoot = new JSONObject(contents.getText().toString());
    
    JSONObject response = jsonRoot.getJSONObject("response");
    JSONObject thread = response.getJSONObject("thread");
    String forum = thread.getString("forum");
    String id = thread.getJSONArray("identifiers").getString(0);
    
    JSONArray posts = response.getJSONArray("posts");
    for (int i = 0; i < posts.length(); i++) {
      JSONObject post = posts.getJSONObject(i);
      SimpleItem item = new SimpleItem();
      item.body = post.getString("message").replaceAll("<.*?>", "").replaceAll("\\s+", " ");
      item.author = post.getJSONObject("author").getString("name");
      item.likes = post.getInt("likes");
      try {
        item.date = DatatypeConverter.parseDateTime(post.getString("createdAt")).getTime();
        item.timeIsReal = true;
      } catch (Exception e) {
        logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
        item.date = null;
      }
      
      item.postUrl = site;
      item.itemUrl = site + "#post-" + post.getString("id");
      item.threadId = forum + "-" + id;
      
      item.position = position++;
      items.add(item);
    }
    return items;
  }
}
