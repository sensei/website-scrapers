/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge VAlderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class ElConfidencialCommentsSegmenter extends BaseHTMLSegmenter {
  
  private static final SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
  
  private static final Logger logger = Logger.getLogger(ElConfidencialCommentsSegmenter.class);
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    HtmlCleaner cleaner = createCleaner();
    
    int position = 1;
    TagNode root = null;
    String site = null;
    if (url != null) {
      site = url.toString();
    }
    
    root = cleaner.clean(content);
    
    TagNode commentsNode = root.findElementByAttValue("class", "comments-container", true, false);
    ArrayList<SimpleItem> items = null;
    if (commentsNode != null) {
      TagNode[] comments = commentsNode.getAllElements(false)[1].getAllElements(false)[3].getAllElements(false);
      if (comments.length > 0) {
        items = new ArrayList<SimpleItem>();
        for (TagNode tagNode : comments) {
          SimpleItem item = new SimpleItem();
          item.threadId = tagNode.findElementByAttValue("name", "comment-id", true, false).getAttributeByName("value");
          item.itemUrl = tagNode.findElementByAttValue("class", "permalink", true, false).getAllElements(true)[0]
              .getAttributeByName("href").toString();
          String title = tagNode.findElementHavingAttribute("title", true).getText().toString();
          if (title != null) {
            title = title.contains("class=\"id_reply comment-user\">") ? title.replace("class=\"id_reply comment-user\">", "") : title;
          }
          item.title = title;
          String body = tagNode.findElementByAttValue("itemprop", "commentText", true, false).getText().toString();
          if (body != null) {
            body = body.contains("class=\"id_reply comment-user\">") ? body.replace("class=\"id_reply comment-user\">", "") : body;
          }
          item.body = body;
          String publishedTime = tagNode.findElementByAttValue("class", "comment-publication-date", true, false).getText().toString();
          try {
            item.date = sdf.parse(publishedTime);
          } catch (Exception e) {
            logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
            item.date = null;
          }
          
          item.position = position++;
          item.postUrl = site;
          item.isComment = true;
          if (position == 2) {
            // http://www.elconfidencial.com/comunidad/noticia/147746/0/1/
            int positionReplace = site.length() - 2;
            int nextPage = Integer.valueOf(site.charAt(site.length() - 2)) + 1;
            StringBuilder nextUrl = new StringBuilder(site);
            nextUrl.setCharAt(positionReplace, (char) nextPage);
            item.threadURLs = Arrays.asList(nextUrl.toString());
          }
          items.add(item);
        }
      } else {
        items = new ArrayList<SimpleItem>();
        SimpleItem item = new SimpleItem();
        item.fakeItem = true;
        item.postUrl = site;
        item.position = 1;
        items.add(item);
        logger.debug("Fake Clipping with URL due to no comments in the URL added in threadsURL " + site);
      }
    }
    return items;
  }
}
