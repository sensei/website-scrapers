/**
 * Websays Opinion Analytics Engine 
 * 
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 * 
 * Primary Author: Jorge Valderrama
 * Contributors: 
 * Date: 27/1/2015
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.json.JSONObject;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

/**
 *
 **/
public class ElConfidencialDigitalCommentsSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(ElConfidencialDigitalCommentsSegmenter.class);
  
  private static final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy -  HH:mm");
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    // Used a few lines below because we need to parse JSON first (before HTML unescaping)
    // content = segmentInit(content, url);
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    
    int position = 1;
    String site = null;
    if (url != null) {
      site = url.toString();
    }
    
    // Removes the first and last characters because is a JSON object without these character and can be manupule better
    content = content.substring(1, content.length() - 1);
    JSONObject jsonRoot = new JSONObject(content);
    String htmlComments = jsonRoot.getString("html");
    // Convert htmlComments string into HTML to get all comment one by one iterating over their
    HtmlCleaner cleaner = createCleaner();
    TagNode root = null;
    root = cleaner.clean(segmentInit(htmlComments, url));
    @SuppressWarnings("unchecked")
    List<TagNode> comments = root.getElementListByAttValue("id", "comment_", true, false);
    if (comments != null && comments.size() > 0) {
      for (TagNode commentNode : comments) {
        SimpleItem item = new SimpleItem();
        item.APIObjectID = commentNode.findElementByAttValue("class", "comment-count", true, false).getText().toString();
        item.body = commentNode.findElementByAttValue("class", "comment", true, false).getText().toString();
        item.threadId = item.APIObjectID;
        item.author = commentNode.findElementByAttValue("class", "info", true, false).getAllElements(true)[0].getText().toString();
        
        try {
          String sDate = commentNode.findElementByAttValue("class", "info", true, false).getAllElements(true)[1].getText().toString();
          sDate = removeWhiteSpaces(sDate);
          item.date = sdf.parse(sDate.substring(0, sDate.length() - 1));
          item.timeIsReal = true;
        } catch (Exception e) {
          logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
          item.date = null;
        }
        
        item.postUrl = site;
        item.position = position++;
        item.isComment = true;
        items.add(item);
      }
    }
    return items;
  }
}
