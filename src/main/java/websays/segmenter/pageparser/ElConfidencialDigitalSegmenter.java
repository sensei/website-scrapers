/**
 * Websays Opinion Analytics Engine 
 * 
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 * 
 * Primary Author: Jorge Valderrama
 * Contributors: 
 * Date: 27/1/2015
 */
package websays.segmenter.pageparser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem.LanguageTag;


/**
 *
 **/
public class ElConfidencialDigitalSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(ElConfidencialDigitalSegmenter.class);
  
  private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
  
  private static final String comemmtsUrl = "http://www.elconfidencialdigital.com/";
  
  private static final Pattern encodingPattern = Pattern.compile("(bbtcomment\\/entityComments\\/.*\")");
  
  @Override
  public Date findMainDate(TagNode rootNode) {
    Date parse = null;
    TagNode mainDateNode = rootNode.findElementByAttValue("class", "pg-hd", true, false);
    if (mainDateNode != null) {
      TagNode paragrahTagDateNode = mainDateNode.findElementByAttValue("class", "dateline", true, false);
      if (paragrahTagDateNode != null) {
        String date = paragrahTagDateNode.findElementByName("small", true).getText().toString();
        try {
          parse = sdf.parse(date);
        } catch (ParseException e) {
          logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
        }
      }
    }
    return parse;
  }
  
  @Override
  public List<String> findMainThreadURLs(TagNode rootNode) {
    List<String> urls = null;
    TagNode comments = rootNode.findElementByAttValue("id", "ecd-comments-module-container", true, false);
    if (comments != null) {
      TagNode commentTag = comments.findElementByName("script", true);
      if (commentTag != null) {
        String url = commentTag.getText().toString();
        try {
          Matcher matcher = encodingPattern.matcher(url);
          if (matcher.find()) {
            url = matcher.group(1);
            url = comemmtsUrl.concat(url).concat("00").replace("\"", "");
            urls = new ArrayList<String>();
            urls.add(url);
          }
        } catch (Exception e) {
          logger.error("Error getting thread url for ElConfidencialDigital newspapper - Comments page "+ e);
        }
      }
    }
    return urls;
  }
  
  
  @Override
  public String findMainDescripcion(TagNode rootNode) {
    String body = new String();
    TagNode paragraphsNode = rootNode.findElementByAttValue("class", "mce-body mce", true, false);
    if (paragraphsNode != null) {
      TagNode[] paragraphs = paragraphsNode.getElementsByName("p", false);
      if (paragraphs != null && paragraphs.length > 0) {
        for (TagNode tagNode : paragraphs) {
          body = body.concat(tagNode.getText().toString());
        }
      }
    }
    return body;
  }
  
  @Override
  public LanguageTag findMainLanguage(TagNode rootNode) {
    return LanguageTag.spanish;
  }
  
}
