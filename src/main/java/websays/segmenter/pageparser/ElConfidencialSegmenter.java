/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem.LanguageTag;

public class ElConfidencialSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(ElConfidencialSegmenter.class);
  
  private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
  
  private static final SimpleDateFormat timeTagFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
  
  private final String comemmtsUrl = "http://www.elconfidencial.com/comunidad/noticia/";
  
  @Override
  public Date findMainDate(TagNode rootNode) {
    Date parse = null;
    
    TagNode heading = rootNode.findElementByAttValue("itemprop", "datePublished", true, false);
    
    if (heading != null) {
      String date = heading.getText().toString();
      String publishedTime = removeWhiteSpaces(date);
      if (publishedTime != null) {
        if (publishedTime.indexOf("(") >= 0) {
          publishedTime = publishedTime.substring(0, publishedTime.indexOf("("));
        }
        try {
          parse = sdf.parse(publishedTime);
        } catch (Exception e) {
          logger.warn("Error parsing date (old method) for [" + this.getClass().getName() + "] due to [" + e.getMessage()
              + "]. The parsed string was [" + publishedTime + "]. Setting date to null");
        }
      } else {
        logger.warn("Date not found using old method. Trying new method (looking for time tag )");
        try {
          publishedTime = heading.getAttributeByName("content");
          parse = timeTagFormat.parse(publishedTime);
        } catch (Exception e) {
          logger.warn("Error parsing date (new method) for [" + this.getClass().getName() + "] due to [" + e.getMessage()
              + "]. The parsed string was [" + publishedTime + "]. Setting date to null");
        }
      }
    }
    
    return parse;
  }
  
  @Override
  public List<String> findMainThreadURLs(TagNode rootNode) {
    List<String> urls = null;
    
    TagNode comments = rootNode.findElementByAttValue("name", "edi_id", true, false);
    if (comments != null) {
      String comment_id = comments.getAttributeByName("content");
      urls = Arrays.asList(comemmtsUrl + comment_id + "/0/1/");
    }
    
    if (urls == null) {
      logger.warn("Comments url not found using the general case. Falling back to the old way.");
      comments = null;
      comments = rootNode.findElementByAttValue("class", "comments-container", true, false);
      if (comments != null) {
        String comment_id = comments.getAttributeByName("id");
        int startIndex = comment_id.indexOf("-") + 1;
        urls = Arrays.asList(comemmtsUrl + comment_id.substring(startIndex) + "/0/1/");
      }
    }
    
    return urls;
  }
  
  @Override
  public String findMainDescripcion(TagNode rootNode) {
    String body = new String();
    TagNode paragraphsNode = rootNode.findElementByAttValue("class", "articleBody ", true, false);
    if (paragraphsNode == null) {
      logger
          .warn("Tried to find the main body in \"articleBody\" class (old way) and not found. Trying \"news-body-center cms-format\" (new way)");
      paragraphsNode = rootNode.findElementByAttValue("class", "news-body-center cms-format ", true, false);
    }
    
    if (paragraphsNode != null) {
      TagNode[] paragraphs = paragraphsNode.getElementsByName("p", false);
      if (paragraphs != null && paragraphs.length > 0) {
        for (TagNode tagNode : paragraphs) {
          body = body.concat(tagNode.getText().toString());
        }
      }
    } else {
      logger.warn("Unable to find the Main Description. Maybe the Segmenter needs to upgrade to a new web format.");
    }
    return body;
  }
  
  @Override
  public LanguageTag findMainLanguage(TagNode rootNode) {
    return LanguageTag.spanish;
  }
}
