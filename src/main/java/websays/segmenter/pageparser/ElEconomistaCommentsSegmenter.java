/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Oct 27, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;
import websays.types.clipping.SimpleItem.LanguageTag;

public class ElEconomistaCommentsSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(ElEconomistaCommentsSegmenter.class);
  
  private static SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy / HH:mm");
  
  // (\/[0-9]\/)
  private final static Pattern encodingPatternPageComments = Pattern.compile("(\\/[0-9]\\/)");
  
  // (\d\d\-\d\d-\d\d\d\d \/ \d\d:\d\d)
  private final static Pattern encodingPatternDate = Pattern.compile("(\\d\\d\\-\\d\\d-\\d\\d\\d\\d \\/ \\d\\d:\\d\\d)");
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    HtmlCleaner cleaner = createCleaner();
    
    int position = 1;
    TagNode root = null;
    String site = null;
    if (url != null) {
      site = url.toString();
    }
    
    root = cleaner.clean(content);
    TagNode commentsNode = root.findElementByAttValue("id", "comm", true, false);
    try {
      if (commentsNode != null) {
        TagNode[] comments = commentsNode.getElementsByAttValue("class", "coment", true, false);
        if (comments != null && comments.length > 0) {
          for (TagNode tagNode : comments) {
            SimpleItem item = new SimpleItem();
            TagNode nodeDate = tagNode.findElementByAttValue("class", "mensaje-fecha", true, false);
            if (nodeDate != null) {
              Matcher matcher = encodingPatternDate.matcher(nodeDate.getText().toString());
              if (matcher.find()) {
                String sdate = matcher.group(1);
                item.date = sdf.parse(sdate);
                item.timeIsReal = true;
              }
              item.author = nodeDate.findElementByAttValue("class", "nombre-usuario", true, false).getText().toString();
              TagNode poinstNode = nodeDate.findElementByAttValue("class", "puntos", true, false);
              if (poinstNode != null) {
                poinstNode = poinstNode.findElementByName("span", false);
                String points = poinstNode != null ? poinstNode.getText().toString() : "0";
                item.likes = Integer.valueOf(points);
              }
            }
            TagNode nodeBody = tagNode.findElementByAttValue("class", "mensaje", true, false);
            if (nodeBody != null) {
              TagNode[] paragraphs = nodeBody.getElementsByName("p", false);
              if (paragraphs != null && paragraphs.length > 0) {
                String body = new String();
                for (TagNode paragraph : paragraphs) {
                  body = body.concat(paragraph.getText().toString() + " ");
                }
                item.body = body;
              }
            }
            item.language = LanguageTag.spanish;
            item.postUrl = site;
            if (position == 1) {
              Matcher matcher = encodingPatternPageComments.matcher(site);
              if (matcher.find()) {
                String current_page = matcher.group(1);
                Integer next_page = Integer.valueOf(current_page.replaceAll("/", "")) + 1;
                site.replace(current_page, "/" + next_page + "/");
                item.threadURLs = new ArrayList<String>(Arrays.asList(site.replace(current_page, "/" + next_page + "/")));
              } else {
                String firstPartSite = site.substring(0, site.lastIndexOf("/"));
                String secondPartSite = site.substring(site.lastIndexOf("/") + 1);
                item.threadURLs = new ArrayList<String>(Arrays.asList(firstPartSite + "/2/" + secondPartSite));
              }
            }
            item.title = findMainTitle(root);
            item.position = position++;
            item.isComment = true;
            items.add(item);
          }
        }
      }
    } catch (Exception e) {
      logger.error(e.getMessage());
    }
    if (items.size() == 0) {
      // This to avoid infinite loop, due items are empty so it goes to Generic Segmenter and start all the process once and other
      SimpleItem item = new SimpleItem();
      item.fakeItem = true;
      item.postUrl = site;
      item.position = 1;
      items.add(item);
      logger.debug("Fake Clipping with URL due to no comments in the URL added in threadsURL " + site);
    }
    return items;
  }
}
