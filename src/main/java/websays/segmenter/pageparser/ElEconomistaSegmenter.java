/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author:Jorge Valderrama
 * Contributors:
 * Date: Oct 24, 2014
 */
package websays.segmenter.pageparser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem.LanguageTag;

public class ElEconomistaSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(ElEconomistaSegmenter.class);
  
  private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy - hh:mm");
  
  private static final String EL_ECONOMISTA_BASE_URL = "http://www.eleconomista.es";
  
  private final static Pattern datePattern = Pattern.compile("(\\d{2}/\\d{2}/\\d{4}\\s+-\\s+\\d{2}:\\d{2})");
  
  // http://www.eleconomista.es/indicadores-espana/noticias/6181497/10/14/3/El-paro-baja-en-195200-personas-en-el-tercer-trimestre-la-tasa-de-desempleo-desciende-hasta-el-2367.html
  
  @Override
  public String findMainAuthor(TagNode rootNode) {
    String author = null;
    TagNode authorNode = rootNode.findElementByAttValue("id", "cuerpo_noticia", true, false);
    if (authorNode != null) {
      authorNode = authorNode.findElementByAttValue("class", "firma", true, false);
      if (authorNode != null) {
        authorNode = authorNode.findElementByAttValue("class", "f-autor", true, false);
        if (authorNode != null) {
          author = removeWhiteSpaces(authorNode.getText().toString());
        }
      }
    }
    
    if (author == null) {
      authorNode = rootNode.findElementByAttValue("id", "cuerpo_noticia", true, false);
      if (authorNode != null) {
        authorNode = authorNode.findElementByAttValue("class", "firma efe", true, false);
        if (authorNode != null) {
          authorNode = authorNode.findElementByAttValue("class", "f-autor", true, false);
          if (authorNode != null) {
            author = removeWhiteSpaces(authorNode.getText().toString());
            if (author == null || author == "") {
              author = "EFE";
            }
          }
        }
      }
    }
    
    if (author == null) {
      logger.warn("Could not get author for " + this.getClass().getName());
    }
    return author;
  }
  
  @Override
  public Date findMainDate(TagNode rootNode) {
    Date date = null;
    TagNode dateNode = rootNode.findElementByAttValue("id", "cuerpo_noticia", true, false);
    if (dateNode != null) {
      dateNode = dateNode.findElementByAttValue("class", "firma", true, false);
      if (dateNode != null) {
        dateNode = dateNode.findElementByAttValue("class", "f-fecha", true, false);
        if (dateNode != null) {
          try {
            String fdate = dateNode.getText().toString();
            // 29/06/2014-13:22
            date = sdf.parse(fdate);
            timeisReal = true;
          } catch (ParseException e) {
            logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
            return null;
          }
        }
      }
    }
    if (date == null) {
      dateNode = rootNode.findElementByAttValue("id", "cuerpo_noticia", true, false);
      Matcher matcher = datePattern.matcher(dateNode.getText().toString());
      if (matcher.find()) {
        String mdate = matcher.group(1);
        try {
          String fdate = removeWhiteSpaces(mdate);
          // 29/06/2014-13:22
          date = sdf.parse(fdate);
          timeisReal = true;
        } catch (ParseException e) {
          logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage());
        }
      }
    }
    if (date == null) {
      logger.warn("Could not get date for " + this.getClass().getName());
    }
    return date;
  }
  
  @Override
  public Boolean findMainTimeIsReal(TagNode rootNode) {
    findMainDate(rootNode);
    return timeisReal;
  }
  
  @Override
  public LanguageTag findMainLanguage(TagNode rootNode) {
    return LanguageTag.spanish;
  }
  
  @Override
  public List<String> findMainThreadURLs(TagNode rootNode) {
    ArrayList<String> commentsUrl = new ArrayList<String>();
    TagNode commentsNode = rootNode.findElementByAttValue("id", "comm", true, false);
    if (commentsNode != null) {
      commentsNode = commentsNode.findElementByAttValue("class", "bott", true, false);
      if (commentsNode != null) {
        commentsNode = commentsNode.findElementByName("ul", true);
        if (commentsNode != null) {
          TagNode[] commentsPagination = commentsNode.getElementsByName("li", false);
          if (commentsPagination != null && commentsPagination.length > 0) {
            for (TagNode tagNode : commentsPagination) {
              String page = tagNode.findElementByName("a", false).getText().toString();
              if (page.equals("1")) {
                String url = tagNode.findElementByName("a", false).getAttributeByName("href");
                commentsUrl.add(EL_ECONOMISTA_BASE_URL + url);
                break;
              }
            }
          }
        }
      } else {
        String url = getMetaContent(rootNode, "original-source");
        if (url != null) {
          commentsUrl.add(url + "#Comentarios");
        }
      }
    }
    return commentsUrl;
  }
  
  @Override
  public String findMainDescripcion(TagNode rootNode) {
    String body = new String();
    TagNode bodyNode = rootNode.findElementByAttValue("id", "cuerpo_noticia", true, false);
    if (bodyNode != null) {
      TagNode[] paragraphs = bodyNode.getElementsByName("p", true);
      if (paragraphs != null && paragraphs.length > 0) {
        for (TagNode tagNode : paragraphs) {
          // not all p tags inside cuerpo_noticia are the news, only those with style=""
          String styleValue = tagNode.getAttributeByName("style");
          if (styleValue == null || "".compareTo(styleValue) == 0) {
            body = body.concat(tagNode.getText().toString() + " ");
          }
        }
      }
    }
    return body;
  }
}
