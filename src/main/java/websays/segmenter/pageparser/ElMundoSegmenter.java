/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.util.Date;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;

/**
 * Comment structure has changed now, comments are requested by Ajax so we'll miss them.
 * 
 * @author hugoz
 *
 */
public class ElMundoSegmenter extends BaseHTMLSegmenter {
  
  static Logger logger = Logger.getLogger(ElMundoSegmenter.class);
  
  @Override
  public String findMainDescripcion(TagNode rootNode) {
    String body = new String();
    TagNode paragraphsNode = rootNode.findElementByAttValue("itemprop", "articleBody", true, false);
    if (paragraphsNode == null) {
      logger.warn("Tried to find the main body with itemprop=\"articleBody\" and not found. Trying id=\"desarrollo_noticia\"");
      paragraphsNode = rootNode.findElementByAttValue("id", "desarrollo_noticia", true, false);
    }
    
    if (paragraphsNode != null) {
      TagNode[] paragraphs = paragraphsNode.getElementsByName("p", true);
      if (paragraphs != null && paragraphs.length > 0) {
        for (TagNode tagNode : paragraphs) {
          body = body.concat(tagNode.getText().toString());
        }
      }
    } else {
      logger.warn("Unable to find the Main Description. Maybe the Segmenter needs to upgrade to a new web format.");
    }
    return body;
  }
  
  @Override
  public Integer getNativeMax() {
    return null;
  }
  
  @Override
  public String findMainAuthor(TagNode rootNode) {
    return getNodeText(rootNode.findElementByAttValue("itemprop", "name", true, false));
  }
  
  @Override
  public String findMainAuthorLocation(TagNode rootNode) {
    return getNodeText(rootNode.findElementByAttValue("itemprop", "worksFor", true, false));
  }
  
  @Override
  public Integer findMainComments(TagNode rootNode) {
    String commentsNum = getNodeText(rootNode.findElementByAttValue("href", "#comentarios", true, false));
    Float tempFloat = parseFloatSafe(commentsNum);
    return tempFloat != null ? tempFloat.intValue() : null;
  }
  
  @Override
  public TagNode[] findComments(TagNode rootNode) {
    TagNode commentSession = rootNode.findElementByAttValue("id", "comentarios-usuarios", true, false);
    if (commentSession != null) {
      return commentSession.getElementsByName("article", true);
    } else {
      return null;
    }
  }
  
  @Override
  public Date findCommentDate(TagNode commentNode) {
    Date date = null;
    try {
      TagNode dataNode = commentNode.findElementByAttValue("itemprop", "dateCreated", true, false);
      date = DatatypeConverter.parseDateTime(dataNode.getAttributeByName("datetime")).getTime();
      timeisReal = true;
      return date;
    } catch (Exception e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
      return null;
    }
  }
  
  @Override
  public String findCommentAuthor(TagNode commentNode) {
    return findNodeTextByClass(commentNode, "autor");
  }
  
  @Override
  public String findCommentDescripcion(TagNode commentNode) {
    return findNodeTextByClass(commentNode, "texto-comentario");
  }
  
  @Override
  public String findCommentImageLink(TagNode commentNode) {
    return findNodeTextByClass(commentNode, "avatar");
  }
  
  @Override
  public Boolean findMainTimeIsReal(TagNode rootNode) {
    findMainDate(rootNode);
    return timeisReal;
  }
}
