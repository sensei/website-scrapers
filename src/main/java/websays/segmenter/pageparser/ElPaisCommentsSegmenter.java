/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import websays.segmenter.base.MinimalDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class ElPaisCommentsSegmenter implements MinimalDocumentSegmenter {
  
  static Logger logger = Logger.getLogger(ElPaisCommentsSegmenter.class);
  
  private static final String TITLE = "<a title=\"";
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    
    JSONParser parser = new JSONParser();
    JSONObject json = (JSONObject) parser.parse(content);
    
    List<SimpleItem> items = new ArrayList<SimpleItem>();
    JSONArray array = (JSONArray) json.get("mensajes");
    String postURL = null;
    // pos 0 is for moderator
    for (int i = 0; i < array.size(); i++) {
      JSONObject msg = (JSONObject) array.get(i);
      if (i == 0) {
        String text = (String) msg.get("contenido");
        
        int indexOfTitle = text.indexOf(TITLE) + TITLE.length();
        postURL = text.substring(indexOfTitle);
        postURL = postURL.substring(0, postURL.indexOf("\""));
        continue;
      }
      
      SimpleItem si = new SimpleItem();
      String author = (String) msg.get("usuarioOrigen");
      String body = (String) msg.get("contenido");
      try {
        Date date = new Date();
        date.setTime(((Long) msg.get("tsMensaje")) * 1000l);
        si.date = date;
        si.timeIsReal = true;
      } catch (Exception e) {
        logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
        si.date = null;
      }
      
      si.author = author;
      si.body = body;
      
      si.postUrl = postURL;
      si.isComment = true;
      items.add(si);
      
    }
    
    return items;
  }
}
