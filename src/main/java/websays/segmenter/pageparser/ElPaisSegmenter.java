/**
* Websays Opinion Analytics Engine
*
* (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
*
* Primary Author: Marco Martinez/Hugo Zaragoza
* Contributors:
* Date: Jul 7, 2014
*/
package websays.segmenter.pageparser;

import java.util.ArrayList;
import java.util.List;

import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;

public class ElPaisSegmenter extends BaseHTMLSegmenter {
  
  final String BASE_COMMENTS_URL = "http://elpais.com/OuteskupSimple?th=1&msg=";
  
  @Override
  public String findMainAuthor(TagNode rootNode) {
    return getMetaText(rootNode.findElementByAttValue("name", "DC.creator", true, false));
  }
  
  @Override
  public String findMainDescripcion(TagNode rootNode) {
    return getNodeText(rootNode.findElementByAttValue("id", "cuerpo_noticia", true, false));
  }
  
  @Override
  public String findMainAPIObjectID(TagNode rootNode) {
    return null;
  }
  
  @Override
  public String findMainAPIAuthorID(TagNode rootNode) {
    return null;
  }
  
  @Override
  public List<String> findMainThreadURLs(TagNode rootNode) {
    List<TagNode> allElements = rootNode.getElementListHavingAttribute("id", true);
    String id = null;
    for (TagNode node : allElements) {
      String name = node.getAttributeByName("id");
      if (name.startsWith("comentarios_noticia_")) {
        id = name.replaceAll("comentarios_noticia_", "");
        break;
      }
    }
    if (id == null) {
      return null;
    }
    
    List<String> urls = new ArrayList<String>();
    urls.add(BASE_COMMENTS_URL + id);
    return urls;
  }
}
