/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors: Jorge Valderrama
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.json.JSONObject;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class EltenedorSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(EltenedorSegmenter.class);
  
  private static SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
  
  private static final Integer MAX_SCORE = 10;
  
  @Override
  public Integer getNativeMax() {
    return MAX_SCORE;
  }
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    // cannot unescape HTML entities before json parsing
    // content = segmentInit(content, url);
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    HtmlCleaner cleaner = createCleaner();
    
    TagNode root = null;
    String site = null;
    if (url != null) {
      site = url.toString();
    }
    
    int position = 1;
    
    if (!site.contains("contentopiniones/2?uidRestaurant=")) {
      root = cleaner.clean(content);
      // /////////// Main Details Item ////////////
      SimpleItem main = new SimpleItem();
      main.position = position;
      main.postUrl = site;
      
      main.title = removeWhiteSpaces(root.findElementByName("h1", true).getText().toString());
      TagNode description = root.findElementByAttValue("id", "description", true, false);
      if (description != null) {
        // First version of the page
        main.body = removeWhiteSpaces(description.getText().toString());
      } else {
        // Last version of the page
        description = root.findElementByAttValue("class", "description clearfix", true, false);
        if (description != null) {
          main.body = removeWhiteSpaces(description.findElementByName("p", true).getText().toString());
        }
      }
      TagNode countCommentsNode = root.findElementByAttValue("itemprop", "ratingCount", true, false);
      if (countCommentsNode != null) {
        main.comments = Integer.parseInt(countCommentsNode.getText().toString());
      }
      
      TagNode averageNode = root.findElementByAttValue("itemprop", "ratingValue", true, false);
      if (averageNode != null) {
        try {
          String score = averageNode.getText().toString();
          score = score.replaceAll(",", ".");
          Float realAverage = new Float(score);
          main.setScores(realAverage, MAX_SCORE);
        } catch (Exception e) {
          logger.error(e);
        }
      }
      items.add(main);
    }
    
    // Validation if site is just a JSON page of comments, not the main page
    if (site.contains("contentopiniones/2?uidRestaurant=")) {
      JSONObject jsonRoot = new JSONObject(content);
      String htmlComments = jsonRoot.getString("content");
      // Convert htmlComments string into HTML to get all comment one by one iterating over their
      cleaner = createCleaner();
      root = cleaner.clean(htmlComments);
    }
    
    TagNode[] reviewsNode = root.getElementsByAttValue("class", "avis m_y hr_b", true, false);
    if (reviewsNode != null && reviewsNode.length > 0) {
      // First version of the page
      for (TagNode reviewNode : reviewsNode) {
        try {
          SimpleItem review = new SimpleItem();
          TagNode reviewDetail = reviewNode.findElementByAttValue("itemprop", "review", true, false);
          review.author = removeWhiteSpaces(reviewDetail.getChildTags()[1].getText().toString());
          review.body = removeWhiteSpaces(reviewDetail.getChildTags()[3].getText().toString());
          try {
            TagNode dateNode = reviewNode.findElementByAttValue("class", "soft", true, false);
            review.date = sd.parse(dateNode.getText().substring(20));
          } catch (Exception e) {
            logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
            review.date = null;
          }
          
          TagNode reviewAverageNode = reviewDetail.findElementByAttValue("class", "bigger", true, false);
          if (reviewAverageNode != null) {
            Float realAverage = new Float(reviewAverageNode.getText().toString().replace(',', '.'));
            review.setScores(realAverage, MAX_SCORE);
          }
          
          review.position = ++position;
          review.postUrl = site;
          
          items.add(review);
        } catch (Exception e) {
          logger.error(e);
        }
      }
    } else {
      // Last version of the page
      reviewsNode = root.getElementsByAttValue("class", "rateItem clearfix", true, false);
      if (reviewsNode != null && reviewsNode.length > 0) {
        for (TagNode reviewNode : reviewsNode) {
          try {
            SimpleItem review = new SimpleItem();
            TagNode authorNode = reviewNode.findElementByAttValue("class", "rateItem-profile", true, false);
            if (authorNode != null) {
              String sauthor = authorNode.getText().toString();
              review.author = removeWhiteSpaces(sauthor.substring(0, sauthor.lastIndexOf(".") + 1));
            }
            TagNode bodyNode = reviewNode.findElementByAttValue("itemprop", "reviewBody", true, false);
            if (bodyNode != null) {
              review.body = removeWhiteSpaces(bodyNode.getText().toString());
            }
            TagNode dateNode = reviewNode.findElementByAttValue("class", "rateItem-date", true, false);
            if (dateNode != null) {
              try {
                String sdate = removeWhiteSpaces(dateNode.getText().toString());
                sdate = sdate.substring(sdate.length() - 10, sdate.length());
                review.date = sd.parse(sdate);
              } catch (Exception e) {
                logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
                review.date = null;
              }
            }
            TagNode averNode = reviewNode.findElementByAttValue("itemprop", "ratingValue", true, false);
            if (averNode != null) {
              Float realAverage = new Float(averNode.getText().toString().replace(',', '.'));
              review.setScores(realAverage, MAX_SCORE);
            }
            
            if (position == 1 && !site.contains("contentopiniones/2?uidRestaurant=")) {
              TagNode threadUrlsNode = root.findElementByAttValue("class", "pagination oneline text_right", true, false);
              if (threadUrlsNode != null) {
                @SuppressWarnings("unchecked")
                List<TagNode> commentsPagesNodes = threadUrlsNode.getElementListByName("li", true);
                if (commentsPagesNodes != null && !commentsPagesNodes.isEmpty()) {
                  try {
                    Integer numPagesForReviewers = Integer.parseInt(commentsPagesNodes.get(commentsPagesNodes.size() - 1).getText()
                        .toString());
                    if (numPagesForReviewers > 0) {
                      review.threadURLs = new ArrayList<String>();
                      for (int i = 2; i <= numPagesForReviewers; i++) {
                        String id = site.substring(site.lastIndexOf("/") + 1, site.length());
                        // System.out.println(site + "/contentopiniones/2?uidRestaurant=" + id +
                        // "&filters%5Bwith_comments_only%5D=1&sort=RESERVATION_DATE_DESC&page=" + i);
                        review.threadURLs.add(site + "/contentopiniones/2?uidRestaurant=" + id
                            + "&filters%5Bwith_comments_only%5D=1&sort=RESERVATION_DATE_DESC&page=" + i);
                        
                      }
                    }
                  } catch (Exception e) {
                    logger.error("Error getting threadUrls for ElTenedorSegmenter, please check this " + e);
                  }
                }
              }
            }
            
            review.isComment = true;
            review.position = ++position;
            review.postUrl = site;
            
            items.add(review);
          } catch (Exception e) {
            logger.error(e);
          }
        }
      }
    }
    return items;
  }
}