/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.io.StringReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.segmenter.base.Layouts;
import websays.types.clipping.SimpleItem;

public class FeedburnerSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(FeedburnerSegmenter.class);
  public static SimpleDateFormat sd = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss z", new Locale("en", "US"));
  
  // public static SimpleDateFormat sd2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US);
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    // cannot unescape HTML entities before db.parse
    // content = segmentInit(content, url);
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    
    try {
      DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
      DocumentBuilder db = dbf.newDocumentBuilder();
      InputSource is = new InputSource(new StringReader(content));
      Document doc = null;
      doc = db.parse(is);
      doc.getDocumentElement().normalize();
      
      String site = null;
      if (url != null) {
        site = url.toString();
      }
      
      NodeList nList = doc.getElementsByTagName("item");
      HashMap<String,String> layout = null;
      if (nList.getLength() == 0) {
        nList = doc.getElementsByTagName("entry");
        layout = Layouts.getLayouts().get("feedburner2");
      } else {
        layout = Layouts.getLayouts().get("feedburner1");
      }
      
      for (int temp = 0; temp < nList.getLength(); temp++) {
        Node nNode = nList.item(temp);
        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
          Element eElement = (Element) nNode;
          SimpleItem simpleItem = new SimpleItem();
          simpleItem.isComment = false; // each item is a separate
          // post, not one post with
          // many comments.
          String description = getElement(layout.get("body"), eElement);
          String unescapeHtml4 = StringEscapeUtils.unescapeHtml4(description);
          simpleItem.body = removeLongSpaces(unescapeHtml4.replaceAll("<(/p|p|ul|li)>", " ").replaceAll("\\<.*?\\>", ""));
          
          simpleItem.title = getElement(layout.get("title"), eElement);
          simpleItem.author = getElement(layout.get("author"), eElement);
          simpleItem.itemUrl = getElement(layout.get("url"), eElement);
          try {
            String date = getElement(layout.get("date"), eElement);
            if (date != null) {
              try {
                simpleItem.date = sd.parse(date);
              } catch (Exception e) {
                Calendar calendar = javax.xml.bind.DatatypeConverter.parseDateTime(date);
                simpleItem.date = calendar.getTime();
              }
            }
          } catch (Exception e) {
            logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
            simpleItem.date = null;
          }
          
          simpleItem.imageLink = getElement(layout.get("image"), eElement);
          
          simpleItem.comments = Integer.parseInt(getElement(layout.get("comments_count"), eElement));
          
          simpleItem.position = 1;
          simpleItem.postUrl = site;
          items.add(simpleItem);
        }
      }
    } catch (Exception e) {
      // let it go, not a proper RSS feed. Maybe some other parser will
      // catch it later...
    }
    
    return items;
  }
  
  private String getElement(String key, Element eElement) {
    Node node = null;
    if (key.contains("##")) {
      String[] parts = key.split("##");
      key = parts[1];
      node = eElement.getElementsByTagName(parts[0]).item(0);
      boolean found = false;
      if (node != null) {
        while (node.hasChildNodes()) {
          if (node.getNodeName().equals(key)) {
            found = true;
            break;
          }
          node = node.getFirstChild().getNextSibling();
        }
      }
      if (!found) {
        return null;
      }
    } else if (key.contains("@@")) {
      String[] parts = key.split("@@");
      key = parts[1];
      node = eElement.getElementsByTagName(parts[0]).item(0);
      NamedNodeMap attributes = node.getAttributes();
      return attributes.getNamedItem(parts[1]).getNodeValue();
    } else {
      node = eElement.getElementsByTagName(key).item(0);
    }
    if (node == null) {
      return null;
    }
    return removeWhiteSpaces(node.getTextContent());
  }
}
