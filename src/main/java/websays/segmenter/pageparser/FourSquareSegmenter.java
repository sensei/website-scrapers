/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class FourSquareSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(FourSquareSegmenter.class);
  
  private static SimpleDateFormat sdSpn = new SimpleDateFormat("MMMM d, yyyy", new Locale("es", "ES"));
  
  private static final Integer MAX_SCORE = 10;
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    HtmlCleaner cleaner = createCleaner();
    
    TagNode root = null;
    String site = null;
    if (url != null) {
      site = url.toString();
    }
    
    root = cleaner.clean(content);
    
    // /////////// Main Details Item ////////////
    SimpleItem main = new SimpleItem();
    TagNode mainInfo = null;
    int position = 1;
    main.position = position;
    try {
      mainInfo = root.getElementsByAttValue("class", "venueInfoSection", true, false)[0];
    } catch (Exception e) {}
    // ///// Extract Title
    try {
      TagNode titleSection = root.getElementsByAttValue("class", "venueName", true, false)[0];
      String title = removeWhiteSpaces(titleSection.getText().toString());
      main.title = title;
    } catch (Exception ex) {}
    // //////////// Extract Body
    StringBuilder s = new StringBuilder(" ");
    try {
      TagNode[] n = root.getElementsByAttValue("class", "venueDescription", true, false);
      // for (int j = 0; j < n.length; j++) {
      // TagNode[] childs = n[j].getAllElements(false);
      // for (int i = 0; i < childs.length; i++) {
      // try {
      // if (!childs[i].getName().startsWith("a")) {
      // String text = removeWhiteSpaces(childs[i].getText().toString());
      // s.append(text != null ? (text + ", ") : "");
      // }
      // } catch (Exception e) {}
      // }
      // }
      String text = removeWhiteSpaces(n[0].getText().toString());
      main.body = text;
      // if (!s.toString().trim().isEmpty()) {
      // String text = s.toString().trim();
      // main.body = text;
      // }
    } catch (Exception ex) {}
    
    // ////////////// Extract Score
    try {
      TagNode[] n = root.getElementsByAttValue("itemprop", "ratingValue", true, false);
      String score = removeWhiteSpaces(n[0].getText().toString());
      try {
        main.setScores(Float.parseFloat(score), MAX_SCORE);
      } catch (Exception e) {
        logger.warn("Exception [" + e.getMessage() + "] while parsing foursquare score value [" + score + "]");
      }
    } catch (Exception ex) {}
    // ///// Extract Votes
    try {
      TagNode[] n = root.getElementsByAttValue("itemprop", "ratingCount", true, false);
      String votes = removeWhiteSpaces(n[0].getText().toString());
      main.votes = Integer.parseInt(removeWhiteSpaces(votes));
    } catch (Exception ex) {}
    // ///// Extract item URL
    try {
      TagNode[] n = root.getElementsByAttValue("class", "venueLinkAttr secondaryAttr websiteLink", true, false);
      String itemURL = removeWhiteSpaces(n[0].getText().toString());
      main.itemUrl = removeWhiteSpaces(itemURL);
    } catch (Exception ex) {}
    
    main.date = null;
    main.postUrl = site;
    
    TagNode tips = null;
    try {
      tips = root.getElementsByAttValue("class", "tipsSections", true, false)[0];
    } catch (Exception e) {}
    // //////////// Extract Reviews
    try {
      TagNode[] n = tips.getElementsByAttValue("class", "tipCount", true, false);
      String reviews = removeWhiteSpaces(n[0].getText().toString());
      Integer rev = parseNumbers(reviews, true);
      main.reviews = rev;
    } catch (Exception ex) {}
    
    if (passMainTest(main)) {
      items.add(main);
    }
    
    // ///////////////// Comment Items
    
    TagNode rev[] = tips.getElementsByAttValue("class", "tip", true, false);
    
    for (TagNode t : rev) {
      SimpleItem item = new SimpleItem();
      item.position = ++position;
      
      // ////// Body
      try {
        item.body = removeWhiteSpaces(t.getElementsByAttValue("class", "tipText", true, false)[0].getText().toString());
      } catch (Exception e) {}
      // /////// author
      try {
        item.author = removeWhiteSpaces(t.getElementsByAttValue("class", "userName", true, false)[0].getText().toString());
      } catch (Exception e) {}
      
      // /////// votes
      try {
        TagNode tg[] = t.getElementsByAttValue("class", "like", true, false);
        String votes = removeWhiteSpaces(tg[0].getText().toString());
        Integer vt = parseNumbers(votes, false);
        item.votes = vt;
      } catch (Exception e) {}
      // /////// date
      try {
        String date = removeWhiteSpaces(t.getElementsByAttValue("class", "tipDate", true, false)[0].getText().toString());
        item.date = sdSpn.parse(date);
      } catch (Exception e) {
        logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
        item.date = null;
      }
      // /////// item URL
      try {
        String itemUrl = removeWhiteSpaces(t.getElementsByAttValue("class", "tipDate", true, false)[0].getElementsByName("a", false)[0]
            .getAttributeByName("href").toString());
        item.itemUrl = itemUrl;
      } catch (Exception e) {}
      item.postUrl = site;
      if (passCommentTest(item)) {
        items.add(item);
      }
    }
    
    return items;
  }
  
  @Override
  public Integer getNativeMax() {
    return MAX_SCORE;
  }
  
}
