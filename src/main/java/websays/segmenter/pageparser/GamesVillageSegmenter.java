/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class GamesVillageSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(GamesVillageSegmenter.class);
  
  private static final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy','hh:mm:ss");
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    HtmlCleaner cleaner = createCleaner();
    
    TagNode root = null;
    String site = null;
    if (url != null) {
      site = url.toString();
    }
    root = cleaner.clean(content);
    
    // /////////// Main Details Item ////////////
    int position = 1;
    TagNode reviewRoot[] = root.getElementsByAttValue("class", "posts", true, false);
    if (reviewRoot != null && reviewRoot.length > 0) {
      TagNode rev[] = reviewRoot[0].getAllElements(false);
      
      for (TagNode t : rev) {
        String id_post = removeWhiteSpaces(t.getAttributeByName("id"));
        if (id_post != null && id_post.trim().startsWith("post_")) {
          try {
            SimpleItem item = new SimpleItem();
            item.threadId = id_post;
            
            // / Title
            TagNode nodeTitle = t.findElementByAttValue("class", "title icon", true, false);
            if (nodeTitle != null) {
              item.title = removeWhiteSpaces(nodeTitle.getText().toString());
            }
            
            // / Author
            TagNode nodeAuthor = t.findElementByAttValue("class", "username offline popupctrl", true, false);
            if (nodeAuthor != null) {
              item.author = removeWhiteSpaces(nodeAuthor.findElementByName("strong", true).getText().toString());
            }
            
            // / Body
            TagNode nodeBody = t.findElementByAttValue("class", "postcontent restore ", true, false);
            item.body = removeWhiteSpaces(nodeBody.getText().toString());
            
            // / Date
            try {
              TagNode nodeDate = t.findElementByAttValue("class", "date", true, false);
              String datePost = removeLongSpaces(removeWhiteSpaces(nodeDate.getText().toString()));
              Date parse = sdf.parse(datePost);
              item.date = parse;
              item.timeIsReal = true;
            } catch (Exception e) {
              logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
              item.date = null;
            }
            
            item.position = position;
            item.postUrl = url.toString();
            position++;
            
            items.add(item);
          } catch (Exception e) {
            logger.error(e);
            
          }
        }
      }
    }
    return items;
  }
}
