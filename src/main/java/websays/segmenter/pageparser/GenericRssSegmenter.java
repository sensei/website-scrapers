/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.io.StringReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class GenericRssSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(GenericRssSegmenter.class);
  
  public static SimpleDateFormat sd = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss z", new Locale("en", "US"));
  
  // image regex
  private static final String http1 = "[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*\\.(jpe?g|png|bmp)";
  private static final String sHTTP = "((https?|ftp|file)://" + http1 + ")";
  private static final Pattern pURL = Pattern.compile(sHTTP);
  
  static final String htmlTagsRegex = "<(\"[^\"]*\"|'[^']*'|[^'\">])*>";
  public static final Pattern pHTML = Pattern.compile(htmlTagsRegex);
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    
    try {
      DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
      DocumentBuilder db = dbf.newDocumentBuilder();
      // Some XML comes with & and must be replaced with &amp; because of if it is not changed will raise an error for format error
      content = content.replaceAll("(&(?!amp;))", "&amp;");
      InputSource is = new InputSource(new StringReader(content));
      Document doc = null;
      doc = db.parse(is);
      doc.getDocumentElement().normalize();
      
      int position = 1;
      String site = null;
      if (url != null) {
        site = url.toString();
      }
      NodeList nList = doc.getElementsByTagName("item");
      
      for (int temp = 0; temp < nList.getLength(); temp++) {
        Node nNode = nList.item(temp);
        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
          Element eElement = (Element) nNode;
          SimpleItem simpleItem = new SimpleItem();
          simpleItem.isComment = false; // each item is a separate
          // post, not one post with
          // many comments.
          String description = getElement("description", eElement);
          simpleItem.body = removeLongSpaces(StringEscapeUtils.unescapeHtml4(description).replaceAll("\\<.*?\\>", ""));
          
          simpleItem.title = getElement("title", eElement);
          simpleItem.author = getElement("author", eElement);
          simpleItem.itemUrl = getElement("link", eElement);
          
          try {
            String date = getElement("pubDate", eElement);
            if (date != null) {
              simpleItem.date = sd.parse(date);
              simpleItem.timeIsReal = true;
            }
          } catch (Exception e) {
            logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
            simpleItem.date = null;
          }
          
          simpleItem.imageLink = getImageUrl("image", "link", eElement);
          if (simpleItem.imageLink == null) {
            simpleItem.imageLink = getImageUrl("content:encoded", "link", eElement);
          }
          simpleItem.position = position++;
          simpleItem.postUrl = site;
          items.add(simpleItem);
        }
      }
    } catch (Exception e) {
      // let it go, not a proper RSS feed. Maybe some other parser will
      // catch it later...
    }
    
    return items;
  }
  
  private String getElement(String key, Element eElement) {
    Node node = eElement.getElementsByTagName(key).item(0);
    if (node == null) {
      return null;
    }
    return removeWhiteSpaces(node.getTextContent());
  }
  
  private String getImageUrl(String key, String base, Element eElement) {
    String urlStr = null;
    try {
      URL context = new URL(getElement(base, eElement));
      URL url = new URL(context, getElement(key, eElement));
      urlStr = url.toString();
      // some rss contains html in this part, so we need to find it and try to get the first appeareance of an image
      Matcher matcher = pHTML.matcher(urlStr);
      if (matcher.find()) {
        // try to get a valid image link
        Matcher matcher2 = pURL.matcher(urlStr);
        if (matcher2.find()) {
          urlStr = matcher2.group(0);
        } else {
          urlStr = null;
        }
        
      }
    } catch (Exception e) {
      urlStr = null;
    }
    return urlStr;
  }
}
