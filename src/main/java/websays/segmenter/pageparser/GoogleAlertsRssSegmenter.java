/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class GoogleAlertsRssSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(GoogleAlertsRssSegmenter.class);
  
  public static SimpleDateFormat sd = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss z", new Locale("en", "US"));
  
  // image regex
  private static final String http1 = "[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*\\.(jpe?g|png|bmp)";
  private static final String sHTTP = "((https?|ftp|file)://" + http1 + ")";
  private static final Pattern pURL = Pattern.compile(sHTTP);
  
  static final String htmlTagsRegex = "<(\"[^\"]*\"|'[^']*'|[^'\">])*>";
  public static final Pattern pHTML = Pattern.compile(htmlTagsRegex);
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    // cannot unescape HTML entities before db.parse
    // content = segmentInit(content, url);
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    
    try {
      DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
      DocumentBuilder db = dbf.newDocumentBuilder();
      InputSource is = new InputSource(new StringReader(content));
      Document doc = null;
      doc = db.parse(is);
      doc.getDocumentElement().normalize();
      
      int position = 1;
      String site = null;
      if (url != null) {
        site = url.toString();
      }
      NodeList nList = doc.getElementsByTagName("entry");
      SimpleItem simpleItem = new SimpleItem();
      simpleItem.fakeItem = true;
      simpleItem.threadURLs = new ArrayList<String>();
      
      for (int temp = 0; temp < nList.getLength(); temp++) {
        Node nNode = nList.item(temp);
        // System.out.println(nNode.getTextContent());
        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
          Element eElement = (Element) nNode;
          // expectedURLs.add("https://www.google.com/url?q=http://www.diariovasco.com/20140324/local/bajo-deba/once-arrasate-201403241122.html&amp;ct=ga&amp;cd=CAIyAA&amp;usg=AFQjCNExS7qiQOw1eN5c_eojP01kq1JrcA");
          Node node = eElement.getElementsByTagName("link").item(0).getAttributes().getNamedItem("href");
          String urlResult = node.getTextContent();
          urlResult = parseGoogleURL(urlResult);
          simpleItem.threadURLs.add(urlResult);
        }
      }
      items.add(simpleItem);
    } catch (Exception e) {
      // let it go, not a proper RSS feed. Maybe some other parser will
      // catch it later...
    }
    
    return items;
  }
  
  private String parseGoogleURL(String urlResult) {
    if (urlResult != null) {
      String paramURL = "?q=";
      int indexOf = urlResult.indexOf(paramURL);
      int lastIndexOf = urlResult.lastIndexOf("&ct");
      if (lastIndexOf == -1) {
        lastIndexOf = urlResult.length();
      }
      urlResult = urlResult.substring(indexOf + paramURL.length(), lastIndexOf);
      try {
        urlResult = URLDecoder.decode(urlResult, "UTF-8");
      } catch (UnsupportedEncodingException e) {
        logger.error(e);
      }
      
    }
    
    return urlResult;
  }
  
  private String getElement(String key, Element eElement) {
    Node node = eElement.getElementsByTagName(key).item(0);
    if (node == null) {
      return null;
    }
    return removeWhiteSpaces(node.getTextContent());
  }
  
  private String getImageUrl(String key, String base, Element eElement) {
    String urlStr = null;
    try {
      URL context = new URL(getElement(base, eElement));
      URL url = new URL(context, getElement(key, eElement));
      urlStr = url.toString();
      // some rss contains html in this part, so we need to find it and try to get the first appeareance of an image
      Matcher matcher = pHTML.matcher(urlStr);
      if (matcher.find()) {
        // try to get a valid image link
        Matcher matcher2 = pURL.matcher(urlStr);
        if (matcher2.find()) {
          urlStr = matcher2.group(0);
        } else {
          urlStr = null;
        }
        
      }
    } catch (Exception e) {
      urlStr = null;
    }
    return urlStr;
  }
}
