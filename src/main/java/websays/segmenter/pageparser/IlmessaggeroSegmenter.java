/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;

public class IlmessaggeroSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(IlmessaggeroSegmenter.class);
  
  @Override
  public TagNode[] findComments(TagNode rootNode) {
    TagNode commenti = rootNode.findElementByAttValue("name", "commenti", true, false);
    TagNode[] childTags = commenti.getParent().getChildTags();
    ArrayList<TagNode> comments = new ArrayList<TagNode>();
    for (TagNode tagNode : childTags) {
      TagNode footer = tagNode.findElementByAttValue("class", "grigio11lucida", true, false);
      if (footer != null) {
        comments.add(tagNode);
      }
    }
    return comments.toArray(new TagNode[comments.size()]);
  }
  
  @Override
  public String findCommentAuthor(TagNode commentNode) {
    TagNode footer = commentNode.findElementByAttValue("class", "grigio11lucida", true, false);
    return getNodeText(footer.findElementByName("b", true));
  }
  
  @Override
  public String findCommentDescripcion(TagNode commentNode) {
    TagNode[] tr = commentNode.getElementsByName("tr", true);
    return getNodeText(tr[1]);
  }
  
  @Override
  public String findCommentTitle(TagNode commentNode) {
    return getNodeText(commentNode.findElementByName("b", true));
  }
  
  @Override
  public Date findCommentDate(TagNode commentNode) {
    SimpleDateFormat sd = new SimpleDateFormat("dd-MM-yyyy 'alle' HH:mm");
    Date date = null;
    try {
      String footer = findNodeTextByClass(commentNode, "grigio11lucida");
      String dateStr = footer.replaceFirst(".*?([\\d-]+ alle [\\d:]+).*", "$1");
      date = sd.parse(dateStr);
      timeisReal = true;
    } catch (ParseException e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
      return null;
    }
    return date;
  }
  
  @Override
  public Boolean findMainTimeIsReal(TagNode rootNode) {
    findMainDate(rootNode);
    return timeisReal;
  }
}
