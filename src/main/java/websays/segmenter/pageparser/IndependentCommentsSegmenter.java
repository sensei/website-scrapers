/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama 
 * Contributors: Marco Martinez
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.htmlcleaner.TagNode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import websays.segmenter.base.MinimalDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class IndependentCommentsSegmenter implements MinimalDocumentSegmenter {
  
  private static final Logger logger = Logger.getLogger(IndependentCommentsSegmenter.class);
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    // HtmlCleaner cleaner = createCleaner();
    
    int position = 1;
    TagNode root = null;
    String site = null;
    if (url != null) {
      site = url.toString();
    }
    
    // root = cleaner.clean(content);
    // JSONObject jsonRoot = new JSONObject(root.getText().toString());
    JSONObject jsonRoot = new JSONObject(content);
    JSONObject mainInfo = jsonRoot.getJSONObject("streamInfo");
    String streamURL = mainInfo.getString("streamURL");
    
    JSONArray posts = jsonRoot.getJSONArray("comments");
    for (int i = 0; i < posts.length(); i++) {
      JSONObject post = posts.getJSONObject(i);
      SimpleItem item = new SimpleItem();
      item.threadId = post.getString("ID");
      item.body = post.getString("commentText");
      item.author = post.getJSONObject("sender").getString("name");
      
      try {
        item.imageLink = post.getJSONObject("sender").getString("photoURL");
      } catch (JSONException e) {
        item.imageLink = null;
      }
      
      item.likes = post.getInt("posVotes");
      item.votes = post.getInt("TotalVotes");
      try {
        item.date = new Date(post.getLong("timestamp"));
        item.timeIsReal = true;
      } catch (Exception e) {
        logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
        item.date = null;
      }
      item.postUrl = streamURL;
      item.position = position++;
      item.isComment = true;
      items.add(item);
      
    }
    return items;
  }
}
