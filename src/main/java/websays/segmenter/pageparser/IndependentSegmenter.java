/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors: Marco Martinez
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class IndependentSegmenter extends HTMLDocumentSegmenter {
  
  private static final String SITE = "#independent";
  private static final Logger logger = Logger.getLogger(IndependentSegmenter.class);
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    HtmlCleaner cleaner = createCleaner();
    
    TagNode root = null;
    String site = null;
    if (url != null) {
      site = url.toString();
    }
    root = cleaner.clean(content);
    
    // /////////// Main Details Item ////////////
    SimpleItem main = new SimpleItem();
    // ///// Extract Title
    main.title = getMetaContent(root, "og:title");
    main.body = getMetaContent(root, "og:description");
    main.date = null;
    main.position = 1;
    main.postUrl = site;
    
    try {
      String publishedTime = getMetaContent(root, "article:published_time");
      if (publishedTime != null) {
        // Beware, this date is implicitly using the default timezone for the implicit Calendar.
        // Should you have problems with dates, consider explicitly setting a timezone for the implicit Calendar.
        main.date = DatatypeConverter.parseDateTime(publishedTime).getTime();
        main.timeIsReal = true;
      }
    } catch (Exception e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
      main.date = null;
    }
    
    TagNode author = root.findElementByAttValue("class", "authorName", true, false);
    if (author != null) {
      main.author = removeWhiteSpaces(author.getText().toString());
    }
    main.postUrl = getMetaContent(root, "og:url");
    
    try {
      main.threadId = this.getVarforComments(root, "streamID");
      if (main.threadId != null) {
        main.threadURLs = new ArrayList<String>();
        main.threadURLs.add("http://comments.us1.gigya.com/comments.getComments?categoryID=ArticleComments&streamID=" + main.threadId
            + "&start=0&threadLimit=50&sort=votesDesc&threadDepth=2&includeStreamInfo=true&includeUID=true&"
            + "APIKey=2_bkQWNsWGVZf-fA4GnOiUOYdGuROCvoMoEN4WMj6_YBq4iecWA-Jp9D2GZCLbzON4" + SITE);
      }
    } catch (Exception e) {
      logger.error("Error parsing comments from: " + url.toExternalForm(), e);
    }
    items.add(main);
    return items;
  }
  
  private String getVarforComments(TagNode root, String var) {
    TagNode commentReference = root.findElementByAttValue("id", "commentReference", true, false);
    String streamId = null;
    if (commentReference != null) {
      TagNode tagNodeForInfoAPI = (TagNode) commentReference.getChildTagList().get(0);
      String javascript = tagNodeForInfoAPI.getChildren().get(0).toString();
      // System.out.println(javascript);
      logger.trace(javascript);
      int indexOf = javascript.indexOf("streamID");
      if (indexOf == -1) {
        return null;
      }
      javascript = javascript.substring(indexOf);
      streamId = javascript.substring(javascript.indexOf("'") + 1, javascript.indexOf(","));
      streamId = streamId.substring(0, streamId.indexOf("'"));
    }
    return streamId;
  }
  
  private String evaluateString(String dataApi) {
    String[] result = dataApi.split(";");
    if (result.length != 4) {
      throw new IllegalArgumentException("String not in correct format to extract comments");
    }
    return result[3];
  }
  
  private String evaluateToConvertJSON(String sentence) {
    Pattern pattern = Pattern.compile("\\{.*\\}");
    Matcher matcher = pattern.matcher(sentence);
    
    boolean found = false;
    String toJSON = null;
    while (matcher.find()) {
      // System.out.println("I found the text: " + matcher.group().toString());
      toJSON = matcher.group().toString();
      logger.trace("Found JSON [" + toJSON + "] structure (i.e. inside {})");
      found = true;
    }
    if (!found) {
      logger.trace("Not Found JSON structure, there is no {} or nothing inside.");
      // System.out.println("I didn't found the text");
    }
    return toJSON;
  }
  
  /**
   * @return the site
   */
  public static String getSite() {
    return SITE;
  }
}
