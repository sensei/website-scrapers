/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class InstagramSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(InstagramSegmenter.class);
  
  static final Pattern pCapt = Pattern.compile("\"caption\":\"([^\"]*)\"");
  static final Pattern pUser = Pattern.compile("\"owner\":\\{\"username\":\"([^\"]*)\"");
  static final Pattern pDate = Pattern.compile("\"date\":([^,]*)");
  static final Pattern pLikes = Pattern.compile("\"likes\":\\{\"count\":(\\d+)");
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    SimpleItem r = new SimpleItem();
    r.title = "Instagram";
    r.postUrl = url.toExternalForm();
    r.position = 1;
    
    Matcher m;
    
    // MAIN:
    
    m = pCapt.matcher(content);
    if (m.find() && m.groupCount() == 1) {
      r.body = m.group(1);
    } else {
      return null;
    }
    
    m = pUser.matcher(content);
    if (m.find() && m.groupCount() == 1) {
      r.author = m.group(1);
    }
    
    try {
      m = pDate.matcher(content);
      if (m.find() && m.groupCount() == 1) {
        String sdate = m.group(1);
        Double ddate = Double.parseDouble(sdate);
        
        Date date = new Date(1000 * ddate.longValue());
        r.date = date;
        r.timeIsReal = true;
      }
    } catch (Exception e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
      r.date = null;
    }
    
    m = pLikes.matcher(content);
    if (m.find() && m.groupCount() == 1) {
      r.votes = Integer.parseInt(m.group(1));
    }
    
    ArrayList<SimpleItem> ret = new ArrayList<SimpleItem>();
    
    ret.add(r);
    return ret;
  }
}
