/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.apache.tools.ant.util.Base64Converter;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;

public class LaVanguardiaSegmenter extends BaseHTMLSegmenter {

  private static final Logger logger = Logger.getLogger(LaVanguardiaSegmenter.class);

  private final static Pattern encodingPattern = Pattern.compile("\\|([^\\\"\\\\]*(?:\\\\.[^\\\"\\\\]*)*)\\|");
  private final static Pattern datePattern = Pattern.compile("(\\d{2}/\\d{2}/\\d{4}\\s+-\\s+\\d{2}:\\d{2}h)");

  private static SimpleDateFormat sdf = null;

  {
    sdf = new SimpleDateFormat("dd/MM/yyyy - hh:mm");
    sdf.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
  }

  @Override
  public String findMainDescripcion(TagNode rootNode) {
    String body = new String();
    TagNode paragraphsNode = rootNode.findElementByAttValue("class", "ctx_content", true, false);
    if (paragraphsNode == null) {
      logger.debug("Not found content inside \"ctx_content\" tag. Trying \"text\" tag.");
      paragraphsNode = rootNode.findElementByAttValue("class", "text", true, false);
    }

    if (paragraphsNode != null) {
      TagNode[] paragraphs = paragraphsNode.getElementsByName("p", false);
      if (paragraphs != null && paragraphs.length > 0) {
        for (TagNode tagNode : paragraphs) {
          body = body.concat(tagNode.getText().toString());
        }
      }
    } else {
      logger
      .warn("Tag \"ctx_content\" nor tag \"text\" found. Maybe the parser needs an update because the news format has changed. The body will be returned empty.");
    }
    return body;
  }

  @Override
  public String findMainAuthor(TagNode rootNode) {
    return null;
  }

  @Override
  public Date findMainDate(TagNode rootNode) {
    Date date = null;
    // method 1
    TagNode dateNode = rootNode.findElementByAttValue("class", "detalle noticia", true, false);
    if (dateNode != null) {
      String sdate = dateNode.getAllElements(true)[2].getText().toString();
      Matcher matcher = encodingPattern.matcher(sdate);
      if (matcher.find()) {
        String mdate = matcher.group(1);
        date = getDateFromString(date, mdate);
      }
    }
    // method 2
    if (date == null) {
      if (dateNode != null) {
        String sdate = dateNode.getAllElements(true)[1].getText().toString();
        Matcher matcher = datePattern.matcher(sdate);
        if (matcher.find()) {
          String mdate = matcher.group(1);
          date = getDateFromString(date, mdate);
        }
      }
    }

    if (date == null) {
      logger.warn("Could not parse date for " + this.getClass().getName());
    }
    return date;
  }

  private Date getDateFromString(Date date, String mdate) {
    try {
      String fdate = removeWhiteSpaces(mdate.substring(0, mdate.indexOf("h")));
      // 29/06/2014-13:22
      date = sdf.parse(fdate);
      timeisReal = true;
    } catch (ParseException e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage());
    }
    return date;
  }

  @Override
  public String findMainThreadId(TagNode rootNode) {
    return getNodeText(rootNode).replaceFirst(".*?\"articleId\": \"(\\d+)\".*", "$1");
  }

  @Override
  public List<String> findMainThreadURLs(TagNode rootNode) {
    List<String> urls = new ArrayList<String>();
    String commentBase = "http://bootstrap.grupogodo1.fyre.co/bs3/v3.1/grupogodo1.fyre.co/%s/%s/0.json";
    TagNode idsNode = rootNode.findElementByAttValue("class", "comments", true, false);
    if (idsNode != null) {
      TagNode[] parentIds = idsNode.getAllElements(true);
      if (parentIds != null && parentIds.length > 0) {
        TagNode[] childIds = parentIds[1].getAllElements(true);
        if (childIds != null && childIds.length > 0) {
          String articleID = childIds[0].getAttributeByName("data-lf-article-id");
          String siteID = childIds[0].getAttributeByName("data-lf-site-id");
          Base64Converter converter = new Base64Converter();
          String commentsUrl = String.format(commentBase, siteID, converter.encode(articleID));
          urls.add(commentsUrl);
        }
      }
    }
    return urls;
  }

  @Override
  public String findMainBody(TagNode rootNode) {
    String body = null;
    TagNode bodyNode = rootNode.findElementByAttValue("class", "ctx_content", true, false);
    if (bodyNode == null) {
      logger.debug("Not found content inside \"ctx_content\" tag. Trying \"text\" tag.");
      bodyNode = rootNode.findElementByAttValue("class", "text", true, false);
    }

    if (bodyNode != null) {
      body = bodyNode.getText().toString();
    }
    return body;
  }

  @Override
  public Boolean findMainTimeIsReal(TagNode rootNode) {
    findMainDate(rootNode);
    return timeisReal;
  }
}
