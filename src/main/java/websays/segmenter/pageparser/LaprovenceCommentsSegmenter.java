/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class LaprovenceCommentsSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(LaprovenceCommentsSegmenter.class);
  
  public static SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy hh:mm");
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    HtmlCleaner cleaner = createCleaner();
    
    int position = 1;
    TagNode root = null;
    String site = null;
    if (url != null) {
      site = url.toString();
    }
    root = cleaner.clean(content);
    
    TagNode[] commentsTagNodes = root.getElementsByAttValue("class", "comment-published", true, false);
    
    for (TagNode commentTagNode : commentsTagNodes) {
      addSimpleItem(items, commentTagNode, site, position++);
    }
    return items;
  }
  
  private void addSimpleItem(List<SimpleItem> items, TagNode tagNode, String site, int position) {
    SimpleItem simpleItem = new SimpleItem();
    simpleItem.position = position;
    simpleItem.postUrl = site;
    simpleItem.body = getNodeText(tagNode.findElementByAttValue("class", "commentaire", true, false));
    simpleItem.imageLink = tagNode.findElementByName("img", true).getAttributeByName("src");
    simpleItem.author = getNodeText(tagNode.findElementByName("a", true));
    simpleItem.isComment = true;
    try {
      String dateStr = findNodeTextByClass(tagNode, "comment_date");
      simpleItem.date = sd.parse(dateStr);
      simpleItem.timeIsReal = true;
    } catch (ParseException e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
      simpleItem.date = null;
    }
    items.add(simpleItem);
  }
}
