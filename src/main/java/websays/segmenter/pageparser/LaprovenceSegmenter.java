/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;

public class LaprovenceSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(LaprovenceSegmenter.class);
  
  @Override
  public Date findMainDate(TagNode rootNode) {
    SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy à HH'H'mm", Locale.FRANCE);
    Date date = null;
    try {
      String dateTime = findNodeTextByItemprop(rootNode, "datePublished").replaceFirst(".*?([\\d/]+ à [\\dH]+)", "$1");
      date = sd.parse(dateTime);
      timeisReal = true;
    } catch (ParseException e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
      return null;
    }
    return date;
  }
  
  @Override
  public String findMainAuthor(TagNode rootNode) {
    return findNodeTextByItemprop(rootNode, "author");
  }
  
  @Override
  public Integer findMainComments(TagNode rootNode) {
    String reactions = findNodeTextByClass(rootNode, "reactions").replaceFirst(" réactions", "");
    return Integer.parseInt(reactions);
  }
  
  @Override
  public String findMainThreadId(TagNode rootNode) {
    return rootNode.getElementsByName("form", true)[3].getAttributeByName("action");
  }
  
  @Override
  public List<String> findMainThreadURLs(TagNode rootNode) {
    TagNode reactionsNode = rootNode.findElementByAttValue("class", "reactions", true, false);
    String commentsLink = reactionsNode.findElementByName("a", true).getAttributeByName("href");
    ArrayList<String> threadURLs = new ArrayList<String>();
    threadURLs.add(commentsLink);
    return threadURLs;
  }
  
  @Override
  public String findMainDescripcion(TagNode rootNode) {
    TagNode articleNode = rootNode.findElementByAttValue("itemprop", "articleBody", true, false);
    return getNodeText(articleNode.getChildTags()[2]);
  }
  
  @Override
  public Boolean findMainTimeIsReal(TagNode rootNode) {
    findMainDate(rootNode);
    return timeisReal;
  }
  
}
