/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;

public class LefigaroSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(LefigaroSegmenter.class);
  
  @Override
  public Date findMainDate(TagNode rootNode) {
    Date date = null;
    try {
      String dateTime = rootNode.findElementByAttValue("itemprop", "datePublished", true, false).getAttributeByName("datetime");
      date = DatatypeConverter.parseDateTime(dateTime).getTime();
      timeisReal = true;
    } catch (Exception e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
      return null;
    }
    return date;
    
  }
  
  @Override
  public String findMainAuthor(TagNode rootNode) {
    return findNodeTextByItemprop(rootNode, "name");
  }
  
  @Override
  public Boolean findMainTimeIsReal(TagNode rootNode) {
    findMainDate(rootNode);
    return timeisReal;
  }
  
  @Override
  public Integer findMainComments(TagNode rootNode) {
    String comments = findNodeTextByClass(rootNode, "fig-hl");
    return Integer.parseInt(comments);
  }
  
  @Override
  public TagNode[] findComments(TagNode rootNode) {
    TagNode commentsParent = rootNode.findElementByAttValue("class", "fig-comments", true, false);
    return commentsParent.getChildTags();
  }
  
  @Override
  public String findCommentAuthor(TagNode commentNode) {
    return findNodeTextByItemprop(commentNode, "creator");
  }
  
  @Override
  public String findCommentImageLink(TagNode commentNode) {
    return commentNode.findElementByName("img", true).getAttributeByName("src");
  }
  
  @Override
  public Date findCommentDate(TagNode commentNode) {
    SimpleDateFormat sd = new SimpleDateFormat("'Le' dd/MM/yyyy 'à' HH:mm", Locale.FRANCE);
    Date date = null;
    try {
      String dateStr = findNodeTextByItemprop(commentNode, "commentTime");
      date = sd.parse(dateStr);
      timeisReal = true;
    } catch (ParseException e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
      return null;
    }
    return date;
  }
  
  @Override
  public String findCommentDescripcion(TagNode commentNode) {
    return findNodeTextByItemprop(commentNode, "commentText");
  }
  
  @Override
  public Boolean findCommentTimeIsReal(TagNode commentNode) {
    findCommentDate(commentNode);
    return timeisReal;
  }
  
}
