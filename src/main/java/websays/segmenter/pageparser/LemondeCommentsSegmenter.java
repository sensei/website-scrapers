/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class LemondeCommentsSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(LemondeCommentsSegmenter.class);
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    TagNode rootNode = getRootNode(content);
    TagNode[] comments = findComments(rootNode);
    boolean isFirst = true;
    if (comments != null && comments.length > 0) {
      for (TagNode commentNode : comments) {
        if (isFirst) {
          addSimpleItem(createCommentItem(commentNode, url.toString(), findMainThreadId(rootNode), findMainThreadURLs(rootNode)));
          isFirst = false;
        } else {
          addSimpleItem(createCommentItem(commentNode, url.toString(), findMainThreadId(rootNode), null));
        }
        
      }
    }
    return getItems();
  }
  
  @Override
  public String findMainTitle(TagNode rootNode) {
    return null;
  }
  
  @Override
  public String findMainDescripcion(TagNode rootNode) {
    return null;
  }
  
  @Override
  public TagNode[] findComments(TagNode rootNode) {
    return rootNode.getElementsByAttValue("itemprop", "commentText", true, false);
  }
  
  @Override
  public String findCommentAPIObjectID(TagNode rootNode) {
    return rootNode.getAttributeByName("data-reaction_id");
  }
  
  @Override
  public String findCommentParentID(TagNode rootNode) {
    String parentID = null;
    if (rootNode.getAttributeByName("class").equals("reaction clearfix reponse")) {
      TagNode[] allComments = rootNode.getParent().getAllElements(false);
      String auxParentID = null;
      for (TagNode tagNode : allComments) {
        if (rootNode.getAttributeByName("data-reaction_id").equals(tagNode.getAttributeByName("data-reaction_id"))) {
          parentID = auxParentID;
          break;
        } else {
          if (!tagNode.getAttributeByName("class").equals("reaction clearfix reponse")) {
            auxParentID = tagNode.getAttributeByName("data-reaction_id");
          }
        }
      }
    }
    return parentID;
  }
  
  @Override
  public String findMainImageLink(TagNode rootNode) {
    // TODO Auto-generated method stub
    return super.findMainImageLink(rootNode);
  }
  
  @Override
  public String findMainThreadId(TagNode rootNode) {
    String url = getMetaContent(rootNode, "og:url");
    return url.replace("article", "reactions");
  }
  
  @Override
  public String findCommentImageLink(TagNode commentNode) {
    String imageLink = null;
    TagNode imageNode = commentNode.findElementByAttValue("class", "lazy-retina", true, false);
    if (imageNode != null) {
      imageLink = imageNode.getAttributeByName("src");
    }
    return imageLink;
  }
  
  @Override
  public Date findCommentDate(TagNode commentNode) {
    Date date = null;
    try {
      TagNode dateNode = commentNode.findElementByAttValue("class", "date", true, false);
      if (dateNode != null) {
        date = DatatypeConverter.parseDateTime(dateNode.getAttributeByName("data-date-iso")).getTime();
        timeisReal = true;
      }
    } catch (Exception e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
      return null;
    }
    return date;
  }
  
  @Override
  public Boolean findCommentTimeIsReal(TagNode commentNode) {
    findCommentDate(commentNode);
    return timeisReal;
  }
  
  @Override
  public String findCommentAuthor(TagNode commentNode) {
    String author = null;
    TagNode references = commentNode.findElementByAttValue("class", "references", true, false);
    if (references != null) {
      author = getNodeText(references).replaceFirst("\\s[\\s+\\-\\dh\\/]+$", "");
    }
    return author;
  }
  
  @Override
  public String findCommentDescripcion(TagNode commentNode) {
    String description = null;
    TagNode grid = commentNode.findElementByAttValue("class", "grid_9 omega", true, false);
    if (grid != null) {
      description = grid.getChildTags()[1] != null ? getNodeText(grid.getChildTags()[1]) : null;
    }
    return description;
  }
  
  @Override
  public List<String> findMainThreadURLs(TagNode rootNode) {
    int page = 0;
    String current_page = null;
    String next_page = null;
    ArrayList<String> urls = new ArrayList<String>();
    TagNode pagination = rootNode.findElementByAttValue("class", "pagination", true, false);
    if (pagination != null) {
      TagNode[] pages = pagination.getAllElements(false);
      for (TagNode tagNode : pages) {
        if (tagNode.findElementByAttValue("class", "page actif", false, false) != null) {
          current_page = tagNode.findElementByAttValue("class", "page actif", false, false).getText().toString();
        }
      }
      
      if (current_page != null) {
        page = Integer.parseInt(current_page);
      }
      if (page != 0 && pages.length > 0 && page < pages.length) {
        page++;
        for (TagNode tagNode : pages) {
          if (tagNode.findElementByAttValue("class", "page", false, false) != null) {
            if (Integer.parseInt(tagNode.findElementByAttValue("class", "page", false, false).getText().toString()) == page) {
              next_page = tagNode.findElementByAttValue("class", "page", false, false).getAttributeByName("href");
            }
          }
        }
        if (next_page != null) {
          next_page = "http://www.lemonde.fr" + next_page;
          urls.add(next_page);
        }
      }
    }
    return urls;
  }
}
