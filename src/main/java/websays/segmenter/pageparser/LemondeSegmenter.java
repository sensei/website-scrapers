/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class LemondeSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(LemondeSegmenter.class);
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    HtmlCleaner cleaner = createCleaner();
    
    TagNode root = null;
    String site = null;
    if (url != null) {
      site = url.toString();
    }
    
    root = cleaner.clean(content);
    
    // /////////// Main Details Item ////////////
    SimpleItem main = new SimpleItem();
    // IMPORTANT: This conditional IS VERY IMPORTANT due a concurrency problem because if a URL
    // comes with this word all the times is added to the threads URL (comments variable), so don't delete it,
    // or the system goes down
    if (!site.contains("reactions")) {
      // ///// Extract Title
      main.author = this.findMainAuthor(root);
      main.title = getMetaContent(root, "og:title");
      main.body = findNodeTextById(root, "articleBody");
      main.date = this.findMainDate(root);
      main.position = 1;
      main.postUrl = site;
      main.threadId = this.findMainThreadId(root);
      main.APIObjectID = findMainAPIObjectID(root);
      main.imageLink = findMainImageLink(root);
      main.threadURLs = new ArrayList<String>();
      String comments = findMainThreadId(root);
      if (comments != null) {
        main.threadURLs.add(comments);
      }
      getItems().add(main);
      
    } else {
      main.fakeItem = true;
      main.postUrl = site;
      main.position = 1;
      getItems().add(main);
      logger.debug("Fake Clipping with URL due to no comments and avoid bucle issue discovered " + site);
    }
    return getItems();
  }
  
  @Override
  public Date findMainDate(TagNode commentNode) {
    Date date = null;
    try {
      TagNode dataNode = commentNode.findElementByAttValue("itemprop", "datePublished", true, false);
      if (dataNode != null) {
        date = DatatypeConverter.parseDateTime(dataNode.getAttributeByName("datetime")).getTime();
        timeisReal = true;
      }
    } catch (Exception e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
      return null;
    }
    return date;
  }
  
  @Override
  public Boolean findMainTimeIsReal(TagNode rootNode) {
    findMainDate(rootNode);
    return timeisReal;
  }
  
  @Override
  public String findMainAuthor(TagNode rootNode) {
    return findNodeTextByClass(rootNode, "auteur");
  }
  
  @Override
  public String findMainThreadId(TagNode rootNode) {
    String urlComments = null;
    String url = getMetaContent(rootNode, "og:url");
    // IMPORTANT: We've just added comments if the URL of the article contains the word 'article',
    // otherwise we can't, with a couple of test made with the asynchronous process, we've identified
    // that some of the URLs does not contain this word maybe should exist another way to get its comments.
    // Related with the concurrency problem
    if (url.contains("article")) {
      urlComments = url.replace("article", "reactions");
    }
    return urlComments;
    
  }
  
  @Override
  protected String findNodeTextById(TagNode root, String id) {
    String body = new String();
    TagNode firstParagraph = root.findElementByAttValue("class", "taille_courante", true, false);
    if (firstParagraph != null) {
      body = body.concat(firstParagraph.getText().toString());
    }
    TagNode bodyTag = root.findElementByAttValue("id", id, true, false);
    if (bodyTag != null) {
      TagNode[] paragraphs = bodyTag.getElementsByName("p", false);
      if (paragraphs != null && paragraphs.length > 0) {
        for (TagNode tagNode : paragraphs) {
          body = body.concat(tagNode.getText().toString() + " ");
        }
      }
    }
    return body;
  }
}
