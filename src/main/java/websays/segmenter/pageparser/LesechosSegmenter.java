/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;

public class LesechosSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(LesechosSegmenter.class);
  
  @Override
  public String findMainAuthor(TagNode rootNode) {
    return getMetaItemPropContent(rootNode, "creator");
  }
  
  @Override
  public Date findMainDate(TagNode rootNode) {
    SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd", Locale.FRANCE);
    Date date = null;
    try {
      String dateStr = getMetaItemPropContent(rootNode, "dateCreated");
      date = sd.parse(dateStr);
      timeisReal = true;
    } catch (ParseException e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
      return null;
    }
    return date;
  }
  
  @Override
  public Boolean findMainTimeIsReal(TagNode rootNode) {
    findMainDate(rootNode);
    return timeisReal;
  }
  
  protected String getMetaItemPropContent(TagNode root, String propertyName) {
    assert (root != null) : "Must have a root element";
    assert (propertyName != null) : "The propertyName can't be null";
    TagNode heading = root.findElementByAttValue("itemprop", propertyName, true, false);
    if (heading != null) {
      String content = StringEscapeUtils.unescapeXml(heading.getAttributeByName("content"));
      return removeWhiteSpaces(content);
    } else {
      return null;
    }
  }
  
  @Override
  public TagNode[] findComments(TagNode rootNode) {
    return rootNode.getElementsByAttValue("class", "item", true, false);
  }
  
  @Override
  public String findCommentAuthor(TagNode commentNode) {
    return findNodeTextByClass(commentNode, "auteur");
  }
  
  @Override
  public String findCommentDescripcion(TagNode commentNode) {
    return getNodeText(commentNode.findElementByName("a", true));
  }
  
  @Override
  public Date findCommentDate(TagNode commentNode) {
    SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy à HH:mm");
    Date date = null;
    try {
      String p = findNodeTextByClass(commentNode, "meta");
      String dateStr = p.replaceFirst(".*?([\\d/]+ à [\\d:]+)", "$1");
      date = sd.parse(dateStr);
      timeisReal = true;
    } catch (ParseException e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
      return null;
    }
    return date;
  }
  
  @Override
  public Boolean findCommentTimeIsReal(TagNode commentNode) {
    findCommentDate(commentNode);
    return timeisReal;
  }
}
