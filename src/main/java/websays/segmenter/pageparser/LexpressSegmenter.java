/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;

public class LexpressSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(LexpressSegmenter.class);
  
  public Date findMainDate(TagNode rootNode) {
    SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.FRANCE);
    Date date = null;
    try {
      TagNode time = rootNode.findElementByName("time", true);
      String dateTime = time.getAttributeByName("datetime");
      date = sd.parse(dateTime);
      timeisReal = true;
    } catch (ParseException e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
      return null;
    }
    return date;
  }
  
  @Override
  public Boolean findMainTimeIsReal(TagNode rootNode) {
    findMainDate(rootNode);
    return timeisReal;
  }
  
  @Override
  public TagNode[] findComments(TagNode rootNode) {
    return rootNode.getElementsByAttValue("class", "comment_item", true, false);
  }
  
  @Override
  public Integer findCommentLikes(TagNode commentNode) {
    String likes = commentNode.findElementByName("button", true).getAttributeByName("nb");
    return Integer.parseInt(likes);
  }
  
  @Override
  public String findCommentImageLink(TagNode commentNode) {
    return "http://www.lexpress.fr" + commentNode.findElementByName("img", true).getAttributeByName("src");
  }
  
  @Override
  public Date findCommentDate(TagNode commentNode) {
    SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    Date date = null;
    try {
      String p = commentNode.findElementByName("p", true).getText().toString();
      String dateStr = p.replaceFirst(".*?([\\d/]+ [\\d:]+)", "$1");
      date = sd.parse(dateStr);
      timeisReal = true;
    } catch (ParseException e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
      return null;
    }
    return date;
  }
  
  @Override
  public Boolean findCommentTimeIsReal(TagNode commentNode) {
    findMainDate(commentNode);
    return timeisReal;
  }
  
  @Override
  public String findCommentAuthor(TagNode commentNode) {
    return findNodeTextByClass(commentNode, "pseudo tip_account");
  }
  
  @Override
  public String findCommentDescripcion(TagNode commentNode) {
    return findNodeTextByItemprop(commentNode, "commentText");
  }
  
}
