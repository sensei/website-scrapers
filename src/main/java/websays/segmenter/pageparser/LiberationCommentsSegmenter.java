/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.json.JSONArray;
import org.json.JSONObject;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class LiberationCommentsSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(LiberationCommentsSegmenter.class);
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    HtmlCleaner cleaner = createCleaner();
    
    int position = 1;
    TagNode root = null;
    String site = null;
    if (url != null) {
      site = url.toString();
    }
    
    root = cleaner.clean(content);
    JSONObject jsonRoot = new JSONObject(root.getText().toString());
    JSONObject headDocument = jsonRoot.getJSONObject("headDocument");
    if (headDocument != null) {
      JSONArray posts = headDocument.getJSONArray("content");
      for (int i = 0; i < posts.length(); i++) {
        JSONObject headerPost = posts.getJSONObject(i);
        SimpleItem item = new SimpleItem();
        JSONObject post = headerPost.getJSONObject("content");
        item.threadId = post.getString("id");
        item.body = post.has("bodyHtml") ? post.getString("bodyHtml") : null;
        String authorId = post.has("authorId") ? post.getString("authorId") : null;
        findExtraInfoAboutAuthor(authorId, headDocument, item);
        JSONObject like = post.has("annotations") ? post.getJSONObject("annotations") : null;
        if (like != null) {
          if (like.has("likedBy")) {
            item.likes = like.getJSONArray("likedBy").length();
          }
        }
        try {
          Timestamp stamp = new Timestamp(post.getLong("createdAt") * 1000);
          item.date = new Date(stamp.getTime());
          item.timeIsReal = true;
        } catch (Exception e) {
          logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
          item.date = null;
        }
        item.postUrl = site;
        item.position = position++;
        item.isComment = true;
        items.add(item);
      }
    }
    return items;
  }
  
  private void findExtraInfoAboutAuthor(String authorId, JSONObject main, SimpleItem item) {
    if (authorId != null) {
      JSONObject author = main.getJSONObject("authors").getJSONObject(authorId);
      item.author = author.getString("displayName");
      item.imageLink = author.getString("avatar");
    }
  }
}
