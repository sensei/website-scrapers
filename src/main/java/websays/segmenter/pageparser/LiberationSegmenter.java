/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.apache.tools.ant.util.Base64Converter;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;

public class LiberationSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(LiberationSegmenter.class);
  
  // \"articleId\": "([^\"\\]*(?:\\.[^\"\\]*)*)"
  private final static Pattern encodingPattern = Pattern.compile("\\\"articleId\\\": \"([^\\\"\\\\]*(?:\\\\.[^\\\"\\\\]*)*)\"");
  
  @Override
  public String findMainDescripcion(TagNode rootNode) {
    // TagNode heading = rootNode.findElementByAttValue("name", "description", true, false);
    // if (heading != null) {
    // String content = StringEscapeUtils.unescapeXml(heading.getAttributeByName("content"));
    // return removeWhiteSpaces(content);
    // }
    String body = new String();
    body = findNodeTextById(rootNode, "article-body mod");
    return body;
    
  }
  
  @Override
  protected String findNodeTextById(TagNode root, String id) {
    String body = new String();
    TagNode bodyNode = root.findElementByAttValue("class", id, true, false);
    if (bodyNode != null) {
      TagNode bodyNodeTag = bodyNode.findElementByName("div", false);
      if (bodyNodeTag != null) {
        TagNode[] paragraphs = bodyNodeTag.getElementsByName("p", false);
        if (paragraphs != null && paragraphs.length > 0) {
          for (TagNode tagNode : paragraphs) {
            body = body.concat(tagNode.getText().toString() + " ");
          }
        }
      }
    }
    return body;
  }
  
  @Override
  public String findMainAuthor(TagNode rootNode) {
    return findNodeTextByClass(rootNode, "authorname");
  }
  
  @Override
  public Date findMainDate(TagNode rootNode) {
    Date date = null;
    try {
      TagNode dateNode = rootNode.findElementByAttValue("itemprop", "datePublished", true, false);
      date = DatatypeConverter.parseDateTime(dateNode.getAttributeByName("datetime")).getTime();
      timeisReal = true;
    } catch (Exception e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
      return null;
    }
    return date;
  }
  
  @Override
  public Boolean findMainTimeIsReal(TagNode rootNode) {
    findMainDate(rootNode);
    return timeisReal;
  }
  
  @Override
  public String findMainThreadId(TagNode rootNode) {
    return getNodeText(rootNode).replaceFirst(".*?\"articleId\": \"(\\d+)\".*", "$1");
  }
  
  @Override
  public List<String> findMainThreadURLs(TagNode rootNode) {
    List<String> urls = new ArrayList<String>();
    String commentBase = "http://bootstrap.liberation.fyre.co/bs3/v3.1/liberation.fyre.co/336643/%s/init";
    String articleid = null;
    Matcher matcher = encodingPattern.matcher(rootNode.getText());
    if (matcher.find()) {
      articleid = matcher.group(1);
    }
    if (articleid != null) {
      Base64Converter converter = new Base64Converter();
      String commentsUrl = String.format(commentBase, converter.encode(articleid));
      urls.add(commentsUrl);
    }
    return urls;
  }
  
}
