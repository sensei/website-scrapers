/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2016. All rights reserved. http://websays.com )
 *
 * Primary Author: juanfra
 * Contributors:
 * Date: May 2, 2016
 */
package websays.segmenter.pageparser;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

/**
 * @author juanfra
 *
 *         Scraps linkedIn public content.
 */
public class LinkedInSegmenter extends BaseHTMLSegmenter {

  private static final Logger logger = Logger.getLogger(LinkedInSegmenter.class);

  /**
   *
   */
  private static final String PAGING_URL_CLASS = "paging-url";

  /**
   *
   */
  private static final String PAGING_ATTRIBUTE = "data-li-paging-url";

  /**
   * Class of the update nodes in the scraped HTML from linkedIn.
   */
  private static final String FEED_ITEM_CLASS = "feed-item";

  /**
   * HTML attribute of the HTML class feed-item where the unique identifier for an update (linkedIn post) is stored.
   */
  private static final String UPDATE_ID_LABEL = "data-li-update-id";

  /**
   * HTML attribute of the HTML class paging-url where the API ID for the corporate user is stored (inside a URL string).
   */
  private static final String API_ID_LABEL = "data-li-paging-url";

  private static final String LINKEDIN_BASE_URL = "https://www.linkedin.com";

  private static final String COMPANY_PAGE_PREFIX = "/company/";

  private static final String UPDATES_PAGE_PREFIX = "/biz/";

  private static final String UPDATES_PAGE_SUFIX = "/feed?start=0";

  /* Used to get the numerical id of the processed company */
  private static final Pattern companyIDPattern = Pattern.compile("\"companyId\":(\\d+)");

  /* Used tu get the url of the source of the update */
  private static final Pattern postUrlPattern = Pattern.compile(".*url=([^&]*)&");

  /*
   * (non-Javadoc)
   *
   * @see websays.segmenter.base.HTMLDocumentSegmenter#segment(java.lang.String, java.net.URL)
   */
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {

    List<SimpleItem> results = null;

    if (url != null && url.toExternalForm() != null) {

      if (url.toExternalForm().startsWith(LINKEDIN_BASE_URL + COMPANY_PAGE_PREFIX)) {
        /* Build just a fake clipping with the URL of the first updates page to be queued in the asynchronous. */
        results = segmentMainPage(content, url);

      } else if (url.toExternalForm().startsWith(LINKEDIN_BASE_URL + UPDATES_PAGE_PREFIX)) {
        /* Return the scraped content and the URL of the next updates page */
        results = segmentUpdatesPage(content, url);
      } else {
        logger.warn("Unrecognized type of linkedIn page [" + url.toExternalForm() + "]");
      }
    } else {
      logger.warn("The URL object received is null or has not enough info to decide what to do. Nothing can be done.");
    }

    return results;
  }

  /**
   * Parses a linkedin updates page and gets all the information from it. It also gets the URL to the next updates page.
   *
   * @param content
   *          the content to be scraped
   * @param url
   *          the url of the page from which the content was downloaded.
   * @return a list of simpleitems with the information stored in the content. Null if there is no info to process.
   */
  private List<SimpleItem> segmentUpdatesPage(String content, URL url) {
    List<SimpleItem> res = null;
    content = segmentInit(content, url);
    TagNode rootNode = getRootNode(content);
    TagNode[] updateNodes = rootNode.getElementsByAttValue("class", LinkedInSegmenter.FEED_ITEM_CLASS, true, false);
    TagNode[] paging = rootNode.getElementsByAttValue("class", LinkedInSegmenter.PAGING_URL_CLASS, true, false);

    if (updateNodes != null && updateNodes.length > 0) {

      String ApiId = null;
      if (paging != null && paging.length >= 1) {
        /* Getting the API identifier for the linkedin user/company being processed */
        ApiId = getApiIdFromNode(paging[0]);
      }

      res = new ArrayList<SimpleItem>();

      String threadURL = url.toExternalForm();
      for (TagNode currNode : updateNodes) {
        // String updateThreadId = threadURL + "#" + updatePos;
        String updateThreadId = currNode.getAttributeByName(UPDATE_ID_LABEL);
        res = processUpdate(currNode, updateThreadId, res, ApiId);
      }

      if (paging != null && paging.length >= 1) {
        if (paging.length > 1) {
          logger.warn("There are [" + paging.length + "] nodes of class \"" + LinkedInSegmenter.PAGING_URL_CLASS
              + "\". The first one will be used.");
        }

        String pagingPath = paging[0].getAttributeByName(LinkedInSegmenter.PAGING_ATTRIBUTE);
        String nextUpdatesPage = LINKEDIN_BASE_URL + pagingPath;
        if (res != null && res.size() > 0) {
          /* The next updates page url is added to the last SimpleItem in the list. */
          res.get(res.size() - 1).threadURLs = new ArrayList<String>();
          res.get(res.size() - 1).threadURLs.add(nextUpdatesPage);

        } else {
          logger
          .warn("Very strange condition (a bug?). There are no updates (posts) but there is paging next url. Adding it for further processing.");
          res = new ArrayList<SimpleItem>();
          SimpleItem fakePaging = new SimpleItem();
          fakePaging.fakeItem = true;
          fakePaging.threadURLs = new ArrayList<String>();
          fakePaging.threadURLs.add(nextUpdatesPage);
          res.add(fakePaging);

        }

      } else {
        if (logger.isTraceEnabled()) {
          logger.trace("There are no more linkedIn updates beyond [" + url.toExternalForm() + "]");
        }
      }

    } else {
      logger.warn("The page [" + url.toExternalForm() + "] has no class=[" + LinkedInSegmenter.FEED_ITEM_CLASS + "]. Returning null.");
    }

    return res;
  }

  /**
   * Given a node of the paging-url class, gets the user/company API Id for the page the node belongs to.
   *
   * @param tagNode
   *          which contains the API Id
   * @return the API id in textual form.
   */
  private String getApiIdFromNode(TagNode tagNode) {
    String rawURL = tagNode.getAttributeByName(API_ID_LABEL);
    String res = null;
    if (rawURL != null) {
      String[] comps = rawURL.split("/");
      if (comps != null && comps.length > 2) {
        res = comps[2];
      }
    }

    return res;
  }

  /**
   * @param currNode
   * @param updateThreadId
   * @param parOldList
   * @return
   */
  private List<SimpleItem> processUpdate(TagNode currNode, String updateThreadId, List<SimpleItem> parOldList, String parCorporateUser) {

    SimpleItem currUpdate = new SimpleItem();
    List<SimpleItem> res = parOldList;

    /* Position and commentness */
    currUpdate.position = 1;
    currUpdate.isComment = false;

    /* Date */
    currUpdate.date = this.findMainDate(currNode);
    currUpdate.timeIsReal = false;
    if (currUpdate.date != null) {
      currUpdate.timeIsReal = true;
    } else {
      logger.warn("Date for update [" + currUpdate.title + "] not found. Returning null.");
    }

    /* Title */
    currUpdate.title = this.findMainTitle(currNode);

    /* Body */
    currUpdate.body = this.findMainBody(currNode);

    /* Maybe there is an alternative page structure. Let's try to get it. */
    if (currUpdate.title == null && currUpdate.body == null) {
      currUpdate.title = this.findMainTitleMethod2(currNode);
      currUpdate.body = this.findMainBodyMethod2(currNode);
    }

    if (currUpdate.body == null) {
      currUpdate.body = "";

    }
    /* Added the link to the page the post was originated from */
    currUpdate.body = currUpdate.body + " " + this.findReadMoreURL(currNode);

    /* Image if there is one */
    currUpdate.imageLink = this.findMainImageLink(currNode);

    /* Author */
    currUpdate.author = this.findMainAuthor(currNode);

    /* URL */
    currUpdate.itemUrl = this.findItemURL(currNode);
    currUpdate.postUrl = currUpdate.itemUrl;

    /* # of Likes */
    currUpdate.likes = this.findMainLikes(currNode);

    /* # of Comments */
    currUpdate.comments = this.findMainComments(currNode);

    /* Thread id, used to get a compositeID and to add possible comments later */
    currUpdate.threadId = updateThreadId;

    /* Adding the update parent to the results list */
    res.add(currUpdate);

    /* Comments */
    /* TODO: look for a way to get more comments than the two that are served with each update. */
    List<SimpleItem> updateComments = this.findCommentsForUpdate(currNode);
    for (SimpleItem currComm : updateComments) {
      currComm.itemUrl = currUpdate.itemUrl;
      currComm.threadId = currUpdate.threadId;
      // To ensure corporate response works.
      currComm.APIToAuthorID = parCorporateUser;
    }
    res.addAll(updateComments);

    return res;
  }

  /**
   * Returns the body of the update using an alternative method.
   *
   * @param currNode
   *          the node where the update is stored
   * @return the update's body or null if not able to found it.
   */
  private String findMainBodyMethod2(TagNode currNode) {
    TagNode[] bodyNode = currNode.getElementsByAttValue("class", "commentary js-truncate-commentary", true, false);
    if (bodyNode == null || bodyNode.length < 1) {
      logger.warn("No node with class=\"commentary js-truncate-commentary\" found. Returning null for the body.");
      return null;
    }
    return bodyNode[0].getText().toString();
  }

  /**
   * Looks for the title of the update in the linkedin clippings that are of an alternative form.
   *
   * @param currNode
   *          the update node
   * @return the update title or null if it is not possible to find it.
   */
  private String findMainTitleMethod2(TagNode currNode) {
    TagNode[] bodyNode = currNode.getElementsByAttValue("class", "commentary js-truncate-commentary", true, false);
    if (bodyNode == null || bodyNode.length < 1) {
      logger.warn("No node with class=\"commentary js-truncate-commentary\" found. Returning null for the title.");
      return null;
    }

    String[] comps = bodyNode[0].getText().toString().split("\n");
    if (comps == null || comps.length < 2) {
      logger.warn("There is no carriage return to make the title apart from the body. Trying with a full stop.");
      comps = bodyNode[0].getText().toString().split("\\.");
      if (comps == null || comps.length < 2) {
        logger.warn("Title not found even with method2. Title will be null.");
        return null;
      }
    }

    return comps[0];
  }

  @Override
  public String findMainImageLink(TagNode rootNode) {
    String className = "image ";
    TagNode[] imgNode = rootNode.getElementsByAttValue("class", className, true, false);
    if (imgNode == null || imgNode.length < 1) {
      logger.warn("No \"" + className + "\" html tag found. Trying looking for an alternative node \"image feed-external-url\"");
      // alternative method
      className = "image feed-external-url";
      imgNode = rootNode.getElementsByAttValue("class", className, true, false);
      if (imgNode == null || imgNode.length < 1) {
        logger.warn("No \"" + className + "\" html tag found. Returning null for the imageLink.");
        return null;
      }
    }

    TagNode[] innerNode = imgNode[0].getElementsByName("img", true);
    if (innerNode == null || innerNode.length < 1) {
      logger.warn("No \"img\" html tag found inside \"" + className + "\" node. Returning null for the imageLink.");
      return null;
    }
    String rawURL = innerNode[0].getAttributeByName("src");

    if (rawURL == null || rawURL.length() < 1) {
      logger.warn("No \"src\" inside the \"img\" html tag found inside \"" + className + "\" node. Returning null for the imageLink.");
      return null;
    }
    return rawURL;
  }

  /**
   * Returns the itemURL for the SimpleItem associated to the update.
   *
   * @param currNode
   *          the node where the update information is stored
   * @return the update associated url in linkedin (in fact is the company's public feed page because there is no individual url for each update) or
   *         null if not found the information.
   */
  private String findItemURL(TagNode currNode) {
    TagNode[] strongNode = currNode.getElementsByName("strong", true);
    if (strongNode == null || strongNode.length < 1) {
      logger.warn("No \"strong\" html tag found. Returning null for the itemURL.");
      return null;
    }

    TagNode[] anchorNode = strongNode[0].getElementsByName("a", true);
    if (anchorNode == null || anchorNode.length < 1) {
      logger.warn("No \"a\" html tag found inside \"strong\" node. Returning null for the itemURL.");
      return null;
    }
    String rawURL = anchorNode[0].getAttributeByName("href");

    if (rawURL == null || rawURL.length() < 1) {
      logger.warn("No \"href\" inside the \"a\" html tag found inside \"strong\" node. Returning null for the itemURL.");
      return null;

    }

    /* Getting rid of the url parameters. Only the url itself is desired */
    String[] urlComps = rawURL.split("\\?");
    if (urlComps == null || urlComps.length < 1) {
      logger.warn("Error splitting the raw itemURL [" + rawURL + "] by the char (?). Returning null.");
      return null;
    }

    return urlComps[0];
  }

  @Override
  public String findMainAuthor(TagNode rootNode) {

    TagNode[] strongNode = rootNode.getElementsByName("strong", true);
    if (strongNode == null || strongNode.length < 1) {
      logger.warn("No \"strong\" html tag found. Returning null.");
      return null;
    }

    TagNode[] anchorNode = strongNode[0].getElementsByName("a", true);
    if (anchorNode == null || anchorNode.length < 1) {
      logger.warn("No \"a\" html tag found inside \"strong\" node. Returning null.");
      return null;
    }

    return anchorNode[0].getText().toString();

  }

  /**
   * Gets all the comments for the current update.
   *
   * @param currNode
   *          the node that contains the update
   * @return a list of SimpleItems that contains the comments.
   */
  private List<SimpleItem> findCommentsForUpdate(TagNode currNode) {
    List<SimpleItem> res = new ArrayList<SimpleItem>();
    // int pos = 2;

    List<TagNode> commentNodes = currNode.getElementListByAttValue("class", "comment-item", true, false);

    commentNodes.addAll(currNode.getElementListByAttValue("class", "comment-item first", true, false));

    for (TagNode currCommentNode : commentNodes) {
      SimpleItem currComment = new SimpleItem();
      currComment.author = this.findCommentAuthor(currCommentNode);
      currComment.body = this.findCommentBody(currCommentNode);
      currComment.date = this.findCommentDate(currCommentNode);
      // Date is not reliable for Hash because it is generated as N days ago, and we don't know when it changes (i.e. if the comment is read at 8 a.m
      // it has "1 day ago" but read at 3 p.m. it reads "2 days ago". so if the process is executed 2 times in a day, before and after 3 p.m. 2
      // different externalIDs will be generated if date is used for hash.)
      currComment.dateIsReliableForHash = false;
      // currComment.position = pos;
      currComment.isComment = true;
      currComment.hashKeyPrefix = currComment.body; // This avoids 2 different comments from the same author to be "duplicates"
      res.add(currComment);
      // pos++;
    }

    return res;
  }

  /**
   * @param currCommentNode
   * @return
   */
  private String findCommentBody(TagNode currCommentNode) {
    // TagNode[] commentBodyNode = currCommentNode.getElementsByAttValue("class", "comment-text", true, false);
    TagNode[] commentBodyNode = currCommentNode.getElementsByName("q", true);
    if (commentBodyNode == null || commentBodyNode.length < 1) {
      logger.warn("Node with class=\"comment-text\" not found. Returning null");
      return null;
    }

    return commentBodyNode[0].getText().toString();
  }

  @Override
  public Date findCommentDate(TagNode commentNode) {
    Date res = null;

    TagNode[] nusNode = commentNode.getElementsByAttValue("class", "nus-timestamp", true, false);
    if (nusNode == null || nusNode.length < 1) {
      logger.warn("Node with class=\"nus-timestamp\" not found. Returning null");
      return null;
    }

    String rawDate = nusNode[0].getText().toString();

    if (rawDate == null || "".equalsIgnoreCase(rawDate)) {
      logger.warn("The received rawDate [" + rawDate + "] is empty. Returning null.");
      return null;
    }

    if (logger.isTraceEnabled()) {
      logger.trace("Raw date in \"ago\" format [" + rawDate + "]");
    }

    res = parseAgoDate(rawDate.toLowerCase(), new Date(), null);

    if (logger.isTraceEnabled()) {
      logger.trace("Date processed [" + res.toString() + "]");
    }

    return res;
  }

  @Override
  public String findCommentAuthor(TagNode commentNode) {
    TagNode[] commentBodyNode = commentNode.getElementsByAttValue("class", "comment-details", true, false);
    if (commentBodyNode == null || commentBodyNode.length < 1) {
      logger.warn("Node with class=\"comment-details\" not found. Returning null");
      return null;
    }

    TagNode[] anchorCNode = commentBodyNode[0].getElementsByName("a", true);
    if (anchorCNode == null || anchorCNode.length < 1) {
      logger.warn("Anchor node not found. Returning null");
      return null;
    }

    return anchorCNode[0].getText().toString();
  }

  /**
   * @param currNode
   * @return
   */
  private String findReadMoreURL(TagNode currNode) {
    // href="https://www.linkedin.com/company/caixabank/redirect?url=https%3A%2F%2Fblog%2Ecaixabank%2Ees%2F2016%2F04%2Fresultados-de-caixabank-1t2016%2Ehtml&amp;
    // get attribute href and use regular expression url=(.*)&amp;

    TagNode[] titleNode = currNode.getElementsByAttValue("class", "title", true, false);
    if (titleNode == null || titleNode.length < 1) {
      logger.warn("Node with class=\"title\" not found. Returning null");
      return null;
    }
    String rawURL = titleNode[0].getAttributeByName("href");

    if (rawURL == null) {
      logger.warn("\"href\" attribute inside node with class=\"title\" not found. Returning null");
      return null;

    }

    if (logger.isTraceEnabled()) {
      logger.trace("The \"href\" attribute inside the node with class=\"title\" is [" + rawURL + "]");
    }

    Matcher urlMatcher = postUrlPattern.matcher(rawURL);

    if (urlMatcher.find()) {
      String url = urlMatcher.group(1);

      if (logger.isTraceEnabled()) {
        logger.trace("Found url [" + url + "] for the update ");
      }

      if (url != null && url.contains("%")) {
        try {
          String decodedUrl = URLDecoder.decode(url, StandardCharsets.UTF_8.name());
          if (logger.isTraceEnabled()) {
            logger.trace("Correctly decoded url [" + url + "] into [" + decodedUrl + "]");
          }
          url = decodedUrl;
        } catch (UnsupportedEncodingException uee) {
          logger.warn("Error [" + uee.getMessage() + "] while trying to URL decode the url [" + url + "] using the charset ["
              + StandardCharsets.UTF_8 + "]. Returning the raw url. It may make the process fail in later stages.");
        }
      }
      return url;
    } else {
      logger.warn("Not found any url in the string [" + rawURL + "]. Returning null.");
      return null;
    }

  }

  @Override
  public Integer findMainLikes(TagNode rootNode) {
    // class="like" attribute data-li-num-liked="44"
    TagNode[] likesNode = rootNode.getElementsByAttValue("class", "like", true, false);
    if (likesNode == null || likesNode.length < 1) {
      logger.warn("Node with class=\"like\" not found. Trying with class=\"unlike\"");
      likesNode = rootNode.getElementsByAttValue("class", "unlike", true, false);
    }

    if (likesNode == null || likesNode.length < 1) {
      logger.warn("Node with class=\"unlike\" not found. Returning null");
      return null;
    }

    String rawLikes = likesNode[0].getAttributeByName("data-li-num-liked");

    try {
      return Integer.parseInt(rawLikes);
    } catch (NumberFormatException nfe) {
      logger.warn("Error [" + nfe.getMessage() + "] while trying to get likes value. The raw likes value was [" + rawLikes
          + "]. Returning null.");
      return null;
    }

  }

  @Override
  public Integer findMainComments(TagNode rootNode) {
    // class="focus-comment-form" attribute data-li-num-commented="1"
    TagNode[] commentsNode = rootNode.getElementsByAttValue("class", "focus-comment-form", true, false);
    if (commentsNode == null || commentsNode.length < 1) {
      logger.warn("Node with class=\"focus-comment-form\" not found. Returning null");
      return null;
    }

    String rawComments = commentsNode[0].getAttributeByName("data-li-num-commented");

    try {
      return Integer.parseInt(rawComments);
    } catch (NumberFormatException nfe) {
      logger.warn("Error [" + nfe.getMessage() + "] while trying to get comments value. The raw comments value was [" + rawComments
          + "]. Returning null.");
      return null;
    }
  }

  @Override
  public String findMainBody(TagNode rootNode) {
    // class="description" inner text
    TagNode[] bodyNode = rootNode.getElementsByAttValue("class", "description", true, false);
    if (bodyNode == null || bodyNode.length < 1) {
      logger.warn("Node with class=\"description\" not found. Returning null");
      return null;
    }

    return bodyNode[0].getText().toString();
  }

  @Override
  public String findMainTitle(TagNode rootNode) {
    // class="title" inner text
    TagNode[] titleNode = rootNode.getElementsByAttValue("class", "title", true, false);
    if (titleNode == null || titleNode.length < 1) {
      logger.warn("Node with class=\"title\" not found. Returning null");
      return null;
    }

    return titleNode[0].getText().toString();
  }

  @Override
  public Date findMainDate(TagNode rootNode) {

    // data-li-update-date="1461824174252" (java format )
    String rawDate = rootNode.getAttributeByName("data-li-update-date");
    if (rawDate == null) {
      logger.warn("Attribute \"data-li-update-date\" not found. Returning null date.");
      return null;
    }
    try {
      long milis = Long.parseLong(rawDate);
      Calendar calTemp = Calendar.getInstance();
      calTemp.setTimeInMillis(milis);
      return calTemp.getTime();
    } catch (NumberFormatException nfe) {
      logger.warn("Error [" + nfe.getMessage() + "] while processing date. Raw date value [" + rawDate + "]. Returning null.");
      return null;
    }

  }

  /**
   * @param content
   *          the content from which the numerical companyID will be extracted
   * @param url
   *          the page url from which the content was extracted.
   * @return if the companyID is found a List of one SimpleItem that is a pointer to the first updates page for the company. Otherwise null
   */
  private List<SimpleItem> segmentMainPage(String content, URL url) {

    List<SimpleItem> res = null;
    Matcher companyIDMatcher = companyIDPattern.matcher(content);

    if (companyIDMatcher.find()) {
      String companyID = companyIDMatcher.group(1);

      if (logger.isTraceEnabled()) {
        logger.trace("Found company ID [" + companyID + "] for the company in page [" + url.toExternalForm() + "]");
      }

      SimpleItem updatesPage = new SimpleItem();
      updatesPage.fakeItem = true;
      updatesPage.threadURLs = new ArrayList<String>();
      updatesPage.threadURLs.add(LINKEDIN_BASE_URL + UPDATES_PAGE_PREFIX + companyID + UPDATES_PAGE_SUFIX);
      /* Minimal info for the DocumentParser class to create a fake clipping out of this fake simpleItem */
      updatesPage.postUrl = url.toExternalForm();
      updatesPage.date = new Date();
      res = new ArrayList<SimpleItem>();
      res.add(updatesPage);

    } else {
      logger.warn("Not able to get the company numerical ID for the company in the URL [" + url.toExternalForm() + "]. Returning null.");

    }

    return res;

  }
}
