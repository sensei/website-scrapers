/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class MeneameSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(MeneameSegmenter.class);
  
  private static final String BASE_URL = "http://www.meneame.net";
  private static SimpleDateFormat sd = new SimpleDateFormat("dd-MM-yyyy HH:mm");
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    // content = segmentInit(content, url);
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    Date current = new Date();
    HtmlCleaner cleaner = createCleaner();
    TagNode root = null;
    String site = null;
    if (url != null) {
      site = url.toString();
    }
    root = cleaner.clean(content);
    
    // /////////// Main Details Item ////////////
    SimpleItem main = new SimpleItem();
    
    // ///// Extract Title
    try {
      TagNode[] n = root.getElementsByName("h1", true);
      String title = removeWhiteSpaces(n[0].getText().toString());
      main.title = title;
    } catch (Exception ex) {}
    
    TagNode t1 = null;
    TagNode t2 = null;
    // //////////// Extract Author
    try {
      TagNode[] n = root.getElementsByAttValue("class", "news-submitted", true, false);
      TagNode[] tg = n[0].getElementsByName("a", true);
      for (TagNode element : tg) {
        try {
          String att = element.getAttributeByName("href");
          if (att.contains("/user/") && (att.endsWith("/history") || att.endsWith("/history/"))) {
            main.author = removeWhiteSpaces(element.getText().toString());
            t1 = element;
            break;
          }
        } catch (Exception e) {}
      }
    } catch (Exception ex) {}
    // //////////// Extract Date
    try {
      TagNode[] n = root.getElementsByAttValue("class", "news-submitted", true, false);
      try {
        t2 = n[0].findElementByName("strong", true);
        n[0].removeChild(t2);
      } catch (Exception ex) {}
      
      try {
        n[0].removeChild(t1);
      } catch (Exception ex) {}
      
      String date = null;
      date = removeWhiteSpaces(n[0].getText().toString().replace("por", "").replace("&nbsp;", "").trim());
      
      main.date = parseDate(date, current);
      if (main.date == null) {
        String timestamp = n[0].getElementsHavingAttribute("data-ts", true)[0].getAttributeByName("data-ts");
        main.date = new Date(Long.parseLong(timestamp) * 1000);
        main.timeIsReal = true;
      }
    } catch (Exception ex) {
      logger.warn("Error [" + ex.getMessage() + "] parsing date. Setting date to null");
      main.date = null;
    }
    
    // // //////////// Extract Comments
    try {
      TagNode[] n = root.getElementsByAttValue("class", "comments-counter", true, false);
      String comments = removeWhiteSpaces(n[0].getText().toString().replace("comentarios", ""));
      main.comments = Integer.parseInt(comments);
    } catch (Exception ex) {}
    
    // ////////////// Extract Score
    try {
      TagNode[] n = root.getElementsByAttValue("class", "news-body", true, false);
      n = n[0].getElementsByAttValue("class", "votes", true, false);
      String score = removeWhiteSpaces(n[0].getText().toString().replace("meneos", ""));
      try {
        main.likes = (int) Double.parseDouble(score);
      } catch (Exception e) {
        logger.warn("Exception [" + e.getMessage() + "] trying to parse [" + score + "] as a double to store it in likes field.", e);
        
      }
    } catch (Exception ex) {}
    
    // //////////// Extract Body
    try {
      TagNode[] n = root.getElementsByAttValue("class", "news-body", true, false);
      TagNode tg[] = n[0].getAllElements(true);
      for (TagNode element : tg) {
        try {
          n[0].removeChild(element);
        } catch (Exception ee) {}
      }
      String body = removeWhiteSpaces(n[0].getText().toString());
      main.body = body;
    } catch (Exception ex) {}
    
    main.position = 1;
    main.postUrl = site;
    items.add(main);
    
    // // ///////////////// Comment Items
    int position = 2;
    TagNode[] nn = root.getElementsByAttValue("class", "comments-list", true, false);
    TagNode rev[] = null;
    if (nn == null || nn.length < 1) {
      nn = root.getElementsByAttValue("class", "comments", true, false);
      /* Threader gets comments of level >1 in meneame and threader zero gets all the comments with root (0) level */
      rev = nn[0].getElementsByAttValue("class", "threader", true, false);
      TagNode rev2[] = nn[0].getElementsByAttValue("class", "threader zero", true, false);
      rev = ArrayUtils.addAll(rev, rev2);
    } else {
      rev = nn[0].getElementsByName("li", false);
    }
    
    for (TagNode t : rev) {
      SimpleItem item = new SimpleItem();
      
      // ////// Body
      try {
        TagNode[] bd = t.getElementsByAttValue("class", "comment-body", true, false);
        if (bd == null || bd.length < 1) {
          bd = t.getElementsByAttValue("class", "comment-body high", true, false);
        }
        if (bd == null || bd.length < 1) {
          bd = t.getElementsByAttValue("class", "comment-body high author", true, false);
        }
        if (bd == null || bd.length < 1) {
          bd = t.getElementsByAttValue("class", "comment-body author", true, false);
        }
        
        TagNode tg[] = bd[0].getAllElements(true);
        for (TagNode element : tg) {
          try {
            bd[0].removeChild(element);
          } catch (Exception ee) {}
        }
        String body = removeWhiteSpaces(bd[0].getText().toString());
        item.body = body;
      } catch (Exception e) {}
      // // /////// author
      String aTime = null;
      try {
        String author = removeWhiteSpaces(t.getElementsByAttValue("class", "username", true, false)[0].getText().toString());
        aTime = author;
        if (author.contains(" por ")) {
          item.author = author.substring(author.indexOf("por") + 3).trim();
        } else {
          item.author = author;
        }
      } catch (Exception e) {}
      // // /////// date
      try {
        TagNode[] dateNode = t.getElementsByAttValue("class", "ts showmytitle nomobile", true, false);
        if (dateNode == null || dateNode.length < 1) {
          throw new Exception("There is no node with info about the comment's date.");
        }
        String date = dateNode[0].getAttributeByName("data-ts");
        // Changed the date parsing for comments from looking for it textually to get it from a tag attribute. It should work ok wherever, but, the
        // web has also textual dates that our local test document hasnt.
        // Delete the commented lines if not needed beyond June 2015
        // <span class="ts showmytitle nomobile" data-ts="1373290247" title="creado: "></span>
        // String date = aTime.substring(0, aTime.indexOf("por") != -1 ? aTime.indexOf("por") : aTime.length()).replace("*", "").trim();
        // item.date = parseDate(date, current);
        item.date = new Date(Long.parseLong(date) * 1000);
        item.timeIsReal = true;
      } catch (Exception e) {
        logger.warn("Error [" + e.getMessage() + "] parsing date for a comment. Setting comment's date to null");
        item.date = null;
      }
      
      // // /////// votes
      String voteString = null;
      String votes = null;
      try {
        TagNode tg[] = t.getElementsByAttValue("class", "comment-votes-info", true, false);
        votes = removeWhiteSpaces(tg[0].getText().toString());
        voteString = votes;
        if (votes != null) {
          votes = votes.substring(votes.indexOf("votos:") + 6, votes.indexOf("karma")).trim();
          item.votes = Integer.parseInt(votes);
        }
      } catch (Exception e) {
        logger.warn("Exception [" + e.getMessage() + "] when parsing [" + votes + "] votes to put it into votes field.", e);
      }
      
      // // /////// karma is mapped to likes
      String score = null;
      try {
        Integer scoreInt = null;
        
        if (voteString == null || (voteString.indexOf("karma:") + 6) > voteString.length()) {
          scoreInt = 0;
        } else {
          score = voteString.substring(voteString.indexOf("karma:") + 6).trim();
          
          try {
            scoreInt = Integer.parseInt(score);
          } catch (NumberFormatException e) {
            score = score.substring(0, score.indexOf(" "));
            scoreInt = Integer.parseInt(score);
          }
        }
        item.likes = scoreInt;
      } catch (Exception e) {
        logger.warn("Exception [" + e.getMessage() + "] when parsing [" + score + "] karma to put it into likes field.", e);
      }
      
      // // /////// Item URL
      try {
        TagNode tg[] = t.getElementsByAttValue("class", "comment-votes-info", true, false);
        TagNode links[] = tg[0].getElementsByName("a", true);
        for (TagNode link : links) {
          if (link.getAttributeByName("title").equalsIgnoreCase("permalink")) {
            item.itemUrl = BASE_URL + link.getAttributeByName("href");
            break;
          }
        }
      } catch (Exception e) {}
      
      item.postUrl = site;
      item.position = position++;
      items.add(item);
    }
    return items;
  }
  
  final static Pattern relativeDate = Pattern.compile(" *hace +((\\d+) +horas?(?: y)? *)?((\\d+) minutos?)?");
  
  public static Date parseDate(String string, Date referenceTime) {
    Date ret = null;
    try {
      // absolute date (UTC)
      if (string.contains("UTC")) {
        int i = string.indexOf("UTC");
        string = string.substring(i - 17, i - 1);
        ret = sd.parse(string);
      } else if (string.startsWith("el ")) {
        string = string.substring(3);
        ret = sd.parse(string);
      } else {
        // relative date
        Matcher m = relativeDate.matcher(string);
        
        if (m.find()) {
          Calendar cal = Calendar.getInstance();
          
          if (referenceTime != null) {
            cal.setTime(referenceTime);
          }
          
          int hours = 0, mins = 0;
          
          if (m.group(2) != null) {
            hours = Integer.parseInt(m.group(2));
          }
          
          if (m.group(4) != null) {
            mins = Integer.parseInt(m.group(4));
          }
          
          cal.add(Calendar.HOUR_OF_DAY, -hours);
          cal.add(Calendar.MINUTE, -mins);
          ret = cal.getTime();
        }
      }
      
    } catch (Exception e) {
      logger.error("Error [" + e.getMessage() + "] parsing Meneame Date Item: [" + string + "]");
    }
    return ret;
  }
  
}
