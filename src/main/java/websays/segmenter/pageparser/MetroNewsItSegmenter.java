/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;

public class MetroNewsItSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(MetroNewsItSegmenter.class);
  
  @Override
  public String findMainDescripcion(TagNode rootNode) {
    return getNodeText(rootNode.findElementByName("h2", true));
  }
  
  @Override
  public Date findMainDate(TagNode rootNode) {
    SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy  HH:mm", Locale.FRANCE);
    Date date = null;
    try {
      String dateStr = findNodeTextByClass(rootNode, "odierna");
      date = sd.parse(dateStr);
      timeisReal = true;
    } catch (ParseException e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
      return null;
    }
    return date;
  }
  
  @Override
  public Boolean findMainTimeIsReal(TagNode rootNode) {
    findMainDate(rootNode);
    return timeisReal;
  }
  
}
