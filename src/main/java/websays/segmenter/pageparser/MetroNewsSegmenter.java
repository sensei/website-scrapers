/**
* Websays Opinion Analytics Engine
*
* (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
*
* Primary Author: Marco Martinez/Hugo Zaragoza
* Contributors:
* Date: Jul 7, 2014
*/
package websays.segmenter.pageparser;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;

public class MetroNewsSegmenter extends BaseHTMLSegmenter {

	@Override
	public String findMainThreadId(TagNode rootNode) {
		return String.format("%s-%s", findScriptIdentifiers(rootNode));
	}

	@Override
	public List<String> findMainThreadURLs(TagNode rootNode) {
		String discussionPage = "http://disqus.com/embed/comments/?disqus_version=4cacc63e&f=%s&t_i=%s&t_u&s_o=default";

		ArrayList<String> threadURLs = new ArrayList<String>();
		threadURLs.add(String.format(discussionPage,
				findScriptIdentifiers(rootNode)));
		return threadURLs;
	}

	@Override
	public String findMainAuthor(TagNode rootNode) {
		TagNode heading = rootNode.findElementByAttValue("name", "author",
				true, false);
		if (heading != null) {
			String content = StringEscapeUtils.unescapeXml(heading
					.getAttributeByName("content"));
			return removeWhiteSpaces(content);
		}
		return null;
	}

	private Object[] findScriptIdentifiers(TagNode rootNode) {
		String scriptStr = "";
		TagNode[] scripts = rootNode.getElementsByName("script", true);
		for (TagNode scriptNode : scripts) {
			if (scriptNode.getText().indexOf("disqus_identifier") > -1) {
				scriptStr = scriptNode.getText().toString();
				break;
			}
		}
		String forum = scriptStr.replaceFirst(".*disqus_shortname = '(.+?)'.*",
				"$1");
		String id = scriptStr.replaceFirst(".*disqus_identifier = '(.+?)'.*",
				"$1");
		return new String[] { forum, id };
	}
}
