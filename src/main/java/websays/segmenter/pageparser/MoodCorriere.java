/**
 * Websays Opinion Analytics Engine 
 * 
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 * 
 * Primary Author: Jorge Valderrama
 * Contributors: 
 * Date: 15/1/2015
 */
package websays.segmenter.pageparser;

import java.util.ArrayList;
import java.util.List;



/**
 * Class to manage the different type of mood in Corriere newspaper
 **/
public class MoodCorriere {
  
  private int angry;
  private int sad;
  private int worried; 
  private int happy;
  private int smile;
  
  
  public List<String> moodToListString() {
    
    
    List<String> list = new ArrayList<String>();
    list.add("angry:"+ getAngry());
    list.add("sad:"+ getSad());
    list.add("worried:"+ getWorried());
    list.add("happy:"+ getHappy());
    list.add("smile:"+ getSmile());
    return list;
  }
  
  public String moodToString() {
    String sMood = new String();
    sMood = sMood.concat("angry="+ getAngry());
    sMood = sMood.concat("|sad="+ getSad());
    sMood = sMood.concat("|worried="+ getWorried());
    sMood = sMood.concat("|happy="+ getHappy());
    sMood = sMood.concat("|smile="+ getSmile());
    return sMood;
  }
  
  
  
  public float getAngry() {
    return angry;
  }
  
  public void setAngry(int angry) {
    this.angry = angry;
  }
  
  public float getSad() {
    return sad;
  }
  
  public void setSad(int sad) {
    this.sad = sad;
  }
  
  public float getWorried() {
    return worried;
  }
  
  public void setWorried(int worried) {
    this.worried = worried;
  }
  
  public float getHappy() {
    return happy;
  }
  
  public void setHappy(int happy) {
    this.happy = happy;
  }
  
  public float getSmile() {
    return smile;
  }
  
  public void setSmile(int smile) {
    this.smile = smile;
  }
  
}
