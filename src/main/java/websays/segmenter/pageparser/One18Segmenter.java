/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class One18Segmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(One18Segmenter.class);
  
  private static SimpleDateFormat sd = new SimpleDateFormat("d 'de' MMMM 'de' yyyy", new Locale("es", "ES"));
  
  private static final Integer MAX_SCORE = 5;
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    HtmlCleaner cleaner = createCleaner();
    TagNode root = null;
    String site = null;
    if (url != null) {
      site = url.toString();
    }
    
    root = cleaner.clean(content);
    
    // /////////// Main Details Item ////////////
    SimpleItem main = new SimpleItem();
    
    // ///// Extract Title
    try {
      TagNode[] n = root.getElementsByName("h1", true);
      String title = removeWhiteSpaces(n[0].getText().toString());
      main.title = title;
    } catch (Exception ex) {}
    // //////////// Extract Body
    try {
      TagNode[] n = root.getElementsByAttValue("class", "adr", true, false);
      try {
        n[0].removeChild(n[0].getElementsByAttValue("class", "transportation", true, false)[0]);
      } catch (Exception e) {}
      String body = "";
      TagNode[] childs = n[0].getAllElements(false);
      for (int i = 0; i < childs.length; i++) {
        try {
          body += childs[i].getText().toString() + " ";
        } catch (Exception e) {}
      }
      body = removeLongSpaces(body);
      main.body = body;
    } catch (Exception ex) {}
    // //////////// Extract Author
    try {
      TagNode[] n = root.getElementsByAttValue("class", "discoverer", true, false);
      String author = removeWhiteSpaces(n[0].getText().toString().replace("descubierto", "").replace("por:", ""));
      main.author = author;
    } catch (Exception ex) {}
    // ////////////// Extract Reviews
    try {
      TagNode[] n = root.getElementsByAttValue("class", "nop", true, false);
      String reviews = removeWhiteSpaces(n[0].getText().toString().replace("opiniones", ""));
      main.reviews = Integer.parseInt(reviews);
    } catch (Exception ex) {}
    
    // ////////////// Extract Item URL
    try {
      TagNode[] n = root.getElementsByAttValue("class", "url web", true, false);
      String itemUrl = removeWhiteSpaces(n[0].getText().toString());
      main.itemUrl = itemUrl;
      
    } catch (Exception ex) {}
    main.date = null;
    main.position = 1;
    
    // this just indicates the sort of the site by date
    if (site.endsWith("/date")) {
      main.postUrl = site.substring(0, site.length() - 5);
    } else {
      main.postUrl = site;
    }
    // override item url, should be the site
    main.itemUrl = main.postUrl;
    
    items.add(main);
    
    // ///////////////// Comment Items
    int position = 2;
    Date current = new Date();
    TagNode reviews[] = root.getElementsByAttValue("class", "ri-main", true, false);
    
    for (TagNode t : reviews) {
      SimpleItem item = new SimpleItem();
      
      // ////// Title
      try {
        item.title = removeWhiteSpaces(t.getElementsByAttValue("class", "summary", true, false)[0].getText().toString());
      } catch (Exception e) {}
      
      // ///// Author
      try {
        TagNode[] ti = t.getElementsByAttValue("class", "reviewer", true, false)[0].getElementsByName("a", true);
        for (int i = 0; i < ti.length; i++) {
          try {
            if (ti[i].getAttributeByName("itemprop").equalsIgnoreCase("author")) {
              item.author = removeWhiteSpaces(ti[i].getText().toString());
              break;
            }
          } catch (Exception e) {}
        }
      } catch (Exception e) {}
      
      // ////// Body
      try {
        TagNode td = t.getElementsByAttValue("class", "description", true, false)[0];
        item.body = removeWhiteSpaces(td.getText().toString());
        TagNode tg[] = td.getElementsByName("a", true);
        for (int i = 0; i < tg.length; i++) {
          td.removeChild(tg[i]);
        }
        item.body = removeWhiteSpaces(td.getText().toString());
        
      } catch (Exception e) {}
      
      // /////// votes (like)
      
      try {
        TagNode tg[] = t.getElementsByAttValue("class", "like-actions-n ico ico-like", true, false);
        if (tg.length == 0) {
          tg = t.getElementsByAttValue("class", "like-actions-n ico ico-like ", true, false);
        }
        String votes = removeWhiteSpaces(tg[0].getText().toString());
        item.votes = Integer.parseInt(votes);
      } catch (Exception e) {}
      
      // /////// score
      String score = null;
      try {
        
        TagNode tn = t.getElementsByAttValue("class", "rating", true, false)[0];
        score = tn.getAttributeByName("src");
        if (score.contains("rating1.")) {
          item.setScores(1, MAX_SCORE);
        } else if (score.contains("rating2.")) {
          item.setScores(2, MAX_SCORE);
        }
        if (score.contains("rating3.")) {
          item.setScores(3, MAX_SCORE);
        }
        if (score.contains("rating4.")) {
          item.setScores(4, MAX_SCORE);
        }
        if (score.contains("rating5.")) {
          item.setScores(5, MAX_SCORE);
        }
        
      } catch (Exception e) {
        logger.warn("Exception [" + e.getMessage() + "] while getting and parsing rating [" + score + "]", e);
      }
      
      // /////// item URL
      String date = null;
      try {
        TagNode tg[] = t.getElementsByAttValue("class", "meta", true, false);
        tg = tg[0].getElementsByName("a", true);
        for (int i = 0; i < tg.length; i++) {
          try {
            if (tg[i].getAttributeByName("itemprop").equalsIgnoreCase("discussionUrl")) {
              try {
                date = removeWhiteSpaces(tg[i].getText().toString());
              } catch (Exception e) {}
              item.itemUrl = tg[i].getAttributeByName("href");
              if (!item.itemUrl.startsWith("http://11870.com") && !item.itemUrl.startsWith("https://11870.com")) {
                item.itemUrl = "http://11870.com" + item.itemUrl;
              }
              break;
            }
          } catch (Exception e) {}
        }
      } catch (Exception e) {}
      // /////// date
      try {
        item.date = parseAgoDate(date, current, sd);
      } catch (Exception e) {
        logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
        item.date = null;
      }
      
      item.authorLocation = null;
      item.postUrl = site;
      item.position = position++;
      items.add(item);
      
    }
    return items;
  }
  
  @Override
  public Integer getNativeMax() {
    return MAX_SCORE;
  }
  
}
