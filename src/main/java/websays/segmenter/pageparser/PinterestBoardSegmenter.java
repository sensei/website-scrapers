/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.log4j.Logger;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class PinterestBoardSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(PinterestBoardSegmenter.class);
  
  private static final String BASE_URL = "http://www.pinterest.com";
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    List<SimpleItem> items = new ArrayList<SimpleItem>();
    String urlStr = url.toExternalForm();
    // we want the pins section
    if (!urlStr.endsWith("/pins/") && !urlStr.endsWith("/pins")) {
      items = new ArrayList<SimpleItem>();
      if (!urlStr.endsWith("/")) {
        urlStr += "/";
      }
      url = new URL(urlStr + "pins/");
    }
    items = super.segment(content, url);
    return items;
  }
  
  @Override
  public List<String> findMainThreadURLs(TagNode rootNode) {
    List<TagNode> elementListByAttValue = rootNode.getElementListByAttValue("class", "pinImageWrapper ", true, false);
    if (elementListByAttValue == null || elementListByAttValue.size() < 1) {
      elementListByAttValue = rootNode.getElementListByAttValue("class", "pinImageWrapper draggable", true, false);
    }
    HashSet<String> urls = new HashSet<String>();
    for (TagNode t : elementListByAttValue) {
      String href = t.getAttributeByName("href");
      urls.add(BASE_URL + href);
      
    }
    
    List<String> list = new ArrayList<String>(urls);
    
    return list;
  }
  
}
