/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.utils.misc.DateUtilsWebsays;

public class PinterestPinSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(PinterestPinSegmenter.class);
  
  private static final String BASE_URL = "http://www.pinterest.com";
  
  /* Static variables to parse pinterest dates */
  private static final String pinterestDateFormat = "\\s*(\\d+)([hmdwyHMDWY])";
  private static final Pattern pinterestDatePattern = Pattern.compile(pinterestDateFormat);
  
  @Override
  public String findMainAuthor(TagNode rootNode) {
    String author = super.findNodeTextByClass(rootNode, "commentDescriptionCreator");
    if (author == null) {
      author = super.findNodeTextByClass(rootNode, "fullname");
    }
    return author;
  }
  
  @Override
  public String findMainTitle(TagNode rootNode) {
    String title = super.findNodeTextByClass(rootNode, "boardRepSubtitle");
    if (title != null) {
      return title + " pin";
    }
    
    TagNode temp1 = rootNode.findElementByAttValue("class", "boardName", true, false);
    String nodeNameTemp = null;
    
    if (temp1 != null) {
      nodeNameTemp = temp1.getName();
    }
    while (!"a".equalsIgnoreCase(nodeNameTemp) && temp1 != null) {
      temp1 = temp1.findElementByAttValue("class", "boardName", true, false);
      if (temp1 != null) {
        nodeNameTemp = temp1.getName();
      }
      
    }
    
    if (temp1 == null) {
      return null;
    } else {
      return temp1.getText().toString() + " pin";
    }
  }
  
  @Override
  public String findMainImageLink(TagNode rootNode) {
    String image = null;
    TagNode imageTag = null;
    try {
      imageTag = rootNode.findElementByAttValue("class", "pinImage", true, false);
      if (imageTag == null) {
        logger.warn("\"pinImage\" tag not found. Trying with \"pinImage rounded\" tag.");
        imageTag = rootNode.findElementByAttValue("class", "pinImage rounded", true, false);
      }
      
      if (imageTag != null) {
        image = imageTag.getAttributeByName("src");
      } else {
        logger.warn("\"pinImage\" and \"pinImage rounded\" tags not found. No image to return.");
      }
    } catch (Exception e) {
      logger.error("Unexpected error [" + e.getMessage() + "] while getting the image for the main post. Returning null.");
      
    }
    return image;
  }
  
  @Override
  public String findMainDescripcion(TagNode rootNode) {
    String body = super.findNodeTextByClass(rootNode, "commentDescriptionContent");
    String metaContent = super.getMetaContent(rootNode, "pinterestapp:source");
    if (metaContent != null && metaContent.length() > 0) {
      body += " (found on " + metaContent + ")";
    }
    
    return body;
  }
  
  /**
   * Converts a pinterest ago date (i.e. 2y, 33w, 6d, 12h....) into a date object.
   * 
   * @param textualDate
   *          the textual date get from pinterest page.
   * @param referenceDate
   *          a date which is the point in time from which the ago date will be calculated. Usually this is "now"
   * @return a date object that is the date calculated substracting the textualdate from the referenceDate
   */
  private Date parseAgoDatePinterest(String textualDate, Date referenceDate) {
    
    Date res = null;
    if (textualDate == null) {
      logger.warn("Textual date [" + textualDate
          + "] review wether this is an error or there is no date in the page you are parsing. Returning date [" + res + "]");
      return null;
    }
    
    boolean error = false;
    Matcher mDate = pinterestDatePattern.matcher(textualDate);
    
    int amount = 0;
    int timespan = -1;
    if (mDate.matches()) {
      String textAmount = mDate.group(1);
      String span = mDate.group(2);
      try {
        amount = Integer.parseInt(textAmount);
        switch (span) {
          case "m":
          case "M":
            timespan = Calendar.MINUTE;
            break;
          case "h":
          case "H":
            timespan = Calendar.HOUR_OF_DAY;
            break;
          case "d":
          case "D":
            timespan = Calendar.DAY_OF_MONTH;
            break;
          case "w":
          case "W":
            timespan = Calendar.WEEK_OF_YEAR;
            break;
          case "y":
          case "Y":
            timespan = Calendar.YEAR;
            break;
          default:
            logger.error("The date span [" + span + "] that came from [" + textualDate
                + "] is not m:minute, h:hour d:day, w:week or y:year.");
            break;
        }
      } catch (NumberFormatException nfe) {
        logger.error("The date value [" + textAmount + "] that came from [" + textualDate + "] is not a number.");
      }
      
      if (!error) {
        Calendar tempCal = Calendar.getInstance();
        tempCal.setTime(referenceDate);
        tempCal.add(timespan, -(amount));
        res = tempCal.getTime();
      }
      
    }
    
    return res;
  }
  
  @Override
  public Date findMainDate(TagNode rootNode) {
    Date parseAgoDate = null;
    String date = null;
    
    // <meta itemprop="datePublished" content="2014-10-16T17:18:20">
    TagNode heading = rootNode.findElementByAttValue("itemprop", "datePublished", true, false);
    if (heading != null) {
      date = heading.getAttributeByName("content");
      if (date != null && date.length() > 0) {
        if (!date.endsWith("Z")) {
          date = date + "Z";
        }
        parseAgoDate = DateUtilsWebsays.fromISO8601(date);
        if (parseAgoDate != null) {
          return parseAgoDate;
        }
      }
    }
    logger
        .warn("Getting a structured date from the meta tag \"datePublished\" didn't work. Trying to get the date from the page information (commentDescriptionTimeAgo node).");
    date = super.findNodeTextByClass(rootNode, "commentDescriptionTimeAgo");
    try {
      parseAgoDate = HTMLDocumentSegmenter.parseAgoDate(date, new Date(), null);
    } catch (Exception e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
      parseAgoDate = null;
    }
    
    if (parseAgoDate == null) {
      /* Trying to parse the date using another format */
      parseAgoDate = this.parseAgoDatePinterest(date, new Date());
    }
    
    return parseAgoDate;
  }
  
  @Override
  public Integer findMainLikes(TagNode rootNode) {
    String findNodeTextByClass = super
        .findNodeTextByClass(
            rootNode,
            "medium rounded NavigateButton repinLikeNavigateButton like leftRounded pinActionBarButton Button IncrementingNavigateButton hasText Module btn");
    Integer likes = null;
    if (findNodeTextByClass != null) {
      try {
        
        likes = Integer.parseInt(findNodeTextByClass);
      } catch (Exception e) {
        logger.error(e);
      }
    }
    
    /* Alternate method to get likes */
    if (likes == null) {
      findNodeTextByClass = super.findNodeTextByClass(rootNode, "likeCount countInfo");
      if (findNodeTextByClass != null) {
        findNodeTextByClass = findNodeTextByClass.replace("Likes", "").trim();
        try {
          likes = Integer.parseInt(findNodeTextByClass);
        } catch (Exception e) {
          logger.error(e);
        }
      }
    }
    
    return likes;
  }
  
  @Override
  public Integer findMainFavorites(TagNode rootNode) {
    // String findNodeTextByClass = super
    String findNodeTextByClass = super
        .findNodeTextByClass(rootNode,
            "medium rounded NavigateButton repinLikeNavigateButton pinActionBarButton Button primary IncrementingNavigateButton hasText Module btn");
    Integer favs = null;
    if (findNodeTextByClass != null) {
      
      try {
        favs = Integer.parseInt(findNodeTextByClass);
      } catch (Exception e) {
        logger.error(e);
      }
    }
    
    /* Alternate method to get favorites */
    if (favs == null) {
      findNodeTextByClass = super.findNodeTextByClass(rootNode, "repinCount countInfo");
      if (findNodeTextByClass != null) {
        findNodeTextByClass = findNodeTextByClass.replace("Repins", "").trim();
        try {
          favs = Integer.parseInt(findNodeTextByClass);
        } catch (Exception e) {
          logger.error(e);
        }
      }
    }
    
    return favs;
  }
  
  @Override
  public String findMainAPIObjectID(TagNode rootNode) {
    String id = null;
    try {
      String metaContent = getMetaContent(rootNode, "og:url");
      if (metaContent.endsWith("/")) {
        metaContent = metaContent.substring(0, metaContent.length() - 1);
      }
      int lastIndexOf = metaContent.lastIndexOf("/");
      id = metaContent.substring(lastIndexOf + 1, metaContent.length());
    } catch (Exception e) {
      logger.error(e);
    }
    return id;
  }
  
  @Override
  public String findMainAPIAuthorID(TagNode rootNode) {
    String id = null;
    try {
      String metaContent = getMetaContent(rootNode, "pinterestapp:pinner");
      if (metaContent.endsWith("/")) {
        metaContent = metaContent.substring(0, metaContent.length() - 1);
      }
      int lastIndexOf = metaContent.lastIndexOf("/");
      id = metaContent.substring(lastIndexOf + 1, metaContent.length());
    } catch (Exception e) {
      logger.error("Unexpected error [" + e.getMessage() + "] while finding APIAuthorID for the main comment. Returning null.", e);
    }
    return id;
  }
  
  @Override
  public TagNode[] findComments(TagNode rootNode) {
    
    TagNode[] commentList = null;
    commentList = rootNode.getElementsByAttValue("class", "Module PinCommentList detailed", true, false);
    
    if (commentList != null && commentList.length > 0) {
      if (logger.isTraceEnabled()) {
        logger.trace("The pin has [" + commentList.length + "] comments.");
      }
      
    } else {
      if (logger.isTraceEnabled()) {
        logger.trace("The pin has no comments.");
      }
    }
    return commentList;
    
  }
  
  /*
   * 
   * comment.APIObjectID = findCommentAPIObjectID(commentNode); comment.title = findCommentTitle(commentNode); comment.body =
   * findCommentDescripcion(commentNode); comment.author = findCommentAuthor(commentNode); comment.authorLocation =
   * findCommentAuthorLocation(commentNode); comment.date = findCommentDate(commentNode); comment.imageLink = findCommentImageLink(commentNode); Float
   * native_score = findCommentScore(commentNode); Integer native_max = getNativeMax(); if (native_score != null && native_max != null) {
   * comment.setScores(native_score, native_max); } comment.likes = findCommentLikes(commentNode); comment.votes = findCommentVotes(commentNode);
   * comment.comments = findCommentNumComments(commentNode); comment.threadId = threadId; comment.threadURLs = list; comment.isComment = true;
   * comment.parentID = findCommentParentID(commentNode); comment.timeIsReal = findCommentTimeIsReal(commentNode); comment.language =
   * findCommentLanguage(commentNode);
   */
  
}
