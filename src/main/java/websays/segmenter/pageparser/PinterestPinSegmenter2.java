/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;
import websays.utils.misc.DateUtilsWebsays;

/**
 * This class was made to scrap public pinterest pages during 2016. It scraps parting fron the /<account name>/pins of each account. It complements
 * but not overlaps with PinterestPinSegmenter TODO: getting not public content. getting more than the first 24 pins (as a user getting more cippings
 * is done by vertical scrolling. programatically it is complex as involves sending headers to the pinterest server)
 * 
 * @author juanfra
 *
 */
public class PinterestPinSegmenter2 extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(PinterestPinSegmenter2.class);
  
  private static final String BASE_URL = "http://www.pinterest.com";
  
  /* Static variables to parse pinterest dates */
  private static final String pinterestDateFormat = "\\s*(\\d+)([hmdwyHMDWY])";
  private static final Pattern pinterestDatePattern = Pattern.compile(pinterestDateFormat);
  
  /* Static variables to parse the url to get the apiObjectID */
  private static final String pinterestAPIObjectUrlFormat = ".*/pin/(\\d+)/";
  private static final Pattern pinterestAPIObjectPattern = Pattern.compile(pinterestAPIObjectUrlFormat);
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    TagNode rootNode = getRootNode(content);
    List<SimpleItem> res = new ArrayList<SimpleItem>();
    int position = 1;
    List<TagNode> htmlItems = rootNode.getElementListByAttValue("class", "item ", true, false);
    String apiAuthorId = findMainAPIAuthorID(url.toExternalForm());
    for (TagNode currPin : htmlItems) {
      SimpleItem currItem = createMainItem(currPin, url.toString());
      
      currItem.postUrl = findMainUrl(currPin);
      currItem.itemUrl = currItem.postUrl;
      currItem.APIObjectID = findMainAPIObjectID(currItem.postUrl);
      try {
        int temp = Integer.parseInt(currItem.APIObjectID);
        currItem.position = temp; // Do it this way to keep the externalId between more than one indexing when there are new pins.
      } catch (NumberFormatException nfe) {
        logger.warn("Can't convert [" + currItem.APIObjectID + "] into a number. Position will be a serial number [" + position + "]");
        currItem.position = position++;
      }
      currItem.APIAuthorID = apiAuthorId;
      currItem.isComment = false;
      res.add(currItem);
    }
    return res;
  }
  
  /**
   * Gets the url where this pin is stored.
   * 
   * @param currPin
   *          the node inside which all the pin information is stored
   * @return the url for this pin
   */
  private String findMainUrl(TagNode rootNode) {
    String postUrl = null;
    TagNode postUrlTag = null;
    try {
      postUrlTag = rootNode.findElementByAttValue("class", "pinImageWrapper", true, false);
      if (postUrlTag == null) {
        logger.warn("\"pinImageWrapper\" tag not found. postUrl not found. Returning null.");
        return null;
      }
      
      if (postUrlTag != null) {
        postUrl = postUrlTag.getAttributeByName("href");
      }
      
      if (postUrl == null) {
        logger.warn("postUrl not found. Returning null.");
      } else {
        postUrl = BASE_URL + postUrl;
      }
    } catch (Exception e) {
      logger.error("Unexpected error [" + e.getMessage() + "] while getting the postUrl for the main post. Returning null.");
      
    }
    return postUrl;
  }
  
  @Override
  public String findMainAuthor(TagNode rootNode) {
    String author = super.findNodeTextByClass(rootNode, "creditName");
    if (author == null) {
      logger.warn("author not found.");
    }
    return author;
  }
  
  @Override
  public String findMainTitle(TagNode rootNode) {
    String title = super.findNodeTextByClass(rootNode, "creditTitle");
    if (title != null) {
      return title.trim();
    } else {
      logger.warn("Title not found");
    }
    return null;
  }
  
  @Override
  public String findMainImageLink(TagNode rootNode) {
    String image = null;
    TagNode imageTag = null;
    try {
      imageTag = rootNode.findElementByAttValue("class", "pinImg fullBleed", true, false);
      if (imageTag == null) {
        logger.warn("\"pinImg fullBleed loaded\" tag not found. ImageLink not found. Returning null.");
        return null;
      }
      
      if (imageTag != null) {
        image = imageTag.getAttributeByName("src");
      }
      
      if (image == null) {
        logger.warn("Imagelink not found. Returning null.");
      }
    } catch (Exception e) {
      logger.error("Unexpected error [" + e.getMessage() + "] while getting the image for the main post. Returning null.");
      
    }
    return image;
  }
  
  @Override
  public String findMainDescripcion(TagNode rootNode) {
    String body = null;
    TagNode bodyTag = null;
    try {
      bodyTag = rootNode.findElementByAttValue("class", "pinImg fullBleed", true, false);
      if (bodyTag == null) {
        logger.warn("\"pinImg fullBleed loaded\" tag not found. Body not found. Returning null.");
        return null;
      }
      
      if (bodyTag != null) {
        body = bodyTag.getAttributeByName("alt");
      }
      
      if (body == null) {
        logger.warn("Body not found. Returning null.");
      } else {
        // Optional information
        TagNode optional = rootNode.findElementByAttValue("class",
            "Button Module NavigateButton borderless hasText pinNavLink navLinkOverlay", true, false);
        if (optional != null) {
          String reference = optional.getAttributeByName("href");
          if (reference != null && !"".equalsIgnoreCase(reference)) {
            body = body + " (found in " + reference + " )";
          }
        }
      }
    } catch (Exception e) {
      logger.error("Unexpected error [" + e.getMessage() + "] while getting the body for the main post. Returning null.");
      
    }
    return body;
  }
  
  /**
   * Converts a pinterest ago date (i.e. 2y, 33w, 6d, 12h....) into a date object.
   * 
   * @param textualDate
   *          the textual date get from pinterest page.
   * @param referenceDate
   *          a date which is the point in time from which the ago date will be calculated. Usually this is "now"
   * @return a date object that is the date calculated substracting the textualdate from the referenceDate
   */
  private Date parseAgoDatePinterest(String textualDate, Date referenceDate) {
    
    Date res = null;
    if (textualDate == null) {
      logger.warn("Textual date [" + textualDate
          + "] review wether this is an error or there is no date in the page you are parsing. Returning date [" + res + "]");
      return null;
    }
    
    boolean error = false;
    Matcher mDate = pinterestDatePattern.matcher(textualDate);
    
    int amount = 0;
    int timespan = -1;
    if (mDate.matches()) {
      String textAmount = mDate.group(1);
      String span = mDate.group(2);
      try {
        amount = Integer.parseInt(textAmount);
        switch (span) {
          case "m":
          case "M":
            timespan = Calendar.MINUTE;
            break;
          case "h":
          case "H":
            timespan = Calendar.HOUR_OF_DAY;
            break;
          case "d":
          case "D":
            timespan = Calendar.DAY_OF_MONTH;
            break;
          case "w":
          case "W":
            timespan = Calendar.WEEK_OF_YEAR;
            break;
          case "y":
          case "Y":
            timespan = Calendar.YEAR;
            break;
          default:
            logger.error("The date span [" + span + "] that came from [" + textualDate
                + "] is not m:minute, h:hour d:day, w:week or y:year.");
            break;
        }
      } catch (NumberFormatException nfe) {
        logger.error("The date value [" + textAmount + "] that came from [" + textualDate + "] is not a number.");
      }
      
      if (!error) {
        Calendar tempCal = Calendar.getInstance();
        tempCal.setTime(referenceDate);
        tempCal.add(timespan, -(amount));
        res = tempCal.getTime();
      }
      
    }
    
    return res;
  }
  
  @Override
  public Date findMainDate(TagNode rootNode) {
    Date parseAgoDate = null;
    String date = null;
    
    // <meta itemprop="datePublished" content="2014-10-16T17:18:20">
    TagNode heading = rootNode.findElementByAttValue("itemprop", "datePublished", true, false);
    if (heading != null) {
      date = heading.getAttributeByName("content");
      if (date != null && date.length() > 0) {
        if (!date.endsWith("Z")) {
          date = date + "Z";
        }
        parseAgoDate = DateUtilsWebsays.fromISO8601(date);
        if (parseAgoDate != null) {
          return parseAgoDate;
        }
      }
    }
    logger
        .warn("Getting a structured date from the meta tag \"datePublished\" didn't work. Trying to get the date from the page information (commentDescriptionTimeAgo node).");
    date = super.findNodeTextByClass(rootNode, "commentDescriptionTimeAgo");
    try {
      parseAgoDate = HTMLDocumentSegmenter.parseAgoDate(date, new Date(), null);
    } catch (Exception e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
      parseAgoDate = null;
    }
    
    if (parseAgoDate == null) {
      /* Trying to parse the date using another format */
      parseAgoDate = this.parseAgoDatePinterest(date, new Date());
    }
    
    return parseAgoDate;
  }
  
  @Override
  public Integer findMainLikes(TagNode rootNode) {
    String likesString = super.findNodeTextByClass(rootNode, "socialMetaCount likeCountSmall");
    Integer likes = null;
    // Very dirty but expect that it works
    String[] onlyNumber = null;
    if (likesString != null) {
      onlyNumber = likesString.split("\\s");
    }
    if (onlyNumber != null && onlyNumber.length > 0) {
      try {
        
        likes = Integer.parseInt(onlyNumber[0]);
      } catch (Exception e) {
        logger.error("Unexpected error [" + e.getMessage() + "] while parsing likes amount [" + likesString + "]", e);
      }
    }
    
    return likes;
  }
  
  @Override
  public Integer findMainFavorites(TagNode rootNode) {
    // Repines are mapped to favorites in SimpleItem.
    String favsString = super.findNodeTextByClass(rootNode, "socialMetaCount repinCountSmall");
    Integer favs = null;
    // Very dirty but expect that it works
    String[] onlyNumber = null;
    if (favsString != null) {
      onlyNumber = favsString.split("\\s");
    }
    if (onlyNumber != null && onlyNumber.length > 0) {
      try {
        
        favs = Integer.parseInt(onlyNumber[0]);
      } catch (Exception e) {
        logger.error("Unexpected error [" + e.getMessage() + "] while parsing repines amount [" + favsString + "]", e);
      }
    }
    
    return favs;
  }
  
  /**
   * Given a pinterest pin url returns the pin APIObjectId.
   * 
   * @param postUrl
   *          the url of the pin
   * @return null if the url is null or don't match the pattern. The APIObjectId otherwise.
   */
  public String findMainAPIObjectID(String postUrl) {
    String apiObjectId = null;
    if (postUrl != null) {
      Matcher urlMatch = pinterestAPIObjectPattern.matcher(postUrl);
      if (urlMatch.find()) {
        apiObjectId = urlMatch.group(1);
      }
    }
    
    return apiObjectId;
  }
  
  /**
   * Gets the APIAuthorId from the url of the pins page
   * 
   * @param parPinsURL
   *          the url of the pins page
   * @return the APIAuthorId or null if the input url has not the right format
   */
  public String findMainAPIAuthorID(String parPinsURL) {
    String apiAuthorId = null;
    if (parPinsURL != null) {
      String[] comps = parPinsURL.split("/");
      if (comps != null && comps.length >= 4) {
        apiAuthorId = comps[3];
      }
    }
    
    return apiAuthorId;
  }
  
  @Override
  public TagNode[] findComments(TagNode rootNode) {
    
    TagNode[] commentList = null;
    commentList = rootNode.getElementsByAttValue("class", "Module PinCommentList detailed", true, false);
    
    if (commentList != null && commentList.length > 0) {
      if (logger.isTraceEnabled()) {
        logger.trace("The pin has [" + commentList.length + "] comments.");
      }
      
    } else {
      if (logger.isTraceEnabled()) {
        logger.trace("The pin has no comments.");
      }
    }
    return commentList;
    
  }
  
  /*
   * 
   * comment.APIObjectID = findCommentAPIObjectID(commentNode); comment.title = findCommentTitle(commentNode); comment.body =
   * findCommentDescripcion(commentNode); comment.author = findCommentAuthor(commentNode); comment.authorLocation =
   * findCommentAuthorLocation(commentNode); comment.date = findCommentDate(commentNode); comment.imageLink = findCommentImageLink(commentNode); Float
   * native_score = findCommentScore(commentNode); Integer native_max = getNativeMax(); if (native_score != null && native_max != null) {
   * comment.setScores(native_score, native_max); } comment.likes = findCommentLikes(commentNode); comment.votes = findCommentVotes(commentNode);
   * comment.comments = findCommentNumComments(commentNode); comment.threadId = threadId; comment.threadURLs = list; comment.isComment = true;
   * comment.parentID = findCommentParentID(commentNode); comment.timeIsReal = findCommentTimeIsReal(commentNode); comment.language =
   * findCommentLanguage(commentNode);
   */
  
}
