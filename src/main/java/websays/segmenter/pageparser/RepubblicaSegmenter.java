/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;

public class RepubblicaSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(RepubblicaSegmenter.class);
  
  private static final String SITE = "#repubblica";
  
  @Override
  public Date findMainDate(TagNode rootNode) {
    Date date = null;
    try {
      String publishedTime = getMetaContent(rootNode, "article:published_time");
      if (publishedTime == null) {
        return null;
      }
      timeisReal = true;
      date = DatatypeConverter.parseDateTime(publishedTime + ":00").getTime();
    } catch (Exception e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
      return null;
    }
    return date;
  }
  
  @Override
  public Boolean findMainTimeIsReal(TagNode rootNode) {
    findMainDate(rootNode);
    return timeisReal;
  }
  
  @Override
  public String findMainThreadId(TagNode rootNode) {
    try {
      TagNode gsSocial = rootNode.findElementByAttValue("id", "gs-social-comments", true, false);
      if (gsSocial == null) {
        return null;
      }
      String repubblicaCode = gsSocial.getAttributeByName("data-gigya-comment-category");
      String urlStr = getMetaContent(rootNode, "og:url");
      String urlEncode = URLEncoder.encode(urlStr, "UTF-8");
      return repubblicaCode + "/" + urlEncode;
    } catch (UnsupportedEncodingException e) {}
    return null;
  }
  
  @Override
  public List<String> findMainThreadURLs(TagNode rootNode) {
    String gigya = "http://comments.us1.gigya.com/comments/rss/6145611/";
    ArrayList<String> threadURLs = new ArrayList<String>();
    threadURLs.add(gigya + findMainThreadId(rootNode) + SITE);
    return threadURLs;
  }
  
  /**
   * @return the site
   */
  public static String getSite() {
    return SITE;
  }
  
}
