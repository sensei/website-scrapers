/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.log4j.Logger;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class RestaurantesSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(RestaurantesSegmenter.class);
  
  private static SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  
  private static final Integer MAX_SCORE = 10;
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    TagNode root = getRootNode(content);
    String site = url.toString();
    
    // /////////// Main Details Item ////////////
    SimpleItem main = createSimpleItem(site);
    
    main.title = removeWhiteSpaces(root.findElementByName("h1", true).getText().toString());
    TagNode description = root.findElementByAttValue("id", "descripcion", true, false);
    if (description != null) {
      main.body = removeWhiteSpaces(description.getText().toString());
    }
    TagNode countCommentsNode = root.findElementByAttValue("class", "count hide", true, false);
    if (countCommentsNode != null) {
      main.comments = Integer.parseInt(countCommentsNode.getText().toString());
    }
    
    TagNode averageNode = root.findElementByAttValue("class", "average", true, false);
    if (averageNode != null) {
      Float realAverage = new Float(averageNode.getText().toString());
      main.setScores(realAverage, MAX_SCORE);
    }
    TagNode tablaOpinionNode = root.findElementByAttValue("class", "tablaOpinion", true, false);
    if (tablaOpinionNode != null) {
      TagNode strong = tablaOpinionNode.getElementsByName("strong", true)[1];
      main.likes = Integer.parseInt(strong.getText().toString());
    }
    addSimpleItem(main);
    
    TagNode[] reviewsNode = root.getElementsByAttValue("class", "hreview", true, false);
    for (TagNode reviewNode : reviewsNode) {
      addReviews(reviewNode, site);
    }
    return getItems();
  }
  
  private void addReviews(TagNode reviewNode, String postUrl) throws ParseException {
    SimpleItem review = createSimpleItem(postUrl);
    review.author = removeWhiteSpaces(reviewNode.findElementByAttValue("class", "reviewer", true, false).getText().toString());
    
    review.body = removeWhiteSpaces(reviewNode.findElementByAttValue("class", "description", true, false).getText().toString());
    
    TagNode imgNode = reviewNode.getElementsByName("img", true)[1];
    String reviewPointsStr = imgNode.getAttributeByName("title").substring(11);
    
    review.reviews = Integer.parseInt(reviewPointsStr);
    
    try {
      TagNode dateNode = reviewNode.findElementByAttValue("class", "value-title", true, false);
      review.date = sd.parse(dateNode.getAttributeByName("title"));
      review.timeIsReal = true;
    } catch (Exception e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
      review.date = null;
    }
    
    addSimpleItem(review);
  }
  
  @Override
  public Integer getNativeMax() {
    return MAX_SCORE;
  }
  
}
