/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: JOrge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class SpaziosimSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(SpaziosimSegmenter.class);
  
  private static final SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy", Locale.ITALY);
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    HtmlCleaner cleaner = createCleaner();
    
    TagNode root = null;
    String site = null;
    if (url != null) {
      site = url.toString();
    }
    root = cleaner.clean(content);
    
    // /////////// Main Details Item ////////////
    int position = 1;
    TagNode reviewRoot[] = root.getElementsByAttValue("id", "forumposts", true, false);
    if (reviewRoot != null && reviewRoot.length > 0) {
      TagNode rev[] = reviewRoot[0].getElementsHavingAttribute("action", false);
      TagNode threads[] = rev[0].getAllElements(false);
      
      for (TagNode t : threads) {
        
        String id_post = removeWhiteSpaces(t.getAttributeByName("class"));
        if (id_post != null && id_post.trim().startsWith("windowbg")) {
          
          SimpleItem item = new SimpleItem();
          
          // / ThreadId
          TagNode nodeThread = t.findElementByAttValue("class", "inner", true, false);
          if (nodeThread != null) {
            item.threadId = nodeThread.getAttributeByName("id");
          } else {
            continue;
          }
          
          // Title
          TagNode nodeTitle = t.findElementByAttValue("id", "subject_" + item.threadId.substring(4), true, false);
          if (nodeTitle != null) {
            item.title = removeWhiteSpaces(nodeTitle.findElementByName("a", true).getText().toString());
          }
          
          // / Author
          TagNode nodeAuthor = t.findElementByAttValue("class", "poster", true, false);
          if (nodeAuthor != null) {
            item.author = removeWhiteSpaces(nodeAuthor.findElementByName("h4", true).findElementByName("a", true).getText().toString());
          }
          
          // / Body
          TagNode nodeBody = t.findElementByAttValue("class", "post", true, false);
          if (nodeBody != null) {
            item.body = removeWhiteSpaces(nodeBody.findElementByAttValue("id", item.threadId, true, false).getText().toString());
          }
          
          // / Date
          try {
            TagNode nodeDate = t.findElementByAttValue("class", "postarea", true, false);
            if (nodeDate != null) {
              String datePost = removeWhiteSpaces(nodeDate.findElementByAttValue("class", "smalltext", true, false).getText().toString());
              // Aprile 26, 2013
              String dateFirstPart = datePost.substring(datePost.lastIndexOf("il:") + 1).substring(3);
              String dateSecondPart = dateFirstPart.substring(0, dateFirstPart.length() - 15);
              Date parse = sdf.parse(dateSecondPart);
              item.date = parse;
            }
          } catch (Exception e) {
            logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
            item.date = null;
          }
          
          item.position = position;
          item.postUrl = url.toString();
          position++;
          
          items.add(item);
        }
      }
    }
    return items;
  }
}
