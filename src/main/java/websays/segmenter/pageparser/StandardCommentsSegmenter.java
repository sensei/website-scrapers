/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.io.StringReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class StandardCommentsSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(StandardCommentsSegmenter.class);
  
  public static SimpleDateFormat sd = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", new Locale("en", "US"));
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    DocumentBuilder db = dbf.newDocumentBuilder();
    Document doc = db.parse(new InputSource(new StringReader(content)));
    doc.getDocumentElement().normalize();
    
    int position = 1;
    String site = null;
    if (url != null) {
      site = url.toString();
    }
    NodeList nList = doc.getElementsByTagName("item");
    
    for (int temp = 0; temp < nList.getLength(); temp++) {
      Node nNode = nList.item(temp);
      if (nNode.getNodeType() == Node.ELEMENT_NODE) {
        Element eElement = (Element) nNode;
        SimpleItem simpleItem = new SimpleItem();
        String description = eElement.getElementsByTagName("description").item(0).getTextContent();
        simpleItem.body = StringEscapeUtils.unescapeHtml4(description);
        simpleItem.author = eElement.getElementsByTagName("dc:creator").item(0).getTextContent();
        try {
          String date = eElement.getElementsByTagName("pubDate").item(0).getTextContent();
          simpleItem.date = sd.parse(date);
          simpleItem.timeIsReal = true;
        } catch (Exception e) {
          logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
          simpleItem.date = null;
        }
        
        simpleItem.threadId = site.replaceFirst(".*/(\\d+)", "$1");
        simpleItem.position = position++;
        simpleItem.postUrl = site;
        simpleItem.isComment = true;
        items.add(simpleItem);
      }
    }
    return items;
  }
}
