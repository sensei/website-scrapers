/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class StandardSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(StandardSegmenter.class);
  
  private static final String SITE = "#standard";
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    HtmlCleaner cleaner = createCleaner();
    String discussionPage = "http://comments.us1.gigya.com/comments/rss/6148681/Comments/";
    
    TagNode root = null;
    String site = null;
    if (url != null) {
      site = url.toString();
    }
    root = cleaner.clean(content);
    
    // /////////// Main Details Item ////////////
    SimpleItem main = new SimpleItem();
    // ///// Extract Title
    main.title = getMetaContent(root, "og:title");
    main.body = getMetaContent(root, "og:description");
    main.date = null;
    main.position = 1;
    main.postUrl = site;
    
    try {
      String publishedTime = getMetaContent(root, "article:published_time");
      main.date = DatatypeConverter.parseDateTime(publishedTime).getTime();
      main.timeIsReal = true;
    } catch (Exception e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
      main.date = null;
    }
    
    TagNode author = root.findElementByAttValue("class", "authorName", true, false);
    
    main.author = removeWhiteSpaces(author.getText().toString());
    
    main.threadId = site.replaceFirst(".*-(\\d+).html", "$1");
    main.threadURLs = new ArrayList<String>();
    main.threadURLs.add(discussionPage + main.threadId + SITE);
    
    items.add(main);
    return items;
  }
  
  /**
   * @return the site
   */
  public static String getSite() {
    return SITE;
  }
}
