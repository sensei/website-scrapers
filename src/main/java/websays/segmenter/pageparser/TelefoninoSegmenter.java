/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: JOrge Valderrama
 * Contributors: Marco Martinez
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class TelefoninoSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(TelefoninoSegmenter.class);
  
  private static final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy',' hh:mm");
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    HtmlCleaner cleaner = createCleaner();
    
    TagNode root = null;
    String site = null;
    if (url != null) {
      site = url.toString();
    }
    root = cleaner.clean(content);
    
    // /////////// Main Details Item ////////////
    
    TagNode reviewRoot[] = root.getAllElements(true);
    // TagNode reviewRoot[] = root.getElementsByAttValue("id", "posts", true, false);
    // TagNode reviewRoot[] = root.getElementsByAttValue("class", "tborder", true, false);
    // TagNode reviewRoot[] = root.getElementsByAttValue("align", "left", true, false);
    // TagNode reviewRoot[] = root.getElementsByAttValue("class", "page", true, false);
    
    if (reviewRoot != null && reviewRoot.length > 0) {
      // TagNode rev[] = reviewRoot[0].getElementsByAttValue("align", "left", true, false);
      // TagNode rev[] = reviewRoot[0].getChildTags();
      // TagNode rev[] = reviewRoot[0].getAllElements(false);
      
      int position = 1;
      
      String nextPage = null;
      String title = null;
      for (TagNode tParent : reviewRoot) {
        try {
          String postNumber = null;
          SimpleItem item = new SimpleItem();
          
          if (tParent != null) {
            if (!tParent.getAttributes().isEmpty()) {
              if (tParent.hasAttribute("class")) {
                if (tParent.getAttributeByName("class").equals("pagenav") && nextPage == null) {
                  List elementListByAttValue = tParent.getElementListByAttValue("class", "vbmenu_control", true, false);
                  if (elementListByAttValue != null && elementListByAttValue.size() > 0) {
                    TagNode pagination = (TagNode) elementListByAttValue.get(0);
                    
                    // format Pagina X di Y
                    StringBuffer text = pagination.getText();
                    if (text != null) {
                      String string = text.toString();
                      String[] parts = string.split(" ");
                      if (parts.length == 4) {
                        try {
                          Integer actual = Integer.parseInt(parts[1]);
                          Integer all = Integer.parseInt(parts[3]);
                          if (actual < all) {
                            nextPage = "page=" + ++actual;
                          }
                        } catch (NumberFormatException e) {
                          logger.error(e);
                        }
                      }
                    }
                  }
                }
                
                if (tParent.getAttributeByName("class").equals("navbar") && title == null) {
                  
                  TagNode[] childTags = tParent.getChildTags();
                  if (childTags != null && childTags.length == 2) {
                    boolean contains = childTags[0].getAttributeByName("href").contains("showthread.php?t=");
                    if (contains && childTags[1].getName().equals("strong")) {
                      title = childTags[1].getText().toString();
                    }
                  }
                  
                  logger.trace("TagParent's text [" + tParent.getText().toString() + "]");
                  // System.out.println(tParent.getText().toString());
                  ;
                }
                
                if (tParent.getAttributeByName("class").equals("alt1") && tParent.hasAttribute("id")) {
                  if (tParent.getAttributeByName("id").startsWith("td_post_")) {
                    postNumber = tParent.getAttributeByName("id").substring(8);
                    
                    // Thread ID
                    item.threadId = postNumber;
                    item.hashKeyPrefix = postNumber;
                    
                    // Once identified the threadID goes to find the remain information about the item in all TagNode again
                    for (TagNode tson : reviewRoot) {
                      if (tson != null) {
                        if (!tson.getAttributes().isEmpty()) {
                          
                          // Body Message
                          if (tson.hasAttribute("id")) {
                            if (tson.getAttributeByName("id").equals("post_message_".concat(postNumber.toString()))) {
                              item.body = removeWhiteSpaces(tson.getText().toString());
                              // System.out.println(item.body);
                            }
                            // Author
                            else if (tson.getAttributeByName("id").equals("postmenu_".concat(postNumber.toString()))) {
                              item.author = tson.findElementByName("a", true).getText().toString();
                              // System.out.println(item.author);
                            }
                          }
                          
                          // Title
                          if (tson.hasAttribute("class")) {
                            if (tson.getAttributeByName("class").equals("smallfont")) {
                              if (tson.getParent().hasAttribute("id")) {
                                if (tson.getParent().getAttributeByName("id").equals("td_post_".concat(postNumber.toString()))) {
                                  TagNode titleNode = tson.findElementByName("strong", false);
                                  if (titleNode != null && (item.title == null || item.title.length() == 0)) {
                                    item.title = removeWhiteSpaces(titleNode.getText().toString());
                                  }
                                }
                              }
                            }
                          }
                          
                          // Date
                          try {
                            if (tson.hasAttribute("name")) {
                              if (tson.getAttributeByName("name").equals("post".concat(postNumber.toString()))) {
                                // item.date = new Date(tson.getText().toString());
                                String datePost = removeWhiteSpaces(tson.getParent().getText().toString());
                                Date parse;
                                if (datePost.toLowerCase().contains("ieri")) {
                                  Calendar cal = Calendar.getInstance();
                                  cal.add(Calendar.DATE, -1);
                                  parse = sdf.parse(sdf.format(cal.getTime()));
                                } else if ((datePost.toLowerCase().contains("oggi"))) {
                                  Calendar cal = Calendar.getInstance();
                                  cal.get(Calendar.DATE);
                                  parse = sdf.parse(sdf.format(cal.getTime()));
                                } else {
                                  parse = sdf.parse(datePost);
                                }
                                item.date = parse;
                                item.timeIsReal = true;
                              }
                            }
                          } catch (Exception e) {
                            logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage()
                                + "setting date to null");
                            item.date = null;
                          }
                          
                          // Image
                          
                        }
                      }
                    }
                    
                    item.position = position;
                    if (position == 1 && nextPage != null) {
                      item.threadURLs = new ArrayList<String>();
                      String externalForm = url.toExternalForm();
                      String[] split = externalForm.split("page=");
                      String urlFinal = "";
                      if (split.length == 1) {
                        urlFinal += externalForm + "&" + nextPage;
                      } else {
                        urlFinal = split[0] + nextPage;
                        // check others split parts
                        String finalPart = split[1];
                        int indexOf = finalPart.indexOf("&");
                        if (indexOf != -1) {
                          urlFinal += finalPart.substring(indexOf);
                        }
                      }
                      item.threadURLs.add(urlFinal);
                    }
                    item.postUrl = url.toString();
                    position++;
                    
                    if (item.title == null) {
                      item.title = title;
                    }
                    
                    items.add(item);
                  }
                }
              }
            }
          }
          
        } catch (Exception e) {
          logger.error(e.getMessage(), e.getCause());
        }
      }
    }
    
    return items;
  }
}
