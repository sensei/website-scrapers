/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import websays.segmenter.base.MinimalDocumentSegmenter;
import websays.types.clipping.SimpleItem;
import websays.utils.misc.DateUtilsWebsays;

/**
 * The Guardian Comments segmenter old HTML version can be found on SVN. This new ersion makes use of the The Guardian discussion-api that provides
 * JSON data. Example: http://discussion.theguardian.com/discussion-api/discussion//p/4h54t?orderBy=oldest&page=1&pageSize=10
 * 
 * @author marcpoch
 *
 */
public class TheGuardianCommentsSegmenter implements MinimalDocumentSegmenter {
  
  /**
   * 
   */
  private static final String GUARDIAN_API_KEY_PREFIX = "/p/";
  
  private static final int DEFAULT_COMMENT_POSITION = 2;
  
  private static final Logger logger = Logger.getLogger(TheGuardianCommentsSegmenter.class);
  
  String site = null;
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    
    if (url != null) {
      site = url.toString();
    }
    
    JSONParser parser = new JSONParser();
    JSONObject o = (JSONObject) parser.parse(content);
    
    Long currentPage = (Long) o.get("currentPage");
    Long pages = (Long) o.get("pages");
    
    JSONObject discussion = (JSONObject) o.get("discussion");
    String key = (String) discussion.get("key");
    String threadId = getThreadFromKey(key);
    Long commentCount = (Long) discussion.get("commentCount");
    Boolean isClosedForComments = (Boolean) discussion.get("isClosedForComments");
    JSONArray comments = (JSONArray) discussion.get("comments");
    
    if (logger.isTraceEnabled()) {
      logger.trace("key:[" + key + "] currentPage:[" + currentPage + "] pages:[" + pages + "] commentCount:[" + commentCount
          + "] isClosedForComments:[" + isClosedForComments + "] comments:[" + ((comments == null) ? "null" : comments.size()) + "] url:["
          + url + "]");
    }
    
    if (comments == null) {
      return items;
    }
    
    for (int i = 0; i < comments.size(); i++) {
      JSONObject comment = (JSONObject) comments.get(i);
      
      // Get comment
      SimpleItem simpleItem = parseComment(comment);
      items.add(simpleItem);
      
      // Json responses
      JSONArray responses = (JSONArray) comment.get("responses");
      if (logger.isTraceEnabled()) {
        logger.trace("item[" + simpleItem.APIObjectID + "] responses:      " + ((responses == null) ? "null" : responses.size()));
      }
      
      // Parse responses
      ArrayList<SimpleItem> responseItems = parseResponses(responses, simpleItem.APIObjectID, simpleItem.APIAuthorID);
      if (responseItems == null || responseItems.isEmpty()) {
        continue;
      }
      items.addAll(responseItems);
      
    }
    
    // Set simpleItem.threadURLs for first comment so next url with comments is fetched
    if (currentPage < pages) {
      SimpleItem firstItem = items.get(0);
      firstItem.threadURLs = new ArrayList<String>();
      firstItem.threadURLs.add(TheGuardianSegmenter.buildThreadURL(threadId, ++currentPage));
    }
    
    return items;
  }
  
  /**
   * @param key
   * @return
   */
  private String getThreadFromKey(String key) {
    if (key.startsWith(GUARDIAN_API_KEY_PREFIX)) {
      key = key.replaceFirst(GUARDIAN_API_KEY_PREFIX, "");
    }
    return key;
  }
  
  /**
   * @param responses
   * @param aPIAuthorID
   * @param aPIObjectID
   * @param simpleItem
   * @return
   */
  private ArrayList<SimpleItem> parseResponses(JSONArray responses, String parentAPIObjectID, String parentAPIAuthorID) {
    if (responses == null || responses.isEmpty()) {
      return null;
    }
    ArrayList<SimpleItem> responseItems = new ArrayList<SimpleItem>();
    
    for (int i = 0; i < responses.size(); i++) {
      JSONObject response = (JSONObject) responses.get(i);
      
      SimpleItem simpleItem = parseComment(response);
      simpleItem.parentID = parentAPIObjectID;
      simpleItem.APIToAuthorID = parentAPIAuthorID;
      if (logger.isTraceEnabled()) {
        logger.trace("item[" + simpleItem.APIObjectID + "] parentID:      " + simpleItem.parentID);
        logger.trace("item[" + simpleItem.APIObjectID + "] APIToAuthorID: " + simpleItem.APIToAuthorID);
      }
      responseItems.add(simpleItem);
    }
    
    return responseItems;
  }
  
  /**
   * @param comment
   * @return
   */
  private SimpleItem parseComment(JSONObject comment) {
    SimpleItem simpleItem = new SimpleItem();
    simpleItem.isComment = true;
    simpleItem.postUrl = site;
    simpleItem.APIObjectID = Long.toString((Long) comment.get("id"));
    simpleItem.body = parseBody((String) comment.get("body"));
    simpleItem.itemUrl = (String) comment.get("webUrl");
    simpleItem.date = DateUtilsWebsays.fromISO8601((String) comment.get("isoDateTime"));
    simpleItem.timeIsReal = true;
    simpleItem.position = DEFAULT_COMMENT_POSITION;
    
    JSONObject userProfile = (JSONObject) comment.get("userProfile");
    simpleItem.APIAuthorID = (String) userProfile.get("userId");
    simpleItem.author = (String) userProfile.get("displayName");
    
    if (logger.isTraceEnabled()) {
      logger.trace("item[" + simpleItem.APIObjectID + "] position:      " + simpleItem.position);
      logger.trace("item[" + simpleItem.APIObjectID + "] postUrl:       " + simpleItem.postUrl);
      logger.trace("item[" + simpleItem.APIObjectID + "] APIObjectID:   " + simpleItem.APIObjectID);
      logger.trace("item[" + simpleItem.APIObjectID + "] body:          " + simpleItem.body);
      logger.trace("item[" + simpleItem.APIObjectID + "] itemUrl:       " + simpleItem.itemUrl);
      logger.trace("item[" + simpleItem.APIObjectID + "] date:          " + simpleItem.date);
      logger.trace("item[" + simpleItem.APIObjectID + "] APIAuthorID:   " + simpleItem.APIAuthorID);
      logger.trace("item[" + simpleItem.APIObjectID + "] author:        " + simpleItem.author);
    }
    
    return simpleItem;
  }
  
  /**
   * Returns plain text: removes HTML tags and HTML escaped entities.
   * 
   * @param string
   * @return
   */
  private String parseBody(String content) {
    String ret;
    
    ret = content.replaceAll("\\<.*?>", " ");
    ret = StringEscapeUtils.unescapeHtml4(ret);
    
    // logger.trace("BODY ORI>>> " + content);
    // logger.trace("BODY RET>>> " + ret);
    
    return ret;
  }
}
