/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class TheGuardianSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(TheGuardianSegmenter.class);
  
  // old
  // private final static String DISCUSSION_PAGE = "http://discussion.theguardian.com/discussion/p/";
  // private final static String PARAMS_DISCUSSION = "?orderby=newest&per_page=50&commentpage=1&noposting=true&tab=all";
  
  // 2016-03-02 to be able to fetch comments from discussion-api
  private final static String DISCUSSION_PAGE = "http://discussion.theguardian.com/discussion-api/discussion//p/";
  private final static String PARAMS_DISCUSSION = "?orderBy=newest&pageSize=50&page=";
  private final static int DEFAULT_PAGE_START = 1;
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    HtmlCleaner cleaner = createCleaner();
    
    TagNode root = null;
    String site = null;
    if (url != null) {
      site = url.toString();
    }
    root = cleaner.clean(content);
    
    // /////////// Main Details Item ////////////
    SimpleItem main = new SimpleItem();
    // ///// Extract Title
    main.title = getMetaContent(root, "og:title");
    
    // parse body
    main.body = findNodeTextById(root, "article-body-blocks");
    if (main.body == null || main.body.trim().equals("")) {
      main.body = findNodeTextById(root, "articleBody");
    }
    
    main.date = null;
    main.position = 1;
    main.postUrl = site;
    
    try {
      String modifiedTime = getMetaContent(root, "article:modified_time");
      main.date = DatatypeConverter.parseDateTime(modifiedTime).getTime();
      main.timeIsReal = true;
    } catch (Exception e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
      main.date = null;
    }
    
    // parse author
    TagNode[] authors = root.getElementsByAttValue("rel", "author", true, false);
    main.author = joinTagNodes(authors);
    
    // parse authorLocation
    TagNode authorDetail = root.findElementByAttValue("class", "contributor-full", true, false);
    if (authorDetail != null) {
      String[] splitLocation = authorDetail.getText().toString().split(" in ");
      if (splitLocation.length == 2) {
        main.authorLocation = removeWhiteSpaces(splitLocation[1]);
      }
    }
    
    // parse threadId
    TagNode threadId = root.findElementByAttValue("rel", "shorturl", true, false);
    if (threadId != null) {
      main.threadId = removeWhiteSpaces(threadId.getAttributeByName("href")).replace("http://gu.com/p/", "");
    }
    if (main.threadId == null) {
      threadId = root.findElementByAttValue("data-link-name", "View all comments", true, false);
      if (threadId != null) {
        main.threadId = removeWhiteSpaces(threadId.getAttributeByName("href")).replace("http://www.theguardian.com/discussion/p/", "");
      }
    }
    
    main.threadURLs = new ArrayList<String>();
    
    if (main.threadId != null && !main.threadId.isEmpty()) {
      main.threadURLs.add(buildThreadURL(main.threadId, DEFAULT_PAGE_START));
    }
    
    items.add(main);
    return items;
  }
  
  /**
   * @param threadId
   * @param defaultPageStart
   * @return
   */
  public static String buildThreadURL(String threadId, long page) {
    String threadURL = DISCUSSION_PAGE + threadId + PARAMS_DISCUSSION + page;
    return threadURL;
  }
  
  private String joinTagNodes(TagNode... tagNodes) {
    StringBuilder builder = new StringBuilder();
    for (TagNode tagNode : tagNodes) {
      if (builder.length() > 0) {
        builder.append(" and ");
      }
      builder.append(tagNode.getText());
    }
    return builder.toString();
  }
  
  @Override
  protected String findNodeTextById(TagNode root, String value) {
    String body = new String();
    TagNode bodyTag = root.findElementByAttValue("id", value, true, false);
    if (bodyTag == null) {
      bodyTag = root.findElementByAttValue("itemprop", value, true, false);
    }
    if (bodyTag != null) {
      TagNode[] paragraphs = bodyTag.getElementsByName("p", false);
      if (paragraphs != null && paragraphs.length > 0) {
        for (TagNode tagNode : paragraphs) {
          body = body.concat(tagNode.getText().toString() + " ");
        }
      } else {
        TagNode bodyTag2Option = bodyTag.getAllElements(true)[6];
        if (bodyTag2Option != null) {
          TagNode[] paragraphs2 = bodyTag2Option.getElementsByName("p", false);
          if (paragraphs2 != null && paragraphs2.length > 0) {
            for (TagNode tagNode : paragraphs2) {
              body = body.concat(tagNode.getText().toString() + " ");
            }
          }
        }
      }
    }
    return body;
  }
}
