/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.segmenter.common.HTMLEntitiesConverter;
import websays.types.clipping.SimpleItem;

public class TripAdvisorSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(TripAdvisorSegmenter.class);
  
  public static SimpleDateFormat sdEng = new SimpleDateFormat("MMMM dd, yyyy", Locale.ENGLISH);
  public static SimpleDateFormat sdEng2 = new SimpleDateFormat("d MMMM yyyy", Locale.ENGLISH);
  public static SimpleDateFormat sdSpn = new SimpleDateFormat("d MMMM yyyy", new Locale("es", "ES"));
  public static SimpleDateFormat sdIta = new SimpleDateFormat("d MMMM yyyy", new Locale("it", "IT"));
  public static SimpleDateFormat sdISO8601 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", new Locale("es", "ES"));
  // 2015-07-14 T 01:14:44-0400
  public static SimpleDateFormat sdReview = new SimpleDateFormat("yyyy-MM-dd", new Locale("es", "ES")); // delete if not used
  
  private static final Integer MAX_SCORE = 5;
  
  /**
   * Label of the fake query parameter that websays injects in the tripadvisor expanded comments urls to know which was the comments original page.
   */
  private static final String URL_ORIG_LABEL = "orig_url";
  
  /**
   * Used as a base of the threadIds for tripadvisor conversations.
   */
  // public static String TRIPADVISOR_BASE_THREAD_ID = "tripadvisor.general";
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    SimpleItem main = new SimpleItem();
    
    TagNode root = null;
    String site = null;
    String baseURL = null; // This is protocol + "://" + host + ":" + port
    if (url != null) {
      site = url.toString();
      
      baseURL = url.getProtocol() + "://" + url.getHost();
      if (url.getPort() != -1) {
        baseURL = baseURL + ":" + url.getPort();
      }
    }
    root = getRootNode(content);
    
    String pageType = getPageType(root, site);
    SimpleItem fakeFather = null;
    switch (pageType) {
    
    /* This is the "main" facility page, it has the facility description and comments */
      case "v:Review-aggregate":
        if (logger.isTraceEnabled()) {
          logger.trace("Parsing a facility page");
        }
        main = getMainInfoFromMainPage(root);
        main.isComment = false;
        main.postUrl = site;
        // main.threadId = TRIPADVISOR_BASE_THREAD_ID + url.getPath();
        main.threadId = canonicalizeURL(url.toString());
        main.itemUrl = main.threadId;
        // items.add(main);
        // items.addAll(getCommentsFromMainPage(root, site, baseURL, main));
        items = getCommentsFromMainPage(root, site, baseURL, main);
        break;
      /* This is a particular comment page. The only interesting information is the comment itself. */
      case "Review":
        if (logger.isTraceEnabled()) {
          logger.trace("Parsing a comment page. Only main page's comment will be parsed.");
        }
        fakeFather = new SimpleItem();
        fakeFather.fakeItem = true;
        fakeFather.date = null;
        fakeFather.timeIsReal = false;
        fakeFather.position = 1;
        fakeFather.threadId = canonicalizeURL(baseURL + getMainLink(root));
        fakeFather.postUrl = fakeFather.threadId;
        fakeFather.itemUrl = fakeFather.threadId;
        logger.trace("Fakefather url [" + fakeFather.postUrl + "]");
        fakeFather.isComment = false;
        
        items.add(fakeFather);
        items.add(this.getMainInfoFromCommentPage(root, site, baseURL));
        break;
      case "ExpandedReview":
        logger.warn("This never should be run. This is old code superseded by Review.");
        /*
         * if (logger.isTraceEnabled()) { logger.trace("Parsing an expanded comment page. Only a comment will be parsed"); } fakeFather = new
         * SimpleItem(); fakeFather.fakeItem = true; fakeFather.date = null; fakeFather.timeIsReal = false; fakeFather.position = 1;
         * fakeFather.threadId = canonicalizeURL(getThreadIdFromURL(site)); fakeFather.postUrl = fakeFather.threadId; fakeFather.itemUrl =
         * fakeFather.threadId; logger.trace("Fakefather url [" + fakeFather.postUrl + "]"); fakeFather.isComment = false;
         * 
         * items.add(fakeFather); // items.addAll(this.extractReviewData(fakeFather.threadId, baseURL, items, 2, fakeFather.threadId, root)); items =
         * this.extractReviewData(fakeFather.threadId, baseURL, items, 2, fakeFather.threadId, root);
         */
        break;
      case "":
      case "LodgingBusiness":
      case "FoodEstablishment":
      default:
        logger.warn("Page type [" + pageType + "]. A general method to get page's info will be used.");
        items = getItemsFromUnknown(root, site, baseURL, canonicalizeURL(url.toString()));
        break;
    }
    
    /* This step is common to any page */
    if (items != null && items.size() > 0) {
      List<String> moreCommentsURL = getThreadURLs(baseURL, root);
      
      if (moreCommentsURL != null && moreCommentsURL.size() > 0) {
        if (items.get(0).threadURLs == null) {
          items.get(0).threadURLs = moreCommentsURL;
        } else {
          /* The threadURLs could be plenty of long Comment's urls, they are preserved and the next page URL added to the list. */
          items.get(0).threadURLs.addAll(moreCommentsURL);
        }
      }
    }
    
    return items;
  }
  
  /**
   * Canonicalizes a list of URLs
   * 
   * @param moreCommentsURL
   *          the list of URLs to canonicalize
   * @return a list of the same size of moreCommentsURL with all urls canonicalized. Null if moreCommentsURL was null
   */
  private List<String> canonicalizeURLList(List<String> moreCommentsURL) {
    if (moreCommentsURL == null) {
      return null;
    }
    
    ArrayList<String> res = new ArrayList<String>();
    
    for (String currURL : moreCommentsURL) {
      res.add(canonicalizeURL(currURL));
      
    }
    
    return res;
  }
  
  /**
   * Given an expanded comment site, returns the original url where the short comment is.
   * 
   * @param site
   *          the url of the expanded comment site
   * @return the original url for the comment or null if not found
   */
  private String getThreadIdFromURL(String site) {
    String res = null;
    if (site != null) {
      String[] siteComps = site.split(URL_ORIG_LABEL + "=");
      if (siteComps != null && siteComps.length > 1) {
        res = siteComps[1];
      }
    }
    if (logger.isTraceEnabled()) {
      logger.trace("ThreadID [" + res + "] got from expanded comment url querystring [" + site + "]");
    }
    return res;
  }
  
  @Override
  public Integer getNativeMax() {
    return MAX_SCORE;
  }
  
  /**
   * Looks for "next" url in the page and returns it for the crawler to crawl over all comments.
   * 
   * @param parBaseURL
   *          protocol + host + port for the url where the original page is (i.e. http://www.tripadvisor.es:80)
   * @param parRoot
   *          root node of the page
   * @return a list with only 1 element which is the "next" page of the segmented page. Or null if it founds no link.
   */
  private List<String> getThreadURLs(String parBaseURL, TagNode parRoot) {
    List<String> threadURLs = null;
    
    TagNode tempNode[] = parRoot.getElementsByAttValue("class", "nav next rndBtn rndBtnGreen taLnk", true, false);
    
    if (tempNode != null && tempNode.length > 0) {
      String pathToNext = tempNode[0].getAttributeByName("href");
      if (pathToNext != null && !"".equalsIgnoreCase(pathToNext)) {
        threadURLs = new ArrayList<String>();
        String newItem = parBaseURL + pathToNext;
        logger.trace("Adding next comments page [" + newItem + "] to fetching queue.");
        threadURLs.add(newItem);
      }
    }
    
    return threadURLs;
  }
  
  /**
   * Gets the main node of the page when the page is of type Review (ShowUserReviews)
   * 
   * @param parRoot
   *          the root node of the page to segment.
   * @param parSite
   *          the textual url of the site being segmented.
   * @return a node containing the information retrieved.
   */
  private SimpleItem getMainInfoFromCommentPage(TagNode parRoot, String parSite, String parBaseURL) {
    SimpleItem main = new SimpleItem();
    
    if (parRoot == null) {
      logger.warn("Root document node is null. Returning empty SimpleItem");
      return main;
    }
    
    TagNode reviewRoot[] = parRoot.getElementsByAttValue("class", "  reviewSelector ", true, false);
    
    TagNode tempNode[] = null;
    // ///// Extract Title
    tempNode = parRoot.getElementsByAttValue("property", "name", true, false);
    if (tempNode != null && tempNode.length > 0) {
      String text = null;
      /* This is a search for the correct tag that contains comment's title */
      for (TagNode currentNode : tempNode) {
        if ("quote".equals(currentNode.getAttributeByName("class"))) {
          /* As a search, when we find what we want it ends */
          text = currentNode.getText().toString();
          break;
        }
      }
      main.title = removeWhiteSpaces(text);
      if (logger.isTraceEnabled()) {
        logger.trace("main.title before html entities converting[" + text + "] and after [" + main.title + "]");
      }
    } else {
      logger.warn("There is no node with [property=\"name\"]. Maybe the parser should be updated. \"main.title\" will be null.");
    }
    
    // //////////// Extract Body
    tempNode = parRoot.getElementsByAttValue("property", "reviewBody", true, false);
    if (tempNode != null && tempNode.length > 0) {
      main.body = HTMLEntitiesConverter.unHTMLEntities(removeWhiteSpaces(tempNode[0].getText().toString()));
    } else {
      logger.warn("There is no node with [property=\"reviewBody\"]. Maybe the parser should be updated. \"main.body\" will be null.");
    }
    
    // Extract author
    tempNode = parRoot.getElementsByAttValue("property", "author", true, false);
    if (tempNode != null && tempNode.length > 0) {
      main.author = tempNode[0].getText().toString();
    }
    
    if (main.author == null || main.author.trim().equals("")) {
      logger.warn("the node with \"property=author\" not exists or is empty. Looking for a node with class \"username mo\"");
      main.author = removeWhiteSpaces(parRoot.getElementsByAttValue("class", "username mo", true, false)[0].getText().toString());
      if (main.author == null || main.author.trim().equals("")) {
        logger.warn("Author not found in the node with \"property=author\" not in node with class \"username mo\". Author will be null.");
      }
      
    }
    
    // Extract authorLocation
    tempNode = parRoot.getElementsByAttValue("class", "location", true, false);
    if (tempNode != null && tempNode.length > 0) {
      main.authorLocation = tempNode[0].getText().toString();
    } else {
      logger
          .warn("There is no node with [class=\"location\"] in the document. Maybe the parser should be updated. \"main.authorLocation\" will be null.");
    }
    
    // ////////////// Extract Score
    tempNode = parRoot.getElementsByAttValue("property", "ratingValue", true, false);
    if (tempNode != null && tempNode.length > 0) {
      String score = tempNode[0].getAttributeByName("content");
      try {
        main.setScores(Float.parseFloat(score), MAX_SCORE);
      } catch (NumberFormatException nfe) {
        logger.warn("Getting comment's score failed. Reason [" + nfe.getMessage() + "]. Comment's score will be null.");
      }
      
    } else {
      logger.warn("There is no node with [property=\"ratingValue\"]. Maybe the parser should be updated. \"main.score\" will be null.");
    }
    
    // ////////////// Extract Date
    tempNode = parRoot.getElementsByAttValue("property", "datePublished", true, false);
    if (tempNode != null && tempNode.length > 0) {
      String text = null;
      try {
        text = tempNode[0].getAttributeByName("content");
        main.date = sdReview.parse(text);
        main.timeIsReal = false;
      } catch (ParseException e) {
        logger.warn("Error [" + e.getMessage() + "] parsing date [" + text + "]");
      }
    } else {
      logger.warn("There is no node with [property=\"ratingDate\"]. Maybe the parser should be updated. \"main.date\" will be null.");
    }
    main.position = 2;
    main.isComment = true;
    // Not needed because the core system will build the threadID from postURL. Use threadID whenever you have a value that could act as it (i.e.
    // facebook's threadId number)
    // main.threadId = parBaseURL + getMainLink(parRoot);
    main.postUrl = main.threadId;
    main.itemUrl = canonicalizeURL(parSite);
    
    return main;
  }
  
  /**
   * Tries to get the link to the main page when processing a comments page
   * 
   * @param parRoot
   *          the root node of the page
   */
  private String getMainLink(TagNode parRoot) {
    String res = null;
    TagNode[] tempNode;
    TagNode[] tempChildNodes;
    // Extract link to main facility page
    tempNode = parRoot.getElementsByAttValue("class", "altHeadInline", true, false);
    if (tempNode != null && tempNode.length > 0) {
      tempChildNodes = tempNode[0].getElementsByName("a", true);
      if (tempChildNodes != null && tempChildNodes.length > 0) {
        // res = TRIPADVISOR_BASE_THREAD_ID + tempChildNodes[0].getAttributeByName("href");
        res = tempChildNodes[0].getAttributeByName("href");
      } else {
        logger
            .warn("Node with [class=\"altHeadInline\"] has not children. Maybe the parser should be updated. \"main.threadId\" will be null.");
      }
    } else {
      logger
          .warn("There is no node with [class=\"altHeadInline\"] in the document. Maybe the parser should be updated. \"main.threadId\" will be null.");
    }
    if (logger.isTraceEnabled()) {
      logger.trace("Main's page link is [" + res + "]");
    }
    return res;
  }
  
  /**
   * Gets the main node of the page when the page is of type Review-aggregate (Restaurant_Review)
   * 
   * @param parRoot
   *          the root node of the page to segment.
   * @return a node containing the information retrieved.
   */
  private SimpleItem getMainInfoFromMainPage(TagNode parRoot) {
    SimpleItem main = new SimpleItem();
    
    if (parRoot == null) {
      logger.warn("Root document node is null. Returning empty SimpleItem");
      return main;
    }
    
    TagNode tempNode[];
    // Extract author
    /*
     * tempNode[] = parRoot.getElementsByAttValue("property", "v:reviewer", true, false); TagNode tempChildNode[]; if (tempNode != null &&
     * tempNode.length > 0) { tempChildNode = tempNode[0].getChildTags(); if (tempChildNode != null && tempChildNode.length > 0) { main.author =
     * tempChildNode[0].getText().toString(); } else { logger
     * .warn("Node with [property=\"v:reviewer\"] has not children. Maybe the parser should be updated. \"main.author\" will be null.");
     * 
     * } } else { logger
     * .warn("There is no node with [property=\"v:reviewer\"] in the document. Maybe the parser should be updated. \"main.author\" will be null."); }
     */
    
    // ///// Extract Title
    tempNode = parRoot.getElementsByAttValue("rel", "v:name", true, false);
    if (tempNode != null && tempNode.length > 0) {
      main.title = removeWhiteSpaces(getNodeInnerText(tempNode[0]));
    } else {
      logger.warn("There is no node with [class=\"v:name\"]. Maybe the parser should be updated. \"main.title\" will be null.");
    }
    
    // //////////// Extract Body
    tempNode = parRoot.getElementsByAttValue("rel", "v:address", true, false);
    TagNode temNode2[] = parRoot.getElementsByAttValue("class", "tabs_descriptive_text", true, false);
    if (tempNode != null && tempNode.length > 0) {
      main.body = HTMLEntitiesConverter.unHTMLEntities(removeWhiteSpaces(tempNode[0].getText().toString()));
    } else {
      logger.warn("There is no node with [property=\"v:address\"]. Maybe the parser should be updated.");
    }
    
    if (temNode2 != null && temNode2.length > 0) {
      main.body = main.body + "\n" + HTMLEntitiesConverter.unHTMLEntities(removeWhiteSpaces(temNode2[0].getText().toString()));
    } else {
      logger.warn("There is no node with [class=\"tabs_descriptive_text\"]. Maybe the parser should be updated.");
    }
    
    if (logger.isTraceEnabled()) {
      logger.trace("main.body [" + main.body + "]");
    }
    
    // //////////// Extract Reviews
    tempNode = parRoot.getElementsByAttValue("property", "v:count", true, false);
    if (tempNode != null && tempNode.length > 0) {
      String reviews = removeWhiteSpaces(tempNode[0].getText().toString());
      try {
        main.reviews = Integer.parseInt(reviews);
      } catch (NumberFormatException nfe) {
        logger.warn("Exception [" + nfe.getMessage() + "] trying to parse number [" + reviews + "]. \"main.reviews\" will be null.");
      }
    } else {
      logger.warn("There is no node with [property=\"v:count\"]. Maybe the parser should be updated. \"main.reviews\" will be null.");
    }
    
    // ////////////// Extract Score
    tempNode = parRoot.getElementsByAttValue("property", "v:average", true, false);
    if (tempNode != null && tempNode.length > 0) {
      String score = tempNode[0].getAttributeByName("content");
      if (score != null) {
        main.setScores(Float.parseFloat(score), MAX_SCORE);
      } else {
        logger
            .warn("There is no attribute [\"content\"] in the node with [property=\"v:average\"]. Maybe the parser should be updated. \"main.score\" will be null.");
      }
    } else {
      logger.warn("There is no node with [property=\"v:average\"]. Maybe the parser should be updated. \"main.score\" will be null.");
    }
    
    /* As a restaurant or hotel description it has not a defined date, so the current date is used. */
    main.date = new Date();
    main.timeIsReal = true;
    main.position = 1;
    main.itemUrl = null;
    
    return main;
  }
  
  /**
   * Given a page it gets the type of page in order to make an educated segmenting
   * 
   * @param site
   *          the textual URL of the content to be processed.
   * @param root
   *          the node root of the page
   * @return the type of the page, if not found it returns null.
   */
  private String getPageType(TagNode root, String site) {
    String res = "";
    
    if (site != null && site.contains("ExpandedUserReviews")) {
      res = "ExpandedReview";
    } else {
      
      List<TagNode> nodes = root.getElementListByAttValue("id", "PAGE", true, false);
      if (nodes != null) {
        if (nodes.size() == 1) {
          /* Ok: the page has only one type identifier */
          res = nodes.get(0).getAttributeByName("typeof");
        } else if (nodes.size() == 0) {
          /* Ko: the page has no type identifier. null is returned */
          logger.warn("No node with attribute [id=\"PAGE\"] found. Maybe the page structure has changed. The return value will be [" + res
              + "]");
        } else {
          logger.warn("Multiple nodes  with attribute [id=\"PAGE\"] found. The return value will be the one in the first node.");
          res = nodes.get(0).getAttributeByName("typeof");
        }
        
      } else {
        /* Ko: the page has no type identifier. null is returned */
        logger.warn("No node with attribute [id=\"PAGE\"] found. Maybe the page structure has changed. The return value will be [" + res
            + "]");
      }
      
      if (logger.isTraceEnabled()) {
        logger.trace("Returning pagetype of [" + res + "]");
      }
    }
    return res;
    
  }
  
  /**
   * Gets a root node and a string that is a site URL, extracts the information in the document accessing it via root TagNode and returns a list of
   * simple items with the information scraped from the document.
   * 
   * @param root
   *          a tagnode which is the root of the html document to be scraped.
   * @param site
   *          the textual form of the URL from which the document comes.
   * @param parBaseURL
   *          the base (protocol + host + port) part of the URL to be processed.
   * @return a list of SimpleItem objects containing the info scraped from the document.
   */
  private ArrayList<SimpleItem> getItemsFromUnknown(TagNode root, String site, String parBaseURL, String parThreadID) {
    
    SimpleItem main = new SimpleItem();
    ArrayList<SimpleItem> res = new ArrayList<SimpleItem>();
    
    // ///// Extract Title
    try {
      TagNode[] n = root.getElementsByAttValue("id", "HEADING", true, false);
      String title = removeWhiteSpaces(n[0].getText().toString());
      main.title = title;
    } catch (Exception ex) {}
    // //////////// Extract Body
    try {
      
      TagNode temNode2[] = root.getElementsByAttValue("class", "tabs_descriptive_text", true, false);
      TagNode[] n = root.getElementsByAttValue("class", "format_address", true, false);
      String body = HTMLEntitiesConverter.unHTMLEntities(removeWhiteSpaces(n[0].getText().toString()));
      if (temNode2 != null && temNode2.length > 0) {
        body = body + "\n" + HTMLEntitiesConverter.unHTMLEntities(removeWhiteSpaces(temNode2[0].getText().toString()));
      } else {
        // logger.warn("There is no node with [class=\"tabs_descriptive_text\"]. Maybe the parser should be updated.");
      }
      main.body = body;
    } catch (Exception ex) {}
    // //////////// Extract Reviews
    try {
      TagNode[] n = root.getElementsByAttValue("class", "reviews_header", true, false);
      String reviews = removeWhiteSpaces(n[0].getText().toString());
      Integer rev = parseNumbers(reviews, true);
      if (rev == null) {
        rev = parseNumbers(reviews, false);
      }
      main.reviews = rev;
    } catch (Exception ex) {}
    // ////////////// Extract Score
    try {
      TagNode[] n = root.getElementsByAttValue("class", "sprite-ratings", true, false);
      TagNode sc = null;
      for (int i = 0; i < n.length; i++) {
        try {
          if (n[i].getAttributeByName("property").trim().equalsIgnoreCase("v:average")) {
            sc = n[i];
            break;
          }
        } catch (Exception e) {}
      }
      
      if (sc == null) {
        logger.warn("Way 1 to get main score didn't work. Using way 2.");
        n = root.getElementsByAttValue("property", "ratingValue", true, false);
        if (n != null && n.length > 0) {
          
          TagNode tempRating = n[0];
          String stringRate = tempRating.getAttributeByName("content");
          try {
            main.setScores(Float.parseFloat(stringRate), MAX_SCORE);
          } catch (NumberFormatException nfe) {
            logger.warn("Way 2 to get main score failed either. Reason [" + nfe.getMessage() + "]. Main score will be null.");
          }
          
        } else {
          logger.warn("Way 2 to get main score failed either. Main score will be null.");
        }
        
      } else {
        String score = removeWhiteSpaces(sc.getAttributeByName("content"));
        main.setScores(Float.parseFloat(score), MAX_SCORE);
        logger.trace("Native Score [" + main.getScore_native() + "] and websense score [" + main.getScore_websays()
            + "] succesfully parsed using method 1.");
      }
    } catch (Exception ex) {}
    /* There is no date for this element at any tripadvisor page, date will be null */
    main.date = null;
    main.timeIsReal = false;
    main.position = 1;
    /* Get rid of the pagination offset in the url */
    main.postUrl = canonicalizeURL(site);
    logger.trace("Canonical post url (without pagination information and trailing groups.) [" + main.postUrl + "]");
    main.isComment = false;
    main.threadId = parThreadID;
    
    // res.add(main);
    // res.addAll(getCommentsFromMainPage(root, site, parBaseURL, parThreadID));
    res = getCommentsFromMainPage(root, site, parBaseURL, main);
    
    return res;
  }
  
  /**
   * Given a tripadvisor url canonicalizes it. <li>gets rid of the trailing (#.*) group</li> <li>if it has offset information it strips it</li>
   * 
   * @param parSite
   *          the site that is going to be canonicalized
   * @return the canonicalized form of the site
   */
  private String canonicalizeURL(String parSite) {
    String res = parSite.replaceAll("Reviews-or[0-9]{2,}-", "Reviews-");
    int trailIndex = res.indexOf('#');
    if (trailIndex != -1) {
      res = res.substring(0, trailIndex);
    }
    return res;
  }
  
  /**
   * Given a facility (hotel, restaurant) main page, gets the comments
   * 
   * @param parRoot
   *          the root node of the page to be processed.
   * @param parSite
   *          the textual form of the URL of the page to be processed.
   * @param parBaseURL
   *          the base (protocol + hostname + port) of the URL of the page to be processed.
   * @param main
   *          The father comment, needed to add threadURLs for pagination or getting expanded comments.
   * @return An array of simpleItem objects containing a comment each.
   */
  private ArrayList<SimpleItem> getCommentsFromMainPage(TagNode parRoot, String parSite, String parBaseURL, SimpleItem main) {
    
    ArrayList<SimpleItem> res = new ArrayList<SimpleItem>();
    // ///////////////// Comment Items
    int position = 2;
    
    String mainThread = main.threadId;
    
    /* If the threadID does not come from the call, try to calculate it using the info in the page */
    if (mainThread == null) {
      mainThread = parBaseURL + getMainLink(parRoot);
    }
    
    TagNode reviewRoot[] = parRoot.getElementsByAttValue("id", "REVIEWS", true, false);
    if (reviewRoot != null && reviewRoot.length > 0) {
      TagNode rev[] = reviewRoot[0].getAllElements(false);
      
      for (TagNode t : rev) {
        
        String val = removeWhiteSpaces(t.getAttributeByName("id"));
        if (val != null && (val.trim().startsWith("review") || val.trim().startsWith("UR"))) {// single
          // review
          // area
          
          List<TagNode> moreCommentNodes = t.getElementListByAttValue("class", "partnerRvw", true, false);
          if (moreCommentNodes != null && moreCommentNodes.size() > 0) {
            
            // SimpleItem item = new SimpleItem();
            
            // The comment is long, so getting it from another page is needed. The long comment page URL is build here.
            String commentID = val.replaceAll("review_", "");
            commentID = commentID.replaceAll("UR", "");
            
            if (main.threadURLs == null) {
              main.threadURLs = new ArrayList<String>();
            }
            
            String longCommentURL = getItemUrlFromComment(parBaseURL, t, getTitleFromComment(t));
            
            // String longCommentURL = buildLongCommentURL(commentID, mainThread);
            if (logger.isTraceEnabled()) {
              logger.trace("Processing comment [" + commentID + "] Which is long. The URL for the complete comment is [" + longCommentURL
                  + "]");
            }
            main.threadURLs.add(longCommentURL);
            
            /*
             * item.fakeItem = true; item.threadURLs = new ArrayList<String>(); item.date = null; item.timeIsReal = false; item.position = position++;
             * item.isComment = false; item.postUrl = parSite; item.title = getTitleFromComment(t); item.itemUrl = getItemUrlFromComment(parBaseURL,
             * t, item.title); String longCommentURL = buildLongCommentURL(commentID, mainThread); item.threadURLs.add(longCommentURL); res.add(item);
             */
          } else {
            // The comment is short enough to not need a "more" link
            
            res = extractReviewData(parSite, parBaseURL, res, position++, mainThread, t);
          }
        }
      }
    }
    res.add(0, main);
    return res;
  }
  
  /**
   * @param parSite
   * @param parBaseURL
   * @param res
   * @param position
   * @param mainThread
   * @param t
   * @param item
   * @return
   */
  private ArrayList<SimpleItem> extractReviewData(String parSite, String parBaseURL, ArrayList<SimpleItem> res, int position,
      String mainThread, TagNode t) {
    SimpleItem item = new SimpleItem();
    // ////// Body
    try {
      item.body = HTMLEntitiesConverter.unHTMLEntities(removeWhiteSpaces(t.getElementsByAttValue("class", "entry", true, false)[0]
          .findElementByName("p", true).getText().toString()));
    } catch (Exception e) {}
    
    // ///// Title
    item.title = getTitleFromComment(t);
    
    // /////// author
    try {
      item.author = removeWhiteSpaces(t.getElementsByAttValue("class", "username mo", true, false)[0].getText().toString());
    } catch (Exception e) {
      logger.warn("Error [" + e.getMessage() + "] trying to get \"item.author\" ");
      
    }
    // /////// author location
    try {
      item.authorLocation = removeWhiteSpaces(t.getElementsByAttValue("class", "location", true, false)[0].getText().toString());
    } catch (Exception e) {}
    // /////// reviews (reviews for comments is nonsense)
    /*
     * try { String reviews = removeWhiteSpaces(t.getElementsByAttValue("class", "badgeText", true, false)[0].getText().toString()); Integer re =
     * parseNumbers(reviews, true); item.reviews = re; } catch (Exception e) {}
     */
    // /////// votes
    try {// class="helpfulVotesBadge badge no_cpu"
      TagNode tg[] = t.getElementsByAttValue("class", "helpfulVotesBadge badge no_cpu", true, false);
      String votes = removeWhiteSpaces(tg[0].getText().toString());
      // System.out.print("Votes : " + votes);
      Integer vt = parseNumbers(votes, true);
      // System.out.println(" Votes : " + vt);
      item.votes = vt;
    } catch (Exception e) {}
    
    // /////// date
    
    item.date = extractDate(t);
    item.timeIsReal = false;
    
    // /////// score
    try {
      String score = removeWhiteSpaces(t.getElementsByAttValue("class", "sprite-ratings", true, false)[0].getAttributeByName("content")
          .toString());
      item.setScores(Float.parseFloat(score), MAX_SCORE);
      if (logger.isTraceEnabled()) {
        logger.trace("Comment's native score [" + item.getScore_native() + "] and websays score [" + item.getScore_websays()
            + "] succesfully parsed using method 1.");
      }
    } catch (Exception e) {
      logger.warn("Error trying to get a comment's score. A second method will be tried.");
      /* Second way to get the comment's score. Newer page's design. */
      TagNode[] rating = t.getElementsByAttValue("class", "rate sprite-rating_s rating_s", true, false);
      if (rating != null && rating.length > 0) {
        TagNode[] children = rating[0].getChildTags();
        if (children != null && children.length > 0) {
          String childClass = children[0].getAttributeByName("class");
          if (childClass != null) {
            item.setScores(Integer.parseInt(childClass.substring((childClass.length() - 2), (childClass.length() - 1))), MAX_SCORE);
            if (logger.isTraceEnabled()) {
              logger.trace("Comment's native score [" + item.getScore_native() + "] and websays score [" + item.getScore_websays()
                  + "]  succesfully parsed using method 2.");
            }
          } else {
            logger
                .warn("Way 2 to get comment's score failed node's \"rate sprite-rating_s rating_s\" children have no class attribute. Comment's score will be null.");
          }
        } else {
          logger
              .warn("Way 2 to get comment's score failed because node \"rate sprite-rating_s rating_s\" have no children. Comment's score will be null.");
        }
      } else {
        logger.warn("Way 2 to get comment's score failed either. Comment's score will be null.");
      }
      
    }
    
    item.itemUrl = getItemUrlFromComment(parBaseURL, t, item.title);
    
    item.postUrl = parSite;
    if (item.postUrl == null) {
      logger.warn("This item [" + item.title + "] will have its postUrl to null.");
    }
    
    item.position = position;
    item.isComment = true;
    item.threadId = mainThread;
    
    if (passCommentTest(item)) {
      res.add(item);
    }
    return res;
  }
  
  /**
   * Given an html document in TagNode form, extracts its title.
   * 
   * @param t
   *          the html document where the comment's information is stored.
   * @return the comment's title or null if there is no title inside the document or the document is not a compatible comment.
   */
  private String getTitleFromComment(TagNode t) {
    
    String res = null;
    try {
      String ti = removeWhiteSpaces(t.getElementsByAttValue("class", "quote", true, false)[0].getText().toString());
      res = ti.substring(1, ti.length() - 1);
      if (logger.isTraceEnabled()) {
        logger.trace("Comment's title [" + res + "] succesfully parsed using method 1.");
      }
    } catch (Exception e) {
      logger.warn("Unable to get comment's title using way 1. Reason [" + e.getMessage() + "] Let's try way 2.");
      try {
        res = removeWhiteSpaces(t.getElementsByAttValue("class", "noQuotes", true, false)[0].getText().toString());
        if (logger.isTraceEnabled()) {
          logger.trace("Comment's title [" + res + "] succesfully parsed using method 2.");
        }
      } catch (Exception e2) {
        logger.warn("Unable to get comment's title using way 2. Reason [" + e2.getMessage()
            + "]. item.title will be null. Review whether the parser needs to be updated.");
      }
    }
    return res;
  }
  
  /**
   * Given a document that contains a comment, looks for the URL that is the "canonical for that comment" and returns it.
   * 
   * @param parBaseURL
   *          the base url (i.e. the protocol and hostname) for the page where the comment is in.
   * @param t
   *          the Node root of the html document that contains the comment.
   * @param itemID
   *          a string that identifies the comment (only for loggin purposes) usually is the comment's title but can be any other value that let's to
   *          easily identify the comment when debugging.
   */
  private String getItemUrlFromComment(String parBaseURL, TagNode t, String itemID) {
    
    String res = null;
    
    // We start searching for an html tag with class="quote"
    TagNode tempNode[] = t.getElementsByAttValue("class", "quote", true, false);
    if (tempNode != null && tempNode.length > 0) {
      tempNode = tempNode[0].getElementsByName("a", false);
      if (tempNode != null && tempNode.length > 0) {
        res = canonicalizeURL(parBaseURL + tempNode[0].getAttributeByName("href"));
      } else {
        logger.warn("This item [" + itemID + "] has no [a] node inside the [class=\"quote\"] node. This means no own URL for this item.");
        if (logger.isTraceEnabled()) {
          logger.trace("The html that contains the comment is [" + t.getText() + "]");
        }
      }
    } else {
      // trying to find an html tag with class="quote isNew"
      logger.warn("No [class=\"quote\"] found in the html, let's try with [class=\"quote isNew\"]");
      tempNode = t.getElementsByAttValue("class", "quote isNew", true, false);
      if (tempNode != null && tempNode.length > 0) {
        tempNode = tempNode[0].getElementsByName("a", false);
        if (tempNode != null && tempNode.length > 0) {
          res = canonicalizeURL(parBaseURL + tempNode[0].getAttributeByName("href"));
        } else {
          logger.warn("This item [" + itemID
              + "] has no [a] node inside the [class=\"quote isNew\"] node. This means no own URL for this item.");
          if (logger.isTraceEnabled()) {
            logger.trace("The html that contains the comment is [" + t.getText() + "]");
          }
          
        }
      } else {
        logger.warn("No [class=\"quote\"] nor [class=\"quote isNew\"] were found in the html. The itemURL will remain null.");
        if (logger.isTraceEnabled()) {
          logger.trace("The html that contains the comment is [" + t.getText() + "]");
        }
      }
    }
    
    if (logger.isTraceEnabled()) {
      logger.trace("itemURL for item [" + itemID + "] will be [" + res + "]");
    }
    
    return res;
    
  }
  
  /**
   * Generates a long (expanded, with all its text) comment URL from the commentID and the Restaurant| Hotel general url. It relies in the fact that
   * parThreadID is a canonicalized URL. If not, there won't be an error here, but when the expanded comment url is processed.
   * 
   * @param commentID
   *          a number that identifies the comment inside tripadvisor.
   * @param parSite
   *          the site from tripadvisor where the comment is
   * @param parThreadID
   *          the thread this comment belongs to.
   * @return an url from which the comment can be retrieved with all its text.
   */
  private String buildLongCommentURL(String commentID, String parThreadID) {
    
    /*
     * The url with the facility review and the comments: http://www.tripadvisor.XX/Hotel_Review-gYYYYYY-dZZZZZZZ-Reviews-<hotel name>.html or
     * http://www.tripadvisor.XX/Restaurant_Review-gYYYYYY-dZZZZZZZ-Reviews-<restaurant name>.html must be changed to the one with the expanded
     * comment:
     * http://www.tripadvisor.XX/ExpandedUserReviews-gYYYYYY-dZZZZZZZ?target=commentID&context=1&reviews=commentID&expand=1&orig_url=parThreadID
     */
    String[] threadComps;
    String res = null;
    if (parThreadID != null) {
      threadComps = parThreadID.split("-Reviews-");
      if (threadComps != null && threadComps.length > 0) {
        threadComps[0] = threadComps[0].replaceAll("Hotel_Review", "ExpandedUserReviews");
        threadComps[0] = threadComps[0].replaceAll("Restaurant_Review", "ExpandedUserReviews");
        res = threadComps[0] + "?context=1&expand=1" + "&target=" + commentID + "&reviews=" + commentID + "&" + URL_ORIG_LABEL + "="
            + parThreadID;
        
      }
    }
    
    return res;
  }
  
  /**
   * Extracts the date of a comment in the main facility page
   * 
   * @param parNode
   *          the node where the date is stored for the comment
   * @return the comment's date or null if none has been found.
   */
  private Date extractDate(TagNode parNode) {
    
    Date res = null;
    String text = "";
    
    TagNode tempNodes[] = parNode.getElementsByAttValue("class", "ratingDate", true, false);
    TagNode tagNode = null;
    if (tempNodes != null && tempNodes.length > 0) {
      tagNode = tempNodes[0];
      text = tagNode.getText().toString();
      logger.trace("Raw date text from \"ratingDate\" node: [" + text + "]");
      res = parseTextualDate(text);
    } else {
      logger.warn("No node with [class=\"ratingDate\"] trying [class=\"ratingDate relativeDate\"]");
      tempNodes = parNode.getElementsByAttValue("class", "ratingDate relativeDate", true, false);
      if (tempNodes != null && tempNodes.length > 0) {
        text = tempNodes[0].getAttributeByName("content");
        if (text == null) {
          logger.warn("[\"content\"] attribute is empty or missing trying with [\"title\"]");
          text = tempNodes[0].getAttributeByName("title");
          if (text == null) {
            logger.warn("No way to get date. Returning null.");
            return null;
          } else {
            boolean found = false;
            try {
              res = sdEng.parse(text);
              found = true;
              if (logger.isTraceEnabled()) {
                logger.trace("Textual date [" + text + "] converted in date [" + res + "] using English parser(1) [" + sdEng.toPattern()
                    + "]");
              }
            } catch (ParseException e) {
              logger.warn("Error [" + e.getMessage() + "] parsing date [" + text + "] with English parser(1)[" + sdEng.toPattern() + "]");
              try {
                res = sdEng2.parse(text);
                found = true;
                if (logger.isTraceEnabled()) {
                  logger.trace("Textual date [" + text + "] converted in date [" + res + "] using English parser(2) [" + sdEng2.toPattern()
                      + "]");
                }
              } catch (ParseException e1) {
                logger
                    .warn("Error [" + e.getMessage() + "] parsing date [" + text + "] with English parser(2)[" + sdEng2.toPattern() + "]");
              }
            }
            
            if (!found) {
              try {
                res = sdSpn.parse(text);
                found = true;
                if (logger.isTraceEnabled()) {
                  logger.trace("Textual date [" + text + "] converted in date [" + res + "] using Spanish parser [" + sdSpn.toPattern()
                      + "]");
                }
                
              } catch (ParseException e) {
                logger.warn("Error [" + e.getMessage() + "] parsing date [" + text + "] with Spanish parser [" + sdSpn.toPattern() + "]");
              }
            }
            
            if (!found) {
              try {
                res = sdIta.parse(text);
                found = true;
                if (logger.isTraceEnabled()) {
                  logger.trace("Textual date [" + text + "] converted in date [" + res + "] using Italian parser [" + sdIta.toPattern()
                      + "]");
                }
              } catch (ParseException e) {
                logger.warn("Error [" + e.getMessage() + "] parsing date [" + text + "] with Italian parser [" + sdIta.toPattern() + "]");
              }
            }
            if (!found) {
              logger.warn("No way to parse a date from [" + text + "] returning null. ");
              return null;
            }
            
          }
          
        } else {
          logger.trace("Raw date text from \"ratingDate relativeDate\" node: [" + text + "]");
          try {
            res = sdReview.parse(text);
          } catch (ParseException e) {
            logger.warn("Error [" + e.getMessage() + "] parsing date [" + text + "] with parser [" + sdReview.toPattern() + "]");
          }
        }
        
      } else {
        logger
            .warn("Error parsing date. There is no node with [class=\"ratingDate\"] or [class=\"ratingDate relativeDate\"] inside the received document. Date will null.");
        return null;
      }
    }
    
    return res;
  }
  
  /**
   * @param res
   * @param text
   * @return
   */
  private Date parseTextualDate(String text) {
    Date res = null;
    String date = removeWhiteSpaces(text);
    if (logger.isTraceEnabled()) {
      logger.trace("This is text [" + text + "] and this is date [" + date + "]");
    }
    /*
     * PArseo de fechas: ES: Opinión escrita el (dd <Nombre del mes> yyyy) IT: Recensito il (dd <Nombre del mes> yyyy) EN: Reviewed (<Nombre corto del
     * mes(3letras) dd, yyyy) o Reviewed yesterday o Reviewed (X [days|weeks] ago) Acabar
     */
    
    if (date.toLowerCase().contains("opinión")) {
      try {
        res = sdSpn.parse(date.substring(date.indexOf("el") + 2).trim());
      } catch (Exception e) {
        logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage()
            + ". Maybe is an \"ago\" date. Trying to parse this kind of dates.");
        res = parseAgoDate(date, new Date(), sdSpn);
      }
      
    } else if (date.toLowerCase().contains("reviewed")) {
      try {
        date = date.replace("Reviewed", "").trim();
        res = sdEng.parse(date);
      } catch (Exception e) {
        logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage()
            + ". Maybe is an \"ago\" date. Trying to parse this kind of dates.");
        res = parseAgoDate(date, new Date(), sdEng);
      }
      
    } else if (date.toLowerCase().contains("recensito")) {
      try {
        res = sdIta.parse(date.substring(date.indexOf("il") + 2).trim());
      } catch (Exception e) {
        logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage()
            + ". Maybe is an \"ago\" date. Trying to parse this kind of dates.");
        res = parseAgoDate(date, new Date(), sdIta);
      }
      
    }
    if (res == null) {
      logger.warn("Not able to parse textual date [" + date + "]. Returning null");
    } else {
      if (logger.isTraceEnabled()) {
        logger.trace("Textual date [" + date + "] successfully parsed into a date object [" + res + "]");
      }
    }
    return res;
  }
  
  /**
   * assign the parent clipping date, if there are childs, we get a date one day before of the first comment, if not, take the actual date
   * 
   * @param items
   */
  private void assignParentDate(ArrayList<SimpleItem> items) {
    // Default date
    Date parentDate = new Date();
    if (items != null && items.size() > 0) {
      // get one day before of the first comment
      if (items.size() >= 2) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(items.get(1).date);
        calendar.add(Calendar.DATE, -1);
        parentDate = calendar.getTime();
      }
      items.get(0).date = parentDate;
    }
  }
}
