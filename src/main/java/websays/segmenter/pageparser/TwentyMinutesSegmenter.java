/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.htmlcleaner.TagNode;
import org.json.JSONArray;
import org.json.JSONObject;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class TwentyMinutesSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(TwentyMinutesSegmenter.class);
  
  public Date findMainDate(TagNode rootNode) {
    Date date = null;
    try {
      String publishedTime = getMetaContent(rootNode, "article:published_time").replace("+0100", "+01:00");
      if (publishedTime == null) {
        return null;
      }
      date = DatatypeConverter.parseDateTime(publishedTime).getTime();
      timeisReal = true;
    } catch (Exception e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
      return null;
    }
    return date;
  }
  
  @Override
  public Boolean findMainTimeIsReal(TagNode rootNode) {
    findMainDate(rootNode);
    return timeisReal;
  }
  
  @Override
  public List<SimpleItem> addComments(TagNode rootNode, SimpleItem mainItem) {
    List<SimpleItem> comments = new ArrayList<SimpleItem>();
    TagNode parent = rootNode.findElementByAttValue("id", "mn-comments", true, false);
    TagNode script = parent.findElementByName("script", true);
    String jsonStr = script.getText().toString().replaceFirst(".*?=", "").replaceFirst(";var.*", "");
    JSONArray jsonArray = new JSONArray(jsonStr);
    for (int i = 0; i < jsonArray.length(); i++) {
      comments.add(createCommentItemJson(jsonArray.getJSONObject(i), mainItem));
    }
    return comments;
  }
  
  private SimpleItem createCommentItemJson(JSONObject jsonObject, SimpleItem mainItem) {
    SimpleItem comment = createSimpleItem(mainItem.postUrl);
    SimpleDateFormat sd = new SimpleDateFormat("dd.MM.yyyy - HH'h'mm", Locale.FRANCE);
    comment.body = jsonObject.getString("texte");
    comment.author = jsonObject.getString("pseudo");
    comment.imageLink = jsonObject.getString("avatar");
    comment.isComment = true;
    try {
      comment.date = sd.parse(jsonObject.getString("createdAt"));
      comment.timeIsReal = true;
    } catch (Exception e) {
      logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
      comment.date = null;
    }
    try {
      comment.votes = Integer.parseInt(jsonObject.getString("totalVote"));
    } catch (Exception e) {
      // TODO Auto-generated catch block
    }
    return comment;
  }
  
}
