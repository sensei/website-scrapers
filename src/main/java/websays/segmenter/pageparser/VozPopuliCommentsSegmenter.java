/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class VozPopuliCommentsSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(VozPopuliCommentsSegmenter.class);
  
  private static SimpleDateFormat sdSpn = new SimpleDateFormat("dd-MM-yyyy hh:mm");
  
  String site = null;
  TagNode root = null;
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    HtmlCleaner cleaner = createCleaner();
    
    int position = 1;
    if (url != null) {
      site = url.toString();
    }
    root = cleaner.clean(content);
    TagNode commentsTag = root.findElementByAttValue("class", "listadoComentarios", true, false);
    if (commentsTag != null) {
      TagNode comments[] = commentsTag.getAllElements(false);
      if (comments != null && comments.length > 0) {
        for (TagNode commentTagNode : comments) {
          addSimpleItem(items, commentTagNode, site, position++);
        }
      } else {
        items = new ArrayList<SimpleItem>();
        SimpleItem item = new SimpleItem();
        item.fakeItem = true;
        item.postUrl = site;
        item.position = position;
        items.add(item);
        logger.debug("Fake Clipping with URL due to no comments in the URL added in threadsURL " + site);
      }
    }
    return items;
  }
  
  private void addSimpleItem(ArrayList<SimpleItem> items, TagNode commentTagNode, String site, int position) {
    SimpleItem simpleItem = new SimpleItem();
    
    // (java.lang.String) http://estatico.vozpopuli.com/v2/img/detalle/foto-comentarios-usuarios-sinImagen.jpg
    TagNode linkTag = commentTagNode.findElementByAttValue("class", "fotoUsuario", true, false);
    if (linkTag != null) {
      simpleItem.imageLink = linkTag.getAttributeByName("src");
    }
    
    // (java.lang.String) Roman2
    TagNode userTag = commentTagNode.findElementByAttValue("class", "nombreUsuario", true, false);
    if (userTag != null) {
      simpleItem.author = userTag.getText().toString();
    }
    
    // (java.lang.String) 03.07.2014 (10:07)
    TagNode dateTag = commentTagNode.findElementByAttValue("class", "fechaComentario", true, false);
    if (dateTag != null) {
      String sdate = removeWhiteSpaces(dateTag.getText().toString());
      sdate = sdate.replace(".", "-");
      sdate = sdate.replace("(", "");
      sdate = sdate.replace(")", "");
      sdate = sdate.replaceAll("\\s+", "");
      sdate = sdate.substring(0, sdate.indexOf(":") - 2).concat(" ").concat(sdate.substring(sdate.indexOf(":") - 2));
      Date date = null;
      try {
        date = sdSpn.parse(sdate);
        simpleItem.date = date;
        simpleItem.timeIsReal = true;
      } catch (ParseException e) {
        logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
        simpleItem.date = null;
      }
    }
    
    // (java.lang.String) yo creo qe va a utilizar esa compañia para vehiculizar la nueva estrategia de los poderes del PP (ocultos y no) para
    // llevarse el dinero. En lugar de hacerlo con tesoreros en la calle Genova, las multinacionales usaran esta cia de la que el se llevara una
    // parte, para dar dinero al PP y llevarse contratos, fuera del alcance de la judicaturia española. Va a ser el nuevo tesorero del PP. EL PP no
    // está por el cambio, sino por cambiar algo para seguir igual y darnos si acaso las migajas en el suelo par que nos las disputemos.
    TagNode textTag = commentTagNode.findElementByAttValue("class", "textoComentario", true, false);
    if (textTag != null) {
      simpleItem.body = textTag.getText().toString();
    }
    
    // (java.lang.String) 1
    TagNode likesTag = commentTagNode.findElementByAttValue("class", "meGusta", true, false);
    if (likesTag != null) {
      String numLikes = likesTag.getText().toString();
      if (numLikes != null && !numLikes.isEmpty()) {
        simpleItem.likes = Integer.valueOf(numLikes);
      } else {
        simpleItem.likes = new Integer(0);
      }
    }
    
    // (java.lang.String)
    TagNode noLikesTag = commentTagNode.findElementByAttValue("class", "noMeGusta", true, false);
    if (noLikesTag != null) {
      String numNoLikes = noLikesTag.getText().toString();
      if (numNoLikes != null && !numNoLikes.isEmpty()) {
        // simpleItem.likes = Integer.valueOf(numNoLikes);
      } else {
        // simpleItem.likes = new Integer(0);
      }
      simpleItem.APIObjectID = noLikesTag.getAttributeByName("href");
    }
    simpleItem.position = position;
    simpleItem.isComment = true;
    simpleItem.postUrl = site;
    if (position == 1) {
      simpleItem.threadURLs = findMainThreadURLs(root);
    }
    items.add(simpleItem);
  }
  
  @Override
  public List<String> findMainThreadURLs(TagNode rootNode) {
    ArrayList<String> urls = null;
    TagNode paginationTag = rootNode.findElementByAttValue("class", "paginacion", true, false);
    if (paginationTag != null) {
      TagNode pageNextTag = paginationTag.findElementByAttValue("class", "paginadorSiguiente", true, false);
      if (pageNextTag != null) {
        TagNode pageCurrentTag = paginationTag.findElementByAttValue("class", "paginadorPaginaActual", true, false);
        if (pageCurrentTag != null) {
          urls = new ArrayList<String>();
          int nextPage = Integer.valueOf(pageCurrentTag.getText().toString()) + 1;
          String nextUrl = site.substring(0, site.length() - 1) + nextPage;
          urls.add(nextUrl);
        }
      }
    }
    return urls;
    
  }
}
