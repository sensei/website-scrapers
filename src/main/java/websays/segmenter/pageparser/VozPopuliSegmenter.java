/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;

public class VozPopuliSegmenter extends BaseHTMLSegmenter {
  
  // (\d\d-\d\d-\d\d\d\d)
  private final static Pattern encodingPatternIfDate = Pattern.compile("(\\d\\d-\\d\\d-\\d\\d\\d\\d)");
  
  // (\d\d:\d\d)
  private final static Pattern encodingPatternIfHour = Pattern.compile("(\\d\\d:\\d\\d)");
  
  private static final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
  
  private static final Logger logger = Logger.getLogger(VozPopuliSegmenter.class);
  
  private static final String commentsUrlPart1 = "http://vozpopuli.com/v2/includes/ajax/comentarios/comentarios.php?idElemento=";
  
  private static final String commentsUrlPart2 = "&tipo=Noticias&pagina=1";
  
  @Override
  public String findMainTitle(TagNode rootNode) {
    String title = new String();
    TagNode titleNode = rootNode.findElementByAttValue("property", "og:title", true, false);
    if (titleNode != null) {
      title = StringEscapeUtils.unescapeXml(titleNode.getAttributeByName("content"));
    }
    return title;
  }
  
  @Override
  public String findMainDescripcion(TagNode rootNode) {
    String body = new String();
    TagNode bodyNode = rootNode.findElementByAttValue("class", "contenedorTextoNoticia textoEditor", true, false);
    if (bodyNode != null) {
      TagNode[] paragraphs = bodyNode.getElementsByName("p", false);
      if (paragraphs != null && paragraphs.length > 0) {
        for (TagNode tagNode : paragraphs) {
          body = body.concat(tagNode.getText().toString() + " ");
        }
      }
    }
    return body;
  }
  
  @Override
  public String findMainAuthor(TagNode rootNode) {
    String author = new String();
    TagNode authorNode = rootNode.findElementByAttValue("name", "author", true, false);
    if (authorNode != null) {
      author = StringEscapeUtils.unescapeXml(authorNode.getAttributeByName("content"));
    }
    return author;
  }
  
  @Override
  public Date findMainDate(TagNode rootNode) {
    Date date = null;
    TagNode dateNode = rootNode.findElementByAttValue("class", "categoriaAutor", true, false);
    if (dateNode != null) {
      String dateText = removeWhiteSpaces(dateNode.getText().toString());
      Matcher matcher = encodingPatternIfDate.matcher(dateText);
      if (matcher.find()) {
        String mdate = matcher.group(1);
        // String fdate = removeWhiteSpaces(mdate.substring(0, mdate.indexOf("h")));
        // 03-07-2014
        try {
          date = sdf.parse(mdate);
        } catch (ParseException e) {
          logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
          return null;
        }
      } else {
        Matcher matcher1 = encodingPatternIfHour.matcher(dateText);
        if (matcher1.find()) {
          // Put the current date dd-mm-yyyy
          try {
            Calendar cal = Calendar.getInstance();
            cal.get(Calendar.DATE);
            date = sdf.parse(sdf.format(cal.getTime()));
          } catch (ParseException e) {
            logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
            return null;
          }
        }
      }
    }
    return date;
  }
  
  @Override
  public String findMainThreadId(TagNode rootNode) {
    return null;
  }
  
  @Override
  public List<String> findMainThreadURLs(TagNode rootNode) {
    ArrayList<String> urls = null;
    TagNode elementNode = rootNode.findElementByAttValue("id", "contenedorComentariosTotal", true, false);
    if (elementNode != null) {
      urls = new ArrayList<String>();
      String idElementLarge = elementNode.getAttributeByName("class");
      String idElement = idElementLarge.substring(idElementLarge.indexOf("-") + 1, idElementLarge.length());
      urls.add(commentsUrlPart1.concat(idElement).concat(commentsUrlPart2));
    }
    return urls;
    
  }
}
