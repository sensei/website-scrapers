/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 9, 2014
 */
package websays.segmenter.pageparser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem.LanguageTag;

public class ZucconiSegmenter extends BaseHTMLSegmenter {
  
  private static SimpleDateFormat sdfMain = new SimpleDateFormat("dd MMM yyyy", Locale.ITALY);
  
  private static SimpleDateFormat sdfComment = new SimpleDateFormat("dd MMM yyyy  hh:mm", Locale.ITALY);
  
  private static final Logger logger = Logger.getLogger(ZucconiSegmenter.class);
  
  @Override
  public String findMainAPIObjectID(TagNode rootNode) {
    String id = null;
    TagNode bodyId = rootNode.findElementByAttValue("class", "main-article", true, false);
    if (bodyId != null) {
      TagNode elementsBody[] = bodyId.getAllElements(false);
      if (elementsBody != null && elementsBody.length >= 0) {
        id = elementsBody[0].getAttributeByName("id");
      }
    }
    return id;
  }
  
  @Override
  public String findMainDescripcion(TagNode rootNode) {
    String body = null;
    TagNode bodyNode = rootNode.findElementByAttValue("class", "article-maincolblog", true, false);
    if (bodyNode != null) {
      TagNode elementsBody[] = bodyNode.getAllElements(false);
      if (elementsBody != null && elementsBody.length >= 0) {
        body = elementsBody[0].getText().toString();
        body = body.replaceAll("&#\\d\\d\\d*;", "");
      }
    }
    return body;
  }
  
  @Override
  public Date findMainDate(TagNode rootNode) {
    Date date = null;
    TagNode bodyDate = rootNode.findElementByAttValue("class", "datapubb-blog", true, false);
    if (bodyDate != null) {
      TagNode elementsDate[] = bodyDate.getAllElements(false);
      if (elementsDate != null && elementsDate.length >= 3) {
        try {
          String giorno = elementsDate[0].getText().toString();
          String mese = elementsDate[1].getText().toString();
          String anno = elementsDate[2].getText().toString();
          date = sdfMain.parse(giorno + " " + mese + " " + anno);
        } catch (ParseException e) {
          logger.error(e.getMessage());
        } catch (Exception e) {
          logger.error(e.getMessage());
        }
      }
    }
    return date;
  }
  
  @Override
  public LanguageTag findMainLanguage(TagNode rootNode) {
    return LanguageTag.italian;
  }
  
  @Override
  public TagNode[] findComments(TagNode rootNode) {
    TagNode comments[] = null;
    TagNode bodyMain = rootNode.findElementByAttValue("class", "main-article", true, false);
    if (bodyMain != null) {
      TagNode commentsTag = bodyMain.findElementByAttValue("id", "comments", true, false);
      if (commentsTag != null) {
        TagNode elementsTag[] = commentsTag.getElementsByName("li", false);
        if (elementsTag != null && elementsTag.length > 0) {
          comments = elementsTag;
        }
      }
    }
    return comments;
  }
  
  @Override
  public Integer findMainComments(TagNode rootNode) {
    int numComments = 0;
    TagNode bodyMain = rootNode.findElementByAttValue("class", "main-article", true, false);
    if (bodyMain != null) {
      TagNode commentsTag = bodyMain.findElementByAttValue("id", "comments", true, false);
      if (commentsTag != null) {
        TagNode numCommentsTag = commentsTag.findElementByName("h2", false);
        if (numCommentsTag != null) {
          String sNumComments = numCommentsTag.getText().toString();
          sNumComments = removeWhiteSpaces(sNumComments.replaceAll("[A-Za-z]", ""));
          numComments = Integer.valueOf(sNumComments.replace(".", ""));
        }
      }
    }
    return numComments;
  }
  
  @Override
  public String findCommentAPIObjectID(TagNode commentNode) {
    return commentNode.getAttributeByName("id");
  }
  
  @Override
  public String findCommentAuthor(TagNode commentNode) {
    String author = null;
    TagNode authorNode = commentNode.findElementByAttValue("class", "commentheader", true, false);
    if (authorNode != null) {
      TagNode sAuthor = authorNode.findElementByName("strong", true);
      if (sAuthor != null) {
        author = sAuthor.getText().toString();
      }
    }
    return author;
  }
  
  @Override
  public String findCommentDescripcion(TagNode commentNode) {
    String body = null;
    TagNode bodyNode = commentNode.findElementByAttValue("class", "commenttext", true, false);
    if (bodyNode != null) {
      body = bodyNode.getText().toString();
      body = body.replaceAll("&#\\d\\d\\d*;", "");
    }
    return body;
  }
  
  @Override
  public String findCommentImageLink(TagNode commentNode) {
    String link = null;
    TagNode imageNode = commentNode.findElementByAttValue("class", "gravatar", true, false);
    if (imageNode != null) {
      link = imageNode.getText().toString();
    }
    return link;
  }
  
  @Override
  public Date findCommentDate(TagNode commentNode) {
    Date date = null;
    TagNode dateNode = commentNode.findElementByAttValue("class", "commentheader", true, false);
    if (dateNode != null) {
      TagNode dateFinalDate = dateNode.findElementByName("a", false);
      if (dateFinalDate != null) {
        try {
          String sdate = dateFinalDate.getText().toString();
          date = sdfComment.parse(sdate.replace("alle", ""));
          timeisReal = true;
        } catch (ParseException e) {
          logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
          return null;
        }
      }
    }
    return date;
  }
  
  @Override
  public Boolean findMainTimeIsReal(TagNode rootNode) {
    findMainDate(rootNode);
    return timeisReal;
  }
  
  @Override
  public LanguageTag findCommentLanguage(TagNode rootNode) {
    return LanguageTag.italian;
  }
  
}
