/**
< * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
/**
 * 
 */
package websays.segmenter.pageparser.forums;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class ForoEnFemeninoSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(ForoEnFemeninoSegmenter.class);
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    HtmlCleaner cleaner = createCleaner();
    
    int position = 1;
    TagNode root = null;
    String site = null;
    if (url != null) {
      site = url.toString();
    }
    root = cleaner.clean(content);
    
    // /////////// Main Details Item ////////////
    SimpleItem mainItem = new SimpleItem();
    if (checkMainDiscussion(root)) {
      mainItem.title = root.findElementByAttValue("class", "aff_titre", true, false).getText().toString();
      TagNode mainInfo1[] = root.getElementsByAttValue("class", "aff_blocMess", true, false);
      mainItem.author = mainInfo1[0].findElementByAttValue("class", "aff_author", true, false).getText().toString();
      mainItem.threadId = mainInfo1[0].getAttributeByName("id").substring(0, mainInfo1[0].getAttributeByName("id").indexOf("_"));
      
      TagNode mainInfo2 = mainInfo1[0].getAllElements(false)[2];
      if (mainInfo2.hasAttribute("id")) {
        if (mainInfo2.getAttributeByName("id").equals(mainItem.threadId + "_ul")) {
          mainItem.body = mainInfo2.getText().toString().substring(0, mainInfo2.getText().toString().indexOf("aff_FormatDate"));
        }
      }
      int from = mainInfo2.getAllElements(false)[3].getAllElements(true)[3].getText().toString().indexOf("(");
      int to = mainInfo2.getAllElements(false)[3].getAllElements(true)[3].getText().toString().indexOf(",");
      try {
        String milliseconds = mainInfo2.getAllElements(false)[3].getAllElements(true)[3].getText().toString().substring(from + 1, to);
        Date date = new Date(Long.valueOf(milliseconds + "000"));
        mainItem.date = date;
        mainItem.timeIsReal = true;
      } catch (Exception e) {
        logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
        mainItem.date = null;
      }
      
      mainItem.position = position++;
      mainItem.threadURLs = findMainThreadURLs(root);
      mainItem.postUrl = site;
      items.add(mainItem);
    }
    
    // /Responses made by the Main Item in the forum
    TagNode[] responses = root.getElementsByAttValue("id", "af_colCentre", true, false)[0].getAllElements(false);
    for (TagNode tagNode : responses) {
      Map<String,String> attributes = tagNode.getAttributes();
      for (Entry<String,String> entry : attributes.entrySet()) {
        if (entry.getKey().equals("class") && entry.getValue().contains("aff_blocRepNiv")) {
          if (tagNode.getAttributeByName("id").contains("_mb")) {
            SimpleItem commentItem = new SimpleItem();
            commentItem.threadId = tagNode.getAttributeByName("id");
            commentItem.author = tagNode.findElementByAttValue("class", "aff_author", true, false).getText().toString();
            commentItem.title = tagNode.findElementByAttValue("class", "aff_titre", true, false).getText().toString();
            commentItem.body = tagNode.findElementByAttValue("class", "aff_contenu", true, false).getText().toString();
            tagNode.findElementByAttValue("class", "aff_date", true, false).getText().toString();
            int fromComment = tagNode.findElementByAttValue("class", "aff_date", true, false).getText().toString().indexOf("(");
            int toComment = tagNode.findElementByAttValue("class", "aff_date", true, false).getText().toString().indexOf(",");
            String milliseconds = tagNode.findElementByAttValue("class", "aff_date", true, false).getText().toString()
                .substring(fromComment + 1, toComment);
            Date dateComment = new Date(Long.valueOf(milliseconds + "000"));
            mainItem.date = dateComment;
            commentItem.postUrl = site;
            commentItem.position = position++;
            if (position == 2) {
              commentItem.threadURLs = findMainThreadURLs(root);
            }
            commentItem.isComment = true;
            items.add(commentItem);
          }
        }
      }
    }
    return items;
  }
  
  private boolean checkMainDiscussion(TagNode root) {
    boolean check = true;
    TagNode mainInfo1[] = root.getElementsByAttValue("class", "aff_navHigh", true, false);
    if (mainInfo1.length >= 2) {
      if (mainInfo1[1].findElementByAttValue("align", "right", true, false) != null) {
        TagNode nodeNextPage = mainInfo1[1].findElementByAttValue("align", "right", true, false).findElementByName("a", true);
        if (nodeNextPage != null) {
          if (!nodeNextPage.getText().toString().equals("2")) {
            check = false;
          }
        }
      }
    }
    return check;
  }
  
  @Override
  public List<String> findMainThreadURLs(TagNode root) {
    List<String> urls = null;
    TagNode mainInfo1[] = root.getElementsByAttValue("class", "aff_navHigh", true, false);
    if (mainInfo1.length >= 2) {
      if (mainInfo1[1].findElementByAttValue("align", "right", true, false) != null) {
        TagNode nodeNextPage = mainInfo1[1].findElementByAttValue("align", "right", true, false).findElementByName("a", true);
        if (nodeNextPage != null) {
          if (nodeNextPage.getText().toString() != null && !nodeNextPage.getText().toString().equals("Precedentes")) {
            urls = Arrays.asList(nodeNextPage.getAttributeByName("href"));
          }
        }
      }
    }
    return urls;
  }
}
