/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2015. All rights reserved. http://websays.com )
 *
 * Primary Author: juanfra
 * Contributors:
 * Date: Jun 23, 2015
 */
package websays.segmenter.pageparser.forums;

import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.HtmlNode;
import org.htmlcleaner.TagNode;
import org.htmlcleaner.TagNodeVisitor;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;
import websays.types.clipping.SimpleItem.LanguageTag;

/**
 * @author juanfra
 *
 */
public class ForoLosViajerosCommentsSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(ForoLosViajerosCommentsSegmenter.class);
  static final Pattern pTopic = Pattern.compile("[?&]t=(\\d+)");
  
  /**
   * Formater to convert the 3 letter month word to its numerical counterpart
   */
  private static SimpleDateFormat format3LetterMonth = new SimpleDateFormat("MMM", new Locale("es", "ES"));
  
  /**
   * Pattern to find the author's location inside the page
   */
  private static Pattern location = Pattern.compile(".*Ubicación:\\s*(.*)", Pattern.DOTALL);
  
  /**
   * Pattern to get the comment's date from the page.
   */
  private static Pattern dateExpression = Pattern.compile(
      ".*Publicado:\\s+(\\w{3})\\s+(\\w{3})\\s+(\\d{2}),\\s+(\\d{4})\\s+(\\d{1,2}):(\\d{2})\\s+(\\w{2}).*", Pattern.DOTALL);
  
  /**
   * Pattern to get the comment's title from the page.
   */
  private static Pattern titleExpression = Pattern.compile(".*Asunto:\\s*(.*)", Pattern.DOTALL);
  
  /*
   * (non-Javadoc)
   * 
   * @see websays.segmenter.pageparser.DocumentSegmenter#segment(java.lang.String, java.net.URL)
   */
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    HtmlCleaner cleaner = createCleaner();
    
    TagNode root = null;
    root = cleaner.clean(content);
    
    // /////////// Main Details Item ////////////
    SimpleItem main = new SimpleItem();
    
    // ///// Extract Title
    try {
      TagNode heading = root.getElementsByAttValue("class", "maintitle", true, false)[0];
      TagNode[] headChildren = heading.getChildTags();
      if (headChildren != null && headChildren.length > 2) {
        main.title = removeWhiteSpaces(headChildren[1].getText().toString());
        if (logger.isTraceEnabled()) {
          logger.trace("The page comments title is [" + main.title + "]");
        }
        
      } else {
        logger.warn("Error, the actual page structure [" + heading.getText().toString()
            + "] is not what the parser expects. There will be no title in the retreived comments.");
        main.title = "";
        
      }
      
    } catch (Exception ex) {
      logger.warn("Error [" + ex.getMessage() + "] getting the forum title. There will be no title in the retreived comments.", ex);
      main.title = "";
    }
    
    main.date = new Date(); // Forum "parent" or top does not have a specific date, will set later as the first child if any...
    main.position = 1;
    main.language = LanguageTag.spanish;
    String id = null;
    
    // parse URL: (this is crucial to get all the comments from the thread as same composite ID even though they are coming form different pages...)
    String parsedURL = url.toExternalForm();
    main.postUrl = parsedURL;
    Matcher m = pTopic.matcher(parsedURL);
    if (m.find()) {
      main.postUrl = parsedURL.substring(0, parsedURL.lastIndexOf("/")) + "?t=" + m.group(1);
      main.itemUrl = main.postUrl;
    }
    
    if (parsedURL.contains("p=")) {
      String[] parts = parsedURL.split("#");
      if (parts.length == 2) {
        id = parts[1];
        main.APIObjectID = id;
        if (logger.isTraceEnabled()) {
          logger.trace("The page APIOnjectID is [" + main.APIObjectID + "]");
        }
      } else {
        logger.warn("The url structure is not as expected. Actual url [" + parsedURL + "]. The APIObjectID won't be retrieved.");
      }
    }
    
    if ((main.title == null || main.title.length() == 0) && (main.body == null || main.body.length() == 0)) {
      logger.warn("No title or body found for main, ignoring");
    } else {
      items.add(main);
    }
    
    // ///////////////// Thread (get next page, always go forward not to create a loop!)
    String threadUrl = null;
    TagNode nodes[] = root.getElementsByAttValue("class", "table1", true, false);
    for (TagNode n1 : nodes) {
      NextThreadVisitor visitor = new NextThreadVisitor();
      n1.traverse(visitor);
      if (visitor.threadUrl != null) {
        URL threadUrl2 = new URL(url, visitor.threadUrl);
        threadUrl = threadUrl2.toExternalForm();
        break;
      }
    }
    if (threadUrl != null) {
      main.threadURLs = Arrays.asList(new String[] {threadUrl});
    }
    
    // ///////////////// Comment Items
    int position = 2;
    TagNode postsRoot[] = root.getElementsByAttValue("class", "forumline", true, false);
    if (postsRoot != null && postsRoot.length > 0) {
      for (TagNode table : postsRoot) { // (hugoz) added this for loop because sometimes there are some empty ones
        @SuppressWarnings("unchecked")
        List<TagNode> tableBody = table.getElementListByName("tbody", true);
        if (tableBody != null && tableBody.size() > 0) {
          TagNode rows[] = tableBody.get(0).getChildTags();
          if (rows != null && rows.length > 0) {
            for (TagNode t : rows) {
              SimpleItem item;
              if (isCommentRow(t)) {
                item = getItemFromTagNode(t);
                item.position = position++;
                item.postUrl = main.postUrl;
                item.language = LanguageTag.spanish;
                item.isComment = true;
                items.add(item);
              }
            }
          } else {
            logger
                .warn("The \"tbody\" tag has no children. This means there is no comment in this page. Check possible changes in the page structure.");
          }
        } else {
          logger
              .warn("There is no \"tbody\" tag inside the  \"forumline\". Either the page doesn't belong to ForoLosViajeros or the page structure has changed.");
        }
      }
    } else {
      logger
          .warn("There is no \"forumline\" class tag in the page. Either the page doesn't belong to ForoLosViajeros or the page structure has changed.");
    }
    
    if (logger.isTraceEnabled()) {
      logger.trace("Parsed [" + (position - 1) + "] comments from page [" + url + "] and set postUrl=[" + main.postUrl + "]");
    }
    
    // set main date as first child's, it's arbitrary
    if (items.size() > 1) {
      main.date = items.get(1).date;
    }
    
    return items;
    
  }
  
  /**
   * Given a TagNode detects whether it is a comment or not.
   * 
   * @param parNode
   *          the node to be checked.
   * @return true if the node contains comment information. False otherwise
   */
  private boolean isCommentRow(TagNode parNode) {
    TagNode details[] = parNode.getElementsByAttValue("class", "postdetails", true, false);
    return (details != null && details.length > 0);
  }
  
  /**
   * Takes a TagNode and extracts its info to build a SimpleItem.
   * 
   * @param parNode
   *          the node to be processed.
   * @return a simpleItem structure with the information in the node.
   */
  private SimpleItem getItemFromTagNode(TagNode parNode) {
    
    SimpleItem item = new SimpleItem();
    
    /* author */
    item.author = null;
    TagNode tempNodes[] = parNode.getElementsByAttValue("class", "name name-little", true, false);
    if (tempNodes != null && tempNodes.length > 0) {
      tempNodes = tempNodes[0].getElementsByName("b", false);
      if (tempNodes != null && tempNodes.length > 0) {
        item.author = tempNodes[0].getText().toString();
        if (logger.isTraceEnabled()) {
          logger.trace("Succesfully got author's name [" + item.author + "]");
        }
      } else {
        logger
            .warn("There is no \"b\" tag inside tag with [class=\"name name-little\"]. Maybe the segmenter must catch up with the page structure.");
      }
    } else {
      logger
          .warn("Author's name not found because there is not an html tag with [class=\"name name-little\"]. Maybe the segmenter must catch up with the page structure.");
    }
    
    /* author location */
    item.authorLocation = null;
    tempNodes = parNode.getElementsByAttValue("class", "postdetails", true, false);
    if (tempNodes != null && tempNodes.length > 0) {
      String tempNodeContent = tempNodes[0].getText().toString();
      Matcher m = location.matcher(tempNodeContent);
      if (m.find()) {
        item.authorLocation = m.group(1);
        if (logger.isTraceEnabled()) {
          logger.trace("Author's location is [" + item.authorLocation + "]");
        }
      } else {
        // there were many of these because many users missing info, so moving from warn to trace:
        logger.trace("Author's location not found. There is no \"Ubicación\" string in the tag's contents [" + tempNodeContent + "]");
      }
    } else {
      logger
          .trace("Author's location not found because there is not an html tag with [class=\"postdetails\"]. Maybe the segmenter must catch up with the page structure.");
    }
    
    /* title */
    item.title = null;
    tempNodes = parNode.getElementsByName("tbody", true);
    if (tempNodes != null && tempNodes.length > 0) {
      tempNodes = tempNodes[0].getElementsByAttValue("class", "postdetails", true, false);
      if (tempNodes != null && tempNodes.length > 0) {
        String tempNodeContent = tempNodes[0].getText().toString();
        Matcher m = titleExpression.matcher(tempNodeContent);
        if (m.find()) {
          item.title = m.group(1);
          if (logger.isTraceEnabled()) {
            logger.trace("Got comment's title [" + item.title + "]");
          }
        } else {
          logger.warn("Title not found inside the tag's content [" + tempNodeContent
              + "]. Maybe the segmenter must catch up with the page structure.");
        }
      } else {
        logger
            .warn("Title not found because there is no \"postdetails\" node inside the \"tbody\" tag. Maybe the segmenter must catch up with the page structure.");
      }
    } else {
      logger
          .warn("Title not found because there is no \"tbody\" tag inside the node. Maybe the segmenter must catch up with the page structure.");
    }
    
    /* date */
    boolean dateError = false;
    item.date = null;
    tempNodes = parNode.getElementsByName("tbody", true);
    if (tempNodes != null && tempNodes.length > 0) {
      tempNodes = tempNodes[0].getElementsByAttValue("class", "postdetails", true, false);
      if (tempNodes != null && tempNodes.length > 0) {
        String tempNodeContent = tempNodes[0].getText().toString();
        Matcher m = dateExpression.matcher(tempNodeContent);
        if (m.find()) {
          Calendar tempDate = Calendar.getInstance(TimeZone.getTimeZone("Europe/Madrid"), new Locale("es", "ES"));
          try {
            tempDate.setTime(format3LetterMonth.parse(m.group(2)));
          } catch (ParseException e) {
            logger.warn("Error [" + e.getMessage() + "] trying to parse short month name [" + m.group(2) + "]. The date will be null.");
            dateError = true;
          }
          
          if (!dateError) {
            tempDate.clear(Calendar.SECOND);
            tempDate.clear(Calendar.MILLISECOND);
            int year, day, hour, minute, am_pm;
            
            if (logger.isTraceEnabled()) {
              logger.trace("The pattern matcher got: month[" + m.group(2) + "]  day [" + m.group(3) + "] year[" + m.group(4) + "] hour["
                  + m.group(5) + "] minute[" + m.group(6) + "] AM/PM [" + m.group(7) + "]");
            }
            
            try {
              
              day = Integer.parseInt(m.group(3));
              year = Integer.parseInt(m.group(4));
              hour = Integer.parseInt(m.group(5));
              minute = Integer.parseInt(m.group(6));
              tempDate.set(Calendar.MINUTE, minute);
              tempDate.set(Calendar.HOUR, hour);
              am_pm = ("AM".compareToIgnoreCase(m.group(7)) == 0 ? Calendar.AM : Calendar.PM);
              tempDate.set(Calendar.AM_PM, am_pm);
              tempDate.set(Calendar.DAY_OF_MONTH, day);
              tempDate.set(Calendar.YEAR, year);
              
            } catch (NumberFormatException nfe) {
              logger.warn("Error [" + nfe.getMessage() + "] trying to parse either year,day,hour or minute [" + m.group(4) + ", "
                  + m.group(3) + ", " + m.group(5) + ", " + m.group(6) + "]. The date will be null.");
              dateError = true;
              
            }
            
            if (!dateError) {
              item.date = tempDate.getTime();
              if (logger.isTraceEnabled()) {
                logger.trace("Date got from the comment: [" + item.date.toString() + "]");
              }
            }
            
          }
        } else {
          logger.warn("Date not found inside the tag's conent [" + tempNodeContent
              + "]. Maybe the segmenter must catch up with the page structure.");
        }
      } else {
        logger
            .warn("Date not found because there is no \"postdetails\" node inside the \"tbody\" tag. Maybe the segmenter must catch up with the page structure.");
      }
    } else {
      logger
          .warn("Date not found because there is no \"tbody\" tag inside the node. Maybe the segmenter must catch up with the page structure.");
    }
    
    /* itemurl */
    item.itemUrl = null;
    tempNodes = parNode.getElementsByName("tbody", true);
    if (tempNodes != null && tempNodes.length > 0) {
      tempNodes = tempNodes[0].getElementsByAttValue("class", "postdetails", true, false);
      if (tempNodes != null && tempNodes.length > 0) {
        tempNodes = tempNodes[0].getElementsByName("a", true);
        if (tempNodes != null && tempNodes.length > 0) {
          item.itemUrl = tempNodes[0].getAttributeByName("href");
          if (logger.isTraceEnabled()) {
            logger.trace("Got itemUrl for the comment [" + item.itemUrl + "]");
          }
        } else {
          logger
              .warn("ItemUrl not found because there is no \"a\" node inside \"postdetails\" node. Maybe the segmenter must catch up with the page structure.");
        }
      } else {
        logger
            .warn("ItemUrl not found because there is no \"postdetails\" node inside the \"tbody\" tag. Maybe the segmenter must catch up with the page structure.");
      }
    } else {
      logger
          .warn("ItemUrl not found because there is no \"tbody\" tag inside the node. Maybe the segmenter must catch up with the page structure.");
    }
    
    /* body */
    item.body = null;
    tempNodes = parNode.getElementsByAttValue("class", "postbody", true, false);
    if (tempNodes != null && tempNodes.length > 0) {
      item.body = tempNodes[0].getText().toString().trim();
      if (logger.isTraceEnabled()) {
        logger.trace("Got body [" + item.body + "] for the comment.");
      }
    } else {
      logger
          .warn("Body for the comment not found because there is no \"postbody\" tag inside the node. Maybe the segmenter must catch up with the page structure.");
    }
    return item;
  }
  
  class NextThreadVisitor implements TagNodeVisitor {
    
    public String threadUrl = null;
    
    @Override
    public boolean visit(TagNode tagNode, HtmlNode htmlNode) {
      if (htmlNode instanceof TagNode) {
        try {
          TagNode tag = (TagNode) htmlNode;
          String tagName = tag.getName();
          if ("a".equals(tagName)) {
            // System.out.println(">>>" + tag.getChildren().get(0));
            if (tag.getChildren().get(0).toString().equals("Siguiente")) {
              threadUrl = tag.getAttributeByName("href");
              return false;
            }
          }
        } catch (Exception e) {}
      }
      return true;
    }
  }
  
}
