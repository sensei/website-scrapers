package websays.segmenter.pageparser.forums;

import java.util.ArrayList;
import java.util.regex.Pattern;

import websays.segmenter.base.URLCollector;

public class ForoLosViajerosSearch extends URLCollector {
  
  public ForoLosViajerosSearch() {
    urlPrefix = "http://www.losviajeros.com/";
    
    // We only collect the first page of each forum since each page will collect the next when processed. If you collect all you will re-process very
    // many times the same pages.
    
    // First page of each forum (repeated URLs found are uniqued by URLCollector, son don't worry here just get all the topics:
    Pattern p1 = Pattern.compile("\"(foros[.]php[?]t=\\d+)");
    
    // Pattern p1 = Pattern.compile("\"(foros[.]php[?]t=\\d+[&]start=0)\""); // this one works for some pages but not for others (e.g. fails on
    // http://www.losviajeros.com/index.php?name=Forums&file=search&st=pullmantur&sf=65&start=40)
    
    // Next page of search results:
    Pattern p2 = Pattern.compile("<a href=\\\"(index.php[^\"]+)[\"]>Siguiente</a>");
    
    collectors = new ArrayList<Pattern>();
    collectors.add(p1);
    collectors.add(p2);
  }
  
}
