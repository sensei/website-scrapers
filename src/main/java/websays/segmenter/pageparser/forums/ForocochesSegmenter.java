/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser.forums;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class ForocochesSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(ForocochesSegmenter.class);
  
  public static SimpleDateFormat sdSpn = new SimpleDateFormat("dd-MMM-yyyy, HH:mm", new Locale("es", "ES"));
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    HtmlCleaner cleaner = createCleaner();
    String rootPage = "http://www.forocoches.com/foro/";
    
    TagNode root = null;
    String site = null;
    if (url != null) {
      site = url.toString();
    }
    root = cleaner.clean(content);
    
    // /////////// Main Details Item ////////////
    SimpleItem main = new SimpleItem();
    // ///// Extract Title
    try {
      TagNode heading = root.getElementsByAttValue("class", "cmega", true, false)[0];
      String title = removeWhiteSpaces(heading.getText().toString());
      main.title = title;
    } catch (Exception ex) {}
    
    main.date = null;
    main.position = 1;
    String parsedURL = site;
    String id = null;
    if (parsedURL.contains("page=")) {
      String[] parts = parsedURL.split("\\?");
      if (parts.length == 2) {
        StringBuilder temp = new StringBuilder();
        temp.append(parts[0]).append("?");
        String argsPart = parts[1];
        String[] args = argsPart.split("&");
        boolean isFirst = true;
        for (String arg : args) {
          
          String[] entry = arg.split("=");
          boolean add = true;
          if (entry.length == 2) {
            if (entry[0].equals("t") || entry[0].equals("p")) {
              id = entry[1];
            } else if (entry[0].equals("page")) {
              // remove page number in the url
              add = false;
            }
          }
          if (add) {
            temp.append(arg);
          } else {
            continue;
          }
          if (!isFirst) {
            temp.append("&");
          } else {
            isFirst = false;
          }
          
        }
        parsedURL = temp.toString();
      }
    }
    
    main.postUrl = parsedURL;
    main.APIObjectID = id;
    items.add(main);
    
    // ///////////////// Comment Items
    int position = 2;
    TagNode postsRoot[] = root.getElementsByAttValue("id", "posts", true, false);
    if (postsRoot != null && postsRoot.length > 0) {
      TagNode reviews[] = postsRoot[0].getElementsByAttValue("class", "page", true, false);
      if (reviews != null && reviews.length > 0) {
        for (TagNode t : reviews) {
          SimpleItem item = new SimpleItem();
          
          // ///// Body
          try {
            TagNode bd = t.getElementsByAttValue("id", "HOTWordsTxt", true, false)[0];
            TagNode cont = null;
            TagNode[] childs = bd.getAllElements(true);
            
            for (TagNode tg : childs) {
              try {
                if (removeWhiteSpaces(tg.getAttributeByName("id")).startsWith("post_message")) {
                  cont = tg;
                  break;
                }
              } catch (Exception e) {}
            }
            
            String body = removeWhiteSpaces(cont.getText().toString());
            item.body = body;
            
            // comment below lines if we should not avoid Quotes in
            // the comments body
            
            childs = bd.getAllElements(true);
            for (TagNode tg : childs) {
              try {
                cont.removeChild(tg);
              } catch (Exception e) {}
            }
            
            body = removeWhiteSpaces(cont.getText().toString());
            item.body = body;
            
          } catch (Exception e) {}
          // /////// author
          try {
            String author = removeWhiteSpaces(t.getElementsByAttValue("class", "bigusername", true, false)[0].getText().toString());
            item.author = author;
          } catch (Exception e) {}
          
          // /////// author location
          try {
            TagNode[] dd = t.getElementsByAttValue("class", "smallfont", true, false);
            L1: for (TagNode tg : dd) {
              try {
                TagNode loc[] = tg.getElementsByName("div", true);
                if (loc != null) {
                  for (TagNode l : loc) {
                    try {
                      if (removeWhiteSpaces(l.getText().toString()).startsWith("Lugar:")) {
                        String location = removeWhiteSpaces(l.getText().toString());
                        item.authorLocation = location.substring(location.indexOf("Lugar:") + 6).trim();
                        break L1;
                      }
                    } catch (Exception e) {}
                  }
                }
              } catch (Exception e) {}
            }
          } catch (Exception e) {}
          
          // /////// comments
          try {
            TagNode[] dd = t.getElementsByAttValue("class", "smallfont", true, false);
            L1: for (TagNode tg : dd) {
              try {
                TagNode com[] = tg.getElementsByName("div", true);
                if (com != null) {
                  for (TagNode l : com) {
                    try {
                      if (removeWhiteSpaces(l.getText().toString()).contains("Mens.")) {
                        String comments = removeWhiteSpaces(l.getText().toString());
                        comments = comments.substring(comments.lastIndexOf("|") + 1);
                        // IMPORTANT: comment this because this number is wrong, it's the total number of messages of this user, we don't need this
                        // number...
                        // item.comments=parseNumbers(comments.replace(".", "").replace(",", ""), false);
                        break L1;
                      }
                    } catch (Exception e) {}
                  }
                }
              } catch (Exception e) {}
            }
          } catch (Exception e) {}
          // /////// date
          try {
            TagNode[] dd = t.getElementsByAttValue("class", "thead", true, false);
            if (dd != null && dd.length > 0) {
              for (TagNode dt : dd) {
                try {
                  String date = removeWhiteSpaces(dt.getText().toString());
                  if (date.contains(":") && date.contains(", ")) {
                    item.date = sdSpn.parse(date);
                    item.timeIsReal = true;
                    break;
                  }
                } catch (Exception e) {
                  logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
                  item.date = null;
                }
              }
            }
            
          } catch (Exception e) {}
          // /////// item URL
          try {
            TagNode[] dd = t.getElementsByAttValue("class", "thead", true, false);
            if (dd != null && dd.length > 0) {
              L1: for (TagNode dt : dd) {
                try {
                  TagNode href[] = dt.getElementsByName("a", true);
                  
                  if (href != null) {
                    for (TagNode link : href) {
                      try {
                        if (removeWhiteSpaces(link.getAttributeByName("id")).toLowerCase().startsWith("postcount")) {
                          String itemURL = removeWhiteSpaces(link.getAttributeByName("href"));
                          if (itemURL != null && !itemURL.isEmpty()) {
                            item.itemUrl = itemURL.startsWith(rootPage) ? itemURL : (rootPage + itemURL);
                            break L1;
                          }
                        }
                      } catch (Exception e) {}
                    }
                  }
                } catch (Exception e) {}
              }
            }
          } catch (Exception e) {}
          
          item.postUrl = site;
          item.position = position++;
          items.add(item);
          
        }
      }
    }
    return items;
  }
}
