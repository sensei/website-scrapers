/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser.forums;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import websays.segmenter.base.BaseHTMLSegmenter;
import websays.types.clipping.SimpleItem;

public class ForosVogueSegmenter extends BaseHTMLSegmenter {
  
  private static final Logger logger = Logger.getLogger(ForosVogueSegmenter.class);
  
  public static SimpleDateFormat sdSpn = new SimpleDateFormat("MMM dd, yyyy h:mm a", new Locale("es", "ES")); // Sep 01,
  
  // 2013 8:41 am
  
  @Override
  public List<SimpleItem> segment(String content, URL url) throws Exception {
    content = segmentInit(content, url);
    ArrayList<SimpleItem> items = new ArrayList<SimpleItem>();
    HtmlCleaner cleaner = createCleaner();
    String rootPage = "http://foros.vogue.es/";
    
    TagNode root = null;
    String site = null;
    if (url != null) {
      site = url.toString();
    }
    root = cleaner.clean(content);
    
    // /////////// Main Details Item ////////////
    SimpleItem main = new SimpleItem();
    TagNode href = null;
    // ///// Extract Title
    try {
      TagNode[] heading = root.getElementsByName("h2", true);
      
      TagNode h2 = null;
      for (TagNode n : heading) {
        try {
          if (n != null && (href = n.findElementByName("a", true)) != null && href.getAttributeByName("href") != null
              && removeWhiteSpaces(href.getAttributeByName("href")).contains("/viewtopic.php?")) {
            h2 = n;
            String itemURL = removeWhiteSpaces(href.getAttributeByName("href"));
            itemURL = itemURL.substring(itemURL.indexOf("&t=") + 1);
            itemURL = itemURL.substring(0, itemURL.indexOf("&"));
            main.itemUrl = rootPage + "viewtopic.php?" + itemURL;
            
            break;
          }
        } catch (Exception e) {}
      }
      String title = removeWhiteSpaces(h2.getText().toString());
      main.title = title;
    } catch (Exception ex) {}
    try {
      TagNode[] pages = root.getElementsByAttValue("class", "pagination", true, false);
      main.comments = parseNumbers(removeWhiteSpaces(pages[0].getText().toString()), true);
    } catch (Exception ex) {}
    
    main.date = null;
    main.position = 1;
    main.postUrl = site;
    
    items.add(main);
    
    // ///////////////// Comment Items
    int position = 2;
    
    TagNode reviews[] = root.getElementsByAttValue("class", "postbody", true, false);
    if (reviews != null && reviews.length > 0) {
      
      for (TagNode t : reviews) {
        SimpleItem item = new SimpleItem();
        TagNode link = null;
        TagNode parent = null;
        TagNode profile = null;
        try {
          parent = t.getParent().getParent();
          profile = parent.getElementsByAttValue("class", "postprofile", true, false)[0];
        } catch (Exception e) {}
        // ///// Title
        try {
          TagNode h3 = t.getElementsByName("h3", false)[0];
          String ti = removeWhiteSpaces(h3.getText().toString());
          item.title = ti;
          link = h3.findElementByName("a", true);
        } catch (Exception e) {}
        
        // ////// Body
        try {
          TagNode ch[] = t.getElementsByAttValue("class", "content", true, false)[0].getAllElements(true);
          ArrayList<String> texts = new ArrayList<String>();
          TagNode cont = t.getElementsByAttValue("class", "content", true, false)[0];
          
          // un comment below lines if we should avoid BlockQuotes in the comments body
          // TagNode[] quotes=cont.getElementsByName("blockquote", true);
          // if(quotes!=null){
          // for(TagNode q:quotes){
          // try {
          // cont.removeChild(q);
          // } catch (Exception e) {}
          // }
          // }
          
          String body = removeWhiteSpaces(cont.getText().toString());
          item.body = body;
          
          for (TagNode tn : ch) {
            try {
              String txt = removeWhiteSpaces(tn.getText().toString());
              if (txt != null && !txt.trim().isEmpty()) {
                texts.add(txt);
              }
            } catch (Exception e) {}
          }
          
          if (!body.trim().isEmpty()) {
            for (String s : texts) {
              if (s != null && !s.endsWith(" ")) {
                body = body.replace(s, s + " ");
              }
            }
          }
          item.body = body.trim();
          
        } catch (Exception e) {}
        // /////// author
        String author = null;
        try {
          author = removeWhiteSpaces(t.getElementsByAttValue("class", "author", true, false)[0].getText().toString());
          int start = author.indexOf("por ") != -1 ? author.indexOf("por ") + 4 : (0);
          int end = author.indexOf("el ") != -1 ? author.indexOf("el ") : author.length();
          item.author = removeWhiteSpaces(author.substring(start, end));
        } catch (Exception e) {}
        
        // /////// author location
        try {
          TagNode[] dd = profile.getElementsByName("dd", true);
          for (TagNode tg : dd) {
            try {
              String txt = removeWhiteSpaces(tg.getText().toString());
              if (txt.toLowerCase().startsWith("ubicación:")) {
                item.authorLocation = txt.replace("Ubicación:", "").trim();
                break;
              }
            } catch (Exception e) {}
          }
        } catch (Exception e) {}
        
        // /////// comments
        try {
          TagNode[] dd = profile.getElementsByName("dd", true);
          for (TagNode tg : dd) {
            try {
              String txt = removeWhiteSpaces(tg.getText().toString());
              if (txt.toLowerCase().startsWith("mensajes:")) {
                item.comments = Integer.parseInt(txt.replace("Mensajes:", "").trim());
                break;
              }
            } catch (Exception e) {}
          }
        } catch (Exception e) {}
        // /////// date
        try {
          int start = author.indexOf("el ") != -1 ? author.indexOf("el ") + 3 : (0);
          item.date = sdSpn.parse(author.substring(start + 4).trim());
          item.timeIsReal = true;
        } catch (Exception e) {
          logger.warn("Error parsing date for " + this.getClass().getName() + "due to " + e.getMessage() + "setting date to null");
          item.date = null;
        }
        // /////// item URL
        try {
          String ln = removeWhiteSpaces(link.getAttributeByName("href"));
          String itemURL = href != null ? removeWhiteSpaces(href.getAttributeByName("href")) : null;
          if (itemURL != null) {
            item.itemUrl = itemURL + ln;
          } else {
            item.itemUrl = ln;
          }
        } catch (Exception e) {}
        item.postUrl = site;
        item.position = position++;
        items.add(item);
        
      }
    }
    return items;
  }
  
}
