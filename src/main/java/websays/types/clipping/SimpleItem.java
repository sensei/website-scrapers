/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors: Jorge Valderrama
 * Date: Jul 7, 2014
 */
package websays.types.clipping;

import java.util.Date;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.neovisionaries.i18n.LanguageAlpha3Code;

/**
 * Simplest level Clipping, without dependencies, needs to be extractable out of the project for collaborations
 * 
 * 
 * See each field for a description. Some comments:
 * <ul>
 * <li>fake=true items do not generate clippings but contain threadURL pages to be further fetched and processed.
 * <li>threadURL should contain the "next" page's URL in the thread.
 * </ul>
 * 
 * 
 * 
 */
public class SimpleItem {
  
  transient private static GsonBuilder gsonBuilder;
  transient private static Gson gson;
  
  static {
    gsonBuilder = new GsonBuilder();
    gson = gsonBuilder.create();
  }
  
  /**
   * This is the correction factor to avoid storing native floats in solr.
   */
  public static final int NATIVE_SCORE_CORRECTION_FACTOR = 10;
  
  /**
   * Websays scores are in the range 0..100, so to calculate them from native scores we use this RANGE constant.
   */
  public static final int WEBSAYS_SCORE_RANGE = 100;
  
  public String postUrl;
  /**
   * may be different from postUrl if each comment has its own url.
   */
  public String itemUrl;
  
  /**
   * Relative position of item in the page (this is relative and should not be used as key in the future: same clipping may find itself in another
   * position). By definition, parents are position 1, which is why we set this as default.
   * 
   * WARNING: position is very important, since page_url+position is a typical unique key for items
   */
  public int position = 1;
  
  public Date date;
  
  /**
   * This boolean marks if the date will be used for hashkey generation (it is used to generate the externalID of the clippings) or not. Set this
   * field to false if you need to discard date when generating the hashkey.
   */
  public boolean dateIsReliableForHash = true;
  
  public String title;
  
  public String body;
  
  public String author;
  
  public String authorLocation;
  
  /**
   * This is the information provider's (webpage, forum....) native score. It is multiplied by 10 to avoid float values. To display it as its original
   * value, divide it by 10
   */
  private Integer score_native = null;
  
  /**
   * This is websays normalized score. It is work out as follows: (score_native / MAX_PAGE_SCORE) * 100 where MAX_PAGE_SCORE is the maximun score of
   * the information source (i.e. tripadvisor would be 5)
   */
  private Integer score_websays = null;
  public Integer likes;
  public Integer shares;
  public Integer reviews;
  public Integer votes;
  public Integer comments;
  public Integer followers;
  public String APIObjectID; // the ID in the native API where this comes from (eg Facebook post id)
  public String APIAuthorID; // the author ID in the native API where this comes from (eg Facebook user id)
  /**
   * APIIDs of authors to which the post or comment is addressed (WARNING TODO: sometimes can be more than one, but only first stored for now)
   */
  public String APIToAuthorID;
  public String parentID; // the ID in the native API where this comes from (eg Facebook post id)
  public String imageLink;
  public String imageLinkHQ;
  public Double latitude;
  public Double longitude;
  
  /**
   * threadId identifies all simpleItems of same thread (can be used to derive a thread unique key). Note that should be stable across different
   * crawls (eg a comment found today should have the same threadID as a comment found in a previous execution if they refer to the same thread.
   * 
   * Note. when this is null, it will be up to the application to decide when and how to build threadIDs.
   * 
   */
  public String threadId;
  
  /**
   * Lets crawlers know that there are further URLs to visit, fetch and process
   */
  public List<String> threadURLs;
  
  public Integer views;
  public Integer favorites;
  public Boolean isComment; // true when is a comment of previous post, false when is a post. when null, DocumentSegmenter will make a choice
  public String hashKeyPrefix;
  /**
   * Sometimes there is no parent but we need to store parent info to be inherited by the children (e.g. compositeID). If so, create a fake list item
   * as the first element of the list, with fakeitem=true.
   */
  public Boolean fakeItem = false;
  public Boolean timeIsReal = false; // new field, check all parsers to set this to TRUE when the time part of date (the hours, minutes, secs) is
                                     // real, otherwise will be truncated
  
  // New feature (Language knew) Set Language if comes in Simple Item
  public LanguageTag language = null; // Used when we know the language in advance, because the clippings are generated by API request or
  
  // Parser
  
  // Moved from Clipping.java to improve process, related with new feature (Language Knew)
  public enum LanguageTag {
    afrikaans, albanian, alemannisch, alemannic, amharic, arabic, armenian, asturian, basque, belarus, belarusian, bosnian, breton, bulgarian, catalan, chinese, chuvash, croatian, czech, danish, dutch, english, esperanto, estonian, faroese, farsi, finnish, french, galician, german, greek, haitian, hebrew, hindi, hungarian, icelandic, ido, indonesian, interlingua, irish, italian, japanese, javanese, korean, kurdish, latin, latvian, limburgish, lituanian, low, luxembourgish, macedonian, malay, maltese, marathi, min, norwegian_bokmal, norwegian_nynorsk, occitan, ossetic, persian, philippine, polish, portuguese, romanian, russian, scots, serbian, serbo, slovak, slovenian, spanish, swahili, swedish, tagalog, thai, tamil, tatar, turkish, ukrainian, ukranian, vietnamese, walloon, welsh, western, yiddish, UNKNOWN;
    
    /**
     * to ISO 639-1 (lowercase)
     * 
     */
    public static String langTag2short(LanguageTag langshort) {
      
      if (langshort == null) {
        return null;
      }
      String tempShortName = langshort.name();
      if (tempShortName == null || tempShortName.length() < 2) {
        return null;
      }
      
      /********************************** Exceptions hacks **********************************************/
      /* Farsi is a synonim for persian. */
      if (LanguageTag.farsi.name().equalsIgnoreCase(tempShortName)) {
        tempShortName = LanguageTag.persian.name();
      }
      
      /*
       * This language is not in the list we use. It must be resolved directly
       */
      if (LanguageTag.alemannisch.name().equalsIgnoreCase(tempShortName) || LanguageTag.alemannic.name().equalsIgnoreCase(tempShortName)) {
        return "gsw";
      }
      /********************************** Exceptions hacks **********************************************/
      
      /* Convert the languagetag name to a regex pattern suitable for the LanguageAlpha3Code object */
      char firstChar = tempShortName.charAt(0);
      
      if (Character.isLowerCase(firstChar)) {
        tempShortName = "[" + firstChar + Character.toUpperCase(firstChar) + "]" + tempShortName.substring(1);
      }
      
      /* Find the language */
      List<LanguageAlpha3Code> list = LanguageAlpha3Code.findByName(tempShortName);
      if (list != null && list.size() > 0 && list.get(0).getAlpha2() != null) {
        return list.get(0).getAlpha2().name();
      } else {
        return null;
      }
      
      /*
       * OLD WAY, can be erased switch (langshort) { case basque: return "eu"; case english: return "en"; case spanish: return "es"; case french:
       * return "fr"; case catalan: return "ca"; case italian: return "it"; case galician: return "gl"; case portuguese: return "pt"; case russian:
       * return "ru"; case german: return "de"; case dutch: return "nl"; default: return null; }
       */
    }
    
    /* TODO: Deprecate this code , use already made java libraries instead */
    
    /**
     * @param langshort
     * @return default country to every language (for SE markets)
     */
    public static String langTag2country(LanguageTag langshort) {
      /*
       * GOOD idea that doesn't work, needs a little bit more of understanding how locale works Locale tempLoc = new Locale(langshort.name()); String
       * tempCountryCode = tempLoc.getCountry().toLowerCase(); return ("".equals(tempCountryCode) ? null : tempCountryCode);
       */
      
      switch (langshort) {
        case english:
          return "gb";
        case spanish:
          return "es";
        case catalan:
          return "es";
        case basque:
          return "es";
        case french:
          return "fr";
        case italian:
          return "it";
        case galician:
          return "es";
        case portuguese:
          return "pt";
        case russian:
          return "ru";
        case german:
          return "de";
        case dutch:
          return "nl";
        default:
          return null;
      }
      
    }
  }
  
  public List<String> source_extra_metadata;
  
  /**
   * Given a native integer score, it updates score_native and score_websays accordingly
   * 
   * @see setScores(float score_native, int max_native)
   * 
   * @param score_native
   *          the native score from the information source.
   * @param max_native
   *          the max value for the information source (i.e. tripadvisor would be 5)
   */
  public void setScores(int score_native, int max_native) {
    this.score_native = score_native * NATIVE_SCORE_CORRECTION_FACTOR;
    this.score_websays = (new Float(((float) score_native / (float) max_native) * WEBSAYS_SCORE_RANGE)).intValue();
    // setScore_websays(score_native, max_native);
  }
  
  /**
   * Given a native float score, it updates score_native and score_websays accordingly
   * 
   * @param score_native
   *          the native score from the information source.
   * @param max_native
   *          the max value for the information source (i.e. tripadvisor would be 5)
   */
  public void setScores(float score_native, int max_native) {
    this.score_native = (new Float(score_native * NATIVE_SCORE_CORRECTION_FACTOR)).intValue();
    this.score_websays = (new Float((score_native / max_native) * WEBSAYS_SCORE_RANGE)).intValue();
    // setScore_websays(new Float(score_native).intValue(), max_native);
  }
  
  /**
   * @return the score_websays
   */
  public Integer getScore_websays() {
    return score_websays;
  }
  
  /**
   * @return the score_native
   */
  public Integer getScore_native() {
    return score_native;
  }
  
  /**
   * Prints the SimpleItem information in JSON format.
   * 
   * @return a String that contains all the information in the SimpleItem in JSON format.
   */
  public String toJson() {
    return SimpleItem.gson.toJson(this);
  }
  
  /**
   * Converts a JSON string into a SimpleItem object
   * 
   * @param parJson
   *          the serialized SimpleItem in JSON
   * @return an object that contains the information that was in the JSON.
   */
  public static SimpleItem fromJson(String parJson) {
    return SimpleItem.gson.fromJson(parJson, SimpleItem.class);
  }
}
