/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.utils.misc;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.log4j.Logger;

public class DateUtilsWebsays {

  private static final Logger logger = Logger.getLogger(DateUtilsWebsays.class);

  // TODO: phase this out, force code to specify:
  public static final String DEFAULT_TIMEZONE_NAME = "Europe/Madrid"; // used whenever unspecified or null
  public static final TimeZone DEFAULT_TIMEZONE = TimeZone.getTimeZone(DEFAULT_TIMEZONE_NAME);

  public static final TimeZone utcTimezone = TimeZone.getTimeZone("UTC");
  public static final String ISO8601_format = "yyyy-MM-dd'T'HH:mm:ss'Z'";
  public static final SimpleDateFormat sdfUTC = new SimpleDateFormat(ISO8601_format, Locale.US); // timezone is set in constructor

  /**
   * This is the conversion factor between UNIX time (in seconds) to java time (in miliseconds)
   */
  private static final long UNIX_TIME_FACTOR = 1000;

  static {
    sdfUTC.setTimeZone(utcTimezone);
  }

  // ============================
  // TIME AND DATES:
  // ============================

  /**
   * Given a date converts it in a String in the ISO8601 format
   *
   * @param date
   * @return
   */
  public static synchronized String toISO8601(Date date) {
    String nowAsISO = sdfUTC.format(date);
    return nowAsISO;
  }

  /**
   * Given a String in the ISO8601 format converts it into a Date object.
   *
   * @param dateString
   * @return the date represented by the String or null if the String hadn't the correct format.
   */
  public static synchronized Date fromISO8601(String dateString) {
    Date date = null;
    try {
      date = sdfUTC.parse(dateString);
    } catch (ParseException e) {
      return null;
    }
    return date;
  }

  /**
   * Converts a Unix time (seconds from the Epoch) into a java.util.Date object.
   *
   * @param rawDate
   *          the date in String format.
   * @return a Date object that represents the date stored in rawDate. Null if rawDate is not parsable as a number.
   */
  public static Date fromUnixTime2Date(String rawDate) {
    long unixDate = 0;
    Date res = null;
    try {
      String effectiveRawDate = rawDate;
      if (rawDate.contains(".")) {
        effectiveRawDate = rawDate.substring(0, rawDate.indexOf("."));
      }
      unixDate = Long.parseLong(effectiveRawDate);
      res = new java.util.Date(unixDate * UNIX_TIME_FACTOR);

    } catch (NumberFormatException nfe) {
      logger.warn("Error [" + nfe.getMessage() + "] while parsing the string [" + rawDate
          + "] as a long. Date can't be created. Returning null.");
    }
    return res;
  }

  public static synchronized void calToBeginningOfHour(Calendar c) {
    c.set(Calendar.MINUTE, 0);
    c.set(Calendar.SECOND, 0);
    c.set(Calendar.MILLISECOND, 000);
  }

  public static synchronized void calToBeginningOfDay(Calendar c) {
    c.set(Calendar.HOUR_OF_DAY, 0);
    c.set(Calendar.MINUTE, 0);
    c.set(Calendar.SECOND, 0);
    c.set(Calendar.MILLISECOND, 000);
  }

  public static synchronized void calToEndOfDay(Calendar c) {
    // System.out.println(c.getTimeZone());
    // SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
    // sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
    // System.out.println(sdf.format(c.getTime()));
    c.set(Calendar.HOUR_OF_DAY, 23);
    c.set(Calendar.MINUTE, 59);
    c.set(Calendar.SECOND, 59);
    c.set(Calendar.MILLISECOND, 999);
  }

  public static synchronized void calToEndOfYear(Calendar c) {
    c.set(Calendar.HOUR_OF_DAY, 23);
    c.set(Calendar.MINUTE, 59);
    c.set(Calendar.SECOND, 59);
    c.set(Calendar.MILLISECOND, 999);
    c.set(Calendar.MONTH, 11);
    c.set(Calendar.DAY_OF_MONTH, 31);
  }

  public static synchronized void calToEndOfMonth(Calendar c) {
    int lastDate = c.getActualMaximum(Calendar.DATE);
    c.set(Calendar.DATE, lastDate);
    c.set(Calendar.HOUR_OF_DAY, 23);
    c.set(Calendar.MINUTE, 59);
    c.set(Calendar.SECOND, 59);
    c.set(Calendar.MILLISECOND, 999);
  }

  public static synchronized int lastDayOfMonth(Calendar c) {
    // final int[] daysPerMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31,
    // 30, 31};
    // int month = c.get(Calendar.MONTH);
    // return daysPerMonth[month];
    return c.getActualMaximum(Calendar.DATE);
  }

  public static synchronized void calToLastDayOfMonth(Calendar c) {
    int day = lastDayOfMonth(c);
    c.set(Calendar.DATE, day);
  }

  public static synchronized boolean isLastDayOfMonth(Date d, TimeZone timezone) {
    Calendar c = getCalendarInstance(timezone);
    c.setTime(d);
    int ld = lastDayOfMonth(c);
    return ld == c.get(Calendar.DAY_OF_MONTH);
  }

  public static synchronized Date addMonthsAndDays(Date date, int months, int days, TimeZone timezone) {
    Calendar c = getCalendarInstance(date, timezone);
    c.add(Calendar.MONTH, months);
    c.add(Calendar.DAY_OF_YEAR, days);
    return c.getTime();
  }

  /**
   * @param now
   * @return
   */
  public static synchronized Date dateOneWeekAgo(Date now, TimeZone timezone) {
    Calendar c = getCalendarInstance(timezone);
    c.setTime(now);
    c.add(Calendar.DAY_OF_YEAR, -7);
    calToBeginningOfDay(c);
    return c.getTime();
  }

  public static synchronized Date dateEndOfPreviousDay(Date cal, TimeZone timezone) {
    cal = dateBeginningOfDay(cal, timezone);
    // 2016-03-04 to avoid using addMilliseconds
    // cal = DateUtils.addMilliseconds(cal, -1); // end of yesterday
    cal.setTime(cal.getTime() - 1);
    return cal;
  }

  public static synchronized Date dateEndOfDay(Date cal, TimeZone timezone) {
    Calendar c = Calendar.getInstance(guessNullTimeZone(timezone));
    c.setTime(cal);
    calToEndOfDay(c);
    return c.getTime();
  }

  public static synchronized Date dateEndOfYear(Date cal, TimeZone timezone) {
    // THIS BLOWS UP IN alpha: cal = DateUtils.ceiling(cal,
    // Calendar.DAY_OF_MONTH); // end of today
    Calendar c = Calendar.getInstance(guessNullTimeZone(timezone));
    c.setTime(cal);
    calToEndOfYear(c);
    return c.getTime();
  }

  public static synchronized Date dateBeginningOfDay(Date date, TimeZone timezone) {
    Calendar calendar = Calendar.getInstance(guessNullTimeZone(timezone));
    calendar.setTime(date);
    calToBeginningOfDay(calendar);

    return calendar.getTime();
  }

  public static synchronized Date dateBeginningOfHour(Date date, TimeZone timezone) {
    Calendar calendar = Calendar.getInstance(guessNullTimeZone(timezone));
    calendar.setTime(date);
    calToBeginningOfHour(calendar);
    return calendar.getTime();
  }

  public static synchronized Date dateBeginningOfYear(Date date, TimeZone timezone) {
    Calendar calendar = Calendar.getInstance(guessNullTimeZone(timezone));
    calendar.setTime(date);
    calendar.set(Calendar.MONTH, 0);
    calendar.set(Calendar.DATE, 1);
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);

    return calendar.getTime();
  }

  public static synchronized Date dateEndOfMonth(Date date, TimeZone timezone) {
    return dateEndOfMonth(date, 0, timezone);
  }

  public static synchronized Date dateEndOfMonth(Date date, int addMonths, TimeZone timezone) {
    Calendar cal = Calendar.getInstance(guessNullTimeZone(timezone));
    cal.setTime(date);
    cal.add(Calendar.MONTH, addMonths);
    calToEndOfMonth(cal);
    return cal.getTime();
  }

  public static synchronized Date dateBeginningOfMonth(Date cal, TimeZone timezone) {
    return dateBeginningOfMonth(cal, 0, timezone);
  }

  /**
   * @param cal
   * @param addMonths
   *          0 for beginning of this month, -1 for beginning last month, etc.
   * @return
   */
  public static synchronized Date dateBeginningOfMonth(Date cal, int addMonths, TimeZone timezone) {
    Calendar c = Calendar.getInstance(guessNullTimeZone(timezone));
    c.setTime(cal);
    c.add(Calendar.MONTH, addMonths);
    c.set(Calendar.DAY_OF_MONTH, 1);
    c.set(Calendar.HOUR_OF_DAY, 0);
    c.set(Calendar.MINUTE, 0);
    c.set(Calendar.SECOND, 0);
    c.set(Calendar.MILLISECOND, 0);
    return c.getTime();
  }

  public static synchronized Date dateBeginningOfWeek(Date cal, TimeZone timezone) {
    Calendar c = Calendar.getInstance(guessNullTimeZone(timezone));
    c.setTime(cal);
    c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
    c.set(Calendar.HOUR_OF_DAY, 0);
    c.set(Calendar.MINUTE, 0);
    c.set(Calendar.SECOND, 0);
    c.set(Calendar.MILLISECOND, 0);
    return c.getTime();
  }

  public static synchronized boolean isSameDay(Calendar c1, Calendar c2) {
    int[] f = {Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH};
    for (int i : f) {
      if (c1.get(i) != c2.get(i)) {
        return false;
      }
    }
    return true;
  }

  public static synchronized boolean isSameDay(Date c1, Date c2, TimeZone tz) {
    Calendar cal = getCalendarInstance(tz);
    cal.setTime(c1);
    Calendar cal2 = getCalendarInstance(tz);
    cal2.setTime(c2);
    return isSameDay(cal, cal2);
  }

  public static synchronized boolean isSameMonth(Calendar c1, Calendar c2) {
    int[] f = {Calendar.YEAR, Calendar.MONTH};
    for (int i : f) {
      if (c1.get(i) != c2.get(i)) {
        return false;
      }
    }
    return true;
  }

  public static synchronized boolean isSameMonth(Date d1, Date d2, TimeZone timezone) {
    Calendar c1 = getCalendarInstance(timezone);
    c1.setTime(d1);
    Calendar c2 = getCalendarInstance(timezone);
    c2.setTime(d2);
    return isSameMonth(c1, c2);
  }

  /**
   * Works out how many day are there between start and end.
   *
   * @param start
   *          time of start
   * @param end
   *          time of end
   * @return number of days between the two dates.
   */
  public static int getHowManyDays(Date start, Date end) {
    // the +1 makes a whole day when starting form 0:0 to 23:59
    int howManyDays = (int) ((end.getTime() - start.getTime() + 1) / (1000 * 60 * 60 * 24));
    return howManyDays;
  }

  /**
   *
   * @param gap
   * @return
   * @throws ParseException
   */
  public static synchronized int getHowManyMonths(Date start, Date end, TimeZone timezone) {
    Calendar s = getCalendarInstance(timezone);
    Calendar e = getCalendarInstance(timezone);
    s.setTime(start);
    e.setTime(end);

    int months = 12 * (e.get(Calendar.YEAR) - s.get(Calendar.YEAR));
    months += (e.get(Calendar.MONTH) - s.get(Calendar.MONTH));
    return months;
  }

  public static synchronized Date addDays(Date start, int days, TimeZone timezone) {
    Calendar cal = getCalendarInstance(timezone);
    cal.setTime(start);
    cal.add(Calendar.DATE, days); // minus number would decrement the days
    return cal.getTime();
  }

  public static synchronized Date addHours(Date start, int hours, TimeZone timezone) {
    Calendar cal = getCalendarInstance(timezone);
    cal.setTime(start);
    cal.add(Calendar.HOUR_OF_DAY, hours); // minus number would decrement the hours
    return cal.getTime();
  }

  public static synchronized Date addMinutes(Date start, int minutes, TimeZone timezone) {
    Calendar cal = getCalendarInstance(timezone);
    cal.setTime(start);
    cal.add(Calendar.MINUTE, minutes); // minus number would decrement the minutes
    return cal.getTime();
  }

  public static synchronized int getDayOfMonth(Date start, TimeZone timezone) {
    Calendar cal = getCalendarInstance(timezone);
    cal.setTime(start);
    return cal.get(Calendar.DAY_OF_MONTH);
  }

  /**
   * THIS SHOULD GO AWAY, force people to decide timezone or use default by not sending one
   *
   * @param timezone
   * @return
   */

  private static synchronized TimeZone guessNullTimeZone(TimeZone timezone) {
    if (timezone == null) {
      return DEFAULT_TIMEZONE;
    } else {
      return timezone;
    }
  }

  public static synchronized Calendar getCalendarInstance(TimeZone timezone) {
    return Calendar.getInstance(guessNullTimeZone(timezone));
  }

  public static synchronized Calendar getCalendarInstance(Date date, TimeZone timezone) {
    Calendar cal = getCalendarInstance(timezone);
    cal.setTime(date);
    return cal;
  }

  public static synchronized SimpleDateFormat getSimpleDateFormat(String simpleDateFormatString, TimeZone timezone) {
    SimpleDateFormat sdf = new SimpleDateFormat(simpleDateFormatString);
    sdf.setTimeZone(guessNullTimeZone(timezone));
    return sdf;
  }

  /**
   * Returns a simpledateformat with a timezone and "EEE MMM dd HH:mm:ss zzz yyyy"
   * 
   * @param timezone
   * @return
   */
  public static synchronized SimpleDateFormat getSimpleDateFormat(TimeZone timezone) {
    SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
    sdf.setTimeZone(guessNullTimeZone(timezone));
    return sdf;
  }

  public static synchronized int getYear(Date date, TimeZone timezone) {
    Calendar cal = getCalendarInstance(timezone);
    return cal.get(Calendar.YEAR);
  }

  public static synchronized int getMonth(Date date, TimeZone timezone) {
    Calendar cal = getCalendarInstance(timezone);
    return cal.get(Calendar.MONTH);
  }

  /**
   * Works out whether the given Calendar is inside the period of time defined by start and end calendars
   *
   * @param dC
   *          the Calendar we want to know if it is in the period.
   * @param staC
   *          the starting point of the period.
   * @param endC
   *          the ending point of the period.
   * @return True if dC is between staC and endC. False otherwise
   */
  public static synchronized boolean isInPeriod(Calendar dC, Calendar staC, Calendar endC) {
    return (dC.compareTo(staC) >= 0 && dC.compareTo(endC) <= 0);
  }

  /**
   * @param year
   * @param month
   *          1 - 12
   * @param day
   *          1 - last day of month
   * @param tz
   * @return
   */
  public static synchronized Calendar getCalendar(int year, int month, int day, TimeZone tz) {
    Calendar cal = getCalendarInstance(tz);
    cal.set(year, month - 1, day);
    calToBeginningOfDay(cal);
    return cal;
  }

  /**
   * Given a Date object, works out the previous month's beginning date (i.e. first day of the month at 00:00). It uses DEFAULT_TIMEZONE as its
   * timezone. Use getBeginningOfLastMonth(Date parDate, TimeZone parTZ) if timezone is known (highly advised)
   *
   * @param parDate
   *          a date to get the previous month's beginning
   * @return a Date object that is the day 1 of the previous month at 00:00 in DEFAULT_TIMEZONE
   */
  public static Date getBeginningOfLastMonth(Date parDate) {
    return getBeginningOfLastMonth(parDate, DEFAULT_TIMEZONE);
  }

  /**
   *
   * Given a Date object, works out the previous month's beginning date (i.e. first day of the month at 00:00).
   *
   * @param parDate
   *          a date to get the previous month's beginning
   * @param parTZ
   *          TimeZone used to make date calculations.
   * @return a Date object that is the day 1 of the previous month at 00:00 in parTZ timezone.
   */
  public static Date getBeginningOfLastMonth(Date parDate, TimeZone parTZ) {
    Calendar c = Calendar.getInstance(parTZ);
    c.setTime(parDate);
    c.add(Calendar.MONTH, -1);
    return DateUtilsWebsays.dateBeginningOfDay(DateUtilsWebsays.dateBeginningOfMonth(c.getTime(), parTZ), parTZ);
  }

  /**
   * Given a Date object, works out the previous month's ending date (i.e. last day of the month at 23:59). It uses DEFAULT_TIMEZONE as its timezone.
   * Use getEndOfLastMonth(Date parDate, TimeZone parTZ) if timezone is known (highly advised)
   *
   * @param parDate
   *          a date to get the previous month's ending
   * @return a Date object that is the last day of the previous month at 23:59 in DEFAULT_TIMEZONE
   */
  public static Date getEndOfLastMonth(Date parDate) {
    return getEndOfLastMonth(parDate, DEFAULT_TIMEZONE);
  }

  /**
   *
   * Given a Date object, works out the previous month's ending date (i.e. last day of the month at 23:59).
   *
   * @param parDate
   *          a date to get the previous month's ending
   * @param parTZ
   *          TimeZone used to make date calculations.
   * @return a Date object that is the last day of the previous month at 23:59 in parTZ timezone.
   */
  public static Date getEndOfLastMonth(Date parDate, TimeZone parTZ) {
    Calendar c = Calendar.getInstance(parTZ);
    c.setTime(parDate);
    c.add(Calendar.MONTH, -1);
    return DateUtilsWebsays.dateEndOfDay(DateUtilsWebsays.dateEndOfMonth(c.getTime(), parTZ), parTZ);
  }

  /**
   * Given a Date object, works out the previous year's beginning date (i.e. first day of the year at 00:00). It uses DEFAULT_TIMEZONE as its
   * timezone. Use getBeginningOfLastYear(Date parDate, TimeZone parTZ) if timezone is known (highly advised)
   *
   * @param parDate
   *          a date to get the previous year's beginning
   * @return a Date object that is the first day of the previous year at 00:00 in DEFAULT_TIMEZONE
   */
  public static Date getBeginningOfLastYear(Date parDate) {
    return getBeginningOfLastYear(parDate, DEFAULT_TIMEZONE);
  }

  /**
   *
   * Given a Date object, works out the previous year's beginning date (i.e. first day of the year at 00:00).
   *
   * @param parDate
   *          a date to get the previous year's beginning
   * @param parTZ
   *          TimeZone used to make date calculations.
   * @return a Date object that is the day 1 of the previous year at 00:00 in parTZ timezone.
   */
  public static Date getBeginningOfLastYear(Date parDate, TimeZone parTZ) {
    Calendar c = Calendar.getInstance(parTZ);
    c.setTime(parDate);
    c.add(Calendar.YEAR, -1);
    return DateUtilsWebsays.dateBeginningOfDay(DateUtilsWebsays.dateBeginningOfYear(c.getTime(), parTZ), parTZ);
  }

  /**
   * Given a Date object, works out the previous year's ending date (i.e. last day of the year at 23:59). It uses DEFAULT_TIMEZONE as its timezone.
   * Use getEndOfLastYear(Date parDate, TimeZone parTZ) if timezone is known (highly advised)
   *
   * @param parDate
   *          a date to get the previous year's end
   * @return a Date object that is the last day of the previous year at 23:59 in DEFAULT_TIMEZONE
   */
  public static Date getEndOfLastYear(Date parDate) {
    return getEndOfLastYear(parDate, DEFAULT_TIMEZONE);
  }

  /**
   *
   * Given a Date object, works out the previous year's ending date (i.e. last day of the year at 23:59).
   *
   * @param parDate
   *          a date to get the previous year's end
   * @param parTZ
   *          TimeZone used to make date calculations.
   * @return a Date object that is the last day of the previous year at 23:59 in parTZ timezone.
   */
  public static Date getEndOfLastYear(Date parDate, TimeZone parTZ) {
    Calendar c = Calendar.getInstance(parTZ);
    c.setTime(parDate);
    c.add(Calendar.YEAR, -1);
    return DateUtilsWebsays.dateEndOfDay(DateUtilsWebsays.dateEndOfYear(c.getTime(), parTZ), parTZ);
  }

  /**
   * Given a period of time, it gets the beginning of the immediately previous period of the same length expressed in days (i.e. if the period passed
   * to the method is May the 10th to May the 14th (5 days) it returns May the 5th at 00:00). This method uses the DEFAULT_TIMEZONE. If you know the
   * timezone, please use getBeginningOfPreviousDayPeriod(Date parStartDate, Date parEndDate, TimeZone parTZ) instead (highly adviced)
   *
   * @param parStartDate
   *          the beginning of the period of days.
   * @param parEndDate
   *          the end of the period of days.
   * @return a date that is the beginning of the previous period of the same amount of days that the one passed as parameters.
   */
  public static Date getBeginningOfPreviousDayPeriod(Date parStartDate, Date parEndDate) {
    return getBeginningOfPreviousDayPeriod(parStartDate, parEndDate, DEFAULT_TIMEZONE);
  }

  /**
   * Given a period of time, it gets the beginning of the immediately previous period of the same length expressed in days (i.e. if the period passed
   * to the method is May the 10th to May the 14th (5 days) it returns May the 5th at 00:00).
   *
   * @param parStartDate
   *          the beginning of the period of days.
   * @param parEndDate
   *          the end of the period of days.
   * @param parTZ
   *          the timezone that will be used for calendar calculations.
   * @return a date that is the beginning of the previous period of the same amount of days that the one passed as parameters.
   */
  public static Date getBeginningOfPreviousDayPeriod(Date parStartDate, Date parEndDate, TimeZone parTZ) {

    int howManyDays = DateUtilsWebsays.getHowManyDays(parStartDate, parEndDate);
    howManyDays = (howManyDays == 0) ? 1 : howManyDays;
    Calendar c = Calendar.getInstance(parTZ);
    c.setTime(parStartDate);
    c.add(Calendar.DATE, -howManyDays);
    return DateUtilsWebsays.dateBeginningOfDay(c.getTime(), parTZ);
  }

  /**
   * Given a period of time, it gets the end of the immediately previous period of the same length expressed in days (i.e. if the period passed to the
   * method is May the 10th to May the 14th (5 days) it returns May the 9th at 23_59). This method uses the DEFAULT_TIMEZONE. If you know the
   * timezone, please use getEndOfPreviousDayPeriod(Date parStartDate, Date parEndDate, TimeZone parTZ) instead (highly adviced)
   *
   * @param parStartDate
   *          the beginning of the period of days.
   * @param parEndDate
   *          the end of the period of days.
   * @return a date that is the end of the previous period of the same amount of days that the one passed as parameters.
   */
  public static Date getEndOfPreviousDayPeriod(Date parStartDate, Date parEndDate) {
    return getEndOfPreviousDayPeriod(parStartDate, parEndDate, DEFAULT_TIMEZONE);
  }

  /**
   * Given a period of time, it gets the end of the immediately previous period (which is just the end of the day before parStartDate) (i.e. if the
   * period passed to the method is May the 10th to May the 14th (5 days) it returns May the 9th at 23:59).
   *
   * @param parStartDate
   *          the beginning of the period of days.
   * @param parEndDate
   *          the end of the period of days.
   * @param parTZ
   *          the timezone that will be used for calendar calculations.
   * @return a date that is the end of the previous period of the same amount of days that the one passed as parameters.
   */
  public static Date getEndOfPreviousDayPeriod(Date parStartDate, Date ignored, TimeZone parTZ) {
    Calendar c = Calendar.getInstance(parTZ);
    c.setTime(parStartDate);
    c.add(Calendar.DAY_OF_YEAR, -1);
    return DateUtilsWebsays.dateEndOfDay(c.getTime(), parTZ);
  }

}
