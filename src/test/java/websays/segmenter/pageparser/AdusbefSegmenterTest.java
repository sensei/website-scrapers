/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class AdusbefSegmenterTest extends SegmenterTest {
  
  /**
   * Test of segment method, of class AdusbefSegmenter.
   */
  @Test
  public void testAdusbefSegmenterRighURL() throws Exception {
    
    URL url = new URL("http://forum.adusbef.it/leggi-int.asp?id=369943");
    
    StringBuilder content = readPageFromResource("forum.adusbef.it");
    
    Calendar cal = Calendar.getInstance();
    HTMLDocumentSegmenter seg = new AdusbefSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 4;
    assertEquals(expectedResults, result.size());
    
    // ///////// Test first comment item //////////////
    
    SimpleItem main = result.get(0);
    assertEquals("contentid369943", main.threadId);
    assertEquals(null, main.title);
    assertEquals("Claumaff", main.author);
    assertEquals(
        "Su una sim ho l’opzione 100 ore per navigare con la chiavetta, ieri ricarico 20 euro per far fronte ai 19 euro del costo mensile dell’opzione e dopo circa un’ora navigo per 8 minuti, poi la connessione termina e non si riconnetterà più. In serata chiamo il 160 che mi dice che ho finito il credito di 20 euro in quegli 8 minuti di connessione perché non ho atteso il messaggio di rinnovo dell’opzione.Le mie domande sono queste:1. I tempi di rinnovo dell’opzione sono volutamente lunghi per indurre i cienti all’errore?2. La tariffa dati, fuori le 100 ore dell’opzione, di 0,55 euro per Kb è eticamente corretta?",
        main.body);
    cal.set(2014, 0, 8);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), main.date);
    assertEquals(null, main.itemUrl);
    assertEquals(url.toString(), main.postUrl);
    assertEquals(1, main.position);
    assertFalse(main.fakeItem);
    
    // /////// Test Second comment item //////////////
    
    SimpleItem second = result.get(1);
    assertEquals("contentid369949", second.threadId);
    assertEquals(null, main.title);
    assertEquals("menelaoo", second.author);
    assertEquals(
        "Guarda, è successo anche a me sia con tim che con postemobile (che ho ancora adesso e che trovo la migliorr come qualita prezzo). Ho fatto fuoco e fiamme ma poi sono andato a leggere i fogli informativi e...era tutto scritto chiaro. Colpa nostra, purtroppo. Bisogna sempre aspettare il messaggio di rinnovo.  Il costo è effettivamente altissimo ed è una stortura tutta italica. E infatti lo applicano tutte le compagnie felici e contente....proprio quando si è in quel lmbo del mancato rinnovo. ",
        second.body);
    assertEquals(null, second.itemUrl);
    cal.set(2010, 0, 29);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), second.date);
    assertEquals(url.toString(), second.postUrl);
    assertEquals(2, second.position);
    assertFalse(second.fakeItem);
    
  }
  
  /**
   * Test of segment method, of class AdusbefSegmenter.
   */
  @Test
  public void testAdusbefSegmenterWrongURL() throws Exception {
    
    URL url = new URL("http://forum.adusbef.it/leggi.asp?id=369943");
    
    HTMLDocumentSegmenter seg = new AdusbefSegmenter();
    
    List<SimpleItem> result = seg.segment("", url);
    
    int expectedResults = 1;
    assertEquals(expectedResults, result.size());
    
    // ///////// Test first comment item //////////////
    
    SimpleItem main = result.get(0);
    assertEquals(null, main.threadId);
    assertEquals(null, main.title);
    assertEquals(null, main.author);
    assertEquals(null, main.body);
    assertEqualDates(null, main.date);
    ArrayList<String> listUrls = new ArrayList<String>();
    listUrls.add(url.toString());
    assertEquals(listUrls, main.threadURLs);
    assertEquals(null, main.postUrl);
    assertEquals(1, main.position);
    assertTrue(main.fakeItem);
  }
}
