/**
 * Websays Opinion Analytics Engine 
 * 
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 * 
 * Primary Author: Jorge Valderrama
 * Contributors: 
 * Date: Jul 14, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.net.URL;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;
import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

/**
 * Test class
 **/
public class AfiliadoSegmenterTest extends BaseDocumentSegmenterTest {
  
  /**
   * Test complete of segment method, of class AfiliadoSegmenter.
   */
  @Test
  public void testAfiliadoSegmenter() throws Exception {
    
    URL url = new URL("http://www.afiliado.com/foro/viewtopic.php?f=15&t=10629");
    
    StringBuilder content = readPageFromResource("afiliado.com.1.html");
    
    Calendar cal = Calendar.getInstance();
    HTMLDocumentSegmenter seg = new AfiliadoSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 10;
    assertEquals(expectedResults, result.size());
    
    // /////// Test First comment item //////////////
    SimpleItem main = result.get(0);
    assertEquals(" Tradedoubler me está decepcionando", main.title);
    assertEquals("ttekno", main.author);
    assertEquals(
        StringEscapeUtils
            .unescapeHtml4("Buenas tardes,No quería llegar a escribir por aquí pero no me queda más remedio. Llevo experimentando cosas bastante raras en el último año, ventas que no se cuentan, supuestas &quot;deduplicaciones&quot; y tráfico que no genera ingresos, y que es un tráfico por el cual estoy pagando dinero.El tema de las ventas de viajes ya es una guerra perdida, no se que clausulas han aceptado pero han dejado vendidos por completo a los afiliados. Cada vez se generan menos ventas, y las que se generan, la mitad son denegadas... me llevo quejando en varias ocasiones y no estan haciendo nada, no entiendo bien que pasa. Si no hay afiliados no hay clientes, porque no se castiga a los clientes que no pagan como es debido??En éste momento estoy a la espera de que me respondan porque misteriosamente he dejado de cobrar comisiones generando varios cientos de visitantes únicos por día... no se, estoy descontento en general, lo que no puede ser es que uno esté volcado con Tradedoubler y despues ellos no te respondan con la misma moneda.Un saludo"),
        main.body);
    // cal.set(2010, 7, 21, 10, 32);// Year, Month(zero based), Date
    // assertEqualDates(cal.getTime(), main.date);
    assertEquals(url.toString(), main.postUrl);
    assertEquals(1, main.position);
    assertEquals("http://www.afiliado.com/foro/viewtopic.php?f=15&t=10629&start=10", main.threadURLs.get(0));
  }
  
  /**
   * Test last page of segment method, of class AfiliadoSegmenter.
   */
  @Test
  public void testLastPageAfiliadoSegmenter() throws Exception {
    
    URL url = new URL("http://www.afiliado.com/foro/viewtopic.php?f=15&t=10629");
    StringBuilder content = readPageFromResource("afiliado.com.1.html");
    HTMLDocumentSegmenter seg = new AfiliadoSegmenter();
    List<SimpleItem> result = seg.segment(content.toString(), url);
    int expectedResults = 10;
    assertEquals(expectedResults, result.size());
    SimpleItem main = result.get(0);
    assertEquals("http://www.afiliado.com/foro/viewtopic.php?f=15&t=10629&start=10", main.threadURLs.get(0));
    
    URL url2 = new URL(main.threadURLs.get(0));
    content = readPageFromResource("afiliado.com.2.html");
    result = seg.segment(content.toString(), url2);
    expectedResults = 1;
    assertEquals(expectedResults, result.size());
    main = result.get(0);
    assertNull(main.threadURLs);
  }
}
