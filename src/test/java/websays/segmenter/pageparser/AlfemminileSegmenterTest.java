/**
 * Websays Opinion Analytics Engine 
 * 
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 * 
 * Primary Author: joedval
 * Contributors: 
 * Date: Jul 21, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.net.URL;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

/**
 *
 **/
public class AlfemminileSegmenterTest extends BaseDocumentSegmenterTest {
  
  /**
   * Test complete of segment method, of class AfiliadoSegmenter.
   */
  @Test
  public void testAlfemminileSegmenter() throws Exception {
    
    URL url = new URL("http://forum.alfemminile.com/forum/matern1/__f507389_matern1-Ritorno-in-punta-di-piedi.html");
    StringBuilder content = readPageFromResource("alfemminile.html");
    
    Calendar cal = Calendar.getInstance();
    HTMLDocumentSegmenter seg = new AlfemminileSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 21;
    assertEquals(expectedResults, result.size());
    
    // /////// Test First comment item //////////////
    SimpleItem main = result.get(0);
    assertEquals(
        "Ritorno in punta di piedi... Fatto test stamattina , sono a 4 piu 2 .. Ho davvero tanta paura -Che bellooooo rivederti qu.. -Evvaiiii auguriii -Ciao panno -Mi ricordo di te. -Ciao panno!!! -Che bella notizia! -Ciao panno.",
        main.title);
    assertEquals("pannocchietta1974", main.author);
    assertEquals("Fatto test stamattina , sono a 4 piu 2 .... Ho davvero tanta paura", main.body);
    cal.set(2014, 6, 21, 9, 31);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), main.date);
    assertEquals(url.toString(), main.postUrl);
    assertEquals(1, main.position);
    assertEquals("http://forum.alfemminile.com/forum/matern1/__f507389_p2_matern1-Ritorno-in-punta-di-piedi.html", main.threadURLs.get(0));
  }
  
  /**
   * Test last page of segment method, of class AlfemminileSegmenter.
   */
  @Test
  public void testLastPageAlfemminileSegmenter() throws Exception {
    
    URL url = new URL("http://forum.alfemminile.com/forum/matern1/__f507389_matern1-Ritorno-in-punta-di-piedi.html");
    StringBuilder content = readPageFromResource("alfemminile.html");
    HTMLDocumentSegmenter seg = new AlfemminileSegmenter();
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 21;
    assertEquals(expectedResults, result.size());
    
    SimpleItem main = result.get(0);
    assertEquals("http://forum.alfemminile.com/forum/matern1/__f507389_p2_matern1-Ritorno-in-punta-di-piedi.html", main.threadURLs.get(0));
    
    URL url2 = new URL(main.threadURLs.get(0));
    content = readPageFromResource("alfemminile1.html");
    result = seg.segment(content.toString(), url2);
    expectedResults = 14;
    assertEquals(expectedResults, result.size());
    main = result.get(0);
    assertNull(main.threadURLs);
  }
}
