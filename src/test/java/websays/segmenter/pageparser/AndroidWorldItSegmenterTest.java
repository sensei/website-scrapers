/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class AndroidWorldItSegmenterTest extends SegmenterTest {
  
  /**
   * Test of segment method, of class TripAdvisorSegmenter.
   */
  @Test
  public void testSegment() throws Exception {
    
    URL url = new URL(
        "http://www.androidworld.it/forum/samsung-galaxy-s-iii-i9300-279/problema-visualizzazioni-contatti-da-visualizzare-rubrica-137804/");
    
    StringBuilder content = readPageFromResource("androidworld.it");
    
    Calendar cal = Calendar.getInstance();
    HTMLDocumentSegmenter seg = new AndroidWorldItSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 9;
    assertEquals(expectedResults, result.size());
    
    // /////// Test First comment item //////////////
    SimpleItem main = result.get(0);
    assertEquals("Problema visualizzazioni contatti da visualizzare in rubrica", main.title);
    assertEquals("stenava", main.author);
    assertTrue(main.body.startsWith("Ho questo problema relativo") && main.body.endsWith("grazie"));
    assertEquals(
        "http://www.androidworld.it/forum/samsung-galaxy-s-iii-i9300-279/problema-visualizzazioni-contatti-da-visualizzare-rubrica-137804/#post1378507",
        main.itemUrl);
    assertEquals(url.toString(), main.postUrl);
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
    Date date = sdf.parse("2014-03-21 15:12");
    assertEquals(date, main.date);
    // assertEquals("http://forum.telefonino.net/customavatars/avatar67543_17.gif", main.imageLink);
    assertEquals(1, main.position);
    
    // ///////// Test Fourth comment item //////////////
    SimpleItem sub1 = result.get(4);
    assertNull(sub1.title);
    assertEquals("stenava", sub1.author);
    assertTrue(sub1.body.startsWith("Avevo evitato rom custom") && sub1.body.endsWith("rimangono tali.."));
    date = sdf.parse("2014-03-24 09:34");
    assertEquals(date, sub1.date);
    // assertEqualDates(cal.getTime(), sub1.date);
    assertEquals(
        "http://www.androidworld.it/forum/samsung-galaxy-s-iii-i9300-279/problema-visualizzazioni-contatti-da-visualizzare-rubrica-137804/#post1379482",
        sub1.itemUrl);
    assertEquals(url.toString(), sub1.postUrl);
    // assertEquals(null, sub1.imageLink);
    assertEquals(5, sub1.position);
    
  }
}
