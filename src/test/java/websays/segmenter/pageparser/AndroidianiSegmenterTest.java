/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class AndroidianiSegmenterTest extends SegmenterTest {
  
  /**
   * Test of segment method, of class TripAdvisorSegmenter.
   */
  @Test
  public void testSegment() throws Exception {
    
    URL url = new URL("http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile.html");
    
    StringBuilder content = readPageFromResource("androidiani.com");
    
    Calendar cal = Calendar.getInstance();
    HTMLDocumentSegmenter seg = new AndroidianiSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 10;
    assertEquals(expectedResults, result.size());
    
    // /////// Test First comment item //////////////
    SimpleItem main = result.get(0);
    assertEquals("Problema sms Sim Postemobile", main.title);
    assertEquals("Piro90", main.author);
    assertTrue(main.body.startsWith("Ciao a tutti") && main.body.endsWith("lo ha riscontrato??"));
    assertEquals("http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile.html#post4825087", main.itemUrl);
    assertEquals(url.toString(), main.postUrl);
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
    Date date = sdf.parse("2013-11-16 13:52");
    assertEquals(date, main.date);
    // assertEquals("http://forum.telefonino.net/customavatars/avatar67543_17.gif", main.imageLink);
    assertEquals(1, main.position);
    
    // ///////// Test Fourth comment item //////////////
    SimpleItem sub1 = result.get(4);
    assertNull(sub1.title);
    assertEquals("Piro90", sub1.author);
    assertTrue(sub1.body.startsWith("non so dirti precisamente") && sub1.body.endsWith("abbia dato altri problemi..."));
    date = sdf.parse("2013-12-01 17:42");
    assertEquals(date, sub1.date);
    // assertEqualDates(cal.getTime(), sub1.date);
    assertEquals("http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile.html#post4908495", sub1.itemUrl);
    assertEquals(url.toString(), sub1.postUrl);
    // assertEquals(null, sub1.imageLink);
    assertEquals(5, sub1.position);
    
  }
}
