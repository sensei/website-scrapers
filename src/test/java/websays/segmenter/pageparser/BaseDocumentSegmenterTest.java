/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors: Jorge Valderrama
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;

import org.apache.commons.lang3.StringUtils;
import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.junit.Before;
import org.junit.Test;

import websays.segmenter.base.BaseHTMLSegmenter;

public class BaseDocumentSegmenterTest extends SegmenterTest {
  
  protected TagNode rootNode;
  protected BaseHTMLSegmenter seg;
  
  protected HtmlCleaner createCleaner() {
    HtmlCleaner cleaner = new HtmlCleaner();
    CleanerProperties props = cleaner.getProperties();
    props.setTranslateSpecialEntities(true);
    props.setTransResCharsToNCR(true);
    props.setTransSpecialEntitiesToNCR(true);
    props.setOmitComments(true);
    return cleaner;
  }
  
  @Before
  public void setUp() throws Exception {
    this.seg = new BaseHTMLSegmenter();
    this.rootNode = new TagNode("root");
  }
  
  @Test
  public void testSegment() {
    // assertNull(seg.segment(content, url));
  }
  
  @Test
  public void testFindComments() {
    assertNull(seg.findComments(rootNode));
  }
  
  @Test
  public void testFindMainTitle() {
    assertNull(seg.findMainTitle(rootNode));
  }
  
  @Test
  public void testFindMainDescripcion() {
    assertNull(seg.findMainDescripcion(rootNode));
  }
  
  @Test
  public void testFindMainDate() {
    assertNull(seg.findMainDate(rootNode));
  }
  
  @Test
  public void testFindMainAuthor() {
    assertNull(seg.findMainAuthor(rootNode));
  }
  
  @Test
  public void testFindMainAuthorLocation() {
    assertNull(seg.findMainAuthorLocation(rootNode));
  }
  
  @Test
  public void testFindMainScore() {
    assertNull(seg.findMainScore(rootNode));
  }
  
  @Test
  public void testFindMainLikes() {
    assertNull(seg.findMainLikes(rootNode));
  }
  
  @Test
  public void testFindMainReviews() {
    assertNull(seg.findMainReviews(rootNode));
  }
  
  @Test
  public void testFindMainVotes() {
    assertNull(seg.findMainVotes(rootNode));
  }
  
  @Test
  public void testFindMainComments() {
    assertNull(seg.findMainComments(rootNode));
  }
  
  @Test
  public void testFindMainAPIObjectID() {
    assertNull(seg.findMainAPIObjectID(rootNode));
  }
  
  @Test
  public void testFindMainAPIAuthorID() {
    assertNull(seg.findMainAPIAuthorID(rootNode));
  }
  
  @Test
  public void testFindMainAPIToAuthorID() {
    assertNull(seg.findMainAPIToAuthorID(rootNode));
  }
  
  @Test
  public void testFindMainImageLink() {
    assertNull(seg.findMainImageLink(rootNode));
  }
  
  @Test
  public void testFindMainLatitude() {
    assertNull(seg.findMainLatitude(rootNode));
  }
  
  @Test
  public void testFindMainLongitude() {
    assertNull(seg.findMainLongitude(rootNode));
  }
  
  @Test
  public void testFindMainThreadId() {
    assertNull(seg.findMainThreadId(rootNode));
  }
  
  @Test
  public void testFindMainThreadURLs() {
    assertNull(seg.findMainThreadURLs(rootNode));
  }
  
  @Test
  public void testFindMainBody() {
    assertNull(seg.findMainBody(rootNode));
  }
  
  @Test
  public void testFindMainTimeIsReal() {
    assertFalse(seg.findMainTimeIsReal(rootNode));
  }
  
  @Test
  public void testFindMainLanguage() {
    assertNull(seg.findMainLanguage(rootNode));
  }
  
  @Test
  public void testFindMainSourceExtraData() {
    assertNull(seg.findMainSourceExtraData(rootNode));
  }
  
  @Test
  public void testFindCommentNumComments() {
    assertNull(seg.findCommentNumComments(rootNode));
  }
  
  @Test
  public void testFindCommentVotes() {
    assertNull(seg.findCommentVotes(rootNode));
  }
  
  @Test
  public void testFindCommentLikes() {
    assertNull(seg.findCommentLikes(rootNode));
  }
  
  @Test
  public void testFindCommentScore() {
    assertNull(seg.findCommentScore(rootNode));
  }
  
  @Test
  public void testFindCommentImageLink() {
    assertNull(seg.findCommentImageLink(rootNode));
  }
  
  @Test
  public void testFindCommentDate() {
    assertNull(seg.findCommentDate(rootNode));
  }
  
  @Test
  public void testFindCommentAuthorLocation() {
    assertNull(seg.findCommentAuthorLocation(rootNode));
  }
  
  @Test
  public void testFindCommentAuthor() {
    assertNull(seg.findCommentAuthor(rootNode));
  }
  
  @Test
  public void testFindCommentDescripcion() {
    assertNull(seg.findCommentDescripcion(rootNode));
  }
  
  @Test
  public void testFindCommentTitle() {
    assertNull(seg.findCommentTitle(rootNode));
  }
  
  @Test
  public void testfindCommentParentID() {
    assertNull(seg.findCommentParentID(rootNode));
  }
  
  @Test
  public void testFindCommentApiObjectID() {
    assertNull(seg.findCommentAPIObjectID(rootNode));
  }
  
  @Test
  public void testFindCommentTimesIsReal() {
    assertFalse(seg.findCommentTimeIsReal(rootNode));
  }
  
  @Test
  public void testFindCommentLanguage() {
    System.out.println(StringUtils.join(seg.findCommentLanguage(rootNode), ";"));
    assertNull(seg.findCommentLanguage(rootNode));
  }
  
  @Test
  public void testFindCommentSourceExtraData() {
    assertNull(seg.findCommentSourceExtraData(rootNode));
  }
}
