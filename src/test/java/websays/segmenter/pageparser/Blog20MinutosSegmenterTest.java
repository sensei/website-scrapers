/**
 * Websays Opinion Analytics Engine 
 * 
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 * 
 * Primary Author: joedval
 * Contributors: 
 * Date: Jul 31, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

/**
 *
 **/
public class Blog20MinutosSegmenterTest extends BaseDocumentSegmenterTest {
  
  /**
   * Test complete of segment method, of class Blog20MinutosSegmenter.
   */
  @Test
  public void testBlog20MinutosSegmenter() throws Exception {
    
    URL url = new URL(
        "http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/02/20/quieres-darle-a-tus-puertas-una-apariencia-nueva-facilmente-y-en-plan-low-cost-mira-este-truco/");
    
    StringBuilder content = readPageFromResource("blogs.20minutos.es");
    
    Calendar cal = Calendar.getInstance();
    HTMLDocumentSegmenter seg = new Blog20MinutosSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 6;
    assertEquals(expectedResults, result.size());
    
    // /////// Test Article item //////////////
    SimpleItem main = result.get(0);
    assertEquals("¿Quieres darle a tus puertas una apariencia nueva, fácilmente y en plan Low Cost? Mira este truco", main.title);
    assertEquals("http://blogs.20minutos.es", main.author);
    assertEquals(
        "Haz este ejercicio mental: recuerda cuántas veces has estado en un restaurante, un hotel o la casa de unos amigos y has pensado “qué detalles tan chulos”, “qué casa tan moderna”, “qué buen gusto”. Y lo has hecho al abrir la puerta, de una habitación, de un baño.Exacto. Los picaportes visten mucho una casa. Son esos complementos de moda que marcan un estilo. Pequeños cambios en tu hogar que consiguen darle un aire nuevo al instante, por muy poco dinero y esfuerzo. Y precisamente porque es una de esas cosas que no te plantearías, imaginándotela muy complicada, te vamos a demostrar lo fácil y barato que es sustituir los pomos de tus puertas. Un accesorio que ronda, de media, entre los 8€ y los 25€ en cualquier ferretería o tienda de bricolaje, y que hará renacer a tus ojos esa puerta que tienes tan vista.  En el vídeo consejo de aquí arriba te lo contamos paso a paso, y te sorprenderás de ver que no necesitas más ayuda que un destornillador, algo de masilla para madera, una lija suave y algo de barniz. Ejem… ¡y las nuevas manillas!Fíjate en el ejemplo práctico: hemos ayudado a nuestra amiga Marta a sustituir un picaporte tradicional, de los de toda la vida, por uno cuadrado y minimal, mucho más moderno y estiloso.¿Qué sucede? Que los años de convivencia entre la puerta y el antiguo pasaporte dejan huella, querido amigo: necesita una reparación de la zona que antes estaba cubierta por el embellecedor y ahora queda al desnudo.Pero lo primero es lo primero: retirar la vieja roseta. Y para ello hay dos posibilidades, que el embellecedor tenga tornillos a la vista, o esté encastrado. En el primer caso solo necesitas desatornillarlos directamente; en el segundo, tendrás que usar un destornillador de punta plana, apoyarlo sobre una cuña de madera para no marcar la puerta, y hacer palanca en la pequeña rendija que encontrarás para tal efecto.Cuando el artilugio esté retirado, tendrás que rellenar los agujeros con masilla para madera. La encontrarás en tu tienda habitual de bricolaje, en varios colores para hacer juego con tu puerta. Después, lija suavemente la zona y, según el grado de desgastamiento, pinta o barniza solo la zona afectada o hazlo con toda la superficie de la puerta (si la diferencia es tan grande que llega a percibirse el “parche”) .       Una vez como nueva la puerta, preséntale a su nuevo picaporte y atorníllalo con los tirafondos que acompañan al nuevo pomo. Las puertas de hoy en día suelen ser perforables por medio de un destornillador y un poco de fuerza. Pero si su madera es maciza, necesitarás practicar primero un agujero con taladro y una broca más fina que el tirafondos, para ayudarte.       Para terminar, encaja el nuevo embellecedor con un poco de presión y…¿Qué te parece? ¿Abrimos la puerta a la imaginación y cambiamos los picaportes en casa? Publicidad: ¿Tus cerraduras te dan problemas más serios y necesitas ayuda profesional? La tienes a un clic de distancia en tu móvil: descarga e instala GRATIS en tu Smartphone la App de Reparalia para iPhone en iTunes y para Android en Google Play, y estarás protegido contra imprevistos, accidentes en el hogar y todo tipo de problemas domésticos, ya sean de cerrajería, fontanería, electricidad o cualquier otro gremio.¡Pulsa su botón y nuestros profesionales te llamarán en el momento para solucionar tu problema enseguida!Tags: App, Decoración, destornillador, DIY, Gratis, low-cost, manillas, picaportes, pomos, puertas, renovar, Reparalia, roseta |  Almacenado en: Carpintería, Cerrajería, Decoración, DIY, mantenimiento, puertas, videoconsejos",
        main.body);
    cal.set(2014, 1, 20);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), main.date);
    assertEquals(
        "http://blogs.20minutos.es/un-hogar-con-mucho-oficio/files/2014/02/Reparalia_blog_consejo_truco_como_cambiar_sustituir_manilla_pomo_picaporte_puerta_casa_reparaciones_hogar_emergencias_carpinteros_fontaneros_albañiles_toda_España.jpg",
        main.imageLink);
    assertEquals(url.toString(), main.postUrl);
    assertEquals(1, main.position);
    assertFalse(main.isComment);
    assertNull(main.threadURLs);
    
    // /////// Test Comment item //////////////
    SimpleItem fouth = result.get(4);
    assertNull(fouth.title);
    assertEquals("Dice ser Salvador de la Casa", fouth.author);
    assertEquals(
        "¡Hola amigos! el día que os ponéis tiquismiquis, mira que sois, ¿eh? ;PNo hay trampa ni cartón, sino mucho mimo en el trabajo: id al segundo 55 del vídeo -en el texto también os lo contamos- y veréis cómo os explicamos la forma de hacer desaparecer sus rastros: una vez retirado el anterior picaporte, debéis rellenar los agujeros con masilla para madera -del mismo color de la puerta, para disimularlos al máximo.Si lo claváis con el color, como ha hecho nuestro compañero de Reparalia en el vídeo, os aseguro que se notarán muy, muy poquito las marcas del lugar donde estaban estos orificios. Pero es que además, después os recomendamos lijar y repintar o barnizar la zona para dejarla perfecta, que es lo que hemos hecho nosotros.Estos son el Photoshop y el AfterEffects de los manitas: maña, tiempo y buenos materiales  Un abrazo y gracias por ser siempre muy exigentes con todos los proyectos que os planteéis: es la mejor forma de aprender y conseguir resultados tan profesionales como estos.",
        fouth.body);
    cal.set(2014, 1, 20, 18, 55);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), fouth.date);
    assertEquals(url.toString(), fouth.postUrl);
    assertEquals(5, fouth.position);
    assertTrue(fouth.isComment);
    assertNull(fouth.threadURLs);
  }
}
