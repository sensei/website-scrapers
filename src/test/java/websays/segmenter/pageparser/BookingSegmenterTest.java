/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.net.URL;
import java.util.Calendar;
import java.util.List;

import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class BookingSegmenterTest extends BaseDocumentSegmenterTest {
  
  @Override
  public void setUp() throws Exception {
    StringBuilder content = readPageFromResource("Booking.com: Hotel Obelisco , Playa de Palma, Spain - 22 Guest reviews . Book your hotel now!.html");
    this.seg = new BookingSegmenter();
    HtmlCleaner cleaner = createCleaner();
    this.rootNode = cleaner.clean(content.toString());
  }
  
  @Override
  public void testFindMainTitle() {
    String title = seg.findMainTitle(rootNode);
    assertEquals("★★★★ Hotel Obelisco, Playa de Palma, Spain", title);
  }
  
  @Override
  public void testFindMainScore() {
    assertEquals(new Float(7.6), seg.findMainScore(rootNode));
  }
  
  @Override
  public void testFindMainReviews() {
    assertEquals(new Integer(22), seg.findMainReviews(rootNode));
  }
  
  @Override
  public void testFindMainLatitude() {
    assertEquals(new Double(39.51870907792132), seg.findMainLatitude(rootNode));
  }
  
  @Override
  public void testFindMainLongitude() {
    assertEquals(new Double(2.7464575523677013), seg.findMainLongitude(rootNode));
  }
  
  @Override
  public void testFindMainDescripcion() {
    String body = seg.findMainDescripcion(rootNode);
    assertEquals(
        "Offering rooms with balconies, Hotel Obelisco is set just 200 metres from Playa de Palma Beach. It features indoor and outdoor swimming pools, a gym and a restaurant.Each air-conditioned room at Hotel Obelisco has bright, modern décor. There is a flat-screen TV, a rental safe and a minibar for an extra cost.The buffet restaurant serves a range of Mediterranean cuisine. There is also a bar and a café.The hotel also features a sauna, tennis courts and table tennis. Staff can arrange bicycle hire and the hotel offers storage for bikes.Located in Las Maravillas, Obelisco is less than 4 km from Palma Airport. The city centre is just 15 minutes’ drive away.",
        body);
  }
  
  @Override
  public void testFindMainAPIObjectID() {
    assertEquals("145362478954725", seg.findMainAPIObjectID(rootNode));
  }
  
  @Override
  public void testFindMainImageLink() {
    assertEquals("http://r-ec.bstatic.com/images/hotel/max300/998/9989407.jpg", seg.findMainImageLink(rootNode));
  }
  
  @Override
  public void testFindComments() {
    TagNode[] comments = seg.findComments(rootNode);
    assertEquals(22, comments.length);
  }
  
  @Override
  public void testFindCommentScore() {
    TagNode[] comments = seg.findComments(rootNode);
    assertEquals(new Float(5.4), seg.findCommentScore(comments[0]));
    assertEquals(new Float(7.1), seg.findCommentScore(comments[1]));
    assertEquals(new Float(8.8), seg.findCommentScore(comments[2]));
    assertEquals(new Float(7.9), seg.findCommentScore(comments[3]));
    
  }
  
  @Override
  public void testFindCommentDate() {
    Calendar cal = Calendar.getInstance();
    TagNode[] comments = seg.findComments(rootNode);
    cal.set(2013, 9, 20); // 20 October 2013
    assertEqualDates(cal.getTime(), seg.findCommentDate(comments[0]));
    cal.set(2013, 7, 20); // 20 August 2013
    assertEqualDates(cal.getTime(), seg.findCommentDate(comments[1]));
  }
  
  @Override
  public void testFindCommentAuthorLocation() {
    TagNode[] comments = seg.findComments(rootNode);
    assertEquals("London, United Kingdom", seg.findCommentAuthorLocation(comments[0]));
    assertEquals("MARCOUSSIS, France", seg.findCommentAuthorLocation(comments[1]));
    assertEquals("Koblenz, Germany", seg.findCommentAuthorLocation(comments[2]));
    assertEquals("Wil/ZH, Switzerland", seg.findCommentAuthorLocation(comments[3]));
  }
  
  @Override
  public void testFindCommentAuthor() {
    TagNode[] comments = seg.findComments(rootNode);
    assertEquals("Roy", seg.findCommentAuthor(comments[0]));
    assertEquals("Severine", seg.findCommentAuthor(comments[1]));
    assertEquals("Sebastian", seg.findCommentAuthor(comments[2]));
    assertEquals("Karin", seg.findCommentAuthor(comments[3]));
    assertEquals("Anonymous", seg.findCommentAuthor(comments[21]));
  }
  
  @Override
  public void testFindCommentDescripcion() {
    TagNode[] comments = seg.findComments(rootNode);
    assertEquals("Proximity to airport.", seg.findCommentDescripcion(comments[0]));
    assertEquals(
        "The location. Too many German everywhere.in the town, in the hotel...they are veryNoisy all the night in the hotel..:-( Restaurants are written in german,spanish is not spoken....We do not have the impression too be inMallorca. I prefer the South west of the isle which is more quiet !",
        seg.findCommentDescripcion(comments[1]));
    assertEquals(
        "Obwohl wir unsere Kamera in einem der 4 gebuchten Zimmer vergessen hatte, wurde sie von der Putzfrau in der am längsten gemietete Zimmer gelegt. KLASSE. Es gibt doch noch ehrliches Personal!",
        seg.findCommentDescripcion(comments[2]));
    
  }
  
  @Test
  public void testObeliscoSegment() throws Exception {
    
    URL url = new URL("http://www.booking.com/hotel/es/obelisco.en-gb.html");
    StringBuilder content = readPageFromResource("Booking.com: Hotel Obelisco , Playa de Palma, Spain - 22 Guest reviews . Book your hotel now!.html");
    HTMLDocumentSegmenter seg = new BookingSegmenter();
    List<SimpleItem> res = seg.segment(content.toString(), url);
    
    Calendar cal = Calendar.getInstance();
    
    int expectedResults = 23;
    assertEquals(expectedResults, res.size());
    
    // ///////// Test Main item //////////////
    SimpleItem main = res.get(0);// The main details item at
    
    // index 0 (first
    // element)
    assertEquals(
        "Offering rooms with balconies, Hotel Obelisco is set just 200 metres from Playa de Palma Beach. It features indoor and outdoor swimming pools, a gym and a restaurant.Each air-conditioned room at Hotel Obelisco has bright, modern décor. There is a flat-screen TV, a rental safe and a minibar for an extra cost.The buffet restaurant serves a range of Mediterranean cuisine. There is also a bar and a café.The hotel also features a sauna, tennis courts and table tennis. Staff can arrange bicycle hire and the hotel offers storage for bikes.Located in Las Maravillas, Obelisco is less than 4 km from Palma Airport. The city centre is just 15 minutes’ drive away.",
        main.body);
    assertEquals(new Double(39.51870907792132), main.latitude);
    assertEquals(new Double(2.7464575523677013), main.longitude);
    assertEquals("145362478954725", main.APIObjectID);
    assertEquals(new Integer(22), main.reviews);
    assertEquals(new Integer(76), main.getScore_native());// round, the real is 7.6
    assertEquals(new Integer(76), main.getScore_websays());
    assertNull(main.date);
    assertEquals("http://www.booking.com/hotel/es/obelisco.html", main.itemUrl);
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = res.get(1);
    assertEquals(null, sub1.title);
    assertEquals("Proximity to airport.", sub1.body);
    assertEquals("Roy", sub1.author);
    assertEquals("London, United Kingdom", sub1.authorLocation);
    assertEquals(new Integer(54), sub1.getScore_native()); // round, the real is 5.4
    assertEquals(new Integer(54), sub1.getScore_websays()); // round, the real is 5.4
    cal.set(2013, 9, 20); // 20 October 2013
    assertEqualDates(cal.getTime(), sub1.date);
    assertEquals(null, sub1.itemUrl);
    
    // ///////// Test Last comment item //////////////
    SimpleItem subLast = res.get(22);
    assertEquals(null, subLast.title);
    assertEquals(null, subLast.body);
    assertEquals("Anonymous", subLast.author);
    assertEquals("Native Score", new Integer(75), subLast.getScore_native());
    assertEquals("Websays Score", new Integer(75), subLast.getScore_websays());
    cal.set(2013, 3, 30); // 30 April 2013
    assertEqualDates(cal.getTime(), subLast.date);
    assertEquals(null, subLast.itemUrl);
  }
  
  @Test
  public void testObeliscoSegment2015() throws Exception {
    
    URL url = new URL("http://www.booking.com/hotel/es/obelisco.en-us.html");
    StringBuilder content = readPageFromResource("Booking.com: Hotel Obelisco, Playa de Palma, Spain - 2015 - 30 Guest reviews.html");
    HTMLDocumentSegmenter seg = new BookingSegmenter();
    List<SimpleItem> results = seg.segment(content.toString(), url);
    
    Calendar cal = Calendar.getInstance();
    
    int expectedResults = 11;
    assertEquals(expectedResults, results.size());
    
    // ///////// Test Main item //////////////
    SimpleItem main = results.get(0);// The main details item at
    
    // index 0 (first
    // element)
    assertEquals(
        "Offering rooms with balconies, Hotel Obelisco is set just 200 metres from Playa de Palma Beach. It features indoor and outdoor swimming pools, a gym and a restaurant.Each air-conditioned room at Hotel Obelisco has bright, modern décor. There is a flat-screen TV, a rental safe and a minibar for an extra cost.The buffet restaurant serves a range of Mediterranean cuisine. There is also a bar and a café.The hotel also features a sauna, tennis courts and table tennis. Staff can arrange bicycle hire and the hotel offers storage for bikes.Located in Las Maravillas, Obelisco is less than 4 km from Palma Airport. The city centre is just 15 minutes’ drive away.",
        main.body);
    assertEquals(new Double(39.51870907792132), main.latitude);
    assertEquals(new Double(2.7464575523677013), main.longitude);
    assertEquals("145362478954725", main.APIObjectID);
    assertEquals(30, main.reviews.intValue());
    assertEquals(new Integer(76), main.getScore_native());// round, the real is 7.6
    assertEquals(new Integer(76), main.getScore_websays());// round, the real is 7.6
    assertNull(main.date);
    assertEquals("http://www.booking.com/hotel/es/obelisco.html", main.itemUrl);
    assertEquals(1, main.position);
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = results.get(1);
    assertEquals("Aufenthalt im Mai", sub1.title);
    assertEquals("Zimmer sind super und der Service ist hervorragend ! Lage zur Meile ist perfekt", sub1.body);
    assertEquals("Stephan", sub1.author);
    assertEquals("Germany", sub1.authorLocation);
    assertEquals("Native Score", new Integer(79), sub1.getScore_native());
    assertEquals("Websays Score", new Integer(79), sub1.getScore_websays());
    cal.set(2015, 4, 25); // 20 October 2013
    assertEqualDates(cal.getTime(), sub1.date);
    assertEquals(null, sub1.itemUrl);
    assertEquals("http://www.booking.com/hotel/es/obelisco.html", sub1.postUrl);
    assertEquals(2, sub1.position);
    
    // ///////// Test Last comment item //////////////
    SimpleItem subLast = results.get(10);
    assertEquals("Lage für Partyurlaub sehr gut - Einrichtung und Service entsprechen aber keinen vier Sternen", subLast.title);
    assertEquals(
        "- Einrichtung und Service entsprechen keinen vier Sternen - eher landesüblichen drei Sternen- Preisleistungsverhältnis stimmt dementsprechend auch nicht - hier kann für weniger Geld besser ein vergleichbares drei Sterne Hotel gebucht werden- Publikum - entsprechend der Lage ist der Großteil der Gäste auf Partyurlaub unterwegs, was sich leider auch an der Lautstärke und dem Verhalten mancher Gäste bemerkbar macht. + Lage zum Ballermann / Schinkenstraße+ Betten",
        subLast.body);
    assertEquals("Anonymous", subLast.author);
    assertEquals("Native Score", new Integer(54), subLast.getScore_native());
    assertEquals("Websays Score", new Integer(54), subLast.getScore_websays());
    cal.set(2015, 7, 18); // 30 April 2013
    assertEqualDates(cal.getTime(), subLast.date);
    assertEquals(null, subLast.itemUrl);
    assertEquals("http://www.booking.com/hotel/es/obelisco.html", subLast.postUrl);
    assertEquals(11, subLast.position);
  }
}
