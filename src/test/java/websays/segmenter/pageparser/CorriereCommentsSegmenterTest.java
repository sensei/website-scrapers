/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;

import websays.segmenter.base.MinimalDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class CorriereCommentsSegmenterTest extends SegmenterTest {
  
  /**
   * Test of segment method, of class CorriereCommentSegmenter.
   * 
   * @return
   */
  @Test
  public void testCorriereCommentsSegmenter() throws Exception {
    initialTest();
    june03_2014Test();
    commentsOfCommentsTest();
  }
  
  private void initialTest() throws MalformedURLException, UnsupportedEncodingException, IOException, Exception {
    URL urlComments = new URL(
        "http://xml2.temporeale.corriereobjects.it/community-corriere/commenti/articoli/1e/a0/7b/f8/8f/17/11/e3/8c/4a/c3/55/fa/40/79/e9/final/comment-head.json");
    StringBuilder contentComments = readPageFromResource("corriereComments.json");
    MinimalDocumentSegmenter segComments = new CorriereCommentsSegmenter();
    List<SimpleItem> resultComments = segComments.segment(contentComments.toString(), urlComments);
    
    int expectedResults = 15; // No of comments
    assertEquals(expectedResults, resultComments.size());
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = resultComments.get(0);
    // for (SimpleItem simpleItem : resultComments) {
    // System.out.println(simpleItem.threadId);
    // }
    assertEquals("1332912", sub1.APIObjectID);
    assertEquals(null, sub1.title);
    assertEquals(
        "I livelli di dipendenza nello Sato devono essre ridotti a due ed il legislativo non pu&ograve; avere pi&ugrave; di un livello visto che l' Europa   &egrave;  diventata il primo livello legislativo quindi o si aboliscono le regioni o si aboliscono le province . Si pu&ograve; anche avre citt&agrave; metropolitane e regioni , come anche province e comuni , ma..... le citt&agrave; metropolitane  devono essre allo stesso livello delle regionie le province a quello dei comuni, il che significa sempre due soli livelli di decentramento. Il senato elettivo deve essere totalmente abolito , Renzi vai avanti cos&igrave;.",
        sub1.body);
    assertEquals("Lettore_2966191", sub1.author);
    assertEquals(null, sub1.imageLink);
    assertEquals(new Integer(0), sub1.comments);
    Calendar cal = Calendar.getInstance();
    cal.set(2014, 01, 07, 17, 48); // Year, Month(zero based), Date, HourOfDay, Minutes
    assertEqualTimes(cal.getTime(), sub1.date); // February 07, 2014 17:48
    assertEquals(urlComments.toString(), sub1.postUrl);
    assertEquals(1, sub1.position);
    assertTrue(sub1.isComment);
  }
  
  private void june03_2014Test() throws MalformedURLException, UnsupportedEncodingException, IOException, Exception {
    URL urlComments = new URL(
        "http://xml2.temporeale.corriereobjects.it/community-corriere/commenti/articoli/39/2b/bf/00/eb/01/11/e3/90/08/a2/f4/0d/75/35/42/final/comment-body.json");
    StringBuilder contentComments = readPageFromResource("corriereCommentsHead.json");
    MinimalDocumentSegmenter segComments = new CorriereCommentsSegmenter();
    List<SimpleItem> resultComments = segComments.segment(contentComments.toString(), urlComments);
    
    int expectedResults = 207; // No of comments
    assertEquals(expectedResults, resultComments.size());
    
    // ///////// Test First comment item //////////////
    int i = 0;
    SimpleItem sub1 = resultComments.get(9);
    assertEquals("2092180", sub1.APIObjectID);
    assertEquals(null, sub1.title);
    assertEquals(
        "Le chiacchere stanno a zero. Ho perso il conto di quante volte lo Stato ha dovuto salvare Alitalia. Vendetela agli Emirati e che ci facciano quel che vogliono. Vale per l'Alitalia quello che vale per la RAI. Troppa gente, un mare di raccomandati, vespaio di lottizzazione, sindacati autoreferenziali. Se non si puo' risanare (e non si puo'), datela agli stranieri e che se la mangino.  ",
        sub1.body);
    assertEquals("Lettore_2811139", sub1.author);
    assertEquals(null, sub1.imageLink);
    assertEquals(new Integer(0), sub1.comments);
    Calendar cal = Calendar.getInstance();
    cal.set(2014, 05, 03, 17, 15); // Year, Month(zero based), Date, HourOfDay, Minutes
    assertEqualTimes(cal.getTime(), sub1.date); // February 07, 2014 17:48
    assertEquals(urlComments.toString(), sub1.postUrl);
    assertEquals(10, sub1.position);
    assertTrue(sub1.isComment);
  }
  
  private void commentsOfCommentsTest() throws MalformedURLException, UnsupportedEncodingException, IOException, Exception {
    URL urlComments = new URL(
        "http://xml2.temporeale.corriereobjects.it/community-corriere/commenti/articoli/00/df/8c/e6/b4/18/11/e3/be/28/0f/08/b3/8e/26/f7/final/comment-body.json");
    StringBuilder contentComments = readPageFromResource("comments-of-comments.json");
    MinimalDocumentSegmenter segComments = new CorriereCommentsSegmenter();
    List<SimpleItem> resultComments = segComments.segment(contentComments.toString(), urlComments);
    
    int expectedResults = 54; // No of comments
    assertEquals(expectedResults, resultComments.size());
    
    // ///////// Test Father comment item //////////////
    SimpleItem father = resultComments.get(51);
    // Id of the father
    assertEquals("1669877", father.APIObjectID);
    
    assertEquals(null, father.title);
    assertEquals("Bravo, gioco simpatico ma fino  2048, poi a 8192 ho lasciato perdere perch&eacute; ripetitivo&#10;Un saluto", father.body);
    assertEquals("prince9169", father.author);
    assertEquals("2393610", father.APIAuthorID);
    assertEquals(null, father.imageLink);
    assertEquals(new Integer(2), father.comments);
    assertEquals(urlComments.toString(), father.postUrl);
    assertEquals(52, father.position);
    assertTrue(father.isComment);
    
    // ///////// Test First Comment OF Father Comment item //////////////
    SimpleItem firstCommentOfFather = resultComments.get(6);
    // Id of the father related with first comment of a comment with APIObjectID=1669877
    assertEquals("1669877", firstCommentOfFather.parentID);
    
    assertEquals("1674746", firstCommentOfFather.APIObjectID);
    assertEquals(null, firstCommentOfFather.title);
    assertEquals("Anche io arrivato a 16385 ho abbandonato!", firstCommentOfFather.body);
    assertEquals("Lettore_attento", firstCommentOfFather.author);
    assertEquals("3343394", firstCommentOfFather.APIAuthorID);
    assertEquals(null, firstCommentOfFather.imageLink);
    assertEquals(new Integer(0), firstCommentOfFather.comments);
    assertEquals(urlComments.toString(), firstCommentOfFather.postUrl);
    assertEquals(7, firstCommentOfFather.position);
    assertTrue(firstCommentOfFather.isComment);
    
    // ///////// Test Fist Comment OF Father Comment item //////////////
    // Id of the father related with second comment of a comment with APIObjectID=1669877
    SimpleItem secondCommentOfFather = resultComments.get(49);
    
    assertEquals("1669877", firstCommentOfFather.parentID);
    assertEquals("1670126", secondCommentOfFather.APIObjectID);
    assertEquals(null, secondCommentOfFather.title);
    assertEquals(
        "Effettivamente ripetendo mosse e basta si arriva solo attorno a quel punteggio l&igrave;. Poi occorre far partire la creativit&agrave;...",
        secondCommentOfFather.body);
    assertEquals("roscu_torsu", secondCommentOfFather.author);
    assertEquals("9045820", secondCommentOfFather.APIAuthorID);
    assertEquals(null, secondCommentOfFather.imageLink);
    assertEquals(new Integer(0), secondCommentOfFather.comments);
    assertEquals(urlComments.toString(), secondCommentOfFather.postUrl);
    assertEquals(50, secondCommentOfFather.position);
    assertTrue(secondCommentOfFather.isComment);
  }
  
}