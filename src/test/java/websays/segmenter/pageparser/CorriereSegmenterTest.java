/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.htmlcleaner.HtmlCleaner;

public class CorriereSegmenterTest extends BaseDocumentSegmenterTest {
  
  public void testInitial() {
    
  }
  
  // http://www.corriere.it/politica/14_febbraio_06/150-senza-stipendio-renzi-svela-suo-senato-1ea07bf8-8f17-11e3-8c4a-c355fa4079e9.shtml
  @Override
  public void setUp() throws Exception {
    StringBuilder content = readPageFromResource(
        "Un’assemblea di 150, tutti senza stipendio Renzi svela il «suo» Senato. Oggi parola al Pd - Corriere.it.shtml", "Windows-1252");
    this.seg = new CorriereSegmenter();
    HtmlCleaner cleaner = createCleaner();
    this.rootNode = cleaner.clean(content.toString());
  }
  
  @Override
  public void testFindMainTitle() {
    String title = seg.findMainTitle(rootNode);
    assertEquals("Un’assemblea di 150, tutti   senza stipendio   Renzi svela il «suo»  Senato. Oggi parola al Pd", title);
  }
  
  @Override
  public void testFindMainDescripcion() {
    String body = seg.findMainDescripcion(rootNode);
    assertEquals(
        "Nessun membro eletto e 21 esponenti della società civile scelti dal capo dello StatoUn&rsquo;assemblea di 150, tutti   senza stipendio Renzi svela il «suo»  Senato. Oggi parola al Pd Il segretario  anticipa la proposta per il rinnovo di Palazzo Madama:  vi siederanno solo sindaci e governatoriUn&rsquo;assemblea di 150 persone composta da sindaci e presidenti di Regione, oltre a un gruppo di esponenti della società civile scelti direttamente dal capo dello Stato. Un drappello di rappresentanti delle autonomie locali che affiancheranno la Camera dei Deputati senza sovrapporsi alle sue  funzioni legislative. E che per lo svolgimento di questo mandato non percepiranno alcuna indennità. E' questa la proposta di trasformazione del Senato che Matteo Renzi intende presentare oggi alla Direzione nazionale del Pd e che il partito dovrebbe fare propria in aula. Una trasformazione radicale della cosiddetta «camera alta», che fino ad oggi è stata però unicamente un doppione di Montecitorio, con le stesse identiche prerogative e un legame solo indiretto con le regioni (i senatori sono eletti con ripartizione dei seggi su base regionale, ma nulla più).  Renzi, \"Ecco il mio Senato: 150 e tutti senza stipendio\"                               //Global var   var vote_IDL_1;   var vote_IL_1;   var pl_video_link;   // callback per flash:   function LoadPlayerText1(idVideo, playerId, videoLink)     {    var subdirectory = idVideo.substring(0, 2);    pl_video_link = videoLink;        var use_community_temp;    try     {     //use_community_temp = '(none)';     use_community_temp = 1;    } catch (e) {     console.log(e.message);    }        var use_community;    if(use_community_temp == '1')    {      use_community = 1;    }else     {     use_community = 0;    }    if(use_community ==1)    {     $('.box_Player').attr('style','margin-bottom: 15px;');     loadBoxInfoVar1(idVideo, subdirectory);     loadDescription1(idVideo, subdirectory);    }    else    {     $('#js_Title400_1').hide();     loadOldDescription1(idVideo, subdirectory);    }   }      function loadTitle1(idVideo, subdirectory)   {    $.ajax({     url: '/widget/content/video/html/title/titleBoxPlayer/'+subdirectory+'/titleBoxPlayer_'+idVideo+'.html',     type: 'GET',     data: 't=' + (new Date()).getTime(),     dataType: 'html',     cache: false,     success: function (data){      $('#js_Title400_1').html(data);      titleLoaded(1,idVideo);      try       {       loadCommunityVideo(idVideo,1,pl_video_link);             } catch (e) {       console.log(e.message);      }     },     error: function (data){      $('#js_Title400_1').empty();     }    });   }      function loadBoxInfoVar1(idVideo, subdirectory)    {    $.ajax({      url: '/widget/content/video/commenti/box_Info_Var/'+subdirectory+'/commenti_'+idVideo+'_box_Info_Var.html',      type: 'GET',      data: 't=' + (new Date()).getTime(),      dataType: 'text',      cache: false,      success: function (data){       $('#js_boxInfoVar_1').html(data);      },      error: function (data){       $('#js_boxInfoVar_1').empty();       loadComments1(video_id, sub_directory);      }     });   }      function loadDescription1(idVideo, subdirectory)    {    $.ajax({      url: '/widget/content/video/html/descriptionAndTools2013/player400/'+subdirectory+'/descriptionAndTools_Player400_'+idVideo+'.html',      type: 'GET',      data: 't=' + (new Date()).getTime(),      dataType: 'html',      cache: false,      success: function (data){        loadTitle1(idVideo, subdirectory);       var descriptiondiv = $('#js_DescriptionAndTools_1');       descriptiondiv.html(data);       resetAddThis();       descriptionLoaded(1,idVideo,player1);      },      error: function (data){       $('#js_DescriptionAndTools_1').empty();       loadOldDescription1(idVideo, subdirectory);      }     });    }      function loadOldDescription1(idVideo, subdirectory)    {    $.ajax({      url: '/widget/content/video/html/descriptionAndTools/player400/'+subdirectory+'/descriptionAndTools_Player400_'+idVideo+'.html',      type: 'GET',      data: 't=' + (new Date()).getTime(),      dataType: 'html',      cache: false,      success: function (data){              var descriptiondiv = $('#js_DescriptionAndTools_1');       descriptiondiv.html(data);              vote_IDL_1 = null;       vote_IL_1 = null              var visualVote = $(\".box_Voting_Results .box_Vote_Ok .ico_Icon\").attr(\"id\",'sp_iLike_'+idVideo+'_400');       var visualVote2 = $(\".box_Voting_Results .box_Vote_Ko .ico_Icon\").attr(\"id\",'sp_iDontLike_'+idVideo+'_400');              vote_IDL_1 = new Voting({         domain: 'video_corriere',         contentKey: idVideo,         countType: 'I_DONT_LIKE',         votaDivId: 'sp_iDontLike_'+idVideo+'_400',         risDivId: 'sp_iDontLike_'+idVideo+'_400'       });              vote_IL_1 = new Voting({        domain: 'video_corriere',        contentKey: idVideo,        countType: 'I_LIKE',        votaDivId: 'sp_iLike_'+idVideo+'_400',        risDivId: 'sp_iLike_'+idVideo+'_400'       });                                   var voteok = $(descriptiondiv).find('.box_Voting .btn_Vote_Ok');       var voteokhref = 'Javascript:vote_IL_1.doVote();';       voteok.attr(\"href\",voteokhref);              var voteko = $(descriptiondiv).find('.box_Voting .btn_Vote_Ko');       var votekohref = 'Javascript:vote_IDL_1.doVote();';       voteko.attr(\"href\",votekohref);              resetAddThis();       descriptionLoaded(1,idVideo,player1);      },      error: function (data){       $('#js_DescriptionAndTools_1').empty();      }     });    }     var player1 = new CTVPlayer( 3,'null', 'dateDesc', 'f3b16d92-8f22-11e3-8c4a-c355fa4079e9', 'false', 'true','logo_overlay.png','null', 'null',1,null,null,true );   //var player = new Player(\"3\", null, null, \"f3b16d92-8f22-11e3-8c4a-c355fa4079e9\", \"false\",\"false\", null,  \" PRIMO PIANO\", \"idcanale\", \"StringaDiProvaPopout\",null, null, \"false\");         var enableComment = true;       var startDate = \"2014/02/06.13:34:00\";   var titleForUrl = \"http://video.corriere.it/renzi-ecco-mio-senato-150-tutti-senza-stipendio/f3b16d92-8f22-11e3-8c4a-c355fa4079e9\";   var countPreTitle = titleForUrl.indexOf(\"f3b16d92-8f22-11e3-8c4a-c355fa4079e9\");      var urlToComment = titleForUrl.substr(0, countPreTitle) + \"commenti/\" +titleForUrl.substr(countPreTitle, titleForUrl.length);   var urlToAllComment = titleForUrl.substr(0, countPreTitle) + \"commenti-full/\" +titleForUrl.substr(countPreTitle, titleForUrl.length);      var startDateCommunity = \"2014/02/06.13:34:00\";   var creationDateCommunity = \"2014-02-06T13:34:00.0Z\";      var enableCommentCommunity = true;              LE FUNZIONI -«Il Senato deve diventare camera delle autonomie - ha sottolineato il sindaco di Firenze  durante il suo intervento ad un convegno di Confindustria dedicato alle città metropolitane -. Immaginiamo un Senato non elettivo, senza indennità, 150 persone, 108 sindaci dei comuni capoluogo, 21 presidenti di regione e 21 esponenti della società civile che vengono temporaneamente cooptati dal Presidente della Repubblica per un mandato».  Il nuovo Senato immaginato dalla riforma, ha spiegato Renzi, «non vota il bilancio, non da&rsquo; la fiducia ma concorre all&rsquo;elezione del Presidente della Repubblica e dei rappresentanti europei». PROVINCE E CITTA&rsquo; METROPOLITANE - Le Province sono un altro capitolo delle riforme da fare. «Sul superamento delle Province - ha sottolineato il segretario del Pd - non c&rsquo;e&rsquo; l&rsquo;accordo di tutti. Noi vogliamo che il 25 maggio non si voti per le Province, è possibile se il Ddl Del Rio avrà  in queste ore la svolta al Senato. Questo consentira&rsquo; di avere delle Province di secondo livello con i Sindaci protagonisti». Sul fronte delle Citta&rsquo; Metropolitane, Renzi ha spiegato che della riforma «se ne parla da trent&rsquo;anni, da venti sono state istituite» ma «sono una barzelletta, sono rimaste un oggetto misterioso», visto che non c&rsquo;e&rsquo; stato alcun passaggio di poteri costituzionali/istituzionali. ",
        body);
  }
  
  @Override
  public void testFindMainImageLink() {
    assertEquals(
        "http://images.corriereobjects.it/methode_image/2014/02/06/Politica/Foto%20Politica%20-%20Trattate/4c0ef00e139fded6d5420fd074f99829-009-kdjG--401x175@Corriere-Web-Sezioni.jpg",
        seg.findMainImageLink(rootNode));
  }
  
  @Override
  public void testFindMainDate() {
    assertEqualTimes(DatatypeConverter.parseDateTime("2014-02-06").getTime(), seg.findMainDate(rootNode));
  }
  
  @Override
  public void testFindMainAPIObjectID() {
    assertEquals("1ea07bf8-8f17-11e3-8c4a-c355fa4079e9", seg.findMainAPIObjectID(rootNode));
  }
  
  @Override
  public void testFindMainThreadURLs() {
    List<String> urls = seg.findMainThreadURLs(rootNode);
    assertEquals(2, urls.size());
    assertEquals(
        "http://xml2.temporeale.corriereobjects.it/community-corriere/commenti/articoli/1e/a0/7b/f8/8f/17/11/e3/8c/4a/c3/55/fa/40/79/e9/final/comment-head.json",
        urls.get(0));
    assertEquals(
        "http://xml2.temporeale.corriereobjects.it/community-corriere/commenti/articoli/1e/a0/7b/f8/8f/17/11/e3/8c/4a/c3/55/fa/40/79/e9/final/comment-body.json",
        urls.get(1));
  }
  
  @Override
  public void testFindMainTimeIsReal() {
    assertTrue(seg.findMainTimeIsReal(rootNode));
  }
  
  @Override
  public void testFindMainComments() {
    assertTrue(seg.findMainComments(rootNode) > 0);
  }
  
  public void test3Jun2014() throws UnsupportedEncodingException, IOException {
    // http://www.corriere.it/economia/14_giugno_03/alitalia-2400-2500-esuberi-stimati-392bbf00-eb01-11e3-9008-a2f40d753542.shtml
    StringBuilder content = readPageFromResource("alitalia-2400-2500-esuberi-stimati-392bbf00-eb01-11e3-9008-a2f40d753542.shtml",
        "Windows-1252");
    this.seg = new CorriereSegmenter();
    HtmlCleaner cleaner = createCleaner();
    this.rootNode = cleaner.clean(content.toString());
    
    String title = seg.findMainTitle(rootNode);
    assertEquals("Alitalia, “2.400-2.500 esuberi stimati”", title);
    
    String body = seg.findMainDescripcion(rootNode);
    assertEquals(
        "Gli esuberi stimati in Alitalia per l&rsquo;arrivo di Etihad nel capitale della ex compagnia di bandiera sono «tra i 2.400 e i 2.500». Lo sostiene il ministro del Lavoro, Giuliano Poletti, che a margine di un convegno in Fondazione Cariplo a Milano ha spiegato che «poi si dovrà vedere quando ci sarà la discussione di merito tra le parti».  «C&rsquo;è una valutazione &mdash; ha precisato il ministro &mdash;  intorno ai 2400/2500 esuberi. Almeno per quelle che  sono le risultanze pubbliche, poi la discussione di merito ci sarà  quando Alitalia e le parti discuteranno del piano». L&rsquo;attesa lettera con il via libera di Abu Dhabi a chiudere la partita è arrivata proprio in questi giorni. Una trentina di pagine, Etihad Airways dettaglia le «condizioni» e i «criteri» per entrare nel capitale di una newco, in cui verranno conferite le attività operative di Alitalia, con una quota compresa tra il 45 e il 49% (comunque sotto il 50% per non perdere i diritti di compagnia comunitaria), investendo 560 milioni, 500 subito e altri 60 milioni l&rsquo;anno prossimo. Uno dei punti chiave dell&rsquo;intesa era la questione esuberi.  «Bisogna poi vedere le discussioni di merito anche con i sindacati &mdash; ha subito puntualizzato Poletti &mdash; c&rsquo;è un tema di confronto perché ci sono situazioni diverse, c&rsquo;è il  personale di terra e di volo. Su Alitalia adesso parte un confronto in cui c&rsquo;è  una regia del ministro dei Trasporti, Maurizio Lupi». Il ministero del Lavoro, ha sottolineato Poletti, «è a disposizione per la parte che gli compete, in tema di ammortizzatori sociali e di problematiche relative al lavoro, su cui è prevedile e necessario un confronto tra le parti». Nessuna idea sui costi: «c&rsquo;è un fondo  volo che è nelle disponibilità del ministero della Infrastrutture che  viene utilizzato per questa tipologia di intervento &mdash; ha spiegato il ministro del Lavoro &mdash;. Bisogna capire  come questa situazione si configurerà non essendoci ancora un  accordo».  ",
        body);
    
    assertEquals("http://images2.corriereobjects.it/methode_image/socialshare/2014/06/03/f9e06002-eb01-11e3-9008-a2f40d753542.jpg",
        seg.findMainImageLink(rootNode));
    
    assertEqualTimes(DatatypeConverter.parseDateTime("2014-06-03").getTime(), seg.findMainDate(rootNode));
    
    assertTrue(seg.findMainTimeIsReal(rootNode));
    
    assertEquals("39/2b/bf/00/eb/01/11/e3/90/08/a2/f4/0d/75/35/42", seg.findMainAPIObjectID(rootNode));
    
    List<String> urls = seg.findMainThreadURLs(rootNode);
    assertEquals(2, urls.size());
    assertEquals(
        "http://xml2.temporeale.corriereobjects.it/community-corriere/commenti/articoli/39/2b/bf/00/eb/01/11/e3/90/08/a2/f4/0d/75/35/42/final/comment-head.json",
        urls.get(0));
    assertEquals(
        "http://xml2.temporeale.corriereobjects.it/community-corriere/commenti/articoli/39/2b/bf/00/eb/01/11/e3/90/08/a2/f4/0d/75/35/42/final/comment-body.json",
        urls.get(1));
    
    assertTrue(seg.findMainComments(rootNode) > 0);
    
    List<String> list = new ArrayList<String>();
    list.add("angry=63.0|sad=0.0|worried=7.0|happy=30.0|smile=0.0");
    assertEquals(list.get(0), seg.findMainSourceExtraData(rootNode).get(0));
    
  }
  
}
