/**
 * Websays Opinion Analytics Engine 
 * 
 * (Websays Copyright © 2010-2015. All rights reserved. http://websays.com )
 * 
 * Primary Author: Jorge Valderrama
 * Contributors: 
 * Date: 27/2/2015
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

/**
 *
 **/
public class CritizenSegmenterTest extends BaseDocumentSegmenterTest {
  
  /**
   * Test of segment method, of class CritizenSegmenter.
   * 
   * @return
   */
  @Test
  public void testCritizenSegmenter() throws Exception {
    URL urlComments = new URL("https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es");
    StringBuilder contentComments = readPageFromResource("critizen.com");
    HTMLDocumentSegmenter segComments = new CritizenSegmenter();
    List<SimpleItem> resultComments = segComments.segment(contentComments.toString(), urlComments);
    
    int expectedResults = 72; // No of comments
    assertEquals(expectedResults, resultComments.size());
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = resultComments.get(0);
    assertEquals("11364", sub1.threadId);
    assertEquals("11364", sub1.APIObjectID);
    assertEquals(
        "Hoy El Corte Inglés me ha enviado un tercer aviso de retraso en un pedido realizado a través de su tienda virtual. Se trata de unos auriculares que compré el 24 de Enero de 2015, y a día de hoy, si...",
        sub1.body);
    assertNull(sub1.title);
    assertEquals(
        "https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/corte-ingles-enviado-tercer-aviso-retraso-pedido_c11364",
        sub1.itemUrl);
    assertEquals("Raúl García Hernández", sub1.author);
    assertFalse(sub1.timeIsReal);
    assertEquals(urlComments.toString(), sub1.postUrl);
    assertEquals(1, sub1.position);
    assertEquals("https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/2", sub1.threadURLs.get(0));
    assertTrue(sub1.isComment);
    
    urlComments = new URL("https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/2");
    ArrayList<String> arrComms = new ArrayList<String>();
    arrComms.add(urlComments.toString());
    resultComments = segComments.segment(contentComments.toString(), urlComments);
    assertEquals(arrComms, sub1.threadURLs);
  }
  
  /* TODO: a new test that uses critizen-20150515.html as the source file of html */
  
}
