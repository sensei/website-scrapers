/**
 * Websays Opinion Analytics Engine 
 * 
 * (Websays Copyright © 2010-2015. All rights reserved. http://websays.com )
 * 
 * Primary Author: Jorge Valderrama
 * Contributors: 
 * Date: 26/2/2015
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

/**
 *
 **/
public class DirecteCommentsSegmenterTest extends BaseDocumentSegmenterTest {
  
  /**
   * Test of segment method, of class DirecteCommentsSegmenterTest.
   * 
   * @return
   */
  @Test
  public void testDirecteCommentsSegmenterTest() throws Exception {
    URL urlComments = new URL("http://www.directe.cat/ajax/ajax.php/387079/1");
    StringBuilder contentComments = readPageFromResource("directComments.cat");
    HTMLDocumentSegmenter segComments = new DirecteCommentsSegmenter();
    List<SimpleItem> resultComments = segComments.segment(contentComments.toString(), urlComments);
    
    int expectedResults = 10; // No of comments
    assertEquals(expectedResults, resultComments.size());
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = resultComments.get(0);
    assertEquals("comentari_348998", sub1.threadId);
    assertEquals("noticia/387079/freixenet-es-consolida-com-a-cava-no-apte-per-a-independentistes-i-republicans#comentari-348998",
        sub1.itemUrl);
    assertNull(sub1.title);
    assertEquals(
        " El publicista Lluís Bassat ha declarat aquest dimecres durant una entrevista a Tv3 que \"és evident que si sortim d'Espanya, sortim d'Europa\", en referència a una hipotètica independència de Catalunya.N.B.# ,41 ,35 PER DUI CAL VOTS I ,A HORES D'ARA SÓN UN 27% DEL CENS SEGONS LA CONSULTA 9-N.",
        sub1.body);
    assertEquals("BEJOTA", sub1.author);
    Calendar cal = Calendar.getInstance();
    cal.set(2015, 1, 18, 13, 33);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), sub1.date);
    assertTrue(sub1.timeIsReal);
    assertEquals(urlComments.toString(), sub1.postUrl);
    assertEquals(1, sub1.position);
    assertTrue(sub1.isComment);
  }
  
}