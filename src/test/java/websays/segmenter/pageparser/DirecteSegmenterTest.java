/**
 * Websays Opinion Analytics Engine 
 * 
 * (Websays Copyright © 2010-2015. All rights reserved. http://websays.com )
 * 
 * Primary Author: Jorge Valderrama
 * Contributors: 
 * Date: 26/2/2015
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.List;

import org.htmlcleaner.HtmlCleaner;

import websays.types.clipping.SimpleItem.LanguageTag;

public class DirecteSegmenterTest extends BaseDocumentSegmenterTest {
  
  // http://www.corriere.it/politica/14_febbraio_06/150-senza-stipendio-renzi-svela-suo-senato-1ea07bf8-8f17-11e3-8c4a-c355fa4079e9.shtml
  @Override
  public void setUp() throws Exception {
    StringBuilder content = readPageFromResource("directe.cat");
    this.seg = new DirecteSegmenter();
    HtmlCleaner cleaner = createCleaner();
    this.rootNode = cleaner.clean(content.toString());
  }
  
  @Override
  public void testFindMainAuthor() {
    String author = seg.findMainAuthor(rootNode);
    assertEquals("Tirabol Produccions", author);
  }
  
  @Override
  public void testFindMainTitle() {
    String title = seg.findMainTitle(rootNode);
    assertEquals("Freixenet és consolida com a cava no apte per a independentistes i republicans", title);
  }
  
  @Override
  public void testFindMainDescripcion() {
    String body = seg.findMainDescripcion(rootNode);
    assertEquals(
        "Els monarques espanyols han brindat aquest migdia a les caves Freixenet a Sant Sadurní d'Anoia pel primer centenari de la companyia. El president de l'empresa, Josep Lluís Bonet, ha aixecat la copa per brindar per l'alegria d'haver repartit felicitat a tot el món i d'haver servit Catalunya i Espanya. Felipe VI ha elogiat Freixenet perquè el seu bressol barceloní, a Sant Sadurní d'Anoia, i el seu cor profundament català han estat signes d'orgull espanyol passejat amb èxit exemplar per tot el món. La visita dels monarques espanyols a les caves Freixenet ha clausurat de manera oficial la commemoració pels 100 anys de la marca, celebrada durant 2014, en un dinar oficial en què Felipe i Letizia han expressat el seu reconeixement a la qual és “un exemple del millor esperit emprenedor espanyol”. ",
        body);
  }
  
  @Override
  public void testFindMainImageLink() {
    assertEquals("http://www.directe.cat/imatges/noticies/1-229.jpg", seg.findMainImageLink(rootNode));
  }
  
  @Override
  public void testFindMainDate() {
    Calendar cal = Calendar.getInstance();
    cal.set(2015, 1, 25, 12, 5);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), seg.findMainDate(rootNode));
  }
  
  @Override
  public void testFindMainAPIObjectID() {
    assertEquals("286355256889", seg.findMainAPIObjectID(rootNode));
  }
  
  @Override
  public void testFindMainThreadURLs() {
    List<String> urls = seg.findMainThreadURLs(rootNode);
    assertEquals(5, urls.size());
    assertEquals("http://www.directe.cat/ajax/ajax.php/387079/1", urls.get(0));
    assertEquals("http://www.directe.cat/ajax/ajax.php/387079/2", urls.get(1));
    assertEquals("http://www.directe.cat/ajax/ajax.php/387079/3", urls.get(2));
    assertEquals("http://www.directe.cat/ajax/ajax.php/387079/4", urls.get(3));
    assertEquals("http://www.directe.cat/ajax/ajax.php/387079/5", urls.get(4));
  }
  
  @Override
  public void testFindMainTimeIsReal() {
    assertTrue(seg.findMainTimeIsReal(rootNode));
  }
  
  @Override
  public void testFindMainLanguage() {
    assertEquals(LanguageTag.catalan, seg.findMainLanguage(rootNode));
  }
}
