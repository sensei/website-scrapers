package websays.segmenter.pageparser;

/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.List;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class ElConfidencialCommentsSegmenterTest extends SegmenterTest {
  
  private static final SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
  
  /**
   * Test of segment method, of class ElConfidencialCommentsSegmenter.
   * 
   * @return
   */
  @Test
  public void testElConfidencialCommentsSegmenter() throws Exception {
    URL urlComments = new URL("http://www.elconfidencial.com/comunidad/noticia/147746/0/1/");
    StringBuilder contentComments = readPageFromResource("elconfidencialComments.com");
    HTMLDocumentSegmenter segComments = new ElConfidencialCommentsSegmenter();
    List<SimpleItem> resultComments = segComments.segment(contentComments.toString(), urlComments);
    
    int expectedResults = 50; // No of comments
    assertEquals(expectedResults, resultComments.size());
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = resultComments.get(0);
    assertEquals("574248", sub1.threadId);
    assertEquals("http://www.elconfidencial.com/comunidad/comentario/574248/", sub1.itemUrl);
    assertEquals(
        "La CNMC... ¿y qué hace mientras el cochambroso e incompetente Ministro José Manuel Soria? ¿vale para algo este tío?. Ha tenido que actuar la CNMC y el Ministro de Industria, el del ramo, nada, es un desastre de Ministro, dificil ser peor que los precedentes, pero lo ha conseguido. Y al final el posible sancionado será la empresa -¡y ya veremos si se llega hasta ahí!- porque los chupasangres que nos están sajando con el precio de la luz y el robo a todos los demás sectores productivos que son los dirigentes de esta empresa COBRANDO MILLONADAS, a esos no les pasará nada... A España le falta muchísima democracia aún, muchísima.\" La CNMC... ¿y qué hace mientras el cochambroso e incompetente Ministro José Manuel Soria? ¿vale para algo este tío?. Ha tenido que actuar la CNMC y el Ministro de Industria, el del ramo, nada, es un desastre de Ministro, dificil ser peor que los precedentes, pero lo ha conseguido. Y al final el posible sancionado será la empresa -¡y ya veremos si se llega hasta ahí!- porque los chupasangres que nos están sajando con el precio de la luz y el robo a todos los demás sectores productivos que son los dirigentes de esta empresa COBRANDO MILLONADAS, a esos no les pasará nada... A España le falta muchísima democracia aún, muchísima.\" >En respuesta a observador",
        sub1.title);
    assertEquals(
        "La CNMC... ¿y qué hace mientras el cochambroso e incompetente Ministro José Manuel Soria? ¿vale para algo este tío?. Ha tenido que actuar la CNMC y el Ministro de Industria, el del ramo, nada, es un desastre de Ministro, dificil ser peor que los precedentes, pero lo ha conseguido. Y al final el posible sancionado será la empresa -¡y ya veremos si se llega hasta ahí!- porque los chupasangres que nos están sajando con el precio de la luz y el robo a todos los demás sectores productivos que son los dirigentes de esta empresa COBRANDO MILLONADAS, a esos no les pasará nada... A España le falta muchísima democracia aún, muchísima.\" La CNMC... ¿y qué hace mientras el cochambroso e incompetente Ministro José Manuel Soria? ¿vale para algo este tío?. Ha tenido que actuar la CNMC y el Ministro de Industria, el del ramo, nada, es un desastre de Ministro, dificil ser peor que los precedentes, pero lo ha conseguido. Y al final el posible sancionado será la empresa -¡y ya veremos si se llega hasta ahí!- porque los chupasangres que nos están sajando con el precio de la luz y el robo a todos los demás sectores productivos que son los dirigentes de esta empresa COBRANDO MILLONADAS, a esos no les pasará nada... A España le falta muchísima democracia aún, muchísima.\" >En respuesta a observadorA primeros de  febrero el ministro cambió el sistema de subasta del precio de la luz, que  era precisamente donde se producía la manipulación del precio.Los que no hicieron NADA fueron los anteriores misnitros de industria y economía.....Al Cesar o que es del Cesar",
        sub1.body);
    // assertEquals("vicenta", sub1.author);
    assertEqualTimes(sdf.parse("18 Jun 2014"), sub1.date); // June 18, 2014
    assertEquals(urlComments.toString(), sub1.postUrl);
    assertEquals(1, sub1.position);
    assertTrue(sub1.isComment);
    assertEquals("http://www.elconfidencial.com/comunidad/noticia/147746/0/2/", sub1.threadURLs.get(0));
    
    // ///////// Test Second comment item //////////////
    SimpleItem sub2 = resultComments.get(1);
    assertEquals("573817", sub2.threadId);
    assertEquals("#", sub2.title);
    assertEquals(
        "Cuando un ciudadno comete una infraccion de trafico, las multas mas comunes son 100 pavos. El 10% del sueldo de un mileurista que es el 50% de los trabajadores como poco. Si te equivocas en la declaracion de la renta, te meten un palo que te dejan tieso. Si te lo llevas crudo y manipulas el precio de las cosas, te ponen una multa equivalente a un cafe para iberdrola. A los corruptos les pasa igual, roban a manos llenas, no devuelven 1 euro, 5 años a la carcel y a disfrutar. Una ley justa haria que si devuelves la pasta 5 años, i no la devuelves 40 como un terrorista, has robado pero no vas a disfrutarlo.",
        sub2.body);
    // assertEquals("aircrack_85", sub2.author);
    assertEqualTimes(sdf.parse("17 Jun 2014"), sub2.date); // June 18, 2014
    assertEquals(urlComments.toString(), sub2.postUrl);
    assertEquals(2, sub2.position);
    assertTrue(sub2.isComment);
    assertNull(sub2.threadURLs);
  }
}
