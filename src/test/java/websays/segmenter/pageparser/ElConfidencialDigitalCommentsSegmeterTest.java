/**
 * Websays Opinion Analytics Engine 
 * 
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 * 
 * Primary Author: Jorge Valderrama
 * Contributors: 
 * Date: 27/1/2015
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

/**
 *
 **/
public class ElConfidencialDigitalCommentsSegmeterTest extends BaseDocumentSegmenterTest {
  
  private static final SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
  
  /**
   * Test of segment method, of class ElConfidencialDigitalCommentsSegmenter.
   * 
   * @return
   */
  @Test
  public void testElConfidencialDigitalCommentsSegmenter() throws Exception {
    URL urlComments = new URL(
        "http://www.elconfidencialdigital.com/bbtcomment/entityComments/0/ECDNWS20150126_0037.json?commentsPage=1&limit=1000");
    StringBuilder contentComments = readPageFromResource("elconfidencialdigitalComments.com");
    HTMLDocumentSegmenter segComments = new ElConfidencialDigitalCommentsSegmenter();
    List<SimpleItem> resultComments = segComments.segment(contentComments.toString(), urlComments);
    
    int expectedResults = 10; // No of comments
    assertEquals(expectedResults, resultComments.size());
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = resultComments.get(0);
    assertEquals("#46", sub1.threadId);
    assertNull(sub1.itemUrl);
    assertNull(sub1.title);
    assertTrue(sub1.body
        .contains("Para lerdos lerdos los peperros que no han pillado de la caja b del PP, que por lo que dice la poli, son dos y el del tambor."));
    assertEquals("asi es", sub1.author);
    Calendar cal = Calendar.getInstance();
    cal.set(2015, 0, 27, 13, 15);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), sub1.date);
    assertTrue(sub1.timeIsReal);
    assertEquals(urlComments.toString(), sub1.postUrl);
    assertEquals(1, sub1.position);
    assertTrue(sub1.isComment);
  }
  
}
