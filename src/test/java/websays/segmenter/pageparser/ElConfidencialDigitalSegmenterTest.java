/**
 * Websays Opinion Analytics Engine 
 * 
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 * 
 * Primary Author: Jorge Valderrama
 * Contributors: 
 * Date: 27/1/2015
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;

import org.htmlcleaner.HtmlCleaner;

import websays.types.clipping.SimpleItem.LanguageTag;

/**
 *
 **/
public class ElConfidencialDigitalSegmenterTest extends BaseDocumentSegmenterTest {
  
  @Override
  public void setUp() throws Exception {
    // http://www.elconfidencialdigital.com/dinero/Botin-FG-Galan-Davos-Podemos_0_2425557457.html
    StringBuilder content = readPageFromResource("elconfidencialdigital.com");
    this.seg = new ElConfidencialDigitalSegmenter();
    HtmlCleaner cleaner = createCleaner();
    this.rootNode = cleaner.clean(content.toString());
  }
  
  @Override
  public void testFindMainTitle() {
    String title = seg.findMainTitle(rootNode);
    assertEquals("Botín, FG y Galán no consiguen tranquilizar en Davos sobre los peligros de Podemos", title);
  }
  
  @Override
  public void testFindMainDescripcion() {
    String body = seg.findMainDescripcion(rootNode);
    assertEquals(
        "\u201CEn Europa est\u00E1n surgiendo partidos como Podemos en Espa\u00F1a\u201D. Fue la advertencia que lanz\u00F3 este fin de semana el influyente inversor estadounidense Ray Dalio, m\u00E1ximo ejecutivo del fondo Bridgewater, en uno de los debates del foro econ\u00F3mico de Davos.Realiz\u00F3 estas declaraciones, en las que destac\u00F3 su honda preocupaci\u00F3n por este asunto, delante de Ana Bot\u00EDn. Sin embargo, la presidenta del Banco Santander no quiso responder en p\u00FAblico a la alusi\u00F3n de Dalio a Podemos.Seg\u00FAn ha sabido El Confidencial Digital, de miembros de la delegaci\u00F3n espa\u00F1ola reci\u00E9n llegados de Davos, los presidentes del Santander, BBVA e Iberdrola s\u00ED han mantenido apartes, en privado, con m\u00E1ximos ejecutivos de los principales fondos y bancos de inversi\u00F3n extranjeros, como Goldman Sachs, Blackrock y Bridgewater.Estos l\u00EDderes mundiales les mostraron sin rodeos su inquietud sobre los peligros que, desde su punto de vista, puede acarrear que Podemos tenga opciones de Gobierno, no solo para Espa\u00F1a sino tambi\u00E9n para el resto de Europa.Las fuentes a las que ha tenido acceso ECD revelan que los presidentes de las principales empresas espa\u00F1olas transmitieron a sus interlocutores que est\u00E1n \"convencidos\" de que los espa\u00F1oles van a poner el Gobierno en manos de dirigentes que aseguren la estabilidad Argumentaron tambi\u00E9n que la situaci\u00F3n de Grecia, tras el triunfo del partido de Syriza, servir\u00E1 tambi\u00E9n para resaltar los peligros del populismo y, antes de acudir a las urnas en las distintas convocatorias electorales de este 2015, ayudar\u00E1 a meditar a la mayor\u00EDa de la poblaci\u00F3n espa\u00F1ola, a la que consideran \"m\u00E1s moderada que la griega\u201D.Seg\u00FAn las fuentes consultadas por ECD, las respuestas que los presidentes de las compa\u00F1\u00EDas m\u00E1s importantes del pa\u00EDs han escuchado sobre este foco de preocupaci\u00F3n no son positivas.Estos contactos privados en Davos han quedado lejos del objetivo de calmar a los inversores internacionales sobre los riesgos de Podemos.M\u00E1ximos ejecutivos, especialmente de fondos norteamericanos, aseguraron a los directivos espa\u00F1oles que no les tranquiliza nada el que se deje sin m\u00E1s en manos de los ciudadanos la elecci\u00F3n de los dirigentes en los pr\u00F3ximos comicios, es decir, sin que antes se desarrolle una labor de explicaci\u00F3n y de pedagog\u00EDa hacia los espa\u00F1oles sobre qu\u00E9 futuro les aguarda si el partido de Pablo Iglesias llega a gobernar.Consideran que confiar sin m\u00E1s en el buen sentido de la ciudadan\u00EDa no ayuda a despejar las dudas sobre la estabilidad del pa\u00EDs a corto plazo.Los interlocutores les recomendaron que expliquen a la poblaci\u00F3n que lo m\u00E1s responsable ser\u00E1 votar a pol\u00EDticos que se muestren decididos a tomar las medidas que consoliden la recuperaci\u00F3n. Hacerles ver que es necesario respaldar, en definitiva, un programa pol\u00EDtico que garantice la estabilidad y la salida de la crisis.Algunos de estos l\u00EDderes mundiales, con los que charlaron durante la reuni\u00F3n de Davos, llegaron a calificar de \u201Cextremista\u201D al partido de Pablo Iglesias.Lo consideran incluso m\u00E1s radical que a la izquierda griega, representada por Syriza, sobre todo por la \u201Cindefinici\u00F3n\u201D de sus propuestas.Un an\u00E1lisis que llam\u00F3 poderosamente la atenci\u00F3n a los directivos espa\u00F1oles que lo escucharon.Pero la delegaci\u00F3n espa\u00F1ola ha regresado, al menos, con dos mensajes positivos que ha recibido de los ejecutivos mundiales con los que ha establecido contactos en Davos.La econom\u00EDa espa\u00F1ola ha recibido elogios de personalidades de alto nivel que no se hab\u00EDan escuchado en los \u00FAltimos cinco a\u00F1os.Otra de las buenas noticias es que Catalu\u00F1a ha pasado a un segundo plano en Davos, tras el no en el refer\u00E9ndum escoc\u00E9s&nbsp;Los inversores dan por buena la versi\u00F3n del Gobierno de que la consulta del 9-N no fue m\u00E1s que un simulacro y que, en todo caso, la mayor\u00EDa de los catalanes no acudieron a las urnas. Lo que se interpreta como \u201Ctodo un portazo a la independencia\u201D.",
        body);
  }
  
  @Override
  public void testFindMainImageLink() {
    assertEquals("http://images.elconfidencialdigital.com/politica/lider-Podemos-Pablo-Iglesias_ECDIMA20150126_0019_3.jpg",
        seg.findMainImageLink(rootNode));
  }
  
  @Override
  public void testFindMainDate() {
    Calendar cal = Calendar.getInstance();
    cal.set(2015, 0, 27);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), seg.findMainDate(rootNode));
  }
  
  @Override
  public void testFindMainThreadURLs() {
    assertEquals("http://www.elconfidencialdigital.com/bbtcomment/entityComments/0/ECDNWS20150126_0037.json?commentsPage=1&limit=1000", seg
        .findMainThreadURLs(rootNode).get(0));
  }
  
  @Override
  public void testFindMainLanguage() {
    assertEquals(LanguageTag.spanish, seg.findMainLanguage(rootNode));
  }
  
}
