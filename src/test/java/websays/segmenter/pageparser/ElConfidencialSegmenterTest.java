/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;

import websays.types.clipping.SimpleItem.LanguageTag;

public class ElConfidencialSegmenterTest extends BaseDocumentSegmenterTest {
  
  private static final Logger logger = Logger.getLogger(ElConfidencialSegmenterTest.class);
  
  @Override
  public void setUp() throws Exception {
    StringBuilder content = readPageFromResource("elconfidencial.com");
    this.seg = new ElConfidencialSegmenter();
    HtmlCleaner cleaner = createCleaner();
    this.rootNode = cleaner.clean(content.toString());
  }
  
  @Override
  public void testFindMainTitle() {
    String title = seg.findMainTitle(rootNode);
    assertEquals("La CNMC abre expediente muy grave a Iberdrola por manipular el precio de la luz - Noticias de Empresas", title);
  }
  
  @Override
  public void testFindMainDescripcion() {
    String body = seg.findMainDescripcion(rootNode);
    assertEquals(
        "La Comisión Nacional de los Mercados y la Competencia (CNMC) ha incoado un expediente sancionador a Iberdrola por un posible caso de \"manipulación fraudulenta que altere precio\", según una notificación oficial del regulador.Fuentes conocedoras del proceso indicaron a Ep que los hechos que se investigan corresponden a las fuertas subidas que experimentó el mercado mayorista de la electricidad, conocido como 'pool', entre el 5 y el 8 de diciembre, esto es, en pleno puente de la Constitución.Estos días, los precios de la electricidad rebasaron la cota de los 90 euros por megavatio hora (MWh), unos niveles desconocidos desde comienzos de siglo, en un contexto de menor contribución de las energías renovables e indisposición de algunos grupos térmicos.Las fuentes señalan que Iberdrola es la única compañía eléctrica a la que se abre expediente sancionador. El proceso se inicia ante el posible incumplimiento de lo establecido en el artículo 60.a.15 de la Ley del Sector Eléctrico por parte de la empresa Iberdrola Generación, filial de Iberdrola de generación eléctrica.La sanción a la que puede enfrentarse la compañía asciende a un máximo de 30 millones de euros, ya que las prácticas investigadas se consideran \"muy graves\" dentro de la regulación. La decisión de incoar el expediente sancionador fue adoptada en una reunión celebrada ayer por el consejo de administración de la CNMC.Fuentes de la CNMC consultadas por Ep eludieron realizar declaraciones acerca de esta investigación en curso y no precisaron el contenido de la investigación. Además, insistieron en que la apertura de un expediente no prejuzga el resultado de la investigación. A partir de ahora, la CNMC dispone de un plazo de 18 meses para resolver el proceso, que ha pasado a la segunda fase de investigación, al considerar que existen indicios de irregularidades. A partir de este momento, Iberdrola deberá responder a los requerimientos formales del regulador.Iberdrola niega cualquier manipulaciónLa compañía ha negado \"rotundamente\" haber realizado cualquier tipo de manipulación sobre los precios ofertados por sus instalaciones de generación eléctrica. En un comunicado, Iberdrola afirma que los servicios jurídicos de la empresa presentarán las alegaciones pertinentes dentro del plazo de quince días legalmente establecido y añade que la CNMC tiene hasta 18 meses de plazo para resolver el expediente sancionador, cuya resolución puede ser recurrida posteriormente ante la Audiencia Nacional.La compañía eléctrica destaca que la CNMC ha venido revisando desde el pasado mes de diciembre la actuación de todo el parque de producción de Iberdrola Generación SAU a lo largo de 2013 y aclara que el procedimiento sancionador afecta únicamente a las ofertas realizadas por determinadas instalaciones de producción hidroeléctrica situadas en las cuencas de los ríos Duero, Tajo y Sil entre el 30 de noviembre y el 23 de diciembre de 2013. ",
        body);
  }
  
  @Override
  public void testFindMainImageLink() {
    assertEquals("http://www.ecestaticos.com/imagestatic/clipping/57e/672/99f/57e67299fceceea4cebe94a3cef65cde.jpg?mtime=1403002511",
        seg.findMainImageLink(rootNode));
  }
  
  @Override
  public void testFindMainDate() {
    Calendar cal = Calendar.getInstance();
    cal.set(2014, 5, 17);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), seg.findMainDate(rootNode));
  }
  
  @Override
  public void testFindMainAPIObjectID() {
    assertEquals("107842983211", seg.findMainAPIObjectID(rootNode));
  }
  
  @Override
  public void testFindMainThreadURLs() {
    // 147746 id comments
    assertEquals("http://www.elconfidencial.com/comunidad/noticia/147746/0/1/", seg.findMainThreadURLs(rootNode).get(0));
  }
  
  @Override
  public void testFindMainLanguage() {
    assertEquals(LanguageTag.spanish, seg.findMainLanguage(rootNode));
  }
  
}
