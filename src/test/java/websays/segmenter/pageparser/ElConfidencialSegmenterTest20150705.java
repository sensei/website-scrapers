/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;

import websays.types.clipping.SimpleItem.LanguageTag;

/**
 * This is a test for "El confidencial" using an article published during May the 7th 2015
 * 
 * @author juanfra
 *
 */
public class ElConfidencialSegmenterTest20150705 extends BaseDocumentSegmenterTest {
  
  private static final Logger logger = Logger.getLogger(ElConfidencialSegmenterTest20150705.class);
  
  @Override
  public void setUp() throws Exception {
    String resource = "ElConfidencia20150507.html";
    StringBuilder content = new StringBuilder();
    try {
      content = readPageFromResource(resource);
    } catch (Exception e) {
      // TODO Auto-generated catch block
      logger.error("Not possible to find page from resource [" + resource + "] reason [" + e.getMessage() + "]", e);
    }
    
    this.seg = new ElConfidencialSegmenter();
    HtmlCleaner cleaner = createCleaner();
    this.rootNode = cleaner.clean(content.toString());
  }
  
  @Override
  public void testFindMainTitle() {
    String title = seg.findMainTitle(rootNode);
    assertEquals("La Ciudad Financiera del Banco Santander sale a subasta por 2.000 millones de euros. Noticias de Empresas", title);
  }
  
  @Override
  public void testFindMainDescripcion() {
    String body = seg.findMainDescripcion(rootNode);
    assertEquals(
        "Rafael Gimeno-Bayón, el polémico magistrado del Consejo General del Poder Judicial que apenas duró un año en el cargo tras apoyar la inscripción de Sortu como partido político, decidirá en los tres próximos meses quién será el nuevo casero del Banco Santander. Como liquidador de Marme Inversiones, la sociedad propietaria de la Ciudad Financiera de la entidad bancaria, ha lanzado la subasta al mejor postor para comprar los edificios de la sede central del grupo financiero.Así lo aseguran fuentes conocedoras de una situación que empezó a torcerse hace ya tres años. Propinves, la firma que compró en 2008 la Ciudad Financiera por 1.900 millones, dejó de hacer frente en 2011 a los préstamos que cinco bancos le dieron para ejecutar la que fue la mayor operación inmobiliaria de España. En 2013, la sociedad entró en default, varios fondos oportunistas compraron parcialmente la deuda y el Juzgado de lo Mercantil número 9 de Madrid aceptó el preconcurso de acreedores.El pasado año, Marme Inversiones y dos de sus filiales cayeron definitivamente en suspensión de pagos sin que el dueño llegara a un acuerdo con los prestamistas para levantar el concurso. La firma está ahora en liquidación y el administrador ha decidido lanzar la subasta oficial. Durante los tres próximos meses, Lexaudit &amp; Concursal, el administrador para el que trabaja Rafael Gimeno-Bayón, recibirá las ofertas de los potenciales interesados. Se espera que la elección del comprador sea en septiembre si el proceso sigue el curso normal.Entre los que pueden convertirse en el propietario de la Ciudad Financiera están todos los fondos internacionales que han invertido más de 20.000 millones en los dos últimos años en adquirir inmuebles, como Cerberus, Blackstone, HiG, Kennedy Wilson, TPG, Apollo, Varde Partners, Intu Properties o TIIA Henderson. No obstante, cualquiera de ellos tendrá que pasar por el despacho de Ana Botín porque el propio Banco Santander tiene un derecho preferente de tanteo sobre cualquier oferta.Fuentes próximas a la entidad financiera han indicado que, en un principio, no hay un interés especial en recomprar los edificios vendidos hace ahora siete años. No obstante, afirman que analizarán las propuestas que lleguen al administrador concursal ya que, en caso de que las ofertas sean a un precio razonable, dejan la puerta abierta a adquirir los inmuebles por los que en 2008 ingresó 1.900 millones de euros, con plusvalías de más de 600 millones.Entre 2007 y 2008, Banco Santander formalizó la venta de diez inmuebles singulares, de 1.152 sucursales situadas en España y de las oficinas centrales (Ciudad Financiera) con distintos compradores, la mayoría de los cuales han terminado quebrados. A la par, el grupo firmó con dichas sociedades contratos de arrendamiento operativo (mantenimiento, seguros y tributos) de los citados inmuebles por distintos plazos de obligado cumplimiento (entre 12 y 15 años para los inmuebles singulares, entre 24 y 26 para las oficinas y 40 años para la Ciudad Financiera).En consecuencia, la entidad tiene que permanecer en Boadilla del Monte hasta 2048, un contrato de alquiler con distintos acuerdos de revisión de las correspondientes rentas durante dichos períodos y posibles prórrogas. Un arrendamiento que es la principal garantía para los potenciales compradores, ya que tienen las rentas (cash flow) aseguradas. El problema que tuvo el anterior propietario, Marme Inversiones, es que el coste de la deuda en la que incurrió era superior a los ingresos, una vez descontada la pérdida del valor de los activos.Santander pagó el pasado año 292 millones de euros por los alquileres (286 y 269 millones en 2013 y 2012, respectivamente). El valor presente de los pagos futuros mínimos en que incurrirá el grupo durante el periodo de obligado cumplimiento asciende 239 millones de euros en el plazo de un año, 820 millones de euros entre uno y cinco años y 1.708 millones de euros a más de cinco años. Prácticamente lo mismo que costaría recomprarla.",
        body);
  }
  
  @Override
  public void testFindMainImageLink() {
    assertEquals("http://1www.ecestaticos.com/imagestatic/clipping/1a5/56d/d05/1a556dd05418876e5183ebc3a916314a.jpg?mtime=1430942783",
        seg.findMainImageLink(rootNode));
  }
  
  @Override
  public void testFindMainDate() {
    Calendar cal = Calendar.getInstance();
    cal.set(2015, 4, 6);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), seg.findMainDate(rootNode));
  }
  
  @Override
  public void testFindMainAPIObjectID() {
    assertEquals("107842983211", seg.findMainAPIObjectID(rootNode));
  }
  
  @Override
  public void testFindMainThreadURLs() {
    // 147746 id comments
    assertEquals("http://www.elconfidencial.com/comunidad/noticia/789240/0/1/", seg.findMainThreadURLs(rootNode).get(0));
  }
  
  @Override
  public void testFindMainLanguage() {
    assertEquals(LanguageTag.spanish, seg.findMainLanguage(rootNode));
  }
  
}
