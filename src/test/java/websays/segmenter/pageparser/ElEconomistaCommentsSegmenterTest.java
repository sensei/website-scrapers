/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Oct 27, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;
import websays.types.clipping.SimpleItem.LanguageTag;

public class ElEconomistaCommentsSegmenterTest extends SegmenterTest {
  
  @Test
  public void testElEconomistaCommentsSegmenter() throws Exception {
    URL urlComments = new URL(
        "http://www.eleconomista.es/indicadores-espana/noticias/6181497/10/14/El-paro-baja-en-195200-personas-en-el-tercer-trimestre-la-tasa-de-desempleo-desciende-hasta-el-2367.html#Comentarios");
    StringBuilder contentComments = readPageFromResource("eleconomista.es");
    HTMLDocumentSegmenter segComments = new ElEconomistaCommentsSegmenter();
    List<SimpleItem> resultComments = segComments.segment(contentComments.toString(), urlComments);
    
    int expectedResults = 25; // No of comments
    assertEquals(expectedResults, resultComments.size());
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = resultComments.get(0);
    assertEquals(null, sub1.threadId);
    assertEquals("El paro se redujo en 195.200 personas en el tercer trimestre y la tasa cae al 23,67% - elEconomista.es", sub1.title);
    assertEquals("Muy buena noticia. ", sub1.body);
    assertEquals("Juan", sub1.author);
    assertEquals(null, sub1.imageLink);
    assertEquals(new Integer(-15), sub1.likes);
    Calendar cal = Calendar.getInstance();
    cal.set(2014, 9, 23, 9, 10);
    assertEqualTimes(cal.getTime(), sub1.date);
    assertEquals(urlComments.toString(), sub1.postUrl);
    assertEquals(1, sub1.position);
    assertTrue(sub1.isComment);
    assertEquals(LanguageTag.spanish, sub1.language);
    assertEquals(
        "http://www.eleconomista.es/indicadores-espana/noticias/6181497/10/14/2/El-paro-baja-en-195200-personas-en-el-tercer-trimestre-la-tasa-de-desempleo-desciende-hasta-el-2367.html#Comentarios",
        sub1.threadURLs.get(0));
  }
  
  @Test
  public void testElEconomistaCommentsSegmenter1() throws Exception {
    URL urlComments = new URL(
        "http://www.eleconomista.es/emprendedores-pymes/noticias/6103473/09/14/El-autonomo-trabaja-mejor-porque-esta-en-juego-su-nombre-no-la-empresa.html#Comentarios");
    StringBuilder contentComments = readPageFromResource("eleconomista1.es");
    HTMLDocumentSegmenter segComments = new ElEconomistaCommentsSegmenter();
    List<SimpleItem> resultComments = segComments.segment(contentComments.toString(), urlComments);
    
    int expectedResults = 10; // No of comments
    assertEquals(expectedResults, resultComments.size());
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = resultComments.get(0);
    assertEquals(null, sub1.threadId);
    assertEquals("''El autónomo trabaja mejor porque está en juego su nombre, no la empresa'' - elEconomista.es", sub1.title);
    assertEquals("Con Jefes así da gusto, motivar y valorar al empleado. ", sub1.body);
    assertEquals("Monicote", sub1.author);
    assertEquals(null, sub1.imageLink);
    assertEquals(new Integer(-4), sub1.likes);
    Calendar cal = Calendar.getInstance();
    cal.set(2014, 8, 24, 14, 19);
    assertEqualTimes(cal.getTime(), sub1.date);
    assertEquals(urlComments.toString(), sub1.postUrl);
    assertEquals(1, sub1.position);
    assertTrue(sub1.isComment);
    assertEquals(LanguageTag.spanish, sub1.language);
    assertEquals(
        "http://www.eleconomista.es/emprendedores-pymes/noticias/6103473/09/14/2/El-autonomo-trabaja-mejor-porque-esta-en-juego-su-nombre-no-la-empresa.html#Comentarios",
        sub1.threadURLs.get(0));
  }
  
}
