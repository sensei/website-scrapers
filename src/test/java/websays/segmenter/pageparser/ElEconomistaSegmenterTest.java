/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Oct 24, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.Calendar;
import java.util.List;

import org.htmlcleaner.HtmlCleaner;
import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;
import websays.types.clipping.SimpleItem.LanguageTag;

public class ElEconomistaSegmenterTest extends BaseDocumentSegmenterTest {
  
  @Override
  public void setUp() throws Exception {
    // http://www.eleconomista.es/indicadores-espana/noticias/6181497/10/14/El-paro-baja-en-195200-personas-en-el-tercer-trimestre-la-tasa-de-desempleo-desciende-hasta-el-2367.html
    StringBuilder content = readPageFromResource("eleconomista.es");
    this.seg = new ElEconomistaSegmenter();
    HtmlCleaner cleaner = createCleaner();
    this.rootNode = cleaner.clean(content.toString());
  }
  
  @Override
  public void testFindMainTitle() {
    assertEquals("El paro se redujo en 195.200 personas en el tercer trimestre y la tasa cae al 23,67% - elEconomista.es",
        seg.findMainTitle(rootNode));
  }
  
  @Override
  public void testFindMainAuthor() {
    assertEquals("Europa Press", seg.findMainAuthor(rootNode));
  }
  
  @Override
  public void testFindMainDescripcion() {
    assertEquals(
        " El paro bajó en 195.200 personas en el tercer trimestre del año respecto al trimestre anterior, un 3,5% menos, registrando su mayor descenso en un tercer trimestre dentro la serie histórica, según datos de la Encuesta de Población Activa (EPA) publicados este jueves por el Instituto Nacional de Estadística (INE). La otra cara de la EPA: un retrato de España a través de 30 datos curiosos. Tras este recorte del desempleo en el periodo julio-septiembre, el número total de parados alcanzó la cifra de 5.427.700 personas, su nivel más bajo desde el cuarto trimestre de 2011. Así, la tasa de paro bajó ocho décimas en el tercer trimestre, hasta situarse en el 23,67%, experimentando esta tasa su mayor descenso en un tercer trimestre desde 2005. En el último año, el paro se ha reducido un 8,7%, con 515.700 desempleados menos.  Entre julio y septiembre se crearon 151.000 empleos (+0,87%), el mayor incremento de la ocupación desde el tercer trimestre de 2007. En el último año, la ocupación ha aumentado en 274.000 personas, a un ritmo del 1,59%. Al finalizar septiembre, el número total de ocupados alcanzaba los 17.504.000, nivel que no se registraba desde el tercer trimestre de 2012.  Todos los empleos creados entre julio y septiembre pertenecían al sector privado, que registró un avance de la ocupación de 154.900 personas (+1,07%), mientras que el empleo público se redujo en este trimestre en 3.900 personas (-0,13%). El sector público acumula un ajuste de 17.700 puestos de trabajo en el último año (-0,6%), mientras que el privado ha creado 291.600 empleos (+2%). En el tercer trimestre de 2014, el número de asalariados se incrementó en 95.700 personas (+0,7%), tras reducirse los contratados con carácter indefinido en 26.700 personas (-0,25%) y aumentar en 122.400 los asalariados con contrato temporal (+3,6%). Los hogares con todos sus miembros en paro bajaron en el tercer trimestre del año en 44.600, lo que supone un 2,4% menos que en el trimestre anterior, hasta situarse en 1.789.400, según datos de la EPA. En el último año los hogares con todos sus miembros en paro se han reducido en 104.200, un 5,5% menos.  Por su parte, los hogares con todos sus integrantes ocupados aumentaron en 165.400 entre julio y septiembre de este año, un 1,8% respecto al trimestre anterior, hasta un total de 9.073.100 hogares. En el último año, las familias con todos sus miembros ocupados han subido en 353.700 (+4%).  Los hogares con al menos un activo descendieron en 9.700 (-0,07%) en el tercer trimestre y suman 13.365.100 hogares, cifra superior en 27.600 a la del tercer trimestre de 2013 (+0,2%), mientras los hogares en los que no hay ningún activo aumentaron en 34.000 en el tercer trimestre, hasta superar los 4,99 millones. En comparación con el tercer trimestre de 2013, los hogares en los que no hay ningún activo crecieron en 107.700 (+2,2%). El número de jóvenes en paro menores de 25 años subió en 27.000 personas en el tercer trimestre, lo que supone un 3,2% más que en el trimestre anterior, situándose la cifra total de jóvenes en situación de desempleo en 867.600 al finalizar el pasado mes de septiembre, de acuerdo con los datos de la EPA de este tercer trimestre.  No obstante, la tasa de paro de este colectivo bajó hasta el 52,38% al término del tercer trimestre, lo que supone siete décimas menos que en el trimestre anterior, cuando el desempleo de los jóvenes menores de 25 años se situó en el 53,12%.  El hecho de que el paro entre los jóvenes aumente en este trimestre pero descienda al mismo tiempo su tasa de desempleo se explica por la evolución de los activos. En concreto, el número de jóvenes activos se incrementó en 73.700 personas en el tercer trimestre (+4,6%), hasta situarse el total de activos en 1.656.100 jóvenes.  En el último año, el número de jóvenes en paro ha descendido en 94.000 desempleados (-9,7%), mientras que el de activos se ha reducido en 120.000 (-6,7%). La tasa de paro actual de los menores de 25 años es casi 1,8 puntos inferior a la existente en el tercer trimestre del año pasado (54,14%).  De los más de 5,4 millones de parados contabilizados en España al finalizar septiembre, el 16% son jóvenes menores de 25 años y el 50,6% son parados de larga duración (más de un año en el desempleo).  El número de parados de larga duración descendió en 122.300 personas en el tercer trimestre, un 4,2% menos que en el trimestre anterior, hasta situarse en 2.746.600. En el último año, los parados de larga duración se han reducido en 165.600 personas (-5,7%). &amp;amp;amp;amp;lt;/p&amp;amp;amp;amp;gt;          &amp;amp;amp;amp;lt;/body&amp;amp;amp;amp;gt;&amp;amp;amp;lt;/p&amp;amp;amp;gt;&amp;amp;amp;lt;p&amp;amp;amp;gt;&amp;amp;amp;amp;lt;/html&amp;amp;amp;amp;gt; ",
        seg.findMainDescripcion(rootNode));
  }
  
  @Override
  public void testFindMainImageLink() {
    assertEquals("http://s01.s3c.es/imag/video/1048x576/6/e/6/record-caida-paro.jpg", seg.findMainImageLink(rootNode));
  }
  
  @Override
  public void testFindMainDate() {
    Calendar cal = Calendar.getInstance();
    cal.set(2014, 9, 23, 9, 3, 0);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), seg.findMainDate(rootNode));
  }
  
  @Override
  public void testFindMainAPIObjectID() {
    assertNull(seg.findMainAPIObjectID(rootNode));
  }
  
  @Override
  public void testFindMainThreadURLs() {
    assertEquals(
        "http://www.eleconomista.es/indicadores-espana/noticias/6181497/10/14/El-paro-baja-en-195200-personas-en-el-tercer-trimestre-la-tasa-de-desempleo-desciende-hasta-el-2367.html#Comentarios",
        seg.findMainThreadURLs(rootNode).get(0));
  }
  
  @Override
  public void testFindMainTimeIsReal() {
    assertTrue(seg.findMainTimeIsReal(rootNode));
  }
  
  @Override
  public void testFindMainLanguage() {
    assertEquals(LanguageTag.spanish, seg.findMainLanguage(rootNode));
  }
  
  @Test
  public void test() throws Exception {
    URL url = new URL(
        "http://www.eleconomista.es/emprendedores-pymes/noticias/6103473/09/14/El-autonomo-trabaja-mejor-porque-esta-en-juego-su-nombre-no-la-empresa.html");
    StringBuilder content = readPageFromResource("eleconomista1.es");
    HTMLDocumentSegmenter seg = new ElEconomistaSegmenter();
    List<SimpleItem> result = seg.segment(content.toString(), url);
    int expectedResults = 1; // Main item
    assertEquals(expectedResults, result.size());
    
    // ///////// Test Main Item //////////////
    SimpleItem main = result.get(0);
    assertEquals(
        "http://www.eleconomista.es/emprendedores-pymes/noticias/6103473/09/14/El-autonomo-trabaja-mejor-porque-esta-en-juego-su-nombre-no-la-empresa.html#Comentarios",
        main.threadURLs.get(0));
  }
  
}
