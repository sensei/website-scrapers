/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Oct 24, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;

import org.htmlcleaner.HtmlCleaner;

import websays.types.clipping.SimpleItem.LanguageTag;

public class ElEconomistaSegmenterTest20150617 extends BaseDocumentSegmenterTest {
  
  @Override
  public void setUp() throws Exception {
    // http://www.eleconomista.es/indicadores-espana/noticias/6181497/10/14/El-paro-baja-en-195200-personas-en-el-tercer-trimestre-la-tasa-de-desempleo-desciende-hasta-el-2367.html
    StringBuilder content = readPageFromResource("eleconomista20150617.es");
    this.seg = new ElEconomistaSegmenter();
    HtmlCleaner cleaner = createCleaner();
    this.rootNode = cleaner.clean(content.toString());
  }
  
  @Override
  public void testFindMainTitle() {
    // System.err.println("testFindMainTitle:" + seg.findMainTitle(rootNode));
    assertEquals("La Caixa cree que el pulso del sector inmobiliario comienza a recuperarse - elEconomista.es", seg.findMainTitle(rootNode));
  }
  
  @Override
  public void testFindMainAuthor() {
    // System.err.println("testFindMainAuthor:" + seg.findMainAuthor(rootNode));
    assertEquals("EFE", seg.findMainAuthor(rootNode));
  }
  
  @Override
  public void testFindMainDescripcion() {
    // System.err.println("testFindMainDescripcion:" + seg.findMainDescripcion(rootNode));
    assertEquals(
        "Barcelona, 11 jun (EFE).- La Caixa Research, el servicio de estudios y análisis económico de La Caixa, cree que \"el pulso del sector inmobiliario empieza a recuperarse\" y que \"el recorrido al alza de la demanda y la oferta (de vivienda) es amplio\", aunque augura que la recuperación será \"gradual\" en ambos frentes. Así lo asegura en un análisis correspondiente al mes de mayo dedicado al sector inmobiliario, en el cual apunta que, pese a que \"el nuevo ciclo empieza con unas constantes vitales saludables\", será \"imprescindible\" ir tomando la temperatura al sector para certificar que no se comenten \"errores del pasado\". La Caixa Research extrae estas conclusiones a partir del termómetro inmobiliario que elabora, y que se nutre de tres subindicadores: uno dedicado a la oferta, otro a la demanda y un tercero a los precios. El servicio de estudios y análisis de La Caixa sostiene, respecto a la demanda de vivienda, que a pesar de anotarse \"un ligero aumento\" en los últimos meses, \"todavía se encuentra por debajo de los niveles considerados de equilibrio\". La nota comenta que, de momento, los extranjeros impulsan este \"avance\" en la compraventa de viviendas, pero que para alcanzar esos niveles de equilibrio \"es imprescindible que la demanda doméstica se sume al crecimiento\". En este punto, La Caixa espera que la demanda doméstica de vivienda \"gane vigor en 2015, gracias a un entorno económico más propicio, con un crecimiento del PIB y del empleo cercano al 3 % y una mejora de las condiciones de financiación\". Respecto a la oferta, La Caixa augura \"un buen 2015\" gracias al aumento de la inversión residencial, de las peticiones de visados para nueva construcción y de la ocupación en el sector\", unas tendencias que \"deberían consolidarse\" este año, a pesar de que el stock de viviendas es todavía \"muy elevado en algunas regiones\", apunta. En cuanto a los precios, el informe sí cree que han alcanzado \"niveles de equilibrio\" tras estabilizarse en el último trimestre de 2014. No obstante, desde el primer trimestre de 2008, el punto álgido de la burbuja inmobiliaria, los precios han caído un 37 % en términos reales, un 30 % en términos nominales, según La Caixa. Asimismo, el servicio de estudios de La Caixa asegura que, de cara a aumentar la demanda de vivienda, será clave la consolidación del mercado laboral, mientras que la recuperación de la oferta \"sigue estando muy condicionada por el stock de viviendas\", considera. ",
        seg.findMainDescripcion(rootNode));
  }
  
  @Override
  public void testFindMainImageLink() {
    // System.err.println("testFindMainImageLink:" + seg.findMainImageLink(rootNode));
    assertEquals("http://s01.s3c.es/imag/efe/2015/06/11/20150611-10867172w.jpg", seg.findMainImageLink(rootNode));
  }
  
  @Override
  public void testFindMainDate() {
    // System.err.println("testFindMainDate:" + seg.findMainDate(rootNode));
    Calendar cal = Calendar.getInstance();
    cal.set(2015, 5, 11, 14, 44, 0);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), seg.findMainDate(rootNode));
  }
  
  @Override
  public void testFindMainAPIObjectID() {
    assertNull(seg.findMainAPIObjectID(rootNode));
  }
  
  @Override
  public void testFindMainThreadURLs() {
    assertEquals(0, seg.findMainThreadURLs(rootNode).size());
  }
  
  @Override
  public void testFindMainTimeIsReal() {
    assertTrue(seg.findMainTimeIsReal(rootNode));
  }
  
  @Override
  public void testFindMainLanguage() {
    assertEquals(LanguageTag.spanish, seg.findMainLanguage(rootNode));
  }
  
}
