/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class ElMundoSegmenterTest extends BaseDocumentSegmenterTest {
  
  @Override
  public void setUp() throws Exception {
    StringBuilder content = readPageFromResource("ETA asume la vía catalana a favor del 'derecho a decidir' | España | EL MUNDO.html",
        "iso-8859-15");
    this.seg = new ElMundoSegmenter();
    HtmlCleaner cleaner = createCleaner();
    this.rootNode = cleaner.clean(content.toString());
  }
  
  @Override
  public void testFindMainTitle() {
    String title = seg.findMainTitle(rootNode);
    assertEquals("ETA asume la vía catalana a favor del 'derecho a decidir'", title);
  }
  
  @Override
  public void testFindMainDescripcion() {
    String body = seg.findMainDescripcion(rootNode);
    assertEquals(
        "ETA tiene clara cuál es su actual estrategia. Y ésta enfila por la situación política de Cataluña. En documentos intervenidos por las Fuerzas de Seguridad del Estado hace apenas 15 días, la organización terrorista es expresa: «Situar el derecho a decidir en el centro de la política es una labor prioritaria. La experiencia y la fuerza de los pasos realizados por Escocia y Cataluña son una referencia importante».Así de tajante se muestra ETA en la documentación difundida entre sus presos. Incluso, propone imitar las últimas movilizaciones ciudadanas en Cataluña, como la cadena humana independentista de la pasada Diada.«Para activar a la mayoría social en esta línea política se ha puesto en marcha la dinámica popular Gure Esku [en nuestras manos], que ya ha empezado a preparar la enorme cadena humana del próximo 8 de junio», indica la documentación etarra.En estos documentos, los terroristas también analizan la consecuencia de la sentencia de Estrasburgo que ha tumbado la doctrina Parot. «La excarcelación de los 'presos políticos vascos' que estaban presos bajo la doctrina, le tiene que suponer un empuje positivo al proceso. Las excarcelaciones de los presos tienen que ser un acontecimiento positivo y constructor mirando a la solución. Para eso, tanto en práctica como en discurso, hay que hacer frente a la batalla que ha impuesto el enemigo. Tiene que ser el principio del cambio de la política penitenciaria, porque se tiene que tener en cuenta que lo que se ha cancelado es el carácter retroactivo de la doctrina», indican los escritos de los etarras.Otra de sus prioridades es el acercamiento de los presos a EuskadiHacen suyos los festejos y los homenajes a los presos que están abandonando las cárceles en aplicación de la sentencia de Estrasburgo. Enmarcan los «ongi etorris» (actos de bienvenida) dentro de su estrategia. Estos homenajes a los presos están actualmente bajo la lupa tanto de las Fuerzas de Seguridad del Estado como de la Fiscalía. «El enemigo ha acuñado una presión increíble sobre las excarcelaciones de los presos, su primera recepción y los ongis etorris, vencedor/perdedor, (...) Quieren imponer el interesado confrontamiento político de vencedor/perdedor. Los que piensan que excarcelar a un preso es una derrota política no pueden permitir las imágenes que van a vapulear el discurso oficial (después de pasar largo tiempo en la cárcel, sin arrepentirse, ongis etorris populares a los compañeros que van a salir firmes y dignos)».Asume como propia la nueva organización juvenil Aintzina. Además, muestra el daño que les provocó la última actuación de la Guardia civil contra Herrira, la organización de apoyo a los presos: «La operación ha venido a recordar que el ciclo de ilegalización está abierto. (...) Ha sido un duro ataque».La prioridad del colectivo de presos es, como publicó ayer este periódico, conseguir en primer lugar el acercamiento de los encarcelados a los centros penitenciarios del País Vasco. «En otro plano estaría la vía de la vuelta a casa, la vía de la excarcelación», reconocen. Frente al Foro Social auspiciado por el PNV y Sortu, reivindican que son ellos quienes han de «tomar la iniciativa». Dicen que «reniegan de los modos que se han utilizado en el pasado para hacer frente a la imposición», es decir, reniegan de los atentados futuros. Y dicen que, «cuando se dé el caso», tendrán «la preparación para analizar la responsabilidad en las actividades políticas que han causado daño». Ni un paso más, por el momento. Esa fórmula tendrá que anunciarla ETA y después cada preso la asumirá en sus solicitudes particulares de acercamiento o salida ante la Administración. Para que los presos que consideran enfermos salgan (en principio en Francia) aceptan como novedad «el pago de daños», que tendrá que ser explicado «al pueblo». Hasta ahora, se negaban a pagar indemnizaciones a las víctimas.",
        body);
  }
  
  @Override
  public void testFindMainAPIObjectID() {
    assertEquals("279395918757488", seg.findMainAPIObjectID(rootNode));
  }
  
  @Override
  public void testFindMainImageLink() {
    assertEquals("http://estaticos.elmundo.es/assets/multimedia/imagenes/2013/12/03/13861057263914.jpg", seg.findMainImageLink(rootNode));
  }
  
  @Override
  public void testFindMainDate() {
    assertEqualTimes(DatatypeConverter.parseDateTime("2013-12-04T02:44:08+01:00").getTime(), seg.findMainDate(rootNode));
  }
  
  @Override
  public void testFindMainAuthor() {
    assertEquals("FERNANDO LÁZARO", seg.findMainAuthor(rootNode));
  }
  
  @Override
  public void testFindMainAuthorLocation() {
    assertEquals("Madrid", seg.findMainAuthorLocation(rootNode));
  }
  
  @Override
  public void testFindMainComments() {
    assertEquals(new Integer(104), seg.findMainComments(rootNode));
  }
  
  @Override
  public void testFindComments() {
    assertEquals(10, seg.findComments(rootNode).length);
  }
  
  @Override
  public void testFindCommentDate() {
    TagNode[] comments = seg.findComments(rootNode);
    assertEqualDates(DatatypeConverter.parseDateTime("2013-12-05T06:59:00+01:00").getTime(), seg.findCommentDate(comments[0]));
    assertEqualDates(DatatypeConverter.parseDateTime("2013-12-05T02:33:00+01:00").getTime(), seg.findCommentDate(comments[1]));
    assertEqualDates(DatatypeConverter.parseDateTime("2013-12-04T20:47:00+01:00").getTime(), seg.findCommentDate(comments[5]));
    assertEqualDates(DatatypeConverter.parseDateTime("2013-12-04T16:41:00+01:00").getTime(), seg.findCommentDate(comments[9]));
  }
  
  @Override
  public void testFindCommentAuthor() {
    TagNode[] comments = seg.findComments(rootNode);
    assertEquals("Angel Sanchez Fernandezangel89", seg.findCommentAuthor(comments[0]));
    assertEquals("Misitauro", seg.findCommentAuthor(comments[1]));
    assertEquals("heriotza-zigor", seg.findCommentAuthor(comments[2]));
    assertEquals("Iker Gauteguiz Arteagakatxitxone", seg.findCommentAuthor(comments[3]));
    assertEquals("Porcia", seg.findCommentAuthor(comments[9]));
  }
  
  @Override
  public void testFindCommentDescripcion() {
    TagNode[] comments = seg.findComments(rootNode);
    assertEquals(
        "La Constitución prohibe la segregación del Estado español. Las leyes penales establecen que ir vulnerar la ley suprema puede comportar un delito y por tanto la prisión de los que la promueven. La ley es igual para todos.",
        seg.findCommentDescripcion(comments[0]));
    assertEquals(
        "Y sabiendo la que se está preparando ¿va Vd. a hacer algo, Don Mariano? o va a dejar que nos sigamos recociendo en nuestra propia salsa. Luego se quejarán si surgen partidos de ultraderecha...pero es que se lo están poniendo a huevo. El Gobierno no está solo para resolver los problemas económicos, dejen ya de ponerse medallas y léanse su programa electoral. Cómo nos la ha jugado Don Mariano, tan formalito que parecía",
        seg.findCommentDescripcion(comments[1]));
    assertEquals(
        "Porque no tomaron esta vía mil muertos antes. Porque han dejado este rastro de viudas y huérfanos. ¿Poque? Dios mio cuanto sufrimiento inútil para alimentar la avaricia y el egoísmo de 7 nos pocos que justifican la muerte de inocentes. Que Dios los perdone.",
        seg.findCommentDescripcion(comments[6]));
    
  }
  
  @Override
  public void testFindMainTimeIsReal() {
    assertTrue("Main page time should be real, if not a parsing problem could be arising. Check parsing process.",
        seg.findMainTimeIsReal(rootNode));
  }
  
  @Test
  public void testPage() throws Exception {
    URL url = new URL("http://www.elmundo.es/debate/economia/2014/02/17/53020cf9ca4741f6128b4576.html");
    StringBuilder content = readPageFromResource("elmundo.es.v2");
    HTMLDocumentSegmenter seg = new ElMundoSegmenter();
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
  }
}
