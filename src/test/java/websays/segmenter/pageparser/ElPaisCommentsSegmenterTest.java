/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.List;

import org.junit.Test;

import websays.types.clipping.SimpleItem;

public class ElPaisCommentsSegmenterTest extends BaseDocumentSegmenterTest {
  
  @Test
  public void testCommentsElPais() throws Exception {
    StringBuilder content = super.readPageFromResource("comentarios_elpais.com.json", "UTF-8");
    ElPaisCommentsSegmenter seg = new ElPaisCommentsSegmenter();
    
    List<SimpleItem> comments = seg.segment(content.toString(), new URL(
        "http://elpais.com/OuteskupSimple?th=1&msg=1400786331-b2a604d446dfaa18f3ada0ea8ff55850"));
    assertTrue(comments.size() > 0);
    
    for (SimpleItem item : comments) {
      assertNotNull(item.author);
      assertNotNull(item.body);
      assertNotNull(item.date);
      assertEquals("http://elpais.com/economia/2014/05/22/actualidad/1400786331_637886.html", item.postUrl);
      assertTrue(item.isComment);
    }
  }
  
}
