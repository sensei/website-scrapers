/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.htmlcleaner.HtmlCleaner;

public class ElPaisSegmenterTest extends BaseDocumentSegmenterTest {
  
  @Override
  public void setUp() throws Exception {
    StringBuilder content = readPageFromResource("elpais.com.html", "UTF-8");
    this.seg = new ElPaisSegmenter();
    HtmlCleaner cleaner = createCleaner();
    this.rootNode = cleaner.clean(content.toString());
  }
  
  @Override
  public void testFindMainTitle() {
    String title = seg.findMainTitle(rootNode);
    assertEquals("La Caixa deja de ser una caja", title);
  }
  
  @Override
  public void testFindMainAuthor() {
    assertEquals("Ediciones El País", seg.findMainAuthor(rootNode));
  }
  
  @Override
  public void testFindMainDate() {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    Date findMainDate = seg.findMainDate(rootNode);
    String dateStr = sdf.format(findMainDate);
    assertEquals("2014-05-23", dateStr);
  }
  
  @Override
  public void testFindMainDescripcion() {
    String body = seg.findMainDescripcion(rootNode);
    assertEquals(
        "La Caixa deja de ser una caja de ahorros para convertirse en una fundación bancaria. Se trata de su tercera reestructuración desde 2007, pero la más relevante en sus 110 años de historia. Y así lo aprobó este jueves la asamblea general de la entidad financiera, que afronta los cambios forzada por una norma que impuso Bruselas, tras el rescate financiero, para reforzar la solvencia de los bancos procedentes de cajas y que exigió la modificación de la Ley de Cajas y Fundaciones Bancarias. El presidente de la entidad, Isidro Fainé, resaltó, no obstante, que “el modelo de La Caixa sigue siendo más vigente que nunca” y subrayó: “Nuestros valores no cambian. Nuestro modelo, tampoco”. Este cambio jurídico no tiene ningún efecto en los clientes de la entidad.La nueva fundación de La Caixa será la tercera mayor de todo el mundoTras sacar a Bolsa en 2007 su holding industrial a través de Criteria CaixaHolding y hacer lo propio con su negocio bancario integrado en CaixaBank en 2011, La Caixa se convierte ahora en la primera caja española que asume su reconversión. Como resultado, tras integrar todas sus magnitudes económicas, será la tercera mayor fundación mundial (tras la Fundación Bill y Melinda Gates y la británica Wellcome Trust), con una dotación fundacional de 5.868 millones (equivalente al 0,6% del PIB español), un valor de mercado de 19.837 millones y un presupuesto anual de 500 millones.Fainé apuntó durante la asamblea que la nueva estructura, que se completará a finales de año, “permitirá reforzar” los tres pilares sobre los que se asienta la entidad: la actividad financiera, la cartera industrial y la obra social, aunque enfatizó que “el banco es y continuará siendo el corazón del grupo”.Previsiblemente, CaixaBank, que aúna también el negocio asegurador, la presencia en bancos extranjeros y las participaciones en Repsol y Telefónica, ganará independencia en el nuevo organigrama, si bien Fainé intentó espantar cualquier atisbo de ruptura entre las tres áreas del conglomerado: “La Ley nos fuerza a transformarnos en fundación bancaria, pero nuestra unión con CaixaBank es sólida y duradera, y nuestros destinos deben mantenerse ineludiblemente unidos”. La norma impulsada por el Ministerio de Economía plantea requisitos más duros para aquellas fundaciones que controlen más del 50% de un banco, obligándolas a presentar un plan de diversificación de inversiones y un fondo de reserva, lo que podría conllevar diluciones de participación. La fundación de La Caixa controlará de salida el 55,9% de CaixaBank.pulsa en la fotoUna vez concluya la reestructuración, la obra social quedará integrada dentro de la nueva Fundación Bancaria La Caixa. Y bajo ésta quedará Criteria (controlará el 100% del capital), de la que colgarán las otras dos actividades. Por un lado, CaixaBank; por el otro, la cartera industrial e inmobiliaria (en la que figuran las participaciones en Gas Natural y Abertis, entre otras), además de las emisiones de deuda que correspondan a La Caixa. Criteria tendrá un valor patrimonial neto de unos 20.000 millones de euros y unas plusvalías acumuladas de 3.000 millones de euros.Más allá de lo aprobado este jueves, hay otra fecha señalada en el calendario que marcará el futuro de la nueva Caixa. No es otra que junio de 2016. Hasta entonces Isidro Fainé podrá compaginar las presidencias de la fundación y de CaixaBank. Después, obligado por la ley, tendrá que elegir entre un cargo u otro. Y su sucesor —o sucesores— podrán modificar las líneas estratégicas que ha tomado la entidad bajo su mando: mantenimiento de la mayor cartera industrial europea, expansión internacional del negocio bancario y mantenimiento de la actividad social.El cambio es una obligación de la UE al sector para reforzar la solvenciaEl presidente de La Caixa ha diseñado un patronato formado por notables para dirigir los designios del grupo, entre los que se encuentran el multimillonario mexicano Carlos Slim y el presidente de Telefónica, César Alierta. Se sumarán a ese grupo Salvador Alemany (Abertis), Javier Godó (Grupo Godó) y los expolíticos Javier Solana y Francesc Homs (exconsejero de Economía con Jordi Pujol), además de otros expertos financieros y en organizaciones humanitarias.Fainé dijo en la que fue la última asamblea general de La Caixa que se mantiene “la voluntad de avanzarnos a los acontecimientos, y a la capacidad de adaptarnos constantemente a un mundo que no está quieto” y destacó el papel jugado en la reestructuración del sistema financiero español, que le ha servido para integrar Caixa Girona, Banca Cívica y Banco de Valencia, además de estudiar la posibilidad de pujar por Catalunya Banc.El director general de La Caixa —y vicepresidente consejero delegado de CaixaBank—, Juan María Nin, calificó esas operaciones como “apoyo al proceso de reestructuración en que se encuentra todo el sistema financiero español”. “Estas adquisiciones también nos han permitido avanzar hacia una estructura de ingresos más potente y conectada al negocio de banca de particulares”, señaló.",
        body);
  }
  
  @Override
  public void testFindMainAPIObjectID() {
    assertNull(seg.findMainAPIObjectID(rootNode));
  }
  
  @Override
  public void testFindMainImageLink() {
    assertEquals("http://ep00.epimg.net/economia/imagenes/2014/05/22/actualidad/1400786331_637886_1400786500_noticia_normal.jpg",
        seg.findMainImageLink(rootNode));
  }
  
  @Override
  public void testFindMainAuthorLocation() {
    assertNull(seg.findMainAuthorLocation(rootNode));
  }
  
  @Override
  public void testFindMainComments() {
    assertNull(seg.findMainComments(rootNode));
  }
  
  @Override
  public void testFindComments() {
    assertNull(seg.findComments(rootNode));
  }
  
  @Override
  public void testFindMainThreadURLs() {
    assertEquals("http://elpais.com/OuteskupSimple?th=1&msg=1400786331-b2a604d446dfaa18f3ada0ea8ff55850", seg.findMainThreadURLs(rootNode)
        .get(0));
  }
  
}
