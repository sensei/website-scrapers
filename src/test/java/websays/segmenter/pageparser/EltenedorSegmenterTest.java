/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors: Jorge Valderrama
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.Calendar;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class EltenedorSegmenterTest extends SegmenterTest {
  
  @Test
  public void testLahSegment() throws Exception {
    
    URL url = new URL("http://www.eltenedor.es/restaurante/lah/22950");
    StringBuilder content = readPageFromResource("Restaurante Lah en Madrid - eltenedor.es.html");
    HTMLDocumentSegmenter seg = new EltenedorSegmenter();
    
    Calendar cal = Calendar.getInstance();
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 21; // No of comments+ basic details item which
    // you expect in the page
    assertEquals(expectedResults, result.size());
    
    // ///////// Test Main item //////////////
    SimpleItem main = result.get(0);// The main details item at
    // index 0 (first element)
    assertEquals("Lah", main.title);
    assertEquals(
        "Lo más exótico del lejano Oriente, se convierte en una exquisita realidad cuando se habla del restaurante Lah! Y es que su cocina reúne todas las delicias de Asia, para llevarlas a la mesa de los comensales del madrileño distrito de Salamanca.Uno de los ejemplos más logrados de su carta, lo tienes en los triángulos de hojaldre rellenos de patata al curry, guisantes y menta, servidos junto a una salsa de yogur que deleita y refresca a tu paladar. Uno de sus platos fuertes, y que sin duda no te puedes perder, es la ternera guisada con leche de coco, mezcla de especias y nueces. Y para los amantes del picante, puedes añadir pasta de guindilla molida.Mención aparte, la calidad en el servicio y la puesta en escena, digna de una sesión fotográfica. ¡Reserva tu mesa!",
        main.body);
    assertEquals(null, main.author);
    assertEquals(null, main.authorLocation);
    assertEquals(new Integer(286), main.comments);
    assertEquals("Native score", new Integer(80), main.getScore_native());
    assertEquals("Websays score", new Integer(80), main.getScore_websays());
    assertNull(main.date);
    assertNull(main.itemUrl);
    assertEquals(1, main.position);
    assertEquals(url.toString(), main.postUrl);
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = result.get(1);
    assertEquals(null, sub1.title);
    assertEquals("Muy bueno, pero mejor mediodía de lunes a viernes que tienen menú", sub1.body);
    assertEquals("Gil A.", sub1.author);
    assertEquals("Native score ", new Integer(100), sub1.getScore_native());
    assertEquals("Websays score ", new Integer(100), sub1.getScore_websays());
    cal.set(2013, 9, 27); // 27/10/2013
    assertEqualDates(cal.getTime(), sub1.date);
    assertEquals(url.toString(), sub1.postUrl);
    assertEquals(2, sub1.position);
    
    // ///////// Test 5 comment item //////////////
    SimpleItem sub5 = result.get(5);
    assertEquals(null, sub5.title);
    assertEquals(
        "Era la tercera vez que iba. Siempre me habia encantado. Ayer note que habian bajado en la calidad y servicio.Los platos salieron tarde y frios.Los camareros muy amables pero estaban desbordados 2 para todo el restaurante.Una pena pues era uno de mis restaurantes a los q ir en cualquier ocasion.",
        sub5.body);
    assertEquals("Paloma M.", sub5.author);
    assertEquals("Native score", new Integer(65), sub5.getScore_native()); // 6.5 round up
    assertEquals("Websays score", new Integer(65), sub5.getScore_websays()); // 6.5 round up
    cal.set(2013, 8, 18); // 18/09/2013
    assertEqualDates(cal.getTime(), sub5.date);
    assertEquals(url.toString(), sub5.postUrl);
    assertEquals(6, sub5.position);
  }
  
  @Test
  public void test2015Segment() throws Exception {
    
    URL url = new URL("http://www.eltenedor.es/restaurante/st-james-gastro-james-rosario-pino/11814");
    StringBuilder content = readPageFromResource("elTenedor2015.es");
    HTMLDocumentSegmenter seg = new EltenedorSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 21; // No of comments+ basic details item which
    // you expect in the page
    assertEquals(expectedResults, result.size());
    
    // ///////// Test Main item //////////////
    SimpleItem main = result.get(0);// The main details item at
    // index 0 (first element)
    assertEquals("St. James - Gastro James Rosario Pino", main.title);
    assertEquals(
        "Sigue la tradición de excelencia que persigue la casa St. James. El restaurante St. James Rosario Pino es un local amplio en el que ante todo prima el buen gusto y el saber hacer. Con varios salones privados a disposición de los clientes y una espaciosa terraza para un rato de esparcimiento tranquilo, su personal los recibirá con una atención esmerada.La carta, como todas las demás de la firma, se especializa en arroces, con más de 30 variedades para probar cada día. Además, dispone de otros platos como fardos de calamar confitados con cebolla en su tinta al aceite de albahaca, crujiente de dorada sobre juliana de verduras frescas o Milhoja de rabo de toro. Los postres están elaborados artesanalmente y son toda una delicia.La decoración del lugar es distinguida: paredes de tonos claros contrastando con otras más oscuras, lámparas de araña y murales pintados que le dan un aire elegante a la par que urbano. Un sitio que no te puedes perder.",
        main.body);
    assertEquals(null, main.author);
    assertEquals(null, main.authorLocation);
    assertEquals(new Integer(6035), main.comments);
    assertEquals("Native score", new Integer(91), main.getScore_native());
    assertEquals("Websays score", new Integer(91), main.getScore_websays());
    assertNull(main.date);
    assertNull(main.itemUrl);
    assertEquals(1, main.position);
    assertEquals(url.toString(), main.postUrl);
    
    //
    // ///////// Test first review item //////////////
    SimpleItem review = result.get(1);// The first review item at
    // index 1 (first review element)
    assertNull(review.title);
    assertEquals("\"Con el tenedor bien , sino caro\"", review.body);
    assertEquals("Jaime M.", review.author);
    assertEquals(null, review.authorLocation);
    assertEquals("Native score", new Integer(75), review.getScore_native());
    assertEquals("Websays score", new Integer(75), review.getScore_websays());
    assertEqualDates(DatatypeConverter.parseDateTime("2015-01-28").getTime(), review.date);
    assertNull(review.itemUrl);
    assertEquals(2, review.position);
    assertEquals(url.toString(), review.postUrl);
    assertTrue(review.isComment);
    assertEquals(
        "http://www.eltenedor.es/restaurante/st-james-gastro-james-rosario-pino/11814/contentopiniones/2?uidRestaurant=11814&filters%5Bwith_comments_only%5D=1&sort=RESERVATION_DATE_DESC&page=2",
        review.threadURLs.get(0));
    
    url = new URL(
        "http://www.eltenedor.es/restaurante/st-james-gastro-james-rosario-pino/9134/contentopiniones/2?uidRestaurant=11814&filters%5Bwith_comments_only%5D=1&sort=RESERVATION_DATE_DESC&page=2");
    content = readPageFromResource("elTenedor2015Comments.json");
    
    result = seg.segment(content.toString(), url);
    
    expectedResults = 20; // No of comments second page
    // you expect in the page
    assertEquals(expectedResults, result.size());
    
    // ///////// Test first item second page //////////////
    SimpleItem secondPageFisrtReview = result.get(0);// The first review item at
    // index 0 second page (first element)
    assertNull(secondPageFisrtReview.title);
    assertEquals(
        "\"Comida bastante bien,  con un servicio atento y profesional.  Gracias a \"El Tenedor\" pudimos comer a un precio aceptable.\"",
        secondPageFisrtReview.body);
    assertEquals("Antonio F.", secondPageFisrtReview.author);
    assertEquals(null, secondPageFisrtReview.authorLocation);
    assertEquals("Native score", new Integer(85), secondPageFisrtReview.getScore_native());
    assertEquals("Websays score", new Integer(85), secondPageFisrtReview.getScore_websays());
    assertEqualDates(DatatypeConverter.parseDateTime("2015-01-22").getTime(), secondPageFisrtReview.date);
    assertNull(secondPageFisrtReview.itemUrl);
    assertEquals(2, secondPageFisrtReview.position);
    assertEquals(url.toString(), secondPageFisrtReview.postUrl);
    assertTrue(secondPageFisrtReview.isComment);
    assertNull(secondPageFisrtReview.threadURLs);
  }
  
}
