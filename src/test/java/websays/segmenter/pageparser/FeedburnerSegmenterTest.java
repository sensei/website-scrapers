/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class FeedburnerSegmenterTest extends SegmenterTest {
  
  @Test
  public void testCommentSegment() throws Exception {
    
    URL url = new URL("http://feeds.feedburner.com/TechCrunch/startups?format=xml");
    StringBuilder content = readPageFromResource("TechCrunch » Startups.xhtml");
    HTMLDocumentSegmenter seg = new FeedburnerSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 20; // No of comments
    Calendar cal = Calendar.getInstance();
    cal.setTimeZone(TimeZone.getTimeZone("GMT"));
    assertEquals(expectedResults, result.size());
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = result.get(0);
    assertEquals("SensioLabs Raises $6.9 Million To Implement Its PHP Framework ‘Symfony’ Everywhere", sub1.title);
    assertEquals(
        "French company SensioLabs raised $6.9 million (€5 million) from CM-CIC Capital Privé to expand its operations around the open-source PHP framework Symfony. While the startup is only a year old, the team behind it actually created the framework in 2005 — that’s why they know it better than anyone else and can provide all kinds of services and products to help enterprise clients. “SensioLabs is a spin-off company from a web agency that we created back in 1998,” co-founder and CEO Fabien Potencier told me. “We provide a few services around Symfony (code reviewing, training, consulting…). We also run a couple of products to make better use of PHP (quality control, performance optimization…).” With all its flaws, PHP is still by far the dominant programming language of the web. WordPress wouldn’t exist without PHP, and most websites are now custom-designed WordPress installations. That’s why many engineers still favor PHP over Ruby on Rails or Python. But a good framework could greatly help these companies. They will get a more robust structure — the engineering team will become faster as well. That’s where Symfony and SensioLabs come along if a company doesn’t plan to develop and support its own framework. There are other popular PHP frameworks, such as the Zend Framework, but Symfony is the most popular. For example, Yahoo! was one of the first Symfony user, adopting it shortly after the initial release in 2005. In 2011, the Symfony community released Symfony2, generating a high influx of new users. Now, Lagardère, TOEFL, Parisian subway company RATP, Virgin Mobile and French ministries all work with SensioLabs to improve their usage of Symfony. The startup reproduces the traditional consulting business model of successful open-source/for-profit hybrid companies, such as Red Hat or even IBM. Symfony seems to have a healthy community with meetups and conferences happening regularly. One of SensioLabs’ key missions is to make sure that the community remains very active — it can influence its overall direction as well by contributing to the development of the framework. But recently, the company also unveiled a new product called SensioLabs Insights. Still in private beta, this product should help when it comes to testing suites. Other companies have specialized around this area, such as Disrupt alumni Koality. We will see whether SensioLabs’ expertise in all things PHP could be a competitive advantage. Today’s funding round is the first one for SensioLabs. With 80",
        sub1.body);
    assertEquals("Romain Dillet", sub1.author);
    cal.set(2013, 11, 18, 15, 14, 07);// Wed, 18 Dec 2013 15:14:07 +0000
    assertEqualTimes(cal.getTime(), sub1.date);
    assertEquals(url.toString(), sub1.postUrl);
    
    assertFalse(sub1.isComment);
    assertEquals("http://techcrunch.com/2013/12/18/sensiolabs-raises-6-9-million-to-implement-its-php-framework-symfony-everywhere/",
        sub1.itemUrl);
    assertEquals("http://tctechcrunch2011.files.wordpress.com/2013/12/3191872515_e4f7743f5a_o.jpg", sub1.imageLink);
    assertTrue(sub1.comments == 0);
    
    assertEquals(1, sub1.position);
    
    SimpleItem sub2 = result.get(1);
    assertEquals("Couple App Now Lets You Stalk Your Partner On Mobile And Desktop", sub2.title);
    assertEquals(
        "Fans of monogamy should get excited. Couple, the social network for two, has just launched a relatively major update that integrates location data in a totally new way, making sure you can always keep tabs on your partner. And if tracking your sweetie via mobile just isn't enough control, Couple is also launching a web app for the very first time, enabling users to check out their moments, lists, and chat on the desktop.",
        sub2.body);
    assertEquals("Jordan Crook", sub2.author);
    cal.set(2013, 11, 18, 14, 00, 48);// Wed, 18 Dec 2013 14:00:48 +0000
    assertEqualTimes(cal.getTime(), sub2.date);
    assertEquals(url.toString(), sub2.postUrl);
    
    assertFalse(sub2.isComment);
    assertEquals("http://tctechcrunch2011.files.wordpress.com/2013/12/couple.jpg", sub2.imageLink);
    assertEquals(
        "http://techcrunch.com/2013/12/18/couple-now-lets-you-track-down-your-partners-location-recommend-a-date-and-chat-on-the-web/",
        sub2.itemUrl);
    assertTrue(sub2.comments == 0);
    
    assertEquals(1, sub2.position);
    
    SimpleItem sub20 = result.get(19);
    assertEquals("Aereo Founder Explains Delayed Expansion Plans", sub20.title);
    assertEquals(
        "Streaming TV startup Aereo has had a big week. The company agreed to take an arduous legal battle with major network broadcasters to the Supreme Court.To discuss the move and the growth of the company, we sat down with Chet Kanojia, CEO and founder.",
        sub20.body);
    assertEquals("Jordan Crook", sub20.author);
    cal.set(2013, 11, 15, 20, 0, 44);// Sun, 15 Dec 2013 20:00:44 +0000
    assertEqualTimes(cal.getTime(), sub20.date);
    assertEquals(url.toString(), sub20.postUrl);
    assertEquals("http://pthumbnails.5min.com/10361006/518050251_6_1279_727.jpg", sub20.imageLink);
    assertEquals("http://techcrunch.com/2013/12/15/aereo-founder-explains-delayed-expansion-plans/", sub20.itemUrl);
    assertFalse(sub20.isComment);
    assertEquals(1, sub20.position);
    assertTrue(sub20.comments == 1);
    
  }
  
  @Test
  public void testSegment2() throws Exception {
    
    URL url = new URL("http://feeds.feedburner.com/ElBlogDelaCaixa?fmt=xml");
    StringBuilder content = readPageFromResource("blogcaixa_feedburner.xml");
    HTMLDocumentSegmenter seg = new FeedburnerSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 10; // No of comments
    Calendar cal = Calendar.getInstance();
    cal.setTimeZone(TimeZone.getTimeZone("GMT")); // It is equivalent to Zulu time which is what blog dates carry (look at the capital Z at the end of
                                                  // the date.)
    assertEquals(expectedResults, result.size());
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = result.get(0);
    assertEquals("Nace CaixaLab Experience para experimentar el valor del emprendimiento", sub1.title);
    assertEquals(
        "La Obra Social ”la Caixa” presenta CaixaLab Experience, un espacio innovador que abre sus puertas en CaixaForum Barcelona para ponerse al servicio de la educación y de la formación de los jóvenes en el ámbito del emprendimiento. Se trata de un laboratorio con un taller interactivo y multimedia, donde los alumnos visitantes aprenderán el concepto de emprendimiento y las características y los valores de una actitud emprendedora, experimentando en directo el proceso a través de distintas actividades. CaixaLab Experience se enmarca dentro del proyecto pedagógico Jóvenes Emprenededores que impulsa eduCaixa. ¿A quién se dirige? A los jóvenes de 14 a 18 años, que están en una edad de formación clave para descubrir las aspiraciones profesionales que tienen. Al público familiar que visita CaixaForum durante los fines de semana, que también podrá disfrutar y participar en las actividades que se proponen. El espacio Se estructura en distintos ámbitos, y cada uno está dedicado a un concepto clave del proceso de emprender. Se trata de ámbitos para llevar a cabo actividades. Cada ámbito tiene recursos interactivos, variados y diferenciados de los otros. Se vertebra en un eje de contenidos que lo organiza en 3 áreas: Mira:  observación crítica. Piensa:  valoración y reflexión. Haz:  trabajo y desarrollo. Ya puedes realizar una reserva al CaixaLab Experience. Más información de la presentación de CaixaLab Experience en la nota de prensa. The post Nace CaixaLab Experience para experimentar el valor del emprendimiento appeared first on El Blog de ”la Caixa”.",
        sub1.body);
    assertEquals("”la Caixa”", sub1.author);
    cal.set(2014, 0, 15, 14, 9, 45);// 2014-01-15T14:09:45Z
    
    assertEqualTimes(cal.getTime(), sub1.date);
    assertEquals(url.toExternalForm(), sub1.postUrl);
    
    assertFalse(sub1.isComment);
    assertEquals("http://www.blog.lacaixa.es/2014/01/nace-caixalab-experience-para-experimentar-el-valor-del-emprendimiento.html",
        sub1.itemUrl);
    assertNull(sub1.imageLink);
    assertTrue(sub1.comments == 0);
    assertEquals(1, sub1.position);
    
    // // SECOND ITEM
    SimpleItem sub2 = result.get(1);
    assertEquals("Perspectivas económicas para 2014", sub2.title);
    assertEquals(
        "El balance de los datos económicos más reciente apunta que 2014 será el año de la vuelta al crecimiento, según afirma el presidente del Grupo ”la Caixa”, Isidro Fainé. La recuperación empezó en el tercer trimestre de 2013, con un ligero avance del PIB del 0,1% intertrimestral. En 2014, se prevé que ninguno de los principales países estará en recesión. Sin embargo, los riesgos persistirán e irán ligados a la gestión de la política económica y, especialmente, a la retirada de los estímulos monetarios introducidos durante los últimos años. Los países emergentes más importantes siguen manteniendo el vigor exhibido durante los últimos años, y los países avanzados han conseguido consolidar una gradual, pero firme, recuperación económica. En la eurozona, Alemania continuará liderando la salida de la recesión, con el apoyo de los países periféricos, cuya actividad muestra signos de mejora. En España, el impulso que hasta ahora procedía, básicamente, del sector exterior se empieza también a percibir en algunos componentes de la demanda interna. Si el cambio de ciclo se consolida, a mediados del año ya se podría registrar una tasa de crecimiento interanual positiva de la ocupación. La continuidad de unas condiciones monetarias laxas apoyará la estabilización de los tipos interbancarios, lo que debería facilitar el acceso al crédito. En definitiva, hemos dejado atrás las mayores dificultades de la recesión y las tensiones financieras. Puedes leer más información sobre perspectivas 2014 en el Informe Mensual de diciembre de ”la Caixa” Research. Ya puedes leer el nuevo Dossier del Informe Mensual de enero 2014 The post Perspectivas económicas para 2014 appeared first on El Blog de ”la Caixa”.",
        sub2.body);
    assertEquals("”la Caixa”", sub2.author);
    cal.set(2014, 0, 10, 11, 11, 45);// 2014-01-10T11:11:45Z
    assertEqualTimes(cal.getTime(), sub2.date);
    assertEquals(url.toString(), sub2.postUrl);
    
    assertFalse(sub2.isComment);
    assertEquals("http://www.blog.lacaixa.es/wp-content/uploads/2014/01/PIB_cast.png", sub2.imageLink);
    assertEquals("http://www.blog.lacaixa.es/2014/01/perspectivas-economicas-para-2014.html", sub2.itemUrl);
    assertTrue(sub2.comments == 1);
    assertEquals(1, sub2.position);
    
  }
}
