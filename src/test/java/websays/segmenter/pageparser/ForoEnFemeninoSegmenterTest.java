/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
/**
 * 
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;

import java.net.URL;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.segmenter.pageparser.forums.ForoEnFemeninoSegmenter;
import websays.types.clipping.SimpleItem;

public class ForoEnFemeninoSegmenterTest extends SegmenterTest {
  
  /**
   * Test of segment method, of class ForoEnFemeninoSegmenter
   */
  @Test
  public void testMainForoEnFemeninoSegmenter() throws Exception {
    URL url = new URL("http://foro.enfemenino.com/forum/matern4/__f115054_matern4-inseminacion-artificial-lo-conseguiremos-parte-2.html");
    
    StringBuilder content = readPageFromResource("foro.enfemenino.com");
    
    Calendar cal = Calendar.getInstance();
    HTMLDocumentSegmenter seg = new ForoEnFemeninoSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 21;
    assertEquals(expectedResults, result.size());
    
    // ///////// Test first comment item //////////////
    
    SimpleItem main = result.get(0);
    assertEquals("1140021", main.threadId);
    assertEquals("Inseminacion artificial - lo conseguiremos! (parte 2)", main.title);
    assertEquals("flakita2013", main.author);
    assertEquals(
        " Hola chicas! como ya habia dicho, he creado esta nueva charla, pues parece nos han bloqueado la anterior por razones desconocidas... a las chicas nuevas decirles que bienvenidas, este hilo es magico se los recomiendo... y que no PISEMOS LA LISTA DE ABAJO... LEE BIEN, es facil dales siempre responder NO HA ESTE MENSAJE, SI NO, A LA LISTA QUE APARECE ABAJO.... un beso chicasss en nada estamos juntas de nuevo...",
        main.body);
    cal.set(2013, 9, 13);// Year, Month(zero based), Date
    // assertEqualDates(cal.getTime(), main.date);
    assertEquals(null, main.itemUrl);
    assertEquals(url.toString(), main.postUrl);
    assertEquals(1, main.position);
    assertFalse(main.fakeItem);
    assertEquals("http://foro.enfemenino.com/forum/matern4/__f115054_p2_matern4-inseminacion-artificial-lo-conseguiremos-parte-2.html",
        main.threadURLs.get(0));
    
    // /////// Test Second comment item //////////////
    
    SimpleItem second = result.get(1);
    assertEquals("1183363_mb", second.threadId);
    assertEquals("Fecundacion in vitro", second.title);
    assertEquals("chicasunidas", second.author);
    assertEquals(
        "ola,soy nueva en esta pagina!! y me estoy haciendo  fecundacion in vitro¿hay mas mujeres x aqui como yo? muchas gracias :/",
        second.body);
    assertEquals(null, second.itemUrl);
    cal.set(2014, 4, 23);// Year, Month(zero based), Date
    // assertEqualDates(cal.getTime(), second.date);
    assertEquals(url.toString(), second.postUrl);
    assertEquals(2, second.position);
    assertFalse(second.fakeItem);
    assertNull(second.threadURLs);
  }
  
  /**
   * Test of segment method, of class ForoEnFemeninoSegmenter
   */
  @Test
  public void testLastPageForoEnFemeninoSegmenter() throws Exception {
    URL url = new URL(
        "http://foro.enfemenino.com/forum/matern4/__f115054_p207_matern4-inseminacion-artificial-lo-conseguiremos-parte-2.html");
    
    StringBuilder content = readPageFromResource("foro.enfemenino.lastpage.com");
    
    Calendar cal = Calendar.getInstance();
    HTMLDocumentSegmenter seg = new ForoEnFemeninoSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 20;
    assertEquals(expectedResults, result.size());
    
    // ///////// Test first comment item //////////////
    
    SimpleItem main = result.get(0);
    assertEquals(1, main.position);
    assertFalse(main.fakeItem);
    assertNull(main.threadURLs);
  }
  
  /**
   * Test of segment method, of class ForoEnFemeninoSegmenter
   */
  @Test
  public void testJutOnePageForoEnFemeninoSegmenter() throws Exception {
    URL url = new URL("http://foro.enfemenino.com/forum/matern4/__f116853_matern4-Chicas-de-sanitas.html#9r");
    
    StringBuilder content = readPageFromResource("foro.enfemenino.com.nomorepages");
    HTMLDocumentSegmenter seg = new ForoEnFemeninoSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 10;
    assertEquals(expectedResults, result.size());
    
    // ///////// Test first comment item //////////////
    
    SimpleItem main = result.get(0);
    assertNull(main.threadURLs);
  }
}
