/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2015. All rights reserved. http://websays.com )
 *
 * Primary Author: juanfra
 * Contributors:
 * Date: Jun 22, 2015
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;

import java.net.URL;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.segmenter.pageparser.forums.ForoLosViajerosCommentsSegmenter;
import websays.types.clipping.SimpleItem;
import websays.types.clipping.SimpleItem.LanguageTag;

/**
 * @author juanfra Test for the comments segmenter of www.losviajeros.com forum
 */
public class ForoLosViajerosCommentsSegmenterTest extends SegmenterTest {
  
  /**
   * Test of segment method, of class ForocochesSegmenter.
   */
  @Test
  public void testDispatcherRegex() throws Exception {
    String[] urls = new String[] {"http://www.losviajeros.com/foros.php?t=244895&highlight=pullmantur",
        "http://www.losviajeros.com/index.php?name=Forums&file=viewtopic&t=244895&postdays=0&postorder=asc&highlight=pullmantur&start=690",
        "http://www.losviajeros.com/foros.php?st=pullmantur&sf=65",
        "http://www.losviajeros.com/index.php?name=Forums&file=search&st=pullmantur&sf=65&start=40"};
    boolean[] matchComments = new boolean[] {true, true, false, false};
    boolean[] matchSearch = new boolean[] {false, false, true, true};
    
    Matcher mSearch = Pattern.compile("www[.]losviajeros[.]com/((index)|(foros))[.]php.*[&?]st=").matcher("");
    Matcher mComm = Pattern.compile("www[.]losviajeros[.]com/((index)|(foros))[.]php.*[&?]t=").matcher("");
    
    for (int j = 0; j < urls.length; j++) {
      mSearch.reset(urls[j]);
      assertEquals(urls[j], matchSearch[j], mSearch.find());
    }
    for (int j = 0; j < urls.length; j++) {
      mComm.reset(urls[j]);
      assertEquals("COMM: " + urls[j], matchComments[j], mComm.find());
    }
    
  }
  
  /**
   * Test of segment method, of class ForocochesSegmenter.
   */
  @Test
  public void testSegment() throws Exception {
    
    String baseURL = "http://www.losviajeros.com/index.php?name=Forums&file=viewtopic&t=100170&postdays=0&postorder=asc&start=10";
    
    URL url = new URL(baseURL);
    // The page was downloaded on jun 22nd 2015. If it has
    // changed:
    // 1- download it again,
    // 2- generate new tests for the new format,
    // 3- and adapt the segmenter keeping the old parsing for backwards compatibility
    StringBuilder content = readPageFromResource("LosViajeros-20150622.html", "UTF-8");
    Calendar cal = Calendar.getInstance();
    HTMLDocumentSegmenter seg = new ForoLosViajerosCommentsSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    /*
     * Comment out ot debug the test System.out.println(result.size());
     * 
     * for (SimpleItem t : result) { System.out.println("===================================================="); System.out.println("position [" +
     * t.position + "]"); System.out.println("author [" + t.author + "]"); System.out.println("authorLocation [" + t.authorLocation + "]");
     * System.out.println("date [" + t.date + "]"); System.out.println("itemUrl [" + t.itemUrl + "]"); System.out.println("posturl [" + t.postUrl +
     * "]"); System.out.println("language [" + t.language + "]"); System.out.println("title [" + t.title + "]"); System.out.println("body [" + t.body
     * + "]"); System.out.println("===================================================="); }
     */
    
    int expectedResults = 12; // No of comments+ basic details item which
    // you expect in the page
    assertEquals(expectedResults, result.size());
    
    String postURL = "http://www.losviajeros.com?t=100170";
    // ///////// Test Main item //////////////
    SimpleItem main = result.get(0);// The main details item at
    // index 0 (first
    // element)
    assertEquals(null, main.body);
    assertEquals("RELATOS DE CRUCERISTAS-DIARIOS DE A BORDO", main.title);
    assertEquals(null, main.author);
    assertEquals(null, main.authorLocation);
    assertEqualTimes(result.get(1).date, main.date);
    assertEquals(postURL, main.postUrl);
    assertEquals(postURL, main.itemUrl);
    assertEquals(1, main.position);
    assertEquals(LanguageTag.spanish, main.language);
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = result.get(1);
    assertEquals("Re: RELATOS DE CRUCERISTAS-DIARIOS DE ABORDO", sub1.title);
    assertEquals(
        "Hola, bueno nunca habia entado en esta parte de los cruceros siempre he entrado en cuba que es donde viaje el pasado verano, pero el proximo quiero hacer un crucero y de verdad despues de leer el de admin y montsec, como que me animo mucho mas.Seguire leyendo en otro ratito, pero tengo una pregunta para montsec, despues de leer tu diario de viaje creo que no hicistes excursiones programadas por el barco que las hicisteis por vuestra cuenta y veo que cuando bajabais del barco ibais a varias ciudades en cada escala es que estan tan cerca una de otra.GraciasEditado por luna-llena el 14-11-08. Te he editado el mensaje porque el diario no era mío   , sino de admin, no quiero honores que no me pertenecen",
        sub1.body);
    assertEquals("gemi13", sub1.author);
    assertEquals(null, sub1.authorLocation);
    cal.set(2008, 10, 14, 13, 55);// Year, Month(zero based), Date,
    // HourOfDay,
    // Minutes
    assertEqualTimes(cal.getTime(), sub1.date);
    assertEquals(postURL, sub1.postUrl);
    
    assertEquals("http://www.losviajeros.com/foros.php?p=813866#813866", sub1.itemUrl);
    assertEquals(2, sub1.position);
    assertEquals(LanguageTag.spanish, sub1.language);
    
    // ///////// Test last-1 (before last one) comment item. It also tests a comment with author location. //////////////
    
    // ///////// Test Last comment item //////////////
    SimpleItem sub9 = result.get(11);
    assertEquals("Re: RELATOS DE CRUCERISTAS-DIARIOS DE A BORDO", sub9.title);
    assertEquals("Primochenko, traslado tu mensaje a este hilo que es el apropiado dentro del foro de CrucerosSaludos", sub9.body);
    assertEquals("luna-llena", sub9.author);
    assertEquals("Barcelona", sub9.authorLocation);
    
    cal.set(2015, 2, 17, 13, 05);// Year, Month(zero based), Date,
    // HourOfDay,
    // Minutes
    assertEqualTimes(cal.getTime(), sub9.date);
    assertEquals(postURL, sub9.postUrl);
    assertEquals("http://www.losviajeros.com/foros.php?p=4709323#4709323", sub9.itemUrl);
    assertEquals(12, sub9.position);
    
  }
}
