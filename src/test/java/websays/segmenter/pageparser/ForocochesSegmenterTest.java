/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.net.URL;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.segmenter.pageparser.forums.ForocochesSegmenter;
import websays.types.clipping.SimpleItem;

public class ForocochesSegmenterTest extends SegmenterTest {
  
  /**
   * Test of segment method, of class ForocochesSegmenter.
   */
  @Test
  public void testSegment() throws Exception {
    
    String baseURL = "http://www.forocoches.com/foro/showthread.php?t=3379151";
    
    URL url = new URL(baseURL + "&page=1");
    StringBuilder content = readPageFromResource("foroCoches1.htm", "ISO-8859-1"); // charset changed
    Calendar cal = Calendar.getInstance();
    HTMLDocumentSegmenter seg = new ForocochesSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 31; // No of comments+ basic details item which
    // you expect in the page
    assertEquals(expectedResults, result.size());
    
    // ///////// Test Main item //////////////
    SimpleItem main = result.get(0);// The main details item at
    // index 0 (first
    // element)
    assertEquals("Los títulos de la UOC/UNED son respetados?", main.title);
    assertEquals(null, main.body);
    assertEquals(null, main.author);
    assertEquals(null, main.authorLocation);
    assertEquals(null, main.comments);
    assertEquals("Native score", null, main.getScore_native());
    assertEquals("Websays score", null, main.getScore_websays());
    assertEquals(null, main.votes);
    assertEqualTimes(null, main.date);
    assertEquals(baseURL, main.postUrl);
    assertEquals("3379151", main.APIObjectID);
    assertEquals(null, main.itemUrl);
    assertEquals(1, main.position);
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = result.get(1);
    assertEquals(null, sub1.title);
    assertEquals(
        "Si me saco una carrera a distancia, a la hora de buscar trabajo hay algún tipo de diferencia? Desprecian a la gente por no haber estudiado en una universidad presencial?",
        sub1.body);
    assertEquals("karcrap", sub1.author);
    assertEquals("Lo pongo en tela de juicio.", sub1.authorLocation);
    assertEquals(null, sub1.votes);
    assertEquals("Native score", null, sub1.getScore_native());
    assertEquals("Websays score", null, sub1.getScore_websays());
    assertNull(sub1.comments);
    // assertEquals(new Integer(578), sub1.comments);// posts
    cal.set(2013, 7, 27, 13, 39);// Year, Month(zero based), Date,
    // HourOfDay,
    // Minutes
    assertEqualTimes(cal.getTime(), sub1.date);
    assertEquals(url.toString(), sub1.postUrl);
    
    assertEquals("http://www.forocoches.com/foro/showthread.php?p=144219054#post144219054", sub1.itemUrl);
    assertEquals(2, sub1.position);
    
    // ///////// Test Second comment item //////////////
    SimpleItem sub2 = result.get(2);
    assertEquals(null, sub2.title);
    assertEquals("Hasta donde yo se la Uned tiene fama de ser mas dura.", sub2.body);
    assertEquals("MemasTurbo", sub2.author);
    assertEquals("Zaragoza", sub2.authorLocation);
    assertEquals(null, sub2.votes);
    assertEquals("Native score", null, sub2.getScore_native());
    assertEquals("Websays score", null, sub2.getScore_websays());
    assertNull(sub2.comments);
    // assertEquals(new Integer(328), sub2.comments); // posts
    cal.set(2013, 7, 27, 13, 40);// Year, Month(zero based), Date,
    // HourOfDay,
    // Minutes
    assertEqualTimes(cal.getTime(), sub2.date);
    assertEquals(url.toString(), sub2.postUrl);
    assertEquals("http://www.forocoches.com/foro/showthread.php?p=144219123#post144219123", sub2.itemUrl);
    assertEquals(3, sub2.position);
    
    // ///////// Test Third comment item //////////////
    SimpleItem sub3 = result.get(3);
    assertEquals(null, sub3.title);
    assertEquals("La UNED está más respetada que tu ojete.", sub3.body);
    assertEquals("Javicho", sub3.author);
    assertEquals(null, sub3.authorLocation);
    assertEquals(null, sub3.votes);
    assertEquals("Native score", null, sub3.getScore_native());
    assertEquals("Websays score", null, sub3.getScore_websays());
    assertNull(sub3.comments);
    // assertEquals(new Integer(7350), sub3.comments); // posts
    cal.set(2013, 7, 27, 13, 41);// Year, Month(zero based), Date,
    // HourOfDay,
    // Minutes
    assertEqualTimes(cal.getTime(), sub3.date);
    assertEquals(url.toString(), sub3.postUrl);
    assertEquals("http://www.forocoches.com/foro/showthread.php?p=144219213#post144219213", sub3.itemUrl);
    assertEquals(4, sub3.position);
    
    // ///////// Test last-1 (before last one) comment item //////////////
    SimpleItem sub4 = result.get(29);
    assertEquals(null, sub4.title);
    assertEquals("Que compares la UNED (más difícil que una presencial) con la UOC (mucho más fácil que la presencial) tiene tela...",
        sub4.body);
    assertEquals("Dexter DP", sub4.author);
    assertEquals(null, sub4.authorLocation);
    assertEquals(null, sub4.votes);
    assertEquals("Native score", null, sub4.getScore_native());
    assertEquals("Websays score", null, sub4.getScore_websays());
    assertNull(sub4.comments);
    // assertEquals(new Integer(3608), sub4.comments); // posts
    cal.set(2013, 7, 29, 23, 32);// Year, Month(zero based), Date,
    // HourOfDay,
    // Minutes
    assertEqualTimes(cal.getTime(), sub4.date);
    assertEquals(url.toString(), sub4.postUrl);
    assertEquals("http://www.forocoches.com/foro/showthread.php?p=144395807#post144395807", sub4.itemUrl);
    assertEquals(30, sub4.position);
    
    // ///////// Test Last comment item //////////////
    SimpleItem sub9 = result.get(30);
    assertEquals(null, sub9.title);
    assertEquals(
        "yo estuve con derecho en la UNED y la madre que me pario, vaya tochos de libros madre mía. Tiene buena fama pero es muy dura, se basa en horas y horas de estudio, tambien se dice que los estudiantes de la uned son los que sacan mejores notas en las oposiciones mas duras.",
        sub9.body);
    assertEquals("killerbox", sub9.author);
    assertEquals("Leon - Madrid", sub9.authorLocation);
    assertEquals(null, sub9.votes);
    assertEquals("Native score", null, sub9.getScore_native());
    assertEquals("Websays score", null, sub9.getScore_websays());
    assertNull(sub9.comments);
    // assertEquals(new Integer(3106), sub9.comments);
    cal.set(2013, 7, 29, 23, 37);// Year, Month(zero based), Date,
    // HourOfDay,
    // Minutes
    assertEqualTimes(cal.getTime(), sub9.date);
    assertEquals(url.toString(), sub9.postUrl);
    assertEquals("http://www.forocoches.com/foro/showthread.php?p=144396130#post144396130", sub9.itemUrl);
    assertEquals(31, sub9.position);
    
  }
}