/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;

import java.net.URL;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.segmenter.pageparser.forums.ForosVogueSegmenter;
import websays.types.clipping.SimpleItem;

public class ForosVogueSegmenterTest extends SegmenterTest {
  
  /**
   * Test of segment method, of class ForosVogueSegmenter.
   */
  @Test
  public void testSegment() throws Exception {
    
    URL url = new URL("http://foros.vogue.es/viewtopic.php?f=69&t=153884&start=200#p12585286");
    StringBuilder content = readPageFromResource("UDIMA   UOC   UNED   Trabajo y estudios   Foros Vogue.htm");
    Calendar cal = Calendar.getInstance();
    HTMLDocumentSegmenter seg = new ForosVogueSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 10; // No of comments+ basic details item which
    // you expect in the page
    assertEquals(expectedResults, result.size());
    
    // ///////// Test Main item //////////////
    SimpleItem main = result.get(0);// The main details item at
    // index 0 (first
    // element)
    assertEquals("UDIMA / UOC / UNED", main.title);
    assertEquals(null, main.body);
    assertEquals(null, main.author);
    assertEquals(null, main.authorLocation);
    assertEquals(new Integer(209), main.comments);
    assertEquals("Native score", null, main.getScore_native());
    assertEquals("Websays score", null, main.getScore_websays());
    assertEquals(null, main.votes);
    assertEqualTimes(null, main.date);
    assertEquals(url.toString(), main.postUrl);
    assertEquals("http://foros.vogue.es/viewtopic.php?t=153884", main.itemUrl);
    assertEquals(1, main.position);
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = result.get(1);
    assertEquals("Re: UDIMA / UOC / UNED", sub1.title);
    assertEquals(
        "Hola a todas,pues yo pude estudiar a distancia en la Jaume I y en la Universidad de Alcalá de Henares. Y de la primera solo tengo palabras y opiniones positivas y de la segunda no tanto, pero a lo mejor solo fue en mi caso por culpa de mi tutor,  .Estudié un Máster oficial y un Experto.",
        sub1.body);
    assertEquals("nereguay", sub1.author);
    assertEquals("Galicia", sub1.authorLocation);
    assertEquals(null, sub1.votes);
    assertEquals("Native score", null, sub1.getScore_native());
    assertEquals("Websays score", null, sub1.getScore_websays());
    
    assertEquals(new Integer(421), sub1.comments);// posts
    cal.set(2013, 7, 27, 12, 10);// Year, Month(zero based), Date, HourOfDay,
    // Minutes
    assertEqualTimes(cal.getTime(), sub1.date);
    assertEquals(url.toString(), sub1.postUrl);
    // assertEquals("http://foros.vogue.es/viewtopic.php?f=69&t=153884&start=200#p12585286", sub1.itemUrl);
    assertEquals("http://foros.vogue.es/viewtopic.php?f=69&t=153884&start=200&sid=99fa3f788cd622fcdff4ca82bcec10f3#p12585286", sub1.itemUrl);// main
                                                                                                                                             // item
                                                                                                                                             // URL +
                                                                                                                                             // #pXXXXXXX
    assertEquals(2, sub1.position);
    
    // ///////// Test Second comment item //////////////
    SimpleItem sub2 = result.get(2);
    assertEquals("Re: UDIMA / UOC / UNED", sub2.title);
    assertEquals(
        "Hola me gustaría empezar derecho en la uoc o en udima. Udima no tiene Inglés y la Uoc si tiene esta asignatura. La verdad que hace más de 20 años que dejé de estudiarlo. Sabe alguien de la complejidad de esta asignatura en la uoc ? Sabe alguien que universidad es más fácil de llevar( tengo 3 hijos pequeños y trabajo). Mi decisión es clara. Quiero hacerlo. Pero desconozco cual es la mejor elección.  Gracias por vuestra ayuda.",
        sub2.body);
    assertEquals("lola1969", sub2.author);
    assertEquals(null, sub2.authorLocation);
    assertEquals(null, sub2.votes);
    assertEquals("Native score", null, sub2.getScore_native());
    assertEquals("Websays score", null, sub2.getScore_websays());
    assertEquals(new Integer(1), sub2.comments); // posts
    cal.set(2013, 8, 1, 8, 41);// Year, Month(zero based), Date, HourOfDay,
    // Minutes
    assertEqualTimes(cal.getTime(), sub2.date);
    assertEquals(url.toString(), sub2.postUrl);
    assertEquals("http://foros.vogue.es/viewtopic.php?f=69&t=153884&start=200&sid=99fa3f788cd622fcdff4ca82bcec10f3#p12605062", sub2.itemUrl);
    assertEquals(3, sub2.position);
    
    // ///////// Test Third comment item //////////////
    SimpleItem sub3 = result.get(3);
    assertEquals("Re: UDIMA / UOC / UNED", sub3.title);
    assertEquals(
        "Mirando la web de la UOC, aparecen las sedes pero no los puntos de realización de examen (tengo entendido que en algunas CCAA en las que no tienen sede realizan exámenes en colegios o universidades). ¿Los lugares de realización de examen son fijos o depende de que haya un número mínimo de alumnos por titulación? Es que si cuentas con desplazarte a un punto más o menos cercano y al final resulta que tienes que ir a examinarte mucho más lejos...",
        sub3.body);
    assertEquals("MisaeNohara", sub3.author);
    assertEquals(null, sub3.authorLocation);
    assertEquals(null, sub3.votes);
    assertEquals("Native score", null, sub3.getScore_native());
    assertEquals("Websays score", null, sub3.getScore_websays());
    
    assertEquals(new Integer(18), sub3.comments); // posts
    cal.set(2013, 8, 1, 19, 48);// Year, Month(zero based), Date, HourOfDay,
    // Minutes
    assertEqualTimes(cal.getTime(), sub3.date);
    assertEquals(url.toString(), sub3.postUrl);
    assertEquals("http://foros.vogue.es/viewtopic.php?f=69&t=153884&start=200&sid=99fa3f788cd622fcdff4ca82bcec10f3#p12607089", sub3.itemUrl);
    assertEquals(4, sub3.position);
    
    // ///////// Test Forth comment item //////////////
    SimpleItem sub4 = result.get(4);
    assertEquals("Re: UDIMA / UOC / UNED", sub4.title);
    assertEquals(".", sub4.body);
    assertEquals("Belmi", sub4.author);
    assertEquals(null, sub4.authorLocation);
    assertEquals(null, sub4.votes);
    assertEquals("Native score", null, sub4.getScore_native());
    assertEquals("Websays score", null, sub4.getScore_websays());
    
    assertEquals(new Integer(530), sub4.comments); // posts
    cal.set(2013, 8, 3, 12, 5);// Year, Month(zero based), Date, HourOfDay,
    // Minutes
    assertEqualTimes(cal.getTime(), sub4.date);
    assertEquals(url.toString(), sub4.postUrl);
    assertEquals("http://foros.vogue.es/viewtopic.php?f=69&t=153884&start=200&sid=99fa3f788cd622fcdff4ca82bcec10f3#p12613396", sub4.itemUrl);
    assertEquals(5, sub4.position);
    
    // ///////// Test Last comment item //////////////
    SimpleItem sub9 = result.get(9);
    assertEquals("Re: UDIMA / UOC / UNED", sub9.title);
    assertEquals(
        "HelenA escribió: Hola chicas, alguien ha hecho el master de Dirección de Empresas en la OUC?y  el de Direccion y gestión de recursos humanos? y este último en la UDIMA???? estoy entre estas tres opciones.. cualquier opinion será bien recibida a estas alturas porque me tengo que decididir ya!graciasss  puedo mirar en el foro de la cauoc qué impresiones hay. No sé si puedes entrar a mirar tú sin estar registrada.",
        sub9.body);
    assertEquals("¿eh?", sub9.author);
    assertEquals(null, sub9.authorLocation);
    assertEquals(null, sub9.votes);
    assertEquals("Native score", null, sub9.getScore_native());
    assertEquals("Websays score", null, sub9.getScore_websays());
    assertEquals(new Integer(43960), sub9.comments);
    cal.set(2013, 8, 14, 10, 4);// Year, Month(zero based), Date, HourOfDay,
    // Minutes
    assertEqualTimes(cal.getTime(), sub9.date);
    assertEquals(url.toString(), sub9.postUrl);
    assertEquals("http://foros.vogue.es/viewtopic.php?f=69&t=153884&start=200&sid=99fa3f788cd622fcdff4ca82bcec10f3#p12660251", sub9.itemUrl);
    assertEquals(10, sub9.position);
    
  }
  
}