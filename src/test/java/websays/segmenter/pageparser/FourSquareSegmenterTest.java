/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.net.URL;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class FourSquareSegmenterTest extends SegmenterTest {
  
  /**
   * Test of segment method, of class FourSquareSegmenter.
   */
  @Test
  public void testSegment() throws Exception {
    
    // URL url = new URL("https://es.foursquare.com/v/tgi- fridays/4b280662f964a520d58d24e3");
    URL url = new URL("https://es.foursquare.com/v/fridays-bernab%C3%A9u/4b280662f964a520d58d24e3");
    
    // StringBuilder content = readPageFromResource("T.G.I. Friday's - Hispanoamérica - Madrid.htm");
    StringBuilder content = readPageFromResource("foursquare-tgi.html");
    Calendar cal = Calendar.getInstance();
    HTMLDocumentSegmenter seg = new FourSquareSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 46; // No of comments + 1 (basic details item which you expect in the page)
    assertEquals(expectedResults, result.size());
    
    // ///////// Test Main item //////////////
    SimpleItem main = result.get(0);// The main details item at index 0 (first element)
    assertEquals("FRIDAYS BERNABÉU", main.title);
    
    assertEquals(
        "T.G.I. Friday's son restaurantes temáticos especializados en comida americana con un original diseño y el mejor ambiente. Recuerda:“In here it’s always Friday”.",
        main.body);// Program will apply ',' to separate address fields
    assertEquals(null, main.author);
    assertEquals(null, main.authorLocation);
    assertEquals(new Integer(78), main.reviews);
    assertEquals("Native Score", new Integer(72), main.getScore_native());
    assertEquals("Websays Score", new Integer(72), main.getScore_websays());
    assertEquals(new Integer(205), main.votes);
    assertEqualDates(null, main.date);
    assertEquals("tgifridays.es", main.itemUrl);
    assertEquals(url.toString(), main.postUrl);
    assertEquals(1, main.position);
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = result.get(1);
    assertEquals(null, sub1.title);
    assertEquals("Mejor viernes solamente aquí!", sub1.body);
    assertEquals("Roman Abramovich", sub1.author);
    assertEquals(null, sub1.authorLocation);
    assertEquals(null, sub1.reviews);
    assertEquals("Native score", null, sub1.getScore_native());
    assertEquals("Websays score", null, sub1.getScore_websays());
    // assertEquals(new Integer(36), sub1.votes);// likes
    
    cal.set(2014, 11, 8);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), sub1.date);
    assertNull(sub1.itemUrl);
    assertEquals(url.toString(), sub1.postUrl);
    assertEquals(6, sub1.position);
    
    // ///////// Test Second comment item //////////////
    SimpleItem sub2 = result.get(2);
    assertEquals(null, sub2.title);
    assertEquals("T.g.i friday's!! Simply favorite!!", sub2.body);
    assertEquals("Johnie", sub2.author);
    assertEquals(null, sub2.authorLocation);
    assertEquals(null, sub2.reviews);
    assertEquals("Native score", null, sub2.getScore_native());
    assertEquals("Websays score", null, sub2.getScore_websays());
    
    // assertEquals(new Integer(15), sub2.votes); // likes
    
    cal.set(2014, 10, 26);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), sub2.date);
    assertNull(sub2.itemUrl);
    assertEquals(url.toString(), sub2.postUrl);
    assertEquals(7, sub2.position);
    
    // ///////// Test Third comment item //////////////
    SimpleItem sub3 = result.get(3);
    assertEquals(null, sub3.title);
    assertEquals("Las costillas y la torre de tortitas", sub3.body);
    assertEquals("Maite Beobide", sub3.author);
    assertEquals(null, sub3.authorLocation);
    assertEquals(null, sub3.reviews);
    assertEquals("Native score", null, sub3.getScore_native());
    assertEquals("Websays score", null, sub3.getScore_websays());
    
    // assertEquals(new Integer(6), sub3.votes); // likes
    
    cal.set(2014, 9, 26);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), sub3.date);
    assertNull(sub3.itemUrl);
    assertEquals(url.toString(), sub3.postUrl);
    assertEquals(8, sub3.position);
    
    // ///////// Test Forth comment item //////////////
    SimpleItem sub4 = result.get(4);
    assertEquals(null, sub4.title);
    assertEquals("Normal, las margaritas muy buenas", sub4.body);
    assertEquals("Ricardo Montes Beobide", sub4.author);
    assertEquals(null, sub4.authorLocation);
    assertEquals(null, sub4.reviews);
    assertEquals("Native score", null, sub4.getScore_native());
    assertEquals("Websays score", null, sub4.getScore_websays());
    
    // assertEquals(new Integer(3), sub4.votes);// likes
    
    cal.set(2014, 9, 25);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), sub4.date);
    assertNull(sub4.itemUrl);
    assertEquals(url.toString(), sub4.postUrl);
    assertEquals(9, sub4.position);
    
    // ///////// Test Last comment item //////////////
    SimpleItem sub5 = result.get(result.size() - 1);
    assertEquals(null, sub5.title);
    assertEquals("Mejores nachos con queso de MADRID", sub5.body);
    assertEquals("Javato#13", sub5.author);
    assertEquals(null, sub5.authorLocation);
    assertEquals(null, sub5.reviews);
    assertEquals("Native score", null, sub5.getScore_native());
    assertEquals("Websays score", null, sub5.getScore_websays());
    
    assertEquals(null, sub5.votes);// likes
    
    cal.set(2012, 0, 30);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), sub5.date);
    assertNull(sub5.itemUrl);
    assertEquals(url.toString(), sub5.postUrl);
    assertEquals(50, sub5.position);
    
  }
  
}