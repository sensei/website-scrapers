/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;

import java.net.URL;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class GamesVillageTest extends SegmenterTest {
  
  /**
   * Test of segment method, of class GamesVillageSegmenter.
   */
  @Test
  public void testGamesVillageSegmenter() throws Exception {
    
    URL url = new URL(
        "http://www.gamesvillage.it/forum/showthread.php?958202-PosteMobile-amp-Connessione-internet-ho-finito-il-calendario-gi%E0-2-volte");
    
    StringBuilder content = readPageFromResource("gamesvillage.it");
    
    Calendar cal = Calendar.getInstance();
    HTMLDocumentSegmenter seg = new GamesVillageSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 8;
    assertEquals(expectedResults, result.size());
    
    // ///////// Test First comment item //////////////
    
    SimpleItem main = result.get(0);
    assertEquals("post_31702854", main.threadId);
    assertEquals("PosteMobile & Connessione internet [ho finito il calendario gi� 2 volte]", main.title);
    assertEquals("Slash90", main.author);
    
    assertEquals(
        "Salve a tuuuuutti,settimana scorsa ho avuto la geniale idea di passare da 3 a PosteMobile, attivando una promozione davvero interessante: 4,5� al mese per 150minuti/150sms verso tutti e 1gb di internet. Ottimo, prima pagavo 8� alla 3 per meno roba e per quelle dannate soglie settimanali inutili.Se non fosse che la rete internet va proprio a pene di segugio.Mi spiego. Il cellulare prende benissimo, c'� la letterina H e le freccette si \"illuminano\" solo in un senso (quello di invio), ma nessun pacchetto viene scaricato. Questo accade il 95% del tempo, nel restante 5% la connessione va normalmente, ma dopo un p� si incanta di nuovo. Insomma navigare � impossibile, messaggiare con whatsapp non ne parliamo, idem ricevere le email. Ritorno al centro PosteMobile, controllo col tizio che la configurazione sia giusta, ed � giusta. Cambio rom (la MIUI non vede la rete poste ma vodafone, pensavo fosse quello il problema), passo a una rom con Android pulito; ora il cellullare vede la rete PosteMobile, ma il funzionamento ad cazzum della connessione rimane lo stesso.Ritorno al centro PosteMobile, insieme al tizio contattiamo il centro assistenza, dove controllano che tutto sia apposto; non trovano nulla che non va. Provo la sim con altri due cellulari (Xperia Mini Pro, Galaxy S), e idem la situazione non cambia. Provo la sim con un cellulare del centro Poste e magicamente va (e vedi che magari ho beccato quel momento in cui la rete funge  ).Che cosa diavolo pu� essere???? Non posso cambiare operatore per un mese, e per motivi di lavoro non posso cambiare numero di cellulare e ho assoluto bisogno di internet fuori casa",
        main.body);
    
    cal.set(2013, 8, 28);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), main.date);
    assertEquals(null, main.itemUrl);
    assertEquals(url.toString(), main.postUrl);
    // assertEquals(null, sub1.imageLink);
    assertEquals(1, main.position);
    
    // /////// Test Sixth comment item //////////////
    SimpleItem six = result.get(6);
    assertEquals("post_31704094", six.threadId);
    assertEquals(null, six.title);
    assertEquals("sergio leonhart", six.author);
    assertEquals("Rimettere il telefono con rom stock?", six.body);
    assertEquals(null, six.itemUrl);
    cal.set(2013, 8, 29);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), six.date);
    assertEquals(url.toString(), six.postUrl);
    assertEquals(7, six.position);
    
  }
}
