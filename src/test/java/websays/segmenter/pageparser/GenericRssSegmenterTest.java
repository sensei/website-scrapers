/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;

import java.net.URL;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class GenericRssSegmenterTest extends SegmenterTest {
  
  @Test
  public void testCommentSegment() throws Exception {
    
    URL url = new URL("http://www.artinamericamagazine.com/rss/");
    StringBuilder content = readPageFromResource("Art in America - Most Recent.xhtml");
    HTMLDocumentSegmenter seg = new GenericRssSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 40; // No of comments
    Calendar cal = Calendar.getInstance();
    cal.setTimeZone(TimeZone.getTimeZone("GMT+01"));
    assertEquals(expectedResults, result.size());
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = result.get(0);
    assertEquals("Günther Förg, 1952-2013", sub1.title);
    assertEquals(
        "Günther Förg died Dec. 5 at 61. Förg was known for his Modernist-influenced work, which included painting, photography and sculpture.",
        sub1.body);
    assertEquals("By Sarah Cascone", sub1.author);
    cal.set(2013, 11, 9, 18, 00);// Mon, 09 Dec 2013 18:00:00 +0100
    assertEqualTimes(cal.getTime(), sub1.date);
    assertEquals(url.toString(), sub1.postUrl);
    assertEquals("http://www.artinamericamagazine.com/news-features/news/gnther-frg-1952-2013/", sub1.itemUrl);
    assertEquals("http://www.artinamericamagazine.com/files/2013/12/09/img-gunther-forg_175955994889.jpg_standalone.jpg", sub1.imageLink);
    assertEquals(1, sub1.position);
    
    SimpleItem sub2 = result.get(1);
    assertEquals("Moving Pictures at Art Basel Miami Beach", sub2.title);
    assertEquals(
        "Unlike previous years, video was surprisingly well represented this year at Art Basel Miami Beach, which is otherwise focused on static objects for sale.",
        sub2.body);
    assertEquals("By Paul David Young", sub2.author);
    cal.set(2013, 11, 9, 17, 00);// Mon, 09 Dec 2013 17:00:00 +0100
    assertEqualTimes(cal.getTime(), sub2.date);
    assertEquals(url.toString(), sub2.postUrl);
    assertEquals("http://www.artinamericamagazine.com/files/2013/12/09/img-art-basel-video-1_170446851714.jpg_wide_hthumb.jpg",
        sub2.imageLink);
    assertEquals("http://www.artinamericamagazine.com/news-features/news/moving-pictures-at-art-basel-miami-beach/", sub2.itemUrl);
    
    assertEquals(2, sub2.position);
    
    SimpleItem sub30 = result.get(29);
    assertEquals("Lyon Biennale", sub30.title);
    assertEquals(
        "The 12th installment of the Lyon Biennale, titled \"Meanwhile . . . Suddenly and Then,\" is the third and last to address the theme of \"transmission.\"",
        sub30.body);
    assertEquals("By David Ebony", sub30.author);
    cal.set(2013, 10, 27, 0, 0);// Wed, 27 Nov 2013 00:00:00 +0100
    assertEqualTimes(cal.getTime(), sub30.date);
    assertEquals(url.toString(), sub30.postUrl);
    assertEquals("http://www.artinamericamagazine.com/files/2013/10/22/img-21b-lyon-biennale_103732662337.jpg_standalone.jpg",
        sub30.imageLink);
    assertEquals("http://www.artinamericamagazine.com/reviews/lyon-biennale/", sub30.itemUrl);
    assertEquals(30, sub30.position);
    
  }
  
  @Test
  public void testSegment2() throws Exception {
    
    URL url = new URL("http://blog.trasmediterranea.es/feed/");
    StringBuilder content = readPageFromResource("rss.blogs.transmediterranea.es");
    HTMLDocumentSegmenter seg = new GenericRssSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 10; // No of comments
    assertEquals(expectedResults, result.size());
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = result.get(0);
    
    assertEquals("http://blog.trasmediterranea.es/wp-content/uploads/FACE1-150x150.jpg", sub1.imageLink);
    
  }
  
  /*
   * / Some XML comes with & and must be replaced with &amp; because of if it is not changed will raise an error for format error
   */
  @Test
  public void testRSSFormatProblem() throws Exception {
    
    URL url = new URL("http://www.televideo.rai.it/televideo/pub/rss101.xml");
    StringBuilder content = readPageFromResource("rss.televideo.rai.it");
    HTMLDocumentSegmenter seg = new GenericRssSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 20; // No of comments
    assertEquals(expectedResults, result.size());
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = result.get(0);
    
    assertEquals("http://www.televideo.rai.it/televideo/pub/view.jsp?id=253&p=101", sub1.itemUrl);
    
  }
  
}
