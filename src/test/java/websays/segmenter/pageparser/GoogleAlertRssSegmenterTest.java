/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class GoogleAlertRssSegmenterTest extends SegmenterTest {
  
  @Test
  public void testCommentSegment() throws Exception {
    
    URL url = new URL("http://www.google.com/alerts/feeds/06947120149481435190/12732943556499731855");
    StringBuilder content = readPageFromResource("google_alert_eroski.html");
    HTMLDocumentSegmenter seg = new GoogleAlertsRssSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 1; // No of comments
    assertEquals(expectedResults, result.size());
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = result.get(0);
    assertTrue(sub1.fakeItem);
    List<String> threadURLs = sub1.threadURLs;
    List<String> expectedURLs = new ArrayList<String>();
    expectedURLs.add("http://www.alimarket.es/noticia/150901/Eroski-inaugura-su-plataforma-de-Mercapalma;.node1");
    expectedURLs.add("http://www.alimarket.es/noticia/150839/Soysuper-lanza-una-novedosa-herramienta");
    expectedURLs.add("http://www.diariovasco.com/v/20140325/alto-deba/cuponazo-repartio-euros-puesto-20140325.html");
    
    for (int i = 0; i < threadURLs.size(); i++) {
      assertEquals(expectedURLs.get(i), threadURLs.get(i));
    }
    
  }
}
