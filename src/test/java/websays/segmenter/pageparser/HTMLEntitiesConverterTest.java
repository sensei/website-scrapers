/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2015. All rights reserved. http://websays.com )
 *
 * Primary Author: juanfra
 * Contributors:
 * Date: Jul 6, 2015
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import websays.segmenter.common.HTMLEntitiesConverter;

/**
 * Test the HTMLEntitiesConverter
 * 
 * @author juanfra
 *
 */
public class HTMLEntitiesConverterTest {
  
  /**
   * &quot; entity cleaning test.
   */
  @Test
  public void test() {
    String dirty = "&quot;Qualità e cortesia&quot;";
    String clean = null;
    clean = HTMLEntitiesConverter.unHTMLEntities(dirty);
    assertEquals("The html string was not correctly cleaned", "\"Qualità e cortesia\"", clean);
  }
}
