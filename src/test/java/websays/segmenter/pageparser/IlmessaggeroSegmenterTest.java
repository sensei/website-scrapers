/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;

import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

public class IlmessaggeroSegmenterTest extends BaseDocumentSegmenterTest {
  
  @Override
  public void setUp() throws Exception {
    StringBuilder content = readPageFromResource("Il Messaggero.shtml", "ISO-8859-1");
    this.seg = new IlmessaggeroSegmenter();
    HtmlCleaner cleaner = createCleaner();
    this.rootNode = cleaner.clean(content.toString());
  }
  
  @Override
  public void testFindMainTitle() {
    String title = seg.findMainTitle(rootNode);
    assertEquals(
        "«Cosa fareste soli in auto con la Boldrini?» Il post di Grillo scatena gli insulti sessisti Imbarazzo M5s, poi lo staff li cancella - Il Messaggero",
        title);
  }
  
  @Override
  public void testFindMainDescripcion() {
    String body = seg.findMainDescripcion(rootNode);
    assertEquals(
        "Una valanga di insulti sessisti nei confronti della presidente della Camera Laura Boldrini solleva un nuovo caso che coinvolge il Movimento cinque stelle.",
        body);
  }
  
  @Override
  public void testFindMainImageLink() {
    assertEquals("http://www.ilmessaggero.it/MsgrNews/ART/20140201_lb.jpg", seg.findMainImageLink(rootNode));
  }
  
  @Override
  public void testFindComments() {
    assertEquals(5, seg.findComments(rootNode).length);
  }
  
  @Override
  public void testFindCommentAuthor() {
    TagNode[] comments = seg.findComments(rootNode);
    assertEquals("Max Mas", seg.findCommentAuthor(comments[0]));
    assertEquals("Domenico50", seg.findCommentAuthor(comments[1]));
    assertEquals("delenda carthago", seg.findCommentAuthor(comments[2]));
  }
  
  @Override
  public void testFindCommentTitle() {
    TagNode[] comments = seg.findComments(rootNode);
    assertEquals("In auto con la Boldrini ?", seg.findCommentTitle(comments[0]));
    assertEquals("Consideravo", seg.findCommentTitle(comments[1]));
    assertEquals("Avanti Grillo!", seg.findCommentTitle(comments[2]));
  }
  
  @Override
  public void testFindCommentDescripcion() {
    TagNode[] comments = seg.findComments(rootNode);
    assertEquals("Il rischio e' che ti chieda di andare al ristorante ......è non sarà certamente lei a pagare il conto.",
        seg.findCommentDescripcion(comments[0]));
    assertEquals("il parlamento con Berlusconi un Vespasiano, con l'arrivo dei grilllini ormai è una cloaca massima.",
        seg.findCommentDescripcion(comments[1]));
    assertEquals(
        "....quelli che - povere verginelle che al mercato non vanno - sono in disaccordo sono gli stessi che si lamentano che l'Italia va male e poi assistono impotenti al matrimonio Renzi/Berlusca... Il primo che si lamenta lo insulto pure io, altro che politically correct!",
        seg.findCommentDescripcion(comments[2]));
  }
  
  @Override
  public void testFindCommentDate() {
    Calendar cal = Calendar.getInstance();
    TagNode[] comments = seg.findComments(rootNode);
    cal.set(2014, 1, 2, 10, 40); // 02-02-2014 alle 10:40
    assertEqualTimes(cal.getTime(), seg.findCommentDate(comments[0]));
    cal.set(2014, 1, 2, 10, 32);// 02-02-2014 alle 10:32
    assertEqualTimes(cal.getTime(), seg.findCommentDate(comments[1]));
    cal.set(2014, 1, 2, 10, 27); // 02-02-2014 alle 10:27
    assertEqualTimes(cal.getTime(), seg.findCommentDate(comments[2]));
  }
}
