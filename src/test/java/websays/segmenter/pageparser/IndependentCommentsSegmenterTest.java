/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.segmenter.base.MinimalDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class IndependentCommentsSegmenterTest extends SegmenterTest {
  
  /**
   * Test of segment method, of class IndependentComments Segmenter.
   */
  @Test
  public void testCommentSegment() throws Exception {
    
    URL url = new URL(
        "http://www.independent.co.uk/news/uk/home-news/mod-rules-out-compulsory-pregnancy-tests-for-servicewomen-after-it-is-revealed-that-a-miniscule-number-became-pregnant-on-deployment-in-the-last-decade-9137046.html");
    StringBuilder content = readPageFromResource("independent.co.uk.v2");
    HTMLDocumentSegmenter seg = new IndependentSegmenter();
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    assertEquals("9137046", result.get(0).threadId);
    
    URL urlComments = new URL("http://comments.us1.gigya.com/comments.getComments?categoryID=ArticleComments&streamID="
        + result.get(0).threadId + "&start=0&threadLimit=50&sort=votesDesc&threadDepth=2&includeStreamInfo=true&includeUID=true&"
        + "APIKey=2_bkQWNsWGVZf-fA4GnOiUOYdGuROCvoMoEN4WMj6_YBq4iecWA-Jp9D2GZCLbzON4");
    
    assertEquals(urlComments.toString() + IndependentSegmenter.getSite(), result.get(0).threadURLs.get(0));
    
    StringBuilder contentComments = readPageFromResource("independent.co.uk.v2.comments");
    MinimalDocumentSegmenter segComments = new IndependentCommentsSegmenter();
    List<SimpleItem> resultComments = segComments.segment(contentComments.toString(), urlComments);
    
    int expectedResults = 2; // No of comments
    assertEquals(expectedResults, resultComments.size());
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = resultComments.get(0);
    assertEquals(null, sub1.title);
    assertEquals("soldiers should be having sex on the mission ", sub1.body);
    assertEquals("Portonth Rodrigezes", sub1.author);
    assertEquals("http://graph.facebook.com/100006172119492/picture?type=square", sub1.imageLink);
    assertEquals(new Integer(0), sub1.likes);
    assertEquals(new Integer(0), sub1.votes);
    assertEqualTimes(new Date(1392765550109L), sub1.date); // Wed Feb 19 00:19:XX CET 2014
    assertEquals(url.toString(), sub1.postUrl);
    assertEquals("a7d37116fe1c418ba0ba0715df188e60", sub1.threadId);
    assertEquals(1, sub1.position);
    assertTrue(sub1.isComment);
  }
}