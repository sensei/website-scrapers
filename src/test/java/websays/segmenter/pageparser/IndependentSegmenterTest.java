/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;

import java.net.URL;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class IndependentSegmenterTest extends SegmenterTest {
  
  @Test
  public void testArtsEntertainmentSegment() throws Exception {
    
    URL url = new URL(
        "http://www.independent.co.uk/arts-entertainment/films/features/its-not-like-ive-completely-conquered-everything-benedict-cumberbatch-interview-8856778.html");
    StringBuilder content = readPageFromResource("'It's not like I've completely conquered everything': Benedict Cumberbatch interview - Features - Films - The Independent.html");
    HTMLDocumentSegmenter seg = new IndependentSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 1;
    // you expect in the page
    assertEquals(expectedResults, result.size());
    
    // ///////// Test Main item //////////////
    SimpleItem main = result.get(0);// The main details item at index 0 (first element)
    assertEquals("'It's not like I've completely conquered everything': Benedict Cumberbatch interview", main.title);
    assertEquals(
        "Even the brightest stars can have their off days. Take Benedict Cumberbatch and his time on the set of the recent Star Trek Into Darkness, playing the villainous Khan. One scene took place at California's National Ignition Facility, a real-life laboratory practising nuclear fusion. His co-star Simon Pegg told him it was a dangerous location and that he would need protection from special “neutron cream”.",
        main.body);
    assertEquals("James Mottram", main.author);
    assertEquals(null, main.authorLocation);
    assertEqualTimes(DatatypeConverter.parseDateTime("2013-10-04").getTime(), main.date); // 2013-10-04
    assertEquals(1, main.position);
    assertEquals(url.toString(), main.postUrl);
    assertEquals("8856778", main.threadId); // To extract comments
    assertEquals("http://comments.us1.gigya.com/comments.getComments?categoryID=ArticleComments&streamID=" + main.threadId
        + "&start=0&threadLimit=50&sort=votesDesc&threadDepth=2&includeStreamInfo=true&includeUID=true&"
        + "APIKey=2_bkQWNsWGVZf-fA4GnOiUOYdGuROCvoMoEN4WMj6_YBq4iecWA-Jp9D2GZCLbzON4" + IndependentSegmenter.getSite(),
        main.threadURLs.get(0));
  }
  
  @Test
  public void testPage() throws Exception {
    
    URL url = new URL(
        "http://www.independent.co.uk/news/uk/home-news/mod-rules-out-compulsory-pregnancy-tests-for-servicewomen-after-it-is-revealed-that-a-miniscule-number-became-pregnant-on-deployment-in-the-last-decade-9137046.html");
    StringBuilder content = readPageFromResource("independent.co.uk.v2");
    HTMLDocumentSegmenter seg = new IndependentSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 1;
    // you expect in the page
    assertEquals(expectedResults, result.size());
    
    // ///////// Test Main item //////////////
    SimpleItem main = result.get(0);// The main details item at index 0 (first element)
    assertEquals(
        "MoD rules out compulsory pregnancy tests for servicewomen after it is revealed that a 'miniscule' number became pregnant on deployment in the last decade",
        main.title);
    assertEquals(
        "Just 20 servicewomen a year were sent home in the last decade from Afghanistan or Iraq on the grounds they were pregnant, official figures have shown.",
        main.body);
    assertEquals("Lewis Smith", main.author);
    assertEqualTimes(DatatypeConverter.parseDateTime("2014-02-18").getTime(), main.date); // 2014-02-18
    assertEquals(1, main.position);
    assertEquals(url.toString(), main.postUrl);
    assertEquals("9137046", main.threadId); // To extract comments
    assertEquals("http://comments.us1.gigya.com/comments.getComments?categoryID=ArticleComments&streamID=" + main.threadId
        + "&start=0&threadLimit=50&sort=votesDesc&threadDepth=2&includeStreamInfo=true&includeUID=true&"
        + "APIKey=2_bkQWNsWGVZf-fA4GnOiUOYdGuROCvoMoEN4WMj6_YBq4iecWA-Jp9D2GZCLbzON4" + IndependentSegmenter.getSite(),
        main.threadURLs.get(0));
  }
}
