/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;

import java.net.URL;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class InstagramSegmenterTest extends SegmenterTest {
  
  /**
   * Test of segment method, of class Instagram Segmenter.
   */
  @Test
  public void testSegment() throws Exception {
    
    URL url = new URL("http://instagram.com/p/eUl6PQS0jE/");
    StringBuilder content = readPageFromResource("camper.1.html");
    HTMLDocumentSegmenter seg = new InstagramSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 1; // No of comments+ basic details item which
    // you expect in the page
    assertEquals(expectedResults, result.size());
    
    // ///////// Test Main item //////////////
    SimpleItem main = result.get(0);// The main details item at
    // index 0 (first
    // element)
    assertEquals("Instagram", main.title);
    assertEquals("#camper", main.body);
    assertEquals("erdemdenkli", main.author);
    assertEquals(null, main.authorLocation);
    assertEquals(null, main.comments);// 'comentarios' value
    assertEquals("Native score", null, main.getScore_native());
    assertEquals("Websays score", null, main.getScore_websays());
    assertEquals(new Integer(12), main.votes);
    
    Double timeStamp = 1379335495.0;
    long milisFromEpoch = (1000 * timeStamp.longValue());
    assertEqualTimes(new Date(milisFromEpoch), main.date);
    assertEquals(url.toString(), main.postUrl);
    assertEquals(null, main.itemUrl);
    assertEquals(1, main.position);
    
  }
  
  @Test
  public void testSegment2() throws Exception {
    
    URL url = new URL("http://instagram.com/p/eUl6PQS0jE/");
    StringBuilder content = readPageFromResource("camper.2.html");
    HTMLDocumentSegmenter seg = new InstagramSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 1; // No of comments+ basic details item which
    // you expect in the page
    assertEquals(expectedResults, result.size());
    
    // ///////// Test Main item //////////////
    SimpleItem main = result.get(0);// The main details item at
    // index 0 (first
    // element)
    assertEquals("Instagram", main.title);
    assertEquals("I won't say that I missed my @camper boots, but I missed my camper boots #fall", main.body);
    assertEquals("run_jac_run", main.author);
    assertEquals(null, main.authorLocation);
    assertEquals(null, main.comments);// 'comentarios' value
    assertEquals("Native score", null, main.getScore_native());
    assertEquals("Websays score", null, main.getScore_websays());
    assertEquals(new Integer(4), main.votes);
    
    Double timeStamp = 1379353280.0;
    long milisFromEpoch = (1000 * timeStamp.longValue());
    assertEqualTimes(new Date(milisFromEpoch), main.date);
    assertEquals(url.toString(), main.postUrl);
    assertEquals(null, main.itemUrl);
    assertEquals(1, main.position);
    
  }
  
}