/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.List;

import junit.framework.AssertionFailedError;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class LaVanguardiaCommentsSegmenterTest extends SegmenterTest {
  
  @Test
  public void testLaVanguardiaCommentsSegmenter() throws MalformedURLException, UnsupportedEncodingException, IOException, Exception,
      AssertionFailedError {
    URL urlComments = new URL("http://bootstrap.grupogodo1.fyre.co/bs3/v3.1/grupogodo1.fyre.co/351112/NTQ0MTEzOTk1MzI=/0.json");
    StringBuilder contentComments = readPageFromResource("laVarguandiaComments.json");
    HTMLDocumentSegmenter segComments = new LaVanguardiaCommentsSegmenter();
    List<SimpleItem> resultComments = segComments.segment(contentComments.toString(), urlComments);
    
    int expectedResults = 88; // No of comments
    assertEquals(expectedResults, resultComments.size());
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = resultComments.get(0);
    // for (SimpleItem simpleItem : resultComments) {
    // System.out.println(simpleItem.threadId);
    // }
    assertEquals("182663178", sub1.threadId);
    assertEquals(null, sub1.title);
    assertEquals(
        "no me creo que la corrupcion politica de ciu haya llegado a su fin y menos aun que ocupen el centro para seguir defendiento los intereses de la gran burguesia catalana",
        sub1.body);
    assertEquals("robado por estos politicos corruptos", sub1.author);
    assertEquals("http://gravatar.com/avatar/1fc3a3134c0f530dbd6061b2b58d7b6d/?s=50&d=http://avatars.fyre.co/a/anon/50.jpg", sub1.imageLink);
    assertEquals(new Integer(0), sub1.likes);
    
    assertEqualTimes(new Date(1404053609L * 1000L), sub1.date); // June 29, 2014 16:53
    assertEquals(urlComments.toString(), sub1.postUrl);
    assertEquals(1, sub1.position);
    assertTrue(sub1.isComment);
  }
}
