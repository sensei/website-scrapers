/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.TimeZone;

import org.htmlcleaner.HtmlCleaner;

public class LaVanguardiaSegmenterTest extends BaseDocumentSegmenterTest {
  
  @Override
  public void setUp() throws Exception {
    StringBuilder content = readPageFromResource("lavanguardia.html");
    this.seg = new LaVanguardiaSegmenter();
    HtmlCleaner cleaner = createCleaner();
    this.rootNode = cleaner.clean(content.toString());
  }
  
  @Override
  public void testFindMainTitle() {
    String title = seg.findMainTitle(rootNode);
    assertEquals("Duran Lleida anima a CiU a encarar el fin de \"un ciclo político\" y reforzar el centro político", title);
  }
  
  @Override
  public void testFindMainDescripcion() {
    String body = seg.findMainDescripcion(rootNode);
    assertEquals(
        "Caldes de Malavella/Barcelona. (Agencias/Redacci&oacute;n).- El l&iacute;der de Uni&oacute; Democr&agrave;tica de Catalunya (UDC), Josep Antoni Duran Lleida, ha hecho un llamamiento este domingo a su partido para que&nbsp;asuma que la democracia en Catalunya y el conjunto de Espa&ntilde;a se encuentra ante el fin \"de un ciclo pol&iacute;tico\" y ha animado a CiU a asumir \"la responsabilidad de reactivar el espacio de centro\".Duran Lleida, que ha intervenido en la XX Escuela de Verano del partido en Caldes de Malavella, ha sentenciado en su intervenci&oacute;n que \"vivimos una crisis institucional, en la pol&iacute;tica y los partidos. La ciudadan&iacute;a no cree en los partidos. No somos eficaces a los ojos de la gente para resolver los problemas&rdquo;. Muy autocr&iacute;tico con la percepci&oacute;n ciudadana de los partidos, Duran ha admitido que \"vivimos en una sociedad en la que todo lo que huele a instituci&oacute;n o a autoridad no sirve. No nos quieren ni ver\".El l&iacute;der de Uni&oacute; cree que \"como partido pol&iacute;tico debemos tener muy presente\" este escenario y ha se&ntilde;alado que \"tenemos la responsabilidad de reactivar el espacio de centro. El espacio de CiU se debilita pero debemos reforzarlo. Esta es nuestra obligaci&oacute;n, desde Uni&oacute; y desde CDC. Debemos reforzar el espacio de CiU, el espacio de centro.\"En este sentido, Duran ha incidido en que su partido debe \"ir hacia un gran movimiento, una uni&oacute;n de democracia social\" en la que \"se debe comprometer m&aacute;s gente\". De este modo, ha apuntado que \"no se trata de hacer ninguna operaci&oacute;n de maquillaje, sino de contribuir junto con mucha gente a forjar un movimiento que d&eacute; respuesta a las crisis actuales y las supere, que vuelva a reencontrar la confianza de los ciudadanos que hoy est&aacute;n hartos, o en el mejor de los casos, desencantados, de la pol&iacute;tica \".En relaci&oacute;n a la posibilidad de que ERC se incorpore al Govern de la Generalitat, Duran Lleida, ha considerado &ldquo;oportunista&rdquo; que ERC sume votos y adhesiones por no estar en el Govern mientras \"el PSC se hunde y CiU pierde apoyo\".El presidente del Comit&eacute; de Gobierno de UDC asegura que el empuje que cogen los republicanos no se debe s&oacute;lo al independentismo - \"la palabra m&aacute;gica que lo arregla todo\", ha precisado- sino que ERC tambi&eacute;n \"recoge los frutos\" del desencanto que viven los ciudadanos por la pol&iacute;tica.",
        body);
  }
  
  @Override
  public void testFindMainImageLink() {
    assertEquals("http://img02.lavanguardia.com/2014/06/29/Duran-i-Lleida-y-Llibert-Cuatr_54410540745_51365774690_67_67.jpg",
        seg.findMainImageLink(rootNode));
  }
  
  @Override
  public void testFindMainAuthor() {
    assertEquals(null, seg.findMainAuthor(rootNode));
  }
  
  @Override
  public void testFindMainDate() {
    Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Madrid"));
    cal.set(2014, 05, 29, 13, 22);
    assertEqualTimes(cal.getTime(), seg.findMainDate(rootNode));
  }
  
  @Override
  public void testFindMainThreadURLs() {
    assertEquals("http://bootstrap.grupogodo1.fyre.co/bs3/v3.1/grupogodo1.fyre.co/351112/NTQ0MTEzOTk1MzI=/0.json",
        seg.findMainThreadURLs(rootNode).get(0));
  }
  
  @Override
  public void testFindMainThreadId() {
    assertNull(null);
  }
  
  @Override
  public void testFindMainBody() {
    assertEquals(
        " Caldes de Malavella/Barcelona. (Agencias/Redacci&oacute;n).- El l&iacute;der de Uni&oacute; Democr&agrave;tica de Catalunya (UDC), Josep Antoni Duran Lleida, ha hecho un llamamiento este domingo a su partido para que&nbsp;asuma que la democracia en Catalunya y el conjunto de Espa&ntilde;a se encuentra ante el fin \"de un ciclo pol&iacute;tico\" y ha animado a CiU a asumir \"la responsabilidad de reactivar el espacio de centro\". Duran Lleida, que ha intervenido en la XX Escuela de Verano del partido en Caldes de Malavella, ha sentenciado en su intervenci&oacute;n que \"vivimos una crisis institucional, en la pol&iacute;tica y los partidos. La ciudadan&iacute;a no cree en los partidos. No somos eficaces a los ojos de la gente para resolver los problemas&rdquo;. Muy autocr&iacute;tico con la percepci&oacute;n ciudadana de los partidos, Duran ha admitido que \"vivimos en una sociedad en la que todo lo que huele a instituci&oacute;n o a autoridad no sirve. No nos quieren ni ver\". El l&iacute;der de Uni&oacute; cree que \"como partido pol&iacute;tico debemos tener muy presente\" este escenario y ha se&ntilde;alado que \"tenemos la responsabilidad de reactivar el espacio de centro. El espacio de CiU se debilita pero debemos reforzarlo. Esta es nuestra obligaci&oacute;n, desde Uni&oacute; y desde CDC. Debemos reforzar el espacio de CiU, el espacio de centro.\" En este sentido, Duran ha incidido en que su partido debe \"ir hacia un gran movimiento, una uni&oacute;n de democracia social\" en la que \"se debe comprometer m&aacute;s gente\". De este modo, ha apuntado que \"no se trata de hacer ninguna operaci&oacute;n de maquillaje, sino de contribuir junto con mucha gente a forjar un movimiento que d&eacute; respuesta a las crisis actuales y las supere, que vuelva a reencontrar la confianza de los ciudadanos que hoy est&aacute;n hartos, o en el mejor de los casos, desencantados, de la pol&iacute;tica \". En relaci&oacute;n a la posibilidad de que ERC se incorpore al Govern de la Generalitat, Duran Lleida, ha considerado &ldquo;oportunista&rdquo; que ERC sume votos y adhesiones por no estar en el Govern mientras \"el PSC se hunde y CiU pierde apoyo\". El presidente del Comit&eacute; de Gobierno de UDC asegura que el empuje que cogen los republicanos no se debe s&oacute;lo al independentismo - \"la palabra m&aacute;gica que lo arregla todo\", ha precisado- sino que ERC tambi&eacute;n \"recoge los frutos\" del desencanto que viven los ciudadanos por la pol&iacute;tica. ",
        seg.findMainBody(rootNode));
  }
  
  @Override
  public void testFindMainTimeIsReal() {
    assertTrue("Time is real should be true. If not, a problem with parsin may be arising.", seg.findMainTimeIsReal(rootNode));
  }
  
}
