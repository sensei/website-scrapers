/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;

import org.htmlcleaner.HtmlCleaner;

public class LaVanguardiaSegmenterTest20150611 extends BaseDocumentSegmenterTest {
  
  @Override
  public void setUp() throws Exception {
    StringBuilder content = readPageFromResource("lavanguardia20150611.html");
    this.seg = new LaVanguardiaSegmenter();
    HtmlCleaner cleaner = createCleaner();
    this.rootNode = cleaner.clean(content.toString());
  }
  
  @Override
  public void testFindMainTitle() {
    String title = seg.findMainTitle(rootNode);
    assertEquals("MicroBank dará hasta 30 millones en créditos para estudiar un máster europeo", title);
  }
  
  @Override
  public void testFindMainDescripcion() {
    String body = seg.findMainDescripcion(rootNode);
    assertEquals(
        "Barcelona, 11 jun (EFE).- MicroBank, la filial de La Caixa especializada en la concesión de microcréditos, será la primera entidad europea en impulsar, con el apoyo del Fondo Europeo de Inversiones (FEI), una línea de crédito de hasta 30 millones de euros para estudiantes que realicen un máster en otro país europeo.MicroBank calcula que unos 2.500 estudiantes de máster Erasmus+ se podrán beneficiar a lo largo de los próximos tres años de estos créditos, que tendrán un importe de entre 12.000 y 18.000 euros y no requerirán la aportación de ningún tipo de aval, ya que el FEI actúa como garante de las operaciones.Pueden solicitar estos préstamos tanto los estudiantes universitarios españoles que estén inscritos en una universidad europea a nivel de máster, como también los alumnos europeos que se matriculen en un centro universitario español.El rueda de prensa, el presidente de MicroBank, Antonio Vila, ha explicado que el préstamo se concede directamente al solicitante, con el único requisito de acreditar la matriculación en un máster de un centro universitario europeo.El crédito no se empieza a amortizar hasta un año después de terminar el curso y, a partir de ese momento, se puede devolver en un período de cinco o seis años, dependiendo de la duración del máster.Además, ha destacado Vila, existe la posibilidad de solicitar en cualquier momento un período de carencia de un año.El tipo de interés establecido para estos microcréditos es del 5,15 % y no se cobran comisiones de estudio o por amortización anticipada o cancelación. Tan sólo se debe satisfacer una cantidad equivalente al 1 % del capital solicitado en concepto de comisión de apertura.El presidente de MicroBank ha comentado que estarían \"muy contentos\" si la morosidad en estos préstamos \"no pasara del 10 %\", aunque ha recalcado que el FEI cubre hasta el 18 % del importe total, es decir, hasta 5,4 millones de euros.Con esta nueva línea de créditos para estudiantes de máster europeos, la Comisión Europea, a través del FEI, quiere potenciar la movilidad geográfica de los jóvenes, facilitándoles un microcrédito que cubra gastos como la matrícula, los viajes o la estancia en el extranjero durante el período de realización del curso.El director ejecutivo del FEI, Pier Luigi Gilibert, ha explicado que su organismo ha confiado en CaixaBank para poner en marcha esta nueva iniciativa, pero que el objetivo es que de aquí a 2020 bancos de otros países europeos se sumen al proyecto con líneas de financiación de hasta 500 millones de euros, de los que podrían beneficiarse unos 200.000 estudiantes.\"Creo que será un modelo a imitar en el resto de Europa porque es excelente\", ha afirmado Gilibert, que ha resaltado la \"excelente\" colaboración que el FEI mantiene con MicroBank, entidad \"muy bien posicionada\" en el mercado de los microcréditos.\"Estamos convencidos de que este producto llena un vacío en el mercado\", ha añadido el directivo del FEI, que ha recordado que a menudo los estudiantes desisten de realizar cursos en el extranjero por la falta de financiación.Según una encuesta realizada por Stiga para MicroBank, el 74,5 % de los universitarios españoles que quiere estudiar un máster están interesados en hacerlo en otro país europeo. Sin embargo, la falta de recursos económicos es el principal motivo, en un 36,8 % de los casos, por el que se descarta realizar el máster fuera de España.La nueva línea de créditos impulsada por MicroBank y el FEI podrá solicitarse a través de las 5.400 oficinas que CaixaBank tiene en toda España.",
        body);
  }
  
  @Override
  public void testFindMainImageLink() {
    assertEquals(null, seg.findMainImageLink(rootNode));
  }
  
  @Override
  public void testFindMainAuthor() {
    assertEquals(null, seg.findMainAuthor(rootNode));
  }
  
  @Override
  public void testFindMainDate() {
    Calendar cal = Calendar.getInstance();
    cal.set(2015, 05, 11, 14, 45);
    assertEqualTimes(cal.getTime(), seg.findMainDate(rootNode));
  }
  
  @Override
  public void testFindMainThreadURLs() {
    assertEquals("http://bootstrap.grupogodo1.fyre.co/bs3/v3.1/grupogodo1.fyre.co/351112/NTQ0MzIyMjI0Mjc=/0.json",
        seg.findMainThreadURLs(rootNode).get(0));
  }
  
  @Override
  public void testFindMainThreadId() {
    assertNull(null);
  }
  
  @Override
  public void testFindMainBody() {
    assertEquals(
        "    TEMAS RELACIONADOS  Comisión Europea Erasmus Caixabank - Criteria CaixaCorp      NOTICIAS RELACIONADAS  Primeros préstamos Erasmus+ para cursar un máster en el extranjero Adicae denuncia a CaixaBank ante Banco España, CE y EBA por comisión cajeros CE pide información a CaixaBank por comisiones en cajeros de otras tarjetas La UE se interesa por el nuevo sistema de pagos en los cajeros de CaixaBank La CE aprueba la adquisición del banco portugués BPI por parte de CaixaBank    Barcelona, 11 jun (EFE).- MicroBank, la filial de La Caixa especializada en la concesión de microcréditos, será la primera entidad europea en impulsar, con el apoyo del Fondo Europeo de Inversiones (FEI), una línea de crédito de hasta 30 millones de euros para estudiantes que realicen un máster en otro país europeo. MicroBank calcula que unos 2.500 estudiantes de máster Erasmus+ se podrán beneficiar a lo largo de los próximos tres años de estos créditos, que tendrán un importe de entre 12.000 y 18.000 euros y no requerirán la aportación de ningún tipo de aval, ya que el FEI actúa como garante de las operaciones. Pueden solicitar estos préstamos tanto los estudiantes universitarios españoles que estén inscritos en una universidad europea a nivel de máster, como también los alumnos europeos que se matriculen en un centro universitario español. El rueda de prensa, el presidente de MicroBank, Antonio Vila, ha explicado que el préstamo se concede directamente al solicitante, con el único requisito de acreditar la matriculación en un máster de un centro universitario europeo. El crédito no se empieza a amortizar hasta un año después de terminar el curso y, a partir de ese momento, se puede devolver en un período de cinco o seis años, dependiendo de la duración del máster. Además, ha destacado Vila, existe la posibilidad de solicitar en cualquier momento un período de carencia de un año. El tipo de interés establecido para estos microcréditos es del 5,15 % y no se cobran comisiones de estudio o por amortización anticipada o cancelación. Tan sólo se debe satisfacer una cantidad equivalente al 1 % del capital solicitado en concepto de comisión de apertura. El presidente de MicroBank ha comentado que estarían \"muy contentos\" si la morosidad en estos préstamos \"no pasara del 10 %\", aunque ha recalcado que el FEI cubre hasta el 18 % del importe total, es decir, hasta 5,4 millones de euros. Con esta nueva línea de créditos para estudiantes de máster europeos, la Comisión Europea, a través del FEI, quiere potenciar la movilidad geográfica de los jóvenes, facilitándoles un microcrédito que cubra gastos como la matrícula, los viajes o la estancia en el extranjero durante el período de realización del curso. El director ejecutivo del FEI, Pier Luigi Gilibert, ha explicado que su organismo ha confiado en CaixaBank para poner en marcha esta nueva iniciativa, pero que el objetivo es que de aquí a 2020 bancos de otros países europeos se sumen al proyecto con líneas de financiación de hasta 500 millones de euros, de los que podrían beneficiarse unos 200.000 estudiantes. \"Creo que será un modelo a imitar en el resto de Europa porque es excelente\", ha afirmado Gilibert, que ha resaltado la \"excelente\" colaboración que el FEI mantiene con MicroBank, entidad \"muy bien posicionada\" en el mercado de los microcréditos. \"Estamos convencidos de que este producto llena un vacío en el mercado\", ha añadido el directivo del FEI, que ha recordado que a menudo los estudiantes desisten de realizar cursos en el extranjero por la falta de financiación. Según una encuesta realizada por Stiga para MicroBank, el 74,5 % de los universitarios españoles que quiere estudiar un máster están interesados en hacerlo en otro país europeo. Sin embargo, la falta de recursos económicos es el principal motivo, en un 36,8 % de los casos, por el que se descarta realizar el máster fuera de España. La nueva línea de créditos impulsada por MicroBank y el FEI podrá solicitarse a través de las 5.400 oficinas que CaixaBank tiene en toda España.  ",
        seg.findMainBody(rootNode));
  }
  
  @Override
  public void testFindMainTimeIsReal() {
    assertTrue("Time is real should be true. If not, a problem with parsin may be arising.", seg.findMainTimeIsReal(rootNode));
  }
  
}
