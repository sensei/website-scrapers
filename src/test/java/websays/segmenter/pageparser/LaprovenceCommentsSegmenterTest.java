/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class LaprovenceCommentsSegmenterTest extends SegmenterTest {
  
  @Test
  public void testCommentSegment() throws Exception {
    
    URL url = new URL(
        "http://www.laprovence.com/article/edition-aix-pays-daix/2731262/la-neige-invitee-surprise-vendredi-en-provence.html/reactions");
    StringBuilder content = readPageFromResource("La ProvenceComment.html");
    HTMLDocumentSegmenter seg = new LaprovenceCommentsSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 11; // No of comments + replys
    Calendar cal = Calendar.getInstance();
    assertEquals(expectedResults, result.size());
    
    SimpleItem sub1 = result.get(1);
    assertEquals(null, sub1.title);
    assertEquals("Tombe la neige, chanson bien française", sub1.body);
    assertEquals("lesverites13", sub1.author);
    assertEquals("http://www.laprovence.com/media/imagecache/user_profil/pictures/picture-103619-1370596369.gif", sub1.imageLink);
    assertEquals(null, sub1.authorLocation);
    cal.set(2014, 1, 1, 8, 53);// 01/02/2014 08:53
    assertEqualTimes(cal.getTime(), sub1.date);
    assertEquals(url.toString(), sub1.postUrl);
    assertTrue(sub1.isComment);
    assertEquals(2, sub1.position);
    
  }
}
