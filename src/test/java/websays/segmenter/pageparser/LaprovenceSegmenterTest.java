/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.List;

import org.htmlcleaner.HtmlCleaner;

public class LaprovenceSegmenterTest extends BaseDocumentSegmenterTest {
  
  @Override
  public void setUp() throws Exception {
    StringBuilder content = readPageFromResource("Aix-en-Provence | La neige, invitée surprise vendredi en Provence | La Provence.html");
    this.seg = new LaprovenceSegmenter();
    HtmlCleaner cleaner = createCleaner();
    this.rootNode = cleaner.clean(content.toString());
  }
  
  @Override
  public void testFindMainTitle() {
    String title = seg.findMainTitle(rootNode);
    assertEquals("La neige, invitée surprise vendredi en Provence", title);
  }
  
  @Override
  public void testFindMainDescripcion() {
    String body = seg.findMainDescripcion(rootNode);
    assertEquals(
        "Cette nuit et ce matin en Provence, la neige est l'invitée surprise de la météo. Les flocons sont tombés (et tombent encore dans certains endroits) dans le pays aixois et dans le Haut-Var. Venelles, Vauvenargues, Puyloubier, Trets... comptent parmi les communes les plus touchées. Certains parlent de 8 cm sur Vauvenargues.Dans le Haut-Var, le sol est blanc dans le secteur de Saint-Zaccharie, plan d'Aups et de Pourrières. Les flocons - sans tenir au sol selon Vinci autoroutes - sont visibles sur l'A8 (de Pourrières à Rousset) mais aussi sur l'A51, dans le sud des Alpes-de-Haute-Provence et le nord aixois des Bouches-du-Rhône.Selon Météo-France, la perturbation quitte l'Est des Bouches-du-Rhône et le Var pour se diriger vers l'ouest du département. Notamment Salon et Arles. Mais dans cette zone, c'est plutôt de la pluie (neige fondue ?) qui devrait marquer cette météo matinale.",
        body);
  }
  
  @Override
  public void testFindMainImageLink() {
    assertEquals("http://www.laprovence.com/media/imagecache/article-taille-normale/2014/01/31/sbayonbdr.jpg",
        seg.findMainImageLink(rootNode));
  }
  
  @Override
  public void testFindMainAuthor() {
    assertEquals("Xavier Cherica (XCherica@laprovence.com)", seg.findMainAuthor(rootNode));
  }
  
  @Override
  public void testFindMainDate() {
    Calendar cal = Calendar.getInstance();
    cal.set(2014, 0, 31, 8, 30);
    assertEqualTimes(cal.getTime(), seg.findMainDate(rootNode));
  }
  
  @Override
  public void testFindMainComments() {
    assertEquals(new Integer(10), seg.findMainComments(rootNode));
  }
  
  @Override
  public void testFindMainThreadId() {
    assertEquals("/article/edition-aix-pays-daix/2731262/la-neige-invitee-surprise-vendredi-en-provence.html",
        seg.findMainThreadId(rootNode));
  }
  
  @Override
  public void testFindMainThreadURLs() {
    List<String> urls = seg.findMainThreadURLs(rootNode);
    assertEquals(1, urls.size());
    assertEquals(
        "http://www.laprovence.com/article/edition-aix-pays-daix/2731262/la-neige-invitee-surprise-vendredi-en-provence.html/reactions",
        urls.get(0));
  }
  
  @Override
  public void testFindMainTimeIsReal() {
    assertTrue(
        "Main page time is always real. As it is not right now, check the parsing process for errors or changes in the page source structure.",
        seg.findMainTimeIsReal(rootNode));
  }
  
}
