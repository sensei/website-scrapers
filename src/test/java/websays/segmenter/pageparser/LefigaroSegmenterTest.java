/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;

import javax.xml.bind.DatatypeConverter;

import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

public class LefigaroSegmenterTest extends BaseDocumentSegmenterTest {
  
  @Override
  public void setUp() throws Exception {
    StringBuilder content = readPageFromResource("Anna Chapman, une espionne si sexy.html");
    this.seg = new LefigaroSegmenter();
    HtmlCleaner cleaner = createCleaner();
    this.rootNode = cleaner.clean(content.toString());
  }
  
  @Override
  public void testFindMainTitle() {
    String title = seg.findMainTitle(rootNode);
    assertEquals("Anna Chapman, une espionne si sexy", title);
  }
  
  @Override
  public void testFindMainDescripcion() {
    String body = seg.findMainDescripcion(rootNode);
    assertEquals(
        "La célèbre espionne aux allures de James Bond girl, symbole du renouveau diplomatique russe et égérie de la jeunesse poutinienne, s'apprête à venir à Sotchi. Comme un nouveau pied de nez à l'Occident…",
        body);
  }
  
  @Override
  public void testFindMainAPIObjectID() {
    assertEquals("253015184720028", seg.findMainAPIObjectID(rootNode));
  }
  
  @Override
  public void testFindMainImageLink() {
    assertEquals("http://www.lefigaro.fr/medias/2014/01/31/PHO9ab73260-84e3-11e3-97e3-ef0367b39b73-805x453.jpg",
        seg.findMainImageLink(rootNode));
  }
  
  @Override
  public void testFindMainAuthor() {
    assertEquals("Vladimir Fédorovski", seg.findMainAuthor(rootNode));
  }
  
  @Override
  public void testFindMainDate() {
    assertEqualTimes(DatatypeConverter.parseDateTime("2014-01-31T12:04:17+01:00").getTime(), seg.findMainDate(rootNode));
  }
  
  @Override
  public void testFindComments() {
    assertEquals(10, seg.findComments(rootNode).length);
  }
  
  @Override
  public void testFindMainComments() {
    assertEquals(new Integer(14), seg.findMainComments(rootNode));
  }
  
  @Override
  public void testFindCommentAuthor() {
    TagNode[] comments = seg.findComments(rootNode);
    assertEquals("deogratias", seg.findCommentAuthor(comments[0]));
    assertEquals("LODGE", seg.findCommentAuthor(comments[1]));
    assertEquals("kiditout", seg.findCommentAuthor(comments[8]));
  }
  
  @Override
  public void testFindCommentDescripcion() {
    TagNode[] comments = seg.findComments(rootNode);
    assertEquals(
        "ce n'est pas un propos contre le genre féminin de qualifier une femme de sexy ? ... il faudrait demander à Vallaud-Belkacem",
        seg.findCommentDescripcion(comments[0]));
    assertEquals("Échange 200 femen ( françaises ou ukrainiennes ) contre 1 Anna Chapman.", seg.findCommentDescripcion(comments[1]));
    assertEquals("Au moins il a bon goût Poutine !Et nous ?", seg.findCommentDescripcion(comments[8]));
  }
  
  @Override
  public void testFindCommentImageLink() {
    TagNode[] comments = seg.findComments(rootNode);
    assertEquals("http://plus.lefigaro.fr/sites/default/files/imagecache/Petite/pictures/picture-2250285-61awop7x.gif",
        seg.findCommentImageLink(comments[0]));
    assertEquals("http://plus.lefigaro.fr/sites/default/files/imagecache/Petite/pictures/picture-1093236-5so50jo.png",
        seg.findCommentImageLink(comments[1]));
    assertEquals("http://plus.lefigaro.fr/sites/default/files/imagecache/Petite/avatar_selection/avatar-11.jpg",
        seg.findCommentImageLink(comments[8]));
  }
  
  @Override
  public void testFindCommentDate() {
    Calendar cal = Calendar.getInstance();
    TagNode[] comments = seg.findComments(rootNode);
    cal.set(2014, 1, 1, 15, 28); // Le 01/02/2014 à 15:28
    assertEqualTimes(cal.getTime(), seg.findCommentDate(comments[0]));
    cal.set(2014, 1, 1, 14, 10); // Le 01/02/2014 à 14:10
    assertEqualTimes(cal.getTime(), seg.findCommentDate(comments[1]));
    cal.set(2014, 1, 1, 10, 36); // Le 01/02/2014 à 10:36
    assertEqualTimes(cal.getTime(), seg.findCommentDate(comments[8]));
  }
  
  @Override
  public void testFindMainTimeIsReal() {
    assertTrue(seg.findMainTimeIsReal(rootNode));
  }
  
  @Override
  public void testFindCommentTimesIsReal() {
    assertTrue(seg.findCommentTimeIsReal(rootNode));
  }
  
}
