/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.junit.Test;

import websays.types.clipping.SimpleItem;

public class LemondeCommentsSegmenterTest extends BaseDocumentSegmenterTest {
  
  @Test
  public void test1() throws Exception {
    
    StringBuilder content = readPageFromResource("Les Français posent un regard sombre sur leur pays reactions.html");
    this.seg = new LemondeCommentsSegmenter();
    HtmlCleaner cleaner = createCleaner();
    this.rootNode = cleaner.clean(content.toString());
    
    assertEquals(
        "http://www.lemonde.fr/politique/reactions/2013/12/03/les-francais-posent-un-regard-sombre-sur-leur-pays_3524384_823448.html",
        seg.findMainThreadId(rootNode));
    
    assertEquals(29, seg.findComments(rootNode).length);
    
    TagNode[] dates = seg.findComments(rootNode);
    assertEqualDates(DatatypeConverter.parseDateTime("2013-12-05T11:19:42+01:00").getTime(), seg.findCommentDate(dates[0]));
    assertEqualDates(DatatypeConverter.parseDateTime("2013-12-04T20:29:03+01:00").getTime(), seg.findCommentDate(dates[1]));
    assertEqualDates(DatatypeConverter.parseDateTime("2013-12-04T23:33:10+01:00").getTime(), seg.findCommentDate(dates[2]));
    assertEqualDates(DatatypeConverter.parseDateTime("2013-12-04T19:53:54+01:00").getTime(), seg.findCommentDate(dates[3]));
    
    TagNode[] authors = seg.findComments(rootNode);
    assertEquals("ERIC LOMBARD", seg.findCommentAuthor(authors[0]));
    assertEquals("Joseph", seg.findCommentAuthor(authors[1]));
    assertEquals("DH", seg.findCommentAuthor(authors[2]));
    assertEquals("Gérard", seg.findCommentAuthor(authors[3]));
    
    TagNode[] comments = seg.findComments(rootNode);
    assertEquals(
        "Mais où sont donc passés les Créatifs culturels ? Absents du tableau des 5 France, alors qu'ils seraient 17% en France ...",
        seg.findCommentDescripcion(comments[0]));
    assertEquals(
        "C'est sûr que la France est en déclin avec ces gouvernements passés et présent qui ont oublié la notion d'intérêt général. Que dire de ces députés qui ne savent même pas compter ( pas un seul des 20 interrogés a été capable de répondre exactement à la question combien font 8x7) 56 mesdames messieurs qui faites semblant d'être assidus à l'Assemblée en faisant un petit tour de 10 minutes par jour pour passer à la caisse à la fin du mois l'esprit tranquille sans avoir travaillé une minute!",
        seg.findCommentDescripcion(comments[1]));
    assertEquals(
        "il n'y a pas 60 millions de présidents et de députés que diable! il y a tous ceux qui travaillent ,y compris les politiques, et font avancer le pays! avoir confiance dans ses concitoyens c'est déjà reconnaître leurs capacités à compléter les nôtres.Donc heureusement que nous ne faisons pas tous la même chose (à part geindre!) le pays avance. Reconnaissons nous mutuellement des qualités sinon qui reconnaîtra les miennes! Mon plombier, mon boulanger, les profs de mon fils sont des gens formidables",
        seg.findCommentDescripcion(comments[2]));
    
    TagNode[] images = seg.findComments(rootNode);
    assertEquals("//s1.lemde.fr/medias/web/1.2.643/img/placeholder/avatar.svg", seg.findCommentImageLink(images[0]));
    assertEquals("//s1.lemde.fr/medias/web/1.2.643/img/placeholder/avatar.svg", seg.findCommentImageLink(images[1]));
    assertEquals("//s1.lemde.fr/medias/web/1.2.643/img/placeholder/avatar.svg", seg.findCommentImageLink(images[2]));
    assertEquals("//s1.lemde.fr/medias/web/1.2.643/img/placeholder/avatar.svg", seg.findCommentImageLink(images[3]));
    
  }
  
  @Test
  public void testMayo2014FirstPage() throws Exception {
    URL url = new URL("http://www.lemonde.fr/societe/article/2014/05/17/jerome-kerviel-derniere-etape-avant-la-prison_4420550_3224.html");
    
    StringBuilder content = readPageFromResource("jerome-kerviel-derniere-etape-avant-la-prison_4420550_3224.html");
    LemondeSegmenter seg = new LemondeSegmenter();
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    assertEquals("http://www.lemonde.fr/societe/reactions/2014/05/17/jerome-kerviel-derniere-etape-avant-la-prison_4420550_3224.html",
        result.get(0).threadId);
    
    URL urlComments = new URL(result.get(0).threadId);
    
    StringBuilder contentComments = readPageFromResource("reactions-jerome-kerviel-derniere-etape-avant-la-prison_4420550_3224.html");
    LemondeCommentsSegmenter seg1 = new LemondeCommentsSegmenter();
    List<SimpleItem> resultComments = seg1.segment(contentComments.toString(), urlComments);
    
    int expectedResults = 20; // No of comments
    assertEquals(expectedResults, resultComments.size());
    
    SimpleItem first = resultComments.get(0);
    
    // First item
    assertEquals("http://www.lemonde.fr/societe/reactions/2014/05/17/jerome-kerviel-derniere-etape-avant-la-prison_4420550_3224.html",
        first.threadId);
    assertEqualDates(DatatypeConverter.parseDateTime("2014-05-18T08:54:13+01:00").getTime(), first.date);
    assertEquals("2371654", first.APIObjectID);
    assertEquals("Debonaloi", first.author);
    assertEquals(
        "Une affaire incohérente du point de vue factuel et juridique, la banque est responsable des faits et gestes de ses employés surtout quand ils combinent dans le butrecherché  du bénéfice de l'entreprise , les fautes professionnelles  normalement sont du domaine des prud'hommes. Kerviel a été mal défendu, l'incroyable scandale des mises de jeu allouées  dans les faits à l'employé, a fait choisir une stratégie médiatique, c'était une erreur, maintenant l'ex trader doit avoir la piété discrète .",
        first.body);
    assertEquals("//s1.lemde.fr/medias/web/1.2.652/img/placeholder/avatar.svg", first.imageLink);
    assertEquals("http://www.lemonde.fr/societe/reactions/2014/05/17/jerome-kerviel-derniere-etape-avant-la-prison_4420550_3224.html",
        first.threadId);
    assertEquals("http://www.lemonde.fr/societe/reactions/2014/05/17/jerome-kerviel-derniere-etape-avant-la-prison_4420550_3224_2.html",
        first.threadURLs.get(0));
    assertTrue(first.isComment);
    
    // Twenty item
    SimpleItem twenty = resultComments.get(19);
    
    assertEquals("http://www.lemonde.fr/societe/reactions/2014/05/17/jerome-kerviel-derniere-etape-avant-la-prison_4420550_3224.html",
        twenty.threadId);
    assertEqualDates(DatatypeConverter.parseDateTime("2014-05-17T19:59:05+01:00").getTime(), twenty.date);
    assertEquals("2371483", twenty.APIObjectID);
    assertEquals("émouvant", twenty.author);
    assertEquals(
        "Ah, si ce garçon  a le soutien de Mgr di Falco et de JL Mélenchon, nous sommes sauvés. Mais que fait Carla? Elle est en tournée ?",
        twenty.body);
    assertEquals("//s1.lemde.fr/medias/web/1.2.652/img/placeholder/avatar.svg", twenty.imageLink);
    assertEquals("http://www.lemonde.fr/societe/reactions/2014/05/17/jerome-kerviel-derniere-etape-avant-la-prison_4420550_3224.html",
        twenty.threadId);
    assertNull(twenty.threadURLs);
    assertTrue(twenty.isComment);
  }
  
  @Test
  public void testMayo2014SecondPage() throws Exception {
    URL url = new URL("http://www.lemonde.fr/societe/article/2014/05/17/jerome-kerviel-derniere-etape-avant-la-prison_4420550_3224.html");
    
    StringBuilder content = readPageFromResource("jerome-kerviel-derniere-etape-avant-la-prison_4420550_3224.html");
    LemondeSegmenter seg = new LemondeSegmenter();
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    assertEquals("http://www.lemonde.fr/societe/reactions/2014/05/17/jerome-kerviel-derniere-etape-avant-la-prison_4420550_3224.html",
        result.get(0).threadId);
    
    URL urlComments = new URL(result.get(0).threadId);
    
    StringBuilder contentComments = readPageFromResource("reactions-2-jerome-kerviel-derniere-etape-avant-la-prison_4420550_3224.html");
    LemondeCommentsSegmenter seg1 = new LemondeCommentsSegmenter();
    List<SimpleItem> resultComments = seg1.segment(contentComments.toString(), urlComments);
    
    int expectedResults = 22; // No of comments
    assertEquals(expectedResults, resultComments.size());
    
    SimpleItem first = resultComments.get(0);
    
    // First item
    assertEquals("2371538", first.APIObjectID);
    assertEquals("http://www.lemonde.fr/societe/reactions/2014/05/17/jerome-kerviel-derniere-etape-avant-la-prison_4420550_3224.html",
        first.threadId);
    assertEqualDates(DatatypeConverter.parseDateTime("2014-05-17T22:07:58+01:00").getTime(), first.date);
    assertEquals("benoit lebarbe", first.author);
    assertEquals(
        "aucune indulgence!!!il a \"joue \" il a gagne ... puis il a perdu..la SOCIETE GENERALE a gagne ... puis a perdudommage pour ceux qui ont perdu ..sans gagne.. et sans jouer...BONNET BLANC et BLANC BONNETque tout le monde paye!!!",
        first.body);
    assertEquals("//s1.lemde.fr/medias/web/1.2.652/img/placeholder/avatar.svg", first.imageLink);
    assertEquals("http://www.lemonde.fr/societe/reactions/2014/05/17/jerome-kerviel-derniere-etape-avant-la-prison_4420550_3224.html",
        first.threadId);
    assertEquals("http://www.lemonde.fr/societe/reactions/2014/05/17/jerome-kerviel-derniere-etape-avant-la-prison_4420550_3224_3.html",
        first.threadURLs.get(0));
    assertTrue(first.isComment);
    
    // Twenty item
    SimpleItem twenty = resultComments.get(21);
    
    assertEquals("2371427", twenty.APIObjectID);
    assertEquals("http://www.lemonde.fr/societe/reactions/2014/05/17/jerome-kerviel-derniere-etape-avant-la-prison_4420550_3224.html",
        twenty.threadId);
    assertEqualDates(DatatypeConverter.parseDateTime("2014-05-17T18:15:25+01:00").getTime(), twenty.date);
    assertEquals("Fenotte", twenty.author);
    assertEquals(
        "La loi étant la même pour tous (déclaration des droits de l'homme et du citoyen), gageons que le parquet de Paris va délivrer un mandat d'arrêt européen afin de le faire conduire sans attendre davantage en prison.Non ? Mme Taubira va donner des instructions en sens contraire ?Que disait \"moi, président\" sur la République exemplaire ?Parce que s'il n'y va pas, faudra expliquer ça à tous les trafiquants de stups et autres délinquants qui n'ont pas droit au même traitement...",
        twenty.body);
    assertEquals("//s1.lemde.fr/medias/web/1.2.652/img/placeholder/avatar.svg", twenty.imageLink);
    assertEquals("http://www.lemonde.fr/societe/reactions/2014/05/17/jerome-kerviel-derniere-etape-avant-la-prison_4420550_3224.html",
        twenty.threadId);
    assertNull(twenty.threadURLs);
    assertTrue(twenty.isComment);
    
    // Test parentID
    SimpleItem parentComment = resultComments.get(19);
    SimpleItem sonComment = resultComments.get(20);
    assertEquals(sonComment.APIObjectID, "2371773");
    assertEquals(sonComment.parentID, parentComment.APIObjectID);
    assertEquals(sonComment.author, "Jean-Manuel GIELY");
    assertEquals(sonComment.body, "Pour une fois? Alors la CGT se trompe, surtout celle de la SG");
  }
}
