/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.net.URL;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.htmlcleaner.HtmlCleaner;
import org.junit.Test;

import websays.types.clipping.SimpleItem;

public class LemondeSegmenterTest extends BaseDocumentSegmenterTest {
  
  @Test
  public void test1() throws Exception {
    
    StringBuilder content = readPageFromResource("Les Français posent un regard sombre sur leur pays.html");
    this.seg = new LemondeSegmenter();
    HtmlCleaner cleaner = createCleaner();
    this.rootNode = cleaner.clean(content.toString());
    
    String title = seg.findMainTitle(rootNode);
    assertEquals("Les Français posent un regard sombre sur leur pays", title);
    
    assertEquals(
        "http://s1.lemde.fr/image/2013/12/03/200x200/3524404_4_5845_ill-3524404-54f6-000-par7632716_9c899f1b8eb67ee1b9868ba83d1fa8aa.jpg",
        seg.findMainImageLink(rootNode));
    
    String body = seg.findMainDescripcion(rootNode);
    assertEquals("La France est « en déclin », selon une majorité de personnes interrogées par l'institut CSA.", body);
    
    assertEqualTimes(DatatypeConverter.parseDateTime("2013-12-03T10:29:46+01:00").getTime(), seg.findMainDate(rootNode));
    
    assertEquals("Jean-Baptiste de Montvalon", seg.findMainAuthor(rootNode));
    
    assertEquals("166878320861", seg.findMainAPIObjectID(rootNode));
    
    assertEquals(
        "http://www.lemonde.fr/politique/reactions/2013/12/03/les-francais-posent-un-regard-sombre-sur-leur-pays_3524384_823448.html",
        seg.findMainThreadId(rootNode));
    
    // assertEquals(
    // "http://www.lemonde.fr/politique/reactions/2013/12/03/les-francais-posent-un-regard-sombre-sur-leur-pays_3524384_823448.html", seg
    // .findMainThreadURLs(rootNode).get(0));
    
  }
  
  @Test
  public void testMayo2014() throws Exception {
    
    URL url = new URL("http://www.lemonde.fr/societe/article/2014/05/17/jerome-kerviel-derniere-etape-avant-la-prison_4420550_3224.html");
    StringBuilder content = readPageFromResource("jerome-kerviel-derniere-etape-avant-la-prison_4420550_3224.html");
    LemondeSegmenter seg = new LemondeSegmenter();
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    SimpleItem main = result.get(0);
    assertEquals("Jérôme Kerviel brave les autorités françaises", main.title);
    
    assertEquals(
        "http://s1.lemde.fr/image/2014/05/17/200x200/4420685_4_4c4c_jerome-kerviel-a-la-frontiere-entre-l-i_2af9c05e4c2cbc8f2c0cac1bd818c5d6.jpg",
        main.imageLink);
    
    assertNotNull("Main body is null. Beware, the parser could be failing or the webpage source has changed.", main.body);
    String[] sentences = main.body.split(".");
    
    if (sentences != null && sentences.length > 0) {
      
      assertEquals(
          "Main article's body is different from what it is expected.",
          "L'ancien trader de la Société générale Jérôme Kerviel, symbole des dérives qui ont mis à terre le système bancaire mondial, aujourd'hui métamorphosé en repenti, devait rentrer en France, samedi 17 mai, pour y purger trois ans de prison ferme",
          sentences[0]);
    }
    
    assertEqualTimes(DatatypeConverter.parseDateTime("2014-05-17T11:40:41+02:00").getTime(), main.date);
    
    assertEquals(null, main.author);
    
    assertEquals("166878320861", main.APIObjectID);
    
    assertEquals("http://www.lemonde.fr/societe/reactions/2014/05/17/jerome-kerviel-derniere-etape-avant-la-prison_4420550_3224.html",
        main.threadId);
    
    assertEquals("http://www.lemonde.fr/societe/reactions/2014/05/17/jerome-kerviel-derniere-etape-avant-la-prison_4420550_3224.html",
        main.threadURLs.get(0));
  }
}
