/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;

import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

public class LesechosSegmenterTest extends BaseDocumentSegmenterTest {
  
  @Override
  public void setUp() throws Exception {
    StringBuilder content = readPageFromResource("Hollande et Cameron en désaccord sur l’Europe, Actualités.html");
    this.seg = new LesechosSegmenter();
    HtmlCleaner cleaner = createCleaner();
    this.rootNode = cleaner.clean(content.toString());
  }
  
  @Override
  public void testFindMainTitle() {
    String title = seg.findMainTitle(rootNode);
    assertEquals("Hollande et Cameron en désaccord sur l’Europe", title);
  }
  
  @Override
  public void testFindMainDescripcion() {
    String body = seg.findMainDescripcion(rootNode);
    assertEquals(
        "+ VIDEO. A l’occasion du sommet franco-britannique de ce vendredi, les deux leaders ont cependant annoncé un approfondissement de leurs...",
        body);
  }
  
  @Override
  public void testFindMainAPIObjectID() {
    assertEquals("113827195386672", seg.findMainAPIObjectID(rootNode));
  }
  
  @Override
  public void testFindMainImageLink() {
    assertEquals("http://www.lesechos.fr/medias/2014/01/31/647159_0203284637768_web.jpg", seg.findMainImageLink(rootNode));
  }
  
  @Override
  public void testFindMainAuthor() {
    assertEquals("Nicolas Madelaine", seg.findMainAuthor(rootNode));
  }
  
  @Override
  public void testFindMainDate() {
    Calendar cal = Calendar.getInstance();
    cal.set(2014, 0, 31);
    assertEqualDates(cal.getTime(), seg.findMainDate(rootNode));
  }
  
  @Override
  public void testFindComments() {
    assertEquals(3, seg.findComments(rootNode).length);
  }
  
  @Override
  public void testFindCommentAuthor() {
    TagNode[] comments = seg.findComments(rootNode);
    assertEquals("gcgcgc", seg.findCommentAuthor(comments[0]));
    assertEquals("rabino", seg.findCommentAuthor(comments[1]));
    assertEquals("anonyme-117447", seg.findCommentAuthor(comments[2]));
  }
  
  @Override
  public void testFindCommentDescripcion() {
    TagNode[] comments = seg.findComments(rootNode);
    assertEquals("Il y en a un des deux qui n'y comprend rien du tout , devinez lequel ??????", seg.findCommentDescripcion(comments[0]));
    assertEquals(
        "C'est intéressant de constater que lorsque les désaccords sont identifiés et reconnus on peut quand même travailler et coopérer dans le calme sur d'autres sujets concrets.C'est peut être plus ...",
        seg.findCommentDescripcion(comments[1]));
    assertEquals(
        "Comment pourraient-ils s'entendre? Un va de l'avant, l'autre campe sur ses positions devenues aujourd'hui intenables, socialement parlant surtout. Un attire les investisseurs, les jeunes et talentueux entrepreneurs, l'autre les ...",
        seg.findCommentDescripcion(comments[2]));
  }
  
  @Override
  public void testFindCommentDate() {
    Calendar cal = Calendar.getInstance();
    TagNode[] comments = seg.findComments(rootNode);
    cal.set(2014, 0, 31, 15, 50); // 31/01/2014 à 15:50
    assertEqualTimes(cal.getTime(), seg.findCommentDate(comments[0]));
    cal.set(2014, 0, 31, 17, 07); // 31/01/2014 à 17:07
    assertEqualTimes(cal.getTime(), seg.findCommentDate(comments[1]));
    cal.set(2014, 0, 31, 19, 17); // 31/01/2014 à 19:17
    assertEqualTimes(cal.getTime(), seg.findCommentDate(comments[2]));
  }
  
  @Override
  public void testFindMainTimeIsReal() {
    assertTrue("Time is real should be true. If not, a problem with parsin may be arising.", seg.findMainTimeIsReal(rootNode));
  }
  
}
