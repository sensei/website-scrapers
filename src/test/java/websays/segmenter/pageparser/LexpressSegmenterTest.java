/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;

import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

public class LexpressSegmenterTest extends BaseDocumentSegmenterTest {
  
  @Override
  public void setUp() throws Exception {
    StringBuilder content = readPageFromResource(
        "Manif pour tous: Frigide Barjot explique pourquoi elle ne défilera pas dimanche - L'Express.html", "iso-8859-1");
    this.seg = new LexpressSegmenter();
    HtmlCleaner cleaner = createCleaner();
    this.rootNode = cleaner.clean(content.toString());
  }
  
  @Override
  public void testFindMainTitle() {
    String title = seg.findMainTitle(rootNode);
    assertEquals("Manif pour tous: Frigide Barjot explique pourquoi elle ne défilera pas dimanche", title);
  }
  
  @Override
  public void testFindMainDate() {
    Calendar cal = Calendar.getInstance();
    cal.set(2014, 0, 31, 18, 38);
    assertEqualTimes(cal.getTime(), seg.findMainDate(rootNode));
  }
  
  @Override
  public void testFindMainImageLink() {
    assertEquals(
        "http://static.lexpress.fr/medias_7901/w_1271,h_953,c_crop,x_447,y_0/w_605,h_350,c_fill,g_north/virgine-tellene-alias-frigide-barjot-le-29-mai-2013-a-paris_4045439.jpg",
        seg.findMainImageLink(rootNode));
  }
  
  @Override
  public void testFindMainDescripcion() {
    String body = seg.findMainDescripcion(rootNode);
    assertEquals(
        "Dans un \"manifeste\" adressé au Monde, la leader médiatique des manifestations anti-mariage gay du printemps 2013, Frigide Barjot, a annoncé qu'elle ne participera pas à la Manif pour tous, ce dimanche.",
        body);
  }
  
  @Override
  public void testFindMainAPIObjectID() {
    assertEquals("111055588938671", seg.findMainAPIObjectID(rootNode));
  }
  
  @Override
  public void testFindComments() {
    assertEquals(10, seg.findComments(rootNode).length);
  }
  
  @Override
  public void testFindCommentLikes() {
    TagNode[] comments = seg.findComments(rootNode);
    assertEquals(new Integer(0), seg.findCommentLikes(comments[0]));
    assertEquals(new Integer(2), seg.findCommentLikes(comments[4]));
    assertEquals(new Integer(1), seg.findCommentLikes(comments[5]));
    assertEquals(new Integer(6), seg.findCommentLikes(comments[6]));
    assertEquals(new Integer(4), seg.findCommentLikes(comments[9]));
  }
  
  @Override
  public void testFindCommentImageLink() {
    TagNode[] comments = seg.findComments(rootNode);
    assertEquals("http://www.lexpress.fr/communaute/avatar_8d_ae/w_50,h_50,c_fill,g_center/default-0.png",
        seg.findCommentImageLink(comments[0]));
    assertEquals("http://www.lexpress.fr/communaute/avatar_8d_ae/w_50,h_50,c_fill,g_center/default-0.png",
        seg.findCommentImageLink(comments[4]));
    assertEquals("http://www.lexpress.fr/communaute/avatar_57_79/w_50,h_50,c_fill,g_center/v1388945565/24-novembre_942222.jpg",
        seg.findCommentImageLink(comments[5]));
    assertEquals("http://www.lexpress.fr/communaute/avatar_76_93/w_50,h_50,c_fill,g_center/v1389756596/sylvain56_957790.jpg",
        seg.findCommentImageLink(comments[6]));
    assertEquals("http://www.lexpress.fr/communaute/avatar_8d_ae/w_50,h_50,c_fill,g_center/default-0.png",
        seg.findCommentImageLink(comments[9]));
  }
  
  @Override
  public void testFindCommentDate() {
    Calendar cal = Calendar.getInstance();
    TagNode[] comments = seg.findComments(rootNode);
    cal.set(2014, 0, 31, 23, 01, 38); // 31/01/2014 23:01:38
    assertEqualTimes(cal.getTime(), seg.findCommentDate(comments[0]));
    cal.set(2014, 0, 31, 20, 11, 24); // 31/01/2014 20:11:24
    assertEqualTimes(cal.getTime(), seg.findCommentDate(comments[5]));
    cal.set(2014, 0, 31, 19, 21, 50); // 31/01/2014 19:21:50
    assertEqualTimes(cal.getTime(), seg.findCommentDate(comments[8]));
  }
  
  @Override
  public void testFindCommentAuthor() {
    TagNode[] comments = seg.findComments(rootNode);
    assertEquals("Monique Z", seg.findCommentAuthor(comments[0]));
    assertEquals("yoyolud", seg.findCommentAuthor(comments[4]));
    assertEquals("24 Novembre", seg.findCommentAuthor(comments[5]));
    assertEquals("Sylvain56", seg.findCommentAuthor(comments[6]));
    assertEquals("Benjamin77", seg.findCommentAuthor(comments[9]));
  }
  
  @Override
  public void testFindCommentDescripcion() {
    TagNode[] comments = seg.findComments(rootNode);
    assertEquals(
        "Oui, malheureusement, elle a ouvert une boite de Pandore. Elle a ouvert un boulevard royal aux extrémistes de droite, aux  catholiques conservateurs, traditionalistes et elle a bien réussi à déplacer  un peu plus à droite la droite au profit de l'UMP la plus à droite. Elle porte une lourde responsabilité, sans s'en mordre les doigts. Qui sème le vent, récolte la tempête !",
        seg.findCommentDescripcion(comments[0]));
    assertEquals(
        "Elle est a l origine de la radicalisation et a ete completement depasse par le mouvement, lui donner une tribune  au monde : c est une blague.",
        seg.findCommentDescripcion(comments[4]));
    assertEquals("@emeraude35bis : C'est certain qu'elle en a payé les pots cassés pour les autres !",
        seg.findCommentDescripcion(comments[5]));
    assertEquals("Et alors !!!! c'est quoi le scoop ??", seg.findCommentDescripcion(comments[6]));
    assertEquals(
        "Vous avez remarqué, dans leur manif il y a 90% d'hommes , il serait intéressant d'interviewer leurs femmes pour voir quelle est leur condition à la maison. Ils sont libres de vivre comme au 19ème siècle avec bobonne à la maison mais ils ne peuvent plus l'imposer aux autres comme ils ne peuvent plus imposer non plus leurs bondieuseries ,on n'y croit plus c'est tout . Ils sont minoritaires et ils s'énervent ,le monde leur échappe , tant mieux pour nos libertés . S'ils ont un fils gay ou une fille lesbienne ,que font ils? s'ils les rejettent c'est que leur morale ne s'applique pas aux êtres mais ne concerne qu'une idéologie et qu'elle ne passe pas le parvis des églises.....ils sont donc sans morale humaine ,c'est triste pour eux et leur famille ....Mauriac le grand écrivain catholique en sait quelque chose ; il a du cacher son homosexualité toute sa vie à cause de son milieu..... triste vie",
        seg.findCommentDescripcion(comments[9]));
  }
  
  @Override
  public void testFindMainTimeIsReal() {
    assertTrue("Main page time should be real, but it isn't. Check the segmenter is able to parse and scrape the webpage.",
        seg.findMainTimeIsReal(rootNode));
  }
  
  @Override
  public void testFindCommentTimesIsReal() {
    assertTrue("Comments time should be real, but it isn't. Check the segmenter is able to parse and scrape the webpage.",
        seg.findCommentTimeIsReal(rootNode));
  }
  
}
