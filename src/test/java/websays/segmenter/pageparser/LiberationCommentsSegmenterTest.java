/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: JOrge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.List;

import junit.framework.AssertionFailedError;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class LiberationCommentsSegmenterTest extends SegmenterTest {
  
  @Test
  public void testLiberationCommentsSegmenter() throws MalformedURLException, UnsupportedEncodingException, IOException, Exception,
      AssertionFailedError {
    URL urlComments = new URL("http://bootstrap.liberation.fyre.co/bs3/v3.1/liberation.fyre.co/336643/OTYzOTMz/init");
    StringBuilder contentComments = readPageFromResource("liberationComments.json");
    HTMLDocumentSegmenter segComments = new LiberationCommentsSegmenter();
    List<SimpleItem> resultComments = segComments.segment(contentComments.toString(), urlComments);
    
    int expectedResults = 60; // No of comments
    assertEquals(expectedResults, resultComments.size());
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = resultComments.get(0);
    // for (SimpleItem simpleItem : resultComments) {
    // System.out.println(simpleItem.threadId);
    // }
    assertEquals("120410924", sub1.threadId);
    assertEquals(null, sub1.title);
    assertEquals(
        "On aimerait bien savoir, comment l'enseignement est donné en Asie. Ainsi, \"théorème de Thalès\" = sainte parole de Fou-Tchéou? Ou bien, traduit-on mot à mot ? Idem, les principes ou démonstrations de Descartes, d'Euler, de Newton, comment les nomme-t-on là-bas ? ",
        sub1.body);
    assertEquals("chem", sub1.author);
    assertEquals("http://gravatar.com/avatar/5c4ed3ebeb2a13406952b4205d20a0fc/?s=50&d=http://avatars.fyre.co/a/anon/50.jpg", sub1.imageLink);
    assertEquals(new Integer(1), sub1.likes);
    Calendar cal = Calendar.getInstance();
    cal.set(2013, 11, 05, 19, 21); // Year, Month(zero based), Date, HourOfDay, Minutes
    assertEqualTimes(cal.getTime(), sub1.date); // December 05, 2013 19:21
    assertEquals(urlComments.toString(), sub1.postUrl);
    assertEquals(1, sub1.position);
    assertTrue(sub1.isComment);
  }
}
