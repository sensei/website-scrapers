/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.xml.bind.DatatypeConverter;

import org.htmlcleaner.HtmlCleaner;

public class LiberationSegmenterTest extends BaseDocumentSegmenterTest {
  
  @Override
  public void setUp() throws Exception {
    StringBuilder content = readPageFromResource("Shanghai, l'exception scolaire chinoise - Libération.html");
    this.seg = new LiberationSegmenter();
    HtmlCleaner cleaner = createCleaner();
    this.rootNode = cleaner.clean(content.toString());
  }
  
  @Override
  public void testFindMainTitle() {
    String title = seg.findMainTitle(rootNode);
    assertEquals("Shanghai,  l'exception scolaire chinoise", title);
  }
  
  @Override
  public void testFindMainDescripcion() {
    String body = seg.findMainDescripcion(rootNode);
    assertEquals(
        "Encore une fois, la performance des écoliers de la ville de Shanghai au Programme international pour le suivi des élèves (Pisa) est la plus impressionnante. Comme en 2009, première année où la capitale économique de la Chine (23 millions d’habitants) a participé à ce test de l’OCDE destiné à évaluer les performances respectives des systèmes éducatifs de la planète, ses écoliers monopolisent la première place dans les trois matières testées: maths, science et lecture. Cette performance hors norme, explique à Libération Andreas Schleicher, le directeur du programme Pisa, ne doit toutefois pas être perçue comme un succès emblématique de l’ensemble du pays. «Nous n’avons jamais dit que Shanghai était représentatif de toute la Chine, et ce n’est certainement pas le cas», met-il en garde. Nombreux sont ceux qui avaient pourtant abusivement extrapolé les résultats du Pisa de 2009, à commencer par le président américain Barack Obama qui en avait déduit que «les Etats-Unis sont en danger de rester à la traîne». Ce n’est peut-être pas faux, mais c’est très exagéré. «Comparer les meilleurs étudiants d’un pays (la Chine) aux étudiants ordinaires des autres pays n’a aucun sens», souligne pour sa part Xiong Bingqi, vice-directeur du Centre de recherche sur l’éducation, une ONG de Shanghai. Shanghai est en effet plus une exception que la règle à l’échelle de la Chine. Ses habitants gagnent, en moyenne, plus de deux fois le revenu moyen des Chinois. Cette cité privilégiée est pour l’instant le seul endroit (hormis le cas particulier de Hongkong) où les autorités chinoises autorisent l’OCDE à pratiquer ses tests. Un choix qui provient très certainement d’une volonté de présenter le meilleur du lot. Ses écoles très bien équipées sont assurément peu comparables aux écoles rurales des provinces pauvres du Guizhou ou du Hubei où, faute de mobilier scolaire, les élèves doivent parfois venir avec leurs pupitres et leurs tabourets sur le dos le premier jour de l’année scolaire. L’enseignement primaire et les trois premières années du secondaire sont théoriquement gratuits, mais ce principe n’est pas appliqué partout. En outre, nombre de familles rurales ne peuvent s’acquitter du prix des livres scolaires, et doivent pour cette raison retirer leurs enfants de l’école. L’absentéisme peut atteindre 60% dans les zones les plus démunies. L’immense Chine, devenue depuis une décennie l’un des pays les plus inégalitaires au monde, est par endroits un pays riche, et dans d’autres un authentique pays du tiers-monde. Ces inégalités sont renforcées du fait que l’Etat dépense 18 fois plus pour un écolier shanghaïen que pour ceux des provinces les plus pauvres, selon l’Unesco. Cet élitisme inhérent système d’éducation chinois va bien au-delà encore. Les élèves des zones rurales doivent en effet avoir davantage de points au Gaokao (baccalauréat) que ceux des villes pour entrer dans les grandes universités. Résultat: 84% des diplômés du secondaires de Shanghai vont à l’université, contre 24% seulement en moyenne à l’échelle nationale. Les parents d’élèves shanghaïens se mettent de surcroît en quatre pour payer à leur progéniture activités extrascolaire et cours de rattrapage privés si nécessaire (700 euros en moyenne par an pour les cours de maths et d’anglais). Quand Pékin autorisera-t-il l’OCDE à tester le pays tout entier ? Selon Schleicher, le Pisa testera quatre provinces, en plus de Shanghai, en 2015 «et nous travaillons avec le gouvernement chinois pour élargir notre couverture à l’ensemble du pays sur le long terme». A quels résultats faudra-t-il alors s’attendre ? «Ils seront moins bons au début, mais ils s’amélioreront vite, pense Xiong Bingqi, car les écoles se mettront à bachoter le modèle d’examen du Pisa, et tous les efforts des enseignants se concentreront sur cet examen.» Sans trop le dire, Pékin prépare déjà de manière intensive depuis 2009 une douzaine de provinces aux tests du Pisa. Mais c’est un peu comme si on entraînait des enfants à faire un numéro de cirque, d’après cet expert. «En Chine, les examens ne reflètent absolument pas la personnalité des élèves, alors qu’en théorie ils sont faits pour ça», analyse Xiong Bingqi. «Il ne faut pas oublier, dit-il, que la Chine est \"le pays des examens\", et que l’enseignement est exclusivement tourné vers la réussite de tests standardisés. Cette approche qui se concentre sur les connaissances de base ignore le développement personnel des élèves, qui de ce fait manquent d’esprit d’initiative, de curiosité et de capacité d’autonomie.» Pour en savoir plus sur ce dossier, notre livre numérique: «Ecole, la nouvelle» méthode» avec une contribution de Vincent Peillon. Gratuit pour les abonnés.  ",
        body);
  }
  
  @Override
  public void testFindMainImageLink() {
    assertEquals("http://md1.libe.com/photo/601720-chineecole.jpg?modified_at=1386094755", seg.findMainImageLink(rootNode));
  }
  
  @Override
  public void testFindMainAuthor() {
    assertEquals("Philippe Grangereau", seg.findMainAuthor(rootNode));
  }
  
  @Override
  public void testFindMainDate() {
    assertEqualTimes(DatatypeConverter.parseDateTime("2013-12-04T07:16:42").getTime(), seg.findMainDate(rootNode));
  }
  
  @Override
  public void testFindMainThreadId() {
    assertEquals("963933", seg.findMainThreadId(rootNode));
  }
  
  @Override
  public void testFindMainThreadURLs() {
    assertEquals("http://bootstrap.liberation.fyre.co/bs3/v3.1/liberation.fyre.co/336643/OTYzOTMz/init", seg.findMainThreadURLs(rootNode)
        .get(0));
  }
  
  @Override
  public void testFindMainTimeIsReal() {
    assertTrue("Main time should be always real. As it is not, check the segmenting process.", seg.findMainTimeIsReal(rootNode));
  }
  
}
