/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2016. All rights reserved. http://websays.com )
 *
 * Primary Author: juanfra
 * Contributors:
 * Date: May 3, 2016
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;

import java.net.URL;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

/**
 * @author juanfra Test the scraping functionalities using locally downloaded content.
 */
public class LinkedInSegmenterTest extends SegmenterTest {

  /**
   * Tests the main page scraping. From the main page, extracts the companyid and builds a SimpleItem without information but with a URL that points
   * to the first page of updates.
   */
  @Test
  public void testMainPageScrapping() throws Exception {
    TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
    URL url = new URL("https://www.linkedin.com/company/caixabank");
    // StringBuilder content = readPageFromResource("El timo de Legálitas.htm");
    StringBuilder content = readPageFromResource("linkedinMainPage.txt");

    Calendar cal = Calendar.getInstance();
    HTMLDocumentSegmenter seg = new LinkedInSegmenter();

    List<SimpleItem> result = seg.segment(content.toString(), url);

    assertEquals("Not the same SimpleItems amount got that expected.", 1, result.size());
    /* Test that the url to the first updates page is correctly build. */

    SimpleItem linkToUpdates = result.get(0);
    assertEquals("The only SimpleItem returned must be fake", true, linkToUpdates.fakeItem);

    assertEquals("Malformed updates URL", "https://www.linkedin.com/biz/7848/feed?start=0", linkToUpdates.threadURLs.get(0));

  }

  /**
   * Tests the updates page scraping. From a updates page, scraps all the information and returns it. It also returns a link to the next updates page.
   */
  @Test
  public void testUpdatesPageScrapping() throws Exception {
    TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
    URL url = new URL("https://www.linkedin.com/biz/7848/feed?start=0");
    StringBuilder content = readPageFromResource("linkedinUpdatesPage.txt");

    HTMLDocumentSegmenter seg = new LinkedInSegmenter();

    List<SimpleItem> result = seg.segment(content.toString(), url);

    assertEquals("Erroneus number of results", 15, result.size());
    SimpleItem currResult = result.get(0);

    assertEquals("The update title is wrong", "Resultados de CaixaBank 1T2016", currResult.title);

    assertEquals("The update date is wrong", 1461824174252L, currResult.date.getTime());
    assertEquals("The update date is real is wrong", true, currResult.timeIsReal);
    assertEquals(
        "The update body is wrong",
        "CaixaBank obtiene un beneficio de 273 millones durante el primer trimestre de 2016, apoyado en los ingresos bancarios, la contención de los gastos y la reducción de las dotaciones.Los principales datos de los resultados del primer trimestre los... https://blog.caixabank.es/2016/04/resultados-de-caixabank-1t2016.html",
        currResult.body);
    assertEquals("The update author is wrong", "CaixaBank", currResult.author);
    assertEquals("The update url is wrong", "https://www.linkedin.com/company/caixabank", currResult.itemUrl);
    assertEquals("The update url is wrong", "https://www.linkedin.com/company/caixabank", currResult.postUrl);
    assertEquals("The update position is wrong", 1, currResult.position);
    assertEquals("The update number of likes is wrong", new Integer(50), currResult.likes);
    assertEquals("The update number of comments is wrong", new Integer(0), currResult.comments);

    currResult = result.get(1);
    assertEquals("The update title is wrong", "Smartphones: ventajas de usar la huella dactilar", currResult.title);

    assertEquals("The update date is wrong", 1461751809434L, currResult.date.getTime());
    assertEquals("The update date is real is wrong", true, currResult.timeIsReal);
    assertEquals(
        "The update body is wrong",
        "Ya forma parte de nuestras vidas. La telefonía móvil y otros dispositivos electrónicos van sustituyendo progresivamente las contraseñas por la huella dactilar. Hoy ya se pueden encontrar gran variedad de terminales de gama media o media-alta con... https://blog.caixabank.es/2016/04/smartphones-ventajas-de-usar-la-huella-dactilar.html",
        currResult.body);
    assertEquals("The update author is wrong", "CaixaBank", currResult.author);
    assertEquals("The update url is wrong", "https://www.linkedin.com/company/caixabank", currResult.itemUrl);
    assertEquals("The update url is wrong", "https://www.linkedin.com/company/caixabank", currResult.postUrl);
    assertEquals("The update position is wrong", 1, currResult.position);
    assertEquals("The update number of likes is wrong", new Integer(21), currResult.likes);
    assertEquals("The update number of comments is wrong", new Integer(0), currResult.comments);

    currResult = result.get(2);
    String threadId = currResult.threadId;
    assertEquals("The update title is wrong", "¿Qué influye en el precio de la vivienda?", currResult.title);

    assertEquals("The update date is wrong", 1461573886857L, currResult.date.getTime());
    assertEquals("The update date is real is wrong", true, currResult.timeIsReal);
    assertEquals(
        "The update body is wrong",
        "“El precio de la vivienda ha subido un 2,2% en los tres primeros meses de este año”. Titulares como estos pueden provocar más de un sobresalto en nuestra planificación económica, ya que el precio de la vivienda, bien sea por venta o por opciones... https://blog.lacaixa.es/2016/04/que-influye-en-el-precio-de-la-vivienda.html",
        currResult.body);
    assertEquals("The update author is wrong", "CaixaBank", currResult.author);
    assertEquals("The update url is wrong", "https://www.linkedin.com/company/caixabank", currResult.itemUrl);
    assertEquals("The update url is wrong", "https://www.linkedin.com/company/caixabank", currResult.postUrl);
    assertEquals("The update position is wrong", 1, currResult.position);
    assertEquals("The update number of likes is wrong", new Integer(44), currResult.likes);
    assertEquals("The update number of comments is wrong", new Integer(1), currResult.comments);

    currResult = result.get(3);
    assertEquals("The comment thread id is wrong", threadId, currResult.threadId);
    assertEquals("The comment title is wrong", null, currResult.title);

    Calendar expectedDate = Calendar.getInstance();
    expectedDate.set(Calendar.HOUR, 0);
    expectedDate.set(Calendar.MINUTE, 0);
    expectedDate.set(Calendar.SECOND, 0);
    expectedDate.set(Calendar.MILLISECOND, 0);
    expectedDate.add(Calendar.DAY_OF_MONTH, -6);
    Calendar realDate = Calendar.getInstance();
    realDate.setTimeInMillis(currResult.date.getTime());
    realDate.set(Calendar.HOUR, 0);
    realDate.set(Calendar.MINUTE, 0);
    realDate.set(Calendar.SECOND, 0);
    realDate.set(Calendar.MILLISECOND, 0);

    assertEquals("The comment date is wrong", expectedDate, realDate);
    assertEquals("The comment date is real is wrong", false, currResult.timeIsReal);
    assertEquals(
        "The comments body is wrong",
        "Es un buen punto a tener en cuenta pero... ¿Porqué despreciarla evolución del resto de componentes? Por otro lado hay bolsas de suelo compradas \"caras\" ¿Cómo afectará al precio de la vivienda? ¿Qué precio hay que tener en cuenta? ¿El de las compras de suelo actuales o el del suelo sobre qué se está construyendo? ",
        currResult.body);
    assertEquals("The comment position is wrong", 1, currResult.position);

  }

  /**
   * Tests linkedIn updates that return null to the firs parser version.
   *
   * @throws Exception
   */
  @Test
  public void testSomeDifficultUpdates() throws Exception {

    TimeZone.setDefault(TimeZone.getTimeZone("GMT"));

    URL url = new URL("https://www.linkedin.com/biz/7848/feed?start=20");
    StringBuilder content = readPageFromResource("linkedInPublicTest2.html");

    Calendar cal = Calendar.getInstance();
    HTMLDocumentSegmenter seg = new LinkedInSegmenter();

    List<SimpleItem> result = seg.segment(content.toString(), url);

    assertEquals("Erroneus number of results", 14, result.size());
    SimpleItem currResult = result.get(0);

    assertEquals(
        "The update title is wrong",
        "El impacto de las nuevas tecnologías en el mercado laboralLas nuevas tecnologías han provocado que estemos inmersos en lo que algunos expertos vaticinan como el principio de la “Cuarta Revolución Industrial”",
        currResult.title);
    assertEquals("The update image is wrong", "https://image-store.slidesharecdn.com/b550ca03-38f4-47f8-9294-2ffcccae1a25-large.jpeg",
        currResult.imageLink);

    assertEquals("The update date is wrong", 1457535632352L, currResult.date.getTime());
    assertEquals("The update date is real is wrong", true, currResult.timeIsReal);
    assertEquals(
        "The update body is wrong",
        "El impacto de las nuevas tecnologías en el mercado laboralLas nuevas tecnologías han provocado que estemos inmersos en lo que algunos expertos vaticinan como el principio de la “Cuarta Revolución Industrial”. Este tipo de avances está propiciando... https://lnkd.in/eE378vp null",
        currResult.body);
    assertEquals("The update author is wrong", "CaixaBank", currResult.author);
    assertEquals("The update url is wrong", "https://www.linkedin.com/company/caixabank", currResult.itemUrl);
    assertEquals("The update url is wrong", "https://www.linkedin.com/company/caixabank", currResult.postUrl);
    assertEquals("The update position is wrong", 1, currResult.position);
    assertEquals("The update number of likes is wrong", new Integer(125), currResult.likes);
    assertEquals("The update number of comments is wrong", new Integer(1), currResult.comments);

    /* This is a comment */
    currResult = result.get(1);
    assertEquals(
        "The comment body is wrong",
        "porqué cuando se habla de machine learning, se piensa en un robot que sustituye mano de obra directa en entornos industriales? las rutinas, algoritmos y protocolos parametrizados ordenadores no harán desaparecer muchos puestos financieros, comerciales y/o administrativos también y antes?!",
        currResult.body);

    assertEquals("The comment date is real is wrong", false, currResult.timeIsReal);
    assertEquals("The comment author is wrong", "Ryan Khouja", currResult.author);
    assertEquals("The comment url is wrong", "https://www.linkedin.com/company/caixabank", currResult.itemUrl);
    assertEquals("The comment position is wrong", 1, currResult.position);

    /* This is a new update */
    currResult = result.get(2);
    assertEquals(
        "The update title is wrong",
        "Isabel García es Directora de Banca de Empresas de CaixaBank en Oviedo. Su principal misión es ayudar a las empresas a crecer financieramente y a que, además, entiendan la necesidad y el valor de comprometerse con la responsabilidad social... https://blog.lacaixa.es/2016/03/caixabank-una-banca-comprometida.html",
        currResult.body);
    assertEquals(
        "The update image is wrong",
        "https://media.licdn.com/media-proxy/ext?w=180&h=110&f=c&hash=Z70jL%2BvOYTF6%2FVCFOMDW75d3HuY%3D&ora=1%2CaFBCTXdkRmpGL2lvQUFBPQ%2CxAVta5g-0R6onxQdjBAg46-ItU7-4VQIQ4vQB3O-HmX2pYTUI2jrecbZZrKjuzhIKV5QzldjDLCoHXCjTc7pb9O7LNtsi5a6f46-dgA",
        currResult.imageLink);

    assertEquals("The update date is wrong", 1457004544566L, currResult.date.getTime());
    assertEquals("The update date is real is wrong", true, currResult.timeIsReal);
    assertEquals(
        "The update body is wrong",
        "Isabel García es Directora de Banca de Empresas de CaixaBank en Oviedo. Su principal misión es ayudar a las empresas a crecer financieramente y a que, además, entiendan la necesidad y el valor de comprometerse con la responsabilidad social... https://blog.lacaixa.es/2016/03/caixabank-una-banca-comprometida.html",
        currResult.body);
    assertEquals("The update author is wrong", "CaixaBank", currResult.author);
    assertEquals("The update url is wrong", "https://www.linkedin.com/company/caixabank", currResult.itemUrl);
    assertEquals("The update url is wrong", "https://www.linkedin.com/company/caixabank", currResult.postUrl);
    assertEquals("The update position is wrong", 1, currResult.position);
    assertEquals("The update number of likes is wrong", new Integer(103), currResult.likes);
    assertEquals("The update number of comments is wrong", new Integer(0), currResult.comments);

  }

}
