/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;

import java.net.URL;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class MeneameSegmenterTest extends SegmenterTest {
  
  /**
   * Test of segment method, of class MeneameSegmenter.
   */
  @Test
  public void testSegment() throws Exception {
    TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
    URL url = new URL("http://www.meneame.net/story/el-timo-de-legalitas");
    // StringBuilder content = readPageFromResource("El timo de Legálitas.htm");
    StringBuilder content = readPageFromResource("meneameTimoLegalitas2.htm");
    
    Calendar cal = Calendar.getInstance();
    HTMLDocumentSegmenter seg = new MeneameSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 38; // No of comments+ basic details item which
    // you expect in the page
    assertEquals(expectedResults, result.size());
    
    // ///////// Test Main item //////////////
    SimpleItem main = result.get(0);// The main details item at
    // index 0 (first
    // element)
    assertEquals("El timo de Legálitas", main.title);
    assertEquals(
        "Copio y pego: ...Os pongo en antecedentes. Legálitas se llama así, y no Éticas, por algo. En los últimos meses no tenía más contacto con ellos que las llamadas comerciales ofreciéndome nuevas coberturas, y que sólo servían para darme cuenta de que el servicio por el que estaba pagando no incluía dichas coberturas...",
        main.body);
    assertEquals("Cassiopeia", main.author);
    assertEquals(null, main.authorLocation);
    assertEquals(new Integer(37), main.comments);// 'comentarios' value
    assertEquals(new Integer(209), main.likes);// 'meneos' value
    assertEquals(null, main.votes);
    cal.set(2013, 6, 8, 13, 11);// Year, Month(zero based), Date, HourOfDay,
    // Minutes
    assertEqualTimes(cal.getTime(), main.date);
    assertEquals(url.toString(), main.postUrl);
    assertEquals(null, main.itemUrl);
    assertEquals(1, main.position);
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = result.get(1);
    assertEquals(null, sub1.title);
    assertEquals("el timo de cualquier abogado en general.", sub1.body);
    assertEquals("pinipon", sub1.author);
    assertEquals(null, sub1.authorLocation);
    assertEquals(new Integer(9), sub1.votes);
    assertEquals(new Integer(-45), sub1.likes); // 'karma' value
    assertEquals(null, sub1.comments);
    cal.set(2013, 6, 8, 13, 24);// Year, Month(zero based), Date, HourOfDay,
    // Minutes
    assertEqualTimes(cal.getTime(), sub1.date);
    assertEquals(url.toString(), sub1.postUrl);
    assertEquals("http://www.meneame.net/c/13148348", sub1.itemUrl);
    assertEquals(2, sub1.position);
    
    // ///////// Test Second comment item //////////////
    SimpleItem sub2 = result.get(2);
    assertEquals(null, sub2.title);
    assertEquals(
        "¿Cualquier abogado tiene un servicio de atención telefónica con el que entorpecen bajo todo tipo de excusas que te des de baja de un servicio de consulta legal? No sabía yo...",
        sub2.body);
    assertEquals("JohnBoy", sub2.author);
    assertEquals(null, sub2.authorLocation);
    assertEquals(new Integer(6), sub2.votes); // 'votes' value
    assertEquals(new Integer(51), sub2.likes); // 'karma' value
    assertEquals(null, sub2.comments);
    cal.set(2013, 6, 8, 13, 30);// Year, Month(zero based), Date, HourOfDay,
    // Minutes
    assertEqualTimes(cal.getTime(), sub2.date);
    assertEquals(url.toString(), sub2.postUrl);
    assertEquals("http://www.meneame.net/c/13148395", sub2.itemUrl);
    assertEquals(3, sub2.position);
    
    // ///////// Test Third comment item //////////////
    SimpleItem sub3 = result.get(3);
    assertEquals(null, sub3.title);
    assertEquals(
        "¡Una advertencia muy oportuna! Casualmente tenía pensado ponerme en contacto con ellos para contratar sus servicios. Visto lo visto mejor no, por si hay más",
        sub3.body);
    assertEquals("Athreides", sub3.author);
    assertEquals(null, sub3.authorLocation);
    assertEquals(new Integer(14), sub3.votes);// 'votes' value
    assertEquals(new Integer(146), sub3.likes); // 'karma' value
    assertEquals(null, sub3.comments);
    cal.set(2013, 6, 8, 13, 33);// Year, Month(zero based), Date, HourOfDay,
    // Minutes
    assertEqualTimes(cal.getTime(), sub3.date);
    assertEquals(url.toString(), sub3.postUrl);
    assertEquals("http://www.meneame.net/c/13148413", sub3.itemUrl);
    assertEquals(4, sub3.position);
    
    // ///////// Test Forth comment item //////////////
    SimpleItem sub4 = result.get(4);
    assertEquals(null, sub4.title);
    assertEquals("me refiero a que cualquier abogado tima.", sub4.body);
    assertEquals("pinipon", sub4.author);
    assertEquals(null, sub4.authorLocation);
    assertEquals(new Integer(6), sub4.votes);// 'votes' value
    assertEquals(new Integer(-15), sub4.likes); // 'karma' value
    assertEquals(null, sub4.comments);
    cal.set(2013, 6, 8, 13, 33);// Year, Month(zero based), Date, HourOfDay,
    // Minutes
    assertEqualTimes(cal.getTime(), sub4.date);
    assertEquals(url.toString(), sub4.postUrl);
    assertEquals("http://www.meneame.net/c/13148419", sub4.itemUrl);
    assertEquals(5, sub4.position);
    
    // ///////// Test Last comment item //////////////
    SimpleItem sub37 = result.get(37);
    assertEquals(null, sub37.title);
    assertEquals(
        "Apoyo tu postura por completo. Aquí hay mucho LISTO que TODO LO HACE BIEN, se lee y comprende de principio a fín todos los contratos, y nunca le estafan, porque son de otra pasta, son los \"enteraos\". Bien, pues lo que cuenta me parece absolutamente normal y digno de ser publicado y divulgado. ¿Que con la ley en la mano (y ya sabemos quien hace las leyes) no es delito?Seguramente, pero no dista mucho de ser una estafa como la tan traída y llevada de las preferentes. El concepto es muy básico, voy a hacer todo lo posible, incluyendo trucos de todo tipo para quitarte el dinero, en lugar de ceñirnos a algo tan simple como te doy un servicio y te cobro si estas satisfecho, si no, adiós.",
        sub37.body);
    assertEquals("bypper", sub37.author);
    assertEquals(null, sub37.authorLocation);
    assertEquals(new Integer(0), sub37.votes);// 'votes' value
    assertEquals(new Integer(9), sub37.likes); // 'karma' value
    assertEquals(null, sub37.comments);
    cal.set(2013, 6, 9, 10, 2);// Year, Month(zero based), Date, HourOfDay,
    // Minutes
    assertEqualTimes(cal.getTime(), sub37.date);
    assertEquals(url.toString(), sub37.postUrl);
    assertEquals("http://www.meneame.net/c/13153738", sub37.itemUrl);
    assertEquals(38, sub37.position);
    
  }
  
  /**
   * Test of segment method, of class MeneameSegmenter. This test, checks the changes done due to changes in the maneame page between 2014 and 2016
   * Strange enough, the downloaded version of the page has less information than the "live" version so the test is less complete that what the
   * segmenter is able to scrap from the page.
   */
  @Test
  public void testSegment2016() throws Exception {
    TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
    URL url = new URL("https://www.meneame.net/story/informe-jp-morgan-certifica-como-supermercados-dia-exprimen");
    // StringBuilder content = readPageFromResource("El timo de Legálitas.htm");
    StringBuilder content = readPageFromResource("meneame2016.html");
    
    Calendar cal = Calendar.getInstance();
    HTMLDocumentSegmenter seg = new MeneameSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 49; // No of comments+ basic details item which
    // you expect in the page
    assertEquals(expectedResults, result.size());
    
    // ///////// Test Main item //////////////
    SimpleItem main = result.get(0);// The main details item at
    // index 0 (first
    // element)
    assertEquals("Un informe de JP Morgan certifica como Supermercados DIA gana dinero a costa de exprimir a sus franquiciados", main.title);
    assertEquals(
        "El banco de inversión norteamericano se reunió con asociaciones de afectados por las franquicias de DIA ante las dudas de cómo estaban operando los supermercados en su trato con sus socios franquiciados.",
        main.body);
    // assertEquals("Cassiopeia", main.author);
    assertEquals(null, main.authorLocation);
    assertEquals(new Integer(48), main.comments);// 'comentarios' value
    assertEquals(new Integer(708), main.likes);// 'meneos' value
    assertEquals(null, main.votes);
    cal.set(2015, 10, 24, 8, 41);// Year, Month(zero based), Date, HourOfDay,
    // Minutes
    // assertEqualTimes(cal.getTime(), main.date);
    assertEquals(url.toString(), main.postUrl);
    assertEquals(null, main.itemUrl);
    assertEquals(1, main.position);
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = result.get(1);
    assertEquals(null, sub1.title);
    assertEquals(
        "DIA está cotizada y por tanto se somete al escrutinio de casas de análisis y banca de inversión. Por ejemplo Mercadona tiene básicamente un accionista y por ello es mucho menos transparente y la banca de inversión nada puede decir sobre su negocio.",
        sub1.body);
    assertEquals("Caramierder", sub1.author);
    assertEquals(null, sub1.authorLocation);
    assertEquals(new Integer(30), sub1.votes);
    assertEquals(new Integer(266), sub1.likes); // 'karma' value
    assertEquals(null, sub1.comments);
    cal.set(2015, 10, 24, 8, 59);// Year, Month(zero based), Date, HourOfDay,
    // Minutes
    assertEqualTimes(cal.getTime(), sub1.date);
    assertEquals(url.toString(), sub1.postUrl);
    assertEquals("http://www.meneame.net/c/18216067", sub1.itemUrl);
    assertEquals(2, sub1.position);
    
    // ///////// Test Last comment item //////////////
    SimpleItem subLast = result.get(result.size() - 1);
    assertEquals(null, subLast.title);
    assertEquals(
        "Fui trabajador en una franquicia.Básicamente el propietario duerme dentro del local para poder sacarle margen. Todas las horas extra que les puedan hacer los empleados de gratis, inventarios, dias festivos no pagados, son criticos para que la franquicia funcione.Les alquilan el horno de pan, les realquilan la RDSI a 70 pavos al mes, les alquilan las TPV's, les llegan las mercancias con menor prioridad que a las tiendas propias.. Les mandan genero que \"a saber si lo venden\" como…",
        subLast.body);
    assertEquals("Roy_López", subLast.author);
    assertEquals(null, subLast.authorLocation);
    assertEquals(new Integer(3), subLast.votes);// 'votes' value
    assertEquals(new Integer(34), subLast.likes); // 'karma' value
    assertEquals(null, subLast.comments);
    cal.set(2015, 10, 24, 18, 33);// Year, Month(zero based), Date, HourOfDay,
    // Minutes
    assertEqualTimes(cal.getTime(), subLast.date);
    assertEquals(url.toString(), subLast.postUrl);
    assertEquals("http://www.meneame.net/c/18221993", subLast.itemUrl);
    assertEquals(result.size(), subLast.position);
    
  }
}