/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;

import java.net.URL;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class MetroNewsCommentsSegmenterTest extends SegmenterTest {
  
  @Test
  public void testCommentSegment() throws Exception {
    
    URL url = new URL(
        "http://disqus.com/embed/comments/?disqus_version=4cacc63e&f=metrofrance&t_i=/x/metro/2014/01/15/u3yuyDdYjIvs/index.xml&t_u&s_o=default");
    StringBuilder content = readPageFromResource("metronewscomments.html");
    HTMLDocumentSegmenter seg = new MetroNewsCommentsSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 50; // No of comments
    Calendar cal = Calendar.getInstance();
    assertEquals(expectedResults, result.size());
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = result.get(0);
    assertEquals(null, sub1.title);
    assertEquals("Encore de la poudre aux yeux pour faire passer le temps, et masquer des problèmes plus graves !", sub1.body);
    assertEquals("Lili Nocode", sub1.author);
    assertEquals(null, sub1.authorLocation);
    assertEquals(new Integer(17), sub1.likes);
    assertEquals(null, sub1.votes);
    assertEquals(
        "http://disqus.com/embed/comments/?disqus_version=4cacc63e&f=metrofrance&t_i=/x/metro/2014/01/15/u3yuyDdYjIvs/index.xml&t_u&s_o=default#post-1202643495",
        sub1.itemUrl);
    cal.set(2014, 0, 15, 21, 45);
    assertEqualTimes(cal.getTime(), sub1.date);
    assertEquals(url.toString(), sub1.postUrl);
    assertEquals("metrofrance-/x/metro/2014/01/15/u3yuyDdYjIvs/index.xml", sub1.threadId);
    assertEquals(1, sub1.position);
    
  }
}