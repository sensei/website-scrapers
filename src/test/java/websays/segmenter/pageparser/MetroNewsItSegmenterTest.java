/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;

import org.htmlcleaner.HtmlCleaner;

public class MetroNewsItSegmenterTest extends BaseDocumentSegmenterTest {
  
  @Override
  public void setUp() throws Exception {
    StringBuilder content = readPageFromResource("No alla ciclabile farsa sulla via Nomentana.html");
    this.seg = new MetroNewsItSegmenter();
    HtmlCleaner cleaner = createCleaner();
    this.rootNode = cleaner.clean(content.toString());
  }
  
  @Override
  public void testFindMainTitle() {
    String title = seg.findMainTitle(rootNode);
    assertEquals("“No alla ciclabile farsa sulla via Nomentana”", title);
  }
  
  @Override
  public void testFindMainImageLink() {
    assertEquals("http://www.metronews.it/redazione/./documenti/013014908nomentana-11-04-13-2.jpg", seg.findMainImageLink(rootNode));
  }
  
  @Override
  public void testFindMainDescripcion() {
    String body = seg.findMainDescripcion(rootNode);
    assertEquals("Associazioni dei ciclisti in rivolta contro il progetto del Campidoglio: \"Così è meglio non farla\"", body);
  }
  
  @Override
  public void testFindMainDate() {
    Calendar cal = Calendar.getInstance();
    cal.set(2014, 0, 30, 21, 06);// 30/01/2014 21:06
    assertEqualTimes(cal.getTime(), seg.findMainDate(rootNode));
  }
  
  @Override
  public void testFindMainTimeIsReal() {
    assertTrue(seg.findMainTimeIsReal(rootNode));
  }
  
}
