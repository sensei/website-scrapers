/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.htmlcleaner.HtmlCleaner;

public class MetroNewsSegmenterTest extends BaseDocumentSegmenterTest {
  
  @Override
  public void setUp() throws Exception {
    StringBuilder content = readPageFromResource("François Hollande - Julie Gayet : Closer promet de nouvelles révélations vendredi – metronews.html");
    this.seg = new MetroNewsSegmenter();
    HtmlCleaner cleaner = createCleaner();
    this.rootNode = cleaner.clean(content.toString());
  }
  
  @Override
  public void testFindMainTitle() {
    String title = seg.findMainTitle(rootNode);
    assertEquals("François Hollande - Julie Gayet : Closer promet de nouvelles révélations vendredi", title);
  }
  
  @Override
  public void testFindMainDate() {
    assertEqualTimes(DatatypeConverter.parseDateTime("2014-01-16T11:45:00Z").getTime(), seg.findMainDate(rootNode));
  }
  
  @Override
  public void testFindMainImageLink() {
    assertEquals("http://www.metronews.fr/_internal/cimg!0/k6odloir4hd58kc60r48j7u2qvbvjcp/000_Par7516974.jpeg",
        seg.findMainImageLink(rootNode));
  }
  
  @Override
  public void testFindMainDescripcion() {
    String body = seg.findMainDescripcion(rootNode);
    assertEquals(
        "Eteindre l'incendie, tout de suite. Au lendemain de l'intervention de François Hollande, qui a coupé court à toutes les questions sur sa liaison supposée avec Julie Gayet, c'est le gouvernement qui a dû faire face mercredi à un nouveau départ de feu. Le Canard Enchaîné a en effet révélé que le ministère de la Culture avait nommé la comédienne au jury 2014 de la prestigieuse villa Médicis de Rome. De quoi faire naître aussitôt des soupçons de connivence… Dès mercredi matin, les services d'Aurélie Filippetti ont donc annoncé que la nomination était annulée. Fermez le ban.",
        body);
  }
  
  @Override
  public void testFindMainAuthor() {
    assertEquals("Thomas Vampouille", seg.findMainAuthor(rootNode));
  }
  
  @Override
  public void testFindMainThreadId() {
    assertEquals("metrofrance-/x/metro/2014/01/15/u3yuyDdYjIvs/index.xml", seg.findMainThreadId(rootNode));
  }
  
  @Override
  public void testFindMainThreadURLs() {
    List<String> urls = seg.findMainThreadURLs(rootNode);
    assertEquals(1, urls.size());
    assertEquals(
        "http://disqus.com/embed/comments/?disqus_version=4cacc63e&f=metrofrance&t_i=/x/metro/2014/01/15/u3yuyDdYjIvs/index.xml&t_u&s_o=default",
        urls.get(0));
  }
}
