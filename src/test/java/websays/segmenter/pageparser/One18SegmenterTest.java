/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;

import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class One18SegmenterTest extends SegmenterTest {
  
  /**
   * Test of segment method, of class One18Segmenter.
   */
  @Test
  public void testSegment() throws Exception {
    
    String urlStr = "http://11870.com/pro/vips-madrid-2";
    URL url = new URL(urlStr + "/date");
    // StringBuilder content = readPageFromResource("Vips Madrid.htm");
    StringBuilder content = readPageFromResource("vips-madrid-2.html");
    
    Date current = new Date();
    Calendar cal = Calendar.getInstance();
    HTMLDocumentSegmenter seg = new One18Segmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 11; // No of comments+ basic details item which
    // you expect in the page
    assertEquals(expectedResults, result.size());
    
    // ///////// Test Main item //////////////
    SimpleItem main = result.get(0);// The main details item at
    // index 0 (first
    // element)
    assertEquals("Vips", main.title);
    assertEquals("Calle de Velázquez 136 Madrid, Madrid provincia, España", main.body);
    assertEquals("Antón", main.author);
    assertEquals(null, main.authorLocation);
    assertEquals(new Integer(23), main.reviews);
    assertEquals("Native score", null, main.getScore_native());
    assertEquals("Websays score", null, main.getScore_websays());
    
    assertEquals(null, main.votes);
    assertEqualDates(null, main.date);
    assertEquals(urlStr, main.itemUrl);
    assertEquals(urlStr, main.postUrl);
    assertEquals(1, main.position);
    
    int commentItemIndex = 1;
    // Test first comment
    SimpleItem sub = result.get(commentItemIndex++);
    assertEquals(null, sub.title);
    assertEquals(
        "¿Quién no ha estado alguna vez en Vips? Una pregunta boba...quiero decir, ¿quién no ha probado la nueva carta del Vips?El miércoles pasado el Grupo Vips nos reunió a 7 personas para que disfrutásemos del Primer Taller Maestro Hamburguesero, y claro, yo que sólo sé hacer fajitas y una tortilla de patata de chuparse los dedos, lo de hacer hamburguesas era todo un reto para mi.",
        sub.body);
    assertEquals("Comiendo Se Entiende La Gente", sub.author);
    assertEquals(null, sub.authorLocation);
    assertEquals(null, sub.reviews);
    assertEquals("Native score", new Integer(40), sub.getScore_native());// rating
    assertEquals("Websays score", new Integer(80), sub.getScore_websays());// rating
    assertEquals(new Integer(1), sub.votes);// like
    
    cal.set(2012, 5, 8);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), sub.date);
    assertEquals("http://11870.com/pro/vips-madrid-2/comiendoseentiendelagente", sub.itemUrl);
    assertEquals(url.toString(), sub.postUrl);
    assertEquals(commentItemIndex, sub.position);
    
    // ///////// Test First comment item //////////////
    sub = result.get(commentItemIndex++);
    assertEquals("el vips mas grande que conozco", sub.title);
    assertEquals(
        "La ultima vez que fui, enero 2012, y sigue siendo el vips que mas me gusta, el mas grande y con mas mesas con sofa."
            + "Cambiaron la comida hace poco y creo que a mejor. Por lo menos las quesadillas con pollo estaban buenas, para ser vips claro. Aunque la salsa especial vips deja mucho que desear",
        sub.body);
    assertEquals("Cristina Bertrand Pire", sub.author);
    assertEquals(null, sub.authorLocation);
    assertEquals(null, sub.reviews);
    assertEquals("Native score", new Integer(30), sub.getScore_native());// rating
    assertEquals("Websays score", new Integer(60), sub.getScore_websays());// rating
    assertEquals(null, sub.votes);// like
    
    cal.set(2012, 1, 25);// Year, Month(zero based), Date
    
    assertEqualDates(cal.getTime(), sub.date);
    assertEquals("http://11870.com/pro/vips-madrid-2/cristina_bertrand", sub.itemUrl);
    assertEquals(url.toString(), sub.postUrl);
    assertEquals(commentItemIndex, sub.position);
    // ///////// Test Second comment item //////////////
    
    // ///////// Test Third comment item //////////////
    sub = result.get(commentItemIndex++);
    assertEquals("Muy bueno todo pero muy caro", sub.title);
    assertEquals(
        "Lo tenía como un sitio popular para ir a comer y disfrutar de un buen rato pero los precios me echan para atrás. Pagar casi 10€ por un bocadillo no me hace nada de gracia, aunque el bocadillo esté excelente.",
        sub.body);
    assertEquals("Javier", sub.author);
    assertEquals(null, sub.authorLocation);
    assertEquals(null, sub.reviews);
    assertEquals("Native score", new Integer(30), sub.getScore_native());// rating
    assertEquals("Websays score", new Integer(60), sub.getScore_websays());// rating
    assertEquals(null, sub.votes);// like
    
    cal.setTime(current);// 7 months ago
    // cal.set(Calendar.YEAR, 2012);
    cal.add(Calendar.MONTH, -11);// 11 months ago
    // cal.set(Calendar.DATE, 1);// considering day as the first day of month
    assertEqualDates(cal.getTime(), sub.date);
    assertEquals("http://11870.com/pro/vips-madrid-2/javierarraez", sub.itemUrl);
    assertEquals(url.toString(), sub.postUrl);
    assertEquals(commentItemIndex, sub.position);
    // ///////// Test Forth comment item //////////////
    sub = result.get(commentItemIndex++);
    assertEquals("Comer a cualquier hora", sub.title);
    assertEquals(
        "Sigue siendo una referencia para comer a cualquier hora pero aun con el ultimo lavado de cara sigue teniendo un servicio bastantedeficiente. Lo mejor los sandwich.",
        sub.body);
    assertEquals("Mir Mor", sub.author);
    assertEquals(null, sub.authorLocation);
    assertEquals(null, sub.reviews);
    assertEquals("Native score", new Integer(30), sub.getScore_native());// rating
    assertEquals("Websays score", new Integer(60), sub.getScore_websays());// rating
    assertEquals(null, sub.votes);// like
    
    cal.setTime(current);// 5 months ago
    // cal.set(Calendar.YEAR, 2013);
    cal.add(Calendar.MONTH, -8);// 8 months ago
    // cal.set(Calendar.DATE, 1);// considering day as the first day of month
    assertEqualDates(cal.getTime(), sub.date);
    assertEquals("http://11870.com/pro/vips-madrid-2/mir-mor", sub.itemUrl);
    assertEquals(url.toString(), sub.postUrl);
    assertEquals(commentItemIndex, sub.position);
    // ///////// Test Last comment item //////////////
    sub = result.get(10);
    assertEquals("dicen que era...", sub.title);
    assertEquals(
        "el que tenía la tienda más grande de todos los Vips de Madrid... no sé si seguirá vigente, pero era un placer hurgar por todas sus ofertas.",
        sub.body);
    assertEquals("Paloma Abad Pedreira", sub.author);
    assertEquals(null, sub.authorLocation);
    assertEquals(null, sub.reviews);
    assertEquals("Native score", null, sub.getScore_native());// rating
    assertEquals("Websays score", null, sub.getScore_websays());// rating
    assertEquals(null, sub.votes);// like
    
    cal.set(2008, 11, 19);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), sub.date);
    assertEquals("http://11870.com/pro/vips-madrid-2/palomaabad", sub.itemUrl);
    assertEquals(url.toString(), sub.postUrl);
    assertEquals(11, sub.position);
    
  }
}