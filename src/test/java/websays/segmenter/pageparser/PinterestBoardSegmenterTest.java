/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.List;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class PinterestBoardSegmenterTest extends SegmenterTest {
  
  /**
   * Test pinterest board scrapping, before 2016
   */
  @Test
  public void testSegment() throws Exception {
    
    URL url = new URL("https://www.pinterest.com");
    
    StringBuilder content = readPageFromResource("pinterest_board.html");
    HTMLDocumentSegmenter seg = new PinterestBoardSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    assertTrue(result.size() == 1);
    assertTrue(result.get(0).threadURLs.size() == 25);
    assertFalse(result.get(0).fakeItem);
    
  }
  
  /**
   * Test pinterest board scrapping, after 2016
   */
  @Test
  public void testSegment2016() throws Exception {
    
    URL url = new URL("https://www.pinterest.com");
    
    StringBuilder content = readPageFromResource("pinterest_board_2016.html");
    HTMLDocumentSegmenter seg = new PinterestBoardSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    assertTrue(result.size() == 1);
    assertTrue(result.get(0).threadURLs.size() == 25);
    assertFalse(result.get(0).fakeItem);
    
  }
  
}