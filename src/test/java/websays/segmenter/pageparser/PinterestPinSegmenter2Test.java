/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2016. All rights reserved. http://websays.com )
 *
 * Primary Author: juanfra
 * Contributors:
 * Date: Jun 3, 2016
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;

import java.net.URL;
import java.util.List;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

/**
 * @author juanfra
 *
 */
public class PinterestPinSegmenter2Test extends SegmenterTest {
  
  /**
   * Testing pinterest pin fetching (scrapping) for pinterest page format after 2016.
   */
  @Test
  public void testPin() throws Exception {
    
    URL url = new URL("https://es.pinterest.com/mylittleparty/pins/");
    
    StringBuilder content = readPageFromResource("pinterestNotLoggedInPins.html");
    HTMLDocumentSegmenter seg = new PinterestPinSegmenter2();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    assertEquals("Pins scrapped are no the amount expected.", 25, result.size());
    
    // ///////// Test first item //////////////
    SimpleItem main = result.get(0);// The main details item at index 0 (first element)
    assertEquals("Bodas", main.title);
    assertEquals("My Little Party Fiestas con Estilo", main.author);
    assertEquals("http://www.pinterest.com/pin/93309023504124558/", main.postUrl);
    assertEquals(new Integer(1), main.likes);
    assertEquals(new Integer(1), main.favorites);
    assertEquals("mylittleparty", main.APIAuthorID);
    assertEquals("93309023504124558", main.APIObjectID);
    
    assertEquals("https://s-media-cache-ak0.pinimg.com/236x/a1/9b/f1/a19bf10811caf129d0ad2ba49dd4fee4.jpg", main.imageLink);
    assertEquals(
        "Sweet Tables para Eventos Especiales III (found in http://www.mylittleparty.es/blog/sweet-tables-para-eventos-especiales-iii/ )",
        main.body);// Program will apply ',' to separate address fields
    
    // ///////// Test last item //////////////
    main = result.get(result.size() - 1);// The main details item at index 0 (first element)
    assertEquals("Bichos & Animales {Fiestas Temáticas}", main.title);
    
    assertEquals("Fiestas temáticas: Safari (found in http://www.mylittleparty.es/blog/fiestas-infantiles-tematicas-safari/ )", main.body);// Program
                                                                                                                                           // will
                                                                                                                                           // apply
                                                                                                                                           // ',' to
                                                                                                                                           // separate
                                                                                                                                           // address
                                                                                                                                           // fields
    assertEquals("My Little Party Fiestas con Estilo", main.author);
    assertEquals("http://www.pinterest.com/pin/93309023504046595/", main.postUrl);
    assertEquals("https://s-media-cache-ak0.pinimg.com/236x/78/e4/2d/78e42dd9c316729140562cb8ce35f60e.jpg", main.imageLink);
    assertEquals(new Integer(1), main.likes);
    assertEquals(new Integer(1), main.favorites);
    assertEquals("mylittleparty", main.APIAuthorID);
    assertEquals("93309023504046595", main.APIObjectID);
    
  }
  
}
