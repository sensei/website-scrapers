/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.net.URL;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class PinterestPinSegmenterTest extends SegmenterTest {
  
  /**
   * Testing pinterest pin fetching (scrapping) for pinterest page format before 2016.
   */
  @Test
  public void testPin() throws Exception {
    
    URL url = new URL("http://www.pinterest.com/pin/93309023504124558/");
    
    StringBuilder content = readPageFromResource("pinterest_pin.html");
    Calendar cal = Calendar.getInstance();
    HTMLDocumentSegmenter seg = new PinterestPinSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    assertTrue(result.size() == 1);
    
    // ///////// Test Main item //////////////
    SimpleItem main = result.get(0);// The main details item at index 0 (first element)
    assertEquals("Bodas pin", main.title);
    
    assertEquals(
        "Sweet Tables para Eventos Especiales III (found on http://www.mylittleparty.es/blog/sweet-tables-para-eventos-especiales-iii/)",
        main.body);// Program will apply ',' to separate address fields
    assertEquals("My Little Party Fiestas con Estilo", main.author);
    assertNotNull(main.date);
    assertEquals("http://www.pinterest.com/pin/93309023504124558/", main.postUrl);
    assertEquals("https://s-media-cache-ec0.pinimg.com/736x/a1/9b/f1/a19bf10811caf129d0ad2ba49dd4fee4.jpg", main.imageLink);
    assertEquals(new Integer(1), main.likes);
    assertEquals(new Integer(1), main.favorites);
    assertEquals("mylittleparty", main.APIAuthorID);
    assertEquals("93309023504124558", main.APIObjectID);
    
  }
  
}