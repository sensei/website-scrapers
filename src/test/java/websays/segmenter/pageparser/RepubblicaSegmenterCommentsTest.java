/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;

import java.net.URL;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class RepubblicaSegmenterCommentsTest extends SegmenterTest {
  
  @Test
  public void testCommentSegment() throws Exception {
    
    URL url = new URL(
        "http://comments.us1.gigya.com/comments/rss/6145611/www_repubblica_002/http%3A%2F%2Fwww.repubblica.it%2Fpolitica%2F2014%2F01%2F31%2Fnews%2Flegge_elettorale_oggi_i_primi_voti_la_lega_protesta_e_non_parteciper_al_voto-77358891%2F");
    StringBuilder content = readPageFromResource("Legge elettorale oggi in aula. La Lega protesta e non parteciperà al voto - Repubblica.it.xhtml");
    HTMLDocumentSegmenter seg = new RepubblicaSegmenterComments();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 10; // No of comments
    Calendar cal = Calendar.getInstance();
    cal.setTimeZone(TimeZone.getTimeZone("GMT"));
    assertEquals(expectedResults, result.size());
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = result.get(0);
    assertEquals(
        "Come sono veloci quando gli interessa, come riescono a superare il bicameralismo, piegando a piacere i regolamenti. La legge nata invece che in Parlamento nella sede di un partito-azienda con l'accordo a due tra un condannato in primo grado ed un pregiudicato non deve trovare impedimenti. Alcuni diranno che importa finalmente finiranno le grandi intese, governerà il centro-destra o il centro-sinistra. Illusi in questo paese le idee socialiste non le promuove più nessuno, il Pd non è un partito di sinistra, insieme a Berlusconi pensano come aziende solo alle loro convenienze. Chiunque governerà tra questi la sostanza non cambierà, la finanza ed il mercimonio di questi politici resteranno i padroni. La dittatura scambiata per governabilità è il nostro futuro, svuotate le tasche e alzate le mani, preparatevi alle prossime rapine, di soldi e di diritti.",
        sub1.body);
    assertEquals("Luciano Monti", sub1.author);
    cal.set(2014, 1, 1, 14, 07);// 01-02-2014 14:07 GMT
    assertEqualTimes(cal.getTime(), sub1.date);
    assertEquals(url.toString(), sub1.postUrl);
    assertEquals(1, sub1.position);
    assertEquals(
        "www_repubblica_002/http%3A%2F%2Fwww.repubblica.it%2Fpolitica%2F2014%2F01%2F31%2Fnews%2Flegge_elettorale_oggi_i_primi_voti_la_lega_protesta_e_non_parteciper_al_voto-77358891%2F",
        sub1.threadId);
    
    SimpleItem sub2 = result.get(1);
    assertEquals(
        "A questo punto mi piace di più l'ingenuità, ma anche la coerenza di Bersani, piuttosto che l'arroganza di Renzi.Auguro a Bersani una pronta guarigione e di vederlo presto sulla scena politica che conta.",
        sub2.body);
    assertEquals("germalli", sub2.author);
    cal.set(2014, 1, 1, 14, 3);// 01-02-2014 14:03 GMT
    assertEqualTimes(cal.getTime(), sub2.date);
    assertEquals(url.toString(), sub2.postUrl);
    assertEquals(2, sub2.position);
    assertEquals(
        "www_repubblica_002/http%3A%2F%2Fwww.repubblica.it%2Fpolitica%2F2014%2F01%2F31%2Fnews%2Flegge_elettorale_oggi_i_primi_voti_la_lega_protesta_e_non_parteciper_al_voto-77358891%2F",
        sub2.threadId);
    
    SimpleItem sub9 = result.get(9);
    assertEquals(
        "Devo dire che finora Renzi è riuscito a spaccare il centro sinistra, e a unire l'intero centro destra; Casini ne è la prova.Renzi, se continui cosi' rottami anche te stesso e l'intero PD.  ",
        sub9.body);
    assertEquals("germalli", sub9.author);
    cal.set(2014, 1, 1, 12, 35);// 01-02-2014 12:35
    assertEqualTimes(cal.getTime(), sub9.date);
    assertEquals(url.toString(), sub9.postUrl);
    assertEquals(10, sub9.position);
    assertEquals(
        "www_repubblica_002/http%3A%2F%2Fwww.repubblica.it%2Fpolitica%2F2014%2F01%2F31%2Fnews%2Flegge_elettorale_oggi_i_primi_voti_la_lega_protesta_e_non_parteciper_al_voto-77358891%2F",
        sub9.threadId);
    
  }
}
