/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.htmlcleaner.HtmlCleaner;

public class RepubblicaSegmenterTest extends BaseDocumentSegmenterTest {
  
  @Override
  public void setUp() throws Exception {
    // http://www.repubblica.it/politica/2014/01/31/news/legge_elettorale_oggi_i_primi_voti_la_lega_protesta_e_non_parteciper_al_voto-77358891/
    StringBuilder content = readPageFromResource("Alfano a Renzi: \"Si impegni nel governo o è crisi\". Italicum, primo sì dell'aula, ma l'iter rallenta - Repubblica.it.html");
    this.seg = new RepubblicaSegmenter();
    HtmlCleaner cleaner = createCleaner();
    this.rootNode = cleaner.clean(content.toString());
  }
  
  @Override
  public void testFindMainTitle() {
    String title = seg.findMainTitle(rootNode);
    assertEquals("Alfano a Renzi: \"Si impegni nel governo o è crisi\". Italicum, primo sì dell'aula, ma l'iter rallenta", title);
  }
  
  @Override
  public void testFindMainDescripcion() {
    String body = seg.findMainDescripcion(rootNode);
    assertEquals(
        "La legge elettorale tornerà alla Camera l'11 febbraio. Le pregiudiziali di costituzionalità presentate dai gruppi di minoranza oggi sono state",
        body);
  }
  
  @Override
  public void testFindMainAPIObjectID() {
    assertEquals("182234715127717", seg.findMainAPIObjectID(rootNode));
  }
  
  @Override
  public void testFindMainImageLink() {
    assertEquals("http://www.repstatic.it/content/nazionale/img/2014/01/31/103947352-a8ab3d17-d4f4-407e-b6d5-7c49c5e5f6d8.jpg",
        seg.findMainImageLink(rootNode));
  }
  
  @Override
  public void testFindMainDate() {
    assertEqualTimes(DatatypeConverter.parseDateTime("2014-01-31T10:17:00").getTime(), seg.findMainDate(rootNode));
  }
  
  @Override
  public void testFindMainThreadId() {
    assertEquals(
        "www_repubblica_002/http%3A%2F%2Fwww.repubblica.it%2Fpolitica%2F2014%2F01%2F31%2Fnews%2Flegge_elettorale_oggi_i_primi_voti_la_lega_protesta_e_non_parteciper_al_voto-77358891%2F",
        seg.findMainThreadId(rootNode));
  }
  
  @Override
  public void testFindMainThreadURLs() {
    List<String> urls = seg.findMainThreadURLs(rootNode);
    assertEquals(1, urls.size());
    assertEquals(
        "http://comments.us1.gigya.com/comments/rss/6145611/www_repubblica_002/http%3A%2F%2Fwww.repubblica.it%2Fpolitica%2F2014%2F01%2F31%2Fnews%2Flegge_elettorale_oggi_i_primi_voti_la_lega_protesta_e_non_parteciper_al_voto-77358891%2F"
            + RepubblicaSegmenter.getSite(), urls.get(0));
  }
  
  @Override
  public void testFindMainTimeIsReal() {
    assertTrue(
        "Main thread time for this segmenter is always true. As this tests is failing, check the scraping process or that the web has not changed its source structure.",
        seg.findMainTimeIsReal(rootNode));
  }
  
}
