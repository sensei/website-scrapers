/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.net.URL;
import java.util.Calendar;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.segmenter.util.SegmenterTestNavigation;
import websays.types.clipping.SimpleItem;

public class RestaurantesSegmenterTest extends SegmenterTest {
  
  @Test
  public void testRestauranteLahSegment() throws Exception {
    
    URL url = new URL("http://madrid.restaurantes.com/restaurante/Lah!/");
    StringBuilder content = readPageFromResource("Restaurante Lah! en Madrid: restaurantes.com.html");
    HTMLDocumentSegmenter seg = new RestaurantesSegmenter();
    SegmenterTestNavigation navigation = new SegmenterTestNavigation(url, content.toString(), seg);
    
    Calendar cal = Calendar.getInstance();
    
    int expectedResults = 6; // No of comments+ basic details item which
    // you expect in the page
    assertEquals(expectedResults, navigation.size());
    
    // ///////// Test Main item //////////////
    SimpleItem main = navigation.nextItem();// The main details item at
    // index 0 (first
    // element)
    assertEquals("Restaurante Lah!", main.title);
    assertEquals(
        "\"Viaje gastronómico por el sudeste asiático en el que descubrirás los secretos de unas culturas llenas de contrastes. La cocina de Lah! pretende sorprender hasta a los clientes más conocedores de la gastronomía asiática. En ella no encontrarán platos chinos o japoneses tan extendidos en España, sino más bien los platos, materias primas y sabores más auténticos del sureste asiático, aún poco explorados por los paladares occidentales.        Lah! propone un viaje multisensorial hasta aquella lejana región del mundo donde reina la diversidad, recogiendo lo mejor de cada gastronomía local para traer a su mesa una cuidada e ingeniosa selección de sus platos más representativos.        Así es la carta de Lah!, una apetecible mezcla de platos coloridos, exquisitamente presentados, con aromas a especias, a fruta fresca, bosque tropical, y reuniendo todos los perfiles de sabores posibles, desde salado, dulce y agrio hasta amargo.  Una cocina en donde los matices son el ingrediente principal.        \"Además: Admiten:Ticket RestaurantCheque RestaurantCheque Gourmet",
        main.body);
    assertEquals(null, main.author);
    assertEquals(null, main.authorLocation);
    assertEquals(new Integer(19), main.comments);
    assertEquals("Native score", new Integer(88), main.getScore_native());// round, the real is 8.8
    assertEquals("Websays score", new Integer(88), main.getScore_websays());// round, the real is 8.8
    assertEquals(new Integer(5), main.likes);
    assertNull(main.date);
    assertNull(main.itemUrl);
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = navigation.nextItem();
    assertEquals(null, sub1.title);
    assertEquals(" ", sub1.body);
    assertEquals("Bostar", sub1.author);
    assertEquals(new Integer(8), sub1.reviews);
    cal.set(2013, 9, 19, 20, 31, 24);
    assertEqualTimes(cal.getTime(), sub1.date);
    assertNull(sub1.itemUrl);
    
    // ///////// Test Last comment item //////////////
    SimpleItem sub5 = navigation.getLastItem();
    assertEquals(null, sub5.title);
    assertEquals(
        "Buen ambiente, buena decoracion, acogedor...la comida esta bien pero de sabor especial...si eres de comida tradicional mejor no ir, si te apetece probar algo nuevo es tu sitio.",
        sub5.body);
    assertEquals("Anónimo", sub5.author);
    assertEquals(new Integer(7), sub5.reviews);
    cal.set(2013, 5, 1, 02, 34, 48); // 2013-06-01 02:34:48
    assertEqualTimes(cal.getTime(), sub5.date);
    assertNull(sub5.itemUrl);
  }
  
  @Test
  public void testRestauranteLaPanzaEsPrimeroSegment() throws Exception {
    
    URL url = new URL("http://madrid.restaurantes.com/restaurante/China-Town-Leganes/");
    StringBuilder content = readPageFromResource("Restaurante China Town en Leganés - Madrid: restaurantes.com.html");
    HTMLDocumentSegmenter seg = new RestaurantesSegmenter();
    
    SegmenterTestNavigation navigation = new SegmenterTestNavigation(url, content.toString(), seg);
    
    int expectedResults = 1; // No of comments+ basic details item which
    // you expect in the page
    assertEquals(expectedResults, navigation.size());
    
    // ///////// Test Main item //////////////
    SimpleItem main = navigation.getFirstItem();// The main details item at
    // index 0 (first
    // element)
    assertEquals("Restaurante China Town (Leganés)", main.title);
    assertEquals(null, main.body);
    assertEquals(null, main.author);
    assertEquals(null, main.authorLocation);
    assertEquals(null, main.comments);
    assertEquals("Native score", null, main.getScore_native());
    assertEquals("Websays score", null, main.getScore_websays());
    assertEquals(null, main.votes);
    assertNull(main.date);
    assertNull(main.itemUrl);
    assertEquals(1, main.position);
    assertEquals(url.toString(), main.postUrl);
    
  }
}
