/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

import junit.framework.AssertionFailedError;
//import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * Extending from TestCase is Junit 3 way of doing things.....let's try to avoid it if we are going to use Junit 4 everywhere.
 * 
 * @author juanfra
 * 
 */

public class SegmenterTest {
  
  private static final Logger logger = Logger.getLogger(SegmenterTest.class);
  
  private static SimpleDateFormat dFormatter = new SimpleDateFormat("MMMMM dd, yyyy");
  private static SimpleDateFormat tFormatter = new SimpleDateFormat("MMMMM dd, yyyy HH:mm");
  
  public static void assertEqualDates(Date date1, Date date2) throws AssertionFailedError {
    if (date1 == null && date2 == null) {} else {
      String d1 = " ";
      String d2 = "";
      try {
        d1 = dFormatter.format(date1);
      } catch (Exception e) {}
      try {
        d2 = dFormatter.format(date2);
      } catch (Exception e) {}
      if (!d1.equals(d2)) {
        throw new AssertionFailedError("expected:<" + d1 + "> but was:<" + d2 + ">");
      }
    }
  }
  
  public static void assertEqualTimes(Date date1, Date date2) throws AssertionFailedError {
    if (date1 == null && date2 == null) {} else {
      String d1 = " ";
      String d2 = "";
      try {
        d1 = tFormatter.format(date1);
      } catch (Exception e) {}
      try {
        d2 = tFormatter.format(date2);
      } catch (Exception e) {}
      if (!d1.equals(d2)) {
        throw new AssertionFailedError("expected:<" + d1 + "> but was:<" + d2 + ">");
      }
    }
  }
  
  protected static final String resourceDir = "/websays/segmenter/pageparser";
  
  /**
   * @return
   * @throws UnsupportedEncodingException
   * @throws IOException
   */
  public StringBuilder readPageFromResource(String name) throws UnsupportedEncodingException, IOException {
    StringBuilder content = readPageFromResource(name, "UTF-8");
    return content;
  }
  
  public StringBuilder readPageFromResource(String name, String charset) throws UnsupportedEncodingException, IOException {
    InputStream htmlSource = getClass().getResourceAsStream(resourceDir + "/" + name);
    BufferedReader br;
    try {
      br = new BufferedReader(new InputStreamReader(htmlSource, charset));
    } catch (Exception e) {
      logger.error("Could not find resource: \"" + name + "\"");
      throw (e);
    }
    
    StringBuilder content = new StringBuilder();
    
    String line = null;
    while ((line = br.readLine()) != null) {
      content.append(line);
    }
    return content;
  }
  
  /*
   * static public void assertEquals(int expected, Integer actual) { assertEquals(expected, actual.intValue()); }
   */
  @Test
  public void testfaketest() {
    /*
     * This fake test is only needed to avoid a warning, because this class inherits from testcase but doesn't implement any test. This is a ñapa
     * solution. But it is fast and harmless. Should you implement the correct solution: -- Make all segmenters inherit directly from testcase -- Make
     * this class NOT inherit from testcase -- Move this class to the util package -- Arrange all test classes calls to this class functionality
     * accordingly.
     */
  }
  
}
