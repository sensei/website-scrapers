/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;

import java.net.URL;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class SpaziosimSegmenterTest extends SegmenterTest {
  
  /**
   * Test of segment method, of class SpaziosimSegmenter.
   */
  @Test
  public void testSpaziosimSegmenter() throws Exception {
    
    URL url = new URL("http://forum.spaziosim.net/index.php?topic=17117.0");
    
    StringBuilder content = readPageFromResource("forum.spaziosim.net.html");
    
    Calendar cal = Calendar.getInstance();
    HTMLDocumentSegmenter seg = new SpaziosimSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 13;
    assertEquals(expectedResults, result.size());
    
    // ///////// Test Secon comment item //////////////
    
    SimpleItem main = result.get(0);
    assertEquals("msg_183375", main.threadId);
    assertEquals("[PosteMobile]100% Special", main.title);
    assertEquals("Alexey", main.author);
    assertEquals(
        "Da qualche giorno PosteMobile ha lanciato una nuova offerta attivabile esclusivamente con portabilit� tramite operatore. L'offerta, non presente sul sito, si chiama 100% Special e si tratta di una nuova tariffa a consumo con le seguenti caratteristiche:chiamate a 14 centesimi al minuto senza scatto alla risposta e con tariffazione a scatti anticipati di 30\"sms a 14 centesimiraddoppio delle ricariche \"per sempre\"A differenza del precedente raddoppio di PosteMobile, anche quello non pubblicizzato sul sito, questo non verr� erogato immediatamente, bens� all'inizio del mese solare successivo alla ricarica e non ho capito se il bonus scadr� dopo 30 giorni come per il vecchio raddoppio, oppure se durer� fino alla fine del mese solare.Fatto sta che con ogni probabilit� non sar� pi� possibile in alcun modo prorogarne la scadenza, come invece si poteva fare con il vecchio raddoppio semplicemente ricaricando di 1 euro tramite i servizi semplifica.In questo caso per� il raddoppio non sar� per soli due anni ma \"per sempre\".Il credito bonus, in quanto tale, non � ovviamente rimborsabile in caso di cessazione sim o mnp out e non pu� essere utilizzato per il rinnovo delle opzioni. � possibile invece utilizzarlo per pagare il primo canone delle opzioni e i costi di cambio piano, oltre che ovviamente per chiamate, sms, mms, traffico dati a consumo...Non so se sia utilizzabile all'estero e non so se ci sia un limite massimo di ricarica mensile (queste due cose mi sono proprio scordato di chiederle). In caso di cambio piano tariffario si perde automaticamente anche il raddoppio (probabilmente rimarr� solo il bonus residuo in scadenza).Per essere chiamati dall'operatore esistono 3 modi:compilare l'apposito modulo via weblasciare i propri dati a un operatore di assistenza chat sul sitolasciare i propri dati a un operatore di assistenza telefonica al 160**gratis solo da postemobile e fissi telecomLa sim verr� spedita a casa e baster� dare al postino 5 euro e la fotocopia del documento (se non ricordo male non serve la fotocopia della tessera sanitaria/codice fiscale). La sim avr� 5 euro di credito.",
        main.body);
    cal.set(2013, 3, 26);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), main.date);
    assertEquals(null, main.itemUrl);
    assertEquals(url.toString(), main.postUrl);
    // assertEquals(null, sub1.imageLink);
    assertEquals(1, main.position);
    
    // /////// Test Sixth comment item //////////////
    
    SimpleItem six = result.get(5);
    assertEquals("msg_183666", six.threadId);
    assertEquals("Re:[PosteMobile]100% Special", six.title);
    assertEquals("rambo", six.author);
    assertEquals("Il bonus pu� essere trasferito come sos??", six.body);
    assertEquals(null, six.itemUrl);
    cal.set(2013, 3, 27);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), six.date);
    assertEquals(url.toString(), six.postUrl);
    assertEquals(6, six.position);
    
  }
  
}
