/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;

import java.net.URL;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class StandardCommentsSegmenterTest extends SegmenterTest {
  
  @Test
  public void testCommentSegment() throws Exception {
    
    URL url = new URL("http://comments.us1.gigya.com/comments/rss/6148681/Comments/8863475");
    StringBuilder content = readPageFromResource("Fire brigade to charge £350 for each lift rescue after it performs 6,430 in year - London - News - London Evening Standard.xhtml");
    HTMLDocumentSegmenter seg = new StandardCommentsSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 30; // No of comments
    Calendar cal = Calendar.getInstance();
    cal.setTimeZone(TimeZone.getTimeZone("GMT"));
    assertEquals(expectedResults, result.size());
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = result.get(0);
    assertEquals(
        "Lift companies make a great deal of money on their service contracts, and ought to provide an emergency service. On the other hand, there surely must be some technology whereby a lift can go into a default position and release its occupants?",
        sub1.body);
    assertEquals("anarchist666", sub1.author);
    cal.set(2013, 9, 8, 9, 59);// Tue, 08 Oct 2013 09:59:54 GMT
    assertEqualTimes(cal.getTime(), sub1.date);
    assertEquals(url.toString(), sub1.postUrl);
    assertEquals(1, sub1.position);
    assertEquals("8863475", sub1.threadId);
    
    SimpleItem sub2 = result.get(1);
    assertEquals(
        "Often the lifts are stuck either between floors. You wouldn't want a lift door opening automatically in this instance as there could be all sorts of ways to injure or kill yourself if you tried to exit in this scenario. Power needs to be turned off to the lift before any release attempt or you risk the lift mechanism engaging while the occupant is in the middle of exiting. This is very simple knowledge a firefighter gains on the job. But, as with most things we are expected to do, people think there's easier and cheaper options available. Not true.",
        sub2.body);
    assertEquals("Anon", sub2.author);
    cal.set(2013, 9, 8, 10, 06);// Tue, 08 Oct 2013 10:06:35 GMT
    assertEqualTimes(cal.getTime(), sub2.date);
    assertEquals(url.toString(), sub2.postUrl);
    assertEquals(2, sub2.position);
    assertEquals("8863475", sub2.threadId);
    
    SimpleItem sub30 = result.get(29);
    assertEquals("Less than a solicitor would charge an hour so a bargain ", sub30.body);
    assertEquals("Anonymous", sub30.author);
    cal.set(2013, 9, 7, 7, 36);// Mon, 07 Oct 2013 07:36:41 GMT
    assertEqualTimes(cal.getTime(), sub30.date);
    assertEquals(url.toString(), sub30.postUrl);
    assertEquals(30, sub30.position);
    assertEquals("8863475", sub30.threadId);
    
  }
}