/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;

import java.net.URL;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class StandardSegmenterTest extends SegmenterTest {
  
  @Test
  public void testArtsEntertainmentSegment() throws Exception {
    
    URL url = new URL(
        "http://www.standard.co.uk/news/london/fire-brigade-plans-350-callout-charge-for-lift-rescues-after-it-attends-6430-in-a-year-8863475.html");
    StringBuilder content = readPageFromResource("Fire Brigade plans £350 call-out charge for lift rescues after it attends 6,430 in a year - London - News - London Evening Standard.html");
    HTMLDocumentSegmenter seg = new StandardSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 1; // No of comments+ basic details item which
    // you expect in the page
    assertEquals(expectedResults, result.size());
    
    // ///////// Test Main item //////////////
    SimpleItem main = result.get(0);// The main details item at
    // index 0 (first
    // element)
    assertEquals("Fire Brigade plans £350 call-out charge for lift rescues after it attends 6,430 in a year", main.title);
    assertEquals("London Fire Brigade is to charge landlords of tower blocks a £350 call-out fee to rescue people stuck in lifts.",
        main.body);
    assertEquals("Simon Freeman", main.author);
    assertEquals(null, main.authorLocation);
    assertEquals(null, main.comments);// 'comentarios' value
    assertEquals("Native score", null, main.getScore_native());
    assertEquals("Websays score", null, main.getScore_websays());
    assertEquals(null, main.votes);
    assertEqualTimes(DatatypeConverter.parseDateTime("2013-10-07").getTime(), main.date); // 2013-10-07
    assertEquals(null, main.itemUrl);
    assertEquals(1, main.position);
    assertEquals(url.toString(), main.postUrl);
    
    assertEquals("8863475", main.threadId);
    assertEquals(1, main.threadURLs.size());
    assertEquals("http://comments.us1.gigya.com/comments/rss/6148681/Comments/8863475" + StandardSegmenter.getSite(),
        main.threadURLs.get(0));
  }
}
