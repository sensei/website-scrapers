/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;

import java.net.URL;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class TelefoninoSegmenterTest extends SegmenterTest {
  
  /**
   * Test of segment method, of class TripAdvisorSegmenter.
   */
  @Test
  public void testTelefoninoSegment() throws Exception {
    
    URL url = new URL("http://forum.telefonino.net/showthread.php?t=1048553");
    
    StringBuilder content = readPageFromResource("forum.telefonino.net");
    
    Calendar cal = Calendar.getInstance();
    HTMLDocumentSegmenter seg = new TelefoninoSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 11;
    assertEquals(expectedResults, result.size());
    
    // /////// Test First comment item //////////////
    SimpleItem main = result.get(0);
    assertEquals("postemobile si appoggia già su wind?", main.title);
    assertEquals("fra2283", main.author);
    assertEquals(
        "salve! è da un po' che leggo della migrazione dalla rete vodafone a quella wind da parte di postemobile, ma non ho capito se questo passaggio è già avvenuto oppure no. qualcuno ne sa di più? eventualmente quando sarebbe prevista la migrazione? grazie!",
        main.body);
    assertEquals(null, main.itemUrl);
    cal.set(2014, 2, 20);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), main.date);
    assertEquals(url.toString(), main.postUrl);
    // assertEquals("http://forum.telefonino.net/customavatars/avatar67543_17.gif", main.imageLink);
    assertEquals(1, main.position);
    
    // ///////// Test Second comment item //////////////
    SimpleItem sub1 = result.get(1);
    assertEquals(" postemobile si appoggia già su wind?", sub1.title);
    assertEquals("kaiser77", sub1.author);
    assertEquals("ancora no", sub1.body);
    cal.set(2014, 2, 20);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), sub1.date);
    assertEquals(null, main.itemUrl);
    assertEquals(url.toString(), sub1.postUrl);
    // assertEquals(null, sub1.imageLink);
    assertEquals(2, sub1.position);
    
  }
}
