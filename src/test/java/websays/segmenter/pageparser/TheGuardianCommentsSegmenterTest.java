/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.net.URL;
import java.util.List;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.segmenter.base.MinimalDocumentSegmenter;
import websays.types.clipping.SimpleItem;
import websays.utils.misc.DateUtilsWebsays;

public class TheGuardianCommentsSegmenterTest extends SegmenterTest {
  
  /**
   * The Guardian discussion urls like
   * "http://discussion.theguardian.com/discussion/p/4g6mf?orderby=newest&per_page=50&commentpage=1&noposting=true&tab=all" no longer work. From now
   * on (this was detected 2016-03-02) we need to use the API http://discussion.theguardian.com/discussion-api/ with urls like
   * "http://discussion.theguardian.com/discussion-api/discussion//p/3htd7?orderBy=oldest&page=1&pageSize=50". OLD tests can be found on SVN
   * 
   * @throws Exception
   */
  @Test
  public void testComments03Marzo2016() throws Exception {
    
    URL url = new URL("http://www.theguardian.com/lifeandstyle/2016/feb/28/which-way-you-vote-europe-leave-or-remain");
    String baseCommentsURL = "http://discussion.theguardian.com/discussion-api/discussion//p/4h3vb?orderBy=newest&pageSize=50&page=";
    String expectedCommentsURL = baseCommentsURL + "1";
    String expectedCommentsURLnext = baseCommentsURL + "2";
    
    StringBuilder content = readPageFromResource("theGuardian-which-way-you-vote-europe-leave-or-remain.html");
    HTMLDocumentSegmenter seg = new TheGuardianSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 1;
    assertEquals(expectedResults, result.size());
    
    // ///////// Test Main item //////////////
    SimpleItem main = result.get(0);// The main details item at
    URL urlComments = new URL(main.threadURLs.get(0));
    System.out.println("URL comments> " + urlComments);
    assertEquals(expectedCommentsURL, urlComments.toExternalForm());
    
    StringBuilder contentComment = readPageFromResource("theGuardian-which-way-you-vote-europe-leave-or-remain-comments-page1.json");
    MinimalDocumentSegmenter segComments = new TheGuardianCommentsSegmenter();
    
    List<SimpleItem> resultComments = segComments.segment(contentComment.toString(), urlComments);
    
    int expectedResultsComments = 269; // No of comments + responses to those comments
    assertEquals(expectedResultsComments, resultComments.size());
    
    SimpleItem item;
    // check comment
    item = resultComments.get(0);
    assertEquals("69645628", item.APIObjectID);
    // assertEquals("BODYYYY");
    assertEquals("2016-02-29T17:51:48Z", DateUtilsWebsays.toISO8601(item.date));
    assertEquals("https://discussion.theguardian.com/comment-permalink/69645628", item.itemUrl);
    assertEquals("4186628", item.APIAuthorID);
    assertEquals("Madranon", item.author);
    assertEquals(2, item.position);
    assertNotNull(item.threadURLs);
    assertEquals(1, item.threadURLs.size());
    assertEquals(expectedCommentsURLnext, item.threadURLs.get(0));
    
    // check responses
    String expectedBody = " I think a psychologist would be better for you, to help explain how your brain works.. and then maybe a logic teacher? ";
    item = resultComments.get(5);
    assertEquals("69586253", item.APIObjectID);
    assertEquals("Toby Shew", item.author);
    assertEquals("10357681", item.APIAuthorID);
    assertNull(item.threadURLs);
    assertEquals("69584649", item.parentID);
    assertEquals("15945848", item.APIToAuthorID);
    assertEquals(2, item.position);
    assertEquals(expectedBody, item.body);
    
  }
}