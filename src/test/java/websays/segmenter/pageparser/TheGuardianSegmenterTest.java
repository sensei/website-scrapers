/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;

import java.net.URL;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class TheGuardianSegmenterTest extends SegmenterTest {
  
  /**
   * Test of segment method, of class TheGuardian Segmenter.
   */
  @Test
  public void testPageWithoutCommentSegment() throws Exception {
    
    URL url = new URL("http://www.theguardian.com/world/2013/nov/23/john-kerry-geneva-iran-nuclear-negotiations");
    StringBuilder content = readPageFromResource("theguardianWorld.htm");
    HTMLDocumentSegmenter seg = new TheGuardianSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 1; // No of comments+ basic details item which
    // you expect in the page
    assertEquals(expectedResults, result.size());
    
    // ///////// Test Main item //////////////
    SimpleItem main = result.get(0);// The main details item at
    // index 0 (first
    // element)
    assertEquals("John Kerry and William Hague fly to Geneva to try to seal Iran nuclear deal", main.title);
    String body = "John Kerry arrived in Geneva on Saturday morning to join William Hague and other foreign ministers in negotiations on Iran\'s nuclear programme";
    assertEquals(body, main.body.split("\\.")[0]);
    assertEquals("Julian Borger and Saeed Kamali Dehghan", main.author);
    assertEquals("Geneva", main.authorLocation);
    assertEquals(null, main.comments);// 'comentarios' value
    assertEquals("Native score", null, main.getScore_native());
    assertEquals("Websays score", null, main.getScore_websays());
    assertEquals(null, main.votes);
    assertEquals("3kjbe", main.threadId);
    assertEqualTimes(DatatypeConverter.parseDateTime("2013-11-23T08:53:12Z").getTime(), main.date);
    assertEquals(url.toString(), main.postUrl);
    assertEquals(null, main.itemUrl);
    assertEquals(1, main.position);
    
  }
  
  @Test
  public void testPageWithCommentSegment() throws Exception {
    
    URL url = new URL("http://www.theguardian.com/uk-news/2013/oct/06/cabinet-gchq-surveillance-spying-huhne");
    StringBuilder content = readPageFromResource("theGuardianWithComments.html");
    HTMLDocumentSegmenter seg = new TheGuardianSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 1; // No of comments+ basic details item which
    // you expect in the page
    assertEquals(expectedResults, result.size());
    
    // ///////// Test Main item //////////////
    SimpleItem main = result.get(0);// The main details item at
    // index 0 (first
    // element)
    assertEquals("Cabinet was told nothing about GCHQ spying programmes, says Chris Huhne", main.title);
    String body = "Cabinet ministers and members of the national security council were told nothing about the existence and scale of the vast data-gathering programmes run by British and American intelligence agencies, a former member of the government has revealed.Chris Huhne, who was in the cabinet for two years until 2012, said ministers were in \"utter ignorance\" of the two biggest covert operations, Prism and Tempora. The former Liberal Democrat MP admitted he was shocked and mystified by the surveillance capabilities disclosed by the Guardian from files leaked by the whistleblower Edward Snowden.\"The revelations put a giant question mark into the middle of our surveillance state,\" he said. \"The state should not feel itself entitled to know, see and memorise everything that the private citizen communicates. The state is our servant.\"Writing in Monday\'s Guardian, Huhne also questioned whether the Home Office had deliberately misled parliament about the need for the communications data bill when GCHQ, the government\'s eavesdropping headquarters, already had remarkable and extensive snooping capabilities.He said this lack of information and accountability showed \"the supervisory arrangements for our intelligence services need as much updating as their bugging techniques\".Over the past three months the Guardian has made a series of disclosures about the activities of GCHQ and its much bigger American counterpart, the National Security Agency. Two of the most significant programmes uncovered in the Snowden files were Prism, run by the NSA, and Tempora, which was set up by GCHQ. Between them, they allow the agencies to harvest, store and analyse data about millions of phone calls, emails and search engine queries.As a cabinet minister and member of the national security council (NSC), Huhne said he would have expected to be told about these operations, particularly as they were relevant to proposed legislation.\"The cabinet was told nothing about GCHQ\'s Tempora or its US counterpart, the NSA\'s Prism, nor about their extraordinary capability to hoover up and store personal emails, voice contact, social networking activity and even internet searches.\"I was also on the national security council, attended by ministers and the heads of the Secret [Intelligence Service, MI6] and Security Service [MI5], GCHQ and the military. If anyone should have been briefed on Prism and Tempora, it should have been the NSC.\"I do not know whether the prime minister or the foreign secretary (who has oversight of GCHQ) were briefed, but the NSC was not. This lack of information, and therefore accountability, is a warning that the supervision of our intelligence services needs as much updating as their bugging techniques.\"Huhne said Prism and Tempora \"put in the shade Tony Blair\'s proposed ID cards, 90-day detention without trial and the abolition of jury trials\".He added: \"Throughout my time in parliament, the Home Office was trying to persuade politicians to invest in \'upgrading\' Britain\'s capability to recover data showing who is emailing and phoning whom. Yet this seems to be exactly what GCHQ was already doing. Was the Home Office trying to mislead?\"The Home Office was happy to allow the NSC and the cabinet \u2013 let alone parliament \u2013 to remain in utter ignorance of Prism/Tempora while deciding on the communications data bill.\"The draft bill would have given police and the security services access, without a warrant, to details of all online communication in the UK \u2013 such as the time, duration, originator and recipient, and the location of the device from which it was made. The legislation was eventually dropped after splits in the coalition.Proper scrutiny of the intelligence agencies was vital, said Huhne, and surveillance techniques needed to be tempered. \"Joseph Goebbels was simply wrong when he argued that \'if you have nothing to hide, you have nothing to fear\'. Information is power, and the necessary consequence is that privacy is freedom. Only totalitarians pry absolutely.\"Huhne, formerly the energy and climate change minister, was jailed this year after he admitted perverting the course of justice over claims his ex-wife took speeding points for him. In February he was sentenced to eight months in prison but was released after serving 62 days.His intervention comes as concern about the oversight and scrutiny of Britain\'s spy agencies grows. While former members of the intelligence community insist GCHQ, MI5 and MI6 operate with integrity and within the law, even they have questioned whether the oversight regime is fit for purpose following the Snowden revelations.Over the last few days a former member of parliament\'s intelligence and security committee, Lord King, a former director of GCHQ, Sir David Omand, and a former director general of MI5, Dame Stella Rimington, have questioned whether the agencies need to be more transparent and accept more rigorous scrutiny of their work.On Monday, a former legal director of MI5 and MI6 will add his weight to the calls for change. David Bickford told the Guardian Britain\'s intelligence agencies should seek authority for secret operations from a judge rather than a minister because public unease about their surveillance techniques is at an all-time high.Bickford said the government should pass responsibility to the courts because of widespread \"dissatisfaction with the covert, intrusive powers of the UK intelligence and law enforcement agencies\".\"Whether this is based on perception or reality doesn\'t really matter,\" he said. \"As long as government ministers continue to authorise the agencies\' eavesdropping, telephone and electronic surveillance, and informant approval, the public will believe that there is an unhealthy seamless relationship between them.\" Bickford said it was time for ministers to \"step out of the equation and leave the authorisation of these highly intrusive methods to the judiciary\".Bickford was drafted in to MI5 and MI6 following a series of scandals, including the furore over the book Spycatcher, written by the senior former MI5 officer Peter Wright. He worked for almost a decade until 1995 and still advises governments on countering international organised crime and terrorist money laundering.Bickford said giving judges rather than cabinet ministers responsibility for authorising sensitive operations would \"reduce the risk of perception of collusion \u2026 and limit the room for accusations of political interference.\"\"Government may argue that all this is unnecessary as there is adequate oversight of the agencies. However, that cannot substitute for independent judicial authority at the coal face.\"Meanwhile, on Sunday, the World Association of Newspapers and News Publishers (WAN-IFRA) condemned the way the British government had threatened legal action against the Guardian newspaper unless it destroyed the copy of the Snowden files it had in London.\"WAN-IFRA calls on democratic governments to recognise that acts of intimidation and surveillance against the press risk undermining the fabric of transparent, accountable governance,\" the organisation\'s board said in a resolution issued during its meeting on the eve of this week\'s World Publishing Expo in Berlin, Germany.";
    // assertEquals(body, main.body);
    assertEquals("Nick Hopkins and Matthew Taylor", main.author);
    assertEquals(null, main.authorLocation);
    assertEquals(null, main.comments);// 'comentarios' value
    assertEquals("Native score", null, main.getScore_native());
    assertEquals("Websays score", null, main.getScore_websays());
    
    assertEquals(null, main.votes);
    // assertEqualTimes(DatatypeConverter.parseDateTime("2013-10-06T20:48:07+01:00").getTime(), main.date);
    assertEquals(url.toString(), main.postUrl);
    assertEquals(null, main.itemUrl);
    assertEquals(1, main.position);
    String expectedKey = "3jb43";
    assertEquals(expectedKey, main.threadId);
    assertEquals(1, main.threadURLs.size());
    assertEquals(makeExpectedThreadURL(expectedKey), main.threadURLs.get(0));
    
  }
  
  /**
   * @param string
   * @return
   */
  private Object makeExpectedThreadURL(String key) {
    
    // OLD Before discussion API!
    // return String.format(
    // "http://discussion.theguardian.com/discussion/p/%s?orderby=newest&per_page=50&commentpage=1&noposting=true&tab=all", key);
    
    // to be able to use the discussion API from 2016-03-02
    return String.format("http://discussion.theguardian.com/discussion-api/discussion//p/%s?orderBy=newest&pageSize=50&page=1", key);
  }
  
  @Test
  public void testPage22Mayo2014() throws Exception {
    
    URL url = new URL("http://www.theguardian.com/uk-news/2014/may/21/prince-charles-putin-hitler-comparison-latest-string-gaffes");
    StringBuilder content = readPageFromResource("prince-charles-putin-hitler-comparison-latest-string-gaffes");
    HTMLDocumentSegmenter seg = new TheGuardianSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 1;
    assertEquals(expectedResults, result.size());
    
    // ///////// Test Main item //////////////
    SimpleItem main = result.get(0);// The main details item at
    assertEquals("Prince Charles's Putin and Hitler comparison is latest in string of gaffes", main.title);
    String body = " Prince Charles's aides can hardly have expected camera crews camped outside Clarence House when they waved him off to Canada on the latest of his official overseas tours";
    assertEquals(body, main.body.split("\\.")[0]);
    assertEquals("Caroline Davies", main.author);
    assertEquals(null, main.authorLocation);
    assertEquals(null, main.comments);
    assertEquals("Native score", null, main.getScore_native());
    assertEquals("Websays score", null, main.getScore_websays());
    
    assertEquals(null, main.votes);
    assertEqualTimes(DatatypeConverter.parseDateTime("2014-05-21T17:39:15+01:00").getTime(), main.date);
    assertEquals(url.toString(), main.postUrl);
    assertEquals(null, main.itemUrl);
    assertEquals(1, main.position);
    String expectedKey = "3pe2e";
    assertEquals(expectedKey, main.threadId);
    assertEquals(1, main.threadURLs.size());
    assertEquals(makeExpectedThreadURL(expectedKey), main.threadURLs.get(0));
  }
  
  @Test
  public void testPage22Enero2016() throws Exception {
    
    URL url = new URL("http://www.theguardian.com/business/2014/jul/21/housing-market-cools-down-weather-warms-up");
    StringBuilder content = readPageFromResource("theGuardian-housing-market-cools-down-weather-warms-up");
    HTMLDocumentSegmenter seg = new TheGuardianSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 1;
    assertEquals(expectedResults, result.size());
    
    // ///////// Test Main item //////////////
    SimpleItem main = result.get(0);// The main details item at
    assertEquals("Housing market cools down as weather warms up", main.title);
    String body = "The prices demanded by sellers putting their homes on the market have fallen for the first time this year, in the latest sign that some of the heat is coming out of the UK housing market";
    assertEquals(body, main.body.split("\\.")[0]);
    assertEquals("Angela Monaghan", main.author);
    assertEquals(null, main.authorLocation);
    assertEquals(null, main.comments);
    assertEquals("Native score", null, main.getScore_native());
    assertEquals("Websays score", null, main.getScore_websays());
    
    assertEquals(null, main.votes);
    assertEqualTimes(DatatypeConverter.parseDateTime("2016-01-15T09:02:02+01:00").getTime(), main.date);
    assertEquals(url.toString(), main.postUrl);
    assertEquals(null, main.itemUrl);
    assertEquals(1, main.position);
    String expectedKey = "4v4pv";
    assertEquals(expectedKey, main.threadId);
    assertEquals(1, main.threadURLs.size());
    assertEquals(makeExpectedThreadURL(expectedKey), main.threadURLs.get(0));
  }
  
  @Test
  public void testBody() throws Exception {
    
    URL url = new URL("http://www.theguardian.com/world/2013/oct/03/edward-snowden-files-john-lanchester#comment-27612664");
    StringBuilder content = readPageFromResource("theGuardian.html");
    HTMLDocumentSegmenter seg = new TheGuardianSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 1;
    assertEquals(expectedResults, result.size());
    
    // ///////// Test Main item //////////////
    SimpleItem main = result.get(0);// The main details item at
    assertEquals("The Snowden files: why the British public should be worried about GCHQ", main.title);
    String body = "In August, the editor of the Guardian rang me up and asked if I would spend a week in New York, reading the GCHQ files whose UK copy the Guardian was forced to destroy";
    assertEquals(body, (main.body.split("\\."))[0]);
  }
}