/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class TripAdvisorSegmenterTest extends SegmenterTest {
  
  private static final Logger logger = Logger.getLogger(TripAdvisorSegmenterTest.class);
  
  /**
   * Test of segment method, of class TripAdvisorSegmenter.
   */
  @Test
  public void testSegment1() throws Exception {
    
    URL url = new URL(
        "http://www.tripadvisor.com/ShowUserReviews-g562820-d500094-r170085344-Parque_Cristobal_Tenerife-Playa_de_las_Americas_Tenerife_Canary_Islands.html#CHECK_RATES_CONT");
    
    StringBuilder content = readPageFromResource("Review of Parque Cristobal Tenerife, Playa de las Americas, Spain - TripAdvisor.htm");
    
    Calendar cal = Calendar.getInstance();
    HTMLDocumentSegmenter seg = new TripAdvisorSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 6; // No of comments + 1 (basic details item which
    // you expect in the page)
    assertEquals(expectedResults, result.size());
    
    // ///////// Test Main item //////////////
    SimpleItem main = result.get(0);// The main details item at
    // index 0 (first
    // element)
    assertEquals("Parque Cristobal Tenerife: Traveler Reviews", main.title);
    assertEquals("Avda. Rafael Puig Llivina, 7, 38660 Playa de las Americas, Tenerife, Spain", main.body);
    assertEquals(null, main.author);
    assertEquals(null, main.authorLocation);
    assertEquals(new Integer(318), main.reviews);
    assertEquals("Native score", new Integer(40), main.getScore_native());
    assertEquals("Websays score", new Integer(80), main.getScore_websays());
    assertEquals(null, main.votes);
    
    assertEqualDates(new Date(), main.date); // Main item in a facility review has no date by itself, so indexing date is provided, and therefore
                                             // tested.
    assertEquals(
        "http://www.tripadvisor.com/ShowUserReviews-g562820-d500094-r170085344-Parque_Cristobal_Tenerife-Playa_de_las_Americas_Tenerife_Canary_Islands.html",
        main.itemUrl);
    assertEquals(url.toString(), main.postUrl);
    assertEquals(1, main.position);
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = result.get(1);
    assertEquals("We keep going back", sub1.title);
    assertEquals(
        "This is our second time here, it's clean, friendly staff can't do enough for you, mix up on our rooms but got that sorted out with a pool view, up early at 6am to do the lounges, breakfast then sit by the pool. Went to water park twice best time to go is 2,30 close at 6pm back runtime for tea, food is fantastic, with a children's section, bbq on Fridays, water polo for teens and adults, pingpong and pool, laundry room to wash and dry cloths teen entertainment at 7pm all round entertainment from 9.30 then in the pool bar till 11pm then over to Dublin bar till 6am, rooms cleaned daily we went July temps were 38c to 40c came back with a tan, met some lovely people from Ripon not far from us in Leeds, hoping to go back again and every year after that........give it a go you'll want to go back",
        sub1.body);
    assertEquals("callaghanjan", sub1.author);
    assertEquals(null, sub1.authorLocation);
    // assertEquals(new Integer(1), sub1.reviews);
    assertEquals("Native score", new Integer(50), sub1.getScore_native());
    assertEquals("Websays score", new Integer(100), sub1.getScore_websays());
    assertEquals(null, sub1.votes);
    
    cal.set(2013, 6, 31);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), sub1.date);
    /* This is null because the outdated page downloaded has no links to individual comments pages */
    assertEquals(null, sub1.itemUrl);
    assertEquals(url.toString(), sub1.postUrl);
    assertEquals(2, sub1.position);
    
    // ///////// Test Second comment item //////////////
    SimpleItem sub2 = result.get(2);
    assertEquals("Lovely place =)", sub2.title);
    assertEquals(
        "I took myself and my three boys here and we all loved it,location was perfect,I would recommend any family to go here.They are all very friendly & make you feel welcome.Theres plenty to do local and the beach is near by.The food isnt perfect but its ok theres always a long selection of different food depends what you like but just got back from mallorca which was lovely but sadly the hotel was 'Terrible' so yeah defo will be going back to tenerife.The people who do the entertainment here for the children are really good.we wasnt sure about this place but as soon as we got here we knew we had picked the right place.It was very clean,no air-con but there was fans,a flat screen tv a cooker and microwave,and a small fridge freezer,kettle/toaster.etc..The maids were nice and was always busy making sure we had clean bedding & towels.You also get a safe in the room,i would advice anybody to always use a safe because you never know what might happen.Im looking forward to going back.",
        sub2.body);
    assertEquals("missnoon", sub2.author);
    assertEquals("Nottingham, England, United Kingdom", sub2.authorLocation);
    // assertEquals(new Integer(2), sub2.reviews);
    assertEquals("Native score", new Integer(50), sub2.getScore_native());
    assertEquals("Websays score", new Integer(100), sub2.getScore_websays());
    
    assertEquals(new Integer(1), sub2.votes);
    
    cal.set(2013, 6, 28);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), sub2.date);
    /* This is null because the outdated page downloaded has no links to individual comments pages */
    assertEquals(null, sub2.itemUrl);
    assertEquals(url.toString(), sub2.postUrl);
    assertEquals(3, sub2.position);
    
    // ///////// Test Third comment item //////////////
    SimpleItem sub3 = result.get(3);
    assertEquals("Great place", sub3.title);
    assertEquals(
        "After reading some of the reviews on hear before we went I've got to say I was a little concerned incase I did not like it but we had a brilliant holiday could not fault the hotel for anything the entertainment at night could have been better but my little girl who was 10 when we were there loved it and that's all that mattered to me the food was presented beautiful but I'm quite a fussy eater so I spent two weeks eating chips but that was because I would not try different things the best night for all of us was the variety show well done again to all the animation team you certainly made the crowd laugh, a very friendly hotel all staff were amazing would I go back yes but not for 14 nights",
        sub3.body);
    assertEquals("Clackmannan", sub3.author);
    assertEquals(null, sub3.authorLocation);
    // assertEquals(new Integer(1), sub3.reviews);
    assertEquals("Native score", new Integer(40), sub3.getScore_native());
    assertEquals("Websays score", new Integer(80), sub3.getScore_websays());
    
    assertEquals(new Integer(2), sub3.votes);
    
    cal.set(2013, 6, 22);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), sub3.date);
    assertEquals(null, sub3.itemUrl);
    assertEquals(url.toString(), sub3.postUrl);
    assertEquals(4, sub3.position);
    
    // ///////// Test Forth comment item //////////////
    SimpleItem sub4 = result.get(4);
    assertEquals("Family", sub4.title);
    assertEquals(
        "Just returned from a week all inclusive in this complex there was eight of us there and we had a brilliant time .the food was great plenty of choice,drinks were also fine .ThE hotel was in a great location and was very clean and the staff were all lovely .The only thing I would like to fault was the entertainment at night I found it very boring and there wasn't much on .THe kids went to the kids club and my wee boy loved the football that was on but it was mostly just the kids arranging it between themselves .There was loads of family's there and there was loads of entertainers but as we sat at the pool with the kids slides we didn't seem to be included in what was on at the other pool if the entertainers went between the two pools it would be betterb",
        sub4.body);
    assertEquals("clare12395", sub4.author);
    assertEquals("Belfast, United Kingdom", sub4.authorLocation);
    // assertEquals(new Integer(1), sub4.reviews);
    assertEquals("Native score", new Integer(40), sub4.getScore_native());
    assertEquals("Websays score", new Integer(80), sub4.getScore_websays());
    
    assertEquals(null, sub4.votes);
    
    cal.set(2013, 6, 20);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), sub4.date);
    assertEquals(null, sub4.itemUrl);
    assertEquals(url.toString(), sub4.postUrl);
    assertEquals(5, sub4.position);
    
    // ///////// Test Fifth comment item //////////////
    SimpleItem sub5 = result.get(5);
    assertEquals("Another superb stay at the cristobel", sub5.title);
    assertEquals(
        "This was our second visit to this complex with our daughter now 5 and my in laws. This second stay was just as fantastic as the first. We cannot fault anything. The biggest improvement was the animation team headed by Sam who is 1st class giving all guests her attention from morning till night. I am astonished to have read a recent poor review listed and must say that a lot of the points are complete fabrication. The choice of food and it's presentation is really good and my hubby is an accredited head chef so knows exactly what it taken to put out the quantity and quality of food this complex provides its guests every day. All the staff in all areas take pride in their work. I can say nothing more than to recommend this complex to other families. We will be coming back again . First class",
        sub5.body);
    assertEquals("kmdixon", sub5.author);
    assertEquals("Newcastle upon Tyne, United Kingdom", sub5.authorLocation);
    // assertEquals(new Integer(2), sub5.reviews);
    assertEquals("Native score", new Integer(50), sub5.getScore_native());
    assertEquals("Websays score", new Integer(100), sub5.getScore_websays());
    
    assertEquals(new Integer(1), sub5.votes);
    
    cal.set(2013, 6, 20);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), sub5.date);
    assertEquals(null, sub5.itemUrl);
    assertEquals(url.toString(), sub5.postUrl);
    assertEquals(6, sub5.position);
    
  }
  
  /**
   * Test of segment method, of class TripAdvisorSegmenter. Ignored because it is completely outdated.
   */
  @Ignore
  @Test
  public void testSegment2() throws Exception {
    
    URL url = new URL("http://www.tripadvisor.es/ShowUserReviews-g187514-d803063-r172123804-T_G_I_Friday_s-Madrid.html#REVIEWS");
    
    StringBuilder content = readPageFromResource("Comida franquicia americana con personal desmotivado - Opiniones de viajeros sobre T.G.I. Friday's, Madrid - TripAdvisor.htm");
    
    Calendar cal = Calendar.getInstance();
    HTMLDocumentSegmenter seg = new TripAdvisorSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 6; // No of comments + 1 (basic details item which
    // you expect in the page)
    assertEquals(expectedResults, result.size());
    
    // ///////// Test Main item //////////////
    SimpleItem main = result.get(0);// The main details item at
    // index 0 (first
    // element)
    assertEquals("T.G.I. Friday's: Opiniones del restaurante", main.title);
    assertEquals("Gran Vía, 76, Madrid, España", main.body);
    assertEquals(null, main.author);
    assertEquals(null, main.authorLocation);
    assertEquals(new Integer(137), main.reviews);
    assertEquals("Native score", new Integer(35), main.getScore_native());
    assertEquals("Websays score", new Integer(70), main.getScore_websays());
    
    assertEquals(null, main.votes);
    
    assertEqualDates(new Date(), main.date);
    assertEquals("http://www.tripadvisor.es/ShowUserReviews-g187514-d803063-r172123804-T_G_I_Friday_s-Madrid.html", main.itemUrl);
    assertEquals(url.toString(), main.postUrl);
    assertEquals(1, main.position);
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = result.get(1);
    assertEquals("Comida franquicia americana con personal desmotivado", sub1.title);
    assertEquals(
        "Fuí a la esquina del bernabeu, era el cumple de mi hijo y recordaba que antiguamente te montaban un numerito. Pues nada, atención penosa del personal, comida normalita que esperas de un sitio de estos, pero lo de la atención de los camareros fue lamentable, ni una sonrisa, desganados, ni siquiera le ofrecieron unos globos al niño avisándo que era su cumple y los globos ya hinchados. Propina a la altura de la atención 0€.",
        sub1.body);
    assertEquals("yojorste", sub1.author);
    assertEquals(null, sub1.authorLocation);
    // assertEquals(new Integer(31), sub1.reviews);
    assertEquals("Native score", new Integer(20), sub1.getScore_native());
    assertEquals("Websays score", new Integer(40), sub1.getScore_websays());
    
    assertEquals(new Integer(21), sub1.votes);
    
    cal.set(2013, 7, 13);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), sub1.date);
    assertEquals(null, sub1.itemUrl);
    assertEquals(url.toString(), sub1.postUrl);
    assertEquals(2, sub1.position);
    
    // ///////// Test Second comment item //////////////
    SimpleItem sub2 = result.get(2);
    assertEquals("La mejor salsa Jack Daniels", sub2.title);
    assertEquals(
        "La comida en general esta muy buena, tienen varios platos que llevan salsa Jack Daniels, que esta buenísima, muy muy recomendable. EL personal es muy simpático en todos los restaurantes que he visitado, la bebida es coca cola refill, si el restaurante no esta muy lleno no tienes ni que decir que te lo rellenen. si pides mas salsa no te lo cobran, en los menús también entra refill, y aunque puede llegar a salir un poco caro, sales llenisimo, el menú está muy bien, te ponen la misma cantidad/tamaño y el precio del menú si esta muy bien. Siempre que tengo una oportunidad, voy. Ademas a veces tienen 2x1 y con la tarjeta VIPS te mandan también ofertas y vales.",
        sub2.body);
    assertEquals("yuikoyasu", sub2.author);
    assertEquals("Leganés, España", sub2.authorLocation);
    // assertEquals(new Integer(10), sub2.reviews);
    assertEquals("Native score", new Integer(50), sub2.getScore_native());
    assertEquals("Websays score", new Integer(100), sub2.getScore_websays());
    
    assertEquals(new Integer(15), sub2.votes);
    
    cal.set(2013, 7, 1);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), sub2.date);
    assertEquals(null, sub2.itemUrl);
    assertEquals(url.toString(), sub2.postUrl);
    assertEquals(3, sub2.position);
    
    // ///////// Test Fifth comment item //////////////
    SimpleItem sub5 = result.get(5);
    assertEquals("Pésimo servicio, calidad muy floja.", sub5.title);
    assertEquals(
        "Para no repetir. El típico sitio que no merece la pena, en mi opinión. Los camareros despistados, la comida tardó una eternidad en llegar, la calidad mediana, los postres tardaron otra barbaridad. Un desastre,",
        sub5.body);
    assertEquals("Navegador17", sub5.author);
    assertEquals("Madrid, España", sub5.authorLocation);
    // assertEquals(new Integer(44), sub5.reviews);
    assertEquals("Native score", new Integer(20), sub5.getScore_native());
    assertEquals("Websays score", new Integer(40), sub5.getScore_websays());
    
    assertEquals(new Integer(9), sub5.votes);
    
    cal.set(2013, 5, 12);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), sub5.date);
    assertEquals(null, sub5.itemUrl);
    assertEquals(url.toString(), sub5.postUrl);
    assertEquals(6, sub5.position);
    
  }
  
  /**
   * Test of segment method, of class TripAdvisorSegmenter.
   */
  @Test
  public void testSegment3() throws Exception {
    
    URL url = new URL("http://www.tripadvisor.es/ShowUserReviews-g187514-d803063-r172123804-T_G_I_Friday_s-Madrid.html#REVIEWS");
    
    HTMLDocumentSegmenter seg = new TripAdvisorSegmenter();
    
    // First do the test from resource file
    StringBuilder content = readPageFromResource("Comida franquicia americana con personal desmotivado - Opiniones de viajeros sobre T.G.I. Friday's, Madrid - TripAdvisor.htm");
    // System.out.println("===\n" + content.toString() + "\n===");
    List<SimpleItem> result = seg.segment(content.toString(), url);
    friday1Test(url, result, false);
    
    // // Now do the test form URL and make sure it format has not changed
    // InputStream is = url.openStream();
    // String contentS = IOUtils.toString(is);
    // result = seg.segment(contentS, url);
    // friday1Test(url, result, true);
    
    // Now same page, downloaded by simpleFetcher
    content = readPageFromResource("tripAdvisor_v2.html");
    // System.out.println("===\n" + content.toString() + "\n===");
    result = seg.segment(content.toString(), url);
    friday1Test(url, result, true);
    
  }
  
  /**
   * This test is testing a page that only contains an expanded comment. is ignored because the pages that contains expanded comment have a subset of
   * the information that can be get in the individual review pages.
   */
  @Ignore
  @Test
  public void SegmentExpandedComment() {
    
    URL url = null;
    String urlString = "http://www.tripadvisor.es/ExpandedUserReviews-g187514-d803063?context=1&expand=1&target=282901408&reviews=282901408&orig_url=http://www.tripadvisor.es/Restaurant_Review-g187514-d803063-Reviews-TGI_Fridays_Gran_Via-Madrid.html";
    try {
      url = new URL(urlString);
    } catch (MalformedURLException e) {
      fail("Error during the generation of the URL from string [" + urlString + "]. Reason [" + e.getMessage() + "]");
    }
    
    HTMLDocumentSegmenter seg = new TripAdvisorSegmenter();
    
    // First do the test from resource file
    StringBuilder content = null;
    String urlFile = "TripAdvisor-TGI Fridays-ExpandedComment.html";
    try {
      content = readPageFromResource(urlFile);
    } catch (IOException e) {
      fail("Error openning [" + urlFile + "]. Reason [" + e.getMessage() + "]");
    }
    List<SimpleItem> result = null;
    try {
      result = seg.segment(content.toString(), url);
    } catch (Exception e) {
      fail("Error during the \"segment\" method. Reason [" + e.getMessage() + "]");
    }
    
    assertEquals("Number of simpleItems in result ", 2, result.size());
    
    SimpleItem main = result.get(0);
    assertEquals("itemUrl", "http://www.tripadvisor.es/Restaurant_Review-g187514-d803063-Reviews-TGI_Fridays_Gran_Via-Madrid.html",
        main.itemUrl);
    assertEquals("postUrl", "http://www.tripadvisor.es/Restaurant_Review-g187514-d803063-Reviews-TGI_Fridays_Gran_Via-Madrid.html",
        main.postUrl);
    assertEquals("threadID", "http://www.tripadvisor.es/Restaurant_Review-g187514-d803063-Reviews-TGI_Fridays_Gran_Via-Madrid.html",
        main.postUrl);
    assertTrue("isFake", main.fakeItem);
    
    SimpleItem sub1 = result.get(1);
    
    assertEquals("title", "Divertido, familiar, buena atención", sub1.title);
    assertEquals(
        "body",
        "Suelo ir bastantes veces al TGI Fridays de Gran Vía en Madrid. Últimamente más al after work de cócteles que me encantan. Gente profesional, bien formada, sonrisas y buenos precios. Tal vez daría alguna opción más saludable en la carta porque sano, sano, no es. Pero todo está muy rico. En ocasiones me ha parecido ver falta de personal lo que ralentiza mucho el servicio",
        sub1.body);
    assertEquals("author", "Antonio R", sub1.author);
    assertEquals("authorLocation", "Barcelona", sub1.authorLocation);
    assertEquals("votes", new Integer(9), sub1.votes);
    
    assertEquals("itemUrl", "http://www.tripadvisor.es/ShowUserReviews-g187514-d803063-r282901408-TGI_Fridays_Gran_Via-Madrid.html",
        sub1.itemUrl);
    assertEquals("postUrl", "http://www.tripadvisor.es/Restaurant_Review-g187514-d803063-Reviews-TGI_Fridays_Gran_Via-Madrid.html",
        sub1.postUrl);
    assertEquals("threadID", "http://www.tripadvisor.es/Restaurant_Review-g187514-d803063-Reviews-TGI_Fridays_Gran_Via-Madrid.html",
        sub1.threadId);
    Calendar cal = Calendar.getInstance();
    cal.set(2015, 5, 25, 0, 0, 0);// Year, Month(zero based), Date
    cal.set(Calendar.MILLISECOND, 0);
    assertEquals("date", cal.getTime(), sub1.date);
    assertTrue("isComment", sub1.isComment);
    assertEquals("Position ", 2, sub1.position);
    
  }
  
  /**
   * This test is testing the segmenter using a review downloaded during June 2015.
   */
  @Test
  public void testSegmentJune2015() {
    
    URL url = null;
    String urlString = "http://www.tripadvisor.es/Restaurant_Review-g187514-d803063-Reviews-TGI_Fridays_Gran_Via-Madrid.html";
    try {
      url = new URL(urlString);
    } catch (MalformedURLException e) {
      fail("Error during the generation of the URL from string [" + urlString + "]. Reason [" + e.getMessage() + "]");
    }
    
    HTMLDocumentSegmenter seg = new TripAdvisorSegmenter();
    
    // First do the test from resource file
    StringBuilder content = null;
    String urlFile = "TripAdvisor-TGI Fridays-20150629.html";
    try {
      content = readPageFromResource(urlFile);
    } catch (IOException e) {
      fail("Error openning [" + urlFile + "]. Reason [" + e.getMessage() + "]");
    }
    List<SimpleItem> result = null;
    try {
      result = seg.segment(content.toString(), url);
    } catch (Exception e) {
      fail("Error during the \"segment\" method. Reason [" + e.getMessage() + "]");
    }
    
    /*******************
     * Uncomment to get results in the screen SimpleItem main1 = result.get(0); System.out.println(result.size()); System.out.println("main1.title" +
     * main1.title); System.out.println("main1.body" + main1.body); System.out.println("main1.author" + main1.author);
     * System.out.println("main1.authorLocation" + main1.authorLocation); System.out.println("main1.reviews" + main1.reviews);
     * System.out.println("main1.score" + main1.score); System.out.println("main1.votes" + main1.votes); System.out.println("main1.itemUrl" +
     * main1.itemUrl); System.out.println("main1.postUrl" + main1.postUrl); System.out.println("main1.date" + main1.date);
     * 
     * SimpleItem temporal = result.get(1); System.out.println("temporal.title" + temporal.title); System.out.println("temporal.body" +
     * temporal.body); System.out.println("temporal.author" + temporal.author); System.out.println("temporal.authorLocation" +
     * temporal.authorLocation); System.out.println("temporal.reviews" + temporal.reviews); System.out.println("temporal.score" + temporal.score);
     * System.out.println("temporal.votes" + temporal.votes); System.out.println("temporal.itemUrl" + temporal.itemUrl);
     * System.out.println("temporal.postUrl" + temporal.postUrl); System.out.println("temporal.date" + temporal.date);
     * System.out.println("temporal.position" + temporal.position);
     * System.out.println("=========================================================================================");
     * 
     * temporal = result.get(2); System.out.println("temporal.title" + temporal.title); System.out.println("temporal.body" + temporal.body);
     * System.out.println("temporal.author" + temporal.author); System.out.println("temporal.authorLocation" + temporal.authorLocation);
     * System.out.println("temporal.reviews" + temporal.reviews); System.out.println("temporal.score" + temporal.score);
     * System.out.println("temporal.votes" + temporal.votes); System.out.println("temporal.itemUrl" + temporal.itemUrl);
     * System.out.println("temporal.postUrl" + temporal.postUrl); System.out.println("temporal.date" + temporal.date);
     * System.out.println("temporal.position" + temporal.position);
     * System.out.println("=========================================================================================");
     * 
     * temporal = result.get(3); System.out.println("temporal.title" + temporal.title); System.out.println("temporal.body" + temporal.body);
     * System.out.println("temporal.author" + temporal.author); System.out.println("temporal.authorLocation" + temporal.authorLocation);
     * System.out.println("temporal.reviews" + temporal.reviews); System.out.println("temporal.score" + temporal.score);
     * System.out.println("temporal.votes" + temporal.votes); System.out.println("temporal.itemUrl" + temporal.itemUrl);
     * System.out.println("temporal.postUrl" + temporal.postUrl); System.out.println("temporal.date" + temporal.date);
     * System.out.println("temporal.position" + temporal.position);
     * System.out.println("=========================================================================================");
     * 
     * temporal = result.get(4); System.out.println("temporal.title" + temporal.title); System.out.println("temporal.body" + temporal.body);
     * System.out.println("temporal.author" + temporal.author); System.out.println("temporal.authorLocation" + temporal.authorLocation);
     * System.out.println("temporal.reviews" + temporal.reviews); System.out.println("temporal.score" + temporal.score);
     * System.out.println("temporal.votes" + temporal.votes); System.out.println("temporal.itemUrl" + temporal.itemUrl);
     * System.out.println("temporal.postUrl" + temporal.postUrl); System.out.println("temporal.date" + temporal.date);
     * System.out.println("temporal.position" + temporal.position);
     * System.out.println("=========================================================================================");
     * 
     * temporal = result.get(5); System.out.println("temporal.title" + temporal.title); System.out.println("temporal.body" + temporal.body);
     * System.out.println("temporal.author" + temporal.author); System.out.println("temporal.authorLocation" + temporal.authorLocation);
     * System.out.println("temporal.reviews" + temporal.reviews); System.out.println("temporal.score" + temporal.score);
     * System.out.println("temporal.votes" + temporal.votes); System.out.println("temporal.itemUrl" + temporal.itemUrl);
     * System.out.println("temporal.postUrl" + temporal.postUrl); System.out.println("temporal.date" + temporal.date);
     * System.out.println("temporal.position" + temporal.position);
     * System.out.println("=========================================================================================");
     ***************************/
    
    // Let's check the segmented items
    
    int expectedResults = 8; // No of comments + 1 (basic details item which you expect in the page)
    assertEquals(expectedResults, result.size());
    
    // ///////// Test Main item //////////////
    SimpleItem main = result.get(0);// The main details item at index 0 (first element)
    assertEquals("TGI Fridays", main.title);
    assertEquals("Calle Gran Vía, 76, 28013 Madrid, España", main.body);
    assertEquals(null, main.author);
    assertEquals(null, main.authorLocation);
    assertEquals(new Integer(279), main.reviews);
    assertEquals("Native score", new Integer(35), main.getScore_native());
    assertEquals("Websays score", new Integer(70), main.getScore_websays());
    
    assertEquals(null, main.votes);
    
    Calendar cal = Calendar.getInstance();
    assertEqualDates(new Date(), main.date);
    assertEquals("http://www.tripadvisor.es/Restaurant_Review-g187514-d803063-Reviews-TGI_Fridays_Gran_Via-Madrid.html", main.itemUrl);
    assertEquals(url.toString(), main.postUrl);
    assertEquals(1, main.position);
    
    assertNotNull("Main threadURLs must not be null as there is a pagination link and some long comments.", main.threadURLs);
    
    // ///////// Test First comment item //////////////
    SimpleItem sub3 = result.get(1);
    
    // SimpleItem temporal = result.get(3);
    // System.out.println("temporal.title" + temporal.title);
    // System.out.println("temporal.body" + temporal.body);
    // System.out.println("temporal.author" + temporal.author);
    // System.out.println("temporal.authorLocation" + temporal.authorLocation);
    // System.out.println("temporal.reviews" + temporal.reviews);
    // System.out.println("temporal.votes" + temporal.votes);
    // System.out.println("temporal.itemUrl " + temporal.itemUrl);
    // System.out.println("temporal.postUrl " + temporal.postUrl);
    // System.out.println("temporal.threadID " + temporal.threadId);
    // if (temporal.threadURLs != null) {
    // System.out.println("temporal.threadURLs " + Arrays.toString(temporal.threadURLs.toArray()));
    // }
    // System.out.println("temporal.date" + temporal.date);
    // System.out.println("temporal.position" + temporal.position);
    // System.out.println("=========================================================================================");
    
    assertEquals("title", "Bien ambientado", sub3.title);
    assertEquals(
        "body",
        "Fryday's es un ambiente americano. A mis hijas le gusta mucho. Las costillas Jack Daniels están muy bien hechas. Los postres son ricos. El precio es un poco elevado.",
        sub3.body);
    assertEquals("author", "MGF F", sub3.author);
    assertEquals("authorLocation", "Distrito Capital Federal, Argentina", sub3.authorLocation);
    assertEquals("votes", new Integer(1), sub3.votes);
    
    assertEquals("itemUrl", "http://www.tripadvisor.es/ShowUserReviews-g187514-d803063-r281123674-TGI_Fridays-Madrid.html", sub3.itemUrl);
    assertEquals("postUrl", "http://www.tripadvisor.es/Restaurant_Review-g187514-d803063-Reviews-TGI_Fridays_Gran_Via-Madrid.html",
        sub3.postUrl);
    assertEquals("threadID", "http://www.tripadvisor.es/Restaurant_Review-g187514-d803063-Reviews-TGI_Fridays_Gran_Via-Madrid.html",
        sub3.threadId);
    cal.set(2015, 5, 18, 0, 0, 0);// Year, Month(zero based), Date
    cal.set(Calendar.MILLISECOND, 0);
    assertEquals("date", cal.getTime(), sub3.date);
    assertTrue("isComment", sub3.isComment);
    assertEquals("Position ", 2, sub3.position);
    
  }
  
  /**
   * Tests a tripadvisor.it page that is a particular comment about a facility. This review is of type:Review-aggregate and the page is of type
   * ShowUserReviews
   */
  @Test
  public void testItalianCommentPage() {
    
    URL url = null;
    String urlString = "http://www.tripadvisor.it/ShowUserReviews-g187791-d1788652-r281618779-Osteria_Pizzeria_Margherita-Rome_Lazio.html#REVIEWS";
    try {
      url = new URL(urlString);
    } catch (MalformedURLException e) {
      fail("Error during the generation of the URL from string [" + urlString + "]. Reason [" + e.getMessage() + "]");
    }
    
    HTMLDocumentSegmenter seg = new TripAdvisorSegmenter();
    
    // First do the test from resource file
    StringBuilder content = null;
    String urlFile = "TripAdvisorOsteriaMargherita.html";
    try {
      content = readPageFromResource(urlFile);
    } catch (IOException e) {
      fail("Error openning [" + urlFile + "]. Reason [" + e.getMessage() + "]");
    }
    List<SimpleItem> result = null;
    try {
      result = seg.segment(content.toString(), url);
    } catch (Exception e) {
      e.printStackTrace();
      fail("Error during the \"segment\" method. Reason [" + e.getMessage() + "]");
    }
    
    assertEquals("Results ", 2, result.size());
    SimpleItem main1 = result.get(0);
    assertTrue("This should be a fake item and it is not. Review the parser.", main1.fakeItem);
    assertEquals("main1.postURl ",
        "http://www.tripadvisor.it/Restaurant_Review-g187791-d1788652-Reviews-Osteria_Pizzeria_Margherita-Rome_Lazio.html", main1.postUrl);
    
    main1 = result.get(1);
    assertEquals("main1.title ", main1.title, "“\"Qualità e cortesia\"”");
    assertEquals(
        "main1.body ",
        main1.body,
        "Da anni ormai vado al Margherita, mi sento sempre a casa. Sia per il cibo sempre fresco e ben cotto, sia per il servizio attento e cortese!Vi consiglio la pizza ortolana base integrale, eccezionale. Il mio dolce preferito invece è il classico Tiramisù!");
    assertEquals("main1.author ", main1.author, "Valerio P");
    assertEquals("main1.authorlocation ", main1.authorLocation, "Roma, Italia");
    // Comment pages have no reviews, there is no point in a comment having reviews, only facilities have.
    // assertEquals("main1.reveiws ", main1.reviews, new Integer(237));
    assertEquals("Native score", new Integer(50), main1.getScore_native());
    assertEquals("Websays score", new Integer(100), main1.getScore_websays());
    
    assertEquals("main1.votes ", main1.votes, null);
    assertEquals("main1.itemUrl ",
        "http://www.tripadvisor.it/ShowUserReviews-g187791-d1788652-r281618779-Osteria_Pizzeria_Margherita-Rome_Lazio.html", main1.itemUrl);
    assertEquals("main1.postURl ", null, main1.postUrl);
    Calendar cal = Calendar.getInstance();
    cal.set(2015, 5, 20);// Year, Month(zero based), Date
    assertEqualDates(main1.date, cal.getTime());
    assertEquals("main1.threadID ", null, main1.threadId);
    
  }
  
  /**
   * Tests an page from tropadvisor.it that refers to a facility (hotel, restaurant,...)
   */
  @Test
  public void testItalianMainPage() {
    
    URL url = null;
    String urlString = "http://www.tripadvisor.it/Restaurant_Review-g187791-d1788652-Reviews-Osteria_Pizzeria_Margherita-Rome_Lazio.html";
    try {
      url = new URL(urlString);
    } catch (MalformedURLException e) {
      fail("Error during the generation of the URL from string [" + urlString + "]. Reason [" + e.getMessage() + "]");
    }
    
    HTMLDocumentSegmenter seg = new TripAdvisorSegmenter();
    
    // First do the test from resource file
    StringBuilder content = null;
    String urlFile = "TripAdvisorOsteriaMargheritaMain.html";
    try {
      content = readPageFromResource(urlFile);
    } catch (IOException e) {
      fail("Error openning [" + urlFile + "]. Reason [" + e.getMessage() + "]");
    }
    List<SimpleItem> result = null;
    try {
      result = seg.segment(content.toString(), url);
    } catch (Exception e) {
      String error = "Error during the \"segment\" method. Reason [" + e.getMessage() + "]";
      logger.error(error, e);
      fail(error);
    }
    
    int expectedResults = 10; // No of comments + 1 (basic details item which you expect in the page)
    assertEquals(expectedResults, result.size());
    
    // ///////// Test Main item //////////////
    SimpleItem main = result.get(0);// The main details item at index 0 (first element)
    assertEquals("Osteria Pizzeria Margherita", main.title);
    assertEquals("Via delle Coppelle, 34, 00186 Roma, Italia", main.body);
    assertEquals("Main.author", null, main.author);
    assertEquals(null, main.authorLocation);
    assertEquals("Main.reviews", main.reviews, new Integer(237));
    assertEquals("Native score", new Integer(35), main.getScore_native());
    assertEquals("Websays score", new Integer(70), main.getScore_websays());
    
    assertEquals("Main.votes ", null, main.votes);
    
    assertEqualDates(new Date(), main.date);
    assertEquals("Main.itemurl",
        "http://www.tripadvisor.it/Restaurant_Review-g187791-d1788652-Reviews-Osteria_Pizzeria_Margherita-Rome_Lazio.html", main.itemUrl);
    assertEquals("Main.postUrl", url.toString(), main.postUrl);
    assertEquals("Main.position", 1, main.position);
    assertEquals("Main.threadID ",
        "http://www.tripadvisor.it/Restaurant_Review-g187791-d1788652-Reviews-Osteria_Pizzeria_Margherita-Rome_Lazio.html", main.threadId);
    
    assertFalse("Main.isComment ", main.isComment);
    assertNotNull("Main.threadURLs", main.threadURLs);
    assertEquals("Long comment threadURL ",
        "http://www.tripadvisor.it/ShowUserReviews-g187791-d1788652-r273446163-Osteria_Pizzeria_Margherita-Rome_Lazio.html",
        main.threadURLs.get(0));
    
    assertEquals("Long comment next comments URL ",
        "http://www.tripadvisor.it/Restaurant_Review-g187791-d1788652-Reviews-or10-Osteria_Pizzeria_Margherita-Rome_Lazio.html#REVIEWS",
        main.threadURLs.get(1));
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = result.get(1);
    assertEquals("Item.title ", "Da ritornare assolutamente. ", sub1.title);
    assertEquals(
        "Item.body ",
        "Siamo andati con gli amici a pranzo capitati per caso con il dubbio che fosse il solito ristorante turistico ma siamo rimasti sorpresi. Cibo di qualità servizio rapido e cortese e il conto nella media da ritornare assolutamente.",
        sub1.body);
    assertEquals("Item.author ", "sveva1989", sub1.author);
    assertEquals("Item.authorLocation ", "Roma, Italia", sub1.authorLocation);
    
    assertEquals("Item.votes ", null, sub1.votes);
    // assertEquals("Item.reviews ", new Integer(1), sub1.reviews);
    assertEquals("Native score", new Integer(50), sub1.getScore_native());
    assertEquals("Websays score", new Integer(100), sub1.getScore_websays());
    
    Calendar cal = Calendar.getInstance();
    cal.set(2015, 5, 21);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), sub1.date);
    assertEquals("Item.itemUrl ",
        "http://www.tripadvisor.it/ShowUserReviews-g187791-d1788652-r281788234-Osteria_Pizzeria_Margherita-Rome_Lazio.html", sub1.itemUrl);
    assertEquals(url.toString(), sub1.postUrl);
    assertEquals(2, sub1.position);
    assertEquals("sub1.threadID ",
        "http://www.tripadvisor.it/Restaurant_Review-g187791-d1788652-Reviews-Osteria_Pizzeria_Margherita-Rome_Lazio.html", sub1.threadId);
    
    assertTrue("sub1.isComment ", sub1.isComment);
    
    // ///////// Test Second comment item //////////////
    SimpleItem sub2 = result.get(2);
    assertEquals("Item2.title ", "\"Qualità e cortesia\"", sub2.title);
    assertEquals(
        "Item2.body ",
        "Da anni ormai vado al Margherita, mi sento sempre a casa. Sia per il cibo sempre fresco e ben cotto, sia per il servizio attento e cortese!Vi consiglio la pizza ortolana base integrale, eccezionale. Il mio dolce preferito invece è il classico Tiramisù!",
        sub2.body);
    assertEquals("Item2.author ", "Valerio P", sub2.author);
    assertEquals("Item2.authorLocation ", "Roma, Italia", sub2.authorLocation);
    // assertEquals("Item2.reviews ", new Integer(6), sub2.reviews);
    assertEquals("Native score", new Integer(50), sub2.getScore_native());
    assertEquals("Websays score", new Integer(100), sub2.getScore_websays());
    
    assertEquals("ITem2.votes ", null, sub2.votes);
    
    cal.set(2015, 5, 20);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), sub2.date);
    assertEquals("Item2.itemUrl ",
        "http://www.tripadvisor.it/ShowUserReviews-g187791-d1788652-r281618779-Osteria_Pizzeria_Margherita-Rome_Lazio.html", sub2.itemUrl);
    assertEquals("Item2.postURL ", url.toString(), sub2.postUrl);
    assertEquals("Item2.position ", 3, sub2.position);
    assertEquals("Item2.threadID ",
        "http://www.tripadvisor.it/Restaurant_Review-g187791-d1788652-Reviews-Osteria_Pizzeria_Margherita-Rome_Lazio.html", sub2.threadId);
    assertTrue("sub2.isComment ", sub2.isComment);
    
  }
  
  /**
   * Test a restaurant main page downloaded during september 2015
   */
  @Test
  public void testRestaurantLastMainPage() {
    URL url = null;
    String urlString = "http://www.tripadvisor.it/Restaurant_Review-g187791-d1469247-Reviews-or270-Osteria_Margherita-Rome_Lazio.html#REVIEWS";
    try {
      url = new URL(urlString);
    } catch (MalformedURLException e) {
      fail("Error during the generation of the URL from string [" + urlString + "]. Reason [" + e.getMessage() + "]");
    }
    
    HTMLDocumentSegmenter seg = new TripAdvisorSegmenter();
    
    // First do the test from resource file
    StringBuilder content = null;
    String urlFile = "TripAdvisorRestaurantLastMain.html";
    try {
      content = readPageFromResource(urlFile);
    } catch (IOException e) {
      fail("Error openning [" + urlFile + "]. Reason [" + e.getMessage() + "]");
    }
    List<SimpleItem> result = null;
    try {
      result = seg.segment(content.toString(), url);
    } catch (Exception e) {
      String error = "Error during the \"segment\" method. Reason [" + e.getMessage() + "]";
      logger.error(error, e);
      fail(error);
    }
    
    int expectedResults = 1; // No of comments + 1 (basic details item which you expect in the page)
    assertEquals(expectedResults, result.size());
    
    // ///////// Test Main item //////////////
    SimpleItem main = result.get(0);// The main details item at index 0 (first element)
    
    assertEquals("Main title ", "Osteria Margherita", main.title);
    assertEquals("Main body ", "Vicolo Del Cinque 31, 00153 Roma, Italia", main.body);
    assertEquals("Main.author ", null, main.author);
    assertEquals("Main.authorlocation ", null, main.authorLocation);
    assertEquals("Main.reviews ", main.reviews, new Integer(275));
    assertEquals("Native score", new Integer(35), main.getScore_native());
    assertEquals("Websays score", new Integer(70), main.getScore_websays());
    
    assertEquals("Main.votes ", null, main.votes);
    
    assertEquals("Main.itemurl", null, main.itemUrl);
    /* As it is a comments page different from the first, its post url is the canonicalized one */
    assertEquals("Main.postUrl", "http://www.tripadvisor.it/Restaurant_Review-g187791-d1469247-Reviews-Osteria_Margherita-Rome_Lazio.html",
        main.postUrl);
    assertEquals("Main.position", 1, main.position);
    assertEquals("Main.threadID ",
        "http://www.tripadvisor.it/Restaurant_Review-g187791-d1469247-Reviews-Osteria_Margherita-Rome_Lazio.html", main.threadId);
    
    assertFalse("Main.isComment ", main.isComment);
    
    assertNotNull("Main.threadURLs should contain long comments URLs ", main.threadURLs);
    
    assertEquals(
        "Main.threadURLs 0 ",
        "http://www.tripadvisor.ithttp://www.tripadvisor.it/ShowUserReviews-g187791-d1469247-r157710704-Osteria_Margherita-Rome_Lazio.html",
        main.threadURLs.get(0));
    
    assertEquals(
        "Main.threadURLs 1 ",
        "http://www.tripadvisor.ithttp://www.tripadvisor.it/ShowUserReviews-g187791-d1469247-r153729415-Osteria_Margherita-Rome_Lazio.html",
        main.threadURLs.get(1));
    
    assertEquals(
        "Main.threadURLs 2 ",
        "http://www.tripadvisor.ithttp://www.tripadvisor.it/ShowUserReviews-g187791-d1469247-r119747490-Osteria_Margherita-Rome_Lazio.html",
        main.threadURLs.get(2));
    
    assertEquals(
        "Main.threadURLs 3 ",
        "http://www.tripadvisor.ithttp://www.tripadvisor.it/ShowUserReviews-g187791-d1469247-r114980369-Osteria_Margherita-Rome_Lazio.html",
        main.threadURLs.get(3));
    
    assertEquals(
        "Main.threadURLs 4 ",
        "http://www.tripadvisor.ithttp://www.tripadvisor.it/ShowUserReviews-g187791-d1469247-r114755632-Osteria_Margherita-Rome_Lazio.html",
        main.threadURLs.get(4));
    
  }
  
  /**
   * Test a restaurant main page downloaded during september 2015
   */
  @Test
  public void testRestaurantMainPage() {
    
    URL url = null;
    String urlString = "http://www.tripadvisor.it/Restaurant_Review-g187791-d1469247-Reviews-Osteria_Margherita-Rome_Lazio.htm";
    try {
      url = new URL(urlString);
    } catch (MalformedURLException e) {
      fail("Error during the generation of the URL from string [" + urlString + "]. Reason [" + e.getMessage() + "]");
    }
    
    HTMLDocumentSegmenter seg = new TripAdvisorSegmenter();
    
    // First do the test from resource file
    StringBuilder content = null;
    String urlFile = "TripAdvisorRestaurantMain.html";
    try {
      content = readPageFromResource(urlFile);
    } catch (IOException e) {
      fail("Error openning [" + urlFile + "]. Reason [" + e.getMessage() + "]");
    }
    List<SimpleItem> result = null;
    try {
      result = seg.segment(content.toString(), url);
    } catch (Exception e) {
      String error = "Error during the \"segment\" method. Reason [" + e.getMessage() + "]";
      logger.error(error, e);
      fail(error);
    }
    
    int expectedResults = 7; // No of comments + 1 (basic details item which you expect in the page)
    assertEquals(expectedResults, result.size());
    
    // ///////// Test Main item //////////////
    SimpleItem main = result.get(0);// The main details item at index 0 (first element)
    assertEquals("Main title ", "Osteria Margherita", main.title);
    assertEquals("Main body ", "Vicolo Del Cinque 31, 00153 Roma, Italia", main.body);
    assertEquals("Main.author ", null, main.author);
    assertEquals("Main.authorlocation ", null, main.authorLocation);
    assertEquals("Main.reviews ", main.reviews, new Integer(275));
    assertEquals("Native score", new Integer(35), main.getScore_native());
    assertEquals("Websays score", new Integer(70), main.getScore_websays());
    
    assertEquals("Main.votes ", null, main.votes);
    
    assertEqualDates(null, main.date);
    assertEquals("Main.itemurl", null, main.itemUrl);
    assertEquals("Main.postUrl", url.toString(), main.postUrl);
    assertEquals("Main.position", 1, main.position);
    assertEquals("Main.threadID ",
        "http://www.tripadvisor.it/Restaurant_Review-g187791-d1469247-Reviews-Osteria_Margherita-Rome_Lazio.htm", main.threadId);
    
    assertFalse("Main.isComment ", main.isComment);
    assertNotNull("Main.threadURLs", main.threadURLs);
    
    assertEquals("Main.threadURL 0 ",
        "http://www.tripadvisor.it/ShowUserReviews-g187791-d1469247-r304242876-Osteria_Margherita-Rome_Lazio.html", main.threadURLs.get(0));
    
    assertEquals("Main.threadURL 1 ",
        "http://www.tripadvisor.it/ShowUserReviews-g187791-d1469247-r304061988-Osteria_Margherita-Rome_Lazio.html", main.threadURLs.get(1));
    
    assertEquals("Main.threadURL 2 ",
        "http://www.tripadvisor.it/ShowUserReviews-g187791-d1469247-r295236476-Osteria_Margherita-Rome_Lazio.html", main.threadURLs.get(2));
    
    assertEquals("Main.threadURL 3 ",
        "http://www.tripadvisor.it/ShowUserReviews-g187791-d1469247-r288753699-Osteria_Margherita-Rome_Lazio.html", main.threadURLs.get(3));
    
    assertEquals("Main.threadURL 4 ",
        "http://www.tripadvisor.it/Restaurant_Review-g187791-d1469247-Reviews-or10-Osteria_Margherita-Rome_Lazio.html#REVIEWS",
        main.threadURLs.get(4));
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = result.get(1);
    
    assertEquals("Sub1 title", "Non caratteristico......ma buono", sub1.title);
    assertEquals(
        "Sub1 body",
        "Siamo stati il 24/08/15 a pranzo in 4 adulti e un bambino abbiamo mangiato molto bene l'unico neo è che il locale non ha niente di caratteristico da Trastevere..ma per un buon pranzo lo consiglio. .",
        sub1.body);
    assertEquals("Sub1 author", "stogauts", sub1.author);
    assertNull("Sub1 authorLocation", sub1.authorLocation);
    assertNull("Sub1 reviews", sub1.reviews);
    assertEquals("Sub1 native score", new Integer(30), sub1.getScore_native());
    assertEquals("Sub1 websays score", new Integer(60), sub1.getScore_websays());
    
    Calendar cal = Calendar.getInstance();
    cal.set(2015, 7, 25, 0, 0, 0);// Year, Month(zero based), Date
    cal.set(Calendar.MILLISECOND, 0);
    assertEquals("Sub1 date", cal.getTime(), sub1.date);
    
    assertEquals("Sub1 votes", new Integer(14), sub1.votes);
    
    assertEquals("Sub1 itemurl",
        "http://www.tripadvisor.it/ShowUserReviews-g187791-d1469247-r303463542-Osteria_Margherita-Rome_Lazio.html", sub1.itemUrl);
    assertEquals("Sub1 postUrl", "http://www.tripadvisor.it/Restaurant_Review-g187791-d1469247-Reviews-Osteria_Margherita-Rome_Lazio.htm",
        sub1.postUrl);
    assertEquals("Sub1 position", 2, sub1.position);
    assertEquals("Sub1 threadID", "http://www.tripadvisor.it/Restaurant_Review-g187791-d1469247-Reviews-Osteria_Margherita-Rome_Lazio.htm",
        sub1.threadId);
    
  }
  
  /**
   * @param url
   * @param cal
   * @param result
   */
  public void friday1Test(URL url, List<SimpleItem> result, boolean maybeMore) {
    Calendar cal = Calendar.getInstance();
    
    int expectedResults = 6; // No of comments + 1 (basic details item which you expect in the page)
    assertEquals(expectedResults, result.size());
    
    // ///////// Test Main item //////////////
    SimpleItem main = result.get(0);// The main details item at index 0 (first element)
    assertEquals("T.G.I. Friday's: Opiniones del restaurante", main.title);
    assertEquals("Gran Vía, 76, Madrid, España", main.body);
    assertEquals(null, main.author);
    assertEquals(null, main.authorLocation);
    if (maybeMore) {
      assertTrue(137 <= main.reviews);
    } else {
      assertTrue(137 == main.reviews);
    }
    assertEquals("Native score", new Integer(35), main.getScore_native());
    assertEquals("Websays score", new Integer(70), main.getScore_websays());
    
    assertEquals("Main.votes ", null, main.votes);
    
    cal.set(2013, 7, 12);// Year, Month(zero based), Date
    // assertEqualDates(cal.getTime(), main.date);
    assertEquals("Main.itemurl", "http://www.tripadvisor.es/ShowUserReviews-g187514-d803063-r172123804-T_G_I_Friday_s-Madrid.html",
        main.itemUrl);
    assertEquals("Main.postUrl", url.toString(), main.postUrl);
    assertEquals("Main.position", 1, main.position);
    assertEquals("Main.threadId", "http://www.tripadvisor.es/ShowUserReviews-g187514-d803063-r172123804-T_G_I_Friday_s-Madrid.html",
        main.threadId);
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = result.get(1);
    assertEquals("Item.title ", "Comida franquicia americana con personal desmotivado", sub1.title);
    assertEquals(
        "Item.body ",
        "Fuí a la esquina del bernabeu, era el cumple de mi hijo y recordaba que antiguamente te montaban un numerito. Pues nada, atención penosa del personal, comida normalita que esperas de un sitio de estos, pero lo de la atención de los camareros fue lamentable, ni una sonrisa, desganados, ni siquiera le ofrecieron unos globos al niño avisándo que era su cumple y los globos ya hinchados. Propina a la altura de la atención 0€.",
        sub1.body);
    assertEquals("Item.author ", "yojorste", sub1.author);
    assertEquals(null, sub1.authorLocation);
    if (maybeMore) {
      assertTrue(sub1.votes >= 21);
      // assertTrue(sub1.reviews >= 31);
    } else {
      assertEquals(new Integer(21), sub1.votes);
      // assertEquals(new Integer(31), sub1.reviews);
      // assertEquals(new Integer(2), sub1.score);
    }
    
    cal.set(2013, 7, 13);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), sub1.date);
    assertEquals(null, sub1.itemUrl);
    assertEquals(url.toString(), sub1.postUrl);
    assertEquals(2, sub1.position);
    assertEquals("sub1.threadId", "http://www.tripadvisor.es/ShowUserReviews-g187514-d803063-r172123804-T_G_I_Friday_s-Madrid.html",
        sub1.threadId);
    
    // ///////// Test Second comment item //////////////
    SimpleItem sub2 = result.get(2);
    assertEquals("La mejor salsa Jack Daniels", sub2.title);
    assertEquals(
        "La comida en general esta muy buena, tienen varios platos que llevan salsa Jack Daniels, que esta buenísima, muy muy recomendable. EL personal es muy simpático en todos los restaurantes que he visitado, la bebida es coca cola refill, si el restaurante no esta muy lleno no tienes ni que decir que te lo rellenen. si pides mas salsa no te lo cobran, en los menús también entra refill, y aunque puede llegar a salir un poco caro, sales llenisimo, el menú está muy bien, te ponen la misma cantidad/tamaño y el precio del menú si esta muy bien. Siempre que tengo una oportunidad, voy. Ademas a veces tienen 2x1 y con la tarjeta VIPS te mandan también ofertas y vales.",
        sub2.body);
    assertEquals("yuikoyasu", sub2.author);
    assertEquals("Leganés, España", sub2.authorLocation);
    // assertEquals(new Integer(10), sub2.reviews);
    assertEquals("Native score", new Integer(50), sub2.getScore_native());
    assertEquals("Websays score", new Integer(100), sub2.getScore_websays());
    
    assertEquals(new Integer(15), sub2.votes);
    
    cal.set(2013, 7, 1);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), sub2.date);
    assertEquals(null, sub2.itemUrl);
    assertEquals(url.toString(), sub2.postUrl);
    assertEquals(3, sub2.position);
    assertEquals("sub2.threadId", "http://www.tripadvisor.es/ShowUserReviews-g187514-d803063-r172123804-T_G_I_Friday_s-Madrid.html",
        sub2.threadId);
    
    // ///////// Test Fifth comment item //////////////
    SimpleItem sub5 = result.get(5);
    assertEquals("Pésimo servicio, calidad muy floja.", sub5.title);
    assertEquals(
        "Para no repetir. El típico sitio que no merece la pena, en mi opinión. Los camareros despistados, la comida tardó una eternidad en llegar, la calidad mediana, los postres tardaron otra barbaridad. Un desastre,",
        sub5.body);
    assertEquals("Navegador17", sub5.author);
    assertEquals("Madrid, España", sub5.authorLocation);
    // assertEquals(new Integer(44), sub5.reviews);
    assertEquals("Native score", new Integer(20), sub5.getScore_native());
    assertEquals("Websays score", new Integer(40), sub5.getScore_websays());
    
    assertEquals(new Integer(9), sub5.votes);
    
    cal.set(2013, 5, 12);// Year, Month(zero based), Date
    assertEqualDates(cal.getTime(), sub5.date);
    assertEquals(null, sub5.itemUrl);
    assertEquals(url.toString(), sub5.postUrl);
    assertEquals(6, sub5.position);
    assertEquals("sub5.threadId", "http://www.tripadvisor.es/ShowUserReviews-g187514-d803063-r172123804-T_G_I_Friday_s-Madrid.html",
        sub5.threadId);
  }
  
  /**
   * Test of segment method, of class TripAdvisorSegmenter.
   */
  @Test
  public void testSegment4() throws Exception {
    
    URL url = new URL("http://www.tripadvisor.es/ShowUserReviews-g187514-d803063-r172123804-T_G_I_Friday_s-Madrid.html#REVIEWS");
    
    StringBuilder content = readPageFromResource("tripAdvisor_fridays2.html");
    
    Calendar cal = Calendar.getInstance();
    HTMLDocumentSegmenter seg = new TripAdvisorSegmenter();
    
    List<SimpleItem> result = seg.segment(content.toString(), url);
    
    int expectedResults = 6; // No of comments + 1 (basic details item which
    // you expect in the page)
    assertEquals(expectedResults, result.size());
    
    int i = 0;
    for (SimpleItem item : result) {
      if (item.fakeItem) {
        assertNotNull("threadURLs", item.threadURLs);
        assertNotNull("threadURLs[0]", item.threadURLs.get(0));
      } else {
        if (i > 0) {
          // main item doesn't have author
          assertNotNull(item.author);
        }
        assertNotNull(item.body);
        assertNotNull(item.date);
        assertNotNull(item.title);
      }
      i++;
    }
    
  }
  
  /**
   * This test is testing the ago dates parsing for Spanish, English and Italian. Beware, the document file TripAdvisor-EnglishAgoDates.html contains
   * an special crafted page to have a representative set of ago dates examples.
   */
  @Test
  public void testFullAgoDates() {
    
    URL url = null;
    String urlString = "http://www.tripadvisor.com/Hotel_Review-g187791-d232838-Reviews-or30-Hotel_Sonya-Rome_Lazio.html#REVIEWS";
    try {
      url = new URL(urlString);
    } catch (MalformedURLException e) {
      fail("Error during the generation of the URL from string [" + urlString + "]. Reason [" + e.getMessage() + "]");
    }
    
    HTMLDocumentSegmenter seg = new TripAdvisorSegmenter();
    
    // First do the test from resource file
    StringBuilder content = null;
    String urlFile = "TripAdvisor-FullAgoDates.html";
    try {
      content = readPageFromResource(urlFile);
    } catch (IOException e) {
      fail("Error openning [" + urlFile + "]. Reason [" + e.getMessage() + "]");
    }
    List<SimpleItem> result = null;
    try {
      // System.out.println(content.toString());
      result = seg.segment(content.toString(), url);
    } catch (Exception e) {
      e.printStackTrace();
      fail("Error during the \"segment\" method. Reason [" + e.getMessage() + "]");
      
    }
    
    /*******************
     * Uncomment to get results in the screen SimpleItem main1 = result.get(0); System.out.println(result.size()); System.out.println("main1.title" +
     * main1.title); System.out.println("main1.body" + main1.body); System.out.println("main1.author" + main1.author);
     * System.out.println("main1.authorLocation" + main1.authorLocation); System.out.println("main1.reviews" + main1.reviews);
     * System.out.println("main1.score" + main1.score); System.out.println("main1.votes" + main1.votes); System.out.println("main1.itemUrl" +
     * main1.itemUrl); System.out.println("main1.postUrl" + main1.postUrl); System.out.println("main1.date" + main1.date);
     * 
     * SimpleItem temporal = result.get(1); System.out.println("temporal.title" + temporal.title); System.out.println("temporal.body" +
     * temporal.body); System.out.println("temporal.author" + temporal.author); System.out.println("temporal.authorLocation" +
     * temporal.authorLocation); System.out.println("temporal.reviews" + temporal.reviews); System.out.println("temporal.score" + temporal.score);
     * System.out.println("temporal.votes" + temporal.votes); System.out.println("temporal.itemUrl" + temporal.itemUrl);
     * System.out.println("temporal.postUrl" + temporal.postUrl); System.out.println("temporal.date" + temporal.date);
     * System.out.println("temporal.position" + temporal.position);
     * System.out.println("=========================================================================================");
     * 
     * temporal = result.get(2); System.out.println("temporal.title" + temporal.title); System.out.println("temporal.body" + temporal.body);
     * System.out.println("temporal.author" + temporal.author); System.out.println("temporal.authorLocation" + temporal.authorLocation);
     * System.out.println("temporal.reviews" + temporal.reviews); System.out.println("temporal.score" + temporal.score);
     * System.out.println("temporal.votes" + temporal.votes); System.out.println("temporal.itemUrl" + temporal.itemUrl);
     * System.out.println("temporal.postUrl" + temporal.postUrl); System.out.println("temporal.date" + temporal.date);
     * System.out.println("temporal.position" + temporal.position);
     * System.out.println("=========================================================================================");
     * 
     * temporal = result.get(3); System.out.println("temporal.title" + temporal.title); System.out.println("temporal.body" + temporal.body);
     * System.out.println("temporal.author" + temporal.author); System.out.println("temporal.authorLocation" + temporal.authorLocation);
     * System.out.println("temporal.reviews" + temporal.reviews); System.out.println("temporal.score" + temporal.score);
     * System.out.println("temporal.votes" + temporal.votes); System.out.println("temporal.itemUrl" + temporal.itemUrl);
     * System.out.println("temporal.postUrl" + temporal.postUrl); System.out.println("temporal.date" + temporal.date);
     * System.out.println("temporal.position" + temporal.position);
     * System.out.println("=========================================================================================");
     * 
     * temporal = result.get(4); System.out.println("temporal.title" + temporal.title); System.out.println("temporal.body" + temporal.body);
     * System.out.println("temporal.author" + temporal.author); System.out.println("temporal.authorLocation" + temporal.authorLocation);
     * System.out.println("temporal.reviews" + temporal.reviews); System.out.println("temporal.score" + temporal.score);
     * System.out.println("temporal.votes" + temporal.votes); System.out.println("temporal.itemUrl" + temporal.itemUrl);
     * System.out.println("temporal.postUrl" + temporal.postUrl); System.out.println("temporal.date" + temporal.date);
     * System.out.println("temporal.position" + temporal.position);
     * System.out.println("=========================================================================================");
     * 
     * temporal = result.get(5); System.out.println("temporal.title" + temporal.title); System.out.println("temporal.body" + temporal.body);
     * System.out.println("temporal.author" + temporal.author); System.out.println("temporal.authorLocation" + temporal.authorLocation);
     * System.out.println("temporal.reviews" + temporal.reviews); System.out.println("temporal.score" + temporal.score);
     * System.out.println("temporal.votes" + temporal.votes); System.out.println("temporal.itemUrl" + temporal.itemUrl);
     * System.out.println("temporal.postUrl" + temporal.postUrl); System.out.println("temporal.date" + temporal.date);
     * System.out.println("temporal.position" + temporal.position);
     * System.out.println("=========================================================================================");
     ***************************/
    
    // Let's check the segmented items
    
    int expectedResults = 13; // No of comments + 1 (basic details item which you expect in the page)
    // uncomment when not adding more comments to the page
    // assertEquals(expectedResults, result.size());
    
    Calendar calOneDayAgo = Calendar.getInstance();
    calOneDayAgo.add(Calendar.DAY_OF_YEAR, -1);
    Calendar calNDaysAgo = Calendar.getInstance();
    calNDaysAgo.add(Calendar.DAY_OF_YEAR, -2);
    Calendar calOneWeekAgo = Calendar.getInstance();
    calOneWeekAgo.add(Calendar.WEEK_OF_YEAR, -1);
    Calendar calNWeeksAgo = Calendar.getInstance();
    calNWeeksAgo.add(Calendar.WEEK_OF_YEAR, -2);
    Calendar calOneMonthAgo = Calendar.getInstance();
    calOneMonthAgo.add(Calendar.MONTH, -1);
    Calendar calNMonthsAgo = Calendar.getInstance();
    calNMonthsAgo.add(Calendar.MONTH, -2);
    Calendar calOneYearAgo = Calendar.getInstance();
    calOneYearAgo.add(Calendar.YEAR, -1);
    Calendar calNYearsAgo = Calendar.getInstance();
    calNYearsAgo.add(Calendar.YEAR, -2);
    
    SimpleItem testItem = null;
    
    // // ///////// Test "a day ago" //////////////
    // testItem = result.get(0);
    // System.out.println("Results: " + result.size());
    // System.out.println("main1.title " + testItem.title);
    // System.out.println("main1.body " + testItem.body);
    // System.out.println("main1.author " + testItem.author);
    // System.out.println("main1.authorLocation " + testItem.authorLocation);
    // System.out.println("main1.reviews " + testItem.reviews);
    // System.out.println("main1.score " + testItem.score);
    // System.out.println("main1.votes " + testItem.votes);
    // System.out.println("main1.itemUrl " + testItem.itemUrl);
    // System.out.println("main1.postUrl " + testItem.postUrl);
    // System.out.println("main1.date " + testItem.date);
    // // System.out.println("main1.threadUrls: " + testItem.threadURLs.toString());
    //
    // // assertEquals("May 2015 stay very satisfied", testItem.title);
    // // assertEqualDates(calOneDayAgo.getTime(), testItem.date);
    //
    // // ///////// Test "1 day ago" //////////////
    // testItem = result.get(1);
    // System.out.println("main1.title " + testItem.title);
    // System.out.println("main1.body " + testItem.body);
    // System.out.println("main1.author " + testItem.author);
    // System.out.println("main1.authorLocation " + testItem.authorLocation);
    // System.out.println("main1.reviews " + testItem.reviews);
    // System.out.println("main1.score " + testItem.score);
    // System.out.println("main1.votes " + testItem.votes);
    // System.out.println("main1.itemUrl " + testItem.itemUrl);
    // System.out.println("main1.postUrl " + testItem.postUrl);
    // System.out.println("main1.date " + testItem.date);
    // System.out.println("=================================================");
    // // assertEquals("", testItem.title);
    // // assertEqualDates(calOneDayAgo.getTime(), testItem.date);
    //
    // // ///////// Test "hace un día" //////////////
    // testItem = result.get(2);
    // System.out.println("main1.title" + testItem.title);
    // System.out.println("main1.body" + testItem.body);
    // System.out.println("main1.author" + testItem.author);
    // System.out.println("main1.authorLocation" + testItem.authorLocation);
    // System.out.println("main1.reviews" + testItem.reviews);
    // System.out.println("main1.score" + testItem.score);
    // System.out.println("main1.votes" + testItem.votes);
    // System.out.println("main1.itemUrl" + testItem.itemUrl);
    // System.out.println("main1.postUrl" + testItem.postUrl);
    // System.out.println("main1.date" + testItem.date);
    // System.out.println("=================================================");
    // // assertEquals("", testItem.title);
    // // assertEqualDates(calOneDayAgo.getTime(), testItem.date);
    //
    // // ///////// Test "hace 1 día" //////////////
    // testItem = result.get(3);
    // System.out.println("main1.title" + testItem.title);
    // System.out.println("main1.body" + testItem.body);
    // System.out.println("main1.author" + testItem.author);
    // System.out.println("main1.authorLocation" + testItem.authorLocation);
    // System.out.println("main1.reviews" + testItem.reviews);
    // System.out.println("main1.score" + testItem.score);
    // System.out.println("main1.votes" + testItem.votes);
    // System.out.println("main1.itemUrl" + testItem.itemUrl);
    // System.out.println("main1.postUrl" + testItem.postUrl);
    // System.out.println("main1.date" + testItem.date);
    // System.out.println("=================================================");
    // // assertEquals("", testItem.title);
    // // assertEqualDates(calOneDayAgo.getTime(), testItem.date);
    //
    // // ///////// Test "un giorno fa" //////////////
    // testItem = result.get(4);
    // System.out.println("main1.title" + testItem.title);
    // System.out.println("main1.body" + testItem.body);
    // System.out.println("main1.author" + testItem.author);
    // System.out.println("main1.authorLocation" + testItem.authorLocation);
    // System.out.println("main1.reviews" + testItem.reviews);
    // System.out.println("main1.score" + testItem.score);
    // System.out.println("main1.votes" + testItem.votes);
    // System.out.println("main1.itemUrl" + testItem.itemUrl);
    // System.out.println("main1.postUrl" + testItem.postUrl);
    // System.out.println("main1.date" + testItem.date);
    // System.out.println("=================================================");
    // // assertEquals("", testItem.title);
    // // assertEqualDates(calOneDayAgo.getTime(), testItem.date);
    //
    // // ///////// Test "1 giorno fa" //////////////
    // testItem = result.get(5);
    // System.out.println("main1.title" + testItem.title);
    // System.out.println("main1.body" + testItem.body);
    // System.out.println("main1.author" + testItem.author);
    // System.out.println("main1.authorLocation" + testItem.authorLocation);
    // System.out.println("main1.reviews" + testItem.reviews);
    // System.out.println("main1.score" + testItem.score);
    // System.out.println("main1.votes" + testItem.votes);
    // System.out.println("main1.itemUrl" + testItem.itemUrl);
    // System.out.println("main1.postUrl" + testItem.postUrl);
    // System.out.println("main1.date" + testItem.date);
    // assertEquals("English 1 month ago", testItem.title);
    // assertEqualDates(calOneMonthAgo.getTime(), testItem.date);
    // System.out.println("=================================================");
    
    // // ///////// Test "1 year ago" //////////////
    // testItem = result.get(7);
    // assertEquals("English 1 year ago", testItem.title);
    // assertEqualDates(calOneYearAgo.getTime(), testItem.date);
    //
    // // ///////// Test "Hace un día" //////////////
    // testItem = result.get(8);
    // assertEquals("Castellano hace un día", testItem.title);
    // assertEqualDates(calOneDayAgo.getTime(), testItem.date);
    //
    // // ///////// Test "Hace 1 día" //////////////
    // testItem = result.get(9);
    // assertEquals("Castellano hace 1 día", testItem.title);
    // assertEqualDates(calOneDayAgo.getTime(), testItem.date);
    //
    // // ///////// Test "Hace N días" //////////////
    // testItem = result.get(10);
    // assertEquals("Castellano hace N días", testItem.title);
    // assertEqualDates(calNDaysAgo.getTime(), testItem.date);
    //
    // // ///////// Test "Hace 1 semana" //////////////
    // testItem = result.get(11);
    // assertEquals("Castellano hace una semana", testItem.title);
    // assertEqualDates(calOneWeekAgo.getTime(), testItem.date);
    //
    // // ///////// Test "Hace N semanas" //////////////
    // testItem = result.get(12);
    // assertEquals("Castellano hace N semanas", testItem.title);
    // assertEqualDates(calNWeeksAgo.getTime(), testItem.date);
    //
    /* TODO: there are still lots of combinations to test. */
    
  }
}