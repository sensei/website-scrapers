/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Marco Martinez/Hugo Zaragoza
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.htmlcleaner.HtmlCleaner;

import websays.types.clipping.SimpleItem;

public class TwentyMinutesSegmenterTest extends BaseDocumentSegmenterTest {
  
  @Override
  public void setUp() throws Exception {
    StringBuilder content = readPageFromResource("Paris: Un nouveau rassemblement de la Manif pour tous programmé dimanche - 20minutes.fr.html");
    this.seg = new TwentyMinutesSegmenter();
    HtmlCleaner cleaner = createCleaner();
    this.rootNode = cleaner.clean(content.toString());
  }
  
  @Override
  public void testFindMainTitle() {
    String title = seg.findMainTitle(rootNode);
    assertEquals("Paris: Un nouveau rassemblement de la Manif pour tous programmé dimanche", title);
  }
  
  @Override
  public void testFindMainDescripcion() {
    String body = seg.findMainDescripcion(rootNode);
    assertEquals("SOCIETE - Le mouvement s'oppose à la PMA et la GPA pourtant non compris dans la loi Famille...", body);
  }
  
  @Override
  public void testFindMainDate() {
    assertEqualTimes(DatatypeConverter.parseDateTime("2014-01-31T12:01:17+01:00").getTime(), seg.findMainDate(rootNode));
  }
  
  @Override
  public void testFindMainImageLink() {
    assertEquals(
        "http://cache.20minutes.fr/illustrations/2013/05/26/manifestants-venus-defiler-contre-loi-mariage-tous-a-paris-26-mai-2013-1324329-616x380.jpg",
        seg.findMainImageLink(rootNode));
  }
  
  @Override
  public void testFindMainAPIObjectID() {
    assertEquals("132725426770373", seg.findMainAPIObjectID(rootNode));
  }
  
  @Override
  public void testFindComments() {
    List<SimpleItem> comments = seg.addComments(rootNode, new SimpleItem());
    assertEquals(5, comments.size());
  }
  
  @Override
  public void testFindMainTimeIsReal() {
    assertTrue(
        "Main page Time is always true for this segmenter, as it is not in this test, check the scraping process and possible changes in the page source code.",
        seg.findMainTimeIsReal(rootNode));
  }
  
}
