/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.List;

import junit.framework.AssertionFailedError;

import org.junit.Test;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class VozPopuliCommentsSegmenterTest extends SegmenterTest {
  
  @Test
  public void testVozPopuliCommentsSegmenter() throws MalformedURLException, UnsupportedEncodingException, IOException, Exception,
      AssertionFailedError {
    URL urlComments = new URL("http://vozpopuli.com/v2/includes/ajax/comentarios/comentarios.php?idElemento=45866&tipo=Noticias&pagina=1");
    StringBuilder contentComments = readPageFromResource("VozPopuliComments1.html");
    HTMLDocumentSegmenter segComments = new VozPopuliCommentsSegmenter();
    List<SimpleItem> resultComments = segComments.segment(contentComments.toString(), urlComments);
    
    int expectedResults = 30; // No of comments
    assertEquals(expectedResults, resultComments.size());
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = resultComments.get(0);
    // for (SimpleItem simpleItem : resultComments) {
    // System.out.println(simpleItem.threadId);
    // }
    assertEquals("4584474", sub1.APIObjectID);
    assertEquals(null, sub1.title);
    assertEquals(
        "                            yo creo qe va a utilizar esa compañia para vehiculizar la nueva estrategia de los poderes del PP (ocultos y no) para llevarse el dinero. En lugar de hacerlo con tesoreros en la calle Genova, las multinacionales usaran esta cia de la que el se llevara una parte, para dar dinero al PP y llevarse contratos, fuera del alcance de la judicaturia española. Va a ser el nuevo tesorero del PP. EL PP no está por el cambio, sino por cambiar algo para seguir igual y darnos si acaso las  migajas en el suelo par que nos las disputemos.                        ",
        sub1.body);
    assertEquals("Roman2", sub1.author);
    assertEquals("http://estatico.vozpopuli.com/v2/img/detalle/foto-comentarios-usuarios-sinImagen.jpg", sub1.imageLink);
    assertEquals(new Integer(1), sub1.likes);
    // 03.07.2014 (10:07)
    Calendar cal = Calendar.getInstance();
    cal.set(2014, 06, 03, 10, 07);
    assertEqualTimes(cal.getTime(), sub1.date);
    assertEquals(urlComments.toString(), sub1.postUrl);
    assertEquals(1, sub1.position);
    assertTrue(sub1.isComment);
    assertEquals("http://vozpopuli.com/v2/includes/ajax/comentarios/comentarios.php?idElemento=45866&tipo=Noticias&pagina=2",
        sub1.threadURLs.get(0));
  }
  
  @Test
  public void testVozPopuliCommentsSegmenterLastPage() throws MalformedURLException, UnsupportedEncodingException, IOException, Exception,
      AssertionFailedError {
    URL urlComments = new URL("http://vozpopuli.com/v2/includes/ajax/comentarios/comentarios.php?idElemento=45866&tipo=Noticias&pagina=2");
    StringBuilder contentComments = readPageFromResource("VozPopuliComments2.html");
    HTMLDocumentSegmenter segComments = new VozPopuliCommentsSegmenter();
    List<SimpleItem> resultComments = segComments.segment(contentComments.toString(), urlComments);
    
    int expectedResults = 11; // No of comments
    assertEquals(expectedResults, resultComments.size());
    
    // ///////// Test First comment item //////////////
    SimpleItem sub1 = resultComments.get(0);
    assertNull(sub1.threadURLs);
  }
}
