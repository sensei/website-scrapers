/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 7, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.htmlcleaner.HtmlCleaner;

public class VozPopuliSegmenterTest extends BaseDocumentSegmenterTest {
  
  private static final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
  
  private static final Logger logger = Logger.getLogger(VozPopuliSegmenterTest.class);
  
  @Override
  public void setUp() throws Exception {
    StringBuilder content = readPageFromResource("vozPopuli.html");
    this.seg = new VozPopuliSegmenter();
    HtmlCleaner cleaner = createCleaner();
    this.rootNode = cleaner.clean(content.toString());
  }
  
  @Override
  public void testFindMainTitle() {
    assertEquals(
        "Rato crea una sociedad con conexiones en para&iacute;sos fiscales, un a&ntilde;o despu&eacute;s de ser imputado por el caso Bankia",
        seg.findMainTitle(rootNode));
  }
  
  @Override
  public void testFindMainDescripcion() {
    assertEquals(
        "2013 fue un año frenético para Rodrigo Rato. En enero, Telefónica le fichó como asesor externo para sus negocios en América Latina y Europa. En septiembre, Emilio Botín le premió con un sillón en el consejo internacional del Santander. Apenas dos meses después, el ex ministro de Economía y ex director gerente del Fondo Monetario Internacional (FMI) estableció una sociedad de comercio internacional, con sede en Wembley (Inglaterra), pero con claras conexiones con dos paraísos fiscales, según consta en la documentación adjunta. El pasado 29 de noviembre, poco más de un año después de ser imputado por el caso Bankia, Rato inscribió en la Companies House (registro mercantil de Reino Unido) la sociedad Lilac Trading Limited. Para gestionar esta operación, descubierta por el diario&nbsp;politicalocal.es,&nbsp;el economista contrató a Finsbury Trust&amp;Corporate Services Limited, una empresa de servicios fiduciarios de Gibraltar, una de las geografías opacas fiscalmente, que se dedica a la formalización para terceros de sociedades de inversión e inmobiliarias.    El capital social de la firma está constituido por 3.000 acciones con un valor nominal de una libra por título. Cada acción, como señala el documento de constitución de la sociedad, otorga a sus propietarios todos los derechos en lo que respecta a \"votaciones, dividendos y retribuciones\". Rato, como director de Lilac Trading Limited, posee el 75% del capital social, con un total de 2.250 acciones. Mientras, Pedro Diez Martínez, el otro socio de la firma, posee el 25% restante, con 750 títulos. La segunda ramificación con estas geografías 'offshore' se produce en la figura del secretario del consejo de administración de Lilac Trading Limited. La firma que ejerce este cargo es Gateway Management (B.V.I) Limited, una sociedad afincada en Down Town (Tórtola), capital de las Íslas Vírgenes Británicas. El micro estado cuenta con unos 27.000 habitantes y tiene registradas más de 480.000 sociedades, según datos oficiales del organismo supervisor de las islas que no incluye fideicomisos, fundaciones y fondos de inversión.&nbsp;Portcullis TrustNet, el intermediario empresarial que opera en la zona y a cuya base de datos ha tenido acceso el&nbsp;Consorcio Internacional de Periodistas de Investigación&nbsp;(ICIJ, en sus siglas en inglés), señala en su web que hay más de 1,4 millones de entidades, unas 50 entidades por cada ciudadano. Algunas de estas sociedades apenas duran unas semanas. Se estima que el 40% de las sociedades&nbsp;offshore&nbsp;del mundo está registradas en este archipiélago. La estabilidad política, las facilidades para abrir una entidad online, las garantías legales y el bajo coste administrativo son las principales bazas de este archipiélago, que es el principal paraíso fiscal de los inversores chinos. Gateway Management, firma que ejerce de secretario del consejo de la nueva compañía de Rodrigo Rato, fue creada por David Dennis Cuby, quien ejerce de testaferro profesional ofreciendo servicios fiduiciarios desde Gibraltar. Fuentes conocedoras de las grandes operaciones financieras en territorios offshore, explican que, en muchas ocasiones, se utilizan sociedades instrumentales afincadas en paraísos fiscales o países con un régimen tributario favorable para \"descargar los beneficios de las compañías evitando pagar grandes sumas en impuestos, camuflándolas en retribuciones al administrador\". Pese a que las Íslas Vírgenes Británicas ya no pertenecen a la 'lista gris' de la OCDE, tan sólo pertenecen dos geografías (Naurú y Niué), si siguen teniendo la consideración de paraíso fiscal para España, según consta en la lista de estos territorios offshore que maneja la Agencia Tributaria. Lo mismo sucede en el caso de Gibraltar.    No es la primera ocasión que el apellido Rato se asocia con un paraíso fiscal. María Ángeles Rato Figaredo, la hermana del ex presidente de Bankia, aprovechó la aministía fiscal de Cristóbal Montoro para trasladar a Madrid una sociedad radicada hasta ahora en Panamá, como informaba Vozpópuli en diciembre de 2013,&nbsp;país que ofrece grandes ventajas en materia impositiva para los grandes patrimonios extranjeros que decidan instalar allí una sociedad. La firma en cuestión se denomina Walden Enterprises Inc, una instrumental constituida en agosto de 2011 en Panamá de la que Ángeles Rato ha sido nombrada administradora única este mismo mes (en concreto, el pasado 18 de diciembre), según consta en el Registro Mercantil de Madrid. Constituida con un capital social de 10.000 dólares estadounidenses a través del bufete panameño Morgan &amp; Morgan, la empresa estaba domiciliada en un opulento rascacielos en el distrito panameño de Marbella, donde tiene una de sus dos sedes en el país centroamericano esa firma de asesoramiento. La sociedad tenía como objeto principal, según la documentación oficial que obra en el Registro Público de ese país centroamericano, \"el de dedicarse en la República de Panamá o en cualquier otro país, colonia o territorio extranjero, a comprar, vender, transferir, negociar, financiar, permutar, poseer, administrar, dar o tomar dinero en préstamo, abrir y manejar cuentas bancarias en Panamá o en cualquier parte del mundo, dar o tomar en comisión, hipoteca (...) toda clase de bienes, sean muebles o inmuebles, acciones o derechos y celebrar y efectuar todos los actos, contratos, operaciones, negocios y transacciones de lícito comercio\". En el caso del ex vicepresidente del Gobierno de José María Aznar, su firma se dedica al comercio exterior. Los ingresos que obtenga por esta actividad complementarán sus retribuciones en Telefónica, Santander, La Caixa (desde marzo pasado ocupa un puesto como consejero de Servihábitat), además de los 522.000 euros que, también desde marzo pasado, tiene derecho a percibir por su plan de pensiones en Bankia, como adelantó este medio en febrero pasado. La cantidad se divide entre los 489.000 euros aportados por Bankia y los 33.000 euros por BFA, a lo largo de 2011. Su imputación en el caso Bankia no será un obstáculo para que pueda recuperar todo su fondo de pensiones. De hecho, el juez de la Audiencia Nacional Fernando Andreu decidió no imponer limitación alguna para que los 24 ex consejeros de Bankia imputados en la causa puedan ir devengando las cantidades de sus planes de pensiones según vayan cumpliendo los 65 años. ",
        seg.findMainDescripcion(rootNode));
  }
  
  @Override
  public void testFindMainImageLink() {
    assertEquals(
        "http://estatico.vozpopuli.com/imagenes/Noticias/DFDA1E09-5126-1DF2-E5DF-EE5EE77635BA.jpg/resizeCut/879-0-1136/0-34-622/imagen.jpg",
        seg.findMainImageLink(rootNode));
  }
  
  @Override
  public void testFindMainAuthor() {
    assertEquals("Vozpopuli", seg.findMainAuthor(rootNode));
  }
  
  @Override
  public void testFindMainDate() {
    try {
      Calendar cal = Calendar.getInstance();
      cal.get(Calendar.DATE);
      Date date = sdf.parse(sdf.format(cal.getTime()));
      assertEqualTimes(date, seg.findMainDate(rootNode));
    } catch (ParseException e) {
      logger.error(e.getMessage());
    }
    
  }
  
  @Override
  public void testFindMainThreadId() {
    assertNull(seg.findMainThreadId(rootNode));
  }
  
  @Override
  public void testFindMainThreadURLs() {
    assertEquals("http://vozpopuli.com/v2/includes/ajax/comentarios/comentarios.php?idElemento=45866&tipo=Noticias&pagina=1", seg
        .findMainThreadURLs(rootNode).get(0));
  }
  
}
