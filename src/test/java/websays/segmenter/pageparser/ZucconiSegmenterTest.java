/**
 * Websays Opinion Analytics Engine
 *
 * (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
 *
 * Primary Author: Jorge Valderrama
 * Contributors:
 * Date: Jul 9, 2014
 */
package websays.segmenter.pageparser;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;

import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.junit.Test;

import websays.types.clipping.SimpleItem;

public class ZucconiSegmenterTest extends BaseDocumentSegmenterTest {
  
  @Override
  public void setUp() throws Exception {
    // http://zucconi.blogautore.repubblica.it/2014/07/02/il-raccomandato/
    StringBuilder content = readPageFromResource("zucconi.blogautore.repubblica.it");
    this.seg = new ZucconiSegmenter();
    HtmlCleaner cleaner = createCleaner();
    this.rootNode = cleaner.clean(content.toString());
  }
  
  @Override
  public void testFindMainTitle() {
    String title = seg.findMainTitle(rootNode);
    assertEquals("Paternalismo", title);
  }
  
  @Override
  public void testFindMainDescripcion() {
    String body = seg.findMainDescripcion(rootNode);
    assertEquals(
        "Quando sento esponenti politici, personalità pubbliche o commentatori occasionali ripetere che lItalia è un paese di ladri e che gli italiani sono geneticamente tutti profittatori o corrotti o furbastri ripenso invariabilmente allunica occasione nella quale la mia vita professionale intersecò quella di mio padre. Nel 1975, dopo avere lasciato la Domenica del Corriere, mio padre Guglielmo aveva accettatio di guidare, insieme con lamico, il bravissimo Antonio Alberti, un settimanale illustrato già importante, ma ridotto alla canna del gas chiamato il Tempo di Milano. Io vivevo e lavoravo negli Stati Uniti, come corrispondente della Stampa e mio padre, che non poteva permettersi corrispondenti esteri, mi chiese, con grande imbarazzo, se potessi scrivere ogni tanto qualche cosa per Il Tempo, naturalmente con uno pseudonimo, Guido Borsara, dal nome della casa, Villa Borsara a Bastiglia di Modena, dove i miei erano sfollati e io ero nato.Ti posso pagare pochissimo, si giustificava, per questo lo domando a te e non ad altri che vorrebbero essere pagati bene, è unimpresa disperata, non abbiamo una lira, al massimo 10 mila a pezzo, che erano poche anche allora. Non potevo rifiutare, anche se era una perdita di tempo, e gli mandai quattro o cinque servizi, ogni tanto, facendo sempre attenzione a non danneggiare quella Stampa che era il mio editore in esclusiva e mi pagava bene. Lo dissi anche al direttore del quotidiano di Torino che chiuse un occhio, sapendo che non sarebbe stato un danno.Alla fine dellanno, non avendo visto arrivare neanche una lira, e sapendo che il Tempo era proprietà di un ricco stampatore milanese che aveva lesclusiva dei biglietti del tram  tiratura e vendite assicurate  gli chiesi conto dei mancati pagamenti. Mio padre, dopo qualche scusa, mi diede una busta con 50 mila lire in contanti. Va bene, gli dissi subodorando qualche cosa, ma voglio il borderò, il bollettino di pagamento, sai, per le tasse, le trattenute.Lui, imbarazzato e rosso in viso come gli capitava quando era sorpreso a dire bugie, cominciò a farfugliare scuse e improvvisamente compresi. Questi sono soldi tuoi, gli dissi, mi stai pagando di tasca tua per il lavoro che ho fatto! Dovette ammetterlo. Era vero, aveva cercato di pagarmi con soldi suoi. Non posso mica pagare mio figlio con i soldi del giornale, si giustificò, sarebbe vergognoso.Naturalmente rifiutai quelle cinquanta e mi venne il magone, presto superato dallammirazione e dallorgoglio di avere  di avere avuto  un padre così. No, non siamo, non siamo stati, tutti ladri, tutti profittatori, tutti venduti, tutti mafiosi, tutti camorristi. ",
        body);
  }
  
  @Override
  public void testFindMainAPIObjectID() {
    assertEquals("post-9863", seg.findMainAPIObjectID(rootNode));
  }
  
  @Override
  public void testFindMainImageLink() {
    assertEquals("http://www.repubblica.it/static/images/detail/2011/erreit-logo.png", seg.findMainImageLink(rootNode));
  }
  
  @Override
  public void testFindMainDate() {
    Calendar cal = Calendar.getInstance();
    cal.set(2014, 06, 02, 00, 00, 00);
    assertEqualTimes(cal.getTime(), seg.findMainDate(rootNode));
  }
  
  @Override
  public void testFindMainAuthor() {
    assertEquals("Repubblica.it", seg.findMainAuthor(rootNode));
  }
  
  @Override
  public void testFindMainComments() {
    assertEquals(new Integer(347), seg.findMainComments(rootNode));
  }
  
  @Override
  public void testFindComments() {
    assertEquals(347, seg.findComments(rootNode).length);
  }
  
  @Override
  @Test
  public void testFindCommentApiObjectID() {
    TagNode[] comments = seg.findComments(rootNode);
    assertEquals("li-comment-352747", seg.findCommentAPIObjectID(comments[0]));
    assertEquals("li-comment-352746", seg.findCommentAPIObjectID(comments[1]));
    assertEquals("li-comment-352745", seg.findCommentAPIObjectID(comments[2]));
    assertEquals("li-comment-352739", seg.findCommentAPIObjectID(comments[3]));
    assertEquals("li-comment-352738", seg.findCommentAPIObjectID(comments[4]));
  }
  
  @Override
  public void testFindCommentDate() {
    TagNode[] comments = seg.findComments(rootNode);
    Calendar cal = Calendar.getInstance();
    cal.set(2014, 06, 9, 00, 53, 00);
    assertEqualTimes(cal.getTime(), seg.findCommentDate(comments[0]));
    cal.set(2014, 06, 9, 00, 53, 00);
    assertEqualTimes(cal.getTime(), seg.findCommentDate(comments[1]));
    cal.set(2014, 06, 9, 00, 51, 00);
    assertEqualTimes(cal.getTime(), seg.findCommentDate(comments[2]));
    cal.set(2014, 06, 9, 00, 33, 00);
    assertEqualTimes(cal.getTime(), seg.findCommentDate(comments[3]));
    cal.set(2014, 06, 9, 00, 25, 00);
    assertEqualTimes(cal.getTime(), seg.findCommentDate(comments[4]));
    cal.set(2014, 06, 9, 00, 00, 00);
    
  }
  
  @Override
  public void testFindCommentAuthor() {
    TagNode[] comments = seg.findComments(rootNode);
    assertEquals("leo561", seg.findCommentAuthor(comments[0]));
    assertEquals("valcatt", seg.findCommentAuthor(comments[1]));
    assertEquals("ubimel", seg.findCommentAuthor(comments[2]));
    assertEquals("ubimel", seg.findCommentAuthor(comments[3]));
    assertEquals("valcatt", seg.findCommentAuthor(comments[4]));
  }
  
  @Override
  public void testFindCommentDescripcion() {
    TagNode[] comments = seg.findComments(rootNode);
    assertEquals(
        "A differenza di altri sport, dove nessuno sostiene che una squadra che sta già vincendo larghissimamente deve fermarsi per evitare di umiliare lavversario, nel calcio si favoleggia di una fantomatica legge non scritta che dice che, più o meno, non è una bella cosa stravincere e che sarebbe meglio se una squadra si accontentasse di vincere e basta.Invece nel rugby cè una legge non scritta opposta, cioè è prassi comune quella di cercare di segnare più mete possibili, anche se si sta vincendo 50 a zero, e fermarsi è considerata una mancanza di rispetto per gli spettatori.",
        seg.findCommentDescripcion(comments[0]));
    assertEquals(
        "Sono riuscito a guardarla fino a che la regia non ha inquadrato un bambino che piangeva, poi ho fatto altro--Oh, ma che animo sensibile. Oh, ma che cuor doro. Oh, ma quale delicatezza di spirito.Ti faccio notare che quel bambino era in uno stadio dove si giocava una semifinale mondiale, e che per portarlo lì, la sua famiglia ha sborsato qualche centinaio di euro. Al mondo ci sono milioni di bambini che piangono perché non hanno da mangiare, non perché la loro squadra del cuore è stata eliminata, quindi, queste ipocrite frasi fatte di maniera, ce le puoi anche risparmiare.",
        seg.findCommentDescripcion(comments[1]));
    assertEquals(
        "Il filtro non mi fa passare le formazioni delle due squadre (la roma e il mamucium castrum, il campo della tettarella, cittadella fondata dai soldati di giulio agricola nel I sec).Da lì si vedono le cause del 7-1.Io dico che giulio agricola poteva pure dormire in campagna, quella sera, no?",
        seg.findCommentDescripcion(comments[2]));
    assertEquals(
        "@valMagari. Il blocco mentale, lo stress, la pressioneIl dramma è che quella volta i 7 gol ci stavano proprio tutti, senza motivi particolari.I tifosi dei red devils hanno votato quella partita come la migliore della loro storia, e si regalano il dvd a natale. E quale squadra del mondo doveva capitare sotto, quella sera?",
        seg.findCommentDescripcion(comments[3]));
    assertEquals("State parlando di Manchester  Roma 7 a 1?", seg.findCommentDescripcion(comments[4]));
  }
  
  @Override
  public void testFindCommentImageLink() {
    TagNode[] comments = seg.findComments(rootNode);
    assertEquals("", seg.findCommentImageLink(comments[0]));
    assertEquals("", seg.findCommentImageLink(comments[1]));
    assertEquals("", seg.findCommentImageLink(comments[2]));
    assertEquals("", seg.findCommentImageLink(comments[3]));
    assertEquals("", seg.findCommentImageLink(comments[4]));
  }
  
  @Override
  public void testFindMainLanguage() {
    assertEquals(SimpleItem.LanguageTag.italian, seg.findMainLanguage(rootNode));
  }
  
  @Override
  public void testFindCommentLanguage() {
    assertEquals(SimpleItem.LanguageTag.italian, seg.findCommentLanguage(rootNode));
  }
  
}
