/**
* Websays Opinion Analytics Engine
*
* (Websays Copyright © 2010-2014. All rights reserved. http://websays.com )
*
* Primary Author: Marco Martinez/Hugo Zaragoza
* Contributors:
* Date: Jul 7, 2014
*/
package websays.segmenter.util;

import static org.junit.Assert.*;

import java.net.URL;
import java.util.List;

import websays.segmenter.base.HTMLDocumentSegmenter;
import websays.types.clipping.SimpleItem;

public class SegmenterTestNavigation {
	private URL url;
	private List<SimpleItem> result;
	private int index;

	public SegmenterTestNavigation(URL url, String content,
			HTMLDocumentSegmenter seg) throws Exception {
		this.url = url;
		this.result = seg.segment(content, url);
		this.index = 0;
	}

	public SimpleItem nextItem() {
		return getItem(index++);
	}

	public SimpleItem getItem(int i) {
		assertTrue(size() > i);
		SimpleItem simpleItem = result.get(i);
		assertEquals(++i, simpleItem.position);
		assertEquals(url.toString(), simpleItem.postUrl);
		return simpleItem;
	}

	public SimpleItem getFirstItem() {
		return getItem(0);
	}

	public SimpleItem getLastItem() {
		return getItem(size() - 1);
	}

	public int size() {
		return result.size();
	}

}
