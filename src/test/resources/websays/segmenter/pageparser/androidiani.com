<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="it" id="vbulletin_html">
<head>
<base href="http://www.androidiani.com/forum/" /><!--[if IE]></base><![endif]-->
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta id="e_vb_meta_bburl" name="vb_meta_bburl" content="http://www.androidiani.com/forum" />

<meta name="generator" content="vBulletin 4.2.2" />
<!--
	<link rel="Shortcut Icon" href="http://www.androidiani.com/forum/favicon.ico" type="image/x-icon" />
-->







<script type="text/javascript">
<!--
	if (typeof YAHOO === 'undefined') // Load ALL YUI Local
	{
		document.write('<script type="text/javascript" src="http://www.androidiani.com/forum/clientscript/yui/yuiloader-dom-event/yuiloader-dom-event.js?v=422"><\/script>');
		document.write('<script type="text/javascript" src="http://www.androidiani.com/forum/clientscript/yui/connection/connection-min.js?v=422"><\/script>');
		var yuipath = 'clientscript/yui';
		var yuicombopath = '';
		var remoteyui = false;
	}
	else	// Load Rest of YUI remotely (where possible)
	{
		var yuipath = 'clientscript/yui';
		var yuicombopath = '';
		var remoteyui = true;
		if (!yuicombopath)
		{
			document.write('<script type="text/javascript" src="http://www.androidiani.com/forum/clientscript/yui/connection/connection-min.js"><\/script>');
		}
	}
	var SESSIONURL = "s=9696167c94718066d86ee6909ffa9e7f&";
	var SECURITYTOKEN = "guest";
	var IMGDIR_MISC = "images/styles/androidiani/misc";
	var IMGDIR_BUTTON = "images/styles/androidiani/buttons";
	var vb_disable_ajax = parseInt("0", 10);
	var SIMPLEVERSION = "422";
	var BBURL = "http://www.androidiani.com/forum";
	var LOGGEDIN = 0 > 0 ? true : false;
	var THIS_SCRIPT = "showthread";
	var RELPATH = "showthread.php?t=358796";
	var PATHS = {
		forum : "",
		cms   : "",
		blog  : ""
	};
	var AJAXBASEURL = "http://www.androidiani.com/forum/";
// -->
</script>
<script type="text/javascript" src="http://www.androidiani.com/forum/clientscript/vbulletin-core.js?v=422"></script>



	<link rel="alternate" type="application/rss+xml" title="Forum Android Italiano Feed RSS" href="http://feeds.feedburner.com/VBSEO_FEEDBURNER" />
	
		<link rel="alternate" type="application/rss+xml" title="Forum Android Italiano - Altri Operatori - Feed RSS" href="http://www.androidiani.com/forum/external.php?type=RSS2&amp;forumids=358" />
	



	<link rel="stylesheet" type="text/css" href="http://www.androidiani.com/forum/clientscript/vbulletin_css/style00012l/main-rollup.css?d=1394790642" />
        

	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="http://www.androidiani.com/forum/clientscript/vbulletin_css/style00012l/popupmenu-ie.css?d=1394790642" />
	<link rel="stylesheet" type="text/css" href="http://www.androidiani.com/forum/clientscript/vbulletin_css/style00012l/vbulletin-ie.css?d=1394790642" />
	<link rel="stylesheet" type="text/css" href="http://www.androidiani.com/forum/clientscript/vbulletin_css/style00012l/vbulletin-chrome-ie.css?d=1394790642" />
	<link rel="stylesheet" type="text/css" href="http://www.androidiani.com/forum/clientscript/vbulletin_css/style00012l/vbulletin-formcontrols-ie.css?d=1394790642" />
	<link rel="stylesheet" type="text/css" href="http://www.androidiani.com/forum/clientscript/vbulletin_css/style00012l/editor-ie.css?d=1394790642" />
	<![endif]-->

<!-- CSS Stylesheet -->

<link rel="stylesheet" type="text/css" href="/forum/chat/css/shoutbox.css"> 
<link rel="stylesheet" type="text/css" href="/extra/javascript/fancybox/jquery.fancybox-1.3.4.css"> 
<link rel="stylesheet" type="text/css" href="/extra/forum/css/forum_extra.css"> 
<script type="text/javascript">
var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];
(function() {
var gads = document.createElement('script');
gads.async = true;
gads.type = 'text/javascript';
var useSSL = 'https:' == document.location.protocol;
gads.src = (useSSL ? 'https:' : 'http:') + 
'//www.googletagservices.com/tag/js/gpt.js';
var node = document.getElementsByTagName('script')[0];
node.parentNode.insertBefore(gads, node);
})();
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-4166062-16', 'androidiani.com');
  ga('send', 'pageview');

</script>
<script type='text/javascript'>
googletag.cmd.push(function() {
googletag.defineSlot('/21878899/ForumSottoChatbox1', [728, 90], 'div-gpt-ad-1357911084365-0').addService(googletag.pubads());
googletag.defineSlot('/21878899/ForumSottoPrimoPost', [728, 90], 'div-gpt-ad-1357911084365-1').addService(googletag.pubads());
googletag.defineSlot('/21878899/ForumSottoChatbox_2', [728, 90], 'div-gpt-ad-1365946406915-0').addService(googletag.pubads());
googletag.defineSlot('/21878899/ForumFooter', [728, 90], 'div-gpt-ad-1370204836492-0').addService(googletag.pubads());
googletag.pubads().enableSingleRequest();
googletag.enableServices();
});
</script>

<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(
  ['_setAccount', 'UA-4166062-3'],
  ['_setDomainName', 'none'],
  ['_setSiteSpeedSampleRate', 2],
  ['_trackPageview'],
  ['second._setAccount', 'UA-2693135-19'],
  ['second._setDomainName', 'none'],
  ['second._setSiteSpeedSampleRate', 2],
  ['second._trackPageview']
);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

<!-- / CSS Stylesheet -->




<link href="https://plus.google.com/114096906508936606779" rel="publisher"><script type="text/javascript" src="http://www.androidiani.com/forum/clientscript/post_thanks.js"></script>

<style type="text/css">
.postbitlegacy .postfoot .textcontrols a.post_thanks_button, .postbit .postfoot .textcontrols a.post_thanks_button  {
	background: url(images/styles/androidiani/buttons/post_thanks.png) no-repeat transparent left;
	padding-left: 20px;
}
.postbitlegacy .postfoot .textcontrols a.post_thanks_button:hover, .postbit .postfoot .textcontrols a.post_thanks_button:hover  {
	background: url(images/styles/androidiani/buttons/post_thanks-hover.png) no-repeat transparent left;
</style>

	<meta name="description" content="Ciao a tutti, ho un problema con la mia sim PosteMobile. Qualche tempo fa ho cambiato gestore passando a PosteMobile usufruendo della nota promozione &" />

	<title>Problema sms Sim Postemobile - Forum Android Italiano</title>
	<link rel="canonical" href="http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile.html" />
	
	
	
	
	
	
	
	
	

	
		<link rel="stylesheet" type="text/css" href="http://www.androidiani.com/forum/clientscript/vbulletin_css/style00012l/showthread-rollup.css?d=1394790642" />
	
	<!--[if lt IE 8]><link rel="stylesheet" type="text/css" href="http://www.androidiani.com/forum/clientscript/vbulletin_css/style00012l/toolsmenu-ie.css?d=1394790642" />
	<link rel="stylesheet" type="text/css" href="http://www.androidiani.com/forum/clientscript/vbulletin_css/style00012l/postlist-ie.css?d=1394790642" />
	<link rel="stylesheet" type="text/css" href="http://www.androidiani.com/forum/clientscript/vbulletin_css/style00012l/showthread-ie.css?d=1394790642" />
	<link rel="stylesheet" type="text/css" href="http://www.androidiani.com/forum/clientscript/vbulletin_css/style00012l/postbit-ie.css?d=1394790642" />
	<link rel="stylesheet" type="text/css" href="http://www.androidiani.com/forum/clientscript/vbulletin_css/style00012l/poll-ie.css?d=1394790642" /><![endif]-->
<link rel="stylesheet" type="text/css" href="http://www.androidiani.com/forum/clientscript/vbulletin_css/style00012l/additional.css?d=1394790642" />

</head>

<body>


<!-- Tapatalk Detect body start -->
<style type="text/css">
.ui-mobile [data-role="page"], .ui-mobile [data-role="dialog"], .ui-page 
{
top:auto;
}
</style>
<script type="text/javascript">tapatalkDetect()</script>
<!-- Tapatalk Detect banner body end -->

<div id="topmenu">
			<ul id="menu-topmenu" class="menu"><li id="menu-item-144170" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-144170"><a href="/">Home</a></li>
<li id="menu-item-144171" class="menu-item menu-item-type-custom current-menu-item current_page_item menu-item-object-custom menu-item-144171"><a href="/forum">Forum</a></li>
<li id="menu-item-144172" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-144172"><a href="javascript:void(0)" onclick="jumpInside('chi-siamo')" title="Chi Siamo">Chi Siamo</a></li>
<li id="menu-item-144173" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-144173"><a href="javascript:void(0)" onclick="jumpInside('collabora')" title="Collabora">Collabora</a></li>
<li id="menu-item-144174" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-144174"><a href="javascript:void(0)" onclick="jumpInside('forum/sendmessage.php')" title="Contatti">Contatti</a></li>
<li id="menu-item-144175" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-144175"><a href="javascript:void(0)" onclick="jumpInside('supporta-androidianicom');" title="Donazioni">Donazioni</a></li>
</ul>	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js" type="text/javascript"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" type="text/javascript"></script>
        <script src="/extra/forum/js/ricerca-terminali.js" type="text/javascript"></script>
<script type="text/javascript" src="http://www.google.it/jsapi"></script>
	<script type="text/javascript">
	  google.load('search', '1');
	  google.setOnLoadCallback(function() {
		google.search.CustomSearchControl.attachAutoCompletion(
		  'partner-pub-2246076230784003:7405493187',
		  document.getElementById('realsearch'),
		  'formrealsearch');
	  });
	</script>
	
   <div id="searchboxes_container">
      <div id="searchboxes">
         
         <form action="http://www.androidiani.com/forum/sitesearch.htm" id="formrealsearch">
            <input type="hidden" name="cx" value="partner-pub-2246076230784003:7405493187" />
            <input type="hidden" name="cof" value="FORID:10" />
            <input type="hidden" name="ie" value="UTF-8" />
            <div class="label" onclick="document.getElementById('formrealsearch').submit()" style="cursor:pointer">CERCA</div>	
            <input type="text" id="realsearch" class="search-style" name="q" autocomplete="off" />
         </form>
         <div style="clear:both"></div>
         <div class="label">PER MODELLO</div>	
         <input type="text" id="search" class="search-style" />
      </div>
   </div>
   <div style="clear:both"></div>
</div>

<div class="above_body"> <!-- closing tag is in template navbar -->

<div id="header" class="floatcontainer doc_header">
	
	<div id="toplinks" class="toplinks notloggedin">
		
			<ul class="nouser">
			
				<li><a href="http://www.androidiani.com/forum/register.php" rel="nofollow">Registrazione</a></li>
			
				<li><a rel="help" href="http://www.androidiani.com/forum/faq.php">Aiuto</a></li>
				<li>
			<script type="text/javascript" src="http://www.androidiani.com/forum/clientscript/vbulletin_md5.js?v=422"></script>
			<form id="navbar_loginform" action="http://www.androidiani.com/forum/login.php?do=login" method="post" onsubmit="md5hash(vb_login_password, vb_login_md5password, vb_login_md5password_utf, 0)">
				<fieldset id="logindetails" class="logindetails">
					<div>
						<div>
					<input type="text" class="textbox default-value" name="vb_login_username" id="navbar_username" size="10" accesskey="u" tabindex="101" value="Nome utente" />
					<input type="password" class="textbox" tabindex="102" name="vb_login_password" id="navbar_password" size="10" />
					<input type="text" class="textbox default-value" tabindex="102" name="vb_login_password_hint" id="navbar_password_hint" size="10" value="Password" style="display:none;" />
					<input type="submit" class="loginbutton" tabindex="104" value="Entra" title="Inserisci il tuo nome utente e password in questo form e loggati, oppure clicca su 'registrati' per crearti un account." accesskey="s" />
						</div>
					</div>
				</fieldset>
				<div id="remember" class="remember">
					<label for="cb_cookieuser_navbar"><input type="checkbox" name="cookieuser" value="1" id="cb_cookieuser_navbar" class="cb_cookieuser_navbar" accesskey="c" tabindex="103" /> Ricordati?</label>
				</div>

				<input type="hidden" name="s" value="9696167c94718066d86ee6909ffa9e7f" />
				<input type="hidden" name="securitytoken" value="guest" />
				<input type="hidden" name="do" value="login" />
				<input type="hidden" name="vb_login_md5password" />
				<input type="hidden" name="vb_login_md5password_utf" />
			</form>
			<script type="text/javascript">
			YAHOO.util.Dom.setStyle('navbar_password_hint', "display", "inline");
			YAHOO.util.Dom.setStyle('navbar_password', "display", "none");
			vB_XHTML_Ready.subscribe(function()
			{
			//
				YAHOO.util.Event.on('navbar_username', "focus", navbar_username_focus);
				YAHOO.util.Event.on('navbar_username', "blur", navbar_username_blur);
				YAHOO.util.Event.on('navbar_password_hint', "focus", navbar_password_hint);
				YAHOO.util.Event.on('navbar_password', "blur", navbar_password);
			});
			
			function navbar_username_focus(e)
			{
			//
				var textbox = YAHOO.util.Event.getTarget(e);
				if (textbox.value == 'Nome utente')
				{
				//
					textbox.value='';
					textbox.style.color='#000000';
				}
			}

			function navbar_username_blur(e)
			{
			//
				var textbox = YAHOO.util.Event.getTarget(e);
				if (textbox.value == '')
				{
				//
					textbox.value='Nome utente';
					textbox.style.color='#777777';
				}
			}
			
			function navbar_password_hint(e)
			{
			//
				var textbox = YAHOO.util.Event.getTarget(e);
				
				YAHOO.util.Dom.setStyle('navbar_password_hint', "display", "none");
				YAHOO.util.Dom.setStyle('navbar_password', "display", "inline");
				YAHOO.util.Dom.get('navbar_password').focus();
			}

			function navbar_password(e)
			{
			//
				var textbox = YAHOO.util.Event.getTarget(e);
				
				if (textbox.value == '')
				{
					YAHOO.util.Dom.setStyle('navbar_password_hint', "display", "inline");
					YAHOO.util.Dom.setStyle('navbar_password', "display", "none");
				}
			}
			</script>
				</li>
				
			</ul>
		
	</div>
	<div class="ad_global_header">
		
		
	</div>
	<hr />
</div>


</div>
<div id="navbar" class="navbar">

	<ul id="navtabs" class="navtabs floatcontainer">
		
		
			<li class="selected"><a class="navtab" href="http://www.androidiani.com/forum/">Forum</a>
				<ul class="floatcontainer">
					
					
					
					
					
					<li><a rel="help" href="http://www.androidiani.com/forum/faq.php" accesskey="5">FAQ</a></li>
					
					<li><a href="http://www.androidiani.com/forum/calendar.php">Calendario</a></li>
					

					
					
					<li class="popupmenu">
						<a href="javascript:;" class="popupctrl">Azioni del forum</a>
						<ul class="popupbody popuphover">
							<li>
								<a rel="nofollow" href="http://www.androidiani.com/forum/forumdisplay.php?do=markread&amp;markreadhash=guest" onclick="return confirm('Sei sicuro di voler contrassegnare tutti i forum come letti? Questa zione � irreversibile.')">Segna forum come letti</a>
							</li>
                                                        
						</ul>
					</li>
					<li class="popupmenu">
						<a href="javascript:;" class="popupctrl" accesskey="3">Link veloci</a>
						<ul class="popupbody popuphover">
							
							
							<li><a href="http://www.androidiani.com/forum/showgroups.php" rel="nofollow">
		
			Lo staff del forum
		
	</a></li>
							
							
							
							
							
							
						</ul>
					</li>
					
				</ul>

			</li>
		
		
		
			<li><a class="navtab" href="http://www.androidiani.com/forum/search.php?do=getdaily&amp;contenttype=vBForum_Post" accesskey="2">Novit�</a></li>
		
		
	</ul>
</div>
</div><!-- closing div for above_body -->

<div class="body_wrapper">
<div style="margin: 15px 0px;">
<a id="chatbox_full" href="javascript:;" onclick="window.open('http://www.androidiani.com/forum/chat/','Androidiani ChatBox','width=800,height=600,scrollbars=yes');" style=" padding-top: 8px; padding-right: 10px; color: rgb(65, 65, 65); font-weight: bold; font-size: 14px;">FullScreen Chatbox! :)</a> 
</div>
<div id="chatboxbanners"><div class="leftcolumn"><div class="criteo"><div id='div-gpt-ad-1357911084365-0'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1357911084365-0'); });
</script>
</div></div><div class="juice_728"><div id='div-gpt-ad-1365946406915-0'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1365946406915-0'); });
</script>
</div></div></div><div class="rightcolumn"><div class="utenteDelGiorno"><p 
	style="
			-moz-box-shadow: -2px 2px 2px #c8c8c8;
			-webkit-box-shadow: -2px 2px 2px #c8c8c8;
			box-shadow: -2px 2px 2px #c8c8c8;
			padding:5px;
			background: #CEDFEB none;
			line-height:20px;
	">
	<b>Utente del giorno:</b> <b><a href="javascript:void(0);" onclick="jumpUsersForum('members/skorpion83.html')">skorpion83</a></b> con ben 27 <a href="javascript:void(0);" class="bestuserthanks">Thanks</a> ricevuti nelle ultime 24 ore<br/><b>Utente della settimana:</b> <b><a href="javascript:void(0);" onclick="jumpUsersForum('members/Slim80.html')">Slim80</a></b> con ben 371 <a href="javascript:void(0);" class="bestuserthanks">Thanks</a> ricevuti negli ultimi sette giorni<br/><b>Utente del mese:</b> <b><a href="javascript:void(0);" onclick="jumpUsersForum('members/Pierdebeer.html')">Pierdebeer</a></b> con ben 1991 <a href="javascript:void(0);" class="bestuserthanks">Thanks</a> ricevuti nell'ultimo mese
</p>
</div></div><div style="clear:both"></div></div>

<div id="breadcrumb" class="breadcrumb">
	<ul class="floatcontainer">
		<li class="navbithome"><a href="http://www.androidiani.com/forum/" accesskey="1"><img src="http://www.androidiani.com/forum/images/styles/androidiani/misc/navbit-home.png" alt="Home" /></a></li>

		
	<li class="navbit" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://www.androidiani.com/forum/" itemprop="url"><span itemprop="title">Forum</span></a></li>

	<li class="navbit" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://www.androidiani.com/forum/gestori-e-operatori-mobili/" itemprop="url"><span itemprop="title">Gestori e Operatori Mobili</span></a></li>

	<li class="navbit" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="http://www.androidiani.com/forum/altri-operatori/" itemprop="url"><span itemprop="title">Altri Operatori</span></a></li>

		
	<li class="navbit lastnavbit" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title"> Problema sms Sim Postemobile</span></li>

	</ul>
	<hr />
</div>





	<form action="http://www.androidiani.com/forum/profile.php?do=dismissnotice" method="post" id="notices" class="notices">
		<input type="hidden" name="do" value="dismissnotice" />
		<input type="hidden" name="s" value="s=9696167c94718066d86ee6909ffa9e7f&amp;" />
		<input type="hidden" name="securitytoken" value="guest" />
		<input type="hidden" id="dismiss_notice_hidden" name="dismiss_noticeid" value="" />
		<input type="hidden" name="url" value="" />
		<div id="vbseo_vhtml_0"></div>
	</form>






	<div id="above_postlist" class="above_postlist">
		
		<div id="pagination_top" class="pagination_top">
		
			<form action="http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile-2.html" method="get" class="pagination popupmenu nohovermenu">
<input type="hidden" name="t" value="358796" /><input type="hidden" name="s" value="9696167c94718066d86ee6909ffa9e7f" />
	
		<span><a href="javascript://" class="popupctrl">Pagina 1 di 2</a></span>
		
		
		
		<span class="selected"><a href="javascript://" title="Visualizzazione dei risultati da 1 a 10 su 16">1</a></span><span><a href="http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile-2.html" title="visualizza i risultati 11 sin 16 da 16">2</a></span>
		
		
		<span class="prev_next"><a rel="next" href="http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile-2.html" title="Pagina seguente - Resultati 11 sino 16 di 16"><img src="http://www.androidiani.com/forum/images/styles/androidiani/pagination/next-right.png" alt="Seguente" /></a></span>
		
		
		<span class="first_last"><a href="http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile-2.html" title="ultima pagina - Resultati 11 sino 16 di 16">ultimo<img src="http://www.androidiani.com/forum/images/styles/androidiani/pagination/last-right.png" alt="ultimo" /></a></span>
		
	
	<ul class="popupbody popuphover">
		<li class="formsubmit jumptopage"><label>Vai alla pagina <input type="text" name="page" size="4" /></label> <input type="submit" class="button" value="Prosegui" /></li>
	</ul>
</form>
		
			<div id="postpagestats_above" class="postpagestats">
				Visualizzazione dei risultati da 1 a 10 su 16
			</div>
		</div>
	</div>
	<div id="pagetitle" class="pagetitle">
		Discussione: <span class="threadtitle"><a href="http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile.html" title="Ricarica questa pagina"><h1 style="display:inline;">Problema sms Sim Postemobile</h1></a></span>
		
	</div>
	<div id="thread_controls" class="thread_controls toolsmenu">
		<div>
		<ul id="postlist_popups" class="postlist_popups popupgroup">
			
			
			
			<li class="popupmenu" id="linkbacktools">
				<h6><a class="popupctrl" href="javascript://">LinkBack</a><a name="goto_linkback"></a></h6>
				<ul class="popupbody popuphover">
                                             
					<li><a href="http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile.html" onclick="prompt('Use the following URL when referencing this thread from another forum or blog.','http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile.html');return false;"><img class="inlineimg" src="http://www.androidiani.com/forum/vbseo/resources/images/forum/linkback_url.gif" alt="LinkBack URL" /> LinkBack URL</a></li>
					<li><a rel="nofollow" href="http://www.androidiani.com/forum/misc.php?do=linkbacks"><img class="inlineimg" src="http://www.androidiani.com/forum/vbseo/resources/images/forum/linkback_about.gif" alt="About LinkBacks" /> About LinkBacks</a></li>
					
				</ul>
			</li>
<li class="popupmenu" id="threadtools">
				<h6><a class="popupctrl" href="javascript://">Strumenti discussione</a></h6>
				<ul class="popupbody popuphover">
					<li><a href="http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile-print.html" accesskey="3" rel="nofollow">Visualizza la versione stampabile</a></li>
					
					<li>
						
							<a href="http://www.androidiani.com/forum/subscription.php?do=addsubscription&amp;t=358796" rel="nofollow">Sottoscrivi questa discussione&hellip;</a>
						
					</li>
					
				</ul>
			</li>

			

			

			

			
				<li class="popupmenu" id="displaymodes">
					<h6><a class="popupctrl" href="javascript://">Visualizza</a></h6>
					<ul class="popupbody popuphover">
						<li><label>Modalit� lineare</label></li>
						<li><a rel="nofollow" href="http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile.html?mode=hybrid"> Passa alla modalit� ibrida</a></li>
						<li><a rel="nofollow" href="http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile.html?mode=threaded#post4825087"> Passa in modalit� elencata</a></li>
					</ul>
				</li>
			

			
			</ul>
		</div>
	</div>

<div id="postlist" class="postlist restrain">
	

	
		<ol id="posts" class="posts" start="1">
			
<li class="postbitlegacy postbitim postcontainer" id="post_4825087">
<!-- see bottom of postbit.css for .userinfo .popupmenu styles -->

	<div class="posthead">
		
			<span class="postdate old">
				
					<span class="date">16-11-13&nbsp;<span class="time">13:52</span></span>
				
			</span>
			<span class="nodecontrols">
				
					<a name="post4825087" href="http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile.html#post4825087" class="postcounter">#1</a><a id="postcount4825087" name="1"></a>
				
				
				
			</span>
		
	</div>
	<div class="postdetails">
		<div class="userinfo">
			<div class="username_container">
			
				<div class="popupmenu memberaction">
	<a rel="nofollow" class="username offline " href="http://www.androidiani.com/forum/members/piro90.html" title="Piro90 non � in linea"><strong>Piro90</strong></a>
	
</div>
				<img class="inlineimg onlinestatus" src="http://www.androidiani.com/forum/images/styles/androidiani/statusicon/user-offline.png" alt="Piro90 non � in linea" border="0" />

			
			</div>
			<span class="usertitle">
				Baby Droid
			</span>
			
			
			
			 
			
				<hr />
				<dl class="userinfo_extra">
					<dt>Registrato dal</dt> <dd>Jun 2012</dd>
					<dt>Localit�</dt> <dd>Puglia</dd>
					
					<dt>Messaggi</dt> <dd>15</dd>
					<dt>Smartphone</dt> <dd>Samsung Galaxy S3</dd>
					
	<dt>Thanks</dt> <dd>3</dd>
	
		<dd style="white-space:nowrap; display:inline; float: right;">Thanked 2 Times in 2 Posts</dd>
	

				</dl>
				
				
				<div class="imlinks">
					    
				</div>
			
		</div>
		<div class="postbody">
			<div class="postrow has_after_content">
				
			
				<img src="http://www.androidiani.com/forum/images/icons/icon1.png" alt="Predefinito" style="float:left;margin-top:10px" />
<h3 class="title icon"  style="margin:0px 10px;float:left;">Problema sms Sim Postemobile
					</h3>
				
						<span class="twittersharepage"></span><span class="gplusonepage"></span><span class="facebooklikepage"></span>
						<div style="border-bottom:1px solid #aaa;padding-top:10px;margin-bottom:10px"></div>
				
			

						
							
							
						
						
				
				<div class="content">
				
					<div id="post_message_4825087">
						<blockquote class="postcontent restore ">
							Ciao a tutti,<br />
ho un problema con la mia sim PosteMobile.<br />
Qualche tempo fa ho cambiato gestore passando a PosteMobile usufruendo della nota promozione &quot;extra small&quot; da 4,5�/10 anni con minuti,sms e internet incluso. L'ho utilizzata per poco tempo nel mio vecchio Nokia N73 e fin qui tutto bene.<br />
Poco fa ho acquistato un Galaxy S3 no brand da un noto centro commerciale della mia citt� (E***NICS senza fare nomi xD) e ho subito passato la sim PosteMobile nel nuovo smartphone.<br />
Configurazione perfetta, chiamate eccellenti e internet veloce, tutto ok. Dopo qualche giorno (PRECISO SENZA AVER INSTALLATO ALCUNA APP) ho notato che cominciavano a scalare gli sms. Nella cronologia degli sms del telefono non risulta nulla, cos� sono andato a verificare sul dettaglio sul sito di PosteMobile.it e mi sono accorto che venivano inviati 1/2 sms al giorno ad un numero Vodafone che non conosco assolutamente (393424012*** le ultime cifre sono nascoste dal sito per la privacy).<br />
Dal servizio assistenza PosteMobile non sanno cosa dirmi e mi hanno consigliato di farmi sostituire la sim.<br />
So di altri utenti PosteMobile che hanno avuto il mio stesso problema con altri dispositivi. Qualcuno di voi lo ha riscontrato??
						</blockquote>
					</div>
					
					
<!-- LUPO -->
<div style="margin-top:25px;">
<div style="width:250px;float:right">
</div>
<script type="text/javascript"><!--
    var random_number = Math.random();
    if (random_number < 0.50) {
                google_ad_client = "ca-pub-2246076230784003";
                /* Forum, Primo Post (Original) - 1 */
                google_ad_slot = "1227914043";
                google_ad_width = 728;
                google_ad_height = 90;

    } else {
                google_ad_client = "ca-pub-2246076230784003";
                /* Forum, Primo Post (Original) */
                google_ad_slot = "8874885734";
                google_ad_width = 728;
                google_ad_height = 90;
    }

//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
</div>

					

					
				</div>
			</div>
			
			<div class="after_content">
				
                                       
                                
				
		        		<blockquote class="signature restore"><div class="signaturecontainer"><hr /><br />
Noto con piacere che il mio N73 prende meglio del Samsung Galaxy S3 Stock <img src="http://www.androidiani.com/forum/images/smilies/roftl.gif" border="0" alt="" title="Rotfl" class="inlineimg" /></div></blockquote>
				
                                        
			</div>
			
			<div class="cleardiv"></div>
		</div>
	</div>
		<div class="postfoot">
			<!-- <div class="postfoot_container"> -->
			<div class="textcontrols floatcontainer">
				<span class="postcontrols">
					<img style="display:none" id="progress_4825087" src="http://www.androidiani.com/forum/images/styles/androidiani/misc/progress.gif" alt="" />
					
					
					
						<a id="qrwq_4825087" class="newreply" href="http://www.androidiani.com/forum/newreply.php?do=newreply&amp;p=4825087" rel="nofollow"><img id="quoteimg_4825087" src="http://www.androidiani.com/forum/clear.gif" alt="Rispondi quotando" />  Rispondi quotando</a> 
					
					
					
				</span>
				<span class="postlinking">
					
						
					

					
					
					
					

					
					

					
					
					
					
				</span>
			<!-- </div> -->
			</div>
		</div>
	<hr />
</li>
<li class="postbitlegacy postbitim" id="post_thanks_box_4825087" style="display:none">
	
</li>


<!-- Adsense Forum -->

<!-- vb:if condition="$post[postcount] % $vboptions[maxposts] == 1 || $post['islastshown']"-->

<li class="postbitlegacy postbitim postcontainer" id="post_">
<div class="posthead">&nbsp;</div>
<div class="postdetails">
	<div class="userinfo">
		<div class="username_container">
			<span class="usertitle">
				<div style="margin:5px 0;text-align: left;font-weight:bold" class="google_ads_bom_title">Annunci Google</div>
			</span>
		</div>
	</div>
	<div class="postbody">
		<div class="postrow">
			<div class="content">
				<div>
					<blockquote class="postcontent restore">
<div id='div-gpt-ad-1357911084365-1'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1357911084365-1'); });
</script>
</div>
					</blockquote>
				</div>
			</div>
		</div>
		<div class="cleardiv"></div>
	</div>
</div>
</li>




<!-- Fine Adsense2 -->
<li class="postbitlegacy postbitim postcontainer" id="post_4907421">
<!-- see bottom of postbit.css for .userinfo .popupmenu styles -->

	<div class="posthead">
		
			<span class="postdate old">
				
					<span class="date">01-12-13&nbsp;<span class="time">14:20</span></span>
				
			</span>
			<span class="nodecontrols">
				
					<a name="post4907421" href="http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile.html#post4907421" class="postcounter">#2</a><a id="postcount4907421" name="2"></a>
				
				
				
			</span>
		
	</div>
	<div class="postdetails">
		<div class="userinfo">
			<div class="username_container">
			
				<div class="popupmenu memberaction">
	<a rel="nofollow" class="username offline " href="http://www.androidiani.com/forum/members/polpojpg.html" title="polpojpg non � in linea"><strong>polpojpg</strong></a>
	
</div>
				<img class="inlineimg onlinestatus" src="http://www.androidiani.com/forum/images/styles/androidiani/statusicon/user-offline.png" alt="polpojpg non � in linea" border="0" />

			
			</div>
			<span class="usertitle">
				Baby Droid
			</span>
			
			
			
			
			<a rel="nofollow" class="postuseravatar" href="http://www.androidiani.com/forum/members/polpojpg.html" title="polpojpg non � in linea">
				<img src="http://www.androidiani.com/forum/avatars_and_pics/avatars/avatar623206_2.gif" alt="L'avatar di polpojpg" title="L'avatar di polpojpg" />
			</a>
			 
			
				<hr />
				<dl class="userinfo_extra">
					<dt>Registrato dal</dt> <dd>Dec 2013</dd>
					<dt>Localit�</dt> <dd>Genova</dd>
					
					<dt>Messaggi</dt> <dd>13</dd>
					<dt>Smartphone</dt> <dd>Galaxy next, s3, s4,s?</dd>
					
	<dt>Thanks</dt> <dd>1</dd>
	
		<dd style="white-space:nowrap; display:inline; float: right;">Thanked 2 Times in 2 Posts</dd>
	

				</dl>
				
				
				<div class="imlinks">
					    
				</div>
			
		</div>
		<div class="postbody">
			<div class="postrow">
				
			
				<img src="http://www.androidiani.com/forum/images/icons/icon1.png" alt="Predefinito" style="float:left;margin-top:10px" />
<h3 class="title icon"  style="margin:0px 10px;"><div style="padding-top:10px;margin-bottom:10px"></div></h3>
				
			

						
						
				
				<div class="content" style="margin-top:5px">
				
					<div id="post_message_4907421">
						<blockquote class="postcontent restore ">
							<img src="http://www.androidiani.com/forum/images/smilies/sad.gif" border="0" alt="" title="Frown" class="inlineimg" /> Ciao a tutti stesso problema, dal 23-06-2013 giorno che credo di aver inserito per laprima volta la mia sim postemobile su mio galaxy S3 import non ufficilale italia comprato online su gli stoki...  magazzino vendita a Bologna , tutto bene per il telefono ma da allora mi accorsi di addebiti anomali ad ogni accensione del dispositivo di 0,12 centesimi per sms inviato verso il numero 393424012*** numero credo vodafone come da area clienti, da qu� qualche giorno fa inoltrai richiesta a postemobile via fax al num 800242626 per richiedere la lista dei numeri in chiaro.. nessuno sin ora � stato in grado di darmi una spiegazione, il telefono � pulito fatto wipe reset completo e poi molte altre preve che mi lasciano sconcerto, del tipo: ho provato la sim su altri 4 smartphone, next 5570, Galaxy S3, Galaxy S4,huawei ecc ma colpo di scena su nessuno di questi dispositivi succede nulla , nessun addebito nessuna traccia.... qualcuno sa darmi qualche altro dettaglio?
						</blockquote>
					</div>
					
					
					

					
				</div>
			</div>
			
			<div class="cleardiv"></div>
		</div>
	</div>
		<div class="postfoot">
			<!-- <div class="postfoot_container"> -->
			<div class="textcontrols floatcontainer">
				<span class="postcontrols">
					<img style="display:none" id="progress_4907421" src="http://www.androidiani.com/forum/images/styles/androidiani/misc/progress.gif" alt="" />
					
					
					
						<a id="qrwq_4907421" class="newreply" href="http://www.androidiani.com/forum/newreply.php?do=newreply&amp;p=4907421" rel="nofollow"><img id="quoteimg_4907421" src="http://www.androidiani.com/forum/clear.gif" alt="Rispondi quotando" />  Rispondi quotando</a> 
					
					
					
				</span>
				<span class="postlinking">
					
						
					

					
					
					
					

					
					

					
					
					
					
				</span>
			<!-- </div> -->
			</div>
		</div>
	<hr />
</li>
<li class="postbitlegacy postbitim" id="post_thanks_box_4907421" style="display:none">
	
</li>


<!-- Adsense Forum -->

<!-- vb:if condition="$post[postcount] % $vboptions[maxposts] == 1 || $post['islastshown']"-->



<!-- Fine Adsense2 -->
<li class="postbitlegacy postbitim postcontainer" id="post_4908366">
<!-- see bottom of postbit.css for .userinfo .popupmenu styles -->

	<div class="posthead">
		
			<span class="postdate old">
				
					<span class="date">01-12-13&nbsp;<span class="time">17:14</span></span>
				
			</span>
			<span class="nodecontrols">
				
					<a name="post4908366" href="http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile.html#post4908366" class="postcounter">#3</a><a id="postcount4908366" name="3"></a>
				
				
				
			</span>
		
	</div>
	<div class="postdetails">
		<div class="userinfo">
			<div class="username_container">
			
				<div class="popupmenu memberaction">
	<a rel="nofollow" class="username offline " href="http://www.androidiani.com/forum/members/piro90.html" title="Piro90 non � in linea"><strong>Piro90</strong></a>
	
</div>
				<img class="inlineimg onlinestatus" src="http://www.androidiani.com/forum/images/styles/androidiani/statusicon/user-offline.png" alt="Piro90 non � in linea" border="0" />

			
			</div>
			<span class="usertitle">
				Baby Droid
			</span>
			
			
			
			 
			
				<hr />
				<dl class="userinfo_extra">
					<dt>Registrato dal</dt> <dd>Jun 2012</dd>
					<dt>Localit�</dt> <dd>Puglia</dd>
					
					<dt>Messaggi</dt> <dd>15</dd>
					<dt>Smartphone</dt> <dd>Samsung Galaxy S3</dd>
					
	<dt>Thanks</dt> <dd>3</dd>
	
		<dd style="white-space:nowrap; display:inline; float: right;">Thanked 2 Times in 2 Posts</dd>
	

				</dl>
				
				
				<div class="imlinks">
					    
				</div>
			
		</div>
		<div class="postbody">
			<div class="postrow has_after_content">
				
			
				<img src="http://www.androidiani.com/forum/images/icons/icon1.png" alt="Predefinito" style="float:left;margin-top:10px" />
<h3 class="title icon"  style="margin:0px 10px;"><div style="padding-top:10px;margin-bottom:10px"></div></h3>
				
			

						
						
				
				<div class="content" style="margin-top:5px">
				
					<div id="post_message_4908366">
						<blockquote class="postcontent restore ">
							Ciao polpojpg,<br />
<br />
ti aggiorno sulle mie ultime novit�:<br />
<br />
- Hard Reset dello smartphone non ha risolto il problema;<br />
- Controllato in &quot;Avviso cambio Sim&quot; (Impostazioni-&gt;sicurezza-&gt;Avviso Cambio Sim) risulta disattivato;<br />
- Sostituzione Sim PosteMobile il problema persiste ancora;<br />
<br />
Sulla rete, nei vari forum, anche altri utenti hanno riscontrato lo stesso problema: sia utenti puramente Vodafone, altri utenti PosteMobile come noi, e anche utenti CoopVoce, anche se il numero a cui vengono mandati gli sms sono diversi in quanto cambia gestore.<br />
Il numero in questione, per Vodafone e PosteMobile, � questo +39 3424012345. Abbastanza strano in quanto contiene una bella scala di 12345 e provando a chiamare dice che il numero � inesistente o disattivato.<br />
A quanto pare il problema � imputabile all'ultimo firmware dell'S3, infatti dicono che passando ad una versione meno recente il problema si risolve.<br />
Io non volendo rischiare facendo qualche prova, in quanto in questi casi Samsung invalida la garanzia, ho deciso di contattare una community gestita da Samsung.<br />
Questi se ne sono &quot;lavati le mani&quot;, dicendomi di recarmi presso un qualsiasi centro di assistenza Samsung Gold o Platinum e verificare il malfunzionamento con loro.<br />
E ora sto valutando il da farsi.<br />
Per curiosit� tu che versione di Android hai? Con quale kernel?
						</blockquote>
					</div>
					
					
					

					
				</div>
			</div>
			
			<div class="after_content">
				
                                       
                                
				
		        		<blockquote class="signature restore"><div class="signaturecontainer"><hr /><br />
Noto con piacere che il mio N73 prende meglio del Samsung Galaxy S3 Stock <img src="http://www.androidiani.com/forum/images/smilies/roftl.gif" border="0" alt="" title="Rotfl" class="inlineimg" /></div></blockquote>
				
                                        
			</div>
			
			<div class="cleardiv"></div>
		</div>
	</div>
		<div class="postfoot">
			<!-- <div class="postfoot_container"> -->
			<div class="textcontrols floatcontainer">
				<span class="postcontrols">
					<img style="display:none" id="progress_4908366" src="http://www.androidiani.com/forum/images/styles/androidiani/misc/progress.gif" alt="" />
					
					
					
						<a id="qrwq_4908366" class="newreply" href="http://www.androidiani.com/forum/newreply.php?do=newreply&amp;p=4908366" rel="nofollow"><img id="quoteimg_4908366" src="http://www.androidiani.com/forum/clear.gif" alt="Rispondi quotando" />  Rispondi quotando</a> 
					
					
					
				</span>
				<span class="postlinking">
					
						
					

					
					
					
					

					
					

					
					
					
					
				</span>
			<!-- </div> -->
			</div>
		</div>
	<hr />
</li>
<li class="postbitlegacy postbitim" id="post_thanks_box_4908366" style="display:none">
	
</li>


<!-- Adsense Forum -->

<!-- vb:if condition="$post[postcount] % $vboptions[maxposts] == 1 || $post['islastshown']"-->



<!-- Fine Adsense2 -->
<li class="postbitlegacy postbitim postcontainer" id="post_4908412">
<!-- see bottom of postbit.css for .userinfo .popupmenu styles -->

	<div class="posthead">
		
			<span class="postdate old">
				
					<span class="date">01-12-13&nbsp;<span class="time">17:23</span></span>
				
			</span>
			<span class="nodecontrols">
				
					<a name="post4908412" href="http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile.html#post4908412" class="postcounter">#4</a><a id="postcount4908412" name="4"></a>
				
				
				
			</span>
		
	</div>
	<div class="postdetails">
		<div class="userinfo">
			<div class="username_container">
			
				<div class="popupmenu memberaction">
	<a rel="nofollow" class="username offline " href="http://www.androidiani.com/forum/members/polpojpg.html" title="polpojpg non � in linea"><strong>polpojpg</strong></a>
	
</div>
				<img class="inlineimg onlinestatus" src="http://www.androidiani.com/forum/images/styles/androidiani/statusicon/user-offline.png" alt="polpojpg non � in linea" border="0" />

			
			</div>
			<span class="usertitle">
				Baby Droid
			</span>
			
			
			
			
			<a rel="nofollow" class="postuseravatar" href="http://www.androidiani.com/forum/members/polpojpg.html" title="polpojpg non � in linea">
				<img src="http://www.androidiani.com/forum/avatars_and_pics/avatars/avatar623206_2.gif" alt="L'avatar di polpojpg" title="L'avatar di polpojpg" />
			</a>
			 
			
				<hr />
				<dl class="userinfo_extra">
					<dt>Registrato dal</dt> <dd>Dec 2013</dd>
					<dt>Localit�</dt> <dd>Genova</dd>
					
					<dt>Messaggi</dt> <dd>13</dd>
					<dt>Smartphone</dt> <dd>Galaxy next, s3, s4,s?</dd>
					
	<dt>Thanks</dt> <dd>1</dd>
	
		<dd style="white-space:nowrap; display:inline; float: right;">Thanked 2 Times in 2 Posts</dd>
	

				</dl>
				
				
				<div class="imlinks">
					    
				</div>
			
		</div>
		<div class="postbody">
			<div class="postrow">
				
			
				<img src="http://www.androidiani.com/forum/images/icons/icon1.png" alt="Predefinito" style="float:left;margin-top:10px" />
<h3 class="title icon"  style="margin:0px 10px;"><div style="padding-top:10px;margin-bottom:10px"></div></h3>
				
			

						
						
				
				<div class="content" style="margin-top:5px">
				
					<div id="post_message_4908412">
						<blockquote class="postcontent restore ">
							Ciao grazi... allora poi ci guardo e ti dico ma posso dirti ora immediatamente che me lo faceva sia prima che andasse in assistenza per quel problemino antennna che ora, lunica cosa che ho notato nella ricevuta di consegna riparazione che gli amici di milano dell'assistenza hamesso mano anche ad un aggiornamento firmware,  non ci crederai io avevo il firmware suadfrica credo se non ricordo male identificato con xfe non importa ma ora certamente ti dicevo i tecnici dellassistenza mi misero dentro stranamente un firmware ITV , pur essendo il mio un dispositivo import come ho verificato risulta esserlo ancora per Samsung , ho chiesto in assistenza e il mio imiei � associato ad un device import mentre sotto ho un firmware ITV...Che ne pensi?
						</blockquote>
					</div>
					
					
					

					
				</div>
			</div>
			
			<div class="cleardiv"></div>
		</div>
	</div>
		<div class="postfoot">
			<!-- <div class="postfoot_container"> -->
			<div class="textcontrols floatcontainer">
				<span class="postcontrols">
					<img style="display:none" id="progress_4908412" src="http://www.androidiani.com/forum/images/styles/androidiani/misc/progress.gif" alt="" />
					
					
					
						<a id="qrwq_4908412" class="newreply" href="http://www.androidiani.com/forum/newreply.php?do=newreply&amp;p=4908412" rel="nofollow"><img id="quoteimg_4908412" src="http://www.androidiani.com/forum/clear.gif" alt="Rispondi quotando" />  Rispondi quotando</a> 
					
					
					
				</span>
				<span class="postlinking">
					
						
					

					
					
					
					

					
					

					
					
					
					
				</span>
			<!-- </div> -->
			</div>
		</div>
	<hr />
</li>
<li class="postbitlegacy postbitim" id="post_thanks_box_4908412" style="display:none">
	
</li>


<!-- Adsense Forum -->

<!-- vb:if condition="$post[postcount] % $vboptions[maxposts] == 1 || $post['islastshown']"-->



<!-- Fine Adsense2 -->
<li class="postbitlegacy postbitim postcontainer" id="post_4908495">
<!-- see bottom of postbit.css for .userinfo .popupmenu styles -->

	<div class="posthead">
		
			<span class="postdate old">
				
					<span class="date">01-12-13&nbsp;<span class="time">17:42</span></span>
				
			</span>
			<span class="nodecontrols">
				
					<a name="post4908495" href="http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile.html#post4908495" class="postcounter">#5</a><a id="postcount4908495" name="5"></a>
				
				
				
			</span>
		
	</div>
	<div class="postdetails">
		<div class="userinfo">
			<div class="username_container">
			
				<div class="popupmenu memberaction">
	<a rel="nofollow" class="username offline " href="http://www.androidiani.com/forum/members/piro90.html" title="Piro90 non � in linea"><strong>Piro90</strong></a>
	
</div>
				<img class="inlineimg onlinestatus" src="http://www.androidiani.com/forum/images/styles/androidiani/statusicon/user-offline.png" alt="Piro90 non � in linea" border="0" />

			
			</div>
			<span class="usertitle">
				Baby Droid
			</span>
			
			
			
			 
			
				<hr />
				<dl class="userinfo_extra">
					<dt>Registrato dal</dt> <dd>Jun 2012</dd>
					<dt>Localit�</dt> <dd>Puglia</dd>
					
					<dt>Messaggi</dt> <dd>15</dd>
					<dt>Smartphone</dt> <dd>Samsung Galaxy S3</dd>
					
	<dt>Thanks</dt> <dd>3</dd>
	
		<dd style="white-space:nowrap; display:inline; float: right;">Thanked 2 Times in 2 Posts</dd>
	

				</dl>
				
				
				<div class="imlinks">
					    
				</div>
			
		</div>
		<div class="postbody">
			<div class="postrow has_after_content">
				
			
				<img src="http://www.androidiani.com/forum/images/icons/icon1.png" alt="Predefinito" style="float:left;margin-top:10px" />
<h3 class="title icon"  style="margin:0px 10px;"><div style="padding-top:10px;margin-bottom:10px"></div></h3>
				
			

						
						
				
				<div class="content" style="margin-top:5px">
				
					<div id="post_message_4908495">
						<blockquote class="postcontent restore ">
							non so dirti precisamente quale sia il firmware che risolve i problema e come mai ti abbiano messo un firmware ITV, per� penso che se te lo abbiano fatto in un centro assistenza abbiano avuto buoni motivi, l'importante che non ti abbia dato altri problemi...
						</blockquote>
					</div>
					
					
					

					
				</div>
			</div>
			
			<div class="after_content">
				
                                       
                                
				
		        		<blockquote class="signature restore"><div class="signaturecontainer"><hr /><br />
Noto con piacere che il mio N73 prende meglio del Samsung Galaxy S3 Stock <img src="http://www.androidiani.com/forum/images/smilies/roftl.gif" border="0" alt="" title="Rotfl" class="inlineimg" /></div></blockquote>
				
                                        
			</div>
			
			<div class="cleardiv"></div>
		</div>
	</div>
		<div class="postfoot">
			<!-- <div class="postfoot_container"> -->
			<div class="textcontrols floatcontainer">
				<span class="postcontrols">
					<img style="display:none" id="progress_4908495" src="http://www.androidiani.com/forum/images/styles/androidiani/misc/progress.gif" alt="" />
					
					
					
						<a id="qrwq_4908495" class="newreply" href="http://www.androidiani.com/forum/newreply.php?do=newreply&amp;p=4908495" rel="nofollow"><img id="quoteimg_4908495" src="http://www.androidiani.com/forum/clear.gif" alt="Rispondi quotando" />  Rispondi quotando</a> 
					
					
					
				</span>
				<span class="postlinking">
					
						
					

					
					
					
					

					
					

					
					
					
					
				</span>
			<!-- </div> -->
			</div>
		</div>
	<hr />
</li>
<li class="postbitlegacy postbitim" id="post_thanks_box_4908495">
	<div class="postbody" style="margin-left: 0">
	<div class="postrow">
	
	<h2 class="title">
		
			Il seguente Utente ha ringraziato Piro90 per il post:
		
	</h2>
	<div class="content">
		<div id="post_thanks_bit_4908495">
			<blockquote class="postcontent restore">
				<a href="http://www.androidiani.com/forum/members/polpojpg.html" rel="nofollow">polpojpg</a>&nbsp;(01-12-13)
			</blockquote>
		</div>
	</div>
	</div>
</div>
<hr />
</li>


<!-- Adsense Forum -->

<!-- vb:if condition="$post[postcount] % $vboptions[maxposts] == 1 || $post['islastshown']"-->



<!-- Fine Adsense2 -->
<li class="postbitlegacy postbitim postcontainer" id="post_4908574">
<!-- see bottom of postbit.css for .userinfo .popupmenu styles -->

	<div class="posthead">
		
			<span class="postdate old">
				
					<span class="date">01-12-13&nbsp;<span class="time">17:55</span></span>
				
			</span>
			<span class="nodecontrols">
				
					<a name="post4908574" href="http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile.html#post4908574" class="postcounter">#6</a><a id="postcount4908574" name="6"></a>
				
				
				
			</span>
		
	</div>
	<div class="postdetails">
		<div class="userinfo">
			<div class="username_container">
			
				<div class="popupmenu memberaction">
	<a rel="nofollow" class="username offline " href="http://www.androidiani.com/forum/members/polpojpg.html" title="polpojpg non � in linea"><strong>polpojpg</strong></a>
	
</div>
				<img class="inlineimg onlinestatus" src="http://www.androidiani.com/forum/images/styles/androidiani/statusicon/user-offline.png" alt="polpojpg non � in linea" border="0" />

			
			</div>
			<span class="usertitle">
				Baby Droid
			</span>
			
			
			
			
			<a rel="nofollow" class="postuseravatar" href="http://www.androidiani.com/forum/members/polpojpg.html" title="polpojpg non � in linea">
				<img src="http://www.androidiani.com/forum/avatars_and_pics/avatars/avatar623206_2.gif" alt="L'avatar di polpojpg" title="L'avatar di polpojpg" />
			</a>
			 
			
				<hr />
				<dl class="userinfo_extra">
					<dt>Registrato dal</dt> <dd>Dec 2013</dd>
					<dt>Localit�</dt> <dd>Genova</dd>
					
					<dt>Messaggi</dt> <dd>13</dd>
					<dt>Smartphone</dt> <dd>Galaxy next, s3, s4,s?</dd>
					
	<dt>Thanks</dt> <dd>1</dd>
	
		<dd style="white-space:nowrap; display:inline; float: right;">Thanked 2 Times in 2 Posts</dd>
	

				</dl>
				
				
				<div class="imlinks">
					    
				</div>
			
		</div>
		<div class="postbody">
			<div class="postrow has_after_content">
				
			
				<img src="http://www.androidiani.com/forum/images/icons/icon1.png" alt="Predefinito" style="float:left;margin-top:10px" />
<h3 class="title icon"  style="margin:0px 10px;"><div style="padding-top:10px;margin-bottom:10px"></div></h3>
				
			

						
						
				
				<div class="content" style="margin-top:5px">
				
					<div id="post_message_4908574">
						<blockquote class="postcontent restore ">
							No anzi va bene per me il cell � perfetto anzi me la tiro come ufficiale italia, anche se renderlo ufficiale �  molto semplice. .. ti compri il modulo jig 2,96 &#8364; su ebay e ti flasglhi il telefono , poi con il mod jig se vuoi tornare indietro all'originale � un attimo e funziona funziona molto bene io avevo un next brad TIM l'ho sbrandizzato con il Samsung Galaxy tutti S S3 Unbrick Mod Download Jig USB Dongle prendendo il firmware da un next ITV originale e ora anche kies me lo riconosce come originale, ma no divaghiamo ora devo risolvere sto problema. ...ciao e grazie
						</blockquote>
					</div>
					
					
					

					
				</div>
			</div>
			
			<div class="after_content">
				
				<!-- edit note -->
				<blockquote class="postcontent lastedited">
<!--					<img src="http://www.androidiani.com/forum/images/styles/androidiani/buttons/edit_40b.png" class="inlineimg" alt="" /> -->
					
						Ultima modifica di polpojpg;  01-12-13 a <span class="time">19:14</span>
					
					
						<span class="reason">Motivo:</span> errore di scrittura
					
				</blockquote>
				<!-- / edit note -->
				
                                       
                                
				
                                        
			</div>
			
			<div class="cleardiv"></div>
		</div>
	</div>
		<div class="postfoot">
			<!-- <div class="postfoot_container"> -->
			<div class="textcontrols floatcontainer">
				<span class="postcontrols">
					<img style="display:none" id="progress_4908574" src="http://www.androidiani.com/forum/images/styles/androidiani/misc/progress.gif" alt="" />
					
					
					
						<a id="qrwq_4908574" class="newreply" href="http://www.androidiani.com/forum/newreply.php?do=newreply&amp;p=4908574" rel="nofollow"><img id="quoteimg_4908574" src="http://www.androidiani.com/forum/clear.gif" alt="Rispondi quotando" />  Rispondi quotando</a> 
					
					
					
				</span>
				<span class="postlinking">
					
						
					

					
					
					
					

					
					

					
					
					
					
				</span>
			<!-- </div> -->
			</div>
		</div>
	<hr />
</li>
<li class="postbitlegacy postbitim" id="post_thanks_box_4908574" style="display:none">
	
</li>


<!-- Adsense Forum -->

<!-- vb:if condition="$post[postcount] % $vboptions[maxposts] == 1 || $post['islastshown']"-->



<!-- Fine Adsense2 -->
<li class="postbitlegacy postbitim postcontainer" id="post_4946138">
<!-- see bottom of postbit.css for .userinfo .popupmenu styles -->

	<div class="posthead">
		
			<span class="postdate old">
				
					<span class="date">08-12-13&nbsp;<span class="time">17:16</span></span>
				
			</span>
			<span class="nodecontrols">
				
					<a name="post4946138" href="http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile.html#post4946138" class="postcounter">#7</a><a id="postcount4946138" name="7"></a>
				
				
				
			</span>
		
	</div>
	<div class="postdetails">
		<div class="userinfo">
			<div class="username_container">
			
				<div class="popupmenu memberaction">
	<a rel="nofollow" class="username offline " href="http://www.androidiani.com/forum/members/polpojpg.html" title="polpojpg non � in linea"><strong>polpojpg</strong></a>
	
</div>
				<img class="inlineimg onlinestatus" src="http://www.androidiani.com/forum/images/styles/androidiani/statusicon/user-offline.png" alt="polpojpg non � in linea" border="0" />

			
			</div>
			<span class="usertitle">
				Baby Droid
			</span>
			
			
			
			
			<a rel="nofollow" class="postuseravatar" href="http://www.androidiani.com/forum/members/polpojpg.html" title="polpojpg non � in linea">
				<img src="http://www.androidiani.com/forum/avatars_and_pics/avatars/avatar623206_2.gif" alt="L'avatar di polpojpg" title="L'avatar di polpojpg" />
			</a>
			 
			
				<hr />
				<dl class="userinfo_extra">
					<dt>Registrato dal</dt> <dd>Dec 2013</dd>
					<dt>Localit�</dt> <dd>Genova</dd>
					
					<dt>Messaggi</dt> <dd>13</dd>
					<dt>Smartphone</dt> <dd>Galaxy next, s3, s4,s?</dd>
					
	<dt>Thanks</dt> <dd>1</dd>
	
		<dd style="white-space:nowrap; display:inline; float: right;">Thanked 2 Times in 2 Posts</dd>
	

				</dl>
				
				
				<div class="imlinks">
					    
				</div>
			
		</div>
		<div class="postbody">
			<div class="postrow">
				
			
				<img src="http://www.androidiani.com/forum/images/icons/icon1.png" alt="Predefinito" style="float:left;margin-top:10px" />
<h3 class="title icon"  style="margin:0px 10px;"><div style="padding-top:10px;margin-bottom:10px"></div></h3>
				
			

						
						
				
				<div class="content" style="margin-top:5px">
				
					<div id="post_message_4946138">
						<blockquote class="postcontent restore ">
							Ciao a tutti, ancora nessuna novit� in merito al problema segnalato, se non che dopo varie segnalazioni a PosteMobile fui contattato pi� volte da operatori diversi e come da me precedentemente richiesto via Fax al n� 800242626 mi inviarono la lista completa degli SMS incriminati comprensiva degli ultimi numeri mancanti (<font color="#FF0000">393424012</font>***) che sono e confermo il  <font color="#FF0000">345</font> gia anticipati da Piro90, dopodich� per farla breve l'operatore procedette al rimborso del credito anomalo sottratto dal 23-06-2013 ad oggi,inoltrando le segnalazioni ai reparti preposti per approfondimenti, praticamente � proprio il caso di dire: RIMBORSATO ma NON SODDISFATTO xk� a tutt'oggi il problema rimane....
						</blockquote>
					</div>
					
					
					

					
				</div>
			</div>
			
			<div class="cleardiv"></div>
		</div>
	</div>
		<div class="postfoot">
			<!-- <div class="postfoot_container"> -->
			<div class="textcontrols floatcontainer">
				<span class="postcontrols">
					<img style="display:none" id="progress_4946138" src="http://www.androidiani.com/forum/images/styles/androidiani/misc/progress.gif" alt="" />
					
					
					
						<a id="qrwq_4946138" class="newreply" href="http://www.androidiani.com/forum/newreply.php?do=newreply&amp;p=4946138" rel="nofollow"><img id="quoteimg_4946138" src="http://www.androidiani.com/forum/clear.gif" alt="Rispondi quotando" />  Rispondi quotando</a> 
					
					
					
				</span>
				<span class="postlinking">
					
						
					

					
					
					
					

					
					

					
					
					
					
				</span>
			<!-- </div> -->
			</div>
		</div>
	<hr />
</li>
<li class="postbitlegacy postbitim" id="post_thanks_box_4946138">
	<div class="postbody" style="margin-left: 0">
	<div class="postrow">
	
	<h2 class="title">
		
			Il seguente Utente ha ringraziato polpojpg per il post:
		
	</h2>
	<div class="content">
		<div id="post_thanks_bit_4946138">
			<blockquote class="postcontent restore">
				<a href="http://www.androidiani.com/forum/members/piro90.html" rel="nofollow">Piro90</a>&nbsp;(08-12-13)
			</blockquote>
		</div>
	</div>
	</div>
</div>
<hr />
</li>


<!-- Adsense Forum -->

<!-- vb:if condition="$post[postcount] % $vboptions[maxposts] == 1 || $post['islastshown']"-->



<!-- Fine Adsense2 -->
<li class="postbitlegacy postbitim postcontainer" id="post_4947200">
<!-- see bottom of postbit.css for .userinfo .popupmenu styles -->

	<div class="posthead">
		
			<span class="postdate old">
				
					<span class="date">08-12-13&nbsp;<span class="time">20:24</span></span>
				
			</span>
			<span class="nodecontrols">
				
					<a name="post4947200" href="http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile.html#post4947200" class="postcounter">#8</a><a id="postcount4947200" name="8"></a>
				
				
				
			</span>
		
	</div>
	<div class="postdetails">
		<div class="userinfo">
			<div class="username_container">
			
				<div class="popupmenu memberaction">
	<a rel="nofollow" class="username offline " href="http://www.androidiani.com/forum/members/piro90.html" title="Piro90 non � in linea"><strong>Piro90</strong></a>
	
</div>
				<img class="inlineimg onlinestatus" src="http://www.androidiani.com/forum/images/styles/androidiani/statusicon/user-offline.png" alt="Piro90 non � in linea" border="0" />

			
			</div>
			<span class="usertitle">
				Baby Droid
			</span>
			
			
			
			 
			
				<hr />
				<dl class="userinfo_extra">
					<dt>Registrato dal</dt> <dd>Jun 2012</dd>
					<dt>Localit�</dt> <dd>Puglia</dd>
					
					<dt>Messaggi</dt> <dd>15</dd>
					<dt>Smartphone</dt> <dd>Samsung Galaxy S3</dd>
					
	<dt>Thanks</dt> <dd>3</dd>
	
		<dd style="white-space:nowrap; display:inline; float: right;">Thanked 2 Times in 2 Posts</dd>
	

				</dl>
				
				
				<div class="imlinks">
					    
				</div>
			
		</div>
		<div class="postbody">
			<div class="postrow has_after_content">
				
			
				<img src="http://www.androidiani.com/forum/images/icons/icon1.png" alt="Predefinito" style="float:left;margin-top:10px" />
<h3 class="title icon"  style="margin:0px 10px;"><div style="padding-top:10px;margin-bottom:10px"></div></h3>
				
			

						
						
				
				<div class="content" style="margin-top:5px">
				
					<div id="post_message_4947200">
						<blockquote class="postcontent restore ">
							io consiglio a tutti quelli che hanno riscontrato il problema di sollecitare i propri operatori per trovare una soluzione al problema...pi� siamo e maggiori sono le possibilit� che il problema sia risolto nel pi� breve tempo possibile. Come disse Gianni Morandi: &quot;Siamo uniti!&quot;
						</blockquote>
					</div>
					
					
					

					
				</div>
			</div>
			
			<div class="after_content">
				
                                       
                                
				
		        		<blockquote class="signature restore"><div class="signaturecontainer"><hr /><br />
Noto con piacere che il mio N73 prende meglio del Samsung Galaxy S3 Stock <img src="http://www.androidiani.com/forum/images/smilies/roftl.gif" border="0" alt="" title="Rotfl" class="inlineimg" /></div></blockquote>
				
                                        
			</div>
			
			<div class="cleardiv"></div>
		</div>
	</div>
		<div class="postfoot">
			<!-- <div class="postfoot_container"> -->
			<div class="textcontrols floatcontainer">
				<span class="postcontrols">
					<img style="display:none" id="progress_4947200" src="http://www.androidiani.com/forum/images/styles/androidiani/misc/progress.gif" alt="" />
					
					
					
						<a id="qrwq_4947200" class="newreply" href="http://www.androidiani.com/forum/newreply.php?do=newreply&amp;p=4947200" rel="nofollow"><img id="quoteimg_4947200" src="http://www.androidiani.com/forum/clear.gif" alt="Rispondi quotando" />  Rispondi quotando</a> 
					
					
					
				</span>
				<span class="postlinking">
					
						
					

					
					
					
					

					
					

					
					
					
					
				</span>
			<!-- </div> -->
			</div>
		</div>
	<hr />
</li>
<li class="postbitlegacy postbitim" id="post_thanks_box_4947200" style="display:none">
	
</li>


<!-- Adsense Forum -->

<!-- vb:if condition="$post[postcount] % $vboptions[maxposts] == 1 || $post['islastshown']"-->



<!-- Fine Adsense2 -->
<li class="postbitlegacy postbitim postcontainer" id="post_4947395">
<!-- see bottom of postbit.css for .userinfo .popupmenu styles -->

	<div class="posthead">
		
			<span class="postdate old">
				
					<span class="date">08-12-13&nbsp;<span class="time">21:03</span></span>
				
			</span>
			<span class="nodecontrols">
				
					<a name="post4947395" href="http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile.html#post4947395" class="postcounter">#9</a><a id="postcount4947395" name="9"></a>
				
				
				
			</span>
		
	</div>
	<div class="postdetails">
		<div class="userinfo">
			<div class="username_container">
			
				<div class="popupmenu memberaction">
	<a rel="nofollow" class="username offline " href="http://www.androidiani.com/forum/members/polpojpg.html" title="polpojpg non � in linea"><strong>polpojpg</strong></a>
	
</div>
				<img class="inlineimg onlinestatus" src="http://www.androidiani.com/forum/images/styles/androidiani/statusicon/user-offline.png" alt="polpojpg non � in linea" border="0" />

			
			</div>
			<span class="usertitle">
				Baby Droid
			</span>
			
			
			
			
			<a rel="nofollow" class="postuseravatar" href="http://www.androidiani.com/forum/members/polpojpg.html" title="polpojpg non � in linea">
				<img src="http://www.androidiani.com/forum/avatars_and_pics/avatars/avatar623206_2.gif" alt="L'avatar di polpojpg" title="L'avatar di polpojpg" />
			</a>
			 
			
				<hr />
				<dl class="userinfo_extra">
					<dt>Registrato dal</dt> <dd>Dec 2013</dd>
					<dt>Localit�</dt> <dd>Genova</dd>
					
					<dt>Messaggi</dt> <dd>13</dd>
					<dt>Smartphone</dt> <dd>Galaxy next, s3, s4,s?</dd>
					
	<dt>Thanks</dt> <dd>1</dd>
	
		<dd style="white-space:nowrap; display:inline; float: right;">Thanked 2 Times in 2 Posts</dd>
	

				</dl>
				
				
				<div class="imlinks">
					    
				</div>
			
		</div>
		<div class="postbody">
			<div class="postrow has_after_content">
				
			
				<img src="http://www.androidiani.com/forum/images/icons/icon1.png" alt="Predefinito" style="float:left;margin-top:10px" />
<h3 class="title icon"  style="margin:0px 10px;"><div style="padding-top:10px;margin-bottom:10px"></div></h3>
				
			

						
						
				
				<div class="content" style="margin-top:5px">
				
					<div id="post_message_4947395">
						<blockquote class="postcontent restore ">
							S� sono d'accordo , dovrebbe avere pi� visibilit� il problema,  visto che a me' l'assistenza Samsung mi considera poco o niente direi,  dovrebbe a mio modesto parere intervenire il gestore PosteMobile e segnalare il problema a Samsung,  credo che tra grandi si ascoltino,o almeno credo! appurato che l'anomalia avviene soltanto associando il Galaxy S3 con la SIM PosteMobile,  in questo caso dico, a meno che al gestore PosteMobile.. non importi nulla perdere la propria clientela. .quindi sollecitate Gente... <img src="http://www.androidiani.com/forum/images/smilies/emo_im_winking.png" border="0" alt="" title="Wink" class="inlineimg" />
						</blockquote>
					</div>
					
					
					

					
				</div>
			</div>
			
			<div class="after_content">
				
				<!-- edit note -->
				<blockquote class="postcontent lastedited">
<!--					<img src="http://www.androidiani.com/forum/images/styles/androidiani/buttons/edit_40b.png" class="inlineimg" alt="" /> -->
					
						Ultima modifica di polpojpg;  08-12-13 a <span class="time">21:07</span>
					
					
						<span class="reason">Motivo:</span> Errore scrittura
					
				</blockquote>
				<!-- / edit note -->
				
                                       
                                
				
                                        
			</div>
			
			<div class="cleardiv"></div>
		</div>
	</div>
		<div class="postfoot">
			<!-- <div class="postfoot_container"> -->
			<div class="textcontrols floatcontainer">
				<span class="postcontrols">
					<img style="display:none" id="progress_4947395" src="http://www.androidiani.com/forum/images/styles/androidiani/misc/progress.gif" alt="" />
					
					
					
						<a id="qrwq_4947395" class="newreply" href="http://www.androidiani.com/forum/newreply.php?do=newreply&amp;p=4947395" rel="nofollow"><img id="quoteimg_4947395" src="http://www.androidiani.com/forum/clear.gif" alt="Rispondi quotando" />  Rispondi quotando</a> 
					
					
					
				</span>
				<span class="postlinking">
					
						
					

					
					
					
					

					
					

					
					
					
					
				</span>
			<!-- </div> -->
			</div>
		</div>
	<hr />
</li>
<li class="postbitlegacy postbitim" id="post_thanks_box_4947395" style="display:none">
	
</li>


<!-- Adsense Forum -->

<!-- vb:if condition="$post[postcount] % $vboptions[maxposts] == 1 || $post['islastshown']"-->



<!-- Fine Adsense2 -->
<li class="postbitlegacy postbitim postcontainer" id="post_4953897">
<!-- see bottom of postbit.css for .userinfo .popupmenu styles -->

	<div class="posthead">
		
			<span class="postdate old">
				
					<span class="date">09-12-13&nbsp;<span class="time">22:39</span></span>
				
			</span>
			<span class="nodecontrols">
				
					<a name="post4953897" href="http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile.html#post4953897" class="postcounter">#10</a><a id="postcount4953897" name="10"></a>
				
				
				
			</span>
		
	</div>
	<div class="postdetails">
		<div class="userinfo">
			<div class="username_container">
			
				<div class="popupmenu memberaction">
	<a rel="nofollow" class="username offline " href="http://www.androidiani.com/forum/members/piro90.html" title="Piro90 non � in linea"><strong>Piro90</strong></a>
	
</div>
				<img class="inlineimg onlinestatus" src="http://www.androidiani.com/forum/images/styles/androidiani/statusicon/user-offline.png" alt="Piro90 non � in linea" border="0" />

			
			</div>
			<span class="usertitle">
				Baby Droid
			</span>
			
			
			
			 
			
				<hr />
				<dl class="userinfo_extra">
					<dt>Registrato dal</dt> <dd>Jun 2012</dd>
					<dt>Localit�</dt> <dd>Puglia</dd>
					
					<dt>Messaggi</dt> <dd>15</dd>
					<dt>Smartphone</dt> <dd>Samsung Galaxy S3</dd>
					
	<dt>Thanks</dt> <dd>3</dd>
	
		<dd style="white-space:nowrap; display:inline; float: right;">Thanked 2 Times in 2 Posts</dd>
	

				</dl>
				
				
				<div class="imlinks">
					    
				</div>
			
		</div>
		<div class="postbody">
			<div class="postrow has_after_content">
				
			
				<img src="http://www.androidiani.com/forum/images/icons/icon1.png" alt="Predefinito" style="float:left;margin-top:10px" />
<h3 class="title icon"  style="margin:0px 10px;"><div style="padding-top:10px;margin-bottom:10px"></div></h3>
				
			

						
						
							
						
				
				<div class="content" style="margin-top:5px">
				
					<div id="post_message_4953897">
						<blockquote class="postcontent restore ">
							Un altro dubbio che mi � sorto: ma l's3 dato in abbonamento da postemobile che firmware avr�?  Se c'� qualcuno che pu� che pu� illuminarci si faccia avanti
						</blockquote>
					</div>
					
					
					

					
				</div>
			</div>
			
			<div class="after_content">
				
                                       
                                
				
		        		<blockquote class="signature restore"><div class="signaturecontainer"><hr /><br />
Noto con piacere che il mio N73 prende meglio del Samsung Galaxy S3 Stock <img src="http://www.androidiani.com/forum/images/smilies/roftl.gif" border="0" alt="" title="Rotfl" class="inlineimg" /></div></blockquote>
				
                                        
			</div>
			
			<div class="cleardiv"></div>
		</div>
	</div>
		<div class="postfoot">
			<!-- <div class="postfoot_container"> -->
			<div class="textcontrols floatcontainer">
				<span class="postcontrols">
					<img style="display:none" id="progress_4953897" src="http://www.androidiani.com/forum/images/styles/androidiani/misc/progress.gif" alt="" />
					
					
					
						<a id="qrwq_4953897" class="newreply" href="http://www.androidiani.com/forum/newreply.php?do=newreply&amp;p=4953897" rel="nofollow"><img id="quoteimg_4953897" src="http://www.androidiani.com/forum/clear.gif" alt="Rispondi quotando" />  Rispondi quotando</a> 
					
					
					
				</span>
				<span class="postlinking">
					
						
					

					
					
					
					

					
					

					
					
					
					
				</span>
			<!-- </div> -->
			</div>
		</div>
	<hr />
</li>
<li class="postbitlegacy postbitim" id="post_thanks_box_4953897" style="display:none">
	
</li>


<!-- Adsense Forum -->

<!-- vb:if condition="$post[postcount] % $vboptions[maxposts] == 1 || $post['islastshown']"-->



<!-- Fine Adsense2 -->
		</ol>
		<div class="separator"></div>
		<div class="postlistfoot">
			
		</div>

	

</div>

<div id="below_postlist" class="noinlinemod below_postlist">
	
	<div id="pagination_bottom" class="pagination_bottom">
<div style="width:728px;float:right">
<div id='div-gpt-ad-1370204836492-0' style='width:728px; height:90px;'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1370204836492-0'); });
</script>
</div>
</div>
	
		<form action="http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile-2.html" method="get" class="pagination popupmenu nohovermenu">
<input type="hidden" name="t" value="358796" /><input type="hidden" name="s" value="9696167c94718066d86ee6909ffa9e7f" />
	
		<span><a href="javascript://" class="popupctrl">Pagina 1 di 2</a></span>
		
		
		
		<span class="selected"><a href="javascript://" title="Visualizzazione dei risultati da 1 a 10 su 16">1</a></span><span><a href="http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile-2.html" title="visualizza i risultati 11 sin 16 da 16">2</a></span>
		
		
		<span class="prev_next"><a rel="next" href="http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile-2.html" title="Pagina seguente - Resultati 11 sino 16 di 16"><img src="http://www.androidiani.com/forum/images/styles/androidiani/pagination/next-right.png" alt="Seguente" /></a></span>
		
		
		<span class="first_last"><a href="http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile-2.html" title="ultima pagina - Resultati 11 sino 16 di 16">ultimo<img src="http://www.androidiani.com/forum/images/styles/androidiani/pagination/last-right.png" alt="ultimo" /></a></span>
		
	
	<ul class="popupbody popuphover">
		<li class="formsubmit jumptopage"><label>Vai alla pagina <input type="text" name="page" size="4" /></label> <input type="submit" class="button" value="Prosegui" /></li>
	</ul>
</form>
	
		
	</div>
</div>








	<!-- next / previous links -->
	<div class="navlinks">
		
			<strong>&laquo;</strong>
			<a href="http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile-prev-thread.html" rel="nofollow">Discussione precedente</a>
			|
			<a href="http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile-next-thread.html" rel="nofollow">Prossima discussione</a>
			<strong>&raquo;</strong>
		
	</div>
	<!-- / next / previous links -->

<div id="thread_info" class="thread_info block">
	
	
	
	
	
	
	<h4 class="threadinfohead blockhead">Tag per questa discussione</h4>
	<div id="thread_tags_list" class="thread_info_block blockbody formcontrols">
		<div class="inner_block">
			
			<div id='tag_list_cell'>
				
		
			<a href="http://www.androidiani.com/forum/tags/galaxy%20s3.html">galaxy s3</a>, 
		
			<a href="http://www.androidiani.com/forum/tags/postemobile.html">postemobile</a>, 
		
			<a href="http://www.androidiani.com/forum/tags/problema.html">problema</a>, 
		
			<a href="http://www.androidiani.com/forum/tags/sms.html">sms</a>
		

<img src="http://www.androidiani.com/forum/images/styles/androidiani/misc/11x11progress.gif" id="tag_form_progress" class="inlineimg hidden" alt="" />
			</div>
			<p>
				<a href="http://www.androidiani.com/forum/tags/">Visualizza nuvola dei tag</a>
			</p>
		</div>
	</div>
	
	<div class="options_block_container">
		
		<div class="options_block">
			<h4 class="collapse blockhead options_correct">
				<a class="collapse" id="collapse_posting_rules" href="http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile.html#top"><img src="http://www.androidiani.com/forum/images/styles/androidiani/buttons/collapse_40b.png" alt="" /></a>
				Permessi di invio
			</h4>
			<div id="posting_rules" class="thread_info_block blockbody formcontrols floatcontainer options_correct">
				
<div id="forumrules" class="info_subblock">

	<ul class="youcandoblock">
		<li><strong>Non puoi</strong> inserire discussioni</li>
		<li><strong>Non puoi</strong> inserire repliche</li>
		<li><strong>Non puoi</strong> inserire allegati</li>
		<li><strong>Non puoi</strong> modificare i tuoi messaggi</li>
		<li>&nbsp;</li>
	</ul>
	<div class="bbcodeblock">
		<ul>
			<li><a rel="nofollow" href="http://www.androidiani.com/forum/misc.php?do=bbcode" target="_blank">Il codice BB</a> �  <strong>attivo</strong></li>
			<li><a rel="nofollow" href="http://www.androidiani.com/forum/misc.php?do=showsmilies" target="_blank">Le smilie</a> sono attive </li>
			<li><a rel="nofollow" href="http://www.androidiani.com/forum/misc.php?do=bbcode#imgcode" target="_blank">Il codice</a> [IMG] <strong>attivo</strong></li>
			<li><a rel="nofollow" href="http://www.androidiani.com/forum/misc.php?do=bbcode#videocode" target="_blank">[VIDEO]</a> code is <strong>attivo</strong></li>
			<li>il codice HTML � <strong>disattivato</strong></li>
<li><a rel="nofollow" href="http://www.androidiani.com/forum/misc.php?do=linkbacks#trackbacks" target="_blank">Trackbacks</a> are <strong>attivo</strong></li>
<li><a rel="nofollow" href="http://www.androidiani.com/forum/misc.php?do=linkbacks#pingbacks" target="_blank">Pingbacks</a> are <strong>attivo</strong></li>
<li><a rel="nofollow" href="http://www.androidiani.com/forum/misc.php?do=linkbacks#refbacks" target="_blank">Refbacks</a> are <strong>attivo</strong></li>
		</ul>
	</div>
	<p class="rules_link"><a rel="nofollow" href="http://www.androidiani.com/forum/misc.php?do=showrules" target="_blank">Regole del Forum</a></p>

</div>

			</div>
		</div>
	</div>
</div>









<div id="footer" class="floatcontainer footer">

	<form action="http://www.androidiani.com/forum/" method="get" id="footer_select" class="footer_select">

			
		
		
		
			<select name="langid" onchange="switch_id(this, 'lang')">
				<optgroup label="Scelta rapida del linguaggio">
					
	<option value="1" class="" >-- English (US)</option>

	<option value="2" class="" selected="selected">-- Italiano</option>

				</optgroup>
			</select>
		
	</form>

	<ul id="footer_links" class="footer_links">
		<li><a href="http://www.androidiani.com/forum/sendmessage.php" rel="nofollow" accesskey="9">Contatta lo Staff</a></li>
		<li><a href="http://www.androidiani.com">Il forum italiano dedicato ad android</a></li>
		
		
		<li><a href="http://www.androidiani.com/forum/archive/index.php/">Archivio</a></li>
		
		
		
		<li><a href="http://www.androidiani.com/forum/altri-operatori/358796-problema-sms-sim-postemobile.html#top" onclick="document.location.hash='top'; return false;">Vai in cima</a></li>
	</ul>
	
	
	
	
	<script type="text/javascript">
	<!--
		// Main vBulletin Javascript Initialization
		vBulletin_init();
	//-->
	</script>
        
</div>
</div> <!-- closing div for body_wrapper -->

<div class="below_body">
<div id="footer_time" class="shade footer_time"><b>Tutti gli orari sono GMT +1. Attualmente sono le <span class="time">14:25</span>.</b></div>

<div id="footer_copyright" class="shade footer_copyright">
	<!-- Do not remove this copyright notice -->
	Powered by <a rel="nofollow" href="http://www.vbulletin.com" id="vbulletinlink">vBulletin&trade;</a> Version 4.2.2 <br />Copyright &copy; 2014 vBulletin Solutions, Inc. All rights reserved
<br />Search Engine Friendly URLs by vBSEO &copy;2011, Crawlability, Inc.</b>
<br><b>Traduzione italiana Team: <a rel="nofollow" href="http://www.vbulletin-italia.it">vBulletin-italia.it</a></b>

	<!-- Do not remove this copyright notice -->	
</div>
<div id="footer_morecopyright" class="shade footer_morecopyright">
	<!-- Do not remove cronimage or your scheduled tasks will cease to function -->
	
	<!-- Do not remove cronimage or your scheduled tasks will cease to function -->
	
</div>

</div>



<script src="/forum/clientscript/jquery.cookie.js" type="text/javascript"></script>

<script src="/forum/clientscript/jquery.tooltip.min.js" type="text/javascript"></script>
<script src="/extra/javascript/fancybox/jquery.fancybox-1.3.4.pack.js" type="text/javascript"></script>
<script src="/forum/clientscript/forum_customscripts.js" type="text/javascript"></script>

	<script type="text/javascript" src="/extra/javascript/jquery.social_applet.js"></script>
	<script type="text/javascript" src="/extra/javascript/androidiani.social.js"></script>

<!--<script src="http://api.twitter.com/1/statuses/user_timeline.json?screen_name=androidiani&amp;callback=twitterCallback2&amp;count=1&amp;include_rts=1" type="text/javascript"></script>-->

<script type="text/javascript">
//<![CDATA[

var vbseo_jshtml = new Array();
vbseo_jshtml[0] = "<"+"ol>			<"+"li class=\"restore\" id=\"navbar_notice_2\">		Se questa � la tua prima visita su Androidiani.com, controlla le <"+"a href=\"http://www.androidiani.com/forum/faq.php\" target=\"_blank\"><"+"b>FAQ<"+"/b><"+"/a> cliccando sul link. Oppure registrati cliccando <"+"a href=\"http://www.androidiani.com/forum/register.php\" target=\"_blank\"><"+"b>qui<"+"/b><"+"/a>. In questo modo potrai iniziare a scrivere e partecipare attivamente alla community!<"+"/li>		<"+"/ol>";

for(var vi=0;vi<vbseo_jshtml.length;vi++)
if(fetch_object("vbseo_vhtml_"+vi))fetch_object("vbseo_vhtml_"+vi).innerHTML = vbseo_jshtml[vi];

window.orig_onload = window.onload;
window.onload = function() {
var cpost=document.location.hash.substring(1);var cpost2='';if(cpost && (typeof fetch_object != 'undefined')){ var ispost=cpost.substring(0,4)=='post';if(ispost)cpost2='post_'+cpost.substring(4);if((cobj = fetch_object(cpost))||(cobj = fetch_object(cpost2))){cobj.scrollIntoView(true);}else if(ispost){cpostno = cpost.substring(4,cpost.length);if(parseInt(cpostno)>0){location.replace('http://www.androidiani.com/forum/showthread.php?p='+cpostno);};} }

if(typeof window.orig_onload == "function") window.orig_onload();
}

//]]>
</script>
</body>
</html>