<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="robots" content="index,follow" />

		
			<meta property="og:url" content="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/02/20/quieres-darle-a-tus-puertas-una-apariencia-nueva-facilmente-y-en-plan-low-cost-mira-este-truco/"/>
			<meta property="og:title" content="¿Quieres darle a tus puertas una apariencia nueva, fácilmente y en plan Low Cost? Mira este truco" />
			<meta property="og:description" content="Otras entradas que podrían interesarte Lo mejor del mes según todos vosotros… en un solo post ¿Te quedas en Semana Santa? Aprovecha: 16 Ideas para cambiar tus picaportes y un vídeo consejo Video consejo: cómo decapar un mueble (para dejarlo como nuevo o darle un efecto &#8216;vintage&#8217;)" />
			<meta property="og:type" content="article" />
		    <meta property="og:image" content="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/files/2014/02/Reparalia_blog_consejo_truco_como_cambiar_sustituir_manilla_pomo_picaporte_puerta_casa_reparaciones_hogar_emergencias_carpinteros_fontaneros_albañiles_toda_España.jpg" />

		
		<title>¿Quieres darle a tus puertas una apariencia nueva, fácilmente y en plan Low Cost? Mira este truco &#124;  Un hogar con mucho oficio</title>
		<link rel="stylesheet" href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/wp-content/themes/20minutos/style.css?20140629" type="text/css" media="screen" />
		<link rel="stylesheet" type="text/css" media="all" href="http://cdn.20minutos.es/mmedia/feeds/css/feed.css" />

		
		<link rel="pingback" href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/xmlrpc.php" />

		
		<link rel="alternate" type="application/rss+xml" title="Un hogar con mucho oficio &raquo; ¿Quieres darle a tus puertas una apariencia nueva, fácilmente y en plan Low Cost? Mira este truco RSS de los comentarios" href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/02/20/quieres-darle-a-tus-puertas-una-apariencia-nueva-facilmente-y-en-plan-low-cost-mira-este-truco/feed/" />
<link rel='stylesheet' id='contact-form-7-css'  href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=3.8' type='text/css' media='all' />
<link rel='stylesheet' id='better-related-frontend-css'  href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/wp-content/plugins/better-related/css/better-related.css?ver=0.3.5' type='text/css' media='all' />
<script type='text/javascript' src='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/wp-includes/js/jquery/jquery.js?ver=1.11.0'></script>
<script type='text/javascript' src='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/wp-includes/wlwmanifest.xml" /> 
<link rel='prev' title='Pintura y decoración: Viaja desde casa con un mapamundi interactivo DIY en tu pared' href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/02/18/pintura-y-decoracion-viaja-desde-casa-con-un-mapamundi-interactivo-diy-en-tu-pared/' />
<link rel='next' title='¿Tienes preguntas sobre tu caldera? Nuestro profesional te responde' href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/02/24/tienes-preguntas-sobre-tu-caldera-nuestro-profesional-te-responde/' />
<meta name="generator" content="WordPress 3.9.1" />
<link rel='canonical' href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/02/20/quieres-darle-a-tus-puertas-una-apariencia-nueva-facilmente-y-en-plan-low-cost-mira-este-truco/' />
<link rel='shortlink' href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/?p=5731' />
<style type="text/css" media="screen">

		/* Begin Contact Form CSS */

		.contactform { 
			position: relative;
			overflow: hidden;
			}
		
		.contactleft {
			width: 25%; 
			text-align: right;
			clear: both; 
			float: left; 
			display: inline; 
			padding: 4px; 
			margin: 5px 0; 
			}
		
		.contactright {
			width: 70%;
			text-align: left;  
			float: right; 
			display: inline; 
			padding: 4px; 
			margin: 5px 0; 
			}

    .contacterror {
      border: 1px solid #b00640;
    	}
		/* End Contact Form CSS */

	</style>


		<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
		<script type="text/javascript" src="http://apis.google.com/js/plusone.js">{lang: 'es'}</script>
		<script type="text/javascript" src="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/wp-content/themes/20minutos/js/public.min.js?20140404"></script>

		
		<script type="text/javascript">
	OAS_sitepage = '20minutos.es/blog/un-hogar-con-mucho-oficio';
	OAS_listpos = 'x01,Top,x31,x41,Right1,Right2';
	OAS_query = '';
	OAS_target = '_top';
</script>
<script type="text/javascript" src="http://cdn.20minutos.es/js2/realmedia.js"></script>
		<style type="text/css">

			
			
			
#cabecera { background-image : url(http://blogs.20minutos.es/un-hogar-con-mucho-oficio/files/2014/07/un-hogar-con-mucho-oficio.jpg?1406110095);}

						
#cont_cabecera { background-color : #ffffff;}

			
		</style>

	</head>
	<body class="single single-post postid-5731 single-format-standard">
		<div style="display:none;">
			<script type="text/javascript">
var _comscore = _comscore || [];
_comscore.push({ c1: "2", c2: "8189131" });
(function() {
var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
el.parentNode.insertBefore(s, el);
})();
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=8189131&amp;cv=2.0&amp;cj=1" />
</noscript>
		</div>

		<div id="fb-root"></div>
<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/es_LA/all.js#xfbml=1&appId=217041948309757";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

		<div id="page">
			<div id="publi">
				<div class="cont_publi">
					<div id="x01">
	<script type="text/javascript">
<!--
OAS_AD('x01');
//-->
	</script>
</div>					<div id="Top">
	<script type="text/javascript">
<!--
OAS_AD('Top');
//-->
	</script>
</div>				</div>
			</div>

			<div id="header20m-wrapper">
				<link rel="stylesheet" type="text/css" media="all" href="http://cdn.20minutos.es/css4/header-footer-mini.css?20140613" />

<div id="header-20m" class="local-es">
	<div class="h1"><a href="http://www.20minutos.es/" title="Noticias actualidad en 20minutos.es">20minutos.es</a></div>
	<ul id="20m-menu"><li><a onclick="if (typeof(xt_click) == 'function') { return xt_click(this, 'C', xtn2, 'Cabecera_mini_blogs', 'S'); }" href="http://www.20minutos.es/blogs_opinion/" title="Portada 20minutos.es">Blogs</a></li><li><a onclick="if (typeof(xt_click) == 'function') { return xt_click(this, 'C', xtn2, 'Cabecera_mini_nacional', 'S'); }" href="http://www.20minutos.es/nacional/" title="Nacional">Nacional</a></li><li><a onclick="if (typeof(xt_click) == 'function') { return xt_click(this, 'C', xtn2, 'Cabecera_mini_internacional', 'S'); }" href="http://www.20minutos.es/internacional/" title="Internacional">Internacional</a></li><li><a onclick="if (typeof(xt_click) == 'function') { return xt_click(this, 'C', xtn2, 'Cabecera_mini_economia', 'S'); }" href="http://www.20minutos.es/economia/" title="Economía">Economía</a></li><li><a onclick="if (typeof(xt_click) == 'function') { return xt_click(this, 'C', xtn2, 'Cabecera_mini_tu-ciudad', 'S'); }" href="http://www.20minutos.es/tu-ciudad/" title="Tu ciudad">Tu ciudad</a></li><li><a onclick="if (typeof(xt_click) == 'function') { return xt_click(this, 'C', xtn2, 'Cabecera_mini_deportes', 'S'); }" href="http://www.20minutos.es/deportes/" title="Deportes">Deportes</a></li><li><a onclick="if (typeof(xt_click) == 'function') { return xt_click(this, 'C', xtn2, 'Cabecera_mini_tecnologia', 'S'); }" href="http://www.20minutos.es/tecnologia/" title="Tecnología">Tecnología</a></li><li><a onclick="if (typeof(xt_click) == 'function') { return xt_click(this, 'C', xtn2, 'Cabecera_mini_ciencia', 'S'); }" href="http://www.20minutos.es/ciencia/" title="Ciencia">Ciencia</a></li><li><a onclick="if (typeof(xt_click) == 'function') { return xt_click(this, 'C', xtn2, 'Cabecera_mini_artes', 'S'); }" href="http://www.20minutos.es/artes/" title="Artes">Artes</a></li><li><a onclick="if (typeof(xt_click) == 'function') { return xt_click(this, 'C', xtn2, 'Cabecera_mini_gente-y-tv', 'S'); }" href="http://www.20minutos.es/gente-television/" title="Gente y TV">Gente y TV</a></li><li><a onclick="if (typeof(xt_click) == 'function') { return xt_click(this, 'C', xtn2, 'Cabecera_mini_comunidad20', 'S'); }" href="http://www.20minutos.es/comunidad20/" title="Comunidad20">Comunidad20</a></li><li><a onclick="if (typeof(xt_click) == 'function') { return xt_click(this, 'C', xtn2, 'Cabecera_mini_horoscopo', 'S'); }" href="http://www.20minutos.es/horoscopo/" title="Horóscopo">Horóscopo</a></li></ul>
</div>

			</div>

			<div id="header">
				<div id="cont_cabecera">
					<div id="cabecera">
							<h1><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/">Un hogar con mucho oficio</a></h1>
							<p id="desc"></p>
							<div id="busca">
							<form method="get" id="searchform" action="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/">
	<!--<label class="hidden" for="s">Buscar:</label>-->
	<div>
		<input type="text" value="" name="s" id="s" />
		<input type="submit" id="searchsubmit" value="busca" />
	</div>
</form>						</div>
					</div>

					<div id="menu" class="clearfix">
						<ul>
							<li><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/">inicio</a></li>
							<li><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/archivo/">archivo</a></li>
							<li><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/contacto/">contacto</a></li>
							<li><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/feed/">suscríbete</a></li>
													</ul>
					</div>
				</div>
			</div>
<div id="cont_contenido">
	<div id="cont_columnas">

		<div id="x31">
	<script type="text/javascript">
<!--
OAS_AD('x31');
//-->
	</script>
</div><div id="x41">
	<script type="text/javascript">
<!--
OAS_AD('x41');
//-->
	</script>
</div>
		<div id="content" class="widecolumn">

			
			<p class="nav center">
				&laquo; <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/02/18/pintura-y-decoracion-viaja-desde-casa-con-un-mapamundi-interactivo-diy-en-tu-pared/" rel="prev">Pintura y decoración: Viaja desde casa con un mapamundi interactivo DIY en tu pared</a> | <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/">Inicio</a> | <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/02/24/tienes-preguntas-sobre-tu-caldera-nuestro-profesional-te-responde/" rel="next">¿Tienes preguntas sobre tu caldera? Nuestro profesional te responde</a> &raquo;			</p>

			<div class="post-5731 post type-post status-publish format-standard hentry category-carpinteria-2 category-cerrajeria category-decoracion category-diy category-mantenimiento category-puertas category-videoconsejos tag-app tag-decoracion tag-destornillador tag-diy tag-gratis tag-low-cost tag-manillas tag-picaportes tag-pomos tag-puertas tag-renovar tag-reparalia tag-roseta" id="post-5731">
				<h2>¿Quieres darle a tus puertas una apariencia nueva, fácilmente y en plan Low Cost? Mira este truco</h2>
				<small style="float: left;">20 febrero 2014  <!-- por Salvador de la Casa--></small>

				<div class="social_links">
					<div>
	<div class="g-plusone" data-size="medium" data-href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/02/20/quieres-darle-a-tus-puertas-una-apariencia-nueva-facilmente-y-en-plan-low-cost-mira-este-truco/"></div>
</div>
<div>
	<a href="http://twitter.com/share" class="twitter-share-button" data-text="¿Quieres darle a tus puertas una apariencia nueva, fácilmente y en plan Low Cost? Mira este truco" data-url="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/02/20/quieres-darle-a-tus-puertas-una-apariencia-nueva-facilmente-y-en-plan-low-cost-mira-este-truco/" data-count="horizontal" data-via="reparalia" data-related="20m" data-lang="es">Tweet</a>
</div><div>
	<div class="fb-like" data-href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/02/20/quieres-darle-a-tus-puertas-una-apariencia-nueva-facilmente-y-en-plan-low-cost-mira-este-truco/" data-send="false" data-layout="button_count" data-width="125" data-show-faces="false" data-locale="es_ES"></div>
</div>				</div>
				<script type="text/javascript">
jQuery(document).ready(function()
{
	if (jQuery.browser.msie)
	{
		jQuery('iframe').attr('allowTransparency', 'true');
	}
});
				</script>
				<div style="clear:both;"></div>

				<p></p>

				<div class="entry">
					<p>Haz este ejercicio mental: recuerda cuántas veces has estado en un <b>restaurante</b>, un <b>hotel</b> o la <b>casa de unos amigos</b> y has pensado <i>“qué detalles tan chulos”</i>, <i>“qué casa tan moderna”, “qué buen gusto”</i>. Y lo has hecho <b>al abrir la puerta,</b> de una habitación, de un baño.</p>
<p><b>Exacto. Los picaportes visten mucho una casa. </b>Son esos complementos de moda que marcan un estilo. <b>Pequeños cambios en tu hogar</b> que consiguen darle un aire nuevo al instante, <b>por muy poco dinero y esfuerzo. </b></p>
<p>Y precisamente porque es una de esas cosas que no te plantearías, imaginándotela muy complicada, <b>te vamos a demostrar lo fácil y barato que es sustituir los pomos de tus puertas. </b>Un accesorio que ronda, de media, entre los 8€ y los 25€ en cualquier ferretería o tienda de bricolaje, y que hará renacer a tus ojos esa puerta que tienes tan vista.</p>
<p>&nbsp;</p>
<p><iframe src="//www.youtube.com/embed/5TSO3JR8dzY" height="360" width="640" frameborder="0"></iframe></p>
<p>&nbsp;</p>
<p>En el <b>vídeo consejo</b> de aquí arriba te lo contamos <b>paso a paso</b>, y te sorprenderás de ver que no necesitas más ayuda que <b>un destornillador</b>, algo de <b>masilla para madera</b>, una <b>lija</b> suave y algo de <b>barniz</b>. Ejem… ¡y las nuevas <b>manillas</b>!</p>
<p>Fíjate en el ejemplo práctico: hemos ayudado a nuestra amiga Marta a <b>sustituir un picaporte tradicional</b>, de los de <i>toda la vida</i>, por uno cuadrado y <i>minimal</i>, <b>mucho más moderno y estiloso</b>.</p>
<p>¿Qué sucede? Que los años de convivencia entre la puerta y el antiguo pasaporte dejan huella, querido amigo: necesita una <b>reparación</b> de la zona que antes estaba cubierta por el <b>embellecedor</b> y ahora queda al desnudo.</p>
<p><a title="Los cerrajeros profesionales de Reparalia te enseñan a cambiar una manilla por otra en este video consejo. Renueva tus puertas, y con ellas el estilo de tu casa, en muy poco tiempo y de forma económica sustituyendo los picaportes con nuestra ayuda y, si necesitas un técnico especialista, llámanos a cualquier hora y en toda España para que te enviemos uno de nuestros expertos." href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/files/2014/02/Reparalia_blog_consejo_truco_como_cambiar_sustituir_manilla_pomo_picaporte_puerta_casa_reparaciones_hogar_emergencias_carpinteros_fontaneros_albañiles_toda_España.jpg" target="_blank"><img class="size-full wp-image-5732 alignnone" alt="Los cerrajeros profesionales de Reparalia te enseñan a cambiar una manilla por otra en este video consejo. Renueva tus puertas, y con ellas el estilo de tu casa, en muy poco tiempo y de forma económica sustituyendo los picaportes con nuestra ayuda y, si necesitas un técnico especialista, llámanos a cualquier hora y en toda España para que te enviemos uno de nuestros expertos." src="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/files/2014/02/Reparalia_blog_consejo_truco_como_cambiar_sustituir_manilla_pomo_picaporte_puerta_casa_reparaciones_hogar_emergencias_carpinteros_fontaneros_albañiles_toda_España.jpg" width="600" height="341" /></a></p>
<p>Pero lo primero es lo primero: <b>retirar la vieja roseta.</b> Y para ello hay dos posibilidades, que el embellecedor tenga <b>tornillos a la vista, o esté encastrado</b>. En el primer caso solo necesitas desatornillarlos directamente; en el segundo, tendrás que usar un destornillador de punta plana, apoyarlo sobre una cuña de madera para no marcar la puerta, y hacer palanca en la pequeña rendija que encontrarás para tal efecto.</p>
<p><a title="Los cerrajeros profesionales de Reparalia te enseñan a cambiar una manilla por otra en este video consejo. Renueva tus puertas, y con ellas el estilo de tu casa, en muy poco tiempo y de forma económica sustituyendo los picaportes con nuestra ayuda y, si necesitas un técnico especialista, llámanos a cualquier hora y en toda España para que te enviemos uno de nuestros expertos." href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/files/2014/02/Reparalia_blog_consejo_truco_como_cambiar_sustituir_manilla_pomo_picaporte_puerta_casa_reparaciones_hogar_emergencias_carpinteros_fontaneros_albañiles_reparacion.jpg" target="_blank"><img class="size-full wp-image-5733 alignnone" alt="Los cerrajeros profesionales de Reparalia te enseñan a cambiar una manilla por otra en este video consejo. Renueva tus puertas, y con ellas el estilo de tu casa, en muy poco tiempo y de forma económica sustituyendo los picaportes con nuestra ayuda y, si necesitas un técnico especialista, llámanos a cualquier hora y en toda España para que te enviemos uno de nuestros expertos." src="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/files/2014/02/Reparalia_blog_consejo_truco_como_cambiar_sustituir_manilla_pomo_picaporte_puerta_casa_reparaciones_hogar_emergencias_carpinteros_fontaneros_albañiles_reparacion.jpg" width="600" height="341" /></a></p>
<p>Cuando el artilugio esté retirado, tendrás que <b>rellenar los agujeros con masilla para madera.</b> La encontrarás en tu tienda habitual de bricolaje, en varios colores para hacer juego con tu puerta. Después, <b>lija suavemente la zona</b> y, según el grado de desgastamiento, <b>pinta o barniza</b> solo la zona afectada o hazlo con toda la superficie de la puerta (si la diferencia es tan grande que llega a percibirse el “parche”) .</p>
<p><a title="Los cerrajeros profesionales de Reparalia te enseñan a cambiar una manilla por otra en este video consejo. Renueva tus puertas, y con ellas el estilo de tu casa, en muy poco tiempo y de forma económica sustituyendo los picaportes con nuestra ayuda y, si necesitas un técnico especialista, llámanos a cualquier hora y en toda España para que te enviemos uno de nuestros expertos." href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/files/2014/02/Reparalia_blog_consejo_truco_como_cambiar_sustituir_manilla_pomo_picaporte_puerta_casa_reparaciones_cerrajeros.jpg" target="_blank"><img class="size-medium wp-image-5734 alignleft" alt="Los cerrajeros profesionales de Reparalia te enseñan a cambiar una manilla por otra en este video consejo. Renueva tus puertas, y con ellas el estilo de tu casa, en muy poco tiempo y de forma económica sustituyendo los picaportes con nuestra ayuda y, si necesitas un técnico especialista, llámanos a cualquier hora y en toda España para que te enviemos uno de nuestros expertos." src="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/files/2014/02/Reparalia_blog_consejo_truco_como_cambiar_sustituir_manilla_pomo_picaporte_puerta_casa_reparaciones_cerrajeros-300x170.jpg" width="300" height="170" /></a></p>
<p><a style="line-height: 1.5em" href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/files/2014/02/Reparalia_blog_consejo_truco_como_cambiar_sustituir_manilla_pomo_picaporte_puerta_casa_reparacion_hogar_.jpg"><img class="size-medium wp-image-5735 alignleft" alt="Los cerrajeros profesionales de Reparalia te enseñan a cambiar una manilla por otra en este video consejo. Renueva tus puertas, y con ellas el estilo de tu casa, en muy poco tiempo y de forma económica sustituyendo los picaportes con nuestra ayuda y, si necesitas un técnico especialista, llámanos a cualquier hora y en toda España para que te enviemos uno de nuestros expertos." src="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/files/2014/02/Reparalia_blog_consejo_truco_como_cambiar_sustituir_manilla_pomo_picaporte_puerta_casa_reparacion_hogar_-300x170.jpg" width="300" height="170" /> </a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Una vez como nueva la puerta, <b>preséntale a su nuevo picaporte y atorníllalo</b> con los tirafondos que acompañan al nuevo pomo. Las puertas de hoy en día suelen ser perforables por medio de <b>un destornillador y un poco de fuerza</b>. Pero si su madera es <b>maciza</b>, necesitarás practicar primero un agujero con <b>taladro</b> <b>y una broca más fina que el tirafondos</b>, para ayudarte.</p>
<p><a title="Los cerrajeros profesionales de Reparalia te enseñan a cambiar una manilla por otra en este video consejo. Renueva tus puertas, y con ellas el estilo de tu casa, en muy poco tiempo y de forma económica sustituyendo los picaportes con nuestra ayuda y, si necesitas un técnico especialista, llámanos a cualquier hora y en toda España para que te enviemos uno de nuestros expertos." href="https://itunes.apple.com/es/app/reparalia/id699655567?mt=8" target="_blank"><img class="size-medium wp-image-5738 alignleft" alt="Los cerrajeros profesionales de Reparalia te enseñan a cambiar una manilla por otra en este video consejo. Renueva tus puertas, y con ellas el estilo de tu casa, en muy poco tiempo y de forma económica sustituyendo los picaportes con nuestra ayuda y, si necesitas un técnico especialista, llámanos a cualquier hora y en toda España para que te enviemos uno de nuestros expertos." src="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/files/2014/02/Reparalia_blog_consejo_truco_como_cambiar_sustituir_manilla_pomo_picaporte_puerta_casa_reparaciones_hogar_emergencias_carpinteros_electricistas-300x170.jpg" width="300" height="170" /></a><a title="Los cerrajeros profesionales de Reparalia te enseñan a cambiar una manilla por otra en este video consejo. Renueva tus puertas, y con ellas el estilo de tu casa, en muy poco tiempo y de forma económica sustituyendo los picaportes con nuestra ayuda y, si necesitas un técnico especialista, llámanos a cualquier hora y en toda España para que te enviemos uno de nuestros expertos." href="https://play.google.com/store/apps/details?id=com.reparalia.reparalia" target="_blank"><img class="size-medium wp-image-5737 alignleft" alt="Los cerrajeros profesionales de Reparalia te enseñan a cambiar una manilla por otra en este video consejo. Renueva tus puertas, y con ellas el estilo de tu casa, en muy poco tiempo y de forma económica sustituyendo los picaportes con nuestra ayuda y, si necesitas un técnico especialista, llámanos a cualquier hora y en toda España para que te enviemos uno de nuestros expertos." src="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/files/2014/02/Reparalia_blog_consejo_truco_como_cambiar_sustituir_manilla_pomo_picaporte_puerta_casa_reparaciones_low_cost-300x170.jpg" width="300" height="170" /></a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Para terminar, encaja el nuevo embellecedor con un poco de presión y…</p>
<h2>¡TACHANNN! ¡Parece una puerta nueva!</h2>
<p><a title="Los cerrajeros profesionales de Reparalia te enseñan a cambiar una manilla por otra en este video consejo. Renueva tus puertas, y con ellas el estilo de tu casa, en muy poco tiempo y de forma económica sustituyendo los picaportes con nuestra ayuda y, si necesitas un técnico especialista, llámanos a cualquier hora y en toda España para que te enviemos uno de nuestros expertos." href="https://itunes.apple.com/es/app/reparalia/id699655567?mt=8" target="_blank"><img class="aligncenter size-full wp-image-5740" alt="Los cerrajeros profesionales de Reparalia te enseñan a cambiar una manilla por otra en este video consejo. Renueva tus puertas, y con ellas el estilo de tu casa, en muy poco tiempo y de forma económica sustituyendo los picaportes con nuestra ayuda y, si necesitas un técnico especialista, llámanos a cualquier hora y en toda España para que te enviemos uno de nuestros expertos." src="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/files/2014/02/Reparalia_blog_consejo_truco_como_cambiar_sustituir_manilla_pomo_picaporte_puerta_casa_reparaciones_hogar_emergencias_carpinteros_fontaneros_albañiles_profesionales.jpg" width="600" height="341" /></a></p>
<p>¿Qué te parece? ¿Abrimos la puerta a la imaginación y cambiamos los picaportes en casa?</p>
<p>&nbsp;</p>
<p>Publicidad: ¿Tus cerraduras te dan problemas más serios y necesitas ayuda profesional? La tienes a un clic de distancia en tu móvil: descarga e instala<b> GRATIS en tu Smartphone la <a title="Los cerrajeros profesionales de Reparalia te enseñan a cambiar una manilla por otra en este video consejo. Renueva tus puertas, y con ellas el estilo de tu casa, en muy poco tiempo y de forma económica sustituyendo los picaportes con nuestra ayuda y, si necesitas un técnico especialista, llámanos a cualquier hora y en toda España para que te enviemos uno de nuestros expertos." href="https://itunes.apple.com/es/app/reparalia/id699655567?mt=8" target="_blank">App de Reparalia para iPhone</a> en iTunes y <a title="Los profesionales del hogar de Reparalia, que te ofrecen toda la ayuda que necesites en casa frente a accidentes, averías, reparaciones, reformas… ponen a tu disposición una red con miles de técnicos especialistas en toda España: electricistas, fontaneros" href="https://play.google.com/store/apps/details?id=com.reparalia.reparalia" target="_blank">para Android</a> en Google Play, y estarás protegido</b> contra imprevistos, accidentes en el hogar y todo tipo de problemas domésticos, ya sean de cerrajería, fontanería, electricidad o cualquier otro gremio.</p>
<p><a title="Los cerrajeros profesionales de Reparalia te enseñan a cambiar una manilla por otra en este video consejo. Renueva tus puertas, y con ellas el estilo de tu casa, en muy poco tiempo y de forma económica sustituyendo los picaportes con nuestra ayuda y, si necesitas un técnico especialista, llámanos a cualquier hora y en toda España para que te enviemos uno de nuestros expertos." href="https://itunes.apple.com/es/app/reparalia/id699655567?mt=8" target="_blank"><img class="aligncenter size-full wp-image-5741" alt="Los cerrajeros profesionales de Reparalia te enseñan a cambiar una manilla por otra en este video consejo. Renueva tus puertas, y con ellas el estilo de tu casa, en muy poco tiempo y de forma económica sustituyendo los picaportes con nuestra ayuda y, si necesitas un técnico especialista, llámanos a cualquier hora y en toda España para que te enviemos uno de nuestros expertos." src="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/files/2014/02/App_Reparalia_gratis_iphone_ios_Android_Google_Play_iTunes_gratuita_reparaciones_herramientas.jpg" width="600" height="313" /></a></p>
<p><span style="color: #ff0000">¡Pulsa su botón y nuestros profesionales te llamarán en el momento para solucionar tu problema enseguida!</span></p>
<div class="betterrelated"><p><h3>Otras entradas que podrían interesarte</h3></p>
<ol><li> <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2013/11/29/lo-mejor-del-mes-segun-todos-vosotros-en-un-solo-post/" title="Permanent link to Lo mejor del mes según todos vosotros… en un solo post">Lo mejor del mes según todos vosotros… en un solo post</a>  </li>
<li> <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/04/15/te-quedas-en-semana-santa-aprovecha-16-ideas-para-cambiar-tus-picaportes-y-un-video-consejo/" title="Permanent link to ¿Te quedas en Semana Santa? Aprovecha: 16 Ideas para cambiar tus picaportes y un vídeo consejo">¿Te quedas en Semana Santa? Aprovecha: 16 Ideas para cambiar tus picaportes y un vídeo consejo</a>  </li>
<li> <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2013/11/27/video-consejo-como-decapar-un-mueble-para-dejarlo-como-nuevo-o-darle-un-efecto-vintage/" title="Permanent link to Video consejo: cómo decapar un mueble (para dejarlo como nuevo o darle un efecto &#8216;vintage&#8217;)">Video consejo: cómo decapar un mueble (para dejarlo como nuevo o darle un efecto &#8216;vintage&#8217;)</a>  </li>
</ol></div>										<p class="postmetadata">Tags: <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/app/" rel="tag">App</a>, <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/decoracion/" rel="tag">Decoración</a>, <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/destornillador/" rel="tag">destornillador</a>, <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/diy/" rel="tag">DIY</a>, <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/gratis/" rel="tag">Gratis</a>, <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/low-cost/" rel="tag">low-cost</a>, <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/manillas/" rel="tag">manillas</a>, <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/picaportes/" rel="tag">picaportes</a>, <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/pomos/" rel="tag">pomos</a>, <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/puertas/" rel="tag">puertas</a>, <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/renovar/" rel="tag">renovar</a>, <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/reparalia/" rel="tag">Reparalia</a>, <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/roseta/" rel="tag">roseta</a> |  Almacenado en: <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/carpinteria-2/" title="Ver todas las entradas en Carpintería" rel="category tag">Carpintería</a>, <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/cerrajeria/" title="Ver todas las entradas en Cerrajería" rel="category tag">Cerrajería</a>, <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/decoracion/" title="Ver todas las entradas en Decoración" rel="category tag">Decoración</a>, <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/diy/" title="Ver todas las entradas en DIY" rel="category tag">DIY</a>, <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/mantenimiento/" title="Ver todas las entradas en mantenimiento" rel="category tag">mantenimiento</a>, <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/puertas/" title="Ver todas las entradas en puertas" rel="category tag">puertas</a>, <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/videoconsejos/" title="Ver todas las entradas en videoconsejos" rel="category tag">videoconsejos</a></p>
				</div>
				<!-- ADSENSE -->
				<div id="adsense">
					<script type="text/javascript" src="http://a.ligatus.com/?ids=26107&amp;t=js"></script>
				</div>
				<!-- /ADSENSE -->
			</div>

							<div class="fb-recommendations-bar" data-read-time="10" data-max-age="7" data-href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/02/20/quieres-darle-a-tus-puertas-una-apariencia-nueva-facilmente-y-en-plan-low-cost-mira-este-truco/"></div>
			
			
<!-- You can start editing here. -->

	<div id="comentarios_numero">
		<h2 id="comments">5 comentarios · <a href="#commentform">Escribe aquí tu comentario</a></h2>
	</div>

	<div class="navigation-comments right clearfix">
		  	</div>

	<ol class="commentlist">
		<li class="comment even thread-even depth-1" id="li-comment-1370">
	<div id="comment-1370">
		<div class="comment-author vcard">
						<cite class="fn">Dice ser jjl</cite> <span class="says"></span>		</div>
		
		<p>Mira que sois cutres, que habeis hecho con los agujeros?</p>

		<p class="meta"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/02/20/quieres-darle-a-tus-puertas-una-apariencia-nueva-facilmente-y-en-plan-low-cost-mira-este-truco/comment-page-1/#comment-1370">20 febrero 2014 | 13:19</a></p>

	</div>
</li><!-- #comment-## -->
<li class="comment odd alt thread-odd thread-alt depth-1" id="li-comment-1371">
	<div id="comment-1371">
		<div class="comment-author vcard">
						<cite class="fn">Dice ser javier</cite> <span class="says"></span>		</div>
		
		<p>Muy buen truco. Para otro artículo ¿podrías tratar cómo cambiar el color de la puerta? Es decir, esa misma puerta dejarla blanca decapada por ejemplo, con ese picaporte nuevo. Eso sí que sería una puerta nueva-nueva.<br />
Gracias.</p>

		<p class="meta"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/02/20/quieres-darle-a-tus-puertas-una-apariencia-nueva-facilmente-y-en-plan-low-cost-mira-este-truco/comment-page-1/#comment-1371">20 febrero 2014 | 13:46</a></p>

	</div>
</li><!-- #comment-## -->
<li class="comment even thread-even depth-1" id="li-comment-1372">
	<div id="comment-1372">
		<div class="comment-author vcard">
						<cite class="fn">Dice ser Cenutrios</cite> <span class="says"></span>		</div>
		
		<p>Vaya spam más cutre&#8230;artículo ridículo, y solo para publicitar un app que no se descarga ni el tato&#8230;madre mía, eso si, al menos el photoshop par tapar los agujeros del embellecedor viejo esta bien hecho</p>

		<p class="meta"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/02/20/quieres-darle-a-tus-puertas-una-apariencia-nueva-facilmente-y-en-plan-low-cost-mira-este-truco/comment-page-1/#comment-1372">20 febrero 2014 | 14:26</a></p>

	</div>
</li><!-- #comment-## -->
<li class="comment odd alt thread-odd thread-alt depth-1" id="li-comment-1373">
	<div id="comment-1373">
		<div class="comment-author vcard">
						<cite class="fn">Dice ser <a href='http://blogs.20minutos.es/' rel='external nofollow' class='url'>Salvador de la Casa</a></cite> <span class="says"></span>		</div>
		
		<p>¡Hola amigos! el día que os ponéis tiquismiquis, mira que sois, ¿eh? ;P</p>
<p>No hay trampa ni cartón, sino mucho mimo en el trabajo: id al segundo 55 del vídeo -en el texto también os lo contamos- y veréis cómo os explicamos la forma de hacer desaparecer sus rastros: una vez retirado el anterior picaporte, debéis rellenar los agujeros con masilla para madera -del mismo color de la puerta, para disimularlos al máximo.</p>
<p>Si lo claváis con el color, como ha hecho nuestro compañero de Reparalia en el vídeo, os aseguro que se notarán muy, muy poquito las marcas del lugar donde estaban estos orificios. Pero es que además, después os recomendamos lijar y repintar o barnizar la zona para dejarla perfecta, que es lo que hemos hecho nosotros.</p>
<p>Estos son el Photoshop y el AfterEffects de los manitas: maña, tiempo y buenos materiales <img src="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley" /> </p>
<p>Un abrazo y gracias por ser siempre muy exigentes con todos los proyectos que os planteéis: es la mejor forma de aprender y conseguir resultados tan profesionales como estos.</p>

		<p class="meta"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/02/20/quieres-darle-a-tus-puertas-una-apariencia-nueva-facilmente-y-en-plan-low-cost-mira-este-truco/comment-page-1/#comment-1373">20 febrero 2014 | 18:55</a></p>

	</div>
</li><!-- #comment-## -->
<li class="comment even thread-even depth-1" id="li-comment-1377">
	<div id="comment-1377">
		<div class="comment-author vcard">
						<cite class="fn">Dice ser Jimena</cite> <span class="says"></span>		</div>
		
		<p>me parece muy buena idea para renovar la imagen de una puerta con poco tiempo y dinero, no se me había ocurrido, gracias.</p>

		<p class="meta"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/02/20/quieres-darle-a-tus-puertas-una-apariencia-nueva-facilmente-y-en-plan-low-cost-mira-este-truco/comment-page-1/#comment-1377">24 febrero 2014 | 12:19</a></p>

	</div>
</li><!-- #comment-## -->
	</ol>

	<div class="navigation-comments right clearfix">
		  	</div>


	<!-- If comments are open, but there are no comments. -->



	<div id="respond">

		<h3>Escribe aquí tu comentario</h3>

		<div class="cancel-comment-reply">
			<small><a rel="nofollow" id="cancel-comment-reply-link" href="/un-hogar-con-mucho-oficio/2014/02/20/quieres-darle-a-tus-puertas-una-apariencia-nueva-facilmente-y-en-plan-low-cost-mira-este-truco/#respond" style="display:none;">Clic para cancelar respuesta.</a></small>
		</div>

		<ol class="commentlist"></ol>

		
			<div id="nuevo_comentario">

				<form action="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/wp-comments-post.php" method="post" id="commentform">

					
						<p>
							<label for="comment_name">Nombre</label> <br>
							<input type="text" name="author" id="author" value="" size="32" tabindex="1" aria-required='true' />
						</p>
						<p>
							<label for="comment_email">Correo-e (no aparecerá publicado)</label> <br>
							<input type="text" name="email" id="email" value="" size="32" tabindex="2" aria-required='true' />
						</p>
						<p>
							<label for="comment_uri">Tu página web</label><br>
							<input type="text" name="url" id="url" value="" size="32" tabindex="3" />
						</p>

					
					<!--<p><small><strong>XHTML:</strong> Puedes usar estas etiquetas: <code>&lt;a href=&quot;&quot; title=&quot;&quot;&gt; &lt;abbr title=&quot;&quot;&gt; &lt;acronym title=&quot;&quot;&gt; &lt;b&gt; &lt;blockquote cite=&quot;&quot;&gt; &lt;cite&gt; &lt;code&gt; &lt;del datetime=&quot;&quot;&gt; &lt;em&gt; &lt;i&gt; &lt;q cite=&quot;&quot;&gt; &lt;strike&gt; &lt;strong&gt; &lt;div align=&quot;&quot; class=&quot;&quot; dir=&quot;&quot; id=&quot;&quot; lang=&quot;&quot; style=&quot;&quot; xml:lang=&quot;&quot;&gt; &lt;embed style=&quot;&quot; type=&quot;&quot; id=&quot;&quot; height=&quot;&quot; width=&quot;&quot; src=&quot;&quot; object=&quot;&quot;&gt; &lt;iframe width=&quot;&quot; height=&quot;&quot; frameborder=&quot;&quot; scrolling=&quot;&quot; marginheight=&quot;&quot; marginwidth=&quot;&quot; src=&quot;&quot;&gt; &lt;object style=&quot;&quot; height=&quot;&quot; width=&quot;&quot; type=&quot;&quot; data=&quot;&quot; param=&quot;&quot; embed=&quot;&quot;&gt; &lt;param name=&quot;&quot; value=&quot;&quot;&gt; </code></small></p>-->

					<p>
						<label for="comment_message">Tu comentario</label><br>
						<textarea name="comment" id="comment_message" cols="100%" rows="12" tabindex="4"></textarea>
					</p>

					<!-- RECAPTCHA -->
					<div class="captcha">
						<label for="codigo_de_seguridad">Código de Seguridad (código de verificación para prevenir envios automáticos)</label><br />
						<script type="text/javascript">
var RecaptchaOptions =
{
	theme : 'clean',
	lang : 'es',
	tabindex : 5
};
						</script>
						<script type="text/javascript" src="http://www.google.com/recaptcha/api/challenge?k=6Ldb7cQSAAAAAKTgyn3GLkJIBos5WOXYln0hxowP">
						</script>

						<noscript>
							<iframe src="http://www.google.com/recaptcha/api/noscript?k=6Ldb7cQSAAAAAKTgyn3GLkJIBos5WOXYln0hxowP" height="300" width="500" frameborder="0"></iframe><br>
							<textarea name="recaptcha_challenge_field" rows="3" cols="40" tabindex="5"></textarea>
							<input type="hidden" name="recaptcha_response_field" value="manual_challenge">
						</noscript>
					</div>
					<!-- RECAPTCHA -->

					<!-- CONDICIONES DE USO -->
					<div class="condiciones">
					<h4><a href="http://www.20minutos.es/normas_comentar_20minutos/">Normas para comentar en 20minutos.es</a></h4>
						<ul>
							<li>Esta es la opinión de los internautas, no la de 20minutos.es.</li>
							<li>No está permitido verter comentarios contrarios a las leyes españolas o injuriantes.</li>
							<li>Nos reservamos el derecho a eliminar los comentarios que consideremos fuera de tema.</li>
							<li>Por favor, céntrate en el tema.</li>
							<li><a href="http://www.20minutos.es/condiciones_uso_comentarios/">Normas y protección de datos</a>.</li>
						</ul>
					</div>
					<!-- /CONDICIONES DE USO -->

					<p><input name="submit" type="submit" id="submit" tabindex="10" value="Enviar" />
						<input type='hidden' name='comment_post_ID' value='5731' id='comment_post_ID' />
<input type='hidden' name='comment_parent' id='comment_parent' value='0' />
					</p>
					
				</form>
			</div>

				</div>

			
		</div>

		<div id="sidebar">

	<!-- Place this tag after the last widget tag. -->
	<script type="text/javascript">
		(function() {
			var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
			po.src = 'https://apis.google.com/js/platform.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
		})();
	</script>

		
	
		<div class="widget">
			<h2 class="widgettitle">Sobre mí</h2>
			<div class="textwidget">
				<div class="cont_info clearfix">
					<img src="http://cdn.20minutos.es//i/blogs_peque_un-hogar-con-mucho-oficio.jpg" alt="un-hogar-con-mucho-oficio" width="70" height="70" />
					<div class="txt" style="margin-bottom: 0.5em;">
						Soy Salvador de la Casa –¡lo sé, el nombre me viene al pelo!–. Estoy aquí para compartir nuestra pasión (y nuestros secretos) para que estés en casa como en ninguna parte.Aquí encontrarás bricolaje, decoración, ocio, cocina, sostenibilidad, ecología… y muchos trucos para ahorrar en todos ellos. Además, comprobarás que hay pequeñas reparaciones que puedes hacer tú mismo, y que las averías del hogar a veces se solucionan con un poco de maña y buenos consejos.  Pregúntame todas tus dudas en cualquier momento, comparte tus truquillos caseros y saca el manitas que llevas dentro.					</div>
					
						<div style="margin-bottom: 0.3em; display: inline;"><a href="http://twitter.com/reparalia" class="twitter-follow-button" data-show-count="false">Seguir a @reparalia</a></div>

					
					<div style="margin-bottom: 0.3em;" class="fb-like" data-href="https://www.facebook.com/pages/Reparalia/171581496222049?fref=ts" data-width="300" data-layout="button_count" data-show-faces="false" data-send="false"></div>

					
						<div class="g-follow" data-annotation="bubble" data-height="20" data-href="https://plus.google.com/u/0/b/116669579870528915620/+ReparaliaEs_reparaciones_hogar/posts" data-rel="author"></div>

					
					
						<div class="g-ytsubscribe" data-channel="ReparaliaSAU"></div>

					
					
				</div>
			</div>
		</div>

	
	<div class="publi">
			<div class="cont_publi">
				<div id="Right1">
	<script type="text/javascript">
<!--
OAS_AD('Right1');
//-->
	</script>
</div>		</div>
	</div>

	<div class="widget_reparalia">
	<h3>¿Quieres pintar tu casa? ¿Tienes alguna avería que reparar?</h3>
	<p>¿Necesitas un Manitas? Solicítanos un presupuesto.</p>
	<div class="textwidget">
		<div class="loading"></div>
		<div class="widget_content widget_rep">
			<div class="logos"></div>
			<p>Rellena estos datos y nos pondremos en contacto contigo para darte una solución.</p>
			<form action="/wp-20m-reparalia-form.php" method="post" enctype="multipart/form-data" name="reparalia" id="form_reparalia" style="overflow: hidden;">
				<div>
					<label for="rep_tipo_reparacion">Tipo de reparación</label>
					<input type="text" name="rep_tipo_reparacion" id="rep_tipo_reparacion" />
					<div class="rep_val_error">El campo Tipo de reparación es obligatorio</div>
				</div>
				<div>
					<label for="rep_provincia">Provincia</label>
					<input type="text" name="rep_provincia" id="rep_provincia" />
					<div class="rep_val_error">El campo Provincia es obligatorio</div>

				</div>
				<div>
					<label for="rep_telefono">Telefono</label>
					<input type="text" name="rep_telefono" id="rep_telefono" />
					<div class="rep_val_error">El campo Teléfono es obligatorio</div>
				</div>
				<div>
					<label for="rep_email">Email</label>
					<input type="text" name="rep_email" id="rep_email" />
					<div class="rep_val_error">El campo Email es obligatorio</div>
				</div>
				<div class="legal">
					<input type="checkbox" name="rep_terminos" class="legal-check" id="rep_terminos" />
					<small><strong>S&iacute;</strong>, he le&iacute;do y acepto el <a href="http://www.reparalia.com/es/inicio/aviso-legal" target="_blank">aviso legal</a> y la <a href="http://www.reparalia.com/es/inicio/politica-de-privacidad" target="_blank">pol&iacute;tica de privacidad.</a></small>
					<div class="rep_val_error">Debes aceptar el aviso legal y la política de privacidad</div>
				</div>
				<div>
					<input value="¡Enviar!" type="submit" name="image" class="boton" />
				</div>
			</form>
			<div id="rep_ok">Los datos se han enviado correctamente. Muchas gracias.</div>
			<div id="rep_error">Se ha producido un error al procesar los datos. Por favor, inténtalo de nuevo más tarde.</div>
		</div>
		<h3>Sin compromiso. Sin sorpresas. <strong>Y con la garantía Reparalia</strong></h3>
	</div>
</div>
<script type="text/javascript">
jQuery(document).ready(function()
{
	jQuery('#form_reparalia').bind('submit', function(e)
	{
		var dataString = '';

		e.preventDefault();
		jQuery('.rep_val_error').hide();
		var jqForm = jQuery(this);
		if (jqForm.find('#rep_terminos:checked').length === 0)
		{
			jqForm.find('.legal .rep_val_error').show();
			jqForm.find('#rep_terminos').focus();
			return false;
		}
		jqForm.find('input[type=text]').each(function()
		{
			if (jQuery(this).val() == '')
			{
				jQuery(this).siblings('.rep_val_error').show();
				jQuery(this).focus();
				return false;
			}
			else
			{
				if (dataString !== '')
				{
					dataString += '&';
				}
				dataString += jQuery(this).attr('name') + '=' + jQuery(this).val();
			}
		});
		if (jQuery('.rep_val_error:visible').length == 0)
		{
			var formHeight = jQuery('.widget_rep').height();
			var formWidth = jQuery('.widget_rep').width();
			jQuery('.widget_reparalia .loading')
				.height(formHeight)
				.width(formWidth)
				.show();
			jQuery('.rep_val_error, #rep_ok, #rep_error').hide();

			jQuery.ajax({
				type: "POST",
				url: jqForm.attr('action'),
				data: dataString,
				success: function(data) {
					jqForm.css('visibility', 'hidden');
					jQuery('.widget_reparalia .loading').hide();
					if (data == 'ok')
					{
						jQuery('#rep_ok').fadeIn();
					}
					else
					{
						jQuery('#rep_error').fadeIn();
					}
				},
				error: function() {
					jqForm.css('visibility', 'hidden');
					jQuery('.widget_reparalia .loading').hide();
					jQuery('#rep_error').fadeIn();
				}
			});
		}
		return false;
	});
});
</script>
	<div class="widget">			<div class="textwidget"><a class="twitter-timeline"  href="https://twitter.com/Reparalia"  data-widget-id="316244888603738114">Tweets por @Reparalia</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</div>
		</div><div class="widget"><h2 class="widgettitle">Videoconsejos</h2>			<div class="textwidget"><div class="widget_content"> <a href="http://www.20minutos.tv/video/ehqvyHM7-como-pintar-una-pared-trucos/0/">- Cómo pintar una pared, trucos.</a><br />
<a href="http://www.20minutos.tv/video/frtHKS68-como-aislar-tu-casa-del-frio-y-el-calor/0/">- Cómo aislar tu casa instalando burletes.</a><br />
<a href="http://www.20minutos.tv/video/fhlnGJM0-como-colgar-una-lampara/0/">- Cómo colgar una lámpara.</a></div>
</div>
		</div>
	<div class="widget widget_recent_comments"><h2 class="widgettitle">Comentarios recientes</h2><ul id="recentcomments"><li class="recentcomments">veranito en <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/07/25/deja-de-sufrir-la-noche-veraniega-10-trucos-para-dormir-fresco-sin-aire-acondicionado/comment-page-1/#comment-1593">8 maneras de adecuar tu casa para dormir mejor este verano</a></li><li class="recentcomments">veranito en <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/07/30/6-formas-de-ayudar-a-tu-casa-a-ahorrar-y-a-respetar-el-medio-ambiente-mientras-estas-de-vacaciones/comment-page-1/#comment-1592">6 formas de ayudar a tu casa a ahorrar (y a respetar el Medio Ambiente) mientras estás de vacaciones</a></li><li class="recentcomments">marian en <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/07/25/deja-de-sufrir-la-noche-veraniega-10-trucos-para-dormir-fresco-sin-aire-acondicionado/comment-page-1/#comment-1591">8 maneras de adecuar tu casa para dormir mejor este verano</a></li><li class="recentcomments">madam Rose en <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2013/05/09/abrete-sesamo-soluciones-a-los-6-problemas-de-cerraduras-mas-habituales/comment-page-1/#comment-1590">Ábrete Sésamo: soluciones a los 6 problemas de cerraduras más habituales</a></li><li class="recentcomments">alg oritmo en <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/03/13/12-cabeceros-de-cama-diy-low-cost-que-te-sorprenderan/comment-page-1/#comment-1589">12 cabeceros de cama DIY &amp; Low Cost que te sorprenderán</a></li><li class="recentcomments">Exploraocio en <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/07/25/deja-de-sufrir-la-noche-veraniega-10-trucos-para-dormir-fresco-sin-aire-acondicionado/comment-page-1/#comment-1588">8 maneras de adecuar tu casa para dormir mejor este verano</a></li><li class="recentcomments">daniela en <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2013/05/09/abrete-sesamo-soluciones-a-los-6-problemas-de-cerraduras-mas-habituales/comment-page-1/#comment-1587">Ábrete Sésamo: soluciones a los 6 problemas de cerraduras más habituales</a></li><li class="recentcomments">gorullull en <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/07/25/deja-de-sufrir-la-noche-veraniega-10-trucos-para-dormir-fresco-sin-aire-acondicionado/comment-page-1/#comment-1586">8 maneras de adecuar tu casa para dormir mejor este verano</a></li><li class="recentcomments">Eléctrica Isleña en <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/04/24/sabes-que-es-un-boletin-electrico-y-si-lo-necesitas-en-tu-casa/comment-page-1/#comment-1585">¿Sabes qué es un boletín eléctrico y si lo necesitas en tu casa?</a></li><li class="recentcomments">Alarmas para casa en <a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2012/07/18/te-vas-de-vacaciones-10-trucos-para-evitar-que-te-roben-en-casa/comment-page-1/#comment-1584">¿Te vas de vacaciones? 10 trucos para evitar que te roben en casa</a></li></ul></div>
	<script type="text/javascript" src="http://www.google.com/jsapi"></script>
	<script type="text/javascript" src="http://cdn.20minutos.es/mmedia/feeds/js/feed.js"></script>

	<script type="text/javascript">
google.setOnLoadCallback(function() {
	var feeds = new j20m.feeds.MultipleFeeds([
		'http://www.20minutos.es/rss/blogs/'
	], {numFeedEntries: 10});
	feeds.load(feeds.display, {
	totalEntries: 10
	});
});
	</script>

	<div id="c20m_feed" class="c20m_feed widget">
		<h2><a href="http://www.20minutos.es/blogs_opinion/" target="_blank">Ahora en los blogs de 20minutos.es</a></h2>
		<div class="widget_content">
			<div id="j20m-feed" class="c20m_feed_datos">
				<p class="c20m_feed_cargando"><img alt="Cargando..." src="http://estaticos.20minutos.es/i/mini20/loading_news.gif"/></p>
			</div>
			<a class="c20m_feed_plus" href="http://www.20minutos.es/blogs_opinion/" target="_blank">Todos nuestros blogs</a>
		</div>
	</div>

	<div class="publi">
		<div class="cont_publi">
			<div id="Right2">
	<script type="text/javascript">
<!--
OAS_AD('Right2');
//-->
	</script>
</div>		</div>
	</div>

	<div class="widget"><h2 class="widgettitle">Piiiiii&#8230;.</h2>			<div class="textwidget">Aquí el “contestador automático” de Salva! Este es tu blog, así que por favor, siéntete libre para<strong> preguntarme en los comentarios</strong>  lo que quieras acerca de cada post, de <strong> tu experiencia o tus dudas</strong>  como manitas, y por supuesto para <strong> proponerme nuevos temas</strong>  para tratar próximamente. 
 
Ah, por cierto, como imaginarás ando todo el día de aquí para allá enfrascado en reparaciones, por eso te pido un poco de paciencia si tardo algo en contestar. Intentaré ponerme al día siempre con vuestros comentarios en mis huecos libres entre semana… ¡Palabra de Salvador! ¡Muchas gracias!
</div>
		</div><div class="widget"><h2 class="widgettitle">Recomendado por Reparalia </h2>			<div class="textwidget"><div class="widget_content">   <a href="http://www.reparalia.com/uploads/assets/docs/a925eae73f34b257af219da7f1792868de101c2c.pdf">- Consejos para ahorrar agua.</a><br />
     <a href="http://www.reparalia.com/uploads/assets/docs/a925eae73f34b257af219da7f1792868de101c2c.pdf">- Guía de actuación ante accidentes en el hogar.</a><br />
     <a href="http://www.reparalia.com/uploads/assets/docs/Guia%20Prevencion%20de%20Accidentes%20en%20el%20Hogar%20REPARALIA.pdf">- Guía de prevención de accidentes en el hogar.</a><br />
     <a href="http://www.reparalia.es/es/solicita-una-reparacion">- Solicita una reparación.</a>
</div>
</div>
		</div>
	<div class="widget widget_tag_cloud"><h2 class="widgettitle">Tags</h2><div class="tagcloud"><a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/agua/' class='tag-link-552581' title='10 temas' style='font-size: 9.1052631578947pt;'>Agua</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/ahorro/' class='tag-link-1606' title='27 temas' style='font-size: 14.140350877193pt;'>Ahorro</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/averias/' class='tag-link-1829243' title='9 temas' style='font-size: 8.6140350877193pt;'>averías</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/banos/' class='tag-link-1807915' title='9 temas' style='font-size: 8.6140350877193pt;'>Baños</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/calefaccion-2/' class='tag-link-1829246' title='11 temas' style='font-size: 9.5964912280702pt;'>calefacción</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/calor/' class='tag-link-7179' title='8 temas' style='font-size: 8pt;'>calor</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/cocina/' class='tag-link-975172' title='17 temas' style='font-size: 11.80701754386pt;'>Cocina</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/concurso/' class='tag-link-983576' title='17 temas' style='font-size: 11.80701754386pt;'>Concurso</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/consejos/' class='tag-link-1786050' title='25 temas' style='font-size: 13.649122807018pt;'>consejos</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/consultorio/' class='tag-link-1829265' title='25 temas' style='font-size: 13.649122807018pt;'>Consultorio</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/decoracion/' class='tag-link-1007380' title='39 temas' style='font-size: 15.982456140351pt;'>Decoración</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/diy/' class='tag-link-1824271' title='35 temas' style='font-size: 15.491228070175pt;'>DIY</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/dudas/' class='tag-link-1829267' title='11 temas' style='font-size: 9.5964912280702pt;'>dudas</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/electricidad/' class='tag-link-1544222' title='14 temas' style='font-size: 10.824561403509pt;'>electricidad</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/fontaneria-2/' class='tag-link-1835581' title='9 temas' style='font-size: 8.6140350877193pt;'>fontanería</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/frio/' class='tag-link-554090' title='8 temas' style='font-size: 8pt;'>Frío</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/hogar/' class='tag-link-854763' title='21 temas' style='font-size: 12.789473684211pt;'>hogar</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/ideas/' class='tag-link-1792933' title='14 temas' style='font-size: 10.824561403509pt;'>ideas</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/iluminacion/' class='tag-link-2021' title='9 temas' style='font-size: 8.6140350877193pt;'>Iluminación</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/invierno/' class='tag-link-554460' title='12 temas' style='font-size: 9.9649122807018pt;'>invierno</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/jardin-2/' class='tag-link-1824234' title='15 temas' style='font-size: 11.070175438596pt;'>jardín</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/juguetes/' class='tag-link-558670' title='9 temas' style='font-size: 8.6140350877193pt;'>Juguetes</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/limpieza/' class='tag-link-953' title='9 temas' style='font-size: 8.6140350877193pt;'>limpieza</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/madera/' class='tag-link-1218045' title='14 temas' style='font-size: 10.824561403509pt;'>Madera</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/manitas/' class='tag-link-1824275' title='13 temas' style='font-size: 10.456140350877pt;'>Manitas</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/mantenimiento/' class='tag-link-1840549' title='13 temas' style='font-size: 10.456140350877pt;'>mantenimiento</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/ninos/' class='tag-link-553596' title='23 temas' style='font-size: 13.280701754386pt;'>niños</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/pared/' class='tag-link-1816736' title='8 temas' style='font-size: 8pt;'>pared</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/paredes/' class='tag-link-1825159' title='9 temas' style='font-size: 8.6140350877193pt;'>Paredes</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/participa/' class='tag-link-1825176' title='8 temas' style='font-size: 8pt;'>Participa</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/pintura/' class='tag-link-552459' title='19 temas' style='font-size: 12.298245614035pt;'>Pintura</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/preguntas/' class='tag-link-1829266' title='18 temas' style='font-size: 12.052631578947pt;'>preguntas</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/premio/' class='tag-link-1340491' title='9 temas' style='font-size: 8.6140350877193pt;'>Premio</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/profesionales/' class='tag-link-1829272' title='38 temas' style='font-size: 15.859649122807pt;'>Profesionales</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/reciclaje/' class='tag-link-559421' title='14 temas' style='font-size: 10.824561403509pt;'>Reciclaje</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/regalo/' class='tag-link-559031' title='16 temas' style='font-size: 11.438596491228pt;'>regalo</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/reparalia/' class='tag-link-1829271' title='121 temas' style='font-size: 22pt;'>Reparalia</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/respuestas/' class='tag-link-1844952' title='11 temas' style='font-size: 9.5964912280702pt;'>respuestas</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/reutilizar/' class='tag-link-1824270' title='8 temas' style='font-size: 8pt;'>Reutilizar</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/salvador-de-la-casa/' class='tag-link-1835615' title='10 temas' style='font-size: 9.1052631578947pt;'>Salvador de la Casa</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/seguridad/' class='tag-link-554085' title='22 temas' style='font-size: 13.035087719298pt;'>seguridad</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/trucos/' class='tag-link-1792934' title='20 temas' style='font-size: 12.543859649123pt;'>Trucos</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/twitter/' class='tag-link-253' title='12 temas' style='font-size: 9.9649122807018pt;'>Twitter</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/verano/' class='tag-link-514' title='21 temas' style='font-size: 12.789473684211pt;'>Verano</a>
<a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/tag/video-consejo/' class='tag-link-1848141' title='10 temas' style='font-size: 9.1052631578947pt;'>vídeo consejo</a></div>
</div>
	<div class="widget widget_archivo">
		<h2>Archivo</h2>
		<div class="widget_content">
			<ul>
					<li><a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/07/'>julio 2014</a>&nbsp;(9)</li>
	<li><a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/06/'>junio 2014</a>&nbsp;(6)</li>
	<li><a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/05/'>mayo 2014</a>&nbsp;(5)</li>
	<li><a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/04/'>abril 2014</a>&nbsp;(12)</li>
	<li><a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/03/'>marzo 2014</a>&nbsp;(9)</li>
	<li><a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/02/'>febrero 2014</a>&nbsp;(9)</li>
	<li><a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2014/01/'>enero 2014</a>&nbsp;(8)</li>
	<li><a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2013/12/'>diciembre 2013</a>&nbsp;(8)</li>
	<li><a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2013/11/'>noviembre 2013</a>&nbsp;(10)</li>
	<li><a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2013/10/'>octubre 2013</a>&nbsp;(12)</li>
	<li><a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2013/09/'>septiembre 2013</a>&nbsp;(9)</li>
	<li><a href='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/2013/08/'>agosto 2013</a>&nbsp;(7)</li>
            	<li class="more"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/archivo/">+ ver todo el archivo</a></li>
			</ul>
		</div>
	</div>

    <div class="widget widget_categories"><h2 class="widgettitle">Categorías</h2>		<ul>
	<li class="cat-item cat-item-1606"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/ahorro/" title="Ver todas las entradas archivadas en Ahorro">Ahorro</a> (91)
</li>
	<li class="cat-item cat-item-1874403"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/aire-acondicionado-2/" title="Ver todas las entradas archivadas en Aire Acondicionado">Aire Acondicionado</a> (4)
</li>
	<li class="cat-item cat-item-1825141"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/albanileria/" title="Ver todas las entradas archivadas en Albañilería">Albañilería</a> (24)
</li>
	<li class="cat-item cat-item-554759"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/alquiler/" title="Ver todas las entradas archivadas en Alquiler">Alquiler</a> (4)
</li>
	<li class="cat-item cat-item-1615"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/anecdotas/" title="Ver todas las entradas archivadas en Anécdotas">Anécdotas</a> (23)
</li>
	<li class="cat-item cat-item-553297"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/animales/" title="Ver todas las entradas archivadas en Animales">Animales</a> (3)
</li>
	<li class="cat-item cat-item-1807915"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/banos/" title="Ver todas las entradas archivadas en Baños">Baños</a> (20)
</li>
	<li class="cat-item cat-item-1848133"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/bisagras/" title="Ver todas las entradas archivadas en bisagras">bisagras</a> (2)
</li>
	<li class="cat-item cat-item-1874307"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/calderas/" title="Ver todas las entradas archivadas en calderas">calderas</a> (2)
</li>
	<li class="cat-item cat-item-949657"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/calefaccion/" title="Ver todas las entradas archivadas en Calefacción">Calefacción</a> (17)
</li>
	<li class="cat-item cat-item-7179"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/calor/" title="Ver todas las entradas archivadas en calor">calor</a> (1)
</li>
	<li class="cat-item cat-item-1812770"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/capitan-planeta/" title="Ver todas las entradas archivadas en Capitán Planeta">Capitán Planeta</a> (31)
</li>
	<li class="cat-item cat-item-1848128"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/carpinteria-2/" title="Ver todas las entradas archivadas en Carpintería">Carpintería</a> (18)
</li>
	<li class="cat-item cat-item-1848114"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/cerrajeria/" title="Ver todas las entradas archivadas en Cerrajería">Cerrajería</a> (7)
</li>
	<li class="cat-item cat-item-975172"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/cocina/" title="Ver todas las entradas archivadas en Cocina">Cocina</a> (28)
</li>
	<li class="cat-item cat-item-983576"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/concurso/" title="Ver todas las entradas archivadas en Concurso">Concurso</a> (19)
</li>
	<li class="cat-item cat-item-1736"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/concursos/" title="Ver todas las entradas archivadas en Concursos">Concursos</a> (14)
</li>
	<li class="cat-item cat-item-1829265"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/consultorio/" title="Ver todas las entradas archivadas en Consultorio">Consultorio</a> (25)
</li>
	<li class="cat-item cat-item-1007380"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/decoracion/" title="Ver todas las entradas archivadas en Decoración">Decoración</a> (76)
</li>
	<li class="cat-item cat-item-1824243"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/descanso/" title="Ver todas las entradas archivadas en Descanso">Descanso</a> (26)
</li>
	<li class="cat-item cat-item-1824271"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/diy/" title="Ver todas las entradas archivadas en DIY">DIY</a> (62)
</li>
	<li class="cat-item cat-item-1040821"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/ecologia/" title="Ver todas las entradas archivadas en Ecología">Ecología</a> (35)
</li>
	<li class="cat-item cat-item-1825216"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/electricidad-2/" title="Ver todas las entradas archivadas en Electricidad">Electricidad</a> (30)
</li>
	<li class="cat-item cat-item-1047290"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/electrodomesticos/" title="Ver todas las entradas archivadas en Electrodomésticos">Electrodomésticos</a> (3)
</li>
	<li class="cat-item cat-item-1842995"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/eventos/" title="Ver todas las entradas archivadas en Eventos">Eventos</a> (2)
</li>
	<li class="cat-item cat-item-673"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/exterior/" title="Ver todas las entradas archivadas en Exterior">Exterior</a> (23)
</li>
	<li class="cat-item cat-item-1835580"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/fontaneria/" title="Ver todas las entradas archivadas en Fontanería">Fontanería</a> (27)
</li>
	<li class="cat-item cat-item-1858543"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/fontaneros/" title="Ver todas las entradas archivadas en fontaneros">fontaneros</a> (4)
</li>
	<li class="cat-item cat-item-1848135"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/humedad-2/" title="Ver todas las entradas archivadas en humedad">humedad</a> (5)
</li>
	<li class="cat-item cat-item-2021"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/iluminacion/" title="Ver todas las entradas archivadas en Iluminación">Iluminación</a> (25)
</li>
	<li class="cat-item cat-item-1824241"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/interior/" title="Ver todas las entradas archivadas en Interior">Interior</a> (72)
</li>
	<li class="cat-item cat-item-1858577"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/invierno-2/" title="Ver todas las entradas archivadas en Invierno">Invierno</a> (11)
</li>
	<li class="cat-item cat-item-1818023"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/jardin/" title="Ver todas las entradas archivadas en Jardín">Jardín</a> (25)
</li>
	<li class="cat-item cat-item-558670"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/juguetes/" title="Ver todas las entradas archivadas en Juguetes">Juguetes</a> (13)
</li>
	<li class="cat-item cat-item-1848130"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/lijadora/" title="Ver todas las entradas archivadas en lijadora">lijadora</a> (1)
</li>
	<li class="cat-item cat-item-1840883"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/limpieza-2/" title="Ver todas las entradas archivadas en Limpieza">Limpieza</a> (9)
</li>
	<li class="cat-item cat-item-1840549"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/mantenimiento/" title="Ver todas las entradas archivadas en mantenimiento">mantenimiento</a> (48)
</li>
	<li class="cat-item cat-item-1851494"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/memes/" title="Ver todas las entradas archivadas en Memes">Memes</a> (3)
</li>
	<li class="cat-item cat-item-1824245"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/mobiliario/" title="Ver todas las entradas archivadas en Mobiliario">Mobiliario</a> (36)
</li>
	<li class="cat-item cat-item-1824267"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/ninos-2/" title="Ver todas las entradas archivadas en Niños">Niños</a> (28)
</li>
	<li class="cat-item cat-item-1835591"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/nuestros-mayores/" title="Ver todas las entradas archivadas en Nuestros mayores">Nuestros mayores</a> (7)
</li>
	<li class="cat-item cat-item-1824278"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/oficinas/" title="Ver todas las entradas archivadas en Oficinas">Oficinas</a> (8)
</li>
	<li class="cat-item cat-item-1825135"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/organizacion/" title="Ver todas las entradas archivadas en Organización">Organización</a> (33)
</li>
	<li class="cat-item cat-item-552459"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/pintura/" title="Ver todas las entradas archivadas en Pintura">Pintura</a> (29)
</li>
	<li class="cat-item cat-item-1324609"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/piscinas/" title="Ver todas las entradas archivadas en Piscinas">Piscinas</a> (1)
</li>
	<li class="cat-item cat-item-1824248"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/plagas-e-insectos/" title="Ver todas las entradas archivadas en Plagas e insectos">Plagas e insectos</a> (5)
</li>
	<li class="cat-item cat-item-1824252"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/plantas-2/" title="Ver todas las entradas archivadas en Plantas">Plantas</a> (7)
</li>
	<li class="cat-item cat-item-1829266"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/preguntas/" title="Ver todas las entradas archivadas en preguntas">preguntas</a> (4)
</li>
	<li class="cat-item cat-item-1829272"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/profesionales/" title="Ver todas las entradas archivadas en Profesionales">Profesionales</a> (46)
</li>
	<li class="cat-item cat-item-1848129"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/puertas/" title="Ver todas las entradas archivadas en puertas">puertas</a> (6)
</li>
	<li class="cat-item cat-item-1824260"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/recetas-de-cocina/" title="Ver todas las entradas archivadas en Recetas de cocina">Recetas de cocina</a> (4)
</li>
	<li class="cat-item cat-item-1824233"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/reciclaje-y-gestion-de-basuras/" title="Ver todas las entradas archivadas en Reciclaje y gestión de basuras">Reciclaje y gestión de basuras</a> (30)
</li>
	<li class="cat-item cat-item-2179"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/regalos/" title="Ver todas las entradas archivadas en Regalos">Regalos</a> (8)
</li>
	<li class="cat-item cat-item-1825142"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/reparacion/" title="Ver todas las entradas archivadas en Reparación">Reparación</a> (54)
</li>
	<li class="cat-item cat-item-1829271"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/reparalia/" title="Ver todas las entradas archivadas en Reparalia">Reparalia</a> (6)
</li>
	<li class="cat-item cat-item-1844952"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/respuestas/" title="Ver todas las entradas archivadas en respuestas">respuestas</a> (2)
</li>
	<li class="cat-item cat-item-856315"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/ropa/" title="Ver todas las entradas archivadas en Ropa">Ropa</a> (2)
</li>
	<li class="cat-item cat-item-1848076"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/salud-2/" title="Ver todas las entradas archivadas en Salud">Salud</a> (11)
</li>
	<li class="cat-item cat-item-1814715"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/seguridad-2/" title="Ver todas las entradas archivadas en Seguridad">Seguridad</a> (50)
</li>
	<li class="cat-item cat-item-1708"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/sin-categoria/" title="Ver todas las entradas archivadas en Sin categoría">Sin categoría</a> (23)
</li>
	<li class="cat-item cat-item-1858595"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/solidaridad-2/" title="Ver todas las entradas archivadas en Solidaridad">Solidaridad</a> (1)
</li>
	<li class="cat-item cat-item-1400"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/television/" title="Ver todas las entradas archivadas en Televisión">Televisión</a> (1)
</li>
	<li class="cat-item cat-item-1824261"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/terraza/" title="Ver todas las entradas archivadas en Terraza">Terraza</a> (2)
</li>
	<li class="cat-item cat-item-1886045"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/vacaciones-2/" title="Ver todas las entradas archivadas en vacaciones">vacaciones</a> (2)
</li>
	<li class="cat-item cat-item-1848134"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/verano-2/" title="Ver todas las entradas archivadas en verano">verano</a> (24)
</li>
	<li class="cat-item cat-item-1814840"><a href="http://blogs.20minutos.es/un-hogar-con-mucho-oficio/category/videoconsejos/" title="Ver todas las entradas archivadas en videoconsejos">videoconsejos</a> (32)
</li>
		</ul>
</div>
	
</div>
							<div id="bottom" class="clearfix">
											</div>
				</div>
			</div>
		</div>

		<div id="pie">
			<div id="footer20m-wrapper">
				<link rel="stylesheet" type="text/css" media="all" href="http://cdn.20minutos.es/css4/header-footer-mini.css?20140613" />
<div id="footer-20m" class="local-es">
	<div class="inner-20m">
		<div class="head-footer-20m clearfix-20m">
			<div class="h1"><a href="http://www.20minutos.es/" title="Noticias actualidad en 20minutos.es">Noticias actualidad en 20minutos.es</a></div>
			<div class="h3"><a href="http://www.20minutos.es/especial/corporativo/creative-commons/">Este periódico se publica bajo licencia Creative Commons</a></div>
		</div>
		<div class="sep-top-20m">
			<div class="block-20m clearfix-20m">
	<div class="h4 menu-title-20m">Secciones</div>
	<ul class="menu-20m">
		<li><a href="http://www.20minutos.es/">Portada</a></li>
		<li><a href="http://www.20minutos.es/nacional/">Nacional</a></li>
		<li><a href="http://www.20minutos.es/internacional/">Internacional</a></li>
		<li><a href="http://www.20minutos.es/economia/">Economía</a></li>
		<li><a href="http://www.20minutos.es/tu-ciudad/">Tu ciudad</a></li>
		<li><a href="http://www.20minutos.es/deportes/">Deportes</a></li>
		<li><a href="http://www.20minutos.es/tecnologia/">Tecnología & Internet</a></li>
                <li><a href="http://www.20minutos.es/ciencia/">Ciencia</a></li>
		<li><a href="http://www.20minutos.es/cine/">Cine</a></li>
		<li><a href="http://www.20minutos.es/musica/">Música</a></li>
		<li><a href="http://www.20minutos.es/gente-television/">Gente y TV</a></li>
		<li><a href="http://www.harpersbazaar.es/">Moda y belleza</a></li>
		<li><a href="http://www.20minutos.es/zona20/">Comunidad20</a></li>
		<li><a href="http://lablogoteca.20minutos.es/">laBlogoteca</a></li>
		<li><a href="http://www.20minutos.es/salud/">Salud</a></li>
		<li><a href="http://www.20minutos.es/viajes/">Viajes</a></li>
                <li><a href="http://www.20minutos.es/gastronomia/">Gastro</a></li>
		<li><a href="http://www.20minutos.es/vivienda/">Vivienda</a></li>
		<li><a href="http://www.20minutos.es/empleo/">Empleo</a></li>
		<li><a href="http://www.20minutos.es/entrevistas/">Entrevistas</a></li>
		<li><a href="http://www.20minutos.es/servicios/">Servicios</a></li>
		<li><a href="http://www.20minutos.es/boletin/">Boletines</a></li>
                <li><a href="http://m.20minutos.es">Versión móvil</a></li>
                <li><span class="vlink" onclick="window.location.href='http://www.harpersbazaar.es/'">Harper's Bazaar</span></li>
                <li><a href="http://www.esquire.es/">Esquire</a></li>
                <li><a href="http://forbesmagazine.es/">Forbes</a></li>
	</ul>
</div>
<div class="block-20m clearfix-20m">
	<div class="h4 menu-title-20m">Especiales</div>
	<ul class="menu-20m">
                <li><a href="http://www.20minutos.es/especial/final-champions/">Final de la Champions</a></li>	
                <li><a href="http://www.20minutos.es/especial/carreras-populares/">Carreras populares</a></li>		
<li><a href="http://www.gonzoo.com/aulas/story/notas-de-corte-2014-para-acceder-a-la-universidad-1740/">Notas de corte 2014</a></li>   
		<li><a href="http://www.20minutos.es/especial/primera-guerra-mundial/" title="100 años de la IGM">100 años de la IGM</a></li>		
                <li><a href="http://www.20minutos.es/especial/declaracion-renta/">Renta 2013</a></li>	                
                <li><a href="http://www.20minutos.es/especial/formula-1/">Formula 1</a></li>
                <li><a href="http://www.20minutos.es/especial/decimo-aniversario-11m/">Diez años del 11-M</a></li>	
		<li><a href="http://www.20minutos.es/especial/loteria-navidad/" title="Lotería Navidad">Lotería de Navidad</a></li>
		<li><a href="http://tiempoytemperatura.es" title="El tiempo">El tiempo</a></li>
	</ul>
</div>
<div class="block-20m clearfix-20m">
	<div class="h4 menu-title-20m">Minutecas</div>
	<ul class="menu-20m">
                <li><a href="http://www.20minutos.es/minuteca/abdicacion-rey-juan-carlos-i/" title="El rey Juan Carlos abdica">El rey Juan Carlos abdica</a></li>
 <li><a href="http://www.20minutos.es/minuteca/rey-felipe-vi/ " title="Rey Felipe VI">Rey Felipe VI</a></li>
                <li><a href="http://www.20minutos.es/minuteca/elpidio-jose-silva/" title="Elpidio José Silva">Elpidio Silva</a></li>
	        <li><a href="http://www.20minutos.es/minuteca/pablo-iglesias/" title="Pablo Iglesia">Pablo Iglesias</a></li>	
                <li><a href="http://www.20minutos.es/minuteca/malasia/" title="Avión desaparecido">Avión desaparecido</a></li>
                <li><a href="http://www.20minutos.es/minuteca/fernando-alonso/">Fernando Alonso</a></li>
		<li><a href="http://www.20minutos.es/minuteca/luis-barcenas/">Luis Bárcenas</a></li>
		<li><a href="http://www.20minutos.es/minuteca/facebook/">Facebook</a></li>
		<li><a href="http://www.20minutos.es/minuteca/youtube/">Youtube</a></li>
		<li><a href="http://www.20minutos.es/minuteca/twitter/">Twitter</a></li>
		<li><a href="http://www.20minutos.es/minuteca/apple/">Apple</a></li>
	</ul>
</div>

<div class="block-20m clearfix-20m">
	<div class="h4 menu-title-20m">Corporativo</div>
	<ul class="menu-20m">
         	<li><a href="http://www.grupo20minutos.com">Grupo 20minutos</a></li>
		<li><span class="vlink" onclick="window.location.href='http://www.schibsted.com'">Schibsted Group</span></li>
		<li><a href="http://www.grupo20minutos.com/contacto.html">Publicidad en 20minutos</a></li>
		<li><span class="vlink" onclick="window.location.href='http://www.20minutos.es/especial/corporativo/quienes-somos'">Quiénes somos</span></li>
		<li><span class="vlink" onclick="window.location.href='http://www.20minutos.es/especial/corporativo/contacto'">Contacto</span></li>
		<li><span class="vlink" onclick="window.location.href='http://www.20minutos.es/aviso_legal/'">Aviso legal</span></li>
                <li><a href="http://www.gonzoo.com" title="Gonzoo, diario para jóvenes">Gonzoo</a></li>
                <li><a href="http://www.20minutos.com.mx/" title="20minutos México">Noticias de México</a></li>
		<li><a href="http://www.20minutos.com/" title="20minutos USA">Noticias de USA</a></li>
	</ul>
</div>
		</div>
	</div>
</div>

			</div>
		</div>

		<!-- Xiti Tag -->
<script type="text/javascript">
<!--
xtnv = document;
xtsd = "http://logi125";
xtsite = "467263";
xtn2 = "14";
xtpage = "un-hogar-con-mucho-oficio::" + document.location.pathname;
xtdi = "";
xt_multc = "";
xt_an = "";
xt_ac = "";

if (window.xtparam!=null){window.xtparam+=xt_multc+"&ac="+xt_ac+"&an="+xt_an;}
else{window.xtparam=xt_multc+"&ac="+xt_ac+"&an="+xt_an;};
//-->
</script>
<script type="text/javascript" src="http://cdn.20minutos.es/js2/xtcore.js"></script>
<noscript>
<img width="1" height="1" alt="" src="http://logi125.xiti.com/hit.xiti?s=467263&amp;s2=14&amp;p=un-hogar-con-mucho-oficio::sin_url&amp;di=&amp;an=&amp;ac=" />
</noscript>
<!-- End Xiti Tag -->
		<!-- START Google Analytics -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ?"https://ssl.":"http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost +"google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-3207705-1");
pageTracker._setDomainName("20minutos.es");
pageTracker._initData();
pageTracker._trackPageview();
</script>
<!-- END Google Analytics -->
						<script type='text/javascript' src='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/wp-includes/js/comment-reply.min.js?ver=3.9.1'></script>
<script type='text/javascript' src='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/wp-content/plugins/contact-form-7/includes/js/jquery.form.min.js?ver=3.50.0-2014.02.05'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _wpcf7 = {"loaderUrl":"http:\/\/blogs.20minutos.es\/un-hogar-con-mucho-oficio\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","sending":"Enviando...","cached":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='http://blogs.20minutos.es/un-hogar-con-mucho-oficio/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=3.8'></script>

		<!-- Cookies policy -->
		<script type="text/javascript" src="http://www.grupo20minutos.com/js/cookiespolicy.min.js?20131121"></script>

	</body>
</html>

<!-- Dynamic page generated in 0.162 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2014-07-31 10:17:40 -->

<!-- super cache -->