<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="es"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="es"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="es"><![endif]-->
<!--[if IE 9]><html class="no-js ie9" lang="es"><![endif]-->
<!--[if gt IE 9]><!--><html class="no-js no-touch" lang="es"><!--<![endif]-->
	<head>
		<title>El Corte Inglés - Reclamaciones, quejas y opiniones</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="application-name" content="El Corte Inglés - Reclamaciones, quejas y opiniones">

	<meta name="description" content="El Corte Inglés, reclamaciones, quejas y opiniones de El Corte Inglés. Lee todas las quejas de los usuarios de Critizen sobre El Corte Inglés  y escribe la tuya. ">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="author" content="Critizen">
	<link rel="canonical" href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es">
	


<meta property="og:url" content="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es">
<meta property="fb:app_id" content="139349849412235">
				<meta property="og:type" content="critizen:cosa">
						<meta property="og:site_name" content="Critizen">
						<meta property="og:title" content="El Corte Inglés - Reclamaciones, quejas y opiniones">
								<meta property="og:image" content="https://www.critizen.com/critizen/img/critic-default-card.png">
						<meta property="critizen:created_at" content="2014-10-01">
								


				<meta property="twitter:card" content="summary">
						<meta property="twitter:site" content="@critizen">
						<meta property="twitter:title" content="El Corte Inglés - Reclamaciones, quejas y opiniones">
								<meta property="twitter:image" content="https://www.critizen.com/critizen/img/critic-default-list.png">
						<meta property="twitter:url" content="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es">
						<meta property="twitter:app:country" content="es">
						<meta property="twitter:app:name:iphone" content="306934135">
																<meta property="twitter:app:name:googleplay" content="Critizen">
						<meta property="twitter:app:id:googleplay" content="com.critizen">
						<meta property="twitter:app:url:googleplay" content="https://www.critizen.com/critic/690">
		

	<link rel="next" href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/2">


<link rel="apple-touch-icon" href="https://d138d6gvpe36jl.cloudfront.net/critizen/img/iphone-icon-favicon152-alpha.png">
<meta name="apple-itunes-app" content="app-id=943013189">
<meta name="google-play-app" content="app-id=com.critizen">

<meta property="al:android:url" content="https://play.google.com/store/apps/details?id=com.critizen" />
<meta property="al:android:app_name" content="Critizen" />
<meta property="al:android:package" content="com.next.critizen" />

		<!-- CSS -->
		<!--[if (!IE)|(gte IE 10)]><!-->
					<link rel="stylesheet" href="https://d138d6gvpe36jl.cloudfront.net/css/min/1424880299_group_critizen.css">
				<!--<![endif]-->

		<!--[if lt IE 10]>
					<link rel="stylesheet" href="https://d138d6gvpe36jl.cloudfront.net/critizen/css/bootstrap.min.css">
					<link rel="stylesheet" href="https://d138d6gvpe36jl.cloudfront.net/critizen/css/bootstrap-switch.css">
					<link rel="stylesheet" href="https://d138d6gvpe36jl.cloudfront.net/critizen/css/critizen-theme.css">
					<link rel="stylesheet" href="https://d138d6gvpe36jl.cloudfront.net/css/smartbanner.css">
					<link rel="stylesheet" href="https://d138d6gvpe36jl.cloudfront.net/critizen/css/font-awesome.css">
					<link rel="stylesheet" href="https://d138d6gvpe36jl.cloudfront.net/critizen/css/bootstrap-social.css">
				<![endif]-->
		
		
		<link rel="shortcut icon" href="https://d138d6gvpe36jl.cloudfront.net/critizen/img/favicon.ico">

		
					

		
		<script>
			var jsBaseUrl = "https://d138d6gvpe36jl.cloudfront.net";
			App = [];
		</script>

        
                <script src="https://d138d6gvpe36jl.cloudfront.net/js/min/1418649912_group_critizen_head.js" type="text/javascript"></script>
        
		
		<!--[if lt IE 9]>
		<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<script>window.html5 || document.write('<script src="js/libs/html5.js"><\/script>')</script>
		<![endif]-->

		<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(["_setAccount","UA-53522380-1"]);
	_gaq.push(["_setDomainName","critizen.com"]);
	_gaq.push(["_setCampaigncookieTimeout",648000000]);
	_gaq.push(["_setAllowLinker",true]);
	_gaq.push(["_setAllowHash",false]);
	_gaq.push(["_trackPageview"]);
	_gaq.push(["_setCustomVar",1,'login','logoff', 2]);
	(function() {
		var ga = document.createElement('script');
		ga.type = 'text/javascript';
		ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(ga, s);
	})();
</script>
<!-- start Mixpanel --><script type="text/javascript">(function(f,b){if(!b.__SV){var a,e,i,g;window.mixpanel=b;b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");
for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2;a=f.createElement("script");a.type="text/javascript";a.async=!0;a.src="//cdn.mxpnl.com/libs/mixpanel-2.2.min.js";e=f.getElementsByTagName("script")[0];e.parentNode.insertBefore(a,e)}})(document,window.mixpanel||[]);
mixpanel.init("21c26f5fdaa2ff15277c1c314da5f9fb");</script><!-- end Mixpanel -->



	</head>

	<body class="">
		<div class="main-wrapper">
		
				<div class="alert alert-info" role="alert">
			  Utilizamos cookies propias y de terceros para mejorar nuestros servicios y mostrarle publicidad que pueda estar relacionada con su localización geográfica. Si continúa navegando consideramos que acepta su uso. Puede cambiar la configuración u obtener más información en nuestra <a href="/politica-cookies">Política de cookies</a>
			  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		</div>
		
		<div role="navigation" class="navbar navbar-header">
			<header class="main-header">
				<div class="container">
			      <!--Logo shema-->
			      <div itemscope itemtype="http://schema.org/Organization" class="logo-critic">
			        <a class="visible-sm visible-md visible-lg" itemprop="url" title="Critizen" href="https://www.critizen.com/home/dispatch/es">
			          <img itemprop="logo" alt="Critizen" src="/critizen/img/logo-critizen.png" />
			        </a>
			        <a class="hidden-sm hidden-md hidden-lg" itemprop="url" title="Critizen" href="https://www.critizen.com/home/dispatch/es">
			          <img itemprop="logo" alt="Critizen" src="/critizen/img/logo-critic-small.png" />
			        </a>
			      </div>
			      <!-- .Logo shema-->
					<h1 class="pull-left hidden-xs">Quéjate fuerte</h1>
											<nav class="nav-header clearfix">
							<div class="pull-right">
								<a href="https://www.critizen.com/critic/create" class="btn-critic-dark last-critics">Haz tu crítica o elogio</a>
								<a href="https://www.critizen.com/user/loginExternalRequest/facebook" class="btn btn-social-icon btn-facebook"><i class="fa fa-facebook"></i></a>
								<a href="https://www.critizen.com/user/loginExternalRequest/google"  class="btn btn-social-icon btn-google-plus"><i class="fa fa-google-plus"></i></a>
								<a href="https://www.critizen.com/user/loginExternalRequest/twitter"   class="btn btn-social-icon btn-twitter"><i class="fa fa-twitter"></i></a>
							</div>
						</nav>
									</div>
			</header>
			<!-- TIENE QUE APARECER SOLO EN VISTA MOVIL -->
			<!--   <div class="navbar navbar-fixed-bottom hidden-sm hidden-md hidden-lg c-lowerMainNavbar">
				<ul class="list-inline">
				  <li>
				<a href="search.php"><span class="glyphicon glyphicon-search"></span></a>
				  </li>
				  <li class="active">
				<a href="#"><img class="btn-lowerNav" src="images/critizen//btn-lowerNavbar.png"></span></a>
				  </li>
				  <li>
				<a href="profile.php"><span class="glyphicon glyphicon-user"></span></a>
				  </li>
				  <li class="align-center">
				<a href="#">Condiciones legales</a>
				  </li>
				</ul>
			  </div>
			-->
		</div>


	


<section class="m-card-carousel">
    <div class="container">
		<div id="carousel-example-generic" class="carousel slide" data-interval="false">

			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				<div class="item no-back active">

					<!--       <img src="http://globalassets.starbucks.com/assets/b7e45f5dcc7341d188c72f4d5cd52d0c.jpg" alt="...">
					-->
					<div class="carousel-caption">

						<div class="card-brand">
							<img src="/critizen/img/critic-default-card.png">
						</div>

						<div class="card-info" data-model="item" data-item="{&quot;id&quot;:&quot;690&quot;,&quot;user_id&quot;:&quot;1&quot;,&quot;group_id&quot;:null,&quot;business_id&quot;:null,&quot;type&quot;:&quot;brand&quot;,&quot;name&quot;:&quot;El Corte Ingl\u00e9s&quot;,&quot;address&quot;:null,&quot;locality&quot;:null,&quot;region&quot;:null,&quot;postal_code&quot;:null,&quot;country_iso&quot;:&quot;&quot;,&quot;image_id&quot;:null,&quot;latitude&quot;:null,&quot;longitude&quot;:null,&quot;fb&quot;:&quot;http:\/\/es-es.facebook.com\/elcorteingles&quot;,&quot;tw&quot;:&quot;https:\/\/twitter.com\/elcorteingles&quot;,&quot;phone&quot;:null,&quot;web&quot;:&quot;http:\/\/www.elcorteinglescorporativo.es\/elcorteinglescorporativo\/elcorteinglescorporativo\/index.jsp&quot;,&quot;email&quot;:&quot;servicio_clientes@elcorteingles.es,lola.burson@fullsix.com&quot;,&quot;schedule&quot;:null,&quot;created_at&quot;:&quot;1412158558&quot;,&quot;deleted_at&quot;:null,&quot;revised_at&quot;:&quot;1412158558&quot;,&quot;deleted_by&quot;:null,&quot;critic_rank&quot;:3.253,&quot;source&quot;:&quot;tr&quot;,&quot;external_id&quot;:&quot;69085&quot;,&quot;redirect_id&quot;:null,&quot;image&quot;:{&quot;fingerprint&quot;:null,&quot;environment&quot;:null},&quot;categories&quot;:[{&quot;id&quot;:&quot;9&quot;,&quot;metadatum&quot;:&quot;food&quot;,&quot;langList&quot;:{&quot;es&quot;:&quot;Alimentaci\u00f3n y consumo&quot;,&quot;en&quot;:&quot;Food and consumer&quot;}},{&quot;id&quot;:&quot;16&quot;,&quot;metadatum&quot;:&quot;fashion&quot;,&quot;langList&quot;:{&quot;es&quot;:&quot;Moda y Hogar&quot;,&quot;en&quot;:&quot;Fashion and home decor&quot;}}],&quot;permalink&quot;:{&quot;es&quot;:&quot;\/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es&quot;,&quot;en&quot;:&quot;\/food-and-consumer-complaints-el-corte-ingles_i690_en&quot;}}">
							<h2>El Corte Inglés</h2>
																								<p class="index" ><span class="icon-info" data-toggle="tooltip" title="La nota está basada en el Índice de Recomendación asignado a una empresa en función de las críticas positivas y negativas de los usuarios a éstas."></span> Nota: <b>3</b>/10</p>
							
															<p class="critics">
									75 Críticas
								<p>
														</div>
						<a href="https://www.critizen.com/critic/create/690" class="btn-critic btn-short"><span class="icon-megaphone"></span> Critícalo</a>
					</div>

					<!--
					<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
						<span class="icon-right"></span>
					</a>
					-->
				</div>
				<!--
				<div class="item no-back">
					<img src="http://globalassets.starbucks.com/assets/16c05f19a48d437f9c97d321c4a18fa9.png" alt="...">
					<div class="carousel-caption">
						<p class="card-description">
				
			</p>
		</div>
		<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
			<span class="icon-left"></span>
		</a>
	</div>
				-->
			</div>
		</div>
    </div>
</section>

<nav class="feelings">
    <div class="container">
		<ul class="unstyled inlined">
			<li class="all active">
				<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/">
					<span></span>
					Todos
				</a>
			</li>
			<li class="happy-bar ">
				<a  href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/happy">
					<!-- 					<span class="happy f-nav"></span> -->
					<span>Felices</span>
				</a>
			</li>
			<li class="disappointed-bar ">
				<a  href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/disappointed">
					<!-- 					<span class="disappointed f-nav"></span> -->
					<span>Disgustados</span>
				</a>
			</li>
			<li class="frustrated-bar ">
				<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/frustrated">
					<!-- 					<span class="frustrated f-nav"></span> -->
					<span>Frustrados</span>
				</a>
			</li>
			<li class="indignant-bar ">
				<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/indignant">
					<!-- 					<span class="indignant f-nav"></span> -->
					<span>Indignados</span>
				</a>
			</li>
			<li class="furious-bar ">
				<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/furious">
					<!-- 					<span class="furious f-nav"></span> -->
					<span>Furiosos</span>
				</a>
			</li>
		</ul>
    </div>
</nav>

<section class="m-list bg-page">

    <div class="container">

		
		
		<div class="row">
			<div class="row" id="grid-layout">
			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="11364"
         data-critic-item="690"
         data-critic-score="indignant"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  indignant">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/22151"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/cef34e4e/7b2d9180/ea56abdd/99f61eb7/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/22151">Raúl García Hernández</a>			</h4>
			<p>Con indignación. <span class="muted">Hace <b>22 horas</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/corte-ingles-enviado-tercer-aviso-retraso-pedido_c11364" data-action="expand"><p>Hoy El Corte Inglés me ha enviado un tercer aviso de retraso en un pedido realizado a través de su tienda virtual. Se trata de unos auriculares que compré el 24 de Enero de 2015, y a día de hoy, si...</p></a>
							<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/corte-ingles-enviado-tercer-aviso-retraso-pedido_c11364" data-action="expand"><img src="https://d138d6gvpe36jl.cloudfront.net/critic/f440fe76/4ba242c0/a7f58094/5c4ac180/400.jpg"></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/corte-ingles-enviado-tercer-aviso-retraso-pedido_c11364" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						5252
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/corte-ingles-enviado-tercer-aviso-retraso-pedido_c11364" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							4
						</span>
					</a>
				</span>
					

			<!--sended frame-->
			    	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="11364">+<span class="plusone-num">6</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/corte-ingles-enviado-tercer-aviso-retraso-pedido_c11364"
		   			   data-twitter-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="11364"
		   			   data-image="https://d138d6gvpe36jl.cloudfront.net/critic/f440fe76/4ba242c0/a7f58094/5c4ac180/550.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="11172"
         data-critic-item="690"
         data-critic-score="happy"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  happy">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/19295"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/64124805/a5b9a37b/781601af/c0cb472b/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/19295">Soraya Hyppie</a>			</h4>
			<p>Con felicidad. <span class="muted">Hace <b>1 día</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/super-contenta-vestido-nuevo-para-eventode-eeso_c11172" data-action="expand"><p>super contenta con mi vestido nuevo para un evento.de 100 e a 30 e.eso son rebajas!!!cuestión de suerte porque solo había esa talla pero que no vayas buscando y encuentres da una alegría!!</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/super-contenta-vestido-nuevo-para-eventode-eeso_c11172" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						3498
					</a>
				</span>
			
		
		

			<!--sended frame-->
			    	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="11172">+<span class="plusone-num">4</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/super-contenta-vestido-nuevo-para-eventode-eeso_c11172"
		   			   data-twitter-title="Otro que está #czfeliz con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfeliz con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="11172"
		   			   data-image="https://www.critizen.com/critizen/img/score/happy.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="10813"
         data-critic-item="690"
         data-critic-score="indignant"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  indignant">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/20979"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/0a4925d1/fe68069b/3a8f1d5f/72f5b78c/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/20979">JOSE ANTONIO GARCIA</a>			</h4>
			<p>Con indignación. <span class="muted">Hace <b>3 días</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/servicio-tienda-online-deplorable-compras-producto_c10813" data-action="expand"><p>Servicio tienda online deplorable.  Compras un producto hace un mes que se supone te entregan en 15 días,  vendrá de Australia por barco,  y te mandan un email cada 7 días aplazando la entrega. Qui...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/servicio-tienda-online-deplorable-compras-producto_c10813" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						6372
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/servicio-tienda-online-deplorable-compras-producto_c10813" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							5
						</span>
					</a>
				</span>
					

									<span data-toggle="tooltip" title="Veces compartida">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/servicio-tienda-online-deplorable-compras-producto_c10813" data-action="expand">
						<i class="glyphicon glyphicon-share"></i>
						<span class="js-share-counter " data-single="" data-plural="">
							3
						</span>
					</a>
				</span>
						<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="10813">+<span class="plusone-num">3</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/servicio-tienda-online-deplorable-compras-producto_c10813"
		   			   data-twitter-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="10813"
		   			   data-image="https://www.critizen.com/critizen/img/score/indignant.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="10675"
         data-critic-item="690"
         data-critic-score="happy"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  happy">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/7336"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/7f33eb14/606421a8/42eb65c5/93d28275/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/7336">Madeleine Motiger</a>			</h4>
			<p>Con felicidad. <span class="muted">Hace <b>3 días</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/encanta-iniciativa-corte-ingles-aventura-madre_c10675" data-action="expand"><p>Me encanta la iniciativa de El Corte Inglés "la aventura de ser madre". Han puesto actividades, una interesante financiación y también charlas de profesionales. El otro día pude ver la de Supernann...</p></a>
							<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/encanta-iniciativa-corte-ingles-aventura-madre_c10675" data-action="expand"><img src="https://d138d6gvpe36jl.cloudfront.net/critic/15849691/332cdd2e/9919ea0b/4f3e80cf/400.jpg"></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/encanta-iniciativa-corte-ingles-aventura-madre_c10675" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						5100
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/encanta-iniciativa-corte-ingles-aventura-madre_c10675" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							3
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="10675">+<span class="plusone-num">5</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/encanta-iniciativa-corte-ingles-aventura-madre_c10675"
		   			   data-twitter-title="Otro que está #czfeliz con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfeliz con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="10675"
		   			   data-image="https://d138d6gvpe36jl.cloudfront.net/critic/15849691/332cdd2e/9919ea0b/4f3e80cf/550.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="10670"
         data-critic-item="690"
         data-critic-score="happy"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  happy">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/76"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/8b1ce3a3/5533a95a/3656491b/b0478cdf/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/76">Alberto Moreno Rodriguez</a>			</h4>
			<p>Con felicidad. <span class="muted">Hace <b>3 días</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/diciembre-compre-abrigo-tenia-cremallera-metalica_c10670" data-action="expand"><p>En diciembre compré un abrigo que tenía la cremallera metálica y se atascaba con facilidad si no tenias cuidado. Hoy, dos meses y medio después, he ido sin ticket a que me dieran una alternativa ya...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/diciembre-compre-abrigo-tenia-cremallera-metalica_c10670" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						5091
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/diciembre-compre-abrigo-tenia-cremallera-metalica_c10670" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							3
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="10670">+<span class="plusone-num">4</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/diciembre-compre-abrigo-tenia-cremallera-metalica_c10670"
		   			   data-twitter-title="Otro que está #czfeliz con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfeliz con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="10670"
		   			   data-image="https://www.critizen.com/critizen/img/score/happy.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="10487"
         data-critic-item="690"
         data-critic-score="furious"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  furious">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/20197"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/5951bd6c/2fe5ba8b/0d5d8cc6/83ea280a/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/20197">silvia suarez</a>			</h4>
			<p>Con furia. <span class="muted">Hace <b>4 días</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compre-internet-prendas-cambiado-devuelto-otra-hace_c10487" data-action="expand"><p>compre por internet 2 prendas. he cambiado una y devuelto otra. hace casi un mes y no me han devuelto el dinero. en llamadas al 902 llevo gastado un monton y nada. los mails a atencion al cliente (...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compre-internet-prendas-cambiado-devuelto-otra-hace_c10487" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						7988
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compre-internet-prendas-cambiado-devuelto-otra-hace_c10487" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							7
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="10487">+<span class="plusone-num">4</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compre-internet-prendas-cambiado-devuelto-otra-hace_c10487"
		   			   data-twitter-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="10487"
		   			   data-image="https://www.critizen.com/critizen/img/score/furious.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="10143"
         data-critic-item="690"
         data-critic-score="indignant"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  indignant">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/18567"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/f63dc747/5ab1ca33/c29c35b6/fd0cd45e/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/18567">Pili Esteban</a>			</h4>
			<p>Con indignación. <span class="muted">Hace <b>5 días</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/black-friday-compre-unas-peliculas-oferta-ademas_c10143" data-action="expand"><p>En Black Friday me compre unas peliculas de oferta. y ademas de cobrarme 4 euros de gastos de envío tardaron 10 dias em traermelo…  Un poco de rapidez no vendria mal si pagas esos gastos de envio </p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/black-friday-compre-unas-peliculas-oferta-ademas_c10143" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						5998
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/black-friday-compre-unas-peliculas-oferta-ademas_c10143" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							2
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="10143">+<span class="plusone-num">3</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/black-friday-compre-unas-peliculas-oferta-ademas_c10143"
		   			   data-twitter-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="10143"
		   			   data-image="https://www.critizen.com/critizen/img/score/indignant.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="9748"
         data-critic-item="690"
         data-critic-score="furious"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  furious">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/18057"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/445e589e/3f19cbd9/038f55d7/a2d94b00/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/18057">juan carlos escobar orzaez</a>			</h4>
			<p>Con furia. <span class="muted">Hace <b>6 días</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compreis-moviles-corte-ingles-pasan-garantia_c9748" data-action="expand"><p>no compréis móviles en el corte inglés ya que no te pasan garantía, si lo llevas aunque sea un poco sucio se escudan en no cubrir la garantía alegando mal uso y ademas te lo devuelven a los dos mes...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compreis-moviles-corte-ingles-pasan-garantia_c9748" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						6487
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compreis-moviles-corte-ingles-pasan-garantia_c9748" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							10
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="9748">+<span class="plusone-num">4</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compreis-moviles-corte-ingles-pasan-garantia_c9748"
		   			   data-twitter-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="9748"
		   			   data-image="https://www.critizen.com/critizen/img/score/furious.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="9435"
         data-critic-item="690"
         data-critic-score="indignant"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  indignant">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/17261"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/bfae96d8/e1b70e34/84a31a6b/975879dc/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/17261">MARI CARMEN MARTINEZ</a>			</h4>
			<p>Con indignación. <span class="muted">Hace <b>7 días</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/pasado-compre-corte-ingles-tarjeta-mas-normal_c9435" data-action="expand"><p>el mes pasado compre en el corte inglés con mi tarjeta.  y lo más normal es q la factura venga a mi nombre. Pues no llego a nombre de la persona q tdavia vive conmigo. por que hacen eso?<br />
</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/pasado-compre-corte-ingles-tarjeta-mas-normal_c9435" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						4477
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/pasado-compre-corte-ingles-tarjeta-mas-normal_c9435" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							3
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="9435">+<span class="plusone-num">1</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/pasado-compre-corte-ingles-tarjeta-mas-normal_c9435"
		   			   data-twitter-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="9435"
		   			   data-image="https://www.critizen.com/critizen/img/score/indignant.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="9186"
         data-critic-item="690"
         data-critic-score="indignant"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  indignant">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/16665"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/940a4852/81b61d0b/998c0514/b0793561/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/16665">Javier Serna</a>			</h4>
			<p>Con indignación. <span class="muted">Hace <b>1 semana</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/hola-compre-impresora-corte-ingles-recibirla-sorpresa_c9186" data-action="expand"><p>Hola, compre una impresora en la web del corte inglés y al recibirla mi sorpresa cual fue, me enviaron otra impresora de menor valor y peores características, llame para que me hicieran el cambio y...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/hola-compre-impresora-corte-ingles-recibirla-sorpresa_c9186" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						6091
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/hola-compre-impresora-corte-ingles-recibirla-sorpresa_c9186" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							4
						</span>
					</a>
				</span>
					

									<span data-toggle="tooltip" title="Veces compartida">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/hola-compre-impresora-corte-ingles-recibirla-sorpresa_c9186" data-action="expand">
						<i class="glyphicon glyphicon-share"></i>
						<span class="js-share-counter " data-single="" data-plural="">
							1
						</span>
					</a>
				</span>
						<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="9186">+<span class="plusone-num">5</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/hola-compre-impresora-corte-ingles-recibirla-sorpresa_c9186"
		   			   data-twitter-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="9186"
		   			   data-image="https://www.critizen.com/critizen/img/score/indignant.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="9080"
         data-critic-item="690"
         data-critic-score="disappointed"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  disappointed">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/16392"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/8c2bcc30/a67ac271/d4191bbd/0cb0b6f9/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/16392">Pablo Canales Pérez</a>			</h4>
			<p>Con disgusto. <span class="muted">Hace <b>1 semana</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/molesta-corte-ingles-hinchado-precioshabeis-visto_c9080" data-action="expand"><p>Lo que mas molesta del Corte Ingles es lo hinchado de sus precios...¿Habeis visto el precio de las colonias?La misma marca y modelo en tiendas como Druni o Pacos esta mas economica...¿Que explicaci...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/molesta-corte-ingles-hinchado-precioshabeis-visto_c9080" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						6333
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/molesta-corte-ingles-hinchado-precioshabeis-visto_c9080" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							14
						</span>
					</a>
				</span>
					

									<span data-toggle="tooltip" title="Veces compartida">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/molesta-corte-ingles-hinchado-precioshabeis-visto_c9080" data-action="expand">
						<i class="glyphicon glyphicon-share"></i>
						<span class="js-share-counter " data-single="" data-plural="">
							3
						</span>
					</a>
				</span>
						<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="9080">+<span class="plusone-num">6</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/molesta-corte-ingles-hinchado-precioshabeis-visto_c9080"
		   			   data-twitter-title="Otro que está #czdisgustado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czdisgustado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="9080"
		   			   data-image="https://www.critizen.com/critizen/img/score/disappointed.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="9072"
         data-critic-item="690"
         data-critic-score="frustrated"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  frustrated">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/16378"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/af40974b/f3730333/27bdfe8a/aec46300/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/16378">RODRIGO EVA</a>			</h4>
			<p>Con frustración. <span class="muted">Hace <b>1 semana</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compro-pagina-corte-ingles-donde-figura-eslogan_c9072" data-action="expand"><p>Compro en página web de el corte inglés donde figura su eslogan "si no estás satisfecho te devolvemos tu dinero".<br />
Cuando voy a devolver artículo(sin abrir) me dicen que lea la letra pequeña: SÓLO H...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compro-pagina-corte-ingles-donde-figura-eslogan_c9072" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						6742
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compro-pagina-corte-ingles-donde-figura-eslogan_c9072" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							12
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="9072">+<span class="plusone-num">5</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compro-pagina-corte-ingles-donde-figura-eslogan_c9072"
		   			   data-twitter-title="Otro que está #czfrustrado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfrustrado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="9072"
		   			   data-image="https://www.critizen.com/critizen/img/score/frustrated.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="9037"
         data-critic-item="690"
         data-critic-score="frustrated"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  frustrated">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/16279"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/60e18aca/a8ce1c8a/7880e73b/02fc6924/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/16279">julian redstone</a>			</h4>
			<p>Con frustración. <span class="muted">Hace <b>1 semana</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/deberian-prestar-mas-servicios-burgos-ciudad-reformar_c9037" data-action="expand"><p>Deberían prestar más servicios en Burgos ciudad y reformar parte de sus instalaciones o hacer una gran superfície. También implicarse con la ciudad, y no solo vender. Por ejemplo patrocinar el Burg...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/deberian-prestar-mas-servicios-burgos-ciudad-reformar_c9037" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						5877
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/deberian-prestar-mas-servicios-burgos-ciudad-reformar_c9037" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							1
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="9037">+<span class="plusone-num">5</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/deberian-prestar-mas-servicios-burgos-ciudad-reformar_c9037"
		   			   data-twitter-title="Otro que está #czfrustrado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfrustrado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="9037"
		   			   data-image="https://www.critizen.com/critizen/img/score/frustrated.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="9029"
         data-critic-item="690"
         data-critic-score="indignant"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  indignant">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/16267"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/662dcfd6/f501917e/6baced26/0d16c3a3/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/16267">Feli Chico</a>			</h4>
			<p>Con indignación. <span class="muted">Hace <b>1 semana</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/como-bajado-calidad-atencion-cliente-compre-samsung_c9029" data-action="expand"><p>Como ha bajado la calidad de la atencion al cliente. Me compre un Samsung Galasy S5 y me dijeron que tenia una seguro gratuito de 3 meses. Tuve la mala suerte de que me robaran el movil a la semana...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/como-bajado-calidad-atencion-cliente-compre-samsung_c9029" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						6096
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/como-bajado-calidad-atencion-cliente-compre-samsung_c9029" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							13
						</span>
					</a>
				</span>
					

									<span data-toggle="tooltip" title="Veces compartida">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/como-bajado-calidad-atencion-cliente-compre-samsung_c9029" data-action="expand">
						<i class="glyphicon glyphicon-share"></i>
						<span class="js-share-counter " data-single="" data-plural="">
							3
						</span>
					</a>
				</span>
						<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="9029">+<span class="plusone-num">5</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/como-bajado-calidad-atencion-cliente-compre-samsung_c9029"
		   			   data-twitter-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="9029"
		   			   data-image="https://www.critizen.com/critizen/img/score/indignant.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="8775"
         data-critic-item="690"
         data-critic-score="furious"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  furious">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/7465"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/c0c0d4d6/9f630207/4a58e4d5/0f26a644/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/7465">Belén Reina</a>			</h4>
			<p>Con furia. <span class="muted">Hace <b>1 semana</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/corte-ingles-estan-asquerosos-tanto-tienda-hiper_c8775" data-action="expand"><p>los wc de corte inglés están asquerosos tanto tienda, hiper,  open si necesitan más personal de limpieza que los ponga que ganan mucho dinero puede ser culpa del cliente x no cuidar los baños pero ...</p></a>
							<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/corte-ingles-estan-asquerosos-tanto-tienda-hiper_c8775" data-action="expand"><img src="https://d138d6gvpe36jl.cloudfront.net/critic/b620994a/e4090afd/32c5a1e2/d167fce5/400.jpg"></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/corte-ingles-estan-asquerosos-tanto-tienda-hiper_c8775" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						7317
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/corte-ingles-estan-asquerosos-tanto-tienda-hiper_c8775" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							17
						</span>
					</a>
				</span>
					

									<span data-toggle="tooltip" title="Veces compartida">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/corte-ingles-estan-asquerosos-tanto-tienda-hiper_c8775" data-action="expand">
						<i class="glyphicon glyphicon-share"></i>
						<span class="js-share-counter " data-single="" data-plural="">
							1
						</span>
					</a>
				</span>
						<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="8775">+<span class="plusone-num">13</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/corte-ingles-estan-asquerosos-tanto-tienda-hiper_c8775"
		   			   data-twitter-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="8775"
		   			   data-image="https://d138d6gvpe36jl.cloudfront.net/critic/b620994a/e4090afd/32c5a1e2/d167fce5/550.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="8732"
         data-critic-item="690"
         data-critic-score="indignant"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  indignant">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/4355"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/7aca23b0/015f447c/f45a16a5/50daedb9/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/4355">Jose GP</a>			</h4>
			<p>Con indignación. <span class="muted">Hace <b>1 semana</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/critica-seccion-telefonia-movil-cualquier-corte_c8732" data-action="expand"><p>Mi critica va a la sección de telefonia móvil de cualquier corte ingles o su web, es una vergüenza al precio que tienen los terminales, les ponen su precio de salida o incluso mas elevado que los q...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/critica-seccion-telefonia-movil-cualquier-corte_c8732" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						6531
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/critica-seccion-telefonia-movil-cualquier-corte_c8732" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							6
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="8732">+<span class="plusone-num">3</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/critica-seccion-telefonia-movil-cualquier-corte_c8732"
		   			   data-twitter-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="8732"
		   			   data-image="https://www.critizen.com/critizen/img/score/indignant.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="8701"
         data-critic-item="690"
         data-critic-score="furious"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  furious">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/15533"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/88dad0b0/b170ebfc/62e4261e/446e917b/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/15533">MARNAV DAVE</a>			</h4>
			<p>Con furia. <span class="muted">Hace <b>1 semana</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/hace-meses-compre-lavavajillas-corte-ingles-avda_c8701" data-action="expand"><p>Hace 18 meses compré un lavavajillas en El Corte Inglés Avda Libertad Murcia, por eso de cómpralo allí que no hay problemas.<br />
No me bastó con comprarlo además fui tan primo que contraté con Seguros ...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/hace-meses-compre-lavavajillas-corte-ingles-avda_c8701" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						6426
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/hace-meses-compre-lavavajillas-corte-ingles-avda_c8701" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							6
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="8701">+<span class="plusone-num">9</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/hace-meses-compre-lavavajillas-corte-ingles-avda_c8701"
		   			   data-twitter-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="8701"
		   			   data-image="https://www.critizen.com/critizen/img/score/furious.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="8379"
         data-critic-item="690"
         data-critic-score="indignant"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  indignant">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/12470"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/3e8c36e8/597d05e6/a932cf80/de533478/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/12470">Spike Spikeson</a>			</h4>
			<p>Con indignación. <span class="muted">Hace <b>2 semanas</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/meto-pagina-corte-ingles-pone-abre-1100-2100-espero_c8379" data-action="expand"><p>Si me meto en la página de El Corte Inglés y pone que HOY abre de 11:00 a 21:00 espero llegar al Corte Inglés y que no esté cerrado como me ha pasado. </p></a>
							<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/meto-pagina-corte-ingles-pone-abre-1100-2100-espero_c8379" data-action="expand"><img src="https://d138d6gvpe36jl.cloudfront.net/critic/8f34e755/70207dbd/a0f4bb80/83c12081/400.jpg"></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/meto-pagina-corte-ingles-pone-abre-1100-2100-espero_c8379" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						9457
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/meto-pagina-corte-ingles-pone-abre-1100-2100-espero_c8379" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							13
						</span>
					</a>
				</span>
					

									<span data-toggle="tooltip" title="Veces compartida">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/meto-pagina-corte-ingles-pone-abre-1100-2100-espero_c8379" data-action="expand">
						<i class="glyphicon glyphicon-share"></i>
						<span class="js-share-counter " data-single="" data-plural="">
							1
						</span>
					</a>
				</span>
						<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="8379">+<span class="plusone-num">9</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/meto-pagina-corte-ingles-pone-abre-1100-2100-espero_c8379"
		   			   data-twitter-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="8379"
		   			   data-image="https://d138d6gvpe36jl.cloudfront.net/critic/8f34e755/70207dbd/a0f4bb80/83c12081/550.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="7922"
         data-critic-item="690"
         data-critic-score="indignant"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  indignant">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/14190"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/cef8c8fa/e28c92b2/00b99349/6578e806/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/14190">Miguel Angel Olmedo</a>			</h4>
			<p>Con indignación. <span class="muted">Hace <b>2 semanas</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/critica-para-corte-ingles-solicitado-producto_c7922" data-action="expand"><p>Mi crítica va para El Corte Inglés,  he solicitado un producto a credito cuyo valor aproximado son de 1500 euros, y aún teniendo la tarjeta de compras de dicho establecimiento y depues de hacerme e...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/critica-para-corte-ingles-solicitado-producto_c7922" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						6346
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/critica-para-corte-ingles-solicitado-producto_c7922" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							7
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="7922">+<span class="plusone-num">5</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/critica-para-corte-ingles-solicitado-producto_c7922"
		   			   data-twitter-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="7922"
		   			   data-image="https://www.critizen.com/critizen/img/score/indignant.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="7856"
         data-critic-item="690"
         data-critic-score="furious"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  furious">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/7465"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/c0c0d4d6/9f630207/4a58e4d5/0f26a644/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/7465">Belén Reina</a>			</h4>
			<p>Con furia. <span class="muted">Hace <b>2 semanas</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/corte-ingles-1245-avda-andalucia-malaga-estan_c7856" data-action="expand"><p>los wc de corte inglés 12:45 avda Andalucía malaga están muy sucios contratad más personal de limpieza. Racanos! !!</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/corte-ingles-1245-avda-andalucia-malaga-estan_c7856" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						5462
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/corte-ingles-1245-avda-andalucia-malaga-estan_c7856" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							6
						</span>
					</a>
				</span>
					

									<span data-toggle="tooltip" title="Veces compartida">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/corte-ingles-1245-avda-andalucia-malaga-estan_c7856" data-action="expand">
						<i class="glyphicon glyphicon-share"></i>
						<span class="js-share-counter " data-single="" data-plural="">
							1
						</span>
					</a>
				</span>
						<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="7856">+<span class="plusone-num">4</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/corte-ingles-1245-avda-andalucia-malaga-estan_c7856"
		   			   data-twitter-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="7856"
		   			   data-image="https://www.critizen.com/critizen/img/score/furious.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="6861"
         data-critic-item="690"
         data-critic-score="indignant"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  indignant">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/13148"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/f24d5735/de3fd395/136e3abd/ebc27ed4/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/13148">Tomas Garcia</a>			</h4>
			<p>Con indignación. <span class="muted">Hace <b>4 semanas</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compran-bicicletas-orbea-pagan-targeta-1500-plazo-dias_c6861" data-action="expand"><p>Se compran 3 bicicletas orbea y se pagan con targeta 1500 ĕ. Nos dan un plazo de 15 dias maximo. pues bien, llamamon al dia 18 xq no llamaban. Al final estaban cobradas xo no pedidas. Un mes despue...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compran-bicicletas-orbea-pagan-targeta-1500-plazo-dias_c6861" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						8146
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compran-bicicletas-orbea-pagan-targeta-1500-plazo-dias_c6861" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							4
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="6861">+<span class="plusone-num">4</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compran-bicicletas-orbea-pagan-targeta-1500-plazo-dias_c6861"
		   			   data-twitter-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="6861"
		   			   data-image="https://www.critizen.com/critizen/img/score/indignant.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="6759"
         data-critic-item="690"
         data-critic-score="furious"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  furious">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/12945"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/b9b7cef4/e7ee954b/22249858/6b9f9d0d/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/12945">Monica Martinez Fernandez</a>			</h4>
			<p>Con furia. <span class="muted">Hace <b>4 semanas</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/este-sabado-pasado-dia-enero-realice-pedido-barra_c6759" data-action="expand"><p>Éste sábado pasado día 24 de enero realicé un pedido de una barra de sonido en la web del corte inglés la cual estaba disponible, y ya somos jueves 29 y en la web todavía pone "pendiente de prepara...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/este-sabado-pasado-dia-enero-realice-pedido-barra_c6759" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						7115
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/este-sabado-pasado-dia-enero-realice-pedido-barra_c6759" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							3
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="6759">+<span class="plusone-num">4</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/este-sabado-pasado-dia-enero-realice-pedido-barra_c6759"
		   			   data-twitter-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="6759"
		   			   data-image="https://www.critizen.com/critizen/img/score/furious.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="6578"
         data-critic-item="690"
         data-critic-score="frustrated"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  frustrated">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/9077"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/c538f014/2427e34d/24102628/34514ca6/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/9077">Antonio Portell Ferrer</a>			</h4>
			<p>Con frustración. <span class="muted">Hace <b>1 mes</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/cada-dia-atienden-peor-menos-amables-palma-mallorca_c6578" data-action="expand"><p>De cada día atienden peor y son menos amables. En palma de Mallorca avenidas <br />
</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/cada-dia-atienden-peor-menos-amables-palma-mallorca_c6578" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						7782
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/cada-dia-atienden-peor-menos-amables-palma-mallorca_c6578" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							4
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="6578">+<span class="plusone-num">7</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/cada-dia-atienden-peor-menos-amables-palma-mallorca_c6578"
		   			   data-twitter-title="Otro que está #czfrustrado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfrustrado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="6578"
		   			   data-image="https://www.critizen.com/critizen/img/score/frustrated.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="6467"
         data-critic-item="690"
         data-critic-score="furious"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  furious">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/4778"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/ef821fa0/67ed36f4/b5eefb71/c1eb665f/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/4778">Jorge Costas Reigosa</a>			</h4>
			<p>Con furia. <span class="muted">Hace <b>1 mes</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/tengo-punetera-tarjeta-regalo-este-sitio-valor-euros_c6467" data-action="expand"><p>Tengo una puñetera tarjeta de regalo de este sitio por valor de 150 euros. Antes era de 200, pero tuve que comprar un par de cosas para no sentirme tan mal de tirar a la basura 200 puñeteros euros....</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/tengo-punetera-tarjeta-regalo-este-sitio-valor-euros_c6467" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						10883
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/tengo-punetera-tarjeta-regalo-este-sitio-valor-euros_c6467" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							23
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="6467">+<span class="plusone-num">1</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/tengo-punetera-tarjeta-regalo-este-sitio-valor-euros_c6467"
		   			   data-twitter-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="6467"
		   			   data-image="https://www.critizen.com/critizen/img/score/furious.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="6438"
         data-critic-item="690"
         data-critic-score="furious"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  furious">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/9518"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/9233351a/405482f7/0458f7d5/59625ff1/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/9518">Vanessa Costa</a>			</h4>
			<p>Con furia. <span class="muted">Hace <b>1 mes</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/tienen-verguenza-tengo-anos-tarjeta-gastando-reparo_c6438" data-action="expand"><p>No tienen vergüenza, tengo 5 años con la tarjeta gastando sin reparo y pagando a fin de mes el total integro siempre sin problemas y cuando pido una financiación para un portátil me salen con un do...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/tienen-verguenza-tengo-anos-tarjeta-gastando-reparo_c6438" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						9881
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/tienen-verguenza-tengo-anos-tarjeta-gastando-reparo_c6438" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							8
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="6438">+<span class="plusone-num">3</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/tienen-verguenza-tengo-anos-tarjeta-gastando-reparo_c6438"
		   			   data-twitter-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="6438"
		   			   data-image="https://www.critizen.com/critizen/img/score/furious.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="6393"
         data-critic-item="690"
         data-critic-score="frustrated"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  frustrated">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/877"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/8778753f/eefbd52c/993ededf/bf615320/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/877">Marcos Polo</a>			</h4>
			<p>Con frustración. <span class="muted">Hace <b>1 mes</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compro-online-pagina-compra-como-efectuada-embargo_c6393" data-action="expand"><p>Compro online una TV 4k y la pagina me da la compra como efectuada. Sin embargo veo  que no me han realizado cargo. Les aviso y me piden que pague de otra forma. Les digo que les hago una transfere...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compro-online-pagina-compra-como-efectuada-embargo_c6393" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						11229
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compro-online-pagina-compra-como-efectuada-embargo_c6393" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							3
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="6393">+<span class="plusone-num">2</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compro-online-pagina-compra-como-efectuada-embargo_c6393"
		   			   data-twitter-title="Otro que está #czfrustrado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfrustrado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="6393"
		   			   data-image="https://www.critizen.com/critizen/img/score/frustrated.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="6374"
         data-critic-item="690"
         data-critic-score="indignant"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  indignant">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/12445"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/18e09367/7bc50445/a4ec4bb2/3cc019ba/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/12445">Amparo Ballve</a>			</h4>
			<p>Con indignación. <span class="muted">Hace <b>1 mes</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/encargada-camper-falte-profesionalidad-respeto-hacia_c6374" data-action="expand"><p>que la encargada de camper le falte profesionalidad y respeto hacia sus compañeros.  lamentable que gente así con tanto egocentrismo, soberbia, chuleria  barata siga trabajando en una empresa como ...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/encargada-camper-falte-profesionalidad-respeto-hacia_c6374" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						11343
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/encargada-camper-falte-profesionalidad-respeto-hacia_c6374" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							3
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="6374">+<span class="plusone-num">4</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/encargada-camper-falte-profesionalidad-respeto-hacia_c6374"
		   			   data-twitter-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="6374"
		   			   data-image="https://www.critizen.com/critizen/img/score/indignant.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="6325"
         data-critic-item="690"
         data-critic-score="happy"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  happy">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/12319"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/f8bb0b54/f558a2c6/a3ce444b/1152eb22/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/12319">Diego DM</a>			</h4>
			<p>Con felicidad. <span class="muted">Hace <b>1 mes</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/quizas-mas-caro-pero-nunca-tenido-ningun-problema_c6325" data-action="expand"><p>Quizás más caro, pero nunca he tenido ningún problema con ellos. Buen trato y devolución sin problemas en la mayoría de los productos.</p></a>
							<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/quizas-mas-caro-pero-nunca-tenido-ningun-problema_c6325" data-action="expand"><img src="https://d138d6gvpe36jl.cloudfront.net/critic/24467d40/99f83cd5/17d815d1/f3bdcd3d/400.jpg"></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/quizas-mas-caro-pero-nunca-tenido-ningun-problema_c6325" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						11286
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/quizas-mas-caro-pero-nunca-tenido-ningun-problema_c6325" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							9
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="6325">+<span class="plusone-num">15</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/quizas-mas-caro-pero-nunca-tenido-ningun-problema_c6325"
		   			   data-twitter-title="Otro que está #czfeliz con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfeliz con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="6325"
		   			   data-image="https://d138d6gvpe36jl.cloudfront.net/critic/24467d40/99f83cd5/17d815d1/f3bdcd3d/550.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="6308"
         data-critic-item="690"
         data-critic-score="disappointed"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  disappointed">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/7773"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/247a686a/dc8e8db8/a05591b6/5dad82b9/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/7773">Freixa Gutiérrez</a>			</h4>
			<p>Con disgusto. <span class="muted">Hace <b>1 mes</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/centro-esta-bien-pero-podrian-engrasar-puertas_c6308" data-action="expand"><p>El centro está muy bien pero podrían engrasar las puertas del parking q chirrían demasiado. Aun asi buen servicio de aparcamiento. Saludos.</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/centro-esta-bien-pero-podrian-engrasar-puertas_c6308" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						9403
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/centro-esta-bien-pero-podrian-engrasar-puertas_c6308" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							5
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="6308">+<span class="plusone-num">1</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/centro-esta-bien-pero-podrian-engrasar-puertas_c6308"
		   			   data-twitter-title="Otro que está #czdisgustado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czdisgustado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="6308"
		   			   data-image="https://www.critizen.com/critizen/img/score/disappointed.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="5931"
         data-critic-item="690"
         data-critic-score="indignant"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  indignant">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/8217"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/d43eab38/7c815380/02bc40a3/1d831b78/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/8217">Jorge Olmos Tomás</a>			</h4>
			<p>Con indignación. <span class="muted">Hace <b>1 mes</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compramos-television-samsung-smartv-hace-semana-dias_c5931" data-action="expand"><p>Nos compramos una televisión Samsung SmarTV hace una semana y a los dos dias de recibirla nos damos cuenta de que el mando tiene una parte medio rota y cada vez se va rompiendo mas. llamo para come...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compramos-television-samsung-smartv-hace-semana-dias_c5931" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						9874
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compramos-television-samsung-smartv-hace-semana-dias_c5931" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							2
						</span>
					</a>
				</span>
					

									<span data-toggle="tooltip" title="Veces compartida">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compramos-television-samsung-smartv-hace-semana-dias_c5931" data-action="expand">
						<i class="glyphicon glyphicon-share"></i>
						<span class="js-share-counter " data-single="" data-plural="">
							1
						</span>
					</a>
				</span>
						<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="5931">+<span class="plusone-num">4</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compramos-television-samsung-smartv-hace-semana-dias_c5931"
		   			   data-twitter-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="5931"
		   			   data-image="https://www.critizen.com/critizen/img/score/indignant.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="5770"
         data-critic-item="690"
         data-critic-score="furious"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  furious">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/11543"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/4592e09b/b1ba7906/de05081e/3d954895/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/11543">Christelle Baum</a>			</h4>
			<p>Con furia. <span class="muted">Hace <b>1 mes</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/acabo-outlet-corte-ingles-centro-comercial-nassica_c5770" data-action="expand"><p>Acabo de ir al outlet del Corte Inglés en el centro comercial Nassica. Mi maridó coge unos pantalones etiquetados a 19€ con además un 30% de descuento. Una ganga!!! Al pasar por caja nos dicen que ...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/acabo-outlet-corte-ingles-centro-comercial-nassica_c5770" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						11105
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/acabo-outlet-corte-ingles-centro-comercial-nassica_c5770" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							9
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="5770">+<span class="plusone-num">2</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/acabo-outlet-corte-ingles-centro-comercial-nassica_c5770"
		   			   data-twitter-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="5770"
		   			   data-image="https://www.critizen.com/critizen/img/score/furious.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="5392"
         data-critic-item="690"
         data-critic-score="furious"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  furious">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/8320"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/acb5784c/0bdc44b2/6fef5225/d47d6a0a/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/8320">Alexandra Palmieri Di Iuro</a>			</h4>
			<p>Con furia. <span class="muted">Hace <b>1 mes</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/enganosa-hice-pedido-porque-dice-envio-horas_c5392" data-action="expand"><p>Una web ENGAÑOSA. Hice un pedido porque la web dice "envío en 48 horas". transcurrida una semana, me notifican por correo que el pedido llegará en 2 semanas. Es decir, 3 semanas de espera. Irrespet...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/enganosa-hice-pedido-porque-dice-envio-horas_c5392" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						10663
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/enganosa-hice-pedido-porque-dice-envio-horas_c5392" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							3
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="5392">+<span class="plusone-num">5</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/enganosa-hice-pedido-porque-dice-envio-horas_c5392"
		   			   data-twitter-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="5392"
		   			   data-image="https://www.critizen.com/critizen/img/score/furious.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="4928"
         data-critic-item="690"
         data-critic-score="disappointed"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  disappointed">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/7225"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/44a3dac4/92b49600/93911743/fc226812/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/7225">Nacho Martin</a>			</h4>
			<p>Con disgusto. <span class="muted">Hace <b>1 mes</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/tenido-problema-cargador-iphone-antes-aceptar-otra_c4928" data-action="expand"><p>He tenido un problema con el cargador del iPhone 5 y antes de aceptar otra imitacion china he decidido preguntar directamente por el complemento oficial de Apple en El Corte Inglés. Resulta casi ch...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/tenido-problema-cargador-iphone-antes-aceptar-otra_c4928" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						4185
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/tenido-problema-cargador-iphone-antes-aceptar-otra_c4928" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							3
						</span>
					</a>
				</span>
					

									<span data-toggle="tooltip" title="Veces compartida">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/tenido-problema-cargador-iphone-antes-aceptar-otra_c4928" data-action="expand">
						<i class="glyphicon glyphicon-share"></i>
						<span class="js-share-counter " data-single="" data-plural="">
							1
						</span>
					</a>
				</span>
						<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="4928">+<span class="plusone-num">7</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/tenido-problema-cargador-iphone-antes-aceptar-otra_c4928"
		   			   data-twitter-title="Otro que está #czdisgustado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czdisgustado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="4928"
		   			   data-image="https://www.critizen.com/critizen/img/score/disappointed.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="4658"
         data-critic-item="690"
         data-critic-score="furious"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  furious">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/1421"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/5ea4538b/ca8314d2/63a2d85d/592729b7/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/1421">Samara Alonso Rubiales</a>			</h4>
			<p>Con furia. <span class="muted">Hace <b>1 mes</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/dia-diciembre-compre-iphone-dijeron-momento-habia_c4658" data-action="expand"><p>El día 29 de diciembre compre un iPhone 6 de 16 gb, me dijeron que en ese momento no había y tendrían que encargarlo. Han pasado más de 3 semanas y sigo sin movil cuando ya lo he pagado. Encima me ...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/dia-diciembre-compre-iphone-dijeron-momento-habia_c4658" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						4241
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/dia-diciembre-compre-iphone-dijeron-momento-habia_c4658" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							10
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="4658">+<span class="plusone-num">4</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/dia-diciembre-compre-iphone-dijeron-momento-habia_c4658"
		   			   data-twitter-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="4658"
		   			   data-image="https://www.critizen.com/critizen/img/score/furious.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="4542"
         data-critic-item="690"
         data-critic-score="indignant"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  indignant">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/7219"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/e11669db/f760e3de/cbef9c3c/09acf1c0/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/7219">Miguel Weil Di Miele</a>			</h4>
			<p>Con indignación. <span class="muted">Hace <b>1 mes</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/ademas-haber-enviado-pedido-luego-dias-cuando_c4542" data-action="expand"><p>Además de haber enviado el pedido luego de 7 días cuando prometían la entrega en 48 horas, lo han enviado sin factura. A la hora de hacer un cambio en la tienda, el personal con muy mal trato se ha...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/ademas-haber-enviado-pedido-luego-dias-cuando_c4542" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						3830
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/ademas-haber-enviado-pedido-luego-dias-cuando_c4542" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							1
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="4542">+<span class="plusone-num">2</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/ademas-haber-enviado-pedido-luego-dias-cuando_c4542"
		   			   data-twitter-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="4542"
		   			   data-image="https://www.critizen.com/critizen/img/score/indignant.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="4536"
         data-critic-item="690"
         data-critic-score="frustrated"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  frustrated">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/5731"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/6208b127/e320b2fa/2b0454ae/044dba8a/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/5731">Sergio Rodríguez</a>			</h4>
			<p>Con frustración. <span class="muted">Hace <b>1 mes</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/trato-pesimo-corte-ingles-avenida-francia-valencia_c4536" data-action="expand"><p>Trato pésimo en el Corte Inglés Avenida Francia de Valencia sección informática. Primero un señor me dice que me atiende y se va sin decir nada y no vuelve. Después otro de ellos se dirige a mi con...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/trato-pesimo-corte-ingles-avenida-francia-valencia_c4536" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						3907
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/trato-pesimo-corte-ingles-avenida-francia-valencia_c4536" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							3
						</span>
					</a>
				</span>
					

									<span data-toggle="tooltip" title="Veces compartida">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/trato-pesimo-corte-ingles-avenida-francia-valencia_c4536" data-action="expand">
						<i class="glyphicon glyphicon-share"></i>
						<span class="js-share-counter " data-single="" data-plural="">
							2
						</span>
					</a>
				</span>
						<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="4536">+<span class="plusone-num">6</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/trato-pesimo-corte-ingles-avenida-francia-valencia_c4536"
		   			   data-twitter-title="Otro que está #czfrustrado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfrustrado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="4536"
		   			   data-image="https://www.critizen.com/critizen/img/score/frustrated.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="4367"
         data-critic-item="690"
         data-critic-score="indignant"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  indignant">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/4649"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/3ee4bc8a/5b0e262d/0a75647b/cd1e9159/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/4649">Marina Orihuel Pechuán</a>			</h4>
			<p>Con indignación. <span class="muted">Hace <b>1 mes</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/estoy-indignada-agencia-viajes-corte-ingles-hemos_c4367" data-action="expand"><p>Estoy indignada con la Agencia de Viajes del Corte Inglés. Hemos ido mi novio y yo en dos ocasiones para que nos organizaran el viaje de novios. Pasaron de nosotros las dos veces. La segunda vez no...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/estoy-indignada-agencia-viajes-corte-ingles-hemos_c4367" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						4037
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/estoy-indignada-agencia-viajes-corte-ingles-hemos_c4367" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							4
						</span>
					</a>
				</span>
					

									<span data-toggle="tooltip" title="Veces compartida">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/estoy-indignada-agencia-viajes-corte-ingles-hemos_c4367" data-action="expand">
						<i class="glyphicon glyphicon-share"></i>
						<span class="js-share-counter " data-single="" data-plural="">
							1
						</span>
					</a>
				</span>
						<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="4367">+<span class="plusone-num">3</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/estoy-indignada-agencia-viajes-corte-ingles-hemos_c4367"
		   			   data-twitter-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="4367"
		   			   data-image="https://www.critizen.com/critizen/img/score/indignant.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="4210"
         data-critic-item="690"
         data-critic-score="furious"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  furious">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/6945"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/c3d2a56f/08db622e/ac7b01c7/25bd1d55/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/6945">Ôkami No Ki</a>			</h4>
			<p>Con furia. <span class="muted">Hace <b>1 mes</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/navidad-compre-boligrafo-calendario-pequeno-mesa_c4210" data-action="expand"><p>En navidad compre un bolígrafo, un calendario pequeño de mesa y unos lápices de su propia marca. en total me costaba 8 euros. Me intentaron cobrar 18. un puto timo hombre</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/navidad-compre-boligrafo-calendario-pequeno-mesa_c4210" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						4023
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/navidad-compre-boligrafo-calendario-pequeno-mesa_c4210" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							6
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="4210">+<span class="plusone-num">2</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/navidad-compre-boligrafo-calendario-pequeno-mesa_c4210"
		   			   data-twitter-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="4210"
		   			   data-image="https://www.critizen.com/critizen/img/score/furious.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="4145"
         data-critic-item="690"
         data-critic-score="frustrated"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  frustrated">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/7465"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/c0c0d4d6/9f630207/4a58e4d5/0f26a644/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/7465">Belén Reina</a>			</h4>
			<p>Con frustración. <span class="muted">Hace <b>1 mes</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/habeis-entrado-probador-ci-madre-mia-parece-tienda_c4145" data-action="expand"><p>habéis entrado en un probador del c.i? madre mía parece que la tienda está en el probador que desastre!  y cada vez con menos dependientas avd Andalucía</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/habeis-entrado-probador-ci-madre-mia-parece-tienda_c4145" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						2340
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/habeis-entrado-probador-ci-madre-mia-parece-tienda_c4145" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							2
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="4145">+<span class="plusone-num">1</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/habeis-entrado-probador-ci-madre-mia-parece-tienda_c4145"
		   			   data-twitter-title="Otro que está #czfrustrado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfrustrado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="4145"
		   			   data-image="https://www.critizen.com/critizen/img/score/frustrated.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="3993"
         data-critic-item="690"
         data-critic-score="indignant"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  indignant">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/8450"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/91b385a2/0a019329/aca86b5b/ae044b42/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/8450">Elena Covadonga Martín Álvarez</a>			</h4>
			<p>Con indignación. <span class="muted">Hace <b>1 mes</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/hace-ano-corte-ingles-calle-preciados-madrid-pleno_c3993" data-action="expand"><p>hace un año fui al corte inglés de la calle Preciados en Madrid, en pleno centro. Fui con mi familia,de EEUU que no sabe español y querían comprar unas deportivas para sus hijos. Absolutamente nadi...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/hace-ano-corte-ingles-calle-preciados-madrid-pleno_c3993" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						4194
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/hace-ano-corte-ingles-calle-preciados-madrid-pleno_c3993" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							13
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="3993">+<span class="plusone-num">6</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/hace-ano-corte-ingles-calle-preciados-madrid-pleno_c3993"
		   			   data-twitter-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="3993"
		   			   data-image="https://www.critizen.com/critizen/img/score/indignant.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="3742"
         data-critic-item="690"
         data-critic-score="furious"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  furious">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/8027"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/5a1493d3/9215a7e6/6f00e402/108b3e8b/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/8027">Esteban Urrea</a>			</h4>
			<p>Con furia. <span class="muted">Hace <b>1 mes</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/malditos-ladrones-burlan-nosotros-nuestra-cara_c3742" data-action="expand"><p>Malditos Ladrones!! Se burlan de nosotros en nuestra cara. Fui a comprar un chocolate, y la sorpresa es que el precio de 1 tableta es de €1.77 y al lado un pack por dos de esas tabletas, con imagen...</p></a>
							<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/malditos-ladrones-burlan-nosotros-nuestra-cara_c3742" data-action="expand"><img src="https://d138d6gvpe36jl.cloudfront.net/critic/03d4070e/f9bf521a/138582b7/3f90aa16/400.jpg"></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/malditos-ladrones-burlan-nosotros-nuestra-cara_c3742" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						4274
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/malditos-ladrones-burlan-nosotros-nuestra-cara_c3742" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							9
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="3742">+<span class="plusone-num">12</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/malditos-ladrones-burlan-nosotros-nuestra-cara_c3742"
		   			   data-twitter-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="3742"
		   			   data-image="https://d138d6gvpe36jl.cloudfront.net/critic/03d4070e/f9bf521a/138582b7/3f90aa16/550.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="3513"
         data-critic-item="690"
         data-critic-score="frustrated"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  frustrated">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/7676"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/b5fa5bef/2c57fdf1/ced5e4d0/5cbfa954/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/7676">Marta CCh</a>			</h4>
			<p>Con frustración. <span class="muted">Hace <b>1 mes</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/mucha-publicidad-mereces-pues-fui-comprar-gorro_c3513" data-action="expand"><p>Mucha publicidad,:  "te lo mereces", pues yo fuí a comprar un gorro de punto, de la temporada, y no tenía rebaja de ningún tipo!!!!......y encima, me ponen una bolsa que ponía rebajas,   ...o sea, ...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/mucha-publicidad-mereces-pues-fui-comprar-gorro_c3513" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						3731
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/mucha-publicidad-mereces-pues-fui-comprar-gorro_c3513" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							6
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="3513">+<span class="plusone-num">1</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/mucha-publicidad-mereces-pues-fui-comprar-gorro_c3513"
		   			   data-twitter-title="Otro que está #czfrustrado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfrustrado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="3513"
		   			   data-image="https://www.critizen.com/critizen/img/score/frustrated.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="3304"
         data-critic-item="690"
         data-critic-score="happy"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  happy">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/7298"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/585b997e/30284a69/429fdc85/192a4558/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/7298">David Quevedo</a>			</h4>
			<p>Con felicidad. <span class="muted">Hace <b>1 mes</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/navidad-compre-jamon-salio-seco-salado-despues_c3304" data-action="expand"><p>en navidad compre un jamon y salio seco y salado despues de estrenarlo en nochebuena lo lleve y m lo cambiaron o m devolvian en dinero sin problemas. sin descontar lo que habia comido ni pesarlo ni...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/navidad-compre-jamon-salio-seco-salado-despues_c3304" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						3996
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/navidad-compre-jamon-salio-seco-salado-despues_c3304" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							1
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="3304">+<span class="plusone-num">10</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/navidad-compre-jamon-salio-seco-salado-despues_c3304"
		   			   data-twitter-title="Otro que está #czfeliz con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfeliz con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="3304"
		   			   data-image="https://www.critizen.com/critizen/img/score/happy.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="3253"
         data-critic-item="690"
         data-critic-score="frustrated"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  frustrated">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/7219"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/e11669db/f760e3de/cbef9c3c/09acf1c0/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/7219">Miguel Weil Di Miele</a>			</h4>
			<p>Con frustración. <span class="muted">Hace <b>1 mes</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compre-hace-semana-abrigo-corte-ingles-prometia_c3253" data-action="expand"><p>Compré hace una semana un abrigo en la web del Corte Inglés que prometía reparto en 48 horas. Ha pasado una semana y nada. Luego no se explican por qué caen sus ventas. </p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compre-hace-semana-abrigo-corte-ingles-prometia_c3253" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						3715
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compre-hace-semana-abrigo-corte-ingles-prometia_c3253" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							2
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="3253">+<span class="plusone-num">3</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compre-hace-semana-abrigo-corte-ingles-prometia_c3253"
		   			   data-twitter-title="Otro que está #czfrustrado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfrustrado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="3253"
		   			   data-image="https://www.critizen.com/critizen/img/score/frustrated.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="3176"
         data-critic-item="690"
         data-critic-score="disappointed"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  disappointed">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/6490"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/d815536d/5cb0199e/96a176b0/33cdf020/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/6490">Lucas Sintes</a>			</h4>
			<p>Con disgusto. <span class="muted">Hace <b>1 mes</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/repugna-chica-anuncios-radio-corte-ingles-dios-mio_c3176" data-action="expand"><p>Me repugna la voz de la chica de los anuncios de radio de El Corte Inglés,  dios mío,  que molesta es!!!!</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/repugna-chica-anuncios-radio-corte-ingles-dios-mio_c3176" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						3903
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/repugna-chica-anuncios-radio-corte-ingles-dios-mio_c3176" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							4
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="3176">+<span class="plusone-num">5</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/repugna-chica-anuncios-radio-corte-ingles-dios-mio_c3176"
		   			   data-twitter-title="Otro que está #czdisgustado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czdisgustado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="3176"
		   			   data-image="https://www.critizen.com/critizen/img/score/disappointed.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="2935"
         data-critic-item="690"
         data-critic-score="furious"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  furious">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/6600"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/0121f0db/cd827295/037836bf/0c3b1d84/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/6600">Oscar Gonzalez Balboa</a>			</h4>
			<p>Con furia. <span class="muted">Hace <b>2 meses</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/contrate-seguro-para-ipad-este-centro-tener-percance_c2935" data-action="expand"><p>Contrate un seguro para mi IPad en este centro y al tener un percance, me ponen piezas de malísima calidad y encima me roban el cargador original dándome uno chino... 2 meses después de mandárselo ...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/contrate-seguro-para-ipad-este-centro-tener-percance_c2935" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						3919
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/contrate-seguro-para-ipad-este-centro-tener-percance_c2935" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							6
						</span>
					</a>
				</span>
					

									<span data-toggle="tooltip" title="Veces compartida">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/contrate-seguro-para-ipad-este-centro-tener-percance_c2935" data-action="expand">
						<i class="glyphicon glyphicon-share"></i>
						<span class="js-share-counter " data-single="" data-plural="">
							2
						</span>
					</a>
				</span>
						<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="2935">+<span class="plusone-num">5</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/contrate-seguro-para-ipad-este-centro-tener-percance_c2935"
		   			   data-twitter-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="2935"
		   			   data-image="https://www.critizen.com/critizen/img/score/furious.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="2907"
         data-critic-item="690"
         data-critic-score="indignant"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  indignant">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/6552"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/714be8a7/69d34120/4f09d22e/df8981be/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/6552">Lucía González Nieto</a>			</h4>
			<p>Con indignación. <span class="muted">Hace <b>2 meses</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/hace-meses-palpa-descontento-empleados-este_c2907" data-action="expand"><p>Hace meses que se palpa el descontento de empleados de este establecimiento. No existe la misma atención que antes, y los empleados se quejan entre ellos de la empresa en presencia del cliente. ......</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/hace-meses-palpa-descontento-empleados-este_c2907" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						4068
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/hace-meses-palpa-descontento-empleados-este_c2907" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							8
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="2907">+<span class="plusone-num">16</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/hace-meses-palpa-descontento-empleados-este_c2907"
		   			   data-twitter-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="2907"
		   			   data-image="https://www.critizen.com/critizen/img/score/indignant.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="2890"
         data-critic-item="690"
         data-critic-score="indignant"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  indignant">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/2159"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/c3843813/59a663e3/3c2aa98c/4c23a11b/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/2159">Adrián </a>			</h4>
			<p>Con indignación. <span class="muted">Hace <b>2 meses</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compre-frasco-colonia-cuando-llego-cuenta-habia_c2890" data-action="expand"><p>Hoy compré un frasco de colonia y cuando llego , me doy cuenta de que lo había estrenado otra persona, voy a descambiarlo y no me lo descambian.</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compre-frasco-colonia-cuando-llego-cuenta-habia_c2890" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						2450
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compre-frasco-colonia-cuando-llego-cuenta-habia_c2890" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							4
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="2890">+<span class="plusone-num">4</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compre-frasco-colonia-cuando-llego-cuenta-habia_c2890"
		   			   data-twitter-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="2890"
		   			   data-image="https://www.critizen.com/critizen/img/score/indignant.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="2800"
         data-critic-item="690"
         data-critic-score="frustrated"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  frustrated">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/2133"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/5ab52355/ca2e7e47/6c7397fa/c7539522/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/2133">Zic Zac</a>			</h4>
			<p>Con frustración. <span class="muted">Hace <b>2 meses</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/sanchinarro-comprar-reproductor-dvd-queria-estaba_c2800" data-action="expand"><p>fui a uno en Sanchinarro a comprar un reproductor de dvd. el que quería estaba  a "agotado", me ofrecían el aparato que estaba en exposición. digo que no y me aseguran que en una semana (una semana...</p></a>
							<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/sanchinarro-comprar-reproductor-dvd-queria-estaba_c2800" data-action="expand"><img src="https://d138d6gvpe36jl.cloudfront.net/critic/d55ec07e/4c368a48/50f750ba/a1e601db/400.jpg"></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/sanchinarro-comprar-reproductor-dvd-queria-estaba_c2800" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						2462
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/sanchinarro-comprar-reproductor-dvd-queria-estaba_c2800" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							3
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="2800">+<span class="plusone-num">1</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/sanchinarro-comprar-reproductor-dvd-queria-estaba_c2800"
		   			   data-twitter-title="Otro que está #czfrustrado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfrustrado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="2800"
		   			   data-image="https://d138d6gvpe36jl.cloudfront.net/critic/d55ec07e/4c368a48/50f750ba/a1e601db/550.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="2692"
         data-critic-item="690"
         data-critic-score="furious"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  furious">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/4166"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/9b1407fc/578fc522/c238a748/c5d9377e/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/4166">Miguel Angel Serrano</a>			</h4>
			<p>Con furia. <span class="muted">Hace <b>2 meses</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/puesto-reclamacion-quiero-dinero-compre-bici-1800_c2692" data-action="expand"><p>He puesto la reclamación, quiero mi dinero, me compre la bici por 1800 euros y venía mal, el mecánico del otro corte inglés,me lo corrobora, las suspensiones rotas no eran las de los papeles me ven...</p></a>
							<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/puesto-reclamacion-quiero-dinero-compre-bici-1800_c2692" data-action="expand"><img src="https://d138d6gvpe36jl.cloudfront.net/critic/48a40f58/b223f72e/83261383/27554698/400.jpg"></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/puesto-reclamacion-quiero-dinero-compre-bici-1800_c2692" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						3827
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/puesto-reclamacion-quiero-dinero-compre-bici-1800_c2692" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							3
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="2692">+<span class="plusone-num">2</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/puesto-reclamacion-quiero-dinero-compre-bici-1800_c2692"
		   			   data-twitter-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="2692"
		   			   data-image="https://d138d6gvpe36jl.cloudfront.net/critic/48a40f58/b223f72e/83261383/27554698/550.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="2657"
         data-critic-item="690"
         data-critic-score="indignant"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  indignant">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/5981"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/eb988660/f9a7872e/a18d60c9/ff7859ca/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/5981">Toni Viloria</a>			</h4>
			<p>Con indignación. <span class="muted">Hace <b>2 meses</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/hace-chaqueta-marca-element-valia-9999eur-descuento_c2657" data-action="expand"><p>Hace un mes vi una chaqueta de la marca Element y valía 99.99€. Hoy con un 20% de descuento vale 104. Marcaba 130...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/hace-chaqueta-marca-element-valia-9999eur-descuento_c2657" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						3975
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/hace-chaqueta-marca-element-valia-9999eur-descuento_c2657" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							10
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="2657">+<span class="plusone-num">11</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/hace-chaqueta-marca-element-valia-9999eur-descuento_c2657"
		   			   data-twitter-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="2657"
		   			   data-image="https://www.critizen.com/critizen/img/score/indignant.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="2400"
         data-critic-item="690"
         data-critic-score="indignant"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  indignant">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/5405"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/39138c4c/ad83112f/39cc9edc/fa1644a0/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/5405">Alba Gimez</a>			</h4>
			<p>Con indignación. <span class="muted">Hace <b>2 meses</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/para-recibir-placa-induccion-comprada-internet_c2400" data-action="expand"><p>un mes para recibir una placa de induccion comprada por internet que verguenza y nimguna explicaciom</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/para-recibir-placa-induccion-comprada-internet_c2400" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						3629
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/para-recibir-placa-induccion-comprada-internet_c2400" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							2
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="2400">+<span class="plusone-num">2</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/para-recibir-placa-induccion-comprada-internet_c2400"
		   			   data-twitter-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="2400"
		   			   data-image="https://www.critizen.com/critizen/img/score/indignant.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="2300"
         data-critic-item="690"
         data-critic-score="happy"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  happy">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/3146"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/16408adc/90f7139b/ac6428bd/1b744cff/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/3146">Alain Valencia</a>			</h4>
			<p>Con felicidad. <span class="muted">Hace <b>2 meses</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/alguien-puede-decirme-seguro-corte-ingles-recomendable_c2300" data-action="expand"><p>¿Alguien puede decirme si el seguro de El Corte Inglés es recomendable? </p></a>
							<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/alguien-puede-decirme-seguro-corte-ingles-recomendable_c2300" data-action="expand"><img src="https://d138d6gvpe36jl.cloudfront.net/critic/c6df24f5/41473f3a/162b9254/d655d865/400.jpg"></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/alguien-puede-decirme-seguro-corte-ingles-recomendable_c2300" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						4047
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/alguien-puede-decirme-seguro-corte-ingles-recomendable_c2300" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							7
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="2300">+<span class="plusone-num">4</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/alguien-puede-decirme-seguro-corte-ingles-recomendable_c2300"
		   			   data-twitter-title="Otro que está #czfeliz con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfeliz con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="2300"
		   			   data-image="https://d138d6gvpe36jl.cloudfront.net/critic/c6df24f5/41473f3a/162b9254/d655d865/550.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="2288"
         data-critic-item="690"
         data-critic-score="indignant"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  indignant">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/5200"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/07e1df41/e07b5bab/b2abfb05/5af57285/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/5200">Jorge Gallardo Pérez</a>			</h4>
			<p>Con indignación. <span class="muted">Hace <b>2 meses</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/cafeteria-corte-ingles-deja-mucho-desear-camareros_c2288" data-action="expand"><p>La cafeteria del corte ingles de sol deja mucho que desear, los camareros estan amargados, el servicio es lento, llamas a un camarero y como quien oye llover tienes que ir detras de ellos, me sirvi...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/cafeteria-corte-ingles-deja-mucho-desear-camareros_c2288" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						3841
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/cafeteria-corte-ingles-deja-mucho-desear-camareros_c2288" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							1
						</span>
					</a>
				</span>
					

									<span data-toggle="tooltip" title="Veces compartida">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/cafeteria-corte-ingles-deja-mucho-desear-camareros_c2288" data-action="expand">
						<i class="glyphicon glyphicon-share"></i>
						<span class="js-share-counter " data-single="" data-plural="">
							1
						</span>
					</a>
				</span>
						<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="2288">+<span class="plusone-num">6</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/cafeteria-corte-ingles-deja-mucho-desear-camareros_c2288"
		   			   data-twitter-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="2288"
		   			   data-image="https://www.critizen.com/critizen/img/score/indignant.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="2036"
         data-critic-item="690"
         data-critic-score="furious"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  furious">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/4696"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/b3a1a59b/741ff5bc/cb5b2694/4e10881e/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/4696">Yeison Lozano Escobar</a>			</h4>
			<p>Con furia. <span class="muted">Hace <b>2 meses</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/nunca-pierden-solo-interes-ellosno-cliente_c2036" data-action="expand"><p>nunca pierden solo ven por los interés de ellos,no el del cliente </p></a>
							<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/nunca-pierden-solo-interes-ellosno-cliente_c2036" data-action="expand"><img src="https://d138d6gvpe36jl.cloudfront.net/critic/5de9df65/0320f9d4/a2501ed3/40388dff/400.jpg"></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/nunca-pierden-solo-interes-ellosno-cliente_c2036" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						3802
					</a>
				</span>
			
		
		

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="2036">+<span class="plusone-num">1</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/nunca-pierden-solo-interes-ellosno-cliente_c2036"
		   			   data-twitter-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="2036"
		   			   data-image="https://d138d6gvpe36jl.cloudfront.net/critic/5de9df65/0320f9d4/a2501ed3/40388dff/550.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="2027"
         data-critic-item="690"
         data-critic-score="furious"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  furious">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/4696"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/b3a1a59b/741ff5bc/cb5b2694/4e10881e/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/4696">Yeison Lozano Escobar</a>			</h4>
			<p>Con furia. <span class="muted">Hace <b>2 meses</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/nunca-pierden-solo-quieren-hacer-caja_c2027" data-action="expand"><p>nunca pierden, solo quieren hacer caja </p></a>
							<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/nunca-pierden-solo-quieren-hacer-caja_c2027" data-action="expand"><img src="https://d138d6gvpe36jl.cloudfront.net/critic/5a2b7470/bc1024dc/acbe13f1/1d7c5e51/400.jpg"></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/nunca-pierden-solo-quieren-hacer-caja_c2027" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						3922
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/nunca-pierden-solo-quieren-hacer-caja_c2027" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							5
						</span>
					</a>
				</span>
					

									<span data-toggle="tooltip" title="Veces compartida">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/nunca-pierden-solo-quieren-hacer-caja_c2027" data-action="expand">
						<i class="glyphicon glyphicon-share"></i>
						<span class="js-share-counter " data-single="" data-plural="">
							1
						</span>
					</a>
				</span>
						<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="2027">+<span class="plusone-num">1</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/nunca-pierden-solo-quieren-hacer-caja_c2027"
		   			   data-twitter-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="2027"
		   			   data-image="https://d138d6gvpe36jl.cloudfront.net/critic/5a2b7470/bc1024dc/acbe13f1/1d7c5e51/550.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="1897"
         data-critic-item="690"
         data-critic-score="disappointed"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  disappointed">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/4343"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/7194ddf4/bab43e82/9f979612/8f093773/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/4343">Fco. Javier Díaz García</a>			</h4>
			<p>Con disgusto. <span class="muted">Hace <b>2 meses</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/menuda-pedazo-mierda-anuncio-hecho-para-rebajas_c1897" data-action="expand"><p>Menuda pedazo de mierda de anuncio que han hecho para las rebajas. Queriendo ir de graciosos con el "y lo sabes" hacen más el ridículo que otra cosa</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/menuda-pedazo-mierda-anuncio-hecho-para-rebajas_c1897" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						3728
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/menuda-pedazo-mierda-anuncio-hecho-para-rebajas_c1897" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							3
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="1897">+<span class="plusone-num">1</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/menuda-pedazo-mierda-anuncio-hecho-para-rebajas_c1897"
		   			   data-twitter-title="Otro que está #czdisgustado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czdisgustado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="1897"
		   			   data-image="https://www.critizen.com/critizen/img/score/disappointed.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="1830"
         data-critic-item="690"
         data-critic-score="furious"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  furious">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/4166"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/9b1407fc/578fc522/c238a748/c5d9377e/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/4166">Miguel Angel Serrano</a>			</h4>
			<p>Con furia. <span class="muted">Hace <b>2 meses</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compro-bici-1800-euros-llevo-tres-meses-averia_c1830" data-action="expand"><p>Me compro bici por 1800 euros, llevo tres meses y se me avería suspensiones los frenos cambiador de plato, pedalier llevo un mes y medio esperando una solución tengo que acudir a un abogado para re...</p></a>
							<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compro-bici-1800-euros-llevo-tres-meses-averia_c1830" data-action="expand"><img src="https://d138d6gvpe36jl.cloudfront.net/critic/4bf9c459/0debda7a/cdbd5a78/a52a1418/400.jpg"></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compro-bici-1800-euros-llevo-tres-meses-averia_c1830" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						3964
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compro-bici-1800-euros-llevo-tres-meses-averia_c1830" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							4
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="1830">+<span class="plusone-num">7</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compro-bici-1800-euros-llevo-tres-meses-averia_c1830"
		   			   data-twitter-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="1830"
		   			   data-image="https://d138d6gvpe36jl.cloudfront.net/critic/4bf9c459/0debda7a/cdbd5a78/a52a1418/550.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="1525"
         data-critic-item="690"
         data-critic-score="indignant"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  indignant">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/3582"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/982e4bec/79a90b0e/6accc742/f9e608ad/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/3582">Jesus Zaiño Rodriguez</a>			</h4>
			<p>Con indignación. <span class="muted">Hace <b>2 meses</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/pregunto-unas-panama-jack-dice-encargada-agotadas_c1525" data-action="expand"><p>Pregunto por unas panama jack del 44 o 45 y me dice la encargada q agotadas, doi una vuelta y vengo, me atiende otra persona y hay botas!! Me qedo... La caja de la bota no corresponde a la bota. Mu...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/pregunto-unas-panama-jack-dice-encargada-agotadas_c1525" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						3819
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/pregunto-unas-panama-jack-dice-encargada-agotadas_c1525" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							1
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="1525">+<span class="plusone-num">2</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/pregunto-unas-panama-jack-dice-encargada-agotadas_c1525"
		   			   data-twitter-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="1525"
		   			   data-image="https://www.critizen.com/critizen/img/score/indignant.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="1465"
         data-critic-item="690"
         data-critic-score="furious"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  furious">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/3485"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/6f8eb7c0/93e53ff6/d6222ffa/c0c3ff61/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/3485">Cristina Sánchez Moya</a>			</h4>
			<p>Con furia. <span class="muted">Hace <b>2 meses</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/merezco-pero-dinero-chato_c1465" data-action="expand"><p>Me lo merezco y lo se, pero no hay dinero chato</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/merezco-pero-dinero-chato_c1465" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						3799
					</a>
				</span>
			
		
		

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="1465">+<span class="plusone-num">5</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/merezco-pero-dinero-chato_c1465"
		   			   data-twitter-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="1465"
		   			   data-image="https://www.critizen.com/critizen/img/score/furious.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="1449"
         data-critic-item="690"
         data-critic-score="disappointed"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  disappointed">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/3442"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/05958d53/cd9f3d51/44996551/9d22e57b/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/3442">Joaquin Carretero</a>			</h4>
			<p>Con disgusto. <span class="muted">Hace <b>2 meses</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/queremos-modernos-vender-online-pero-damos-informacion_c1449" data-action="expand"><p>queremos ser modernos y vender online pero no damos informacion sobre la agencia de transporte</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/queremos-modernos-vender-online-pero-damos-informacion_c1449" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						3723
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/queremos-modernos-vender-online-pero-damos-informacion_c1449" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							1
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="1449">+<span class="plusone-num">7</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/queremos-modernos-vender-online-pero-damos-informacion_c1449"
		   			   data-twitter-title="Otro que está #czdisgustado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czdisgustado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="1449"
		   			   data-image="https://www.critizen.com/critizen/img/score/disappointed.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="1240"
         data-critic-item="690"
         data-critic-score="frustrated"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  frustrated">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/2938"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/120a694d/ba7fdc48/cce539fa/da8ee0da/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/2938">Isabel Curras Jménez</a>			</h4>
			<p>Con frustración. <span class="muted">Hace <b>2 meses</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/eslogan-publicitario-corte-ingles-dice-queda_c1240" data-action="expand"><p>El eslogan publicitario del corte Inglés dice " si no queda satisfecho le devolvemos el dinero"... En Santander te ponen cualquier impedimento para no darte dinero, eso si, la tarjeta abono no hay ...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/eslogan-publicitario-corte-ingles-dice-queda_c1240" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						2326
					</a>
				</span>
			
		
		

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="1240">+<span class="plusone-num">1</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/eslogan-publicitario-corte-ingles-dice-queda_c1240"
		   			   data-twitter-title="Otro que está #czfrustrado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfrustrado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="1240"
		   			   data-image="https://www.critizen.com/critizen/img/score/frustrated.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="749"
         data-critic-item="690"
         data-critic-score="frustrated"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  frustrated">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/60"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/b459b7ed/25a7211a/5e5ef5e8/114e84ae/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/60">Rodolfo Carpintier</a>			</h4>
			<p>Con frustración. <span class="muted">Hace <b>2 meses</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/tienda-online-deja-buscar-registrarte-antes-pleno_c749" data-action="expand"><p>La tienda online no te deja buscar sin registrarte antes. En pleno siglo XXI las tácticas del XX confunden y molestan al cliente. Si quiero saber si tienen algo debo poder consultarlo sin mas.</p></a>
							<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/tienda-online-deja-buscar-registrarte-antes-pleno_c749" data-action="expand"><img src="https://d138d6gvpe36jl.cloudfront.net/critic/f5acd224/053b04ac/1cef0661/74536abb/400.jpg"></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/tienda-online-deja-buscar-registrarte-antes-pleno_c749" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						4056
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/tienda-online-deja-buscar-registrarte-antes-pleno_c749" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							4
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="749">+<span class="plusone-num">7</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/tienda-online-deja-buscar-registrarte-antes-pleno_c749"
		   			   data-twitter-title="Otro que está #czfrustrado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfrustrado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="749"
		   			   data-image="https://d138d6gvpe36jl.cloudfront.net/critic/f5acd224/053b04ac/1cef0661/74536abb/550.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="707"
         data-critic-item="690"
         data-critic-score="indignant"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  indignant">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/1343"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/275af41b/3c831294/e18dbf94/10ded149/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/1343">Juan Carlos Granados Baeza</a>			</h4>
			<p>Con indignación. <span class="muted">Hace <b>2 meses</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compra-online-para-entregar-horas-despues-dias-dicen_c707" data-action="expand"><p>Compra online para entregar 24 horas. Después de 5 días me dicen que no hay existencias. 2 días después me abonan el importe ya pagado y hasta hoy.  Qué menos que una explicación o disculpa. <br />
</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compra-online-para-entregar-horas-despues-dias-dicen_c707" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						2447
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compra-online-para-entregar-horas-despues-dias-dicen_c707" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							3
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="707">+<span class="plusone-num">4</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compra-online-para-entregar-horas-despues-dias-dicen_c707"
		   			   data-twitter-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="707"
		   			   data-image="https://www.critizen.com/critizen/img/score/indignant.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="668"
         data-critic-item="690"
         data-critic-score="indignant"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  indignant">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/1273"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/5c1e4ee3/821c58e6/5d70a0b0/93329249/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/1273">Silvia Delgado Menor</a>			</h4>
			<p>Con indignación. <span class="muted">Hace <b>2 meses</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/parece-fatal-solo-contraten-menores-anos-subvenciones_c668" data-action="expand"><p>Me parece fatal que solo contraten a menores de 25 años por las subvenciones que les otorga el estado....los mayores de 25 no tenemos derecho a trabajar??</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/parece-fatal-solo-contraten-menores-anos-subvenciones_c668" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						3803
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/parece-fatal-solo-contraten-menores-anos-subvenciones_c668" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							3
						</span>
					</a>
				</span>
					

									<span data-toggle="tooltip" title="Veces compartida">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/parece-fatal-solo-contraten-menores-anos-subvenciones_c668" data-action="expand">
						<i class="glyphicon glyphicon-share"></i>
						<span class="js-share-counter " data-single="" data-plural="">
							1
						</span>
					</a>
				</span>
						<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="668">+<span class="plusone-num">6</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/parece-fatal-solo-contraten-menores-anos-subvenciones_c668"
		   			   data-twitter-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="668"
		   			   data-image="https://www.critizen.com/critizen/img/score/indignant.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="632"
         data-critic-item="690"
         data-critic-score="furious"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  furious">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/1096"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/b4847b0e/64bbc757/f542ea7b/60c0cd05/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/1096">Noemí Talens Soriano</a>			</h4>
			<p>Con furia. <span class="muted">Hace <b>2 meses</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/fatal-servicio-compra-internet-nunca-ocurrira-poca_c632" data-action="expand"><p>fatal su servicio de compra por internet nunca mas  se me ocurrirá poca seriedad e iniciativa para resolución de problemas además todavía 20 días después no me han devuelto mi dinero</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/fatal-servicio-compra-internet-nunca-ocurrira-poca_c632" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						3925
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/fatal-servicio-compra-internet-nunca-ocurrira-poca_c632" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							3
						</span>
					</a>
				</span>
					

									<span data-toggle="tooltip" title="Veces compartida">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/fatal-servicio-compra-internet-nunca-ocurrira-poca_c632" data-action="expand">
						<i class="glyphicon glyphicon-share"></i>
						<span class="js-share-counter " data-single="" data-plural="">
							1
						</span>
					</a>
				</span>
						<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="632">+<span class="plusone-num">7</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/fatal-servicio-compra-internet-nunca-ocurrira-poca_c632"
		   			   data-twitter-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="632"
		   			   data-image="https://www.critizen.com/critizen/img/score/furious.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="621"
         data-critic-item="690"
         data-critic-score="happy"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  happy">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/1221"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/b229ed46/df9096a1/de045ded/8e26d6ef/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/1221">Tarifa Sur</a>			</h4>
			<p>Con felicidad. <span class="muted">Hace <b>2 meses</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/buenas-marcassoy-comprador-habitual-esta-tienda-tanto_c621" data-action="expand"><p>muy buenas marcas<br />
SOY COMPRADOR HABITUAL DE ESTA TIENDA TANTO DE MIS HIJAS ,COMO ROPA PARA MI ,ME GUSTA IR A LA ULTIMA Y ESTA TIENDA TIENE LO QUE NECESITO.<br />
<br />
</p></a>
							<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/buenas-marcassoy-comprador-habitual-esta-tienda-tanto_c621" data-action="expand"><img src="https://d138d6gvpe36jl.cloudfront.net/critic/0199515c/52ace3c7/05e04d83/6aa1027d/400.jpg"></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/buenas-marcassoy-comprador-habitual-esta-tienda-tanto_c621" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						3841
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/buenas-marcassoy-comprador-habitual-esta-tienda-tanto_c621" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							4
						</span>
					</a>
				</span>
					

									<span data-toggle="tooltip" title="Veces compartida">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/buenas-marcassoy-comprador-habitual-esta-tienda-tanto_c621" data-action="expand">
						<i class="glyphicon glyphicon-share"></i>
						<span class="js-share-counter " data-single="" data-plural="">
							1
						</span>
					</a>
				</span>
						<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="621">+<span class="plusone-num">2</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/buenas-marcassoy-comprador-habitual-esta-tienda-tanto_c621"
		   			   data-twitter-title="Otro que está #czfeliz con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfeliz con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="621"
		   			   data-image="https://d138d6gvpe36jl.cloudfront.net/critic/0199515c/52ace3c7/05e04d83/6aa1027d/550.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="619"
         data-critic-item="690"
         data-critic-score="frustrated"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  frustrated">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/60"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/b459b7ed/25a7211a/5e5ef5e8/114e84ae/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/60">Rodolfo Carpintier</a>			</h4>
			<p>Con frustración. <span class="muted">Hace <b>2 meses</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/corte-ingles-barcelona-tiene-parking-matador-quien_c619" data-action="expand"><p>El Corte Ingles de Barcelona tiene un parking matador. No hay quien aparque en menos de media hora</p></a>
							<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/corte-ingles-barcelona-tiene-parking-matador-quien_c619" data-action="expand"><img src="https://d138d6gvpe36jl.cloudfront.net/critic/a0fbb5ae/33730d81/a64738ca/08cc735c/400.jpg"></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/corte-ingles-barcelona-tiene-parking-matador-quien_c619" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						3874
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/corte-ingles-barcelona-tiene-parking-matador-quien_c619" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							3
						</span>
					</a>
				</span>
					

									<span data-toggle="tooltip" title="Veces compartida">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/corte-ingles-barcelona-tiene-parking-matador-quien_c619" data-action="expand">
						<i class="glyphicon glyphicon-share"></i>
						<span class="js-share-counter " data-single="" data-plural="">
							1
						</span>
					</a>
				</span>
						<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="619">+<span class="plusone-num">5</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/corte-ingles-barcelona-tiene-parking-matador-quien_c619"
		   			   data-twitter-title="Otro que está #czfrustrado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfrustrado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="619"
		   			   data-image="https://d138d6gvpe36jl.cloudfront.net/critic/a0fbb5ae/33730d81/a64738ca/08cc735c/550.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="615"
         data-critic-item="690"
         data-critic-score="frustrated"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  frustrated">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/1214"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/d3a7ebc6/0fc77041/3d01c052/3f2d2ad4/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/1214">Isabella Vega</a>			</h4>
			<p>Con frustración. <span class="muted">Hace <b>2 meses</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/parece-algunas-secciones-corte-ingles-haya-productos_c615" data-action="expand"><p>Me parece mal que en algunas secciones del Corte Inglés haya productos sin precio. Sobre todo en la perfumería. No sé si esa estrategia les sirve para algo, porque por mucho que escondas el precio,...</p></a>
							<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/parece-algunas-secciones-corte-ingles-haya-productos_c615" data-action="expand"><img src="https://d138d6gvpe36jl.cloudfront.net/critic/aa4ebfcc/1a3ed363/c008e9cb/400037e9/400.jpg"></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/parece-algunas-secciones-corte-ingles-haya-productos_c615" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						3650
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/parece-algunas-secciones-corte-ingles-haya-productos_c615" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							4
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="615">+<span class="plusone-num">7</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/parece-algunas-secciones-corte-ingles-haya-productos_c615"
		   			   data-twitter-title="Otro que está #czfrustrado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfrustrado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="615"
		   			   data-image="https://d138d6gvpe36jl.cloudfront.net/critic/aa4ebfcc/1a3ed363/c008e9cb/400037e9/550.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="333"
         data-critic-item="690"
         data-critic-score="indignant"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  indignant">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/193"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/c149de47/318f34bf/cf23064d/c426feb9/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/193">Alejandro Estevez</a>			</h4>
			<p>Con indignación. <span class="muted">Hace <b>3 meses</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/surrealismo-corte-ingles-castellanano-atienda-nadie_c333" data-action="expand"><p>Surrealismo en el Corte Ingles de Castellana...no es que no te atienda nadie, es que hay que esperar 15 minutos para que alguien se digne a cobrarte. Que viva el eCommerce!<br />
</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/surrealismo-corte-ingles-castellanano-atienda-nadie_c333" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						3792
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/surrealismo-corte-ingles-castellanano-atienda-nadie_c333" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							2
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="333">+<span class="plusone-num">4</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/surrealismo-corte-ingles-castellanano-atienda-nadie_c333"
		   			   data-twitter-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czindignado con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="333"
		   			   data-image="https://www.critizen.com/critizen/img/score/indignant.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="256"
         data-critic-item="690"
         data-critic-score="furious"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  furious">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/218"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/a7eee729/27925d77/cde28376/3268d948/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/218">Patricia Cartagena Garcia</a>			</h4>
			<p>Con furia. <span class="muted">Hace <b>3 meses</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compre-unos-pantalones-hilo-corte-ingles-segunda-puesta_c256" data-action="expand"><p>Me compre unos pantalones de hilo en el Corte Ingles en su segunda puesta se rompieron por la entrepierna, fui al centro donde me los compre con la intención de que me los cambiaran por otros ya qu...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compre-unos-pantalones-hilo-corte-ingles-segunda-puesta_c256" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						2471
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compre-unos-pantalones-hilo-corte-ingles-segunda-puesta_c256" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							2
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="256">+<span class="plusone-num">4</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/compre-unos-pantalones-hilo-corte-ingles-segunda-puesta_c256"
		   			   data-twitter-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="256"
		   			   data-image="https://www.critizen.com/critizen/img/score/furious.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



			<!--Critic Item-->
<article class="post-box col-xs-12 col-sm-4 fade"
         data-model="critic"
         data-critic-id="220"
         data-critic-item="690"
         data-critic-score="furious"
         data-critic-title="El Corte Inglés">

	<!--sended frame-->
		<!--end sended frame-->

	<!--header-->
	<header class="clearfix  furious">
		
	<div class="user-name">
		<div class="col-md-11 col-xs-11">
			<span class="pull-left">
				<a href="https://www.critizen.com/user/profile/175"><img class="rounded" src="https://d138d6gvpe36jl.cloudfront.net/user/b663594f/c2ab4c79/62e6aac4/3c808225/40.jpg" width='30px' height='30px' ></a>
			</span>
			<h4>
				<a href="https://www.critizen.com/user/profile/175">Paula Clemente</a>			</h4>
			<p>Con furia. <span class="muted">Hace <b>3 meses</b></span></p>
		</div>
			</div>

</header>



<div class="content-critic">
			<h3>El Corte Inglés</h3>
		<div class="critic-text link-dark">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/hice-pedido-para-recoger-tienda-horas-desde-pago_c220" data-action="expand"><p>hice un pedido  para recoger  en tienda  a las 72 horas desde el pago. ha pasado mas de una semana  y nadie sabe donde  esta y estoy  harta. voy a ver si me dejan  cancelarlo porque  son unos  abso...</p></a>
						</div>

	<p class="counter muted">

		
							<span class="js-views-counter" data-toggle="tooltip" title="Visualizaciones">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/hice-pedido-para-recoger-tienda-horas-desde-pago_c220" data-action="expand">
						<i class="glyphicon glyphicon-eye-open"></i>
						3824
					</a>
				</span>
			
		
									<span data-toggle="tooltip" title="Commentarios">
					<a href="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/hice-pedido-para-recoger-tienda-horas-desde-pago_c220" data-action="expand">
						<i class="glyphicon glyphicon-comment"></i>
						<span class="js-comments-counter comments-counter " data-single="" data-plural="">
							1
						</span>
					</a>
				</span>
					

			<!--sended frame-->
						<span class="sended-info first-view">
				<i class="fa fa-send"></i>
				<span class="info-text">Crítica enviada</span>
			</span>
            	<!--end sended frame-->
	</p>

	
	
</div>

<!--footer-->
<footer>
	<div class="col-xs-4 col-sm-4 align-center">

		<span  class="js-plusone-counter  "
               data-action="save-activity"
               data-model="activity"
               data-activity-type="plus_one"
               data-activity-relation="critic"
               data-activity-relation-id="220">+<span class="plusone-num">5</span>
		</span>

	</div>
	<div class="col-xs-4 col-sm-4 align-center">
					<a href="#" data-action="require-login"><span class="glyphicon glyphicon-comment"></span> <span class="text">Comentar</span></a>
			</div>
	<div class="col-xs-4 col-sm-4 align-center">
				<a href="#"
		   data-action="share"
		   data-url="https://www.critizen.com/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/hice-pedido-para-recoger-tienda-horas-desde-pago_c220"
		   			   data-twitter-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
			   data-facebook-title="Otro que está #czfurioso con El Corte Inglés, mira lo que le ha pasado en"
		   		   data-relation="critic"
		   data-relation-id="220"
		   			   data-image="https://www.critizen.com/critizen/img/score/furious.jpg"
		   		   ><span class="glyphicon glyphicon-share"></span> <span class="text">Compartir</span>
		</a>
	</div>
</footer>

</article>
<!-- /Critic Item-->



	</div>

<script>$("#grid-layout").makeGridLayout({ responsive: [ {width: 0, cols: 1}, {width: 540, cols: 2}, {width: 760, cols: 3} ] });</script>
		</div>

		

		

<ul class="pagination pull-right">
	<li class="disabled">
		<a href="#"
				><span class="icon-left"></span></a>
	</li>
		<li class="active">
		<a href="/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/1"
					>1</a>
	</li>
		<li >
		<a href="/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/2"
					>2</a>
	</li>
		<li >
		<a href="/alimentacion-y-consumo-quejas-el-corte-ingles_i690_es/2"
						><span class="icon-right"></span></a>
	</li>
</ul>


    </div>
</section>

		</div> <!-- END  main-wrapper-->
		<!-- FOOTER -->
				<footer class="m-footer">
			<div class="container">
				<div class="row">
					<div class="col-md-12 pull-left">
						
						<ul class="list-inline unstyled">
							<li><a href="/condiciones-legales">Condiciones legales </a></li>
							<li class="hidden-xs">| </li>
							<li><a href="/preguntas-frecuentes">Preguntas Frecuentes </a></li>
							<li class="hidden-xs">| </li>
							<li><a href="/politica-nombre-real">Política de Nombres Reales </a></li>
							<li class="hidden-xs">| </li>
							<li><a href="/indice-critizen">Índice de Recomendación </a></li>
							<li class="hidden-xs">| </li>
							<li><a data-track='{"mp":"click empresas destacadas","ga":"critic/click empresas destacadas"}' href="/quejas-empresas">Empresas más reclamadas </a></li>
							<li class="hidden-xs">| </li>
							<li><a href="https://vimeo.com/116143179">Ver spot </a></li>
							<li class="hidden-xs">| </li>
							<li><a href="#" data-action="feedback">Feedback </a></li>
						</ul>
					</div>
					<p class="pull-left copy">Copyright © Critizen - All rights reserved</p>

											<div class="footer-apps pull-right">
							<span class="text muted">Descárgate la app y hazte escuchar donde quieras</span>

							<span class="apps">
								<a data-track='{"mp":"click google play","ga":"critic/click google play"}' href="https://play.google.com/store/apps/details?id=com.critizen" target="_blank" class="sprite-critizen google-footer"></a>
								<a data-track='{"mp":"click apple store","ga":"critic/click apple store"}' href="https://appsto.re/es/f1tn4.i" target="_blank" class="sprite-critizen apple-footer"></a>
							</span>

						</div>
									</div>
			</div>
		</footer>
		
		<script>App.push({"content_country":"es","appId":139349849412235,"application":"critizen","environment":"pro","url_prefix":"\/","host":{"critizen":"https:\/\/www.critizen.com","critizen-api":"https:\/\/api.critizen.com","critizen-admin":"https:\/\/admin43.critizen.com"},"url_bucket":"https:\/\/d138d6gvpe36jl.cloudfront.net","logged":false,"loggedUserData":[],"isMobile":false,"isTablet":false,"DEBUG_MODE":false,"USER_LANG":"es","browserLang":"en"});</script>

		
					<script src="https://d138d6gvpe36jl.cloudfront.net/js/min/1421753073_group_critizen_commons.js" type="text/javascript"></script>
					<script src="https://d138d6gvpe36jl.cloudfront.net/js/min/d1e119d63d0a02792954351a06ada143_requirejs_map.js" type="text/javascript"></script>
					<script src="https://d138d6gvpe36jl.cloudfront.net/js/min/1424101224_lang_es.js" type="text/javascript"></script>
					<script src="https://d138d6gvpe36jl.cloudfront.net/js/min/1421064697_group_critizen_item.js" type="text/javascript"></script>
		
					<script src="//connect.facebook.net/es_ES/all.js#xfbml=1" type="text/javascript"></script>
					<script src="https://secure-ds.serving-sys.com/BurstingRes/CustomScripts/mmConversionTagV4.js" type="text/javascript"></script>
		
		
		

		
						<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0023/2530.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>

		
	</body>
</html>
