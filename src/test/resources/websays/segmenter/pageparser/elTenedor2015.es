


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="es">
    <head>
        <title>            Restaurante St. James - Gastro James Rosario Pino en Madrid : -50%. Opiniones, precios, reserva
    </title>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(t,e,n){function r(n){if(!e[n]){var o=e[n]={exports:{}};t[n][0].call(o.exports,function(e){var o=t[n][1][e];return r(o?o:e)},o,o.exports)}return e[n].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<n.length;o++)r(n[o]);return r}({QJf3ax:[function(t,e){function n(t){function e(e,n,a){t&&t(e,n,a),a||(a={});for(var c=u(e),f=c.length,s=i(a,o,r),p=0;f>p;p++)c[p].apply(s,n);return s}function a(t,e){f[t]=u(t).concat(e)}function u(t){return f[t]||[]}function c(){return n(e)}var f={};return{on:a,emit:e,create:c,listeners:u,_events:f}}function r(){return{}}var o="nr@context",i=t("gos");e.exports=n()},{gos:"7eSDFh"}],ee:[function(t,e){e.exports=t("QJf3ax")},{}],gos:[function(t,e){e.exports=t("7eSDFh")},{}],"7eSDFh":[function(t,e){function n(t,e,n){if(r.call(t,e))return t[e];var o=n();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(t,e,{value:o,writable:!0,enumerable:!1}),o}catch(i){}return t[e]=o,o}var r=Object.prototype.hasOwnProperty;e.exports=n},{}],D5DuLP:[function(t,e){function n(t,e,n){return r.listeners(t).length?r.emit(t,e,n):(o[t]||(o[t]=[]),void o[t].push(e))}var r=t("ee").create(),o={};e.exports=n,n.ee=r,r.q=o},{ee:"QJf3ax"}],handle:[function(t,e){e.exports=t("D5DuLP")},{}],XL7HBI:[function(t,e){function n(t){var e=typeof t;return!t||"object"!==e&&"function"!==e?-1:t===window?0:i(t,o,function(){return r++})}var r=1,o="nr@id",i=t("gos");e.exports=n},{gos:"7eSDFh"}],id:[function(t,e){e.exports=t("XL7HBI")},{}],loader:[function(t,e){e.exports=t("G9z0Bl")},{}],G9z0Bl:[function(t,e){function n(){var t=l.info=NREUM.info;if(t&&t.agent&&t.licenseKey&&t.applicationID&&c&&c.body){l.proto="https"===p.split(":")[0]||t.sslForHttp?"https://":"http://",a("mark",["onload",i()]);var e=c.createElement("script");e.src=l.proto+t.agent,c.body.appendChild(e)}}function r(){"complete"===c.readyState&&o()}function o(){a("mark",["domContent",i()])}function i(){return(new Date).getTime()}var a=t("handle"),u=window,c=u.document,f="addEventListener",s="attachEvent",p=(""+location).split("?")[0],l=e.exports={offset:i(),origin:p,features:{}};c[f]?(c[f]("DOMContentLoaded",o,!1),u[f]("load",n,!1)):(c[s]("onreadystatechange",r),u[s]("onload",n)),a("mark",["firstbyte",i()])},{handle:"D5DuLP"}]},{},["G9z0Bl"]);</script>
                <meta name="description" content="            -50% en Carta sólo en eltenedor.es - Reserva gratuitamente en el restaurante St. James - Gastro James Rosario Pino y con confirmación inmediata.
    " />
                <meta name="apple-itunes-app" content="app-id=424850908, affiliate-data=myAffiliateData, app-argument=https://itunes.apple.com/es/app/lafourchette-restaurants-reservation/id424850908">
                <meta property="fb:app_id" content="224589267619751" />
                        <meta property="og:title" content="Restaurante St. James - Gastro James Rosario Pino - 28020 Madrid" />
        <meta property="og:description" content="Sigue la tradición de excelencia que persigue la casa St. James. El restaurante St. James Rosario Pino es un local amplio en el que ante todo prima el buen..." />
        <meta property="og:type" content="restaurant" />
                <meta property="og:image" content="http://uploads.lafourchette.com/restaurant_photos/814/11814/square/92/9346f70a7fcf13d73c9e0d6a0b110255.jpg" />
        <meta property="og:url" content="http://www.eltenedor.es/restaurantev2/st-james-gastro-james-rosario-pino/11814" />
    <meta property="og:site_name" content="eltenedor.es" />
    <meta property="fb:admins" content="100001341891476" />
                    <meta name="robots" content="INDEX, FOLLOW" />

                                    <link rel="alternate" hreflang="fr-FR" href="http://www.lafourchette.com/restaurant/st-james-gastro-james-rosario-pino/11814"><link rel="alternate" hreflang="fr-CH" href="http://www.lafourchette.ch/restaurant/st-james-gastro-james-rosario-pino/11814"><link rel="alternate" hreflang="nl-BE" href="http://www.lafourchette.be/nl/restaurant/st-james-gastro-james-rosario-pino/11814"><link rel="alternate" hreflang="fr-BE" href="http://www.lafourchette.be/restaurant/st-james-gastro-james-rosario-pino/11814"><link rel="alternate" hreflang="es-ES" href="http://www.eltenedor.es/restaurante/st-james-gastro-james-rosario-pino/11814"><link rel="alternate" hreflang="en-GB" href="http://www.thefork.com/restaurant/st-james-gastro-james-rosario-pino/11814"><link rel="alternate" hreflang="it-IT" href="http://www.thefork.it/ristorante/st-james-gastro-james-rosario-pino/11814">
                    
        <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]--><link rel="stylesheet" href="http://wac.12523.alphacdn.net/8012523/assets/v-526/css/4f92ec5.css" type="text/css" /><style type="text/css"></style><link rel="stylesheet" href="http://wac.12523.alphacdn.net/8012523/assets/v-526/css/e439718.css" type="text/css" /><link rel="stylesheet" href="http://wac.12523.alphacdn.net/8012523/assets/v-526/css/dd15721.css" type="text/css" /><!--[if lte IE 7]><link rel="stylesheet" href="http://wac.12523.alphacdn.net/8012523/assets/v-526/css/19c06cb.css" type="text/css" /><![endif]-->
        
        <script type="text/javascript">
            var LFFbAppId = 224589267619751;
            var LFHostname = "www.eltenedor.es";
            var LFRoleUserGranted = "";
            var userIdFacebook = "";
            var LFPageLanguage = "es";
            var LFBrand = "es_ES";
            // pre load main sprite
            (new Image()).src = "http://wac.12523.alphacdn.net/8012523/assets/v-526/bundles/lafourchettecore/images/sprite-main.png";        </script>

        <!-- jQuery injected in the head so as to disable jQuery to be redownloaded  in ABTasty javascript content --><script type="text/javascript" src="http://wac.12523.alphacdn.net/8012523/assets/v-526/js/dc8879b.js"></script>            <script src="//cdn.optimizely.com/js/2092620231.js"></script>
        
        <link rel="shortcut icon" href="http://wac.12523.alphacdn.net/8012523/assets/v-526/favicon.png" />
                    <link href="http://www.eltenedor.es/restaurantev2/st-james-gastro-james-rosario-pino/11814" rel="canonical">
    
                                    <script type="text/javascript" >
                    var currentAdobeContext = {"navigation":{"platform":"DESKTOP","breadcrumb":null,"page_category":"restaurant-card","id_brand":2,"locale":"es_ES","city_name":null},"crm":{"is_connected":0,"id_customer":null,"member_type":null,"email":null,"gender":null,"last_name":null,"first_name":null,"phone":null,"birth_date":null,"id_city_zipcode":null,"zipcode":null,"id_city":null,"city_name":null,"id_country":null,"country_name":null,"address":null,"id_customer_job":null,"avatar_url":null,"has_optin_newsletter":null,"has_optin_actus":null,"has_optin_partner":null,"has_optin_sms":null,"has_optin_terms_of_use":null,"gamification_level":null,"rate_count_total":null,"rate_count_own_reservations":null,"reservation_count":null,"cx_hash":null,"facebook_id":null,"is_prescriber":null,"id_restaurant_favorite_list":"default"},"reservation":{"id_sale_type":null,"sale_type_name":null,"sale_type_discount":null,"date":null,"time":null,"pax":null,"has_invitations":null},"search_bar":{"where":"default","cuisine_type":"default","date":"default","time":"default","pax":"default"},"search_filters":{"order_by":null,"page_number":null,"cuisine_type_list":"default","special_offer_list":"default","price_slider":"default","rate_slider":"default","with_whom":"default","guide_list":"default","menu_list":"default","service_list":"default","payment_list":"default","point_of_interest_list":"default","restaurant_type_list":"default","menu_restriction_list":"default","meal_list":"default","setting_list":"default","special_occasion_list":"default","selection_lf_list":"default"},"restaurant_list":{"id_restaurant":11814,"name":"St. James - Gastro James Rosario Pino","gps_lat":40.462183238522,"gps_long":-3.6930201053619,"address":"C\/ Rosario Pino, 14-16","id_city":328022,"city_name":"Madrid","zipcode":"28020","id_country":60,"phone":"34-915704691","avg_rate":9.1,"avg_rate_evolution":"STABLE","rate_distinction":"Sobresaliente","rate_count":6035,"average_price":37,"min_price":19,"min_price_before":37,"cuisine_type_list":"Mediterr\u00e1neo","restaurant_type_list":"Bistr\u00f3|Tradicional"}};
                    LF = window.LF || {}; LF.Adobe = LF.Adobe || {};
                    window.wa_lf = LF.Adobe.AdobeContext = currentAdobeContext;
                </script>
                    
                <script type="text/javascript" src="//assets.adobedtm.com/a64e52029aac0edd6e431e2b5b432ce869b4ee8d/satelliteLib-3a537865a7a1019077c112febd0050243be74e27.js"></script>
    </head>
    <body
        class="es_ES  "
        data-member-type=""
        data-locale="es_ES"
        data-language="es"
    >
        <div id="fb-root"></div>
            <script type="text/javascript">
        var dataLayer = typeof dataLayer === "undefined" ? [] : dataLayer;
        dataLayer.push({
            "pageCategory": "Fiche Restaurant"
        });
        dataLayer.push({
            "siteSearchTerm" : ""
        });
                dataLayer.push({
            "DHP" : "false"
        });
                dataLayer.push({
            "TypeRecherche": "restaurant"
        });
                dataLayer.push({
            "CuisineMoteur": "false"
        });
            </script>
        <script type="text/javascript">
            var dataLayer = typeof dataLayer === "undefined" ? [] : dataLayer;
            var mediaLayer = [].concat(dataLayer);

            for(var i=0;i<mediaLayer.length;i++){
                if(mediaLayer[i]['event']&&mediaLayer[i]['event'].match(/^gtm\./) ){
                    mediaLayer.splice(i, 1);
                }
            }

                    </script>
        <script>
            var injectGtm=function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({"gtm.start":(new Date).getTime(),event:"gtm.js"});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!="dataLayer"?"&l="+l:"";j.async=true;j.src="//www.googletagmanager.com/gtm.js?id="+i+dl;f.parentNode.insertBefore(j,f)};
            injectGtm(window, document, 'script', 'dataLayer', 'GTM-KV7M');
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','mediaLayer','GTM-MRR3');
        </script>
        <div id="bg">
                        <div id="wrapper">
                            <div id="header"><div class="header_content"><div class="title"><a href="/" title="eltenedor.es" class="icon logo-es-mini"><span>eltenedor.es</span></a></div><nav><ul class="noline"><li id="header_account_subscribe" class="first"><a href="http://www.eltenedor.es/mi-cuenta/registro" id="subscription"><span>Regístrate</span></a></li><li id="header_account_unlog"><a href="http://www.eltenedor.es/loginFirst" id="authentication"><span>Inicia sesión</span></a></li><li id="fid"><a href="http://www.eltenedor.es/premiamos-tu-confianza" class="clearfix" id="fid-link" title="premiamos tu confianza"><span class="fid-title">
            premiamos<br />tu&nbsp;confianza
        </span><span class="fid-details"><span class="fid-details-yums">1000 Yums</span><span class="fid-details-currency">10 €</span></span></a></li></ul></nav></div></div>                            
<div id="search" class="light">
    <div class="tagline">
                    <h2>Reservar un restaurante en <span>Madrid</span></h2>
                (<span class="l-f" data-u="MDYvZGFkdWljLXJhaWJtYWMvYWRldXFzdWIv" id="search_bar_change_city">Cambiar</span>)
    </div>
    <div class="belt">
                        <form name="la_fourchette_search_bar_form_type" id="la_fourchette_search_bar_form_type" method="get" action="/busqueda/madrid/328022" novalidate="novalidate">

            <fieldset class="userSearch" ><label class="userSearch-label" for="searchText">Dónde:</label><input type="text" id="searchText" name="searchText"    class="userSearch-input" placeholder="Nombre del restaurante, dirección, C.P..." autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" /><a class="userSearch-geolocBtn"
                        rel="tooltip" title="Mi posición actual" id="target" class="helper"
                ><span class="userSearch-geolocIcon"></span><span class="userSearch-geolocLabel">Cerca de mí</span></a><div class="userSearch-loader"></div></fieldset>
            <fieldset id="foodTypeWrapper" >
                <div class="line" >
                    <select id="foodTypeTag" name="foodTypeTag[]"    autocomplete="false" data-text-type="Tipo de cocina" data-text-all="Todos" data-text-selected-count="# tipos de cocinas" multiple="multiple"><option value="460">Árabe</option><option value="385">Africano</option><option value="429">Alemán</option><option value="459">Americano</option><option value="403">Andaluz</option><option value="453">Argentino</option><option value="404">Arrocería</option><option value="790">Asador</option><option value="387">Asiático</option><option value="418">Asturiano</option><option value="431">Belga</option><option value="454">Brasileño</option><option value="410">Castellano</option><option value="406">Catalán</option><option value="389">Chino</option><option value="455">Colombiano</option><option value="390">Coreano</option><option value="764">Creativo</option><option value="456">Cubano</option><option value="450">De Fusión</option><option value="763">De mercado</option><option value="525">Del Norte</option><option value="432">Español</option><option value="386">Etíope</option><option value="433">Europa del este</option><option value="421">Extremeño</option><option value="434">Francés</option><option value="411">Gallego</option><option value="435">Griego</option><option value="451">Indio</option><option value="471">Internacional</option><option value="436">Italiano</option><option value="391">Japonés</option><option value="452">Latino</option><option value="465">Libanés</option><option value="423">Madrileño</option><option value="791">Marisquería</option><option value="789">Mediterráneo</option><option value="457">Mexicano</option><option value="415">Murciano</option><option value="416">Navarro</option><option value="392">Paquistaní</option><option value="458">Peruano</option><option value="437">Portugués</option><option value="438">Ruso</option><option value="393">Tailandés</option><option value="468">Turco</option><option value="405">Vasco</option><option value="485">Vegetariano</option><option value="394">Vietnamita</option></select>
                </div>
            </fieldset>

            <fieldset>
                <div class="line pos-rel">
                    <input type="text"
                           id="la_fourchette_search_bar_form_date_picker"
                           name="altDate"
                           class="datepicker pointer"
                           value="Fecha"
                           autocomplete="off"
                           readonly="readonly"/>
                    <input type="hidden" id="date" name="date" />
                    <div id="time-simulacre" class="sbHolder searchBar-sbHolder selectbox-la_fourchette_search_bar_form_type_time selectbox-time sbHolderDisabled" tabindex="0"><a href="#" class="sbSelector searchBar-sbSelector" style="color: rgb(187, 187, 187); ">Hora</a></div>
                    <span class="hide"><select id="time" name="time"><option value=""></option><option value="00:00:00">00:00</option><option value="00:30:00">00:30</option><option value="01:00:00">01:00</option><option value="01:30:00">01:30</option><option value="02:00:00">02:00</option><option value="02:30:00">02:30</option><option value="03:00:00">03:00</option><option value="03:30:00">03:30</option><option value="04:00:00">04:00</option><option value="04:30:00">04:30</option><option value="05:00:00">05:00</option><option value="05:30:00">05:30</option><option value="06:00:00">06:00</option><option value="06:30:00">06:30</option><option value="07:00:00">07:00</option><option value="07:30:00">07:30</option><option value="08:00:00">08:00</option><option value="08:30:00">08:30</option><option value="09:00:00">09:00</option><option value="09:30:00">09:30</option><option value="10:00:00">10:00</option><option value="10:30:00">10:30</option><option value="11:00:00">11:00</option><option value="11:30:00">11:30</option><option value="12:00:00">12:00</option><option value="12:30:00">12:30</option><option value="13:00:00">13:00</option><option value="13:30:00">13:30</option><option value="14:00:00">14:00</option><option value="14:30:00">14:30</option><option value="15:00:00">15:00</option><option value="15:30:00">15:30</option><option value="16:00:00">16:00</option><option value="16:30:00">16:30</option><option value="17:00:00">17:00</option><option value="17:30:00">17:30</option><option value="18:00:00">18:00</option><option value="18:30:00">18:30</option><option value="19:00:00">19:00</option><option value="19:30:00">19:30</option><option value="20:00:00">20:00</option><option value="20:30:00">20:30</option><option value="21:00:00">21:00</option><option value="21:30:00">21:30</option><option value="22:00:00">22:00</option><option value="22:30:00">22:30</option><option value="23:00:00">23:00</option><option value="23:30:00">23:30</option></select></span>
                    <div id="pax-simulacre" class="sbHolder searchBar-sbHolder selectbox-la_fourchette_search_bar_form_type_pax selectbox-pax sbHolderDisabled" tabindex="0"><a href="#" class="sbSelector searchBar-sbSelector" style="color: rgb(187, 187, 187); ">Pers.</a></div>
                    <span class="hide"><select id="pax" name="pax"><option value=""></option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option></select></span>

                    <input type="hidden" id="coordinate" name="coordinate" />
                    <input type="hidden" id="idRestaurant" name="idRestaurant"    autocomplete="off" />
                    <input type="hidden" id="locality" name="locality" />
                    <input type="hidden" id="titleSubstitute" name="titleSubstitute" />
                    <input type="hidden" id="localeCode" name="localeCode" />
                    <input type="hidden" id="idGooglePlace" name="idGooglePlace" />

                    <input type="hidden" name="sb" value="1" />
                    <input type="hidden" name="is_restaurant_autocomplete" id="is_restaurant_autocomplete" value="0" />
                    <span class="search-icon datepicker-icon"></span>
                    <span class="search-icon time-icon"></span>
                    <span class="search-icon pax-icon"></span>
                </div>
            </fieldset>
            <fieldset class="submit" id="search-bar-submit">
                <input type="submit" class="btn black" value="Buscar restaurante" id="button_search"/>
            </fieldset>
        </form>
        <form class="hidden" name="la_fourchette_search_bar_form_type_intermediate" id="la_fourchette_search_bar_form_type_intermediate" method="post" action="#" novalidate="novalidate">
            <input type="hidden" name="c" id="countAddressesField" value=" 0 " />
            <input type="hidden" name="cr" id="countRestaurantsField" value="0" />
        </form>
    </div>
    <div id="geoloc_refused" style="display:none;">
        <div class="popin_message alert">
            <p>no hemos podido localizarte. Tienes que permitir el acceso a tu ubicación en la configuración del navegador.</p>
        </div>
    </div>
    <div id="search_data" style="display:none;"
        data-url-autocomplete = "http://www.eltenedor.es/recherche/autocomplete"
        data-search-text = ""
        data-translation-datepicker = "Aún no he decidido la fecha exacta"
        data-translation-see-more = "Ver todos los resultados"
        data-translation-datepicker-choice = "o"
        data-city = "Madrid"
        data-no-date = "Sin fecha"
        data-no-date-value = "noDate"
        data-popin-search = "&lt;div class=&quot;popin_message alert&quot;&gt;&lt;p&gt;Para garantizar la disponibilidad, por favor rellena la fecha, hora y número de personas.&lt;/p&gt;&lt;p&gt;Si tienes alguna fecha en mente, concrétala en el campo &quot;fecha&quot;.&lt;/p&gt;&lt;/div&gt;"
        data-category-restaurant = "Restaurantes"
        data-category-location = "Direcciones"
        data-search-text-default = "Nombre del restaurante, dirección, C.P..."
        data-today = "Hoy"
        data-tomorrow = "Mañana"
        data-popin-list-search-text = "¿Qué buscas?"
        data-my-position = "Mi posición actual"
        data-default-time-trans = "Hora"
        data-default-pax-trans = "Pers."
        data-country-code ="es"
        data-translation-restaurants = "Restaurantes"
        data-translation-addresses = "Direcciones"
        data-translation-restaurants-history = "Mis últimos restaurantes"
        data-translation-addresses-history = "Mis últimas direcciones"
        data-translation-past = "¿Dónde tienes la cabeza? Te has confundido de fecha. Por favor, modifícala."
        data-google-worldwide-launch = "0"
        data-url-intermediate = "http://www.eltenedor.es/search-refine/suggest/date/time/pax"
        data-google-autocomplete-establishment = "0"
        data-google-autocomplete-worldwide = "1"
        data-google-autocomplete-term-black-list = "(^restaurant\w*\s)|(\srestaurant\w*$);"
    ></div>
    </div>
                <div id="main">
                    <div id="breadcrumbs"><a href="/national" >Guía de restaurantes</a>
                 &gt;                    <a href="/ciudad/madrid/328022" >Restaurantes en Madrid</a>
                 &gt;                    <span  class="last">Restaurante St. James - Gastro James Rosario Pino</span><a href="" class="right history_back" id="breadcrumbs_historyBack" style="display:none;">&lt; Volver a los resultados</a></div>                    
    <main role="main" class="clearfix" >

        <article id="content" class="restaurant_card restaurant_card_v2" itemscope itemtype="http://schema.org/Restaurant">
            <summary class="header">
                    



<div class="infoRestaurant alt"><span class="hide" itemprop="url">http://www.eltenedor.es/restaurantev2/st-james-gastro-james-rosario-pino/11814</span><div class="row-fluid" ><div class="span10"><h1 itemprop="name" >St. James - Gastro James Rosario Pino</h1><div class="clearfix"><p class="left" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="streetAddress">C/ Rosario Pino, 14-16 </span><span itemprop="postalCode">28020 </span><span itemprop="addressLocality">Madrid</span></p><p class="mapLink"><span class="pin"></span><a id="scrollToMap" href="#map" class="map bold scroll-to-href tabs-activate"
                           data-tabs-index="0" data-scroll-speed="3" data-scroll-offset="-80">
                            Mapa
                        </a></p></div></div><div class="right" ><div class="summaryRating summaryRating--withRateCount scroll-to-href tab-rates-value tabs-activate"
         data-tabs-index="1" data-href="#rt" data-scroll-offset="-90" data-href-set-disabled         itemscope itemtype="http://schema.org/AggregateRating" itemprop="aggregateRating"         onclick="dataLayer.push({'event':'ReviewsTab', 'reviewAction':'access', 'reviewLabel':'highlight'});"
    ><div class="summaryRating-value" ><span itemprop="ratingValue" content="9.1" >
                9,1
            </span>
            /<span itemprop="bestRating">10</span></div><div class="summaryRating-count" ><span itemprop="ratingCount" >6035</span> opiniones
        </div></div></div></div><div class="tags clearfix"><div id="minPrice" ><span class="normal">Precio medio</span>&nbsp;
        <strong>&nbsp;
            37 €
        </strong><a href="javascript:void(0);" rel="tooltip" title="Precio medio a la carta sin bebidas, calculado a partir de los datos proporcionados por el restaurante, y en base a una comida con entrante/plato principal o plato principal/postre." >
            &nbsp;<span class="tooltipHelp" ></span>&nbsp;
        </a></div><ul><li class="tag bold"><a href="/restaurante+mediterraneo+madrid"    >Mediterráneo</a></li><li class="tag "><a href="/restaurante+bistro+madrid"    >Bistró</a></li><li class="tag expand-desc"><a href="/restaurante+carnes+madrid"    >Carnes</a></li><li class="tag expand-desc"><a href="/restaurante+paella+madrid"    >Paella</a></li><li class="tag expand-desc"><span class="l-f "   data-u="MT1ENSU2QzclMTU2QzclZ2F0X3RuYXJ1YXRzZXJCNSVENSVHQVRCNSVzcmV0bGlmPzIyMDgyMy9kaXJkYW0vYWRldXFzdWIv" >Pasta</span></li><li class="tag expand-desc"><a href="/restaurante+tradicional+madrid"    >Tradicional</a></li><li class="expandTag tag more"><span class="horizontal-dot"></span></li></ul></div></div>            </summary>

            <section>
                <div id="sum" class="">
                    
<div class="actions"><a href="http://www.eltenedor.es/loginFirst/addFavorite" class="block-button block-button-secondary" rel="tooltip" data-toggle="tooltip" title="Añadir a mis favoritos" id="favorite-button" ><span class="heart-empty"></span></a></div><div id="favorite-data" style="display:none;"
     data-favorite-add-url="http://www.eltenedor.es/ajax/add-customer-favorite"
     data-favorite-add-text="Añadir a mis favoritos"
     data-favorite-remove-url="http://www.eltenedor.es/ajax/remove-customer-favorite"
     data-favorite-remove-text="Eliminar de mis favoritos"
></div><div id="slider_restaurant_wrapper" class="slider-wrapper theme-default" style=""><div id="slider_restaurant" class="nivoSlider"><div><img class="nivo-restaurant  first-image " width="612" height="344"
                                     itemprop="image" alt="St. James - Gastro James Rosario Pino Salón Art Decó" src="http://uploads.lafourchette.com/restaurant_photos/814/11814/169/612/1531b9ddfbf74b58a41e76ba8a55c44a.jpg"
                                     onerror="this._alter || (this._alter=true,this.src='http://wac.12523.alphacdn.net/8012523/assets/v-526/bundles/lafourchettecore/images/placeholder/612_344.jpg');" /></div><div><img class="nivo-restaurant " width="612" height="344"
                                     itemprop="image" alt="St. James - Gastro James Rosario Pino Salón Bóveda" content="http://uploads.lafourchette.com/restaurant_photos/814/11814/169/612/a9ece266ad9b570efab8cb3e485d4d5d.jpg"
                                     data-loop="612x344"  data-src="http://uploads.lafourchette.com/restaurant_photos/814/11814/169/612/a9ece266ad9b570efab8cb3e485d4d5d.jpg"
                                     onerror="this._alter || (this._alter=true,this.src='http://wac.12523.alphacdn.net/8012523/assets/v-526/bundles/lafourchettecore/images/placeholder/612_344.jpg');" /></div><div><img class="nivo-restaurant " width="612" height="344"
                                     itemprop="image" alt="St. James - Gastro James Rosario Pino Salón Central" content="http://uploads.lafourchette.com/restaurant_photos/814/11814/169/612/07aa33ca47ba460f807e49e3341b5deb.jpg"
                                     data-loop="612x344"  data-src="http://uploads.lafourchette.com/restaurant_photos/814/11814/169/612/07aa33ca47ba460f807e49e3341b5deb.jpg"
                                     onerror="this._alter || (this._alter=true,this.src='http://wac.12523.alphacdn.net/8012523/assets/v-526/bundles/lafourchettecore/images/placeholder/612_344.jpg');" /></div><div><img class="nivo-restaurant " width="612" height="344"
                                     itemprop="image" alt="St. James - Gastro James Rosario Pino Salón Central" content="http://uploads.lafourchette.com/restaurant_photos/814/11814/169/612/59c0a4c41f669ee17d3a1497bd645e10.jpg"
                                     data-loop="612x344"  data-src="http://uploads.lafourchette.com/restaurant_photos/814/11814/169/612/59c0a4c41f669ee17d3a1497bd645e10.jpg"
                                     onerror="this._alter || (this._alter=true,this.src='http://wac.12523.alphacdn.net/8012523/assets/v-526/bundles/lafourchettecore/images/placeholder/612_344.jpg');" /></div><div><img class="nivo-restaurant " width="612" height="344"
                                     itemprop="image" alt="St. James - Gastro James Rosario Pino Salón Biblioteca" content="http://uploads.lafourchette.com/restaurant_photos/814/11814/169/612/24e153ccf2d635f2bb2176d6acfcdd54.jpg"
                                     data-loop="612x344"  data-src="http://uploads.lafourchette.com/restaurant_photos/814/11814/169/612/24e153ccf2d635f2bb2176d6acfcdd54.jpg"
                                     onerror="this._alter || (this._alter=true,this.src='http://wac.12523.alphacdn.net/8012523/assets/v-526/bundles/lafourchettecore/images/placeholder/612_344.jpg');" /></div><div><img class="nivo-restaurant " width="612" height="344"
                                     itemprop="image" alt="St. James - Gastro James Rosario Pino Salón Biblioteca" content="http://uploads.lafourchette.com/restaurant_photos/814/11814/169/612/5432ec0b32031a2b061a9f0116842192.jpg"
                                     data-loop="612x344"  data-src="http://uploads.lafourchette.com/restaurant_photos/814/11814/169/612/5432ec0b32031a2b061a9f0116842192.jpg"
                                     onerror="this._alter || (this._alter=true,this.src='http://wac.12523.alphacdn.net/8012523/assets/v-526/bundles/lafourchettecore/images/placeholder/612_344.jpg');" /></div><div><img class="nivo-restaurant " width="612" height="344"
                                     itemprop="image" alt="St. James - Gastro James Rosario Pino Terraza" content="http://uploads.lafourchette.com/restaurant_photos/814/11814/169/612/9346f70a7fcf13d73c9e0d6a0b110255.jpg"
                                     data-loop="612x344"  data-src="http://uploads.lafourchette.com/restaurant_photos/814/11814/169/612/9346f70a7fcf13d73c9e0d6a0b110255.jpg"
                                     onerror="this._alter || (this._alter=true,this.src='http://wac.12523.alphacdn.net/8012523/assets/v-526/bundles/lafourchettecore/images/placeholder/612_344.jpg');" /></div><div><img class="nivo-restaurant " width="612" height="344"
                                     itemprop="image" alt="St. James - Gastro James Rosario Pino Terraza" content="http://uploads.lafourchette.com/restaurant_photos/814/11814/169/612/7b81a977b277d8d1d30f4964aa4309e6.jpg"
                                     data-loop="612x344"  data-src="http://uploads.lafourchette.com/restaurant_photos/814/11814/169/612/7b81a977b277d8d1d30f4964aa4309e6.jpg"
                                     onerror="this._alter || (this._alter=true,this.src='http://wac.12523.alphacdn.net/8012523/assets/v-526/bundles/lafourchettecore/images/placeholder/612_344.jpg');" /></div><div><img class="nivo-restaurant " width="612" height="344"
                                     itemprop="image" alt="St. James - Gastro James Rosario Pino Marisco" content="http://uploads.lafourchette.com/restaurant_photos/814/11814/169/612/st-james-gastro-james-rosario-pino-marisco-03a95.jpg"
                                     data-loop="612x344"  data-src="http://uploads.lafourchette.com/restaurant_photos/814/11814/169/612/st-james-gastro-james-rosario-pino-marisco-03a95.jpg"
                                     onerror="this._alter || (this._alter=true,this.src='http://wac.12523.alphacdn.net/8012523/assets/v-526/bundles/lafourchettecore/images/placeholder/612_344.jpg');" /></div><div><img class="nivo-restaurant " width="612" height="344"
                                     itemprop="image" alt="St. James - Gastro James Rosario Pino Paella" content="http://uploads.lafourchette.com/restaurant_photos/814/11814/169/612/st-james-gastro-james-rosario-pino-paella-bd3a3.jpg"
                                     data-loop="612x344"  data-src="http://uploads.lafourchette.com/restaurant_photos/814/11814/169/612/st-james-gastro-james-rosario-pino-paella-bd3a3.jpg"
                                     onerror="this._alter || (this._alter=true,this.src='http://wac.12523.alphacdn.net/8012523/assets/v-526/bundles/lafourchettecore/images/placeholder/612_344.jpg');" /></div></div></div>                </div>
            </section>

            <section>
                                
                <div id="restaurant-tabs" class="m_t">

                    
                    
                    <div class="row-fluid tabs">
                        <div class="tab-restaurant span4 offset0 active">
                            <a class="push_tag_restaurant" data-restaurant-event="OngletNavURI" data-event-type="clicOngletNav" href="#onglet-restaurant">
                                Información práctica
                            </a>
                        </div>

                                                <div class="tab-avis span4 offset0">
                            <a id="rateTabLink" class="push_tag_restaurant" href="#rt" id="tab-avis"
                               data-tab-identifier="rt" data-restaurant-event="OngletNavURI" data-event-type="clicOngletNav"
                               onclick="dataLayer.push({'event':'ReviewsTab', 'reviewAction':'access', 'reviewLabel':'tab'});">
                                <span class="bubble-speech"></span> 6035 opiniones
                            </a>
                        </div>
                        
                                                <div class="tab-tripadvisor span4 offset0">
                            <a href="#onglet-tripadvisor">
                                <span class="icon tripadvisor"></span>
                            </a>
                        </div>
                                            </div>

                    
                    <div id="details" class="border">

                        <div id="onglet-restaurant" data-scroll-offset="-110">
                            <section class="menu-card"><h2 class="sectionTitle " >Carta del restaurante St. James - Gastro James Rosario Pino</h2><ul><li class="cardCategory" ><h3 class="cardCategory-title">Entrante</h3><ul class="cardCategory-items"><li class="cardCategory-item clearfix" ><div class="cardCategory-itemPrice">18.7 €</div><div class="cardCategory-itemTitle" >Pulpo crujiente con romesco</div></li><li class="cardCategory-item clearfix" ><div class="cardCategory-itemPrice">19.8 €</div><div class="cardCategory-itemTitle" >Pequeños chipirones encebollados</div></li><li class="cardCategory-item clearfix" ><div class="cardCategory-itemPrice">18.7 €</div><div class="cardCategory-itemTitle" >Burrata fresca de Puglia con pomodori secchi y carpaccio de tomate</div></li></ul></li><li class="cardCategory" ><h3 class="cardCategory-title">Plato</h3><ul class="cardCategory-items"><li class="cardCategory-item clearfix" ><div class="cardCategory-itemPrice">22 €</div><div class="cardCategory-itemTitle" >Arroz con chipirones y ajetes tiernos</div></li><li class="cardCategory-item clearfix" ><div class="cardCategory-itemPrice">25.3 €</div><div class="cardCategory-itemTitle" >Crujiente de dorada sobre juliana de verduras frecas</div></li><li class="cardCategory-item clearfix" ><div class="cardCategory-itemPrice">23.1 €</div><div class="cardCategory-itemTitle" >Hamburguesa de Buey Valle del Esla con base de pan y crujiente de jamón</div></li></ul></li><li class="cardCategory" ><h3 class="cardCategory-title">Postre</h3><ul class="cardCategory-items"><li class="cardCategory-item clearfix" ><div class="cardCategory-itemPrice">9.35 €</div><div class="cardCategory-itemTitle" >Coulant de chocolate amargo 70% con sorbete de maracuyá y teja de naranja</div></li><li class="cardCategory-item clearfix" ><div class="cardCategory-itemPrice">9.35 €</div><div class="cardCategory-itemTitle" >Tarta fina de manzana con helado de vainilla</div></li><li class="cardCategory-item clearfix" ><div class="cardCategory-itemPrice">8.25 €</div><div class="cardCategory-itemTitle" >Crujiente y cremoso de chocolate con suzette de frutas y sorbete de frambuesas</div></li></ul></li></ul></section>                            



<section class="sale-type-list">
    <h2 class="sectionTitle">Promociones y Menús</h2>

    <div class="discountsMessage">
        Las promociones sólo están disponibles reservando con eltenedor.<br />
                Los menús podrán ser seleccionados en la última etapa de la reserva si hay disponibilidad.
    </div>

                
    <div class="blockmenu block-orange">

        
        <div class="ribbon-wrapper">
            <div class="ribbon-front-wrapper">
                <div class="ribbon-front">Promociones</div>
            </div>
            <div class="ribbon-edge-bottomleft"></div>
        </div>
        
        <div class="details">
            <h3 class="highlight">-50% en Carta sólo en eltenedor.es</h3>

                                                            
            <hr class="dotted"/>
                        <p>Bebida y Menús no incluidos. Aplicable a toda la carta si consumes como mínimo un primero y un segundo o un segundo y un postre por comensal. Válido de lunes a viernes en comidas y de lunes a domingo en cenas.</p>            
                        
        </div>
    </div>

        
    <div class="blockmenu block-orange">

        
        <div class="details">
            <h3 class="highlight">-40% en Carta sólo en eltenedor.es</h3>

                                                            
            <hr class="dotted"/>
                        <p>Bebida y Menús no incluidos. Aplicable a toda la carta si consumes como mínimo un primero y un segundo o un segundo y un postre por comensal. Válido sábado y domingo en comidas.</p>            
                        
        </div>
    </div>

                        </section>

                            

<section class="description clearfix">

    <h2 class="sectionTitle" >Sobre el restaurante St. James - Gastro James Rosario Pino</h2>

    <p itemprop="description">
        Sigue la tradición de excelencia que persigue la casa St. James. El restaurante St. James Rosario Pino es un local amplio en el que ante todo prima el buen gusto y el saber hacer. Con varios salones privados a disposición de los clientes y una espaciosa terraza para un rato de esparcimiento tranquilo, su personal los recibirá con una atención esmerada.<br />
<br />
La carta, como todas las demás de la firma, se especializa en arroces, con más de 30 variedades para probar cada día. Además, dispone de otros platos como fardos de calamar confitados con cebolla en su tinta al aceite de albahaca, crujiente de dorada sobre juliana de verduras frescas o Milhoja de rabo de toro. Los postres están elaborados artesanalmente y son toda una delicia.<br />
<br />
La decoración del lugar es distinguida: paredes de tonos claros contrastando con otras más oscuras, lámparas de araña y murales pintados que le dan un aire elegante a la par que urbano. Un sitio que no te puedes perder.
    </p>
</section>

                            

<section>
    <h2 class="sectionTitle">Precios del restaurante</h2>

            <div class="avgPrice clearfix">
        <div class="avg">
            <strong>Precio medio a la carta</strong> (entrante + plato o plato + postre)
        </div>
        <div class="price">
            37 €
        </div>
    </div>
    

                
        <div class="cardCategory">
            <h3 class="cardCategory-title">Precio de las bebidas</h3>
            <ul class="cardCategory-items">
                                                                                                <li class="cardCategory-item clearfix">
                            <div class="cardCategory-itemPrice" >2.8 €</div>
                            <div class="cardCategory-itemTitle" >Botella de agua mineral</div>
                        </li>
                                                                                <li class="cardCategory-item clearfix">
                            <div class="cardCategory-itemPrice" >2 €</div>
                            <div class="cardCategory-itemTitle" >Copa de vino</div>
                        </li>
                                                                                <li class="cardCategory-item clearfix">
                            <div class="cardCategory-itemPrice" >14 €</div>
                            <div class="cardCategory-itemTitle" >Botella de vino</div>
                        </li>
                                                                                <li class="cardCategory-item clearfix">
                            <div class="cardCategory-itemPrice" >6 €</div>
                            <div class="cardCategory-itemTitle" >Copa de champán</div>
                        </li>
                                                                                <li class="cardCategory-item clearfix">
                            <div class="cardCategory-itemPrice" >32 €</div>
                            <div class="cardCategory-itemTitle" >Botella de champán</div>
                        </li>
                                                                                <li class="cardCategory-item clearfix">
                            <div class="cardCategory-itemPrice" >2.5 €</div>
                            <div class="cardCategory-itemTitle" >Café</div>
                        </li>
                                                </ul>
        </div>

    

        </section>
                            
<section><h2 class="sectionTitle">Cómo llegar al restaurante St. James - Gastro James Rosario Pino</h2><div id="map" class="informations gmap"><span itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates"><meta itemprop="latitude" content="40.462183238522"><meta itemprop="longitude" content="-3.6930201053619"></span><div style="overflow:hidden;height:232px;width:611px;"><div data-gps-lat="40.462183238522" data-gps-lng="-3.6930201053619" data-image-marker="http://wac.12523.alphacdn.net/8012523/assets/v-526/bundles/lafourchettecore/images/map-marker.png"
                 id="map_canvas"  style="height:232px; width:611px"></div></div></div>

<div class="row-fluid"><div class="span6"><h3 class="title">¿Cómo llegar?</h3><div class="informations"><p><b>Transporte público</b><br />Metro Cuzco (L10)</p></div></div><div class="span6"><h3 class="title">Horario de apertura</h3><div class="informations"><p>Comida: de 13:30 a 15:30 (todos los días)<br />
Cena: de 20:30 a 23:00</p></div></div></div><div class="row-fluid"><div class="span12"><h3 class="title">Servicios</h3><div class="informations"><div class="payment-tag-pic" ></div><div class="payment-tag-pic" ></div><p itemprop="paymentAccepted" ><span class="credit-card"></span><span class="service tag"><span class="l-f "   data-u="MT1ENSU1QzclNDEzQzclZ2F0X3RuYXJ1YXRzZXJCNSVENSVHQVRCNSVzcmV0bGlmPzIyMDgyMy9kaXJkYW0vYWRldXFzdWIv" >Mastercard</span>

                                ,             
    <span class="l-f "   data-u="MT1ENSU1QzclNTEzQzclZ2F0X3RuYXJ1YXRzZXJCNSVENSVHQVRCNSVzcmV0bGlmPzIyMDgyMy9kaXJkYW0vYWRldXFzdWIv" >Visa</span></span></p><p><span class="movie"></span><span class="service">
                        Restaurante privatizable
                                                    (120 máximo de personas.)
                                            </span></p><p><span class="grey-tag"></span><span class="service tag"><span class="l-f "   data-u="MT1ENSUwMUM3JTQ1M0M3JWdhdF90bmFydWF0c2VyQjUlRDUlR0FUQjUlc3JldGxpZj8yMjA4MjMvZGlyZGFtL2FkZXVxc3ViLw==" >Aire acondicionado</span>

                            ,             
    <span class="l-f "   data-u="MT1ENSUwMUM3JTI3M0M3JWdhdF90bmFydWF0c2VyQjUlRDUlR0FUQjUlc3JldGxpZj8yMjA4MjMvZGlyZGFtL2FkZXVxc3ViLw==" >Catering </span>

                            ,             
    <span class="l-f "   data-u="MT1ENSUwMUM3JTk1M0M3JWdhdF90bmFydWF0c2VyQjUlRDUlR0FUQjUlc3JldGxpZj8yMjA4MjMvZGlyZGFtL2FkZXVxc3ViLw==" >English spoken</span>

                            ,             
    <a href="/restaurante+privatizable+madrid"    >Privatizable</a>

                            ,             
    <span class="l-f "   data-u="MT1ENSUwMUM3JTU3M0M3JWdhdF90bmFydWF0c2VyQjUlRDUlR0FUQjUlc3JldGxpZj8yMjA4MjMvZGlyZGFtL2FkZXVxc3ViLw==" >Wifi</span></span></p></div></div></div></section>                        </div>

                                                <div id="rt" data-scroll-offset="-110" class="no-uniform-ready">
                            
<div class="aside_avis">

    <div class="row-fluid">
        
        <div class="span3" >
            

<div class="summaryRating summaryRating--withDistinction scroll-to-href tab-rates-value tabs-activate"
         data-tabs-index="1"                   onclick="dataLayer.push({'event':'ReviewsTab', 'reviewAction':'access', 'reviewLabel':'highlight'});"
    ><div class="summaryRating-value" ><span  >
                9,1
            </span>
            /<span >10</span></div><div class="summaryRating-distinction" >
            Sobresaliente
        </div></div>        </div>


        <div class="span9 text_center aggregateRating" >
            <div class="scroll-to-href tab-rates-value tabs-activate" data-tabs-index="1" data-href="#rt" data-scroll-offset="-55" data-href-set-disabled>
                <h2>6035 opiniones de la comunidad</h2>
                <p>Sólo los clientes que acudieron al restaurante pueden dejar su opinión.</p>
            </div>
        </div>
    </div>
</div>

<section>
    <h3 class="title">Resumen</h3>
    
<div class="summary_rating">
    <div class="row-fluid">
        <div class="span5 b_r">

                        <ul class="summaryRateGraph" >
                            <li class="clearfix" >
                    <div class="summaryRateGraph-rangeLabel">
                                                    Más de 9/10
                        
                        <span>(3304)</span>
                    </div>
                    <div class="summaryRateGraph-rangeBar">
                        <div title="54.74%" style="width: 54.74%"></div>
                    </div>
                </li>
                            <li class="clearfix" >
                    <div class="summaryRateGraph-rangeLabel">
                                                    8.5 a 9/10
                        
                        <span>(945)</span>
                    </div>
                    <div class="summaryRateGraph-rangeBar">
                        <div title="15.66%" style="width: 15.66%"></div>
                    </div>
                </li>
                            <li class="clearfix" >
                    <div class="summaryRateGraph-rangeLabel">
                                                    8 a 8.5/10
                        
                        <span>(609)</span>
                    </div>
                    <div class="summaryRateGraph-rangeBar">
                        <div title="10.09%" style="width: 10.09%"></div>
                    </div>
                </li>
                            <li class="clearfix" >
                    <div class="summaryRateGraph-rangeLabel">
                                                    7 a 8/10
                        
                        <span>(744)</span>
                    </div>
                    <div class="summaryRateGraph-rangeBar">
                        <div title="12.33%" style="width: 12.33%"></div>
                    </div>
                </li>
                            <li class="clearfix" >
                    <div class="summaryRateGraph-rangeLabel">
                                                    Menos de 7/10
                        
                        <span>(434)</span>
                    </div>
                    <div class="summaryRateGraph-rangeBar">
                        <div title="7.19%" style="width: 7.19%"></div>
                    </div>
                </li>
                        </ul>

        </div>
        <div class="span7">
            <div class="row-fluid">
                <div class="span6 b_r rating_details">
                                        <div>
                        <span class="left">Cocina</span>
                        <span class="right highlight">9/10</span>
                    </div>
                    
                                        <div>
                        <span class="left">Servicio</span>
                        <span class="right highlight">9,2/10</span>
                    </div>
                    
                                        <div>
                        <span class="left">Ambiente</span>
                        <span class="right highlight">9/10</span>
                    </div>
                                    </div>
                <div class="span6 rating_details">
                                        <div>
                        <span class="left">Calidad-precio</span>
                        <span class="right highlight">Interesante</span>
                    </div>
                    
                                        <div>
                        <span class="left">Nivel de ruido</span>
                        <span class="right highlight">Tranquilo</span>
                    </div>
                    
                                        <div>
                        <span class="left">Tiempo de espera</span>
                        <span class="right highlight">Bien</span>
                    </div>
                                    </div>
            </div>

                        <div class="span12 recommandation">
                <p>¿Recomendarías este restaurante a un amigo?</p>
                <p class="highlight">Sí en 94.1%</p>
            </div>
                    </div>
    </div>
</div>
</section>

<section>
    
<div id="rate-filter-form" class="clearfix" >
    <div class="tri">
        <label for="lafourchette_ratefilter_form_order" >Ordenar :</label>
        <select id="lafourchette_ratefilter_form_order" name="lafourchette_ratefilter_form[order]" required="required"><option value="RESERVATION_DATE_DESC">Los más recientes primero</option><option value="RESERVATION_DATE_ASC">Los más antiguos primero</option><option value="AVG_RATE_DESC">La mejor nota primero</option><option value="AVG_RATE_ASC">La nota más baja primero</option></select>
    </div>
    <div class="comment-only">
        <input type="checkbox" id="lafourchette_ratefilter_form_withCommentsOnly" name="lafourchette_ratefilter_form[withCommentsOnly]"    checked="checked" value="1" /> <label for="lafourchette_ratefilter_form_withCommentsOnly">Sólo notas con comentarios</label>
    </div>
</div>
    <div id="avis_listing">

<div class="rateItem clearfix"><div class="rateItem-profile"><img width="90" height="90" data-src="http://uploads.lafourchette.com/avatar/75/female.jpg" alt="" /><p class="bold">
                        Jaime M.
        </p><span class="l-f highlight smaller" data-u="Njg4Mjg3MjEvcmVtb3RzdWMvZXRhci9zZS5yb2RlbmV0bGUud3d3Ly86cHR0aA==">
                Conquistador/a (<span class="rateItem-rateCount">4 opiniones</span>)
            </span></div><div class="rateItem-reviews rateItem-reviews--monoCustomer block-blue"><div class="rateItem-reviewItem" itemprop="review" itemscope itemtype="http://schema.org/Review" ><div class="rateItem-ribbon"><small itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating"><span itemprop="ratingValue">7,5</span>
                    /<span itemprop="bestRating">10</span></small></div><p class="rateItem-customerComment"><span itemprop="reviewBody">"Con el tenedor bien , sino caro"</span></p><div class="rateItem-more"><div class="rateItem-date"><meta itemprop="datePublished" content="2015-01-28"/>
                    Fecha de la comida : 28/01/2015
                </div></div></div></div></div><div class="rateItem clearfix"><div class="rateItem-profile"><img width="90" height="90" data-src="http://uploads.lafourchette.com/avatar/75/male.jpg" alt="" /><p class="bold">
                        Francisco P.
        </p><span class="l-f highlight smaller" data-u="NDU2NzYxMi9yZW1vdHN1Yy9ldGFyL3NlLnJvZGVuZXRsZS53d3cvLzpwdHRo">
                Rey Gourmet (<span class="rateItem-rateCount">36 opiniones</span>)
            </span></div><div class="rateItem-reviews rateItem-reviews--monoCustomer block-blue"><div class="rateItem-reviewItem" itemprop="review" itemscope itemtype="http://schema.org/Review" ><div class="rateItem-ribbon"><small itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating"><span itemprop="ratingValue">9</span>
                    /<span itemprop="bestRating">10</span></small></div><p class="rateItem-customerComment"><span itemprop="reviewBody">"Todo perfecto."</span></p><div class="rateItem-more"><div class="rateItem-date"><meta itemprop="datePublished" content="2015-01-26"/>
                    Fecha de la comida : 26/01/2015
                </div></div></div></div></div><div class="rateItem clearfix"><div class="rateItem-profile"><img width="90" height="90" data-src="http://uploads.lafourchette.com/avatar/75/male.jpg" alt="" /><p class="bold">
                        Andrés L.
        </p><span class="l-f highlight smaller" data-u="Nzk5NDAwMTAxL3JlbW90c3VjL2V0YXIvc2Uucm9kZW5ldGxlLnd3dy8vOnB0dGg=">
                Explorador/a (<span class="rateItem-rateCount">2 opiniones</span>)
            </span></div><div class="rateItem-reviews rateItem-reviews--monoCustomer block-blue"><div class="rateItem-reviewItem" itemprop="review" itemscope itemtype="http://schema.org/Review" ><div class="rateItem-ribbon"><small itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating"><span itemprop="ratingValue">6,5</span>
                    /<span itemprop="bestRating">10</span></small></div><p class="rateItem-customerComment"><span itemprop="reviewBody">"Restaurante recomendable con la promoción de El Tenedor"</span></p><div class="rateItem-more"><div class="rateItem-date"><meta itemprop="datePublished" content="2015-01-26"/>
                    Fecha de la comida : 25/01/2015
                </div></div></div></div></div><div class="rateItem clearfix"><div class="rateItem-profile"><img width="90" height="90" data-src="http://uploads.lafourchette.com/avatar/75/female.jpg" alt="" /><p class="bold">
                        Marisa L.
        </p><span class="l-f highlight smaller" data-u="ODY5MzcxMjAxL3JlbW90c3VjL2V0YXIvc2Uucm9kZW5ldGxlLnd3dy8vOnB0dGg=">
                Explorador/a (<span class="rateItem-rateCount">2 opiniones</span>)
            </span></div><div class="rateItem-reviews rateItem-reviews--monoCustomer block-blue"><div class="rateItem-reviewItem" itemprop="review" itemscope itemtype="http://schema.org/Review" ><div class="rateItem-ribbon"><small itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating"><span itemprop="ratingValue">9,5</span>
                    /<span itemprop="bestRating">10</span></small></div><p class="rateItem-customerComment"><span itemprop="reviewBody">"Muy buena comida,  buen ambiente y el servicio estupendo"</span></p><div class="rateItem-more"><div class="rateItem-date"><meta itemprop="datePublished" content="2015-01-25"/>
                    Fecha de la comida : 25/01/2015
                </div></div></div></div></div><div class="rateItem clearfix"><div class="rateItem-profile"><img width="90" height="90" data-src="http://uploads.lafourchette.com/avatar/75/male.jpg" alt="" /><p class="bold">
                        Alvaro L.
        </p><span class="l-f highlight smaller" data-u="MTQyMTczMzEvcmVtb3RzdWMvZXRhci9zZS5yb2RlbmV0bGUud3d3Ly86cHR0aA==">
                Conquistador/a (<span class="rateItem-rateCount">5 opiniones</span>)
            </span></div><div class="rateItem-reviews rateItem-reviews--monoCustomer block-blue"><div class="rateItem-reviewItem" itemprop="review" itemscope itemtype="http://schema.org/Review" ><div class="rateItem-ribbon"><small itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating"><span itemprop="ratingValue">8</span>
                    /<span itemprop="bestRating">10</span></small></div><p class="rateItem-customerComment"><span itemprop="reviewBody">"Mi tercera vez y, de nuevo, muy bien. Comida y trato excelentes."</span></p><div class="rateItem-more"><div class="rateItem-date"><meta itemprop="datePublished" content="2015-01-26"/>
                    Fecha de la comida : 25/01/2015
                </div></div></div></div></div><div class="rateItem clearfix"><div class="rateItem-profile"><img width="90" height="90" data-src="http://uploads.lafourchette.com/avatar/75/male.jpg" alt="" /><p class="bold">
                        Jose Luis M.
        </p><span class="l-f highlight smaller" data-u="NTMzMjM3My9yZW1vdHN1Yy9ldGFyL3NlLnJvZGVuZXRsZS53d3cvLzpwdHRo">
                Aventurero/a (<span class="rateItem-rateCount">1 opinión</span>)
            </span></div><div class="rateItem-reviews rateItem-reviews--monoCustomer block-blue"><div class="rateItem-reviewItem" itemprop="review" itemscope itemtype="http://schema.org/Review" ><div class="rateItem-ribbon"><small itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating"><span itemprop="ratingValue">10</span>
                    /<span itemprop="bestRating">10</span></small></div><p class="rateItem-customerComment"><span itemprop="reviewBody">"Comida muy bien elaborada, trato muy amable, aunque demasiado tiempo de espera entre platos"</span></p><div class="rateItem-more"><div class="rateItem-date"><meta itemprop="datePublished" content="2015-01-26"/>
                    Fecha de la comida : 25/01/2015
                </div></div></div></div></div><div class="rateItem clearfix"><div class="rateItem-profile"><img width="90" height="90" data-src="http://uploads.lafourchette.com/avatar/75/male.jpg" alt="" /><p class="bold">
                        Vicente E.
        </p><span class="l-f highlight smaller" data-u="NzE5NDI2OC9yZW1vdHN1Yy9ldGFyL3NlLnJvZGVuZXRsZS53d3cvLzpwdHRo">
                Conquistador/a (<span class="rateItem-rateCount">5 opiniones</span>)
            </span></div><div class="rateItem-reviews rateItem-reviews--monoCustomer block-blue"><div class="rateItem-reviewItem" itemprop="review" itemscope itemtype="http://schema.org/Review" ><div class="rateItem-ribbon"><small itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating"><span itemprop="ratingValue">10</span>
                    /<span itemprop="bestRating">10</span></small></div><p class="rateItem-customerComment"><span itemprop="reviewBody">"Una comida perfecta. Buena atención, muy buena calidad. Poco más se puede decir. Para repetir sin duda."</span></p><div class="rateItem-more"><div class="rateItem-date"><meta itemprop="datePublished" content="2015-01-26"/>
                    Fecha de la comida : 25/01/2015
                </div></div></div></div></div><div class="rateItem clearfix"><div class="rateItem-profile"><img width="90" height="90" data-src="http://uploads.lafourchette.com/avatar/75/male.jpg" alt="" /><p class="bold">
                        Manuel D.
        </p><span class="l-f highlight smaller" data-u="MjgzNDkzMS9yZW1vdHN1Yy9ldGFyL3NlLnJvZGVuZXRsZS53d3cvLzpwdHRo">
                Rey Gourmet (<span class="rateItem-rateCount">33 opiniones</span>)
            </span></div><div class="rateItem-reviews rateItem-reviews--monoCustomer block-blue"><div class="rateItem-reviewItem" itemprop="review" itemscope itemtype="http://schema.org/Review" ><div class="rateItem-ribbon"><small itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating"><span itemprop="ratingValue">8</span>
                    /<span itemprop="bestRating">10</span></small></div><p class="rateItem-customerComment"><span itemprop="reviewBody">"Tanto la comida como el servicio estuvieron a buena altura. Eso sí, los precios, sin la promo de El Tenedor, serían injustificadamente altos."</span></p><div class="rateItem-more"><div class="rateItem-date"><meta itemprop="datePublished" content="2015-01-26"/>
                    Fecha de la comida : 24/01/2015
                </div></div></div></div></div><div class="rateItem clearfix"><div class="rateItem-profile"><img width="90" height="90" data-src="http://uploads.lafourchette.com/avatar/75/female.jpg" alt="" /><p class="bold">
                        SAMANTHA R.
        </p><span class="l-f highlight smaller" data-u="Mzk5MzQwMTAxL3JlbW90c3VjL2V0YXIvc2Uucm9kZW5ldGxlLnd3dy8vOnB0dGg=">
                Explorador/a (<span class="rateItem-rateCount">2 opiniones</span>)
            </span></div><div class="rateItem-reviews rateItem-reviews--monoCustomer block-blue"><div class="rateItem-reviewItem" itemprop="review" itemscope itemtype="http://schema.org/Review" ><div class="rateItem-ribbon"><small itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating"><span itemprop="ratingValue">9,5</span>
                    /<span itemprop="bestRating">10</span></small></div><p class="rateItem-customerComment"><span itemprop="reviewBody">"Habíamos quedado con unos amigos que vienen de Asturias y son muy sibaritas... Les encanto y quedamos genial , todo buenísimo , como siempre."</span></p><div class="rateItem-more"><div class="rateItem-date"><meta itemprop="datePublished" content="2015-01-26"/>
                    Fecha de la comida : 24/01/2015
                </div></div></div></div></div><div class="rateItem clearfix"><div class="rateItem-profile"><img width="90" height="90" data-src="http://uploads.lafourchette.com/avatar/75/female.jpg" alt="" /><p class="bold">
                        Marta L.
        </p><span class="l-f highlight smaller" data-u="NjUwMjcyMjAxL3JlbW90c3VjL2V0YXIvc2Uucm9kZW5ldGxlLnd3dy8vOnB0dGg=">
                Aventurero/a (<span class="rateItem-rateCount">1 opinión</span>)
            </span></div><div class="rateItem-reviews rateItem-reviews--monoCustomer block-blue"><div class="rateItem-reviewItem" itemprop="review" itemscope itemtype="http://schema.org/Review" ><div class="rateItem-ribbon"><small itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating"><span itemprop="ratingValue">9</span>
                    /<span itemprop="bestRating">10</span></small></div><p class="rateItem-customerComment"><span itemprop="reviewBody">"Muy recomendable, el pulpo muy rico. Sin el descuento resultaría demsiado caro."</span></p><div class="rateItem-more"><div class="rateItem-date"><meta itemprop="datePublished" content="2015-01-25"/>
                    Fecha de la comida : 24/01/2015
                </div></div></div></div></div><div class="rateItem clearfix"><div class="rateItem-profile"><img width="90" height="90" data-src="http://uploads.lafourchette.com/avatar/75/female.jpg" alt="" /><p class="bold">
                        Helga f.
        </p><span class="l-f highlight smaller" data-u="ODU2MzUzMTAxL3JlbW90c3VjL2V0YXIvc2Uucm9kZW5ldGxlLnd3dy8vOnB0dGg=">
                Explorador/a (<span class="rateItem-rateCount">2 opiniones</span>)
            </span></div><div class="rateItem-reviews rateItem-reviews--monoCustomer block-blue"><div class="rateItem-reviewItem" itemprop="review" itemscope itemtype="http://schema.org/Review" ><div class="rateItem-ribbon"><small itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating"><span itemprop="ratingValue">8,5</span>
                    /<span itemprop="bestRating">10</span></small></div><p class="rateItem-customerComment"><span itemprop="reviewBody">"Un 8 en general, los platos necesitan leves mejoras pero en  general una buena experiencia."</span></p><div class="rateItem-more"><div class="rateItem-date"><meta itemprop="datePublished" content="2015-01-25"/>
                    Fecha de la comida : 24/01/2015
                </div></div></div></div></div><div class="rateItem clearfix"><div class="rateItem-profile"><img width="90" height="90" data-src="http://uploads.lafourchette.com/avatar/75/male.jpg" alt="" /><p class="bold">
                        Oscar v.
        </p><span class="l-f highlight smaller" data-u="NTU4NTgxMTEvcmVtb3RzdWMvZXRhci9zZS5yb2RlbmV0bGUud3d3Ly86cHR0aA==">
                Conquistador/a (<span class="rateItem-rateCount">7 opiniones</span>)
            </span></div><div class="rateItem-reviews rateItem-reviews--monoCustomer block-blue"><div class="rateItem-reviewItem" itemprop="review" itemscope itemtype="http://schema.org/Review" ><div class="rateItem-ribbon"><small itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating"><span itemprop="ratingValue">9,5</span>
                    /<span itemprop="bestRating">10</span></small></div><p class="rateItem-customerComment"><span itemprop="reviewBody">"Restaurante muy recomendable, atención exquisita. El pulpo, la lubina y la hamburguesa de buey muy ricos. El crujiente de chocolate brutal."</span></p><div class="rateItem-more"><div class="rateItem-date"><meta itemprop="datePublished" content="2015-01-25"/>
                    Fecha de la comida : 24/01/2015
                </div></div></div></div></div><div class="rateItem clearfix"><div class="rateItem-profile"><img width="90" height="90" data-src="http://uploads.lafourchette.com/avatar/75/female.jpg" alt="" /><p class="bold">
                        MARGARITA A.
        </p><span class="l-f highlight smaller" data-u="MzM4MTgzMDAxL3JlbW90c3VjL2V0YXIvc2Uucm9kZW5ldGxlLnd3dy8vOnB0dGg=">
                Aventurero/a (<span class="rateItem-rateCount">1 opinión</span>)
            </span></div><div class="rateItem-reviews rateItem-reviews--monoCustomer block-blue"><div class="rateItem-reviewItem" itemprop="review" itemscope itemtype="http://schema.org/Review" ><div class="rateItem-ribbon"><small itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating"><span itemprop="ratingValue">10</span>
                    /<span itemprop="bestRating">10</span></small></div><p class="rateItem-customerComment"><span itemprop="reviewBody">"todos los platos exceden las expectativas, el arroz St James es fantástico. buena carta de vinos, y excelente servicio con chic francés."</span></p><div class="rateItem-more"><div class="rateItem-date"><meta itemprop="datePublished" content="2015-01-25"/>
                    Fecha de la comida : 24/01/2015
                </div></div></div></div></div><div class="rateItem clearfix"><div class="rateItem-profile"><img width="90" height="90" data-src="http://uploads.lafourchette.com/avatar/75/female.jpg" alt="" /><p class="bold">
                        MARIA JOSE R.
        </p><span class="l-f highlight smaller" data-u="MTE5NzU1NC9yZW1vdHN1Yy9ldGFyL3NlLnJvZGVuZXRsZS53d3cvLzpwdHRo">
                Explorador/a (<span class="rateItem-rateCount">2 opiniones</span>)
            </span></div><div class="rateItem-reviews rateItem-reviews--monoCustomer block-blue"><div class="rateItem-reviewItem" itemprop="review" itemscope itemtype="http://schema.org/Review" ><div class="rateItem-ribbon"><small itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating"><span itemprop="ratingValue">8,5</span>
                    /<span itemprop="bestRating">10</span></small></div><p class="rateItem-customerComment"><span itemprop="reviewBody">"Con la promocion bien, pero sin ella los precios son algo elevados... La comida estaba rica pero poco original..."</span></p><div class="rateItem-more"><div class="rateItem-date"><meta itemprop="datePublished" content="2015-01-25"/>
                    Fecha de la comida : 24/01/2015
                </div></div></div></div></div><div class="rateItem clearfix"><div class="rateItem-profile"><img width="90" height="90" data-src="http://uploads.lafourchette.com/avatar/75/male.jpg" alt="" /><p class="bold">
                        Tomás M.
        </p><span class="l-f highlight smaller" data-u="Nzg0NzYzMTAxL3JlbW90c3VjL2V0YXIvc2Uucm9kZW5ldGxlLnd3dy8vOnB0dGg=">
                Conquistador/a (<span class="rateItem-rateCount">9 opiniones</span>)
            </span></div><div class="rateItem-reviews rateItem-reviews--monoCustomer block-blue"><div class="rateItem-reviewItem" itemprop="review" itemscope itemtype="http://schema.org/Review" ><div class="rateItem-ribbon"><small itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating"><span itemprop="ratingValue">9</span>
                    /<span itemprop="bestRating">10</span></small></div><p class="rateItem-customerComment"><span itemprop="reviewBody">"La comida exquisita y el trato inmejorable"</span></p><div class="rateItem-more"><div class="rateItem-date"><meta itemprop="datePublished" content="2015-01-26"/>
                    Fecha de la comida : 24/01/2015
                </div></div></div></div></div><div class="rateItem clearfix"><div class="rateItem-profile"><img width="90" height="90" data-src="http://uploads.lafourchette.com/avatar/75/male.jpg" alt="" /><p class="bold">
                        FRESNEDA S.
        </p><span class="l-f highlight smaller" data-u="MDY2MzIxNy9yZW1vdHN1Yy9ldGFyL3NlLnJvZGVuZXRsZS53d3cvLzpwdHRo">
                Conquistador/a (<span class="rateItem-rateCount">4 opiniones</span>)
            </span></div><div class="rateItem-reviews rateItem-reviews--monoCustomer block-blue"><div class="rateItem-reviewItem" itemprop="review" itemscope itemtype="http://schema.org/Review" ><div class="rateItem-ribbon"><small itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating"><span itemprop="ratingValue">7</span>
                    /<span itemprop="bestRating">10</span></small></div><p class="rateItem-customerComment"><span itemprop="reviewBody">"Todo correcto. Precios altos. Arroz señoret perfecto y abundante. Postres muy buenos, y te puedes tomar una copa sin prisas."</span></p><div class="rateItem-more"><div class="rateItem-date"><meta itemprop="datePublished" content="2015-01-26"/>
                    Fecha de la comida : 24/01/2015
                </div></div></div></div></div><div class="rateItem clearfix"><div class="rateItem-profile"><img width="90" height="90" data-src="http://uploads.lafourchette.com/avatar/75/male.jpg" alt="" /><p class="bold">
                        Julio N.
        </p><span class="l-f highlight smaller" data-u="ODYzODQxMzEvcmVtb3RzdWMvZXRhci9zZS5yb2RlbmV0bGUud3d3Ly86cHR0aA==">
                Conquistador/a (<span class="rateItem-rateCount">9 opiniones</span>)
            </span></div><div class="rateItem-reviews rateItem-reviews--monoCustomer block-blue"><div class="rateItem-reviewItem" itemprop="review" itemscope itemtype="http://schema.org/Review" ><div class="rateItem-ribbon"><small itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating"><span itemprop="ratingValue">9</span>
                    /<span itemprop="bestRating">10</span></small></div><p class="rateItem-customerComment"><span itemprop="reviewBody">"Uno de mis restaurantes favoritos de Madrid, el ambiente y el trato de 10 y la comida muy buena."</span></p><div class="rateItem-more"><div class="rateItem-date"><meta itemprop="datePublished" content="2015-01-24"/>
                    Fecha de la comida : 23/01/2015
                </div></div></div></div></div><div class="rateItem clearfix"><div class="rateItem-profile"><img width="90" height="90" data-src="http://uploads.lafourchette.com/avatar/75/male.jpg" alt="" /><p class="bold">
                        Tomas P.
        </p><span class="l-f highlight smaller" data-u="NjIxODM5MDAxL3JlbW90c3VjL2V0YXIvc2Uucm9kZW5ldGxlLnd3dy8vOnB0dGg=">
                Conquistador/a (<span class="rateItem-rateCount">8 opiniones</span>)
            </span></div><div class="rateItem-reviews rateItem-reviews--monoCustomer block-blue"><div class="rateItem-reviewItem" itemprop="review" itemscope itemtype="http://schema.org/Review" ><div class="rateItem-ribbon"><small itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating"><span itemprop="ratingValue">8,5</span>
                    /<span itemprop="bestRating">10</span></small></div><p class="rateItem-customerComment"><span itemprop="reviewBody">"Nunca me decepciona. Soy cliente habitual"</span></p><div class="rateItem-more"><div class="rateItem-date"><meta itemprop="datePublished" content="2015-01-24"/>
                    Fecha de la comida : 23/01/2015
                </div></div></div></div></div><div class="rateItem clearfix"><div class="rateItem-profile"><img width="90" height="90" data-src="http://uploads.lafourchette.com/avatar/75/male.jpg" alt="" /><p class="bold">
                        Jorge R.
        </p><span class="l-f highlight smaller" data-u="MDkyMzA5MS9yZW1vdHN1Yy9ldGFyL3NlLnJvZGVuZXRsZS53d3cvLzpwdHRo">
                Rey Gourmet (<span class="rateItem-rateCount">220 opiniones</span>)
            </span></div><div class="rateItem-reviews rateItem-reviews--monoCustomer block-blue"><div class="rateItem-reviewItem" itemprop="review" itemscope itemtype="http://schema.org/Review" ><div class="rateItem-ribbon"><small itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating"><span itemprop="ratingValue">9</span>
                    /<span itemprop="bestRating">10</span></small></div><p class="rateItem-customerComment"><span itemprop="reviewBody">"Local muy elegante, comida y servicio de alto nivel."</span></p><div class="rateItem-more"><div class="rateItem-date"><meta itemprop="datePublished" content="2015-01-24"/>
                    Fecha de la comida : 23/01/2015
                </div></div></div></div></div><div class="rateItem clearfix"><div class="rateItem-profile"><img width="90" height="90" data-src="http://uploads.lafourchette.com/avatar/75/male.jpg" alt="" /><p class="bold">
                        OSCAR G.
        </p><span class="l-f highlight smaller" data-u="NjQ1OTUyMjAxL3JlbW90c3VjL2V0YXIvc2Uucm9kZW5ldGxlLnd3dy8vOnB0dGg=">
                Aventurero/a (<span class="rateItem-rateCount">1 opinión</span>)
            </span></div><div class="rateItem-reviews rateItem-reviews--monoCustomer block-blue"><div class="rateItem-reviewItem" itemprop="review" itemscope itemtype="http://schema.org/Review" ><div class="rateItem-ribbon"><small itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating"><span itemprop="ratingValue">9,5</span>
                    /<span itemprop="bestRating">10</span></small></div><p class="rateItem-customerComment"><span itemprop="reviewBody">"Bastante bien. Servicio de reservas: excelente. Sin el tenedor sería demasiado caro"</span></p><div class="rateItem-more"><div class="rateItem-date"><meta itemprop="datePublished" content="2015-01-26"/>
                    Fecha de la comida : 23/01/2015
                </div></div></div></div></div>
<ul  class="pagination oneline text_right"  >
            
                                                                        <li  class="first"  >1</li>
                                                                                <li><a href="?page=2" data-page-number="2">2</a></li>
                                                                                <li><a href="?page=3" data-page-number="3">3</a></li>
                                                                                <li><a href="?page=4" data-page-number="4">4</a></li>
                                                                                <li><a href="?page=5" data-page-number="5">5</a></li>
                                                    <li>...</li>
                <li><a href="?page=146" data-page-number="146">146</a></li>
                        </ul>
</div>
</section>
                        </div>
                        
                                                <div id="onglet-tripadvisor" data-scroll-offset="-110">
                            <iframe id="tripadvisor-content" data-tripadvisor-params="?display=true&partnerId=9BAEF5A00D9F48AE9B37B29DACAC4F5D&lang=es_ES&locationId=11814"
        width="610" height="1200" scrolling="yes" frameborder="0"
        allowtransparency="true" marginheight="0" marginwidth="0" style="vertical-align:top;overflow-x: hidden;">
</iframe>
                        </div>
                                            </div>

                </div>

            </section>



        </article>

        
        <aside id="sticky">
            

<div class="dotted-background" ><div id="reservationModule"
        data-date-list-url="http://www.eltenedor.es/reservation/module/date-list/11814"
        data-hour-list-url="http://www.eltenedor.es/reservation/module/hour-list/11814/date"
        data-pax-list-url="http://www.eltenedor.es/reservation/module/pax-list/11814/date/time"
        data-sale-type-list-url="http://www.eltenedor.es/reservation/module/sale-type-list/11814/date/time/pax"
                data-confirm-url="http://www.eltenedor.es/reserva/restaurante/11814/%idSaleType%/reservar#date=%date%&time=%date% %hour%:00&pax=%pax%&version=2"
        data-initial-date=""
        data-initial-time=""
        data-initial-pax=""
        data-initial-dhp-defined=""
        data-default-id-sale-type="58048"
        data-has-sale-type-step="1"
        data-translation-brunch="Brunch"
        data-translation-menu="Menú"
        data-translation-normal="Normal"
    ><header><h2>Reservar</h2><p>Reserva gratis. Confirmación inmediata.</p><div id="moduleSaleTypeWrapper" ><p class="moduleSaleType orange">Hasta -50% en Carta sólo en eltenedor.es</p><div class="marker"><a href="javascript:void(0);" rel="tooltip" class="helper"
                       data-toggle="tooltip" data-placement="bottom" data-html data-original-title="Hasta&#x20;-50&#x25;&#x20;en&#x20;Carta&#x20;s&#x00F3;lo&#x20;en&#x20;eltenedor.es&lt;br&#x20;&#x2F;&gt;&#x0A;Bebida&#x20;y&#x20;Men&#x00FA;s&#x20;no&#x20;incluidos.&#x20;Aplicable&#x20;a&#x20;toda&#x20;la&#x20;carta&#x20;si&#x20;consumes&#x20;como&#x20;m&#x00ED;nimo&#x20;un&#x20;primero&#x20;y&#x20;un&#x20;segundo&#x20;o&#x20;un&#x20;segundo&#x20;y&#x20;un&#x20;postre&#x20;por&#x20;comensal.&#x20;V&#x00E1;lido&#x20;de&#x20;lunes&#x20;a&#x20;viernes&#x20;en&#x20;comidas&#x20;y&#x20;de&#x20;lunes&#x20;a&#x20;domingo&#x20;en&#x20;cenas."
                    ><span class="tooltipHelp" ></span></a></div></div></header><div class="row-fluid stepper"><div id="stepper-step1" class="span3 offset0 active current"><span class="step-checked"></span><span class="calendar"></span><span class="arrow-right"></span><p id="step-date" class="step-label">Fecha</p><p class="step-value"></p></div><div id="stepper-step2" class="span3 offset0"><span class="step-checked"></span><span class="time"></span><span class="arrow-right"></span><p id="step-hour" class="step-label">Hora</p><p class="step-value"></p></div><div id="stepper-step3" class="span3 offset0"><span class="step-checked"></span><span class="user-male"></span><span class="arrow-right"></span><p id="step-people" class="step-label">Personas</p><p class="step-value"></p></div><div id="stepper-step4" class="span3 offset0"><span class="step-checked"></span><span class="bigP">%</span><p id="step-offer" class="step-label">Opciones</p><p class="step-value"></p></div></div><div class="step-container"><span class="pending"></span><div id="step1" class="step reservationModuleStep"
                data-translation-tooltip-disabled="
                    &lt;strong&gt;Reserva online cerrada.&lt;/strong&gt;&lt;br/&gt;&lt;span&gt;El restaurante está cerrado o completo.&lt;/span&gt;
                "
                data-translation-subtitle-brunch="Brunch"
                data-translation-subtitle-menu="Menú"
                data-restaurant-web-max-days="90"
            ><div class="custom-calendar-wrap"><div id="custom-inner" class="custom-inner"><div class="custom-header clearfix"><nav><div id="custom-prev"><span class="smallarrow-left"></span></div><div id="custom-next"><span class="smallarrow-right"></span></div></nav><div id="custom-month" class="custom-month"></div></div><div id="calendar" class="fc-calendar-container reservationModuleStep-valueList"></div></div><div id="no-dhp" class="no-dhp-wrap"><div class="no-dhp-inner"><div class="no-dhp-title">¡Ups!</div>
                            
                    No hay disponibilidad en St. James - Gastro James Rosario Pino el jueves 29/01 a las 10h38 para  personas.
                
                            <a href="#"><div class="button">Seleccionar otros criterios</div></a></div></div></div></div><div id="step2" class="step reservationModuleStep"
                data-translation-lunchtype-breakfast="Desayuno"
                data-translation-lunchtype-lunch="Comida"
                data-translation-lunchtype-diner="Cena"
            ><div class="hour-list reservationModuleStep-valueList"></div></div><div id="step3" class="step no-uniform-ready reservationModuleStep"
                data-translation-more="
                    ¿Sois más personas?
                "
            ><div class="pax-list reservationModuleStep-valueList"></div></div><div id="step4" class="step reservationModuleStep"><div class="sale-type-list reservationModuleStep-valueList"></div><a href="#"><div class="button">Finaliza tu reserva</div></a></div></div><script type="text/template" id="discountAmountTemplate"><% var saleType = data.saleType; %><span class="<% if (saleType.isSpecialOffer) { %>orange<% } %>"><% if (saleType.isBrunch) { %>
                Brunch
                <% } else if (saleType.isMenu) { %>
                Menú
                <% } else if (saleType.isSpecialOffer && saleType.discountAmount) { %>
                -<%= saleType.discountAmount %>%
                <% } %></span></script><script type="text/template" id="rowFluidTemplate"><% _.each(chunk, function(dhpItem) { %><%var saleType = dhpItem.bestSaleType; %><div class="block span3" data-value="<%= dhpItem.value %>"><p class="big"><%= dhpItem.label %></p><% if (saleType) { %><p class="subtitle <% if (saleType.isSpecialOffer) { %>orange<% } %>"><% if (saleType.isBrunch) { %>
                        Brunch
                        <% } else if (saleType.isMenu) { %>
                        Menú
                        <% } else if (saleType.isSpecialOffer && saleType.discountAmount) { %>
                        -<%= saleType.discountAmount %>%
                        <% } %></p><% } %></div><% }) %></script><script type="text/template" id="blockLineListTemplate"><% _.each(blockLineList, function(blockLine) { %><div class="block-line"><% if (blockLine.title) { %><h3><%= blockLine.title %></h3><% } %><% _.each(blockLine.chunks, function(chunk) { %><div class="row-fluid"><%= rowFluidTemplateFunction({ chunk: chunk }) %></div><% }) %></div><% }) %></script><script type="text/template" id="saleTypeListTemplate"><% if (data.saleTypes.bestSaleType) { %><div class="sale-type-item block-offers row-fluid" data-sale-type-id="<%= data.saleTypes.bestSaleType.idSaleType %>"><div class="span2"><span id="radio-lf" class="radio"><span class="radio-checked"></span></span></div><div class="span10"><p class="orange"><%= data.saleTypes.bestSaleType.title %></p><hr class="dotted" /><p class="details"><%= data.saleTypes.bestSaleType.exclusions %></p></div></div><% } %><% if (data.saleTypes.otherSaleTypes && data.saleTypes.otherSaleTypes.length) { %><div class="show-all-saleTypes">Ver todas las opciones disponibles</div><% } %><% _.each(data.saleTypes.otherSaleTypes, function(saleType) { %><div class="sale-type-item sale-type-item-other offers hide" data-sale-type-id="<%= saleType.idSaleType %>"><span id="radio-lf" class="radio"><span class="radio-checked"></span></span><p class="bold"><%= saleType.title %></p></div><% }) %><% if (data.saleTypes.normalSaleType) { %><div class="sale-type-item offers" data-sale-type-id="<%= data.saleTypes.normalSaleType.idSaleType %>"><span id="radio-lf" class="radio"><span class="radio-checked"></span></span><% if (data.saleTypes.bestSaleType || data.saleTypes.otherSaleTypes.length) { %><p>Reserva sin promoción</p><% } else { %><p>Reserva sin promoción</p><% } %></div><% } %></script><script type="text/template" id="saleTypeListPopinTemplate"><div class="sale-type-list-popin-wrapper restaurant_card"><% _.each(data.groupedSaleTypes, function(group, groupName) { %><% _.each(group, function(saleType, saleTypeIndex) { %><div class="sale-type-item offers blockmenu <% print(groupName === data.controller.SALE_TYPE_CATEGORY_SPECIAL_OFFER ? 'block-orange' : 'block-black') %>" data-sale-type-id="<%= saleType.idSaleType %>"><% if(saleTypeIndex == 0 && groupName !== data.controller.SALE_TYPE_CATEGORY_NORMAL) { %><div class="ribbon-wrapper"><div class="ribbon-front-wrapper"><div class="ribbon-front"><%= saleType.categoryTitle %></div></div><div class="ribbon-edge-bottomleft"></div></div><% } %><div class="submit-button">Finaliza tu reserva</div><div class="details clearfix"><h3 class="highlight"><%= groupName === data.controller.SALE_TYPE_CATEGORY_NORMAL ? 'Reserva sin promoción' : saleType.title %></h3><% var hasExclusions  = saleType.exclusions %><% var hasMentions    = saleType.mentions %><% var hasDescription = saleType.description %><% var hasMenus       = saleType.menus && saleType.menus.length > 0 %><% if (hasExclusions) { %><p><%= saleType.exclusions %></p><% } %><% if (hasMentions) { %><p><%= saleType.mentions %></p><% } %><% if (hasDescription || hasMenus || saleType.footer) { %><% if (hasExclusions || hasMentions) { %><div class="expand link"><a href="#"><span class="expand-symbol plus"></span><span class="expand-label m_l5" data-label-expand="Ver más" data-label-collapse="Cerrar">Ver más</span></a></div><div class="clearfix"></div><div class="expand-desc" data-visible="false"><% if (hasDescription) { %><p><%= saleType.description %></p><% } %><% if (hasMenus) { %><% _.each(saleType.menus, function(menu, menuIndex) { %><% if (menuIndex === 0) { %><% if (menu.compositions && menu.compositions.length > 0) { %><ul><% _.each(menu.compositions, function(composition) { %><% if (composition.description) { %><% if (composition.name) { %><li class="subtitle cap"><%= composition.name %></li><% } %><li><%= composition.description %></li><% } %><% }) %></ul><% } %><% } %><% }) %><% } %><% if (saleType.footer) { %><p><%= saleType.footer %></p><% } %></div><% } %><% } %></div></div><% }) %><% }) %></div></script><script type="text/template" id="timeslotListTemplate"><%= blockLineListTemplateFunction({ blockLineList: blockLineList, rowFluidTemplateFunction: rowFluidTemplateFunction }) %></script><script type="text/template" id="paxListTemplate"><%= blockLineListTemplateFunction({ blockLineList: blockLineList, rowFluidTemplateFunction: rowFluidTemplateFunction }) %><% if (moreSelection.length > 0) { %><div class="block-line"><div class="more-peoples"><h4 class="left">
                    ¿Sois más personas?
                </h4><select class="right"><option value=""></option><% _.each(moreSelection, function(pax) { %><% var isSpecialOffer = (pax.bestSaleType != null && pax.bestSaleType.isSpecialOffer ) %><option <% if (isSpecialOffer) { %>class="orange"<% } %> value="<%= pax.nbPeople %>"><%= pax.nbPeople %></option><% }) %></select></div></div><% } %></script></div></div>
                        <div class="loyaltyProgramAds"><div class="loyaltyProgramAds-information"><strong>Premiamos tu confianza</strong><p>Con esta reserva ganarás 100 Yums</p></div></div>
        </aside>
        
        <aside id="aside-recommendations" >
            

<div id="recommandations" class="border">

    <h2>Quienes vieron este restaurante también consultaron</h2>

            <button class="recommendations-up disabled">
        <span class="arrow-up"></span>
    </button>
    
        <div class="recommandations-list">

                    
                <div class="recommendationItem clearfix"  >

                        <div class="recommendationItem-thumbnail">
                <a href="http://www.eltenedor.es/restaurante/a-solaina/17099" >
                    <img width="125" alt="A Solaina" data-src="http://uploads.lafourchette.com/restaurant_photos/099/17099/43/160/Vista-comedor.jpg" />
                                        <span class="recommendationItem-rate">
                        <span>8,7</span>&nbsp;/10
                    </span>
                                    </a>
            </div>

                        <div class="recommendationItem-information">
                <div class="recommendationItem-title" >
                    <a href="http://www.eltenedor.es/restaurante/a-solaina/17099" >
                        A Solaina
                    </a>
                </div>

                <p class="recommendationItem-address" >
                    <a href="http://www.eltenedor.es/restaurante/a-solaina/17099" >
                                            28020
                                        Madrid
                                            -&nbsp;Gallego
                                        </a>
                </p>

                                <p class="recommendationItem-saleType">
                    <a href="http://www.eltenedor.es/restaurante/a-solaina/17099" >
                        <span class="icon macaron-orange"></span>&nbsp;
                        <span>-50% en Carta sólo en eltenedor.es</span>
                    </a>
                </p>
                            </div>
        </div>
                    
                <div class="recommendationItem clearfix"  >

                        <div class="recommendationItem-thumbnail">
                <a href="http://www.eltenedor.es/restaurante/l-albufera-hotel-melia-castilla/9134" >
                    <img width="125" alt="L&#039;Albufera - Hotel Meliá Castilla" data-src="http://uploads.lafourchette.com/restaurant_photos/134/9134/43/160/l-albufera-hotel-melia-castilla-sugerencia-del-chef-0b64a.jpg" />
                                        <span class="recommendationItem-rate">
                        <span>9</span>&nbsp;/10
                    </span>
                                    </a>
            </div>

                        <div class="recommendationItem-information">
                <div class="recommendationItem-title" >
                    <a href="http://www.eltenedor.es/restaurante/l-albufera-hotel-melia-castilla/9134" >
                        L&#039;Albufera - Hotel Meliá Castilla
                    </a>
                </div>

                <p class="recommendationItem-address" >
                    <a href="http://www.eltenedor.es/restaurante/l-albufera-hotel-melia-castilla/9134" >
                                            28020
                                        Madrid
                                            -&nbsp;Arrocería
                                        </a>
                </p>

                                <p class="recommendationItem-saleType">
                    <a href="http://www.eltenedor.es/restaurante/l-albufera-hotel-melia-castilla/9134" >
                        <span class="icon macaron-orange"></span>&nbsp;
                        <span>-40% en Carta sólo en eltenedor.es</span>
                    </a>
                </p>
                            </div>
        </div>
                    
                <div class="recommendationItem clearfix"  >

                        <div class="recommendationItem-thumbnail">
                <a href="http://www.eltenedor.es/restaurante/el-viejo-almacen-de-buenos-aires/2751" >
                    <img width="125" alt="El Viejo Almacén de Buenos Aires" data-src="http://uploads.lafourchette.com/restaurant_photos/751/2751/43/160/el-viejo-almacen-de-buenos-aires-vista-sala-a7a39.jpg" />
                                        <span class="recommendationItem-rate">
                        <span>9,1</span>&nbsp;/10
                    </span>
                                    </a>
            </div>

                        <div class="recommendationItem-information">
                <div class="recommendationItem-title" >
                    <a href="http://www.eltenedor.es/restaurante/el-viejo-almacen-de-buenos-aires/2751" >
                        El Viejo Almacén de Buenos Aires
                    </a>
                </div>

                <p class="recommendationItem-address" >
                    <a href="http://www.eltenedor.es/restaurante/el-viejo-almacen-de-buenos-aires/2751" >
                                            28035
                                        Madrid
                                            -&nbsp;Argentino
                                        </a>
                </p>

                                <p class="recommendationItem-saleType">
                    <a href="http://www.eltenedor.es/restaurante/el-viejo-almacen-de-buenos-aires/2751" >
                        <span class="icon macaron-orange"></span>&nbsp;
                        <span>-40% en Carta sólo en eltenedor.es</span>
                    </a>
                </p>
                            </div>
        </div>
                    
                <div class="recommendationItem clearfix"  >

                        <div class="recommendationItem-thumbnail">
                <a href="http://www.eltenedor.es/restaurante/14-rosas/33791" >
                    <img width="125" alt="14 Rosas" data-src="http://uploads.lafourchette.com/restaurant_photos/791/33791/43/160/14-rosas-vista-mesa-76cf2.jpg" />
                                        <span class="recommendationItem-rate">
                        <span>8,9</span>&nbsp;/10
                    </span>
                                    </a>
            </div>

                        <div class="recommendationItem-information">
                <div class="recommendationItem-title" >
                    <a href="http://www.eltenedor.es/restaurante/14-rosas/33791" >
                        14 Rosas
                    </a>
                </div>

                <p class="recommendationItem-address" >
                    <a href="http://www.eltenedor.es/restaurante/14-rosas/33791" >
                                            28020
                                        Madrid
                                            -&nbsp;Español
                                        </a>
                </p>

                                <p class="recommendationItem-saleType">
                    <a href="http://www.eltenedor.es/restaurante/14-rosas/33791" >
                        <span class="icon macaron-orange"></span>&nbsp;
                        <span>-50% en Carta sólo en eltenedor.es</span>
                    </a>
                </p>
                            </div>
        </div>
                    
                <div class="recommendationItem clearfix"  >

                        <div class="recommendationItem-thumbnail">
                <a href="http://www.eltenedor.es/restaurante/asador-gaztelu/38972" >
                    <img width="125" alt="Asador Gaztelu" data-src="http://uploads.lafourchette.com/restaurant_photos/972/38972/43/160/asador-gaztelu-9cf58.jpg" />
                                        <span class="recommendationItem-rate">
                        <span>8,3</span>&nbsp;/10
                    </span>
                                    </a>
            </div>

                        <div class="recommendationItem-information">
                <div class="recommendationItem-title" >
                    <a href="http://www.eltenedor.es/restaurante/asador-gaztelu/38972" >
                        Asador Gaztelu
                    </a>
                </div>

                <p class="recommendationItem-address" >
                    <a href="http://www.eltenedor.es/restaurante/asador-gaztelu/38972" >
                                            28020
                                        Madrid
                                            -&nbsp;Vasco
                                        </a>
                </p>

                                <p class="recommendationItem-saleType">
                    <a href="http://www.eltenedor.es/restaurante/asador-gaztelu/38972" >
                        <span class="icon macaron-orange"></span>&nbsp;
                        <span>-50% en Carta sólo en eltenedor.es</span>
                    </a>
                </p>
                            </div>
        </div>
                    
                <div class="recommendationItem clearfix"  >

                        <div class="recommendationItem-thumbnail">
                <a href="http://www.eltenedor.es/restaurante/asador-guetaria/24647" >
                    <img width="125" alt="Asador Guetaria" data-src="http://uploads.lafourchette.com/restaurant_photos/647/24647/43/160/asador-guetaria-28cc1.jpg" />
                                        <span class="recommendationItem-rate">
                        <span>9</span>&nbsp;/10
                    </span>
                                    </a>
            </div>

                        <div class="recommendationItem-information">
                <div class="recommendationItem-title" >
                    <a href="http://www.eltenedor.es/restaurante/asador-guetaria/24647" >
                        Asador Guetaria
                    </a>
                </div>

                <p class="recommendationItem-address" >
                    <a href="http://www.eltenedor.es/restaurante/asador-guetaria/24647" >
                                            28020
                                        Madrid
                                            -&nbsp;Vasco
                                        </a>
                </p>

                                <p class="recommendationItem-saleType">
                    <a href="http://www.eltenedor.es/restaurante/asador-guetaria/24647" >
                        <span class="icon macaron-orange"></span>&nbsp;
                        <span>-40% en Carta sólo en eltenedor.es</span>
                    </a>
                </p>
                            </div>
        </div>
                    
                <div class="recommendationItem clearfix" style="display: none;" >

                        <div class="recommendationItem-thumbnail">
                <a href="http://www.eltenedor.es/restaurante/cosme/12976" >
                    <img width="125" alt="Cosme" data-src="http://uploads.lafourchette.com/restaurant_photos/976/12976/43/160/cosme-decoracion-y-sala-62bd9.jpg" />
                                        <span class="recommendationItem-rate">
                        <span>9,4</span>&nbsp;/10
                    </span>
                                    </a>
            </div>

                        <div class="recommendationItem-information">
                <div class="recommendationItem-title" >
                    <a href="http://www.eltenedor.es/restaurante/cosme/12976" >
                        Cosme
                    </a>
                </div>

                <p class="recommendationItem-address" >
                    <a href="http://www.eltenedor.es/restaurante/cosme/12976" >
                                            28009
                                        Madrid
                                            -&nbsp;De mercado
                                        </a>
                </p>

                                <p class="recommendationItem-saleType">
                    <a href="http://www.eltenedor.es/restaurante/cosme/12976" >
                        <span class="icon macaron-orange"></span>&nbsp;
                        <span>-30% en Carta sólo en eltenedor.es</span>
                    </a>
                </p>
                            </div>
        </div>
                    
                <div class="recommendationItem clearfix" style="display: none;" >

                        <div class="recommendationItem-thumbnail">
                <a href="http://www.eltenedor.es/restaurante/la-esquina-del-real/16173" >
                    <img width="125" alt="La Esquina del Real" data-src="http://uploads.lafourchette.com/restaurant_photos/173/16173/43/160/Vista-de-la-sala.jpg" />
                                        <span class="recommendationItem-rate">
                        <span>9</span>&nbsp;/10
                    </span>
                                    </a>
            </div>

                        <div class="recommendationItem-information">
                <div class="recommendationItem-title" >
                    <a href="http://www.eltenedor.es/restaurante/la-esquina-del-real/16173" >
                        La Esquina del Real
                    </a>
                </div>

                <p class="recommendationItem-address" >
                    <a href="http://www.eltenedor.es/restaurante/la-esquina-del-real/16173" >
                                            28013
                                        Madrid
                                            -&nbsp;Francés
                                        </a>
                </p>

                                <p class="recommendationItem-saleType">
                    <a href="http://www.eltenedor.es/restaurante/la-esquina-del-real/16173" >
                        <span class="icon macaron-orange"></span>&nbsp;
                        <span>-50% en Carta sólo en eltenedor.es</span>
                    </a>
                </p>
                            </div>
        </div>
                    
                <div class="recommendationItem clearfix" style="display: none;" >

                        <div class="recommendationItem-thumbnail">
                <a href="http://www.eltenedor.es/restaurante/zen-market/21756" >
                    <img width="125" alt="Zen Market" data-src="http://uploads.lafourchette.com/restaurant_photos/756/21756/43/160/ac30a0655a6ecacffcebbdd84b8109e7.jpg" />
                                        <span class="recommendationItem-rate">
                        <span>8,4</span>&nbsp;/10
                    </span>
                                    </a>
            </div>

                        <div class="recommendationItem-information">
                <div class="recommendationItem-title" >
                    <a href="http://www.eltenedor.es/restaurante/zen-market/21756" >
                        Zen Market
                    </a>
                </div>

                <p class="recommendationItem-address" >
                    <a href="http://www.eltenedor.es/restaurante/zen-market/21756" >
                                            28036
                                        Madrid
                                            -&nbsp;Asiático
                                        </a>
                </p>

                                <p class="recommendationItem-saleType">
                    <a href="http://www.eltenedor.es/restaurante/zen-market/21756" >
                        <span class="icon macaron-orange"></span>&nbsp;
                        <span>-50% en Carta sólo en eltenedor.es</span>
                    </a>
                </p>
                            </div>
        </div>
                    
                <div class="recommendationItem clearfix" style="display: none;" >

                        <div class="recommendationItem-thumbnail">
                <a href="http://www.eltenedor.es/restaurante/la-vaca-argentina-castellana/25072" >
                    <img width="125" alt="La Vaca Argentina Castellana" data-src="http://uploads.lafourchette.com/restaurant_photos/072/25072/43/160/47dcffc8046775a0245abbe648cbb23b.jpg" />
                                        <span class="recommendationItem-rate">
                        <span>8,8</span>&nbsp;/10
                    </span>
                                    </a>
            </div>

                        <div class="recommendationItem-information">
                <div class="recommendationItem-title" >
                    <a href="http://www.eltenedor.es/restaurante/la-vaca-argentina-castellana/25072" >
                        La Vaca Argentina Castellana
                    </a>
                </div>

                <p class="recommendationItem-address" >
                    <a href="http://www.eltenedor.es/restaurante/la-vaca-argentina-castellana/25072" >
                                            28046
                                        Madrid
                                            -&nbsp;Argentino
                                        </a>
                </p>

                                <p class="recommendationItem-saleType">
                    <a href="http://www.eltenedor.es/restaurante/la-vaca-argentina-castellana/25072" >
                        <span class="icon macaron-orange"></span>&nbsp;
                        <span>-40% en Carta sólo en eltenedor.es</span>
                    </a>
                </p>
                            </div>
        </div>
        
    </div>

            <button class="recommendations-down disabled">
        <span class="arrow-down"></span>
    </button>
    
</div>
        </aside>

    </main>

    <div id="card_data" style="display:none;"
         data-tabs-active=""
         data-from-search="0"
         data-restaurant-id="11814"
         data-restaurant-name="St. James - Gastro James Rosario Pino"
         data-restaurant-zipcode="28020"
         data-restaurant-city="Madrid"
         data-reservation-available="true">
    </div>
    <div id="twig_rates_data" style="display:none;"
         data-url-tabrates="http://www.eltenedor.es/restaurante/st-james-gastro-james-rosario-pino/11814//tabopiniones/2"
         data-url-rates="http://www.eltenedor.es/restaurante/st-james-gastro-james-rosario-pino/11814/contentopiniones/2"
          data-uidrestaurant="11814">
    </div>
    <div id="twig_details_data" style="display:none;"
         data-url-details="http://www.eltenedor.es/restaurante/menudetails"
         data-url-referer="">
    </div>

    <div id="popin_details" class="hide"></div>

                </div><!-- end main -->
                                                
<div id="footer" class="footer-brand-2"><div class="footer_content alt"><div id="brands" class="toggler"><div class="c-selector right toggle"><a><span class="icon flag-es"></span><span>Espagne</span><span class="icon arrow-white-down right"></span></a></div><div class="c-list hidden popin"><div class="left"><div class="brand-item"><span class="l-f" data-u="bW9jLmV0dGVoY3J1b2ZhbC53d3cvLzpwdHRo" title="Français (FR)"><span class="icon flag-fr"></span><span>France</span></span></div><div class="brand-item"><span class="l-f" data-u="aGMuZXR0ZWhjcnVvZmFsLnd3dy8vOnB0dGg=" title="Français (CH)"><span class="icon flag-ch"></span><span>Suisse</span></span></div><div class="brand-item"><span class="l-f" data-u="ZWIuZXR0ZWhjcnVvZmFsLnd3dy8vOnB0dGg=" title="Français (FR)"><span class="icon flag-be"></span><span>Belgique</span></span></div><div class="brand-item"><span class="l-f" data-u="bW9jLmtyb2ZlaHQud3d3Ly86cHR0aA==" title="English (COM)"><span class="icon flag"></span><span>International</span></span></div><div class="brand-item"><span class="l-f" data-u="dGkua3JvZmVodC53d3cvLzpwdHRo" title=""><span class="icon flag-it"></span><span>Italie</span></span></div></div></div></div><div class="row-fluid"><div class="span2 m_b"><div class="subtitle">Sobre nosotros</div><ul><li><span class="l-f "   data-u="L21vYy5ldHRlaGNydW9mYWxnb2xiLnN1b25zZW1tb3NpdXEvLzpwdHRo" >¿Quiénes somos?</span></li><li><span class="l-f "   data-u="YXpuYWlmbm9jLXV0LXNvbWFpbWVycC9zZS5yb2RlbmV0bGUud3d3Ly86cHR0aA==" >Programa Yums</span></li><li><span class="l-f "   data-u="YXNuZXJwL3NlLnJvZGVuZXRsZS53d3cvLzpwdHRo" >En la prensa</span></li><li><span class="l-f "  data-target="b" data-u="b3RuZWltYXR1bGNlci9zZS5yb2RlbmV0bGUud3d3Ly86cHR0aA==" >¡Trabaja en eltenedor.es!</span></li><li><span class="l-f "  data-target="b" data-u="c2Uucm9kZW5ldGxlLmdvbGIvLzpwdHRo" >Blog eltenedor</span></li></ul></div><div class="span2 m_b"><div class="subtitle">¿Necesitas ayuda?</div><ul><li><span class="l-f "   data-u="b3RjYXRub2Mv" >Contacto</span></li><li><span class="l-f "   data-u="cWFmL3NlLnJvZGVuZXRsZS53d3cvLzpwdHRo" >Preguntas frecuentes</span></li><li><span class="l-f "   data-u="bGFnZWwvYW5pZ2FwL3NlLnJvZGVuZXRsZS53d3cvLzpwdHRo" >Condiciones legales</span></li><li><span class="l-f "   data-u="c2Vpa29vYy1lZC1hY2l0aWxvcC9zZS5yb2RlbmV0bGUud3d3Ly86cHR0aA==" >Política de cookies</span></li><li><span class="l-f "   data-u="b2l0aXMtbGVkLWFwYW0vc2Uucm9kZW5ldGxlLnd3dy8vOnB0dGg=" >Mapa Web</span></li></ul></div><div class="span2 m_b"><div class="subtitle">Móvil</div><ul><li><span class="l-f "  data-target="b" data-u="OD10bT84MDkwNTg0MjRkaS9zZS1yb2RlbmV0bGUvcHBhL3NlL21vYy5lbHBwYS5zZW51dGkvLzpzcHR0aA==" >App iPhone</span></li><li><span class="l-f "  data-target="b" data-u="ZXR0ZWhjcnVvZmFsLmV0dGVoY3J1b2ZhbC5tb2M9ZGk/c2xpYXRlZC9zcHBhL2Vyb3RzL21vYy5lbGdvb2cueWFscC8vOnNwdHRo" >App Android</span></li><li><span class="l-f "  data-target="b" data-u="YzZjM2MwNDczODNlLTk2MzgtY2JkNC01ODljLTQ0OWI3YTNkL2V0dGVoY3J1b2ZhbC9wcGEvZXJvdHMvc2Utc2UvbW9jLmVub2hwc3dvZG5pdy53d3cvLzpwdHRo" >App Windows Phone</span></li><li><span class="l-f "   data-u="c2Uucm9kZW5ldGxlLm0vLzpwdHRo" >Web móvil</span></li></ul></div><div class="span2 m_b"><div class="subtitle">Espacio profesionales</div><ul><li><span class="l-f "  data-target="b" data-u="c2Uub2ZuaS1yb2RlbmV0aW0ud3d3Ly86cHR0aA==" >Espacio restaurantes</span></li><li><span class="l-f "   data-u="c2F6bmFpbGEvc2Uucm9kZW5ldGxlLnd3dy8vOnB0dGg=" >Alianzas</span></li><li><span class="l-f "   data-u="c2Uucm9kZW5ldGxlLnN1bHAuYWlyZWJpLy86cHR0aA==" >Iberia Plus</span></li></ul></div></div><div class="row-fluid"><div class="span7 copy"><p class="small">&copy; 2006 - 2015 eltenedor.es<br />LA FOURCHETTE ESPAÑA, S.L. C/ Benito Gutiérrez, 32 1ª planta 28008 – Madrid. NIF B-85364628.<br/>Registro Mercantil de Madrid, Hoja M-458011, Tomo 25424, Folio 168. @ info@eltenedor.es</p><p class="small">*Ver condiciones y validez en la ficha del restaurante.</p></div></div></div></div>                
                                            <div id="portal-general-config"
     style="display:none;"
     data-lfgm-language="es"
     data-url-loader="http://wac.12523.alphacdn.net/8012523/assets/v-526/bundles/lafourchettecore/images/wait.gif"
     data-lfgm-key="gme-tripadvisorinc"
     data-logout-url="/logout"
></div><script type="text/javascript" src="http://wac.12523.alphacdn.net/8012523/assets/v-526/js/a5eb525.js"></script><!--[if IE 6]>
<script type="text/javascript" src="http://wac.12523.alphacdn.net/8012523/assets/v-526/js/f9820ad.js"></script>
<![endif]-->
                <script type="text/javascript" src="http://wac.12523.alphacdn.net/8012523/assets/v-526/js/6ef1c6a.js"></script>                                
    

        <script type="text/javascript" src="http://wac.12523.alphacdn.net/8012523/assets/v-526/js/5649dce.js"></script>
    
    <script id="restaurantRecommendationsUnderscoreTemplate" type="text/template"><p class="recommendations-title title">Quienes vieron este restaurante también consultaron</p><div id="dynamicRecommendations" class="recommendations"><div class="recommendations-carousel"><ul><% _.each(recList, function(recommendation){ %><li class="recommendation border"><div class="recommendation-inner"><div class="recommendation-picture"><img data-src="<%= recommendation.restaurant.picsMain.replace('/80/', '/160/') %>"
                                 onerror="this._alter || (this._alter=true,this.src='http://wac.12523.alphacdn.net/8012523/assets/v-526/bundles/lafourchettecore/images/placeholder/160_120.jpg');"><% if( recommendation.restaurant.avgRate ) { %><div class="rate"><span><%= recommendation.restaurant.avgRate %></span>&nbsp;/10</div><% } %></div><p class="soft"><% if (typeof(recommendation.city.zipcode) !== 'undefined') { %><%= recommendation.city.zipcode %>&nbsp;
                            <% } %><%= _.escape(recommendation.city.name) %><% if (typeof(recommendation.restaurant.speciality) !== 'undefined') { %>
                                -&nbsp;<%= _.escape(recommendation.restaurant.speciality) %><% } %></p><% if( recommendation.saletype.isSpecialOffer || recommendation.saletype.isMenu ){ %><p class="alt promo"><span class="icon macaron-orange"></span><span>&nbsp;<%= _.escape(recommendation.saletype.title) %></span></p><% } %></div><% var url = 'http://www.eltenedor.es/restaurantev2/0/1'
                        .replace('0', recommendation.restaurant.slug)
                        .replace('1', recommendation.restaurant.idRestaurant);
                    %><a class="recommendation-button strong" href="<%= url %>"><%= _.escape(recommendation.restaurant.name) %></a></li><% }); %></ul></div><a class="recommendations-carousel-prev disabled" href="#"><span></span></a><a class="recommendations-carousel-next" href="#"><span></span></a><div class="recommendations-shadow-left"></div><div class="recommendations-shadow-right"></div></div></script>
        <div class="mboxDefault"></div>
    <script type="text/javascript" >
        LF.Adobe.MBox.trigger('recsRestaurantPage', {"id":"11814_2","restaurantId":11814,"categoryId":789,"categoryName":"Mediterr\u00e1neo","name":"St. James - Gastro James Rosario Pino","slug":"st-james-gastro-james-rosario-pino","pageUrl":"\/restaurantev2\/st-james-gastro-james-rosario-pino\/11814","thumbnailUrl":"http:\/\/uploads.lafourchette.com\/restaurant_photos\/814\/11814\/43\/80\/9346f70a7fcf13d73c9e0d6a0b110255.jpg","value":37,"message":"-50% en Carta s\u00f3lo en eltenedor.es","idCityZipcode":122209,"cityZipcode":"28020","city":"Madrid","country":"Espa\u00f1a","salePrice":19,"rating":9.1,"foodRating":9,"serviceRating":9.2,"ambianceRating":9,"qualityPriceRating":7,"saleTypeId":166577,"saleTypeName":"-50% en Carta s\u00f3lo en eltenedor.es","saleTypeDiscountAmount":50,"saleTypeIsMenu":0,"saleTypeIsSpecialOffer":1,"saleTypeMinPartySize":2,"saleTypeMaxPartySize":8,"isRecommendable":true});
    </script>
    
        <script type="text/javascript">
        LF.Restaurant.staticRecommendationsCarousel =
                new LF.Restaurant.RecommendationCarousel("clicRecoFicheResto", "reco_resto");
        $(function() {
            LF.Restaurant.staticRecommendationsCarousel.init('#staticRecommendations.recommendations');
        });
    </script>                        
        
            </div><!-- end wrapper -->
        </div><!-- end bg -->
        <div id="opaque_screen" style="display:none;"><img src="http://wac.12523.alphacdn.net/8012523/assets/v-526/bundles/lafourchettecore/images/wait.gif" alt="Please wait" id="wait_anim" width="43px" height="11px" /></div><div id="common_data" style="display:none;"
            data-back-to-top="http://wac.12523.alphacdn.net/8012523/assets/v-526/bundles/lafourchettecore/images/backtotop_es.png"
                        data-test-facebook-url="http://www.eltenedor.es/ajax/is-user-id-facebook-exist"
        ></div><ol id="tutorialContent" data-tutorial-id="restaurant-card.module.date-step" class="hide" ><li data-id="calendar" data-options="tipLocation:left" ><p><strong> Para reservar,<br />
                    elige primero una fecha.</strong></p><div class="tutorial-button-wrapper" ><button class="tutorial-button-primary close-button" >OK</button><button class="tutorial-button-secondary close-button" >Cerrar</button></div></li></ol><script type="text/javascript"></script><div class="hide"><li class="toggler" id="header_account_log"><div class="toggle"><span>Mi cuenta <span class="icon arrow-white-down"></span></span></div><div class="popin hidden"><span class="icon arrow-white-big-up"></span><ul><li><a href="/mi-cuenta/perfil">Mi perfil</a></li><li><a href="/mi-cuenta/reservas">Mis reservas</a></li><li><a id="logout_header_autoconnect" href="javascript:void(0);">Cerrar sesión</a></li></ul></div></li></div><div id="wrapperNewsletterPopin" class="hidden"><div class="popin popin-newsletter no-uniform-ready" ><a href="#" class="nyroModalClose nyroModalCloseButton" >x</a><div class="popin-header text_center"><div class="title">¡Hasta -50% en Carta!</div>
        ¿Quieres estar informado de las promociones de tu ciudad?
    </div><div class="popin-content"><form id="newsletterPopinForm" action="/newsletter/subscribe" method="post" ><div class="row-fluid"><div class="span4"><label for="la_fourchette_cms_newsletter_type_idCountry">
                        País
                    </label></div><div class="span8 full"><select id="la_fourchette_cms_newsletter_type_idCountry" name="la_fourchette_cms_newsletter_type[idCountry]" required="required"    class="span12"><option value="60">España</option><option disabled="disabled">-------------------</option><option value="236">Afghanistan</option><option value="2">Africa del Sur</option><option value="3">Albania</option><option value="5">Alemania</option><option value="6">Andorra</option><option value="7">Angola</option><option value="8">Anguila</option><option value="9">Antártida</option><option value="10">Antigua y Barbuda</option><option value="11">Antillas neerlandesas</option><option value="12">Arabia Saudí</option><option value="4">Argelia</option><option value="13">Argentina</option><option value="14">Armenia</option><option value="15">Aruba</option><option value="16">Australia</option><option value="17">Austria</option><option value="18">Azerbaiyán</option><option value="19">Bahamas</option><option value="20">Bahrein</option><option value="21">Bangladesh</option><option value="22">Barbados</option><option value="23">Bélgica</option><option value="24">Belice</option><option value="25">Benín</option><option value="26">Bermudas</option><option value="28">Bielorrusia</option><option value="29">Bolivia</option><option value="239">Bonaire, San Eustaquio y Saba</option><option value="30">Bosnia y Hercegovina</option><option value="31">Botsuana</option><option value="32">Brasil</option><option value="33">Brunei Darussalam</option><option value="34">Bulgaria</option><option value="35">Burkina Faso</option><option value="36">Burundi</option><option value="27">Bután</option><option value="40">Cabo Verde</option><option value="37">Camboya</option><option value="38">Camerún </option><option value="39">Canadá</option><option value="211">Chad</option><option value="41">Chile</option><option value="42">China</option><option value="43">Chipre</option><option value="44">Colombia</option><option value="45">Comores</option><option value="46">Congo</option><option value="47">Congo, República Democrática del</option><option value="49">Corea del Norte</option><option value="48">Corea del Sur</option><option value="51">Costa de Marfil</option><option value="50">Costa Rica</option><option value="52">Croacia</option><option value="240">Cuba</option><option value="241">Cura</option><option value="53">Dinamarca</option><option value="55">Dominica</option><option value="58">Ecuador</option><option value="56">Egipto</option><option value="57">Emiratos Arabes Unidos</option><option value="59">Eritrea</option><option value="199">Eslovaquia</option><option value="200">Eslovenia</option><option value="62">Estados Unidos de América</option><option value="61">Estonia</option><option value="63">Etiopía</option><option value="64">Federación Rusa</option><option value="170">Filipinas</option><option value="66">Finlandia</option><option value="65">Fiyis</option><option value="1">Francia</option><option value="67">Gabón</option><option value="68">Gambia</option><option value="69">Georgia </option><option value="70">Georgia del Sur y las Islas Sandwich del Sur</option><option value="71">Ghana</option><option value="72">Gibraltar</option><option value="120">Granada</option><option value="73">Grecia</option><option value="74">Groenlandia</option><option value="75">Guadalupe</option><option value="76">Guam</option><option value="77">Guatemala</option><option value="242">Guernesey</option><option value="78">Guinea</option><option value="79">Guinea-Bissau</option><option value="80">Guinea Ecuatorial</option><option value="81">Guyana</option><option value="82">Guyana francesa</option><option value="83">Haití</option><option value="168">Hollanda</option><option value="84">Honduras</option><option value="85">Hong-Kong</option><option value="86">Hungría</option><option value="106">India</option><option value="107">Indonesia</option><option value="244">Irak</option><option value="245">Irán</option><option value="108">Irlanda</option><option value="102">Isas Svalbard y Jan Mayen</option><option value="87">Isla Christmas</option><option value="88">Isla de La Reunión</option><option value="243">Isla de Man</option><option value="89">Isla Mauricio</option><option value="90">Isla Norfolk</option><option value="109">Islandia</option><option value="91">Islas Acanalador</option><option value="92">Islas Caimán</option><option value="94">Islas Cocos-Keeling</option><option value="95">Islas Cook</option><option value="237">Islas de Åland</option><option value="96">Islas Feroe</option><option value="97">Islas Heard y Mc Donald</option><option value="98">Islas Malvinas</option><option value="99">Islas Marianas del Norte</option><option value="100">Islas Marshall</option><option value="101">Islas Salomón</option><option value="103">Islas Turcos y Caicos</option><option value="104">Islas Vírgenes americanas</option><option value="105">Islas Vírgenes Británicas</option><option value="110">Israel</option><option value="111">Italia</option><option value="112">Jamaica</option><option value="113">Japón</option><option value="246">Jersey</option><option value="114">Jordania</option><option value="115">Kazajstán</option><option value="116">Kenia</option><option value="117">Kirguizistán</option><option value="118">Kiribati</option><option value="247">Kosovo</option><option value="119">Kuwait</option><option value="122">Lesoto</option><option value="123">Letonia</option><option value="124">Líbano</option><option value="125">Liberia</option><option value="126">Libia</option><option value="127">Liechtenstein</option><option value="128">Lituania</option><option value="129">Luxemburgo</option><option value="130">Macao</option><option value="131">Macedonia</option><option value="132">Madagascar</option><option value="133">Malasia</option><option value="134">Malaui</option><option value="135">Maldivas</option><option value="136">Malí</option><option value="137">Malta</option><option value="138">Marruecos</option><option value="139">Martinica</option><option value="140">Mauritania</option><option value="141">Mayotte</option><option value="142">México</option><option value="143">Micronesia, Estados Federados de</option><option value="144">Moldavia, República de</option><option value="145">Mónaco</option><option value="146">Mongolia</option><option value="248">Montenegro</option><option value="147">Montserrat</option><option value="148">Mozambique</option><option value="149">Myanmar</option><option value="150">Namibia</option><option value="151">Nauru</option><option value="152">Nepal</option><option value="153">Nicaragua</option><option value="154">Níger</option><option value="155">Nigeria</option><option value="156">Niue</option><option value="157">Noruega </option><option value="158">Nueva Caledonia</option><option value="159">Nueva Zelanda</option><option value="160">Omán</option><option value="256">Otros pais</option><option value="163">Pakistán</option><option value="164">Palau</option><option value="165">Panamá</option><option value="166">Papúa Nueva Guinea</option><option value="167">Paraguay</option><option value="169">Perú</option><option value="171">Pitcairn</option><option value="173">Polinesia Francesa</option><option value="172">Polonia</option><option value="174">Portugal</option><option value="175">Puerto Rico</option><option value="176">Qatar</option><option value="182">Reino Unido</option><option value="177">República Centroafricana</option><option value="180">República Checa</option><option value="178">República Democrática Popular de Laos</option><option value="179">República Dominicana</option><option value="183">Ruanda</option><option value="181">Rumania</option><option value="193">Säo Tome y Prìncipe </option><option value="184">Sahara Occidental</option><option value="190">Salvador</option><option value="192">Samoa (Independiente),</option><option value="191">Samoas Americana</option><option value="238">San Bartolomé</option><option value="188">San Marino</option><option value="249">San Martín</option><option value="203">San Pedro y Miquelón</option><option value="189">San Vincente y Granadinas</option><option value="185">Santa Elena</option><option value="187">Santa Lucía</option><option value="186">Santo Kitts y Nevis</option><option value="194">Senegal</option><option value="251">Serbia</option><option value="195">Serbia y Montenegro</option><option value="196">Seychelles</option><option value="197">Sierra Leona</option><option value="198">Singapur</option><option value="254">Sint Maarten</option><option value="255">Siria</option><option value="201">Somalia</option><option value="202">Sri Lanka</option><option value="207">Suazilandia</option><option value="252">Sudán</option><option value="253">Sudán del Sur</option><option value="204">Suecia</option><option value="205">Suiza</option><option value="206">Surinam</option><option value="214">Tailandia</option><option value="209">Taiwán</option><option value="210">Tanzania</option><option value="208">Tayikistán</option><option value="212">Territorios Británicos Indios</option><option value="213">Territorios Franceses del Sur</option><option value="250">Territorios Palestinos</option><option value="215">Timor Este</option><option value="216">Togo</option><option value="217">Tokelau</option><option value="218">Tonga</option><option value="220">Trinidad y Tobago</option><option value="221">Túnez</option><option value="222">Turkmenistán</option><option value="223">Turquía</option><option value="224">Tuvalu</option><option value="225">Ucrania</option><option value="161">Uganda</option><option value="226">United States Minor Outlying Islands</option><option value="227">Uruguay</option><option value="162">Uzbekistán</option><option value="228">Vanuatu</option><option value="229">Vaticano</option><option value="230">Venezuela</option><option value="231">Vietnam</option><option value="232">Wallis y Futuna</option><option value="233">Yemen</option><option value="54">Yibuti</option><option value="234">Zambia</option><option value="235">Zimbabue</option></select></div></div><div class="row-fluid"><div class="span4"><label for="la_fourchette_cms_newsletter_type_zipcode">
                        Código Postal
                    </label></div><div class="span8"><input type="text" id="la_fourchette_cms_newsletter_type_zipcode" name="la_fourchette_cms_newsletter_type[zipcode]" required="required"    class="span12" /></div></div><div class="row-fluid"><div class="span4"><label for="la_fourchette_cms_newsletter_type_email">
                        Email
                    </label></div><div class="span8"><input type="email" id="la_fourchette_cms_newsletter_type_email" name="la_fourchette_cms_newsletter_type[email]" required="required"    class="span12" /></div></div><div class="text_center suggest"><input type="submit" class="btn green" value="Recibe nuestra Newsletter" /></div><div class="legal" >LA FOURCHETTE ESPAÑA, S.L. c/ Benito Gutiérrez, 32 28008 Madrid, almacenará sus datos para gestionar el envío de la Newsletter. Ud. dispone de los derechos de acceso, rectificación, cancelación y oposición según lo establecido en la L.O. 15/99 de Protección de Datos de Carácter Personal, que podrá ejercer dirigiendo una comunicación escrita, debidamente firmada y con copia del DNI, a la dirección antes indicada o a través de contacto@eltenedor.es.</div></form></div></div></div><div class="fbar fbar-cookie-policy"><a href="#" class="cookie-policy-close" ></a><div class="fbar-content">Utilizamos cookies propias y de terceros para mejorar tu experiencia y nuestros servicios. Si continúas navegando, consideramos que aceptas su uso. Puedes cambiar la configuración u obtener más información sobre nuestra política de cookies <a href="/politica-de-cookies" target="_blank">aquí</a>.</div></div><div id="ipadAppPopin" class="hidden" data-message="Descarga la aplicación iPad para mejorar tu experiencia eltenedor.es" ></div><div id="monitoring" class="hide"></div>
                <script type="text/javascript">_satellite.pageBottom();</script>
    <script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"beacon-2.newrelic.com","licenseKey":"039d319660","applicationID":"2327907","transactionName":"YFxTNkMDXkVZVkwIVlkWchdCFl9bF3lZJ1ZCS1IKVBZEU2pQSxVYQktQDEUgRVhcWV0+S1JKRQNEEFFYTGpRD11SQW4UAw==","queueTime":0,"applicationTime":185,"ttGuid":"","agentToken":"","userAttributes":"","errorBeacon":"bam.nr-data.net","agent":"js-agent.newrelic.com\/nr-476.min.js"}</script></body>
</html>
