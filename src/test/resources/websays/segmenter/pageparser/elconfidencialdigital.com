<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Language" content="es" />
<meta property="og:site_name" content="El Confidencial Digital" />
<meta property="og:title" content="Botín, FG y Galán no consiguen tranquilizar en Davos sobre los peligros de Podemos" />
<meta property="og:url" content="http://www.elconfidencialdigital.com/dinero/Botin-FG-Galan-Davos-Podemos_0_2425557457.html" />
<meta property="og:type" content="article" />
<meta property="og:description" content="Los grandes empresarios españoles no han conseguido tranquilizar a los más importantes ejecutivos mundiales, que se han dado cita en Davos, sobre los riesgos de que Podemos tenga opciones de Gobierno. Las explicaciones de Ana Botín, Francisco González e Ignacio Sánchez Galán, en contactos privados, han resultado insuficientes." />
<meta property="og:image" content="http://images.elconfidencialdigital.com/politica/lider-Podemos-Pablo-Iglesias_ECDIMA20150126_0019_3.jpg" />
<meta name="description" content="Los grandes empresarios españoles no han conseguido tranquilizar a los más importantes ejecutivos mundiales, que se han dado cita en Davos, sobre los riesgos de que Podemos tenga opciones de Gobierno. Las explicaciones de Ana Botín, Francisco González e Ignacio Sánchez Galán, en contactos privados, han resultado insuficientes." />
<meta name="keywords" content="Botin,FG,Galan,Davos,Podemos" />
<meta name="news_keywords" content="Botin,FG,Galan,Davos,Podemos" />
<meta name="generator" content="BBT bCube" />

<title>Botín, FG y Galán no consiguen tranquilizar en Davos sobre los peligros de Podemos</title>

<link href="http://static2.elconfidencialdigital.com/static/ECDDigital/min/ecd_output.css?hash=12d57880c14a12918f0dbda7f6f1e0af" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="http://static1.elconfidencialdigital.com/static/ECDDigital/js/modernizr.custom.min.js?hash=144620cdc2125ba0afaf3c17e13139f9"></script>
<script type="text/javascript" src="http://static1.elconfidencialdigital.com/static/ECDDigital/js/jquery-1.7.2.min.js?hash=b8d64d0bc142b3f670cc0611b0aebcae"></script>
<script type="text/javascript" src="http://static1.elconfidencialdigital.com/static/BBTCore/js/bbt.js?hash=1e715a79deab6aa9995d0d27f41ec032"></script>
<script type="text/javascript" src="http://static1.elconfidencialdigital.com/static/ECDDigital/js/ecd.globals.js?hash=fb3772e1b4c60d7a59f7917124dc7e39"></script>
<script type="text/javascript" src="http://static2.elconfidencialdigital.com/static/ECDDigital/min/ecd_output.js?hash=a16dd2300d995375aa6f1b2042533d0a"></script>
<script type="text/javascript" src="http://static2.elconfidencialdigital.com/static/ECDDigital/js/bbt.video.js?hash=1ff1c6b1b33f784c438084af8be3bc1e"></script>
<script type="text/javascript" src="http://static1.elconfidencialdigital.com/static/BBTComponent/js/bbt.component.js?hash=3af7d50167907ee356ef8c9f739a7b88"></script>
<script type="text/javascript" src="http://static2.elconfidencialdigital.com/static/ECDDigital/js/jquery.cookie.js?hash=384772142d1907d7d3aea3ac11fad9d0"></script>
<script type="text/javascript" src="http://static1.elconfidencialdigital.com/static/ECDDigital/js/ecd.prj.js?hash=30ca084ba8ac1e35b286e05597fe04a8"></script>
<script type="text/javascript" src="http://static1.elconfidencialdigital.com/static/BBTComment/js/bbt.comment.js?hash=fa244783c35647cf3595f04618ad9789"></script>

<link rel="alternate" title="Dinero" href="/rss/section/1030000/" type="application/rss+xml" />



<script type="text/javascript">
  var _sf_startpt=(new Date()).getTime();
</script>

<script type="text/javascript"> try{(function(){ var cb = new Date().getTime(); var s = document.createElement("script"); s.defer = true; s.src = "//tag.crsspxl.com/s1.js?d=1772&cb="+cb; var s0 = document.getElementsByTagName('script')[0]; s0.parentNode.insertBefore(s, s0); })();}catch(e){} </script>

<script type='text/javascript'>
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
  (function() {
  var gads = document.createElement('script');
  gads.async = true;
  gads.type = 'text/javascript';
  var useSSL = 'https:' == document.location.protocol;
  gads.src = (useSSL ? 'https:' : 'http:') +
  '//www.googletagservices.com/tag/js/gpt.js';
  var node = document.getElementsByTagName('script')[0];
  node.parentNode.insertBefore(gads, node);
  })();
</script>

<script type="text/javascript">

googletag.cmd.push(function() {


googletag.defineSlot('/5028795/980x250', [980,250], 'ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_980x250').addService(googletag.pubads());

googletag.defineSlot('/5028795/DM11', [300,300], 'ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_dm11').addService(googletag.pubads());

googletag.defineSlot('/5028795/ECD_600x250', [600,250], 'ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_ecd_600x250').addService(googletag.pubads());

googletag.defineSlot('/5028795/ECD_600x250_2', [600,250], 'ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_ecd_600x250_2').addService(googletag.pubads());

googletag.defineSlot('/5028795/Epom11', [300,300], 'ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_epom11').addService(googletag.pubads());

googletag.defineSlot('/5028795/Proximia11', [300,300], 'ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_proximia11').addService(googletag.pubads());

googletag.defineSlot('/5028795/Quisma11', [300,300], 'ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_quisma11').addService(googletag.pubads());

googletag.defineSlot('/5028795/Xaxis11', [300,300], 'ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_xaxis11').addService(googletag.pubads());

googletag.defineSlot('/5028795/inventario10A', [234,90], 'ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario10a').addService(googletag.pubads());

googletag.defineSlot('/5028795/inventario10B', [234,90], 'ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario10b').addService(googletag.pubads());

googletag.defineSlot('/5028795/inventario10C', [234,90], 'ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario10c').addService(googletag.pubads());

googletag.defineSlot('/5028795/inventario11', [300,300], 'ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario11').addService(googletag.pubads());

googletag.defineSlot('/5028795/inventario11a', [300,300], 'ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario11a').addService(googletag.pubads());

googletag.defineSlot('/5028795/inventario11b', [300,600], 'ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario11b').addService(googletag.pubads());

googletag.defineSlot('/5028795/inventario12', [300,600], 'ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario12').addService(googletag.pubads());

googletag.defineSlot('/5028795/inventario12a', [300,1000], 'ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario12a').addService(googletag.pubads());

googletag.defineSlot('/5028795/inventario14A', [980,90], 'ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario14a').addService(googletag.pubads());

googletag.defineSlot('/5028795/inventario14B', [980,90], 'ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario14b').addService(googletag.pubads());

googletag.defineSlot('/5028795/inventario3', [300,250], 'ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario3').addService(googletag.pubads());

googletag.defineSlot('/5028795/inventario4', [300,250], 'ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario4').addService(googletag.pubads());

googletag.defineSlot('/5028795/inventario4a', [300,250], 'ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario4a').addService(googletag.pubads());

googletag.defineSlot('/5028795/inventario4aa', [300,250], 'ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario4aa').addService(googletag.pubads());

googletag.defineSlot('/5028795/inventario5', [728,90], 'ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario5').addService(googletag.pubads());

googletag.defineSlot('/5028795/inventario5B', [728,90], 'ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario5b').addService(googletag.pubads());

googletag.defineSlot('/5028795/inventario5C', [728,90], 'ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario5c').addService(googletag.pubads());

googletag.defineSlot('/5028795/inventario6a', [728,90], 'ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario6a').addService(googletag.pubads());

googletag.defineSlot('/5028795/ub2x2', [1,1], 'ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_ub2x2').addService(googletag.pubads());


   googletag.pubads().enableSingleRequest();
   googletag.enableServices();
});

</script>




<link rel="canonical" href="http://www.elconfidencialdigital.com/dinero/Botin-FG-Galan-Davos-Podemos_0_2425557457.html"/>
<link rel="image_src" type="image/jpeg" href="http://images.elconfidencialdigital.com/politica/lider-Podemos-Pablo-Iglesias_ECDIMA20150126_0019_3.jpg"/>
</head>
<!-- t = 1422316824 -->
<body>
<!-- Google Tag Manager -->
<noscript>
  <iframe src="//www.googletagmanager.com/ns.html?id=GTM-W3V7Q7" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>

<script>
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W3V7Q7');
</script>
<!-- End Google Tag Manager -->


<div class="md-cookies-advice" id="headerCookiesAdvice" style="display:none;">

  <div class="limiter">
    <p class="edi-header-message">
      <strong>elconfidencialdigital.com</strong>
      utiliza cookies para que tenga la mejor experiencia de navegación. Si sigue navegando entendemos que acepta nuestra política de cookies.</p>
    <p class="edi-header-buttonline">
      <a class="edi-header-button-close btn" title="OK" href="">OK</a>
    </p><!-- .edi-header-buttonline -->
  </div><!-- .edi-limiter -->

</div><!-- .md-cookies-advice #headerCookiesAdvice -->

<script type="text/javascript">

	var advice = jQuery("#headerCookiesAdvice");

	var closeButton = advice.find(".edi-header-button-close");

	var cookie = jQuery.cookie('2ac7990e91acc3ddb0d300827dd43a8c');

	if (cookie == null) {
	    jQuery("#headerCookiesAdvice").show();
	    closeButton.click(function(e){
		var date = new Date();
	        date.setTime(date.getTime() + (60 * 60 * 24 * 365 * 10 * 1000));
                jQuery.cookie("2ac7990e91acc3ddb0d300827dd43a8c", "valcookiemsg", { expires: date, path: "/", domain: ".elconfidencialdigital.com" });
	        e.preventDefault();
		e.stopPropagation();
		advice.slideUp("fast");
	    });
	}
</script>


<div id="main" class="document1">

  

  <div id="container">

    <div id="ad" class="rg">
  <div class="limiter limiter2">

    
<div class="ad-hw sk-ad-full">
  <dl class="ad-wrap cf">

    <dt class="caption"><span>·Publicidad·</span></dt>

    <dd class="ct">
      <div class="ad">

	<div id="ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_980x250" class="ecd-adv-container" >
	</div><!-- #ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_980x250 -->

      </div><!-- .ad -->
    </dd><!-- .ct -->

  </dl><!-- .ad-wrap -->
</div><!-- .ad-hw -->





  </div><!-- .limiter -->
</div><!-- #ad -->


    <!-- REGION HEADER (MAX) -->
<div id="hd" class="rg">
  <div class="limiter limiter1">

    <div class="masthead uncollapse cf">

      <p class="timestamp">
  Martes 27/01/2015. <span class="updated">Actualizado <span class="time">13:06h</span></span>
</p>


      <!--
    SNIPPET / PARTIAL
    SOCIAL (HEADER)
  -->
<div class="main-social cf">
  <ul class="lst-main-social">
    <li class="item facebook">
      <iframe src="//www.facebook.com/plugins/like.php?href=https://www.facebook.com/pages/El-Confidencial-Digital/202726949863885&amp;send=false&amp;layout=button_count&amp;width=120&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font&amp;height=21&amp;appId=137484743414" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:120px; height:21px;" allowTransparency="true"></iframe>
    </li>
    <li class="item twitter">
      <a href="https://twitter.com/ecd_" class="twitter-follow-button" data-show-count="false" data-lang="es" data-show-screen-name="false">Seguir a @ecd_</a>
      <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
    </li>
    <li class="item google">
      <g:plusone size="medium" annotation="none"></g:plusone>
      <script type="text/javascript">
	window.___gcfg = {lang: 'es'};

	(function() {
	var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
	po.src = 'https://apis.google.com/js/plusone.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
	})();
      </script>
    </li>
    <li class="item rss">
      <a href="/rss/section/1030000/">
	<img src="/static/ECDDigital/images/ico/ico-rss.png" alt="this image alt" width="51px" height="20px" />
      </a>
    </li>
  </ul>
</div><!-- .main-social -->


      <div class="inner cf">
	<div class="brand">
	  <h1 class="main logo">

	    <img alt="elconfidencialdigital.com" class="logo-print" src="http://static1.elconfidencialdigital.com/static/ECDDigital/images/logo-print.png?hash=a7ac190771d3615be69c0f0dccc5d610">

	    <a class="lnk" href="/" title="home">
	      <span class="name">elconfidencialdigital.com</span>
	    </a>

	  </h1><!-- .main.logo -->

	  
	  
	  <p class="slogan">La web de las personas informadas que desean estar más informadas</p>
	  

	</div><!-- .brand -->

	<div class="aux cf">
	  <div class="aux-tools">
	    <!--
	SNIPPET / PARTIAL
	MAIN SEARCH (HEADER)
 -->
<div class="main-search">
	<form id="mainSearchForm" name="mainSearchForm" method="get" action="/buscador/">
		<fieldset>
			<legend>Busque en El Confidencial Digital:</legend>
			<span class="sk-field">
				<label for="mainSearchField">búsqueda</label>
				<input type="text" class="search-field ecd-search-text" id="mainSearchField" name="text" />
				<a class="bt-submit btk ecd-search-submit" href="#" title="Buscar noticias,fotos ..." onClick="searchSubmit(jQuery('#mainSearchForm'));return false;"><span>Buscar</span></a>
			</span>
		</fieldset>
	</form>
	<script type="text/javascript">
	 <!--
	     var jObj = jQuery("#mainSearchForm");
	     FBSE.simpleSearchForm(jObj, {
	       hint: 'Buscar noticias,fotos ...',
	       searchField : "#mainSearchField",
	       submit : false
	     });

	     function searchSubmit (_jObj) {

	        var jObj = _jObj;

		var url = jObj.attr("action");
	        var jObjText = jObj.find(".ecd-search-text");
	        var text = jObjText.val();

		if (text != ""
		    && text != jObj.find(".ecd-search-submit").attr("title")) {
		    url += "?text=" + text;
		}

		window.location.href = url;

		return false;
             }
	 //-->
	</script>
</div><!-- .main-search -->

	    <ul class="nav-external">

  <li class="">
    <a class="lnk" title="El Confidencial Autonómico" rel="external" href="http://www.elconfidencialautonomico.com/">El Confidencial Autonómico</a>
  </li><!-- .item -->

  <li class="">
    <a class="lnk" title="Monarquía Confidencial" rel="external" href="http://www.monarquiaconfidencial.com/">Monarquía Confidencial</a>
  </li><!-- .item -->

  <li class="">
    <a class="lnk" title="Religión Confidencial" rel="external" href="http://www.religionconfidencial.com/">Religión Confidencial</a>
  </li><!-- .item -->

</ul><!-- .nav-external -->

	  </div><!-- .aux-tools -->
	</div><!-- .aux -->
      </div><!-- .inner -->

    </div><!-- .masthead -->

    <div id="nav" class="main-nav">

  <ul class="navigation cf">
    
    <li class="nav-item "><a class="lnk inner" href="/" title="Portada">Portada</a></li>
    
    <li class="nav-item "><a class="lnk inner" href="/muy_confidencial/el_chivato/" title="Muy Confidencial">Muy Confidencial</a></li>
    
    <li class="nav-item "><a class="lnk inner" href="/politica/" title="Pol&iacute;tica">Política</a></li>
    
    <li class="nav-item "><a class="lnk inner" href="/medios/" title="Medios">Medios</a></li>
    
    <li class="nav-item "><a class="lnk inner" href="/defensa/" title="Defensa">Defensa</a></li>
    
    <li class="nav-item selected"><a class="lnk inner" href="/dinero/" title="Dinero">Dinero</a></li>
    
    <li class="nav-item "><a class="lnk inner" href="/seguridad/" title="Seguridad">Seguridad</a></li>
    
    <li class="nav-item "><a class="lnk inner" href="/casas_reales/" title="Casas Reales">Casas Reales</a></li>
    
    <li class="nav-item "><a class="lnk inner" href="/deportes/" title="Deportes">Deportes</a></li>
    
    <li class="nav-item "><a class="lnk inner" href="/vivir/" title="Vivir">Vivir</a></li>
    
    <li class="nav-item "><a class="lnk inner" href="/opinion/tribuna_libre/" title="Opini&oacute;n">Opinión</a></li>
    
    <li class="nav-item "><a class="lnk inner" href="/videos/" title="V&iacute;deos">Vídeos</a></li>
    
    <li class="nav-item "><a class="lnk inner" href="/te_lo_aclaro/" title="Te lo aclaro">Te lo aclaro</a></li>
    
    <li class="nav-item "><a class="lnk inner" href="/yaLoDijo/" title="Ya lo dijo">Ya lo dijo</a></li>
    
  </ul><!-- .navigation -->
</div><!-- #nav.main-nav -->

<script type="text/javascript">
  ECD.mainNavigation.init(jQuery("#nav"));
</script>

    <div id="subnav" class="submain-nav">

  <ul class="navigation cf">


    

    
    
    <li class="nav-item ">
      <a class="lnk inner" href="http://www.elconfidencialdigital.com/etiqueta/carlos_herrera/" title="Carlos Herrera" target="_blank" >Carlos Herrera</a>
    </li>
    
    
    
    <li class="nav-item ">
      <a class="lnk inner" href="http://www.elconfidencialdigital.com/etiqueta/pedro_j-_ramirez/" title="Pedro J. Ram&iacute;rez" target="_blank" >Pedro J. Ramírez</a>
    </li>
    
    
    
    <li class="nav-item ">
      <a class="lnk inner" href="http://www.elconfidencialdigital.com/etiqueta/mercadona/" title="Mercadona" target="_blank" >Mercadona</a>
    </li>
    
    
    
    <li class="nav-item ">
      <a class="lnk inner" href="http://www.elconfidencialdigital.com/etiqueta/reina_letizia/" title="Reina Letizia" target="_blank" >Reina Letizia</a>
    </li>
    
    
    
    <li class="nav-item ">
      <a class="lnk inner" href="http://www.elconfidencialdigital.com/etiqueta/amancio_ortega/" title="Amancio Ortega" target="_blank" >Amancio Ortega</a>
    </li>
    
    
    
    <li class="nav-item ">
      <a class="lnk inner" href="http://www.elconfidencialdigital.com/etiqueta/guardia_civil/" title="Guardia Civil" target="_blank" >Guardia Civil</a>
    </li>
    
    
    
    <li class="nav-item ">
      <a class="lnk inner" href="http://www.elconfidencialdigital.com/etiqueta/podemos/" title="Podemos" target="_blank" >Podemos</a>
    </li>
    
    
    
    <li class="nav-item last-item">
      <a class="lnk inner" href="http://www.elconfidencialdigital.com/etiqueta/charlie_hebdo/" title="Charlie Hebdo" target="_blank" >Charlie Hebdo</a>
    </li>
    
    

  </ul><!-- .navigation -->

</div><!-- #nav.main-nav -->

<script type="text/javascript">
  ECD.mainNavigation.init(jQuery("#subnav"));
</script>


  </div><!-- .limiter -->
</div><!-- #hd -->


    <div id="bd" class="rg">
      <div class="limiter limiter1">
	
<div id="content" class="pg-story pg-story-sk2 pg">
  <div class="gd gd3-c1 skin-hr">
    <div class="gd-aux faux-g3c1a">

      <!-- MASTER GRID UNIT 2/3 (COL LEFT) -->
      <div class="gdu u2-3-c1">

	<div class="section-title-wrapper">
  <h2 class="section-title">Dinero</h2>
</div><!-- .section-title-wrapper -->


	<!-- PAGE HEADING -->
	<div class="pg-hd">

	  

	  <p class="hammer">Perciben a Pablo Iglesias como “más extremista” que Syriza</p>

	  
	  
	  <p class="dateline">
	    <small class="timestamp">27/01/2015</small>
	  </p><!-- .dateline -->
	  


	  <div class="deck fr">
	    <a href="#">
	      <span class="comment-count">
		<abbr class="count iblk bb-numComments-published">0</abbr>
	      </span>
	    </a>
	  </div>

	  <h2 class="headline mid">Botín, FG y Galán no consiguen tranquilizar en Davos sobre los peligros de Podemos</h2>

	  
	  

	  

	  <p class="tx subhead">Ejecutivos mundiales les preguntaron en contactos privados. Respondieron que &ldquo;Espa&ntilde;a es un pa&iacute;s m&aacute;s maduro y moderado que Grecia&rdquo; pero eso no les calm&oacute;</p>

	  <div class="cp-tools">

  <ul class="cf">

    <li class="iblk">
      <ul class="itools cf">
	<li class="it api fbLikeXFBML" style="margin-bottom:5px;">
	  <span class="apitx">Facebook (Me gusta)</span>
	  <script src="http://connect.facebook.net/es_ES/all.js#xfbml=1"></script><fb:like href="" send="false" layout="button_count" width="120" height="20" show_faces="false" font=""></fb:like>
	</li>

	<li class="it api twTweet" style="margin-bottom:5px;">
	  <span class="apitx">Tweetea!</span>

	  
	  
	  <a href="http://twitter.com/share" class="twitter-share-button" data-count="horizontal" data-via="ecd_" data-text="Botín, FG y Galán no consiguen tranquilizar en Davos sobre los peligros de Podemos" data-url="http://www.elconfidencialdigital.com/_909311d1" data-counturl="http://www.elconfidencialdigital.com/dinero/Botin-FG-Galan-Davos-Podemos_0_2425557457.html" data-lang="es">Tweet</a>
	  

	  <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>

	</li>

	<li class="it api gPlusOne" style="margin-bottom:5px;">
	  <span class="apitx">Google Plus One</span>
	  <script type="text/javascript" src="https://apis.google.com/js/plusone.js">{lang: 'es'}</script>

	  <g:plusone size="medium"  ></g:plusone>
	</li>
      </ul><!-- .itools -->
    </li>

    <li class="iblk">
      <dl class="adjtools ico-tools">
	<dt class="join-title">Compartir:</dt>
	<dd class="join-items">
	  <ul>
	    <li class="it"><span class="ico ath"><a class="ico-tools-print" href="#" title="Imprimir">Imprimir</a></span></li>
            <li class="it"><span class="ico ath"><a class="addthis_button_email" href="#" title="enviar a un amigo">Enviar a un amigo</a></span></li>
	    <li class="it"><span class="ico ath"><a class="addthis_button_twitter">Tweeter</a></span></li>
	    <li class="it"><span class="ico ath"><a class="addthis_button_facebook">Facebook</a></span></li>
	    <li class="it"><span class="ico ath"><a class="addthis_button_meneame">Meneame</a></span></li>
	    <li class="it"><span class="ico ath"><a class="addthis_button_compact">Compartir</a></span></li>
	  </ul>
	</dd>
      </dl><!-- .adjtools -->
    </li>
	<!-- ECDNWS20150126_0037.bb-article-body -->
  </ul>

</div><!-- .cp-tools -->
<script type="text/javascript">
  jQuery("a.ico-tools-print").click( function() {
      window.print();
      return false;
  });
</script>


	  <div class="deck cf"><p>Los grandes empresarios españoles no han conseguido tranquilizar a los más importantes ejecutivos mundiales, que se han dado cita en Davos, sobre los riesgos de que Podemos tenga opciones de Gobierno. Las explicaciones de Ana Botín, Francisco González e Ignacio Sánchez Galán, en contactos privados, han resultado insuficientes.</p></div>

	  <div class="cp-toolbar md">

  <div class="cp-tags">
    <dl class="tags cf">
      <dt class="label fl">Etiquetas</dt>
      <dd class="mt">
	
	<!-- TODO enlace al tag -->
	<a href="/etiqueta/ana_botin/" rel="tag" title="Ana Botin">Ana Botin</a>, 
	
	<!-- TODO enlace al tag -->
	<a href="/etiqueta/pablo_iglesias/" rel="tag" title="Pablo Iglesias">Pablo Iglesias</a>, 
	
	<!-- TODO enlace al tag -->
	<a href="/etiqueta/podemos/" rel="tag" title="Podemos">Podemos</a>, 
	
	<!-- TODO enlace al tag -->
	<a href="/etiqueta/syriza/" rel="tag" title="Syriza">Syriza</a>, 
	
	<!-- TODO enlace al tag -->
	<a href="/etiqueta/ignacio_sanchez_galan/" rel="tag" title="Ignacio S&aacute;nchez Gal&aacute;n">Ignacio Sánchez Galán</a>, 
	
	<!-- TODO enlace al tag -->
	<a href="/etiqueta/grecia/" rel="tag" title="Grecia">Grecia</a>, 
	
	<!-- TODO enlace al tag -->
	<a href="/etiqueta/francisco_gonzalez/" rel="tag" title="Francisco Gonz&aacute;lez">Francisco González</a>, 
	
	<!-- TODO enlace al tag -->
	<a href="/etiqueta/davos/" rel="tag" title="Davos">Davos</a>
	
      </dd><!-- .mt -->
    </dl><!-- .tags -->
  </div><!-- .cp-tags -->

</div><!-- .cp-toolbar -->


	  
<div class="pg-obj mg">
  <p>

    
<img src="http://images.elconfidencialdigital.com/politica/lider-Podemos-Pablo-Iglesias_ECDIMA20150126_0019_16.jpg" width="641" height="341" alt="El l&iacute;der de Podemos, Pablo Iglesias." />


    <span class="caption strap">
  <span class="caption-inner">
    El líder de Podemos, Pablo Iglesias. 
  </span><!-- .inner -->
</span><!-- .caption.strap -->


  </p>
</div><!-- .mg.pg-obj -->








	</div><!-- .pg-hd -->

	<div class="pg-bd cf">

	  <div class="icol-wrapper cf">
	    <div class="icol-main">

	      

	      <div class="tx mce">
		<div class="mce-body mce">
  <p>“En Europa están surgiendo <strong>partidos como Podemos en España</strong>”. Fue la advertencia que lanzó este fin de semana el influyente inversor estadounidense <strong>Ray Dalio</strong>, máximo ejecutivo del fondo<strong> Bridgewater</strong>, en uno de los debates del foro económico de Davos.</p><p>Realizó estas declaraciones, en las que destacó su honda preocupación por este asunto, delante de Ana Botín. Sin embargo, la presidenta del Banco Santander <strong>no quiso responder en público </strong>a la alusión de Dalio a Podemos.</p><h3><em><strong>Apartes privados</strong></em></h3><p>Según ha sabido <strong><em>El Confidencial Digital</em></strong>, de miembros de la delegación española recién llegados de Davos, los presidentes del <strong>Santander, BBVA e Iberdrola</strong> sí han mantenido apartes, en privado, con máximos ejecutivos de los principales fondos y bancos de inversión extranjeros, como <strong>Goldman Sachs, Blackrock y Bridgewater</strong>.</p><p>Estos líderes mundiales les mostraron sin rodeos su inquietud sobre los peligros que, desde su punto de vista, puede acarrear que Podemos tenga <strong>opciones de Gobierno</strong>, no solo para España sino también para el resto de Europa.</p><h3><em><strong>Los mensajes de los ‘vips’ españoles</strong></em></h3><p>Las fuentes a las que ha tenido acceso <strong><em>ECD</em></strong> revelan que los presidentes de las principales empresas españolas transmitieron a sus interlocutores que están "convencidos" de que los españoles van a <strong>poner el Gobierno en manos de dirigentes que aseguren la estabilidad <div class="quote right">
  <div class="inner">
    <p>Algunos líderes mundiales llegaron a calificar de “extremista” al partido de Pablo Iglesias</p>
    </div>
  </div>
</strong class="mce">política, entre otras cosas para no amenazar la recuperación económica.</p><p>Argumentaron también que la situación de Grecia, tras el triunfo del partido de Syriza, servirá también para resaltar los peligros del populismo y, antes de acudir a las urnas en las distintas convocatorias electorales de este 2015, ayudará a meditar a la mayoría de la población española, a la que<strong> consideran "más moderada que la griega”.</strong></p><h3><em><strong>No consiguen convencer sobre Podemos</strong></em></h3><p>Según las fuentes consultadas por <strong><em>ECD</em></strong>, las respuestas que los presidentes de las compañías más importantes del país han escuchado sobre este foco de preocupación <strong>no son positivas</strong>.</p><p>Estos <strong>contactos privados en Davos</strong> han quedado lejos del objetivo de calmar a los inversores internacionales sobre los riesgos de Podemos.</p><p>Máximos ejecutivos, especialmente de fondos norteamericanos, aseguraron a los directivos españoles que<strong> no les tranquiliza nada el que se deje sin más en manos de los ciudadanos </strong>la elección de los dirigentes en los próximos comicios, es decir, sin que antes se desarrolle una labor de explicación y de pedagogía hacia los españoles sobre qué futuro les aguarda si el partido de Pablo Iglesias llega a gobernar.</p><p>Consideran que confiar sin más en el buen sentido de la ciudadanía no ayuda a despejar las <strong>dudas sobre la estabilidad </strong>del país a corto plazo.</p><p>Los interlocutores les recomendaron que expliquen a la población que<strong> lo más responsable será votar a políticos </strong>que se muestren decididos a tomar las medidas que consoliden la recuperación. Hacerles ver que es necesario respaldar, en definitiva, un programa político que <strong>garantice la estabilidad y la salida de la crisis</strong>.</p><h3><em><strong>Más radical que Syriza</strong></em></h3><p>Algunos de estos líderes mundiales, con los que charlaron durante la reunión de Davos, llegaron a calificar de <strong>“extremista” al partido de Pablo Iglesias</strong>.</p><p>Lo consideran incluso más radical que a la izquierda griega, representada por Syriza, sobre todo por la “indefinición” de sus propuestas.</p><p>Un análisis que <strong>llamó poderosamente la atención</strong> a los directivos españoles que lo escucharon.</p><h3><em><strong>El problema catalán se da por amortizado</strong></em></h3><p>Pero la <strong>delegación española</strong> ha regresado, al menos, con dos mensajes positivos que ha recibido de los ejecutivos mundiales con los que ha establecido contactos en Davos.</p><p>La economía española ha recibido <strong>elogios de personalidades de alto nivel</strong> que no se habían escuchado en los últimos cinco años.</p><p>Otra de las buenas noticias es que<strong> Cataluña ha pasado a un segundo plano en Davos</strong>, tras el no en el referéndum escocés&nbsp;</p><p>Los inversores dan por buena la versión del Gobierno de que la consulta del 9-N no fue más que un simulacro y que, en todo caso, <strong>la mayoría de los catalanes no acudieron a las urnas</strong>. Lo que se interpreta como “todo un portazo a la independencia”.</p>
</div><!-- .mce-body -->

	      </div><!-- .tx.mce -->

	    </div><!-- .icol-main -->

	    <div class="icol-rt sticked-content">
	      
	      
	      <ul class="social-col newsocial md">

  <li class="lst ecd-social-fb">
    <a class="lnk ico-fb" href="http://www.facebook.com/share.php?u=http://www.elconfidencialdigital.com/dinero/Botin-FG-Galan-Davos-Podemos_0_2425557457.html" title="Facebook"><span class="ico"> </span><span class="tx">Facebook</span></a>
  </li><!-- /.lst -->

  
  
  <li class="lst ecd-social-tw">
    <a class="lnk ico-tw" href="https://twitter.com/share?url=http://www.elconfidencialdigital.com/_909311d1&via=ecd_&text=Botín, FG y Galán no consiguen tranquilizar en Davos sobre los peligros de Podemos" title="Twitter"><span class="ico"> </span><span class="tx">Twitter</span></a>
  </li><!-- /.lst -->
  

  <li class="lst ecd-social-go">
    <a class="lnk ico-gp" href="https://plus.google.com/share?url=http://www.elconfidencialdigital.com/dinero/Botin-FG-Galan-Davos-Podemos_0_2425557457.html" title="Google Plus"><span class="ico"> </span><span class="tx">Google Plus</span></a>
  </li><!-- /.lst -->

  <li class="lst ecd-social-linkedin">
    <a class="lnk ico-ln" href="http://www.linkedin.com/shareArticle?url=http://www.elconfidencialdigital.com/dinero/Botin-FG-Galan-Davos-Podemos_0_2425557457.html" title="Linked In"><span class="ico"> </span><span class="tx">Linked In</span></a>
  </li><!-- /.lst -->

  <li class="lst ecd-social-men">
    <a class="lnk ico-mn" href="http://www.meneame.net/submit.php?url=http://www.elconfidencialdigital.com/dinero/Botin-FG-Galan-Davos-Podemos_0_2425557457.html" title="Menéame"><span class="ico"> </span><span class="tx">Menéame</span></a>
  </li><!-- /.lst -->

</ul><!-- /.social-col.newsocial.md -->

	      
	    </div><!-- .col-lt -->

	  </div><!-- .icol-wrapper -->

	  

	</div><!-- .pg-bd -->

	<div class="pg-ft">
	  <div class="cp-toolbar md">

  <div class="cp-tags">
    <dl class="tags cf">
      <dt class="label fl">Etiquetas</dt>
      <dd class="mt">
	
	<!-- TODO enlace al tag -->
	<a href="/etiqueta/ana_botin/" rel="tag" title="Ana Botin">Ana Botin</a>, 
	
	<!-- TODO enlace al tag -->
	<a href="/etiqueta/pablo_iglesias/" rel="tag" title="Pablo Iglesias">Pablo Iglesias</a>, 
	
	<!-- TODO enlace al tag -->
	<a href="/etiqueta/podemos/" rel="tag" title="Podemos">Podemos</a>, 
	
	<!-- TODO enlace al tag -->
	<a href="/etiqueta/syriza/" rel="tag" title="Syriza">Syriza</a>, 
	
	<!-- TODO enlace al tag -->
	<a href="/etiqueta/ignacio_sanchez_galan/" rel="tag" title="Ignacio S&aacute;nchez Gal&aacute;n">Ignacio Sánchez Galán</a>, 
	
	<!-- TODO enlace al tag -->
	<a href="/etiqueta/grecia/" rel="tag" title="Grecia">Grecia</a>, 
	
	<!-- TODO enlace al tag -->
	<a href="/etiqueta/francisco_gonzalez/" rel="tag" title="Francisco Gonz&aacute;lez">Francisco González</a>, 
	
	<!-- TODO enlace al tag -->
	<a href="/etiqueta/davos/" rel="tag" title="Davos">Davos</a>
	
      </dd><!-- .mt -->
    </dl><!-- .tags -->
  </div><!-- .cp-tags -->

</div><!-- .cp-toolbar -->

	  <div class="cp-icol-relations md">
  <div class="bd">
    <ul class="lst-relations">
      <li class="rel-grp">

	<em class="join-tit">
	  <span class="jstrap">Más información...</span>
	</em>

	<ul>
	  
	  <li class="rel first">
	    <a href="/politica/ECD-circulo-Podemos-Vea-imagenes_0_2425557432.html" title="ECD asiste a un c&iacute;rculo de Podemos. Vea las im&aacute;genes ">
	      ECD asiste a un círculo de Podemos. Vea las imágenes 
	    </a>
	  </li>
	  
	  <li class="rel first">
	    <a href="/politica/Zapatero-Rubalcaba-filtrador-Pablo-Iglesias_0_2423757622.html" title="Zapatero se&ntilde;ala a Rubalcaba como el filtrador de su reuni&oacute;n con Pablo Iglesias">
	      Zapatero señala a Rubalcaba como el filtrador de su reunión con Pablo Iglesias
	    </a>
	  </li>
	  
	  <li class="rel first">
	    <a href="/muy_confidencial/el_chivato/Sexta-Podemos-famelica-legion_0_2420757914.html" title="La Sexta, Podemos y la &ldquo;fam&eacute;lica legi&oacute;n&rdquo;">
	      La Sexta, Podemos y la “famélica legión”
	    </a>
	  </li>
	  
	  <li class="rel first">
	    <a href="/muy_confidencial/el_chau-chau/Pablo-Iglesias-Cofradia-Cristo-Llagas_0_2418958111.html" title="Pablo Iglesias, nazareno en la Cofrad&iacute;a del Cristo de las Cinco Llagas">
	      Pablo Iglesias, nazareno en la Cofradía del Cristo de las Cinco Llagas
	    </a>
	  </li>
	  
	  <li class="rel first">
	    <a href="/muy_confidencial/el_chivato/primer-banquero-enfrenta-Podemos_0_2418958104.html" title="El primer banquero que se atreve a pronunciarse contra Podemos">
	      El primer banquero que se atreve a pronunciarse contra Podemos
	    </a>
	  </li>
	  
	</ul>

      </li>
    </ul><!-- .lst-relations -->
  </div><!-- .bd -->
</div><!-- .cp-icol-relations -->

	  
	  
<div class="ad-hw sk-ad-article">
  <dl class="ad-wrap cf">
    <dt class="caption"><span>·Publicidad·</span></dt>
    <dd class="ct">
      <div class="ad">

	<div id="ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_ub2x2" class="ecd-adv-container">
	</div><!-- #ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_ub2x2 -->

	<div id="ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_ecd_600x250" class="ecd-adv-container">
	</div><!-- #ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_ecd_600x250 -->

      </div><!-- .ad -->
    </dd><!-- .lt -->
  </dl><!-- .ad-wrap -->
</div><!-- .ad-hw -->






	  <div id="ecd-comments-module-container">
	    <div class="bb-comment-content-comments-ajax"> </div>

<script type="text/javascript">

	jQuery(function() {BBT.comment._showEntityComments(jQuery(".bb-comment-content-comments-ajax"), "/bbtcomment/entityComments/0/ECDNWS20150126_0037.json?commentsPage=1&limit=10"); } );

</script>
<noscript>

<div id="comments" class="cp-comments md">

  <div class="hd">
    <h3 class="title">
      <span class="jstrap comments_0">
	0 comentarios
      </span>
    </h3>

    <div class="cp-filtersort fr 2 contentId_ECDNWS20150126_0037">
  <dl class="dlst-filtersort tx-rt">

    <dt class="label">Ordenar por:</dt>

    <dd class="criteria">

      
      
      <a href="#" title="orden descendente" class="it-criteria asc orderBy_1 tpl_bbtcommententitycommentsmo_ajax_asc.tpl">Ascendente</a>
      

      <span class="pipe"> | </span>


      
      
      <span title="orden ascendente" class="it-criteria active des">Descendente</span>
      

    </dd><!-- .criteria -->

  </dl><!-- .dlst-filtersort -->
</div><!-- .cp-filtersort -->


    <script type="text/javascript">

      var jObjCommentsOrder = jQuery("#comments .cp-filtersort");

      jObjCommentsOrder.find(" .it-criteria").click(function () {

	var url = "/comments/change-order.json";

	var params = {_tpl : BBT.getProperty(jQuery(this), "tpl"),
		      contentId : BBT.getProperty(jObjCommentsOrder, "contentId"),
		      orderBy : BBT.getProperty(jQuery(this), "orderBy")};

	jQuery.get(url, params, function(_response) {
	    var response = BBT.fixJson(_response);

	    if(response["error"] == "0") {

		jQuery("#ecd-comments-module-container").html(response["data"]);

	    } else {
		BBT.log(response["message"]);
	    }
	});

	return false;
    });
    </script>


  </div><!-- .hd -->

  <div class="bd">
    
    
    <ul class="lst-comments">
      
    </ul><!-- .lst-comments -->
    
  </div><!-- .bd -->


  

</div><!-- .cp-comments -->

<div class="cp-commentForm md" id="bbt-comment-create-form">

  <div class="hd">
    <h3 class="title">
      <span class="jstrap">Deja tu comentario</span>
    </h3>
  </div><!-- .hd -->

  <div class="bd">
    <!-- 0 - ECDNWS20150126_0037 -->

    <form id="bb-comment-create-form" name="formEcdCommentCreate" class="commentForm" onsubmit="return false;" action="/bbtcomment/create/0/ECDNWS20150126_0037">
      <div class="lt">
	<div class="cf">
	  <label class="label-name">
	    <span>Nombre de usuario <span class="req">*</span></span>
	    <input id="ecd_comment-nick" name="nick" type="text" class="bb-field bb-field_input" title="Usuario" />
	  </label>

	  <label class="label-pass">
	    <span>Email <span class="req">*</span></span>
	    <input id="ecd_comment-email" name="email" type="text" class="bb-field bb-field_input" title="Usuario" />
	  </label>

	</div><!-- .cf -->
      </div><!-- .lt -->
      <div class="rt">
	<div class="cf">

	  <label class="label-captcha cf">
	    <span class="cf">Escriba el código CAPTCHA: <span class="req">*</span></span>
	    <div class="ecd-commnt-captcha">
	      <img class="fl" title="captcha" width="135" height="34" src="/bbtcaptcha/captcha">
	    </div><!-- .ecd-commnt-captcha -->

	    <input type="text" name="__captcha__" class="fr bb-field bb-field_input" title="Captcha" />
	  </label><!-- .label-captcha.cf -->
	</div><!-- .cf -->
      </div><!-- .rt -->
      <div class="cf">
	<label>
	  <span>Comenta <span class="req">*</span></span>
	  <span class="bb-comment-counter" id="charNum">500</span>
	  <textarea maxlength="500" id="ecd_comment-text" name="text" class="bb-field bb-field_textarea ecd_comment-text" title="Comentario"></textarea>
	</label>

      </div><!-- .cf -->
      <div class="lt">
	<div class="cf">

	  <label>
	    <input class="checkbox" name="terms" type="checkbox" />
	    <span class="itx">He leído y acepto las <a href="http://www.elconfidencialdigital.com/normas_de_uso.html" title="normas de uso" target="_blank">normas de uso</a></span>
	    <span class="req">*</span>
	  </label>

	  <p class="required"><span class="req">*</span> Campos obligatorios</p>
	</div><!-- .cf -->
      </div><!-- .lt -->
      <div class="rt">
	<div class="cf">
	  <div class="submit-box">
	    <p>
	      <input id="bbt-comment-create-submit" class="bbt-comment-create-submit btn-submit bt1 btn iblk rnd5" type="submit" value="Enviar comentario" />
	    </p>
	  </div><!-- .submit-box -->
	</div><!-- .cf -->
      </div><!-- .rt -->

    </form>
  </div><!-- .bd -->
</div><!-- .cp-commentForm -->

<script type="text/javascript">

  jQuery(document).ready(function() {
      var maxChars = 500;
      var jObj = jQuery("#ecd_comment-text");
      jQuery(jObj).keyup(function(){
          var len = jQuery(this).val().length;
          if (len <= maxChars) {
              jQuery('#charNum').html(maxChars-len);
	  }
      });
  });

  ecdReloadCaptcha = function () {

	var jObj = jQuery("#bbt-comment-create-form");
        var captachUrl = "/bbtcaptcha/captcha?rand=" + parseInt(Math.random() * 10000);

        jObj.find(".ecd-commnt-captcha").each(function () {

           jQuery(this).html("");
           jQuery(this).append('<img class="fl" width="135" height="34" src="' + captachUrl + '" alt="" />');
        });
  };

  BBT.comment.initForm(jQuery("#bb-comment-create-form"));

  BBT.comment["formEcdCommentCreate_submit"] = function (url, data) {

        jQuery.post(url, data, function (_response) {
	  var response = BBT.fixJson(_response);

	  if (response["error"] == 0) {
	     window.location.reload();
	  } else {
             ecdReloadCaptcha();
	     alert(response["message"]);
	  }

	});

        return false;
  };


  BBT.comment["formEcdCommentCreate_getData"] = function (form) {

        var maxChars = 500;
	var data = BBT.component.cardForm.getFields(form);


	//Comprobamos que han introducido un nick
	if (data["nick"] == null
	    || data["nick"] == "") {
	    alert("Debe introducir un nick");
	    return false;
	}

	//Comprobamos que han introducido un email
	if (data["email"] == null
	    || data["email"] == "") {
	    alert("Debe introducir un email");
	    return false;
	}

        //Comprobamos que se ha metido un comentario
	if (data["text"] == null
	    || data["text"] == "") {
	    alert("Debe introducir un comentario");
	    return false;
	}

	//Comprobamos que el comentario no supera el numero maximo de caracteres
	if (data["text"].length > maxChars) {
	    alert("El comentario no debe sobrepasar los " + maxChars + " caracteres");
	    return false;
	}

        //Comprobamos que han introducido un captcha
	if (data["__captcha__"] == null
	    || data["__captcha__"] == "") {
	    alert("Debe introducir un captcha");
	    return false;
	}

	//Comprobamos que han aceptado los terminos de uso
	if(!jQuery("input[name='terms']").is(':checked')) {
           alert("Debe aceptar los términos de uso.");
	   return false;
	}

	return data;
  };

</script>



<script type="text/javascript">
  <!--
      function showComments() {
        var numComments = BBT.getProperty(jQuery("#comments .jstrap"), "comments");
        jQuery("#comments").hide();

	if (numComments >= 1) {
	  jQuery("#comments").show();
	}
      }

      showComments();
// -->
</script>


</noscript>

	  </div><!-- .ecd-comments-module-container -->
	  
<div class="ad-hw sk-ad-article">
  <dl class="ad-wrap cf">
    <dt class="caption"><span>·Publicidad·</span></dt>
    <dd class="ct">
      <div class="ad">

	<div id="ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_ecd_600x250_2" class="ecd-adv-container">
	</div><!-- #ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_ecd_600x250_2 -->

      </div><!-- .ad -->
    </dd><!-- .lt -->
  </dl><!-- .ad-wrap -->
</div><!-- .ad-hw -->






	</div><!-- .pg-ft -->

      </div><!-- .gdu.u2-3-c1 -->

      <div class="gdu u1-3-c1 u-last">
	
  
<div class="md-advertisement ad_mpu md">
  <dl class="bd">
    <dt class="caption">· Artículo más leído ·</dt>
    <dd class="ad">

      <div id="ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario4" class="ecd-adv-container">
      </div><!-- #ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario4 -->

    </dd><!-- .ad -->
  </dl><!-- .bd -->
</div><!-- .md-advertisement -->







  <div class="md-muy_confidencial md">
  <div class="hd">
    <h2 class="md-muy_confidencial-title">Muy Confidencial</h2>
  </div><!-- .hd -->
  <div class="bd">
    
    <p class="item-muy cf">
      <span class="item-muy-mg">
	<img width="68" height="59" alt="El Chivato" src="http://static2.elconfidencialdigital.com/bbtfile/1001_20130930UwMnLg.gif" />
      </span><!-- .item-muy-mg -->
      <strong class="item-muy-tx">El Chivato</strong>
      
      
      <a class="item-muy-lnk" href="/muy_confidencial/el_chivato/jefe-Ejercito-Cataluna-fuma-pipa_0_2425557458.html" alt="El jefe del Ej&eacute;rcito en Catalu&ntilde;a est&aacute; que fuma en pipa">
	El jefe del Ejército en Cataluña está que fuma en pipa
      </a>
      

    </p><!-- .item-muy -->
    
    <p class="item-muy cf">
      <span class="item-muy-mg">
	<img width="68" height="59" alt="El Chau-Chau" src="http://static1.elconfidencialdigital.com/bbtfile/1001_20130930qLz73a.gif" />
      </span><!-- .item-muy-mg -->
      <strong class="item-muy-tx">El Chau-Chau</strong>
      
      
      <a class="item-muy-lnk" href="/muy_confidencial/el_chau-chau/ABC-oficial-Convencion-Nacional-PP_0_2425557459.html" alt="ABC, &lsquo;diario oficial&rsquo; de la Convenci&oacute;n Nacional del PP">
	ABC, ‘diario oficial’ de la Convención Nacional del PP
      </a>
      

    </p><!-- .item-muy -->
    

  </div><!-- .bd -->
</div><!-- .md-muy_confidencial -->



  
<div class="md-advertisement ad_mpu md">
  <dl class="bd">
    <dt class="caption">·Publicidad·</dt>
    <dd class="ad">

      <div id="ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario4a" class="ecd-adv-container">
      </div><!-- #ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario4a -->

    </dd><!-- .ad -->
  </dl><!-- .bd -->
</div><!-- .md-advertisement -->







  
<div class="md-advertisement ad_mpu md">
  <dl class="bd">
    <dt class="caption">·Publicidad·</dt>
    <dd class="ad">

      <div id="ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario4aa" class="ecd-adv-container">
      </div><!-- #ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario4aa -->

    </dd><!-- .ad -->
  </dl><!-- .bd -->
</div><!-- .md-advertisement -->







  
<div class="md-advertisement ad_mpu md">
  <dl class="bd">
    <dt class="caption">·Publicidad·</dt>
    <dd class="ad">

      <div id="ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario3" class="ecd-adv-container">
      </div><!-- #ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario3 -->

    </dd><!-- .ad -->
  </dl><!-- .bd -->
</div><!-- .md-advertisement -->







  <!--
    MODULE TOP MOST (SLIDESHOW TABBED CONTENT)
    CLIPPING SIZES: 60x60 (thumb)
    Max items per pane: 10
  -->

<div class="md-topmost md thm-1" id="topmost_7293018059617337345">

  <div class="hd">

    <ul class="tabbing sk1-tabs cf">
      <li class="tab active">
	<span><a href="#">Más leídas</a></span>
      </li>
      <li class="tab">
	<span><a href="#">Más comentadas</a></span>
      </li>
    </ul>

  </div><!-- .hd -->


  <div class="bd">

    <!-- PANES CONTAINER -->
    <div class="panes" id="topmost_panes_7293018059617337345">

      <!-- PANES (CHUNK ITERATION) -->

      <!-- PANE (1) -->
      <div class="pane">

	<!-- Content Items (ITERATION) -->
	<!-- NOTE: MAX ITEMS PER PANE : 10 -->
	<ul class="lst-items">
	  
	  <!-- ITEM (1) -->
	  <li class="item">
	    <span class="num">1.-</span>
	    <div class="md-item mt">
	      
	      
	      <h3 class="title"><a href="/defensa/francotirador-Clint-Eastwood-Espana-especialistas_0_2423757619.html" alt="El francotirador de Clint Eastwood no existe en Espa&ntilde;a. &ldquo;Nuestros especialistas no est&aacute;n orgullosos de sus muertes&rdquo;">El francotirador de Clint Eastwood no existe en España. “Nuestros especialistas no están orgullosos de sus muertes”</a></h3>
	      
	    </div><!-- .content -->
	  </li><!-- .item -->
	  
	  <!-- ITEM (1) -->
	  <li class="item">
	    <span class="num">2.-</span>
	    <div class="md-item mt">
	      
	      
	      <h3 class="title"><a href="/muy_confidencial/el_chivato/jefe-Ejercito-Cataluna-fuma-pipa_0_2425557458.html" alt="El jefe del Ej&eacute;rcito en Catalu&ntilde;a est&aacute; que fuma en pipa">El jefe del Ejército en Cataluña está que fuma en pipa</a></h3>
	      
	    </div><!-- .content -->
	  </li><!-- .item -->
	  
	  <!-- ITEM (1) -->
	  <li class="item">
	    <span class="num">3.-</span>
	    <div class="md-item mt">
	      
	      
	      <h3 class="title"><a href="/seguridad/Segura-Guardia-Civil-corrupcion-Benemerita_0_2424957484.html" alt="Un &lsquo;teniente Segura&rsquo; de la Guardia Civil denuncia corrupci&oacute;n en la Benem&eacute;rita">Un ‘teniente Segura’ de la Guardia Civil denuncia corrupción en la Benemérita</a></h3>
	      
	    </div><!-- .content -->
	  </li><!-- .item -->
	  
	  <!-- ITEM (1) -->
	  <li class="item">
	    <span class="num">4.-</span>
	    <div class="md-item mt">
	      
	      
	      <h3 class="title"><a href="/seguridad/denuncia-Guardia-Civil-irregularidades-generalizadas_0_2425557438.html" alt="Un tedax denuncia: &ldquo;Los acosos existen en la Guardia Civil y las irregularidades son generalizadas&rdquo;">Un tedax denuncia: “Los acosos existen en la Guardia Civil y las irregularidades son generalizadas”</a></h3>
	      
	    </div><!-- .content -->
	  </li><!-- .item -->
	  
	  <!-- ITEM (1) -->
	  <li class="item">
	    <span class="num">5.-</span>
	    <div class="md-item mt">
	      
	      
	      <h3 class="title"><a href="/defensa/Tragico-F-16-Albacete-circunstancias-negativas_0_2425557441.html" alt="Tr&aacute;gico accidente de F-16 en Albacete: un incre&iacute;ble c&uacute;mulo de circunstancias negativas">Trágico accidente de F-16 en Albacete: un increíble cúmulo de circunstancias negativas</a></h3>
	      
	    </div><!-- .content -->
	  </li><!-- .item -->
	  
	  <!-- ITEM (1) -->
	  <li class="item">
	    <span class="num">6.-</span>
	    <div class="md-item mt">
	      
	      
	      <h3 class="title"><a href="/defensa/Accidente-F-16-Albacete-mecanicos-OTAN_0_2425557429.html" alt="Accidente de F-16 en Albacete: el avi&oacute;n perdi&oacute; el control y cay&oacute; encima de pilotos y mec&aacute;nicos de la OTAN">Accidente de F-16 en Albacete: el avión perdió el control y cayó encima de pilotos y mecánicos de la OTAN</a></h3>
	      
	    </div><!-- .content -->
	  </li><!-- .item -->
	  
	  <!-- ITEM (1) -->
	  <li class="item">
	    <span class="num">7.-</span>
	    <div class="md-item mt">
	      
	      
	      <h3 class="title"><a href="/dinero/Botin-FG-Galan-Davos-Podemos_0_2425557457.html" alt="Bot&iacute;n, FG y Gal&aacute;n no consiguen tranquilizar en Davos sobre los peligros de Podemos">Botín, FG y Galán no consiguen tranquilizar en Davos sobre los peligros de Podemos</a></h3>
	      
	    </div><!-- .content -->
	  </li><!-- .item -->
	  
	  <!-- ITEM (1) -->
	  <li class="item">
	    <span class="num">8.-</span>
	    <div class="md-item mt">
	      
	      
	      <h3 class="title"><a href="/la_buena_vida/motor/trucos-pueden-ayudar-ahorrar-gasolina_0_2425557431.html" alt="Ocho trucos que te pueden ayudar a ahorrar en gasolina">Ocho trucos que te pueden ayudar a ahorrar en gasolina</a></h3>
	      
	    </div><!-- .content -->
	  </li><!-- .item -->
	  
	  <!-- ITEM (1) -->
	  <li class="item">
	    <span class="num">9.-</span>
	    <div class="md-item mt">
	      
	      
	      <h3 class="title"><a href="/muy_confidencial/el_chivato/Letizia-quiere-vigilada-propia-casa_0_2424957490.html" alt="Letizia no quiere estar &lsquo;vigilada&rsquo; en su propia casa">Letizia no quiere estar ‘vigilada’ en su propia casa</a></h3>
	      
	    </div><!-- .content -->
	  </li><!-- .item -->
	  
	  <!-- ITEM (1) -->
	  <li class="item">
	    <span class="num">10.-</span>
	    <div class="md-item mt">
	      
	      
	      <h3 class="title"><a href="/politica/pequeno-Nicolas-agredido-manifestacion-AVT_0_2425557437.html" alt="El peque&ntilde;o Nicol&aacute;s estuvo a punto de ser agredido durante la manifestaci&oacute;n de la AVT">El pequeño Nicolás estuvo a punto de ser agredido durante la manifestación de la AVT</a></h3>
	      
	    </div><!-- .content -->
	  </li><!-- .item -->
	  

	  <!-- ITEM (bar) -->
	  <!-- bar... -->

	</ul><!-- .lst-items -->
      </div><!-- .pane -->


      <!-- PANE (2) -->
      <div class="pane">

	<!-- Content Items (ITERATION) -->
	<!-- NOTE: MAX ITEMS PER PANE : 10 -->
	<ul class="lst-items">
	  
	  <!-- ITEM (1) -->
	  <li class="item">
	    <span class="num">1.-</span>
	    <div class="md-item mt">
	      
	      
	      <h3 class="title"><a href="/dinero/Botin-FG-Galan-Davos-Podemos_0_2425557457.html" alt="Bot&iacute;n, FG y Gal&aacute;n no consiguen tranquilizar en Davos sobre los peligros de Podemos">Botín, FG y Galán no consiguen tranquilizar en Davos sobre los peligros de Podemos</a></h3>
	      
	    </div><!-- .content -->
	  </li><!-- .item -->
	  
	  <!-- ITEM (1) -->
	  <li class="item">
	    <span class="num">2.-</span>
	    <div class="md-item mt">
	      
	      
	      <h3 class="title"><a href="/dinero/Amancio-Ortega-principal-competidor-Primark_0_2425557426.html" alt="Amancio Ortega compra el edificio donde se instalar&aacute; su principal competidor Primark">Amancio Ortega compra el edificio donde se instalará su principal competidor Primark</a></h3>
	      
	    </div><!-- .content -->
	  </li><!-- .item -->
	  
	  <!-- ITEM (baz) -->
	  <!-- baz... -->

	</ul><!-- .lst-items -->
      </div><!-- .pane -->


      <!-- PANE (FOO) -->
      <!-- foo -->


    </div><!-- .panes -->
    <!-- END: PANES CONTAINER -->

  </div><!-- .bd -->
</div><!-- .md-topmost -->

<script type="text/javascript">
  jQuery("#topmost_7293018059617337345").toolsTabs("#topmost_panes_7293018059617337345 .pane", {
  tabs: 'li.tab',
  current: 'active',
  effect: 'default'
  }
  );
</script>





  
<div class="md-advertisement ad_mpu2 md">
  <dl class="bd">
    <dt class="caption">·Publicidad·</dt>
    <dd class="ad">

      <div id="ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_epom11" class="ecd-adv-container">
      </div><!-- #ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_epom11 -->

    </dd><!-- .ad -->
  </dl><!-- .bd -->
</div><!-- .md-advertisement -->







  
<div class="md-advertisement ad_mpu2 md">
  <dl class="bd">
    <dt class="caption">·Publicidad·</dt>
    <dd class="ad">

      <div id="ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_quisma11" class="ecd-adv-container">
      </div><!-- #ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_quisma11 -->

    </dd><!-- .ad -->
  </dl><!-- .bd -->
</div><!-- .md-advertisement -->







  
<div class="md-advertisement ad_mpu2 md">
  <dl class="bd">
    <dt class="caption">·Publicidad·</dt>
    <dd class="ad">

      <div id="ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_dm11" class="ecd-adv-container">
      </div><!-- #ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_dm11 -->

    </dd><!-- .ad -->
  </dl><!-- .bd -->
</div><!-- .md-advertisement -->







  
<div class="md-advertisement ad_mpu2 md">
  <dl class="bd">
    <dt class="caption">·Publicidad·</dt>
    <dd class="ad">

      <div id="ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_xaxis11" class="ecd-adv-container">
      </div><!-- #ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_xaxis11 -->

    </dd><!-- .ad -->
  </dl><!-- .bd -->
</div><!-- .md-advertisement -->







  
<div class="md-advertisement ad_mpu2 md">
  <dl class="bd">
    <dt class="caption">·Publicidad·</dt>
    <dd class="ad">

      <div id="ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario12" class="ecd-adv-container">
      </div><!-- #ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario12 -->

    </dd><!-- .ad -->
  </dl><!-- .bd -->
</div><!-- .md-advertisement -->








      </div><!-- .gdu.u1-3-c1.u-last -->

    </div><!-- .gd-aux.faux-g3c1a -->
  </div><!-- .gd3-c1 -->
</div><!-- #content.pg -->


  
<div class="ad-hw sk-ad-full">
  <dl class="ad-wrap cf">
    <dt class="caption"><span>·Publicidad·</span></dt>
    <dd class="ct">
      <div class="ad">

	<div id="ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario14a" class="ecd-adv-container">
	</div><!-- #ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario14a -->

      </div><!-- .ad -->
    </dd><!-- .lt -->

  </dl><!-- .ad-wrap -->
</div><!-- .ad-hw -->








  <div class="md-videos md videos_en_portada_04b009f0839f11e194d35abd9b5b985e">

  <div class="hd">
    <a class="lnk" href="/videos/?sectionId=3001">Video Noticias de ECD</a>
    |
    <a class="lnk" href="/videos/?sectionId=3002">El VideoBlog de José Apezarena</a>
  </div><!-- .hd -->

  <div class="bd mtcol">

    <div class="col1 col">

      <a class="lnk md-videos-logo" href="#">
	<img src="http://static1.elconfidencialdigital.com/static/ECDDigital/images/md-videos-logo.png?hash=63d40086f3898d962c950367536d338a" alt="Envía tus noticias" height="53" width="241" />
      </a>

      
      <div class="col1a">

	<p class="top-headline">
	  Videonoticias de ECD
	</p>

	
	
	<h2 class="headline">
	  <a class="lnk" href="/videos_de_ecd/videonoticias_de_ecd/ECD-circulo-Podemos-Vea-imagenes_3_2425587418.html" title="ECD asiste a un c&iacute;rculo de Podemos. Vea las im&aacute;genes ">ECD asiste a un círculo de Podemos. Vea las imágenes </a>
	</h2>

	

<div class="mg">
  <a href="/videos_de_ecd/videonoticias_de_ecd/ECD-circulo-Podemos-Vea-imagenes_3_2425587418.html" title="ECD asiste a un c&iacute;rculo de Podemos. Vea las im&aacute;genes ">

    
<img src="http://images.elconfidencialdigital.com/videos_de_ecd/videonoticias_de_ecd/ECD-circulo-Podemos-Vea-imagenes_ECDVID20150126_0001_9.jpg" width="288" height="162" alt="ECD asiste a un c&iacute;rculo de Podemos. Vea las im&aacute;genes " />


  </a>
</div><!-- .mg -->



	<p class="comments">0 comentarios</p>

	<p class="teaser">
	  <a class="lnk" href="/videos_de_ecd/videonoticias_de_ecd/ECD-circulo-Podemos-Vea-imagenes_3_2425587418.html" title="ECD asiste a un c&iacute;rculo de Podemos. Vea las im&aacute;genes ">Benjamín, el coordinador del círculo Carabanchel-Latina nos explica cómo transcurre la orden del día y las propuestas presentadas y votadas así como informaciones y valoraciones de sus miembros.</a>
	</p>
	

      </div><!-- .col1 -->
      
      <div class="col1b">

	<p class="top-headline">
	  Videonoticias de ECD
	</p>

	
	
	<h2 class="headline">
	  <a class="lnk" href="/videos_de_ecd/videonoticias_de_ecd/Susana-Diaz-embarazo-ginecologa-consejos_3_2423787598.html" title="Qu&eacute; debe hacer Susana D&iacute;az para llevar bien su embarazo. Una ginec&oacute;loga ofrece cuatro consejos ">Qué debe hacer Susana Díaz para llevar bien su embarazo. Una ginecóloga ofrece cuatro consejos </a>
	</h2>

	

<div class="mg">
  <a href="/videos_de_ecd/videonoticias_de_ecd/Susana-Diaz-embarazo-ginecologa-consejos_3_2423787598.html" title="Qu&eacute; debe hacer Susana D&iacute;az para llevar bien su embarazo. Una ginec&oacute;loga ofrece cuatro consejos ">

    
<img src="http://images.elconfidencialdigital.com/videos_de_ecd/videonoticias_de_ecd/Susana-Diaz-embarazo-ginecologa-consejos_ECDVID20150123_0001_9.jpg" width="288" height="162" alt="Qu&eacute; debe hacer Susana D&iacute;az para llevar bien su embarazo. Una ginec&oacute;loga ofrece cuatro consejos " />


  </a>
</div><!-- .mg -->



	<p class="comments">0 comentarios</p>

	<p class="teaser">
	  <a class="lnk" href="/videos_de_ecd/videonoticias_de_ecd/Susana-Diaz-embarazo-ginecologa-consejos_3_2423787598.html" title="Qu&eacute; debe hacer Susana D&iacute;az para llevar bien su embarazo. Una ginec&oacute;loga ofrece cuatro consejos ">Se aplican en mujeres en estado de gestación con estrés y extensas jornadas laborales. Susana Díaz se enfrenta a una situación excepcional debido a las elecciones que le reportará más presión durante los nueve meses que le restan del parto. Si bien es cierto que otras mujeres profesionales embarazadas pueden encontrarse en una situación similar de más presión. Por eso la médico Mariam Chávez propone cuatro pilares sobre los que poder sobrellevar bien estas contrariedades.</a>
	</p>
	

      </div><!-- .col1 -->
      


    </div><!-- .col1.col -->

    <div class="col2 col">

      


<div class="item">

  <div class="fl">

    

<div class="mg">
  <a href="/videos_de_ecd/videonoticias_de_ecd/SPAS-Policia-Nacional-Guardia-Yihadismo_3_2423187659.html" title="As&iacute; es la SPAS 12, la escopeta que han recuperado Polic&iacute;a Nacional y Guardia Civil para luchar contra el Yihadismo">

    
<img src="http://images.elconfidencialdigital.com/videos_de_ecd/videonoticias_de_ecd/SPAS-Policia-Nacional-Guardia-Yihadismo_ECDVID20150122_0002_14.jpg" width="100" height="56" alt="As&iacute; es la SPAS 12, la escopeta que han recuperado Polic&iacute;a Nacional y Guardia Civil para luchar contra el Yihadismo" />


  </a>
</div><!-- .mg -->



    <p class="comments">0 comentarios</p>

  </div><!-- .fl -->

  <div class="mt">


    <p class="top-headline">
      Videonoticias de ECD
    </p>

    <h2 class="headline">
      <a class="lnk" href="/videos_de_ecd/videonoticias_de_ecd/SPAS-Policia-Nacional-Guardia-Yihadismo_3_2423187659.html" title="As&iacute; es la SPAS 12, la escopeta que han recuperado Polic&iacute;a Nacional y Guardia Civil para luchar contra el Yihadismo">
	Así es la SPAS 12, la escopeta que han recuperado Policía Nacional y Guardia Civil para luchar contra el Yihadismo
      </a>
    </h2>

  </div><!-- mt -->

</div><!-- .item -->





<div class="item">

  <div class="fl">

    

<div class="mg">
  <a href="/videos_de_ecd/experto-explica-paso-resintonizacion-television_3_2422587718.html" title="Un experto explica paso a paso c&oacute;mo resintonizar la televisi&oacute;n en casa ">

    
<img src="http://images.elconfidencialdigital.com/videos_de_ecd/experto-explica-paso-resintonizacion-television_ECDVID20150121_0001_14.jpg" width="100" height="56" alt="Un experto explica paso a paso c&oacute;mo resintonizar la televisi&oacute;n en casa " />


  </a>
</div><!-- .mg -->



    <p class="comments">0 comentarios</p>

  </div><!-- .fl -->

  <div class="mt">


    <p class="top-headline">
      Vídeos de ECD
    </p>

    <h2 class="headline">
      <a class="lnk" href="/videos_de_ecd/experto-explica-paso-resintonizacion-television_3_2422587718.html" title="Un experto explica paso a paso c&oacute;mo resintonizar la televisi&oacute;n en casa ">
	Un experto explica paso a paso cómo resintonizar la televisión en casa 
      </a>
    </h2>

  </div><!-- mt -->

</div><!-- .item -->





<div class="item">

  <div class="fl">

    

<div class="mg">
  <a href="/videos_de_ecd/videonoticias_de_ecd/submarinos-nucleares-reabastecen-Gibraltar_3_2421987779.html" title="C&oacute;mo son los submarinos nucleares que se reabastecen en Gibraltar">

    
<img src="http://images.elconfidencialdigital.com/videos_de_ecd/videonoticias_de_ecd/submarinos-nucleares-reabastecen-Gibraltar_ECDVID20150120_0002_14.jpg" width="100" height="56" alt="C&oacute;mo son los submarinos nucleares que se reabastecen en Gibraltar" />


  </a>
</div><!-- .mg -->



    <p class="comments">0 comentarios</p>

  </div><!-- .fl -->

  <div class="mt">


    <p class="top-headline">
      Videonoticias de ECD
    </p>

    <h2 class="headline">
      <a class="lnk" href="/videos_de_ecd/videonoticias_de_ecd/submarinos-nucleares-reabastecen-Gibraltar_3_2421987779.html" title="C&oacute;mo son los submarinos nucleares que se reabastecen en Gibraltar">
	Cómo son los submarinos nucleares que se reabastecen en Gibraltar
      </a>
    </h2>

  </div><!-- mt -->

</div><!-- .item -->





<div class="item">

  <div class="fl">

    

<div class="mg">
  <a href="/videos_de_ecd/videonoticias_de_ecd/musulmana-explica-vestidos-utiliza-ocasion_3_2421387838.html" title="Una mujer musulmana explica c&oacute;mo son sus vestidos que utiliza en cada ocasi&oacute;n ">

    
<img src="http://images.elconfidencialdigital.com/videos_de_ecd/videonoticias_de_ecd/musulmana-explica-vestidos-utiliza-ocasion_ECDVID20150119_0001_14.jpg" width="100" height="56" alt="Una mujer musulmana explica c&oacute;mo son sus vestidos que utiliza en cada ocasi&oacute;n " />


  </a>
</div><!-- .mg -->



    <p class="comments">0 comentarios</p>

  </div><!-- .fl -->

  <div class="mt">


    <p class="top-headline">
      Videonoticias de ECD
    </p>

    <h2 class="headline">
      <a class="lnk" href="/videos_de_ecd/videonoticias_de_ecd/musulmana-explica-vestidos-utiliza-ocasion_3_2421387838.html" title="Una mujer musulmana explica c&oacute;mo son sus vestidos que utiliza en cada ocasi&oacute;n ">
	Una mujer musulmana explica cómo son sus vestidos que utiliza en cada ocasión 
      </a>
    </h2>

  </div><!-- mt -->

</div><!-- .item -->





    </div><!-- .col2.col -->

  </div><!-- .bd -->

</div><!-- .md-videos.md -->



  
<div class="ad-hw sk-ad-full">
  <dl class="ad-wrap cf">
    <dt class="caption"><span>·Publicidad·</span></dt>
    <dd class="ct">
      <div class="ad">

	<div id="ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario14b" class="ecd-adv-container">
	</div><!-- #ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario14b -->

      </div><!-- .ad -->
    </dd><!-- .lt -->

  </dl><!-- .ad-wrap -->
</div><!-- .ad-hw -->








  <div class="gd gd1 skin-hr">
  <div class="gdu u1">

    <div class="highlighted-4col md skin-md2">

      
      
      <div class="hd">
	<h2 class="title regular">Otros Titulares</h2>
      </div>
      

      <div class="bd">

	
	<ul class="lst-highlighted-4col cf">
	  
	  <li class="item ">

	    
<div class="md-news md">

  <p class="kicker">
    <em>Política</em>
  </p>


  
  
  <p class="tx teaser t-regular">
    <a class="lnk" title="Rajoy comunica a G&eacute;nova que anunciar&aacute; los candidatos el 10 de febrero" href="/politica/Rajoy-comunica-Genova-anunciara-candidatos_0_2423757618.html">
      Rajoy comunica a Génova que anunciará los candidatos el 10 de febrero
    </a>
  </p><!-- .tx.teaser.t-regular -->
  

</div><!-- .md-news -->



	  </li><!-- .item -->
	  
	  <li class="item ">

	    
<div class="md-news md">

  <p class="kicker">
    <em>Medios</em>
  </p>


  
  
  <p class="tx teaser t-regular">
    <a class="lnk" title="Injerencias pol&iacute;ticas provocan cambios en medios informativos andaluces: Canal Sur, RTVE, Agencia Efe, la Junta&hellip;" href="/medios/Injerencias-Canal-RTVE-Agencia-Junta_0_2424957489.html">
      Injerencias políticas provocan cambios en medios informativos andaluces: Canal Sur, RTVE, Agencia Efe, la Junta…
    </a>
  </p><!-- .tx.teaser.t-regular -->
  

</div><!-- .md-news -->



	  </li><!-- .item -->
	  
	  <li class="item ">

	    
<div class="md-news md">

  <p class="kicker">
    <em>Defensa</em>
  </p>


  
  
  <p class="tx teaser t-regular">
    <a class="lnk" title="El francotirador de Clint Eastwood no existe en Espa&ntilde;a. &ldquo;Nuestros especialistas no est&aacute;n orgullosos de sus muertes&rdquo;" href="/defensa/francotirador-Clint-Eastwood-Espana-especialistas_0_2423757619.html">
      El francotirador de Clint Eastwood no existe en España. “Nuestros especialistas no están orgullosos de sus muertes”
    </a>
  </p><!-- .tx.teaser.t-regular -->
  

</div><!-- .md-news -->



	  </li><!-- .item -->
	  
	  <li class="item last">

	    
<div class="md-news md">

  <p class="kicker">
    <em>Dinero</em>
  </p>


  
  
  <p class="tx teaser t-regular">
    <a class="lnk" title="Javier Mar&iacute;n plantea al Banco Santander una salida al &lsquo;estilo Goiri&rsquo; del BBVA" href="/dinero/Javier-Marin-Santander-Goiri-BBVA_0_2424357541.html">
      Javier Marín plantea al Banco Santander una salida al ‘estilo Goiri’ del BBVA
    </a>
  </p><!-- .tx.teaser.t-regular -->
  

</div><!-- .md-news -->



	  </li><!-- .item -->
	  
	</ul><!-- .lst-highlighted-4col -->
	
	<ul class="lst-highlighted-4col cf">
	  
	  <li class="item ">

	    
<div class="md-news md">

  <p class="kicker">
    <em>Casas reales</em>
  </p>


  
  
  <p class="tx teaser t-regular">
    <a class="lnk" title="Sorpresa en &aacute;mbitos judiciales por la admisi&oacute;n de la demanda de paternidad contra don Juan Carlos" href="/casas_reales/Sorpresa-judiciales-paternidad-Juan-Carlos_0_2418358169.html">
      Sorpresa en ámbitos judiciales por la admisión de la demanda de paternidad contra don Juan Carlos
    </a>
  </p><!-- .tx.teaser.t-regular -->
  

</div><!-- .md-news -->



	  </li><!-- .item -->
	  
	  <li class="item ">

	    
<div class="md-news md">

  <p class="kicker">
    <em>Seguridad</em>
  </p>


  
  
  <p class="tx teaser t-regular">
    <a class="lnk" title="Cacheos en la c&aacute;rcel de Estremera para identificar a quien grab&oacute; a Granados jugando a las cartas" href="/seguridad/Cacheos-Estremera-identificar-Granados-jugando_0_2423757617.html">
      Cacheos en la cárcel de Estremera para identificar a quien grabó a Granados jugando a las cartas
    </a>
  </p><!-- .tx.teaser.t-regular -->
  

</div><!-- .md-news -->



	  </li><!-- .item -->
	  
	  <li class="item ">

	    
<div class="md-news md">

  <p class="kicker">
    <em>Deportes</em>
  </p>


  
  
  <p class="tx teaser t-regular">
    <a class="lnk" title="A la venta la camiseta republicana del Real Madrid" href="/deportes/venta-camiseta-republicana-Real-Madrid_0_2397960202.html">
      A la venta la camiseta republicana del Real Madrid
    </a>
  </p><!-- .tx.teaser.t-regular -->
  

</div><!-- .md-news -->



	  </li><!-- .item -->
	  
	  <li class="item last">

	    
<div class="md-news md">

  <p class="kicker">
    <em>Vivir</em>
  </p>


  
  
  <p class="tx teaser t-regular">
    <a class="lnk" title="Alarma entre los cazadores VIP: el Gobierno suelta cabras montesas en Francia" href="/vivir/Alarma-cazadores-VIP-Gobierno-Francia_0_2424957483.html">
      Alarma entre los cazadores VIP: el Gobierno suelta cabras montesas en Francia
    </a>
  </p><!-- .tx.teaser.t-regular -->
  

</div><!-- .md-news -->



	  </li><!-- .item -->
	  
	</ul><!-- .lst-highlighted-4col -->
	

      </div><!-- .bd -->

    </div><!-- .highlighted-4col.md.skin-md2 -->

  </div><!-- .gdu.u1 -->
</div><!-- .gd.gd1.skin-hr -->



  
<div class="ad-hw">
  <dl class="ad-wrap cf">

    <dt class="caption"><span>·Publicidad·</span></dt>

    <dd class="lt">
      <div class="ad">
	<div id="ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario5c" class="ecd-adv-container" >
	</div><!-- #ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario5b -->
      </div><!-- .ad -->
    </dd><!-- .lt -->

    <dd class="rt">
      <div class="ad">
	<div id="ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86_inventario10c" class="ecd-adv-container" >
	</div><!-- #ecd-advertising-item_1aacc926f3706c9f04d753d49c6a7c86 -->
      </div><!-- .ad -->
    </dd><!-- .rt -->

  </dl><!-- .ad-wrap -->
</div><!-- .ad-hw -->












      </div><!-- .limiter1 -->
    </div><!-- #bd -->

    <div id="bl" class="rg">
  <div class="limiter limiter1">
    <div class="footer-tools cf">
      <div class="inner">

	<div class="cf">
	  <div class="links-subnav">

	    
	    <h4 class="tit">Y además...</h4>
	    

	    
	    <ul class="lst-links-subnav">
	      
	      <li class="item"><a href="http://www.elconfidencialdigital.com/etiqueta/francisco_nicolas_gomez_iglesias/" target="_blank">Francisco Nicolás Iglesias</a></li>
	      
	      <li class="item"><a href="http://www.elconfidencialdigital.com/etiqueta/casimiro_garcia-abadillo/" target="_blank">Casimiro García-Abadillo</a></li>
	      
	      <li class="item"><a href="http://www.elconfidencialdigital.com/etiqueta/pedro_sanchez/" target="_blank">Pedro Sánchez</a></li>
	      
	      <li class="item"><a href="http://www.elconfidencialdigital.com/etiqueta/estados_unidos/" target="_blank">Estados Unidos</a></li>
	      
	    </ul>
	    
	    <ul class="lst-links-subnav">
	      
	      <li class="item"><a href="http://www.elconfidencialdigital.com/etiqueta/canada/" target="_blank">Canadá</a></li>
	      
	      <li class="item"><a href="http://www.elconfidencialdigital.com/etiqueta/gibraltar/" target="_blank">Gibraltar</a></li>
	      
	      <li class="item"><a href="http://www.elconfidencialdigital.com/etiqueta/policia_nacional/" target="_blank">Policia Nacional</a></li>
	      
	      <li class="item"><a href="http://www.elconfidencialdigital.com/etiqueta/pablo_iglesias/" target="_blank">Pablo Iglesias</a></li>
	      
	    </ul>
	    
	    <ul class="lst-links-subnav">
	      
	      <li class="item"><a href="http://www.elconfidencialdigital.com/etiqueta/tania_sanchez/" target="_blank">Tania Sánchez</a></li>
	      
	      <li class="item"><a href="http://www.elconfidencialdigital.com/etiqueta/militares/" target="_blank">Militares</a></li>
	      
	      <li class="item"><a href="http://www.elconfidencialdigital.com/etiqueta/amenaza_terrorista_en_espana/" target="_blank">Amenaza Terrorista en España</a></li>
	      
	      <li class="item"><a href="http://www.elconfidencialdigital.com/etiqueta/viviendas/" target="_blank">Viviendas</a></li>
	      
	    </ul>
	    
	    <ul class="lst-links-subnav">
	      
	      <li class="item"><a href="http://www.elconfidencialdigital.com/etiqueta/arturo_zamarriego/" target="_blank">Arturo Zamarriego</a></li>
	      
	      <li class="item"><a href="http://www.elconfidencialdigital.com/etiqueta/ciudadanos/" target="_blank">Ciudadanos</a></li>
	      
	      <li class="item"><a href="http://www.elconfidencialdigital.com/etiqueta/revista_inspire/" target="_blank">Revista Inspire</a></li>
	      
	      <li class="item"><a href="http://www.elconfidencialdigital.com/etiqueta/estado_islamico/" target="_blank">Estado Islámico</a></li>
	      
	    </ul>
	    
	    <ul class="lst-links-subnav">
	      
	      <li class="item"><a href="http://www.elconfidencialdigital.com/etiqueta/ejercito_del_aire/" target="_blank">Ejército del Aire</a></li>
	      
	      <li class="item"><a href="http://www.elconfidencialdigital.com/etiqueta/yihadismo/" target="_blank">Yihadismo</a></li>
	      
	      <li class="item"><a href="http://www.elconfidencialdigital.com/etiqueta/divorcios/" target="_blank">Divorcios</a></li>
	      
	      <li class="item"><a href="http://www.elconfidencialdigital.com/etiqueta/metodo_perricone/" target="_blank">Método Perricone</a></li>
	      
	    </ul>
	    
	    <ul class="lst-links-subnav">
	      
	      <li class="item"><a href="http://www.elconfidencialdigital.com/etiqueta/eugenio_pino/" target="_blank">Eugenio Pino</a></li>
	      
	      <li class="item"><a href="http://www.elconfidencialdigital.com/etiqueta/93_metros/" target="_blank">93 metros</a></li>
	      
	      <li class="item"><a href="http://www.elconfidencialdigital.com/etiqueta/cni/" target="_blank">CNI</a></li>
	      
	      <li class="item"><a href="http://www.elconfidencialdigital.com/etiqueta/juan_carlos_monedero/" target="_blank">Juan Carlos Monedero</a></li>
	      
	    </ul>
	    
	  </div><!-- .links-subnav -->

	  <div class="search-social">

	  <div class="main-search footer">

  <form id="AuxMainSearchForm" name="AuxMainSearchForm" method="get" action="/buscador/">
    <fieldset>
      <legend>Busque en El Confidencial Digital:</legend>
      <span class="sk-field">
	<label for="AuxMainSearchField">búsqueda</label>
	<input type="text" class="search-field ecd-search-text" id="AuxMainSearchField" name="text" />
	<a class="bt-submit btk ecd-search-submit" href="#" title="Buscar noticias,fotos ..." onClick="searchSubmit(jQuery('#AuxMainSearchForm'));return false;"><span>Buscar</span></a>
      </span>
    </fieldset>
  </form>

  <script type="text/javascript">
    <!--
	var jObj = jQuery("#AuxMainSearchForm");
	FBSE.simpleSearchForm(jObj, {
	   hint: 'Buscar noticias,fotos ...',
	   searchField : "#AuxMainSearchField",
	   submit : false
	});
	//-->
  </script>

</div><!-- .main-search -->


	  <ul class="lst-social">
  
  <li class="item rss"><a href="/rss/section/1/"><span>RSS</span></a></li>
  
  <li class="item twitter"><a href="https://twitter.com/ecd_"><span>Twitter</span></a></li>
  
  <li class="item facebook"><a href="https://www.facebook.com/pages/El-Confidencial-Digital/202726949863885?fref=ts"><span>Facebook</span></a></li>
  
</ul>


	  </div><!-- .search-social -->

	</div><!-- .cf -->

	<div class="logo cf">
	  <h1 class="logo-footer">
	    <a class="lnk" href="/" title="home"><span class="name">elconfidencialdigital.com</span></a>
	  </h1><!-- .main.logo -->
	  <ul class="lst-logo-links">
	    <li class="item"><a href="/quienes_somos.html">¿Quiénes Somos?</a>&nbsp;|&nbsp;</li>
	    <li class="item"><a href="/politica_de_privacidad.html">Pol&iacute;tica de privacidad</a>&nbsp;|&nbsp;</li>
	    <li class="item"><a href="mailto:redaccion@elconfidencialdigital.com">Contactar</a>&nbsp;|&nbsp;</li>
	    <li class="item"><a href="/Publicidad.html">Publicidad</a>&nbsp;|&nbsp;</li>
	    <li class="item"><a href="/quienes_somos.html">© Copyright "El Confidencial Digital"</a></li>
	  </ul>
	  <p class="footer-tx-aux">
	    Todos los derechos reservados. Esta información es para el uso exclusivo de los lectores de El Confidencial Digital.
	  </p><!-- /.footer-tx-aux -->
	  <p class="footer-tx-aux">
	    No está autorizada su difusión ni citando la fuente. El Confidencial Digital. C/Naranjo 3, 1º Dcha 28039 Madrid T 91 445 96 97  F 91 593 85 48
          </p><!-- /.footer-tx-aux -->
        </div><!-- .logo -->


      </div><!-- .inner -->
    </div><!-- .footer-tools -->
  </div><!-- .limiter -->
</div><!-- #bl -->


  </div><!-- #container -->

</div><!-- #main -->

<!-- DON'T REMOVE OR MOVE UP (IE Advanced rulesets) -->
<!--[if (gte IE 6)&(lte IE 8)]>
    <script src="/static/ECDDigital/js/ecd.ie.js" type="text/javascript"></script>
    <![endif]-->
<!-- DON'T REMOVE -->


<script type="text/javascript">

  var _sf_async_config={};

  /** CONFIGURATION START **/
  _sf_async_config.uid = 47812;
  _sf_async_config.domain = "elconfidencialdigital.com";
  /** CONFIGURATION END **/

  _sf_async_config.sections = "Dinero";

  (function(){
  function loadChartbeat() {
    window._sf_endpt=(new Date()).getTime();
    var e = document.createElement("script");
    e.setAttribute("language", "javascript");
    e.setAttribute("type", "text/javascript");
    e.setAttribute('src', '//static.chartbeat.com/js/chartbeat.js');
    document.body.appendChild(e);
  }
  var oldonload = window.onload;
  window.onload = (typeof window.onload != "function") ?
     loadChartbeat : function() { oldonload(); loadChartbeat(); };
  })();
</script>


<!-- ADDTHIS SETUP -->
<script type="text/javascript">
  <!--
      // @see	http://www.addthis.com/help/client-api
      var addthis_config = {
      // basic options:
      username: "YOUR ADDTHIS USERNAME", // Always global to a page. (string)
      ui_508_compliant: true,	         // ui-508 Accesibility Compliance (set to true)
      ui_language: "es",		 // UI language
      data_track_addressbar: false,
      data_track_clickback: false,
      // ui_use_css: false,
      // data_track_clickback: true,
      // Cobrand:
      ui_cobrand: "El Confidencial Digital",
      ui_header_color: "#fff",
      ui_header_background: "#FA7734",
      // custom destinations:
      services_compact: 'email, facebook, twitter, more',
      services_exclude: 'print'
      };
      //-->
</script>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js"></script>


<script type="text/javascript">
// ******* GOOGLE *******
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-4982657-1']);
_gaq.push(['_trackPageview']);
(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>

<script type="text/javascript">
//  ******* COMSCORE *********
var _comscore = _comscore || [];
_comscore.push({ c1: "2", c2: "15866089" });
(function() {
var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
el.parentNode.insertBefore(s, el);
})();
</script>

<script type="text/javascript">
  ECDPRJ.socialTools();
</script>


<script type="text/javascript">
	var rand= "";
	while(rand.length < 9) {
		rand += "" + parseInt(Math.random(0) * 9);
	}

	var i = new Image(1, 1);
	i.src = "/bbtstats/pixel.gif?category=30&entityId=1030000" + "&rand=" + rand; 
</script>
<noscript>
	<img width="1" height="1" src="/bbtstats/pixel.gif?category=30&entityId=1030000&rand=53555205" />
</noscript>


<script type="text/javascript">
	var rand= "";
	while(rand.length < 9) {
		rand += "" + parseInt(Math.random(0) * 9);
	}

	var i = new Image(1, 1);
	i.src = "/bbtstats/pixel.gif?category=1&entityId=ECDNWS20150126_0037" + "&rand=" + rand; 
</script>
<noscript>
	<img width="1" height="1" src="/bbtstats/pixel.gif?category=1&entityId=ECDNWS20150126_0037&rand=1643457848" />
</noscript>
</body>
</html>