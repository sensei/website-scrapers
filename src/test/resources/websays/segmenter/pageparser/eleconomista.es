<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
					"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
					<html xmlns="http://www.w3.org/199/xhtml" xml:lang="es" xmlns:fb="http://www.facebook.com/2008/fbml">
					<head><title>El paro se redujo en 195.200 personas en el tercer trimestre y la tasa cae al 23,67% - elEconomista.es</title>
					<meta property= "og:title" name="title" content="El paro se redujo en 195.200 personas en el tercer trimestre y la tasa cae al 23,67% - elEconomista.es" />

<meta property="og:description" name="description" content="El paro bajó en 195.200 personas en el tercer trimestre del año respecto al trimestre anterior, un 3,5% menos, registrando su mayor descenso en un tercer trimestre dentro la serie histórica, según datos de la Encuesta de Población Activa (EPA) publicados este jueves por el Instituto Nacional de Estadística (INE). La otra cara de la EPA: un retrato de España a través de 30 datos curiosos." />

<meta property="og:image" name="image" content="http://s01.s3c.es/imag/video/1048x576/6/e/6/record-caida-paro.jpg" />

<meta name="image_thumbnail" content="http://s01.s3c.es/imag/video/1048x576/6/e/6/50x50_record-caida-paro.jpg" />
<meta http-equiv='X-UA-Compatible' content='IE=Edge' /><meta name="keywords" content="TRIMESTRE,PARO,TERCER,PERSONAS,REDUJO" />
			<meta name="distribution" content="global" />
			<meta name="resource-type" content="document"/>
			<meta property="og:type" content="article" />
			<meta name="robots" content="all"/>
			<meta name="author" content="elEconomista.es" />
			<meta  name="organization" content="Editorial Ecoprensa S.A." />
			<meta  name="classification" content="World, Espanol, Paises, Espana, Noticias y medios, Periodicos, Economicos" />
			<meta  name="revisit-after" content="7 days"/><meta http-equiv="content-Type" content="text/html; charset=utf-8" /><meta name="date" content="2014-10-23T09:03:45+02:00" />
<link rel="stylesheet" type="text/css" href="http://s02.s3c.es/css3/eleconomista/cache/general,carousel,eM-dcha,etp.407.cache.css" media="screen" />
<link rel="stylesheet" type="text/css" href="http://s01.s3c.es/css3/eleconomista/cache/print.css" media="print" />
<script type='text/javascript' language='javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js'></script>
<script type='text/javascript' src='http://s01.s3c.es/js/cookiesdirective.v1.js' charset='utf-8'></script>
<script type='text/javascript' language='javascript' src='http://ajax.googleapis.com/ajax/libs/prototype/1.6.1/prototype.js'></script>
<script type='text/javascript' language='javascript' src='http://ajax.googleapis.com/ajax/libs/scriptaculous/1.8.3/scriptaculous.js'></script>
<script type='text/javascript' language='javascript' src='http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js'></script>
<script type='text/javascript' language='javascript' src='http://ajax.googleapis.com/ajax/libs/scriptaculous/1.8.3/effects.js'></script>
<script type='text/javascript' language='javascript' src='http://s01.s3c.es/js/eleconomista/cache/c18e2338813cf985712efbd86d0037e2.75.cache.js'></script>
<link href="http://www.eleconomista.es/rss/rss-flash-del-mercado.php" rel="alternate" type="application/rss+xml" title="elEconomista :: Flash del Mercado"/>
<link href="http://www.eleconomista.es/rss/rss-category.php?category=indicadores-espana" rel="alternate" type="application/rss+xml" title="elEconomista :: Indicadores España"/><meta name="original-source" content="http://www.eleconomista.es/indicadores-espana/noticias/6181497/10/14/El-paro-baja-en-195200-personas-en-el-tercer-trimestre-la-tasa-de-desempleo-desciende-hasta-el-2367.html" /><meta name="cXenseParse:title" content="El paro se redujo en 195.200 personas en el tercer trimestre y la tasa cae al 23,67%" /><meta name="cXenseParse:recs:articleid" content="6181497"><link rel="next" href="http://www.eleconomista.es/indicadores-espana/noticias/6181497/10/14/2/El-paro-baja-en-195200-personas-en-el-tercer-trimestre-la-tasa-de-desempleo-desciende-hasta-el-2367.html" /><script type="text/javascript" src="//apis.google.com/js/plusone.js">{lang: 'es'}</script></head>

	<body id="economia" s="1"><div class="principal-cab">
<script>
  var _comscore = _comscore || [];
  _comscore.push({ c1: "2", c2: "7107810" });
  (function() {
    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
    s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
    el.parentNode.insertBefore(s, el);
  })();
</script><noscript><img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=7107810&amp;cv=2.0&amp;cj=1"></noscript>
<script language="JavaScript" type="text/javascript" src="http://s01.s3c.es/js/s_code.2014.pro.v2.js"></script><script language="JavaScript" type="text/javascript">//<![CDATA[
		s.pageName="";
		s.channel="Red ElEconomista";
		s.prop1="elEconomista.es";
		s.prop2="elEconomista.es";
		s.prop6="El Economista";s.prop8="elEconomista.es";s.prop9="eleconomista.es";

		s.campaign="";
		s.eVar1="";
		s.eVar1="";
		s.eVar2="";
		s.eVar3="";
		s.eVar4="";
		s.eVar5="";
		var s_code=s.t();if(s_code)document.write(s_code)
	//]]></script><script language="JavaScript" type="text/javascript"></script><noscript><img src="http://eleconomista.d1.sc.omtrdc.net/b/ss/eleconomista/1/H.25.4--NS/0" height="1" width="1" border="0" alt=""></noscript>
<script type="text/javascript" src="//secure-uk.imrworldwide.com/v60.js"></script><script type="text/javascript">
		 	var pvar = { cid: "es-eleconomista", content: "0", server: "secure-uk" };
		 	var trac = nol_t(pvar);
		 	 trac.record().post();
		</script><noscript><div><img src="//secure-uk.imrworldwide.com/cgi-bin/m?ci=es-eleconomista&amp;cg=0&amp;cc=1&amp;ts=noscript" width="1" height="1" alt=""></div></noscript>
<script language="javascript">//<![CDATA[
		var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
		document.write(unescape("%3Cscript src=\'" + gaJsHost + "google-analytics.com/ga.js\' type=\'text/javascript\'%3E%3C/script%3E"));
	//]]></script><script language="javascript">//<![CDATA[
		var pageTracker = _gat._getTracker('UA-1010908-1');	
		pageTracker._initData();
		pageTracker._trackPageview();

//]]></script><div id="banners-superiores_1">
<script type="text/javascript" language="javascript">//<![CDATA[
		var time = new Date() ;
		randnum= (time.getTime()) ;
	//]]></script><script language="javascript">//<![CDATA[
		sz = "1x1" ;
		st = "eleconomista.es" ;
		std = "http://ad.es.doubleclick.net/" ;
		isnt = "";
		sect="6181497";
		document.write( '<SCR'+'IPT language="JavaScript" SRC="'+std+'adj/'+st+'/economista_noticias_indicadores_espana;tile=1;dcopt=ist;sz='+sz+';isnt='+isnt+';ord='+randnum+'?"></SCR'+'IPT>' ) ;
	//]]></script><noscript><a href="http://ad.doubleclick.net/jump/eleconomista.es/economista_noticias_indicadores_espana;tile=1;sz=1x1;sect=6181497;ord=123456789?" target="_blank" rel="nofollow"><img src="http://ad.doubleclick.net/ad/eleconomista.es/economista_noticias_indicadores_espana;sz=1x1;sect=6181497;ord=123456789?" width="1" height="1" border="0"></a></noscript>
</div>
<div class="red3">
<div class="menu3" style="width:auto;"><ul>
<li class="sel"><a href="http://www.eleconomista.es">Portada</a></li>
<li class="ncab-ed"><a href="http://ecodiario.eleconomista.es">Eco<span style="color: #d5b036;">Diario</span></a></li>
<li class="ncab-etv" style="padding-top: 0; height: 30px;"><a href="http://ecoteuve.eleconomista.es/"><img src="http://s04.s3c.es/imag3/ecotv/etv-peq.png"></a></li>
<li class="ncab-em"><a href="http://www.eleconomista.es/ecomotor">Eco<span style="color: #0079c8;">Motor</span></a></li>
<li class="ncab-ea"><a href="http://ecoaula.eleconomista.es">Eco<span style="color: #f60;">Aula</span></a></li>
<li class="ncab-el"><a href="http://www.eleconomista.es/ecoley">Eco<span style="color: #9c6;">ley</span></a></li>
<li class="ncab-el"><a href="http://www.eleconomista.es/evasion">Evasión</a></li>
<li class="ncab-et"><a href="http://www.eleconomista.es/ecotrader">Eco<span style="color: #f00;">trader</span></a></li>
<li class="ncab-en"><a href="http://www.eleconomista.es/monitor"><span style="color: #f60;">el</span>Monitor</a></li>
<li onclick="window.location.href='http://www.eleconomista.es/pymes'" class="ncab-en patro-logo-py"><a href="http://www.eleconomista.es/pymes"><span>Eco</span><span style="color: #2A92BE;">pymes</span></a></li>
<li class="ncab-en"><a href="http://www.eleconomista.es/english/">In English</a></li>
<li class="menu_right li-ame" style="z-index: 2300;">
<a class="drop desp-ame" href="http://www.eleconomistaamerica.com/" target="_blank">América</a><div class="dropdown_1column align_right" style="top:27px; z-index: 3000;">
<div class="col_1 new ">
<h2>Países</h2>
<ul class="greybox"><li><a href="http://www.eleconomistaamerica.com">Portada</a></li></ul>
<ul>
<li><a href="http://www.eleconomistaamerica.com.ar/"><img style="vertical-align:middle; margin-right:3px;" src="http://s01.s3c.es/imag3/latam/f-argentina.png">Argentina</a></li>
<li><a href="http://www.eleconomistaamerica.com.br/"><img style="vertical-align:middle; margin-right:3px;" src="http://s01.s3c.es/imag3/latam/f-brasil.png">Brasil</a></li>
<li><a href="http://www.eleconomistaamerica.cl/"><img style="vertical-align:middle; margin-right:3px;" src="http://s01.s3c.es/imag3/latam/f-chile.png">Chile</a></li>
<li><a href="http://www.eleconomistaamerica.co/"><img style="vertical-align:middle; margin-right:3px;" src="http://s01.s3c.es/imag3/latam/f-colombia.png">Colombia</a></li>
<li><a href="http://www.economiahoy.mx/"><img style="vertical-align:middle; margin-right:3px;" src="http://s01.s3c.es/imag3/latam/f-mexico.png">México</a></li>
<li><a href="http://www.eleconomistaamerica.pe/"><img style="vertical-align:middle; margin-right:3px;" src="http://s01.s3c.es/imag3/latam/f-peru.png">Perú</a></li>
</ul>
</div>
<div class="col_1 new black_box" style="margin-top: 10px; width: 120px;">
<h2 style="margin-bottom: 5px; padding-bottom: 5px; font-size: 16px;">Materias Primas</h2>
<ul class="dd-tt-s">
<li><a href="http://www.eleconomistaamerica.com/materia-prima/Crudo-wti">Crudo</a></li>
<li><a href="http://www.eleconomistaamerica.com/materia-prima/oro">Oro</a></li>
<li><a href="http://www.eleconomistaamerica.com/materia-prima/soja">Soja</a></li>
<li><a href="http://www.eleconomistaamerica.com/materias-primas/">Ver más</a></li>
</ul>
<h2 style="margin-bottom: 5px; padding-bottom: 5px; font-size: 16px;">Índices</h2>
<ul class="dd-tt-s">
<li><a href="http://www.economiahoy.mx/indice/IPC-MEXICO">IPC México</a></li>
<li><a href="http://www.eleconomistaamerica.com/indice/COL20">COL 20</a></li>
<li><a href="http://www.eleconomistaamerica.com/indice/BOVESPA">BOVESPA</a></li>
<li><a href="http://www.eleconomistaamerica.com/indices-latinoamericanos/index.html">Ver más</a></li>
</ul>
</div>
</div>
</li>
</ul></div>
<div class="cont2" style="width: 250px; float: right; margin-top: 0px;"><div id="login-cont">
<div id="login-superior" class="topnav"> ¿Usuario de elEconomista? <a id="login-boton" class="login-boton"><span>Conéctate</span></a>
</div>
<fieldset id="login-menu" class="login-menu">
<div class="field1"><form method="post" id="signin" onsubmit="Captura_mouseClick_enviar();">
<p id="mensaje-error"></p>
<p><label for="username">Dirección de email</label><input id="username" name="username" value="" title="username" tabindex="4" type="text" autocorrect="off" autocapitalize="off"></p>
<p><label for="password">Contraseña</label><input id="password" name="password" value="" title="password" tabindex="5" type="password"></p>
<p class="login-recordar"><span id="boton_registro"><input id="login-enviar" value="Entrar" tabindex="6" type="submit"></span><input id="remember" name="remember_me" value="1" tabindex="7" type="checkbox" CHECKED><label for="remember">Recordarme</label></p>
<p class="login-enlace"><a href="http://www.eleconomista.es/registro/perfil.php" id="resend_password_link">¿Olvidaste tu contraseña?</a></p>
<p class="login-enlace"><a href="http://www.eleconomista.es/registro/alta.php" id="resend_password_link">Registrarse</a></p>
</form></div>
<div class="field2">
<h4 style="color: #f60;">Servicios Premium</h4>
<p class="login-enlace"><a style="color:#000!important;" href="/ecotrader" id="resend_password_link">Eco<span style="color:#f00;">trader</span></a></p>
<p class="login-enlace"><a href="http://www.eleconomista.es/hemeroteca" id="resend_password_link">Edición PDF + Hemeroteca</a></p>
<p class="forgot"><a href="http://www.eleconomista.es/elsuperlunes" id="resend_password_link">El Superlunes</a></p>
<h4 style="margin-top: 15px; color: #f60;">Servicios gratuitos</h4>
<p class="login-enlace"><a href="http://listas.eleconomista.es" id="resend_password_link">Listas y rankings</a></p>
<p class="login-enlace"><a href="http://www.eleconomista.es/cartera/portada.php" id="resend_password_link">Cartera</a></p>
<p class="login-enlace"><a href="http://www.eleconomista.es/foros" id="resend_password_link">Foros</a></p>
</div>
</fieldset>
<fieldset id="login-menu2" class="login-menu" style="display: none">
<div class="field1">
<h4>Zona usuario</h4>
<p class="login-enlace"><a href="http://www.eleconomista.es/registro/datosPersonales.php" id="resend_password_link">Datos personales</a></p>
<p class="login-enlace"><a href="http://www.eleconomista.es/registro/perfil.php" id="resend_password_link">Editar perfil</a></p>
<p class="login-enlace"><a href="http://www.eleconomista.es/premium/mediospago.php" id="resend_password_link">Medios de pago</a></p>
<p class="login-enlace"><a href="http://www.eleconomista.es/premium/facturas.php" id="resend_password_link">Facturas</a></p>
<p class="login-enlace"><a href="http://www.eleconomista.es/premium/suscripciones.php" id="resend_password_link">Servicios Premium</a></p>
<form method="post" id="signin2"><p class="remember"><span id="boton_desconectar"><input id="signin_submit" class="desconexion" value="Desconectarme" tabindex="6" type="button"></span></p></form>
</div>
<div class="field2">
<h4 style="color: #f60;">Servicios Premium</h4>
<p class="forgot"><a href="http://www.eleconomista.es/ecotrader" id="resend_password_link">Ecotrader</a></p>
<p class="forgot"><a href="http://www.eleconomista.es/hemeroteca" id="resend_password_link">Edición PDF + Hemeroteca</a></p>
<p class="forgot"><a href="http://www.eleconomista.es/elsuperlunes" id="resend_password_link">El Superlunes</a></p>
<h4 style="margin-top: 15px; color: #f60;">Servicios gratuitos</h4>
<p class="forgot"><a href="http://listas.eleconomista.es" id="resend_password_link">Listas y rankings</a></p>
<p class="forgot"><a href="http://www.eleconomista.es/cartera/portada.php" id="resend_password_link">Cartera</a></p>
<p class="forgot"><a href="http://www.eleconomista.es/foros" id="resend_password_link">Foros</a></p>
</div>
</fieldset>
</div></div>
<div style="clear: both;display: block; margin: 10px 0;"></div>
</div>
<script type="text/javascript">//<![CDATA[
		if ( getCookie("Navegacion_Web") ) {
			var content = '<li><a href="#" onclick="return soloIPhone() ;">Volver a la version iPhone</a></li>' ;
			
			$("menuLink").insert( {bottom: content} ) ; 
		}
		
		function soloIPhone() {
			deleteCookie("Navegacion_Web", "/", "http://www.eleconomista.es") ;
			location = "http://iphone.eleconomista.mobi"
		}
		//]]></script>
</div>
<style>
            #localizacion_aviso_predefinido{
                padding: 10px;
                border-right: 1px solid #B9B7AC;
                width: 970px;
                margin: 0 auto;
            }
            .el-ed{
                padding: 10px;
                border: 1px solid #096490;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                background-image: linear-gradient(bottom, rgb(12,145,206) 42%, rgb(30,132,179) 71%, rgb(31,158,217) 86%);
                background-image: -o-linear-gradient(bottom, rgb(12,145,206) 42%, rgb(30,132,179) 71%, rgb(31,158,217) 86%);
                background-image: -moz-linear-gradient(bottom, rgb(12,145,206) 42%, rgb(30,132,179) 71%, rgb(31,158,217) 86%);
                background-image: -webkit-linear-gradient(bottom, rgb(12,145,206) 42%, rgb(30,132,179) 71%, rgb(31,158,217) 86%);
                background-image: -ms-linear-gradient(bottom, rgb(12,145,206) 42%, rgb(30,132,179) 71%, rgb(31,158,217) 86%);
                background-image: -webkit-gradient(
                linear,
                left bottom,
                left top,
                color-stop(0.42, rgb(12,145,206)),
                color-stop(0.71, rgb(30,132,179)),
                color-stop(0.86, rgb(31,158,217))
                );
                
                #background-color:rgb(12,145,206);
                #width:948px;
                /* For Internet Explorer 5.5 - 7 */
                filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#1f9ed9, endColorstr=#1e84b3);
                
                /* For Internet Explorer 8 */
                -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#1f9ed9, endColorstr=#1e84b3)";
                padding-bottom:5px\9;
            }
            
            .el-ed h4 {
                margin: 0;
                font-size: 13px;
                color: #fff;
                font-weight:bold;
            }
            .el-ed h4 a:link, .el-ed h4 a:active, .el-ed h4 a:visited {
                color: #fff;
                text-decoration: underline;
            }
            .cierre {
                float: right;
                margin-top: -20px;
            }
            
            .el-ed-flecha {
                position: absolute;
                top: 30px;
                left: 100px;
            }
            
            .edicion {
                color: #333;
                font-size: 12px;
                margin-top: 10px;
            }
            
			.eleccion-pais { 
				/*background: #C34E00; */
				margin: 5px 5px 8px 0; 
				padding: 15px 10px; 
				color: #fff; 
				font-size: 16px;
				overflow: hidden;
				/*background: #c00;*/
				background:#F60;
				font-family: 'Trebuchet MS', Helvetica, sans-serif;
            }
            .eleccion-pais a:link, .eleccion-pais a:active, .eleccion-pais a:visited {
                color: #fff;
                text-decoration:none;
            }
            .eleccion-pais a:hover { text-decoration: underline;}
            .eleccion-pais-cerrar { float: right;}
            .eleccion-pais img{ margin-right:5px; vertical-align: baseline; opacity: 0.8;}
            .eleccion-pais b { /*color: #f60;*/ text-transform:uppercase;}
            .eleccion-indice { 
				font-size: 12px; 
				color:#333;
				/*margin-left: 20px;*/ 
				margin-top: 10px; 
				height:23px;
				padding-top: 10px; 
				padding-left: 5px;
				background: #ffe0cc;
            }
            .eleccion-indice span { /*background: #0072bf; padding: 4px;*/ margin-right: 5px; text-transform: uppercase; color: #333; font-weight:bold;}
            .eleccion-indice a { color:#333; cursor:pointer;}
            .eleccion-indice a:hover { text-decoration:none;}
            .eleccion-indice a span{ color: #0c0; font-weight:normal;}
            .eleccion-indice a span.accion1{ color: #0c0;}
            .eleccion-indice a span.accion-1{ color: #f00;}
            .eleccion-indice a span.accion-1{ color: #f00;}
            .eleccion-indice a.cotizacion{ color: #000;}
        </style><div class="row" mod="4379">
<div id="localizacion_aviso_predefinido" style="display:none;"><div class="eleccion-pais">
<a href="#" onclick="redirigirASite();"><img src="http://s01.s3c.es/imag3/flechas-hem/final_d_org.gif"><span id="localizacion_aviso_pais"></span></a><div class="eleccion-pais-cerrar"><a href="#" onclick="jQuery('#localizacion_aviso_predefinido').hide();">X</a></div>
<div class="eleccion-indice" id="localizacion_aviso_cotizaciones"></div>
</div></div>
<script language="javascript">//<![CDATA[
                var siteprincipal = '';
                var siteprincipal=getCookie("siteprincipal");
                var siteprincipalConsulta=getCookie("siteprincipalConsulta");
                var siteprincipalRecomendado = getCookie("siteprincipalRecomendado");
                var modulosSite = new Array();
                var modulosReemplazados = new Array();
                
                jQuery(window).load(function(){
                    if((siteprincipal==null || siteprincipal=='' ) || ( siteprincipalConsulta==1 && siteprincipalRecomendado>1 )){
                        //Petición de datos
                        var ajax_data = {};
                        
                        var ajaxRequest = new Ajax.Request(
                        '/iplocalizacion/iplocalizacion.php',
                        {
                            method: 'post',
                            parameters: ajax_data,
                            //asynchronous: true,
                            onComplete: function(xmlHttpRequest, responseHeader){
                                if(xmlHttpRequest.responseJSON != null ){
                                    oRespuesta = xmlHttpRequest.responseJSON;
                                    if(oRespuesta.resultado == 1){
                                        siteprincipal = oRespuesta.datos.site_local;
										jQuery('#localizacion_aviso_pais').html(oRespuesta.datos.msg);
										jQuery('#localizacion_avisoclick_pais').html(oRespuesta.datos.msgclick);
                                        mostrarCotizacionesSiteAsociado(oRespuesta.datos.cotizaciones, '');
                                        //if(typeof(siteprincipalRecomendado) == "undefined") {
                                            setCookie("siteprincipalRecomendado", siteprincipal, 1,"/",".eleconomista.es");
                                            siteprincipalRecomendado = siteprincipal;
                                        //}
                                        jQuery('#localizacion_aviso_predefinido').show();
                                        //if(siteprincipal!='')alert(siteprincipal);
                                        recargarModulosPorLocalizacion();
                                    }else{
                                        //No hay site asociado a esa localizacion  
                                        setCookie("siteprincipalConsulta", 1,1,"/",".eleconomista.es");
                                        setCookie("siteprincipalRecomendado", 1,1,"/",".eleconomista.es");
                                    }
                                }else{
                                    //
                                }
                            }
                            
                        });
                    }else{
                        //Site por defecto definido. Dar opción a su eliminación
                        //alert('Definido - ' + siteprincipal);
                    }
                    
                });
                
                function recargarModulosPorLocalizacion(){
                    //console.log("prueba");
                    /* */
                    var ajaxRequest2 = new Ajax.Request(
                    "/iplocalizacion/noticiasSite.php"
                    ,{
                        method: "get"
                        , parameters: {'site': siteprincipalRecomendado, 'Modulos': modulosSite.join(",")}
                        , onComplete: function(xmlHttpRequest, responseHeader){
                            if(xmlHttpRequest.responseJSON != null ){
                                oRespuesta = xmlHttpRequest.responseJSON;
                                if(oRespuesta.resultado == 1){
                                    //captura el listado de datos
                                    for(k=0;k<oRespuesta.datos.length;k++){
                                        //alert("change-"+ k);
                                        if(modulosSite.indexOf(oRespuesta.datos[k].mod+"")>-1){
                                            moduloAReemplazar = modulosReemplazados[modulosSite.indexOf(oRespuesta.datos[k].mod+"")];
                                            //jQuery("#prueba_" + moduloAReemplazar ).html(oRespuesta.datos[k].codigoHTML);
                                            jQuery('div[mod="' + moduloAReemplazar +'"]' ).html(oRespuesta.datos[k].codigoHTML);
                                        }
                                    }
                                }else{
                                    
                                }
                            }else{
                                //
                            }
                        }
                        
                    }
                    );
                }
                
                function establecerSitePrincipal(){
                    if(siteprincipal!='') {setCookie("siteprincipal", siteprincipal,365,"/",".eleconomista.es");jQuery('#localizacion_aviso_predefinido').hide();}
                    else{setCookie("siteprincipal", '',-1,"/",".eleconomista.es");}
                }
                
                function mostrarCotizacionesSiteAsociado(aCotizaciones, sEnlace){
                    if(sEnlace=='') sEnlace ='http://www.eleconomistaamerica.com/indices-latinoamericanos/index.html';
                    var sHTML = '';
                    for(i=0;i<aCotizaciones.length;i++){
                        //sHTML += ((sHTML!='')? ',':'');
                        sHTML += '<a href="' + aCotizaciones[i].CotizacionURL + '" class="cotizacion">' + aCotizaciones[i].CotizacionNombre + ' <span class="ultimo_' + aCotizaciones[i].CotizacionId + '" table="mod_geo" item="item_' + aCotizaciones[i].CotizacionCodigoFinInfo + '_' + aCotizaciones[i].CotizacionQuality + '" field="precio_ultima_cotizacion" source="lightstreamer">' + aCotizaciones[i].CotizacionValor + ' </span><span class="ico_' + aCotizaciones[i].CotizacionId + '" table="mod_geo" item="item_' + aCotizaciones[i].CotizacionId + '_' + aCotizaciones[i].CotizacionId + '" field="#arrow" source="lighstreamer"><img height="11" width="11" src="' + aCotizaciones[i].CotizacionIcono + '"/></span><span class="accion' + aCotizaciones[i].CotizacionEstado + ' pDif_' + aCotizaciones[i].CotizacionId + ' estado_' + aCotizaciones[i].CotizacionId + '"  acc="' + aCotizaciones[i].CotizacionEstado + '" table="mod_geo" item="item_' + aCotizaciones[i].CotizacionCodigoFinInfo + '_' + aCotizaciones[i].CotizacionQuality + '" field="variacion_porcentual" source="lightstreamer">' + aCotizaciones[i].CotizacionVariacion + '</span></a> | ';
                    }
                    sHTML = '<span>Cotizaciones:</span>' + sHTML + '<a href="' + sEnlace + '" class="cotizacion">Ver todas</a>';
                    jQuery('#localizacion_aviso_cotizaciones').html(sHTML);
                    //Activación del lightstreamer
                    /*
					var mod_geo = new OverwriteTable( null, null, "MERGE" ) ;
                    mod_geo.setSnapshotRequired( true ) ;
                    mod_geo.setRequestedMaxFrequency( 1.0 ) ;
                    mod_geo.onItemUpdate = updateItem ;
                    mod_geo.setDataAdapter("FINFEED");
                    mod_geo.onChangingValues = formatValues ;
                    mod_geo.setPushedHtmlEnabled( true ) ;
                    pushPage.addTable( mod_geo, "mod_geo" ) ;
                    var decimales = new Hash() ;
                    for(i=0;i<aCotizaciones.length;i++){
                        decimales.set('item_' + aCotizaciones[i].CotizacionId + '_' + aCotizaciones[i].CotizacionId , 2) ;
                    }
                    mod_geo.decimales = decimales ;
					*/
                }
                
                
    function redirigirASite(){
        if(siteprincipal==21){ window.location.replace('http://www.eleconomistaamerica.co/');}
        if(siteprincipal==22){ window.location.replace('http://www.eleconomistaamerica.mx/');}
        if(siteprincipal==23){ window.location.replace('http://www.eleconomistaamerica.cl/');}
        if(siteprincipal==24){ window.location.replace('http://www.eleconomistaamerica.com.br/');}
        if(siteprincipal==25){ window.location.replace('http://www.eleconomistaamerica.com.ar/');}
        if(siteprincipal==26){ window.location.replace('http://www.eleconomistaamerica.pe/');}
    }
    //]]></script>
</div>

	<div class="principal"><div class="cabecera">
<div class="logo">
<a href="/"><img src="http://s01.s3c.es/imag3/logo-h1.gif" alt="elEconomista.es"></a><div class="lead izda"><span class="l-fecha">Jueves, 23 de Octubre de 2014</span></div>
</div>
<div class="ruta">Indicadores España</div>
<div class="buscador buscador-int"><form method="GET" action="/buscador/resultados.php" name="buscador" style="margin: 0px; padding: 0px; color:#fff" accept-charset="UTF-8">
<input type="hidden" value="partner-pub-2866707026894500:3x0zyny7y13" name="cx"><input type="hidden" value="FORID:10" name="cof"><input type="hidden" value="UTF-8" name="ie"><input type="hidden" value="0" name="recordatorio" style="margin: 0pt 3px 3px; padding: 0pt;"><input type="hidden" value="0" name="pagina" style="margin: 0pt 3px 3px; padding: 0pt;"><input type="text" id="texto_formulario" name="fondo" placeholder="Noticias, acciones..." style="height:26px;"><input type="submit" value="Buscar" name="Submit" class="boton-buscar">
</form></div>
<script language="javascript">
	//<![CDATA[		
		function DesplegarMenu(menu_actual, menu_nuevo) {
			Element.removeClassName(menu_actual, 'sel');
			Element.removeClassName('s-'+menu_actual, 'sel');
			Element.addClassName('s-'+menu_actual, 'non-sel');
			
			Element.removeClassName('s-'+menu_nuevo,'non-sel');
			Element.addClassName('s-'+menu_nuevo,'sel');
			Element.addClassName(menu_nuevo,'sel');
			return (menu_nuevo);
			
		};
		
		function Patrocinio(id, clase) {
			$(id).toggleClassName(clase);
		}
			
		var menu_actual = 'eco-menu';
	//]]>
	</script><div class="menu-v2"><ul id="menu-drop">
<li id="home-menu"><a href="/">Portada</a></li>
<li id="merc-menu" onmouseover="javascript:Patrocinio('merc-menu','merc-pat-b');" onmouseout="javascript:Patrocinio('merc-menu','merc-pat-b');">
<a class="drop" href="/mercados-cotizaciones/index.html"><span>Mercados y Cotizaciones</span></a><div class="dropdown_3columns">
<div class="col_1">
<div class="drop-tit">Mercados</div>
<ul>
<li><a href="/mercados-cotizaciones/index.html">Portada Mercados</a></li>
<li><a href="/mercados-cotizaciones/renta-variable-fundamental/">Renta Variable</a></li>
<li><a href="/mercados-cotizaciones/renta-fija/">Renta Fija</a></li>
<li><a href="/mercados-cotizaciones/divisas/">Divisas</a></li>
<li><a href="/mercados-cotizaciones/divisas/">Calculadora Divisas</a></li>
<li><a href="/mercados-cotizaciones/materias-primas/">Materias Primas</a></li>
<li><a href="/cfd/index.html">CFDs</a></li>
<li><a href="/carteras/index.html">Carteras consenso</a></li>
<li><a href="/fondos/buscador_fondos_avanzado.php">Buscador de fondos</a></li>
</ul>
<ul class="greybox"><li><a class="dest" href="/english/">English Edition</a></li></ul>
</div>
<div class="col_2 black_box" style="width: 255px;">
<div class="drop-tit">Cotizaciones</div>
<ul>
<li style="width: 250px;">
<a href="http://www.cepsa.com" rel="nofollow" target="_blank"><img src="http://s04.s3c.es/imag3/logos/cepsa.png" alt="cepsa" style="float: right;"></a><a href="/indices-mundiales/index.html">Índices mundiales</a>
</li>
<li style="width: 180px;"><a href="http://www.eleconomistaamerica.com/indices-latinoamericanos/index.html">Índices latinoamericanos</a></li>
<li style="width: 180px;"><a href="/stoxx/index.html">Índices Stoxx</a></li>
</ul>
<div class="col_1" style="width: 120px;">
<h3>Mercado español</h3>
<ul>
<li><a href="/indice/ibex-35">Ibex 35</a></li>
<li><a href="/mercados/mercado-continuo">M. Continuo</a></li>
<li><a href="/indice/IGBM">IGBM</a></li>
<li><a href="/indice/eco10">Eco 10</a></li>
<li><a href="/eco30">Eco 30</a></li>
<li><a href="/indice/ibex-dividendo">Ibex dividendo</a></li>
</ul>
<h3>Divisas y tipos</h3>
<ul>
<li><a href="/cruce/EURUSD">Euro/Dólar</a></li>
<li><a href="/cruce/EURJPY">Euro/Yen</a></li>
<li><a href="/cruce/EURGBP">Euro/Libra</a></li>
<li><a href="/tipo-interbancario/euribor-1-year">Euribor 1 año</a></li>
<li><a href="/tipo-interbancario/euribor-6-meses">Euribor 6 meses</a></li>
</ul>
<ul class="greybox"><li style="width: auto;overflow:hidden"><a class="dest" href="/prima-de-riesgo/index.html" style="float:left">Primas de Riesgo</a></li></ul>
</div>
<div class="col_1" style="margin-right: 0; width: 120px;">
<h3>Europa</h3>
<ul>
<li><a href="/indice/EUROSTOXX-50">Stoxx50</a></li>
<li><a href="/indice/DAX-30">Dax30</a></li>
<li><a href="/indice/CAC-40">Cac40</a></li>
<li><a href="/indice/FTSE-100">FTSE</a></li>
</ul>
<h3>EEUU/Asia</h3>
<ul>
<li><a href="/indice/DOW-JONES">DowJones</a></li>
<li><a href="/indice/S-P-500">SP500</a></li>
<li><a href="/indice/NASDAQ-100">Nasdaq</a></li>
<li><a href="/indice/NASDAQ-COMPOSITE">Nasdaq Comp.</a></li>
<li><a href="/indice/NIKKEI">Nikkei</a></li>
</ul>
</div>
</div>
</div>
</li>
<li id="emp-menu" class="emp-pat" onmouseover="javascript:Patrocinio('emp-menu','emp-pat-b');" onmouseout="javascript:Patrocinio('emp-menu','emp-pat-b');">
<a class="drop" href="/empresas-finanzas/index.html"><span>Empresas</span></a><div class="dropdown_3columns">
<div class="col_3"><div class="drop-tit">Sectores</div></div>
<div class="col_1"><ul>
<li><a href="/empresas-finanzas/index.html">Portada empresas</a></li>
<li><a href="/empresas-finanzas/distribucion-alimentacion/index.html">Alimentación<img src="http://s04.s3c.es/imag3/logos/lidl-p.png" alt="Lidl" style="float:right; margin-top:3px; width:18px;"></a></li>
<li><a href="/empresas-finanzas/energia/index.html">Energía
            </a></li>
<li><a href="/empresas-finanzas/consumo/index.html">Consumo</a></li>
<li><a href="/empresas-finanzas/fundaciones/">Fundaciones</a></li>
</ul></div>
<div class="col_1"><ul>
<li style="overflow:hidden">
<a style="float:left;" href="/empresas-finanzas/infraestructuras-construccion-inmobiliario/index.html">Construcción</a><a target="_blank" href="http://www.fcc.es/fccweb/index.html"><img src="http://s04.s3c.es/imag3/logos/fcc81x17.gif" alt="FCC" style="margin-left:5px; float:left"></a>
</li>
<li><a href="/ecosanidad/index.html">Sanidad</a></li>
<li><a href="/empresas-finanzas/finanzas-seguros/index.html">Finanzas </a></li>
<li><a href="/tecnologia/index.html">Tecnología</a></li>
</ul></div>
<div class="col_1"><ul>
<li><a href="/empresas-finanzas/telecomunicaciones-medios-comunicacion/index.html">Telecomunicaciones</a></li>
<li><a href="/empresas-finanzas/transporte/index.html">Transporte</a></li>
<li><a href="/especiales/turismo-viajes/">Turismo y viajes</a></li>
<li><a href="/empresas-finanzas/agua-medioambiente/index.html">Agua</a></li>
</ul></div>
<div class="col_3"><ul class="greybox">
<li style="width: auto;"><a href="/empresas-finanzas/seguros/">Seguros y Asegurados Magazine- <span style="color: #666666; font-weight: normal; font-size: 11px;">Vea la sección y lea la nueva revista</span></a></li>
<li style="width: auto;overflow:hidden">
<a style="float:left" href="/pymes/index.html">Emprendedores y Pymes</a><a href="http://www.bancopopular.es/popular-web"><img src="http://s04.s3c.es/imag3/logos/popular2.png" alt="Banco Popular" style="margin-left:10px; float:left"></a>
</li>
<li style="width: auto; overflow: hidden;">
<a href="/ecosanidad/index.html" style="float: left;">Sanidad - <span style="color: #666666; font-weight: normal; font-size: 11px;">Vea la sección y lea la nueva revista</span></a><a target="_blank" href="http://www.novartis.es/"><img src="http://s04.s3c.es/imag3/logos/novartis.png" alt="Novartis" style="margin-left:10px; float:left"></a>
</li>
<li style="width: auto;overflow:hidden">
<a href="http://empresite.eleconomista.es" style="float: left;">Buscador de empresas - <span style="color: #666666; font-weight: normal; font-size: 11px;">no cotizadas</span></a><a href="http://empresite.eleconomista.es/"><img src="http://s04.s3c.es/imag3/logos/empresite-p2.png" alt="Empresite" style="margin-left:10px; float:left"></a>
</li>
<li style="width: auto; overflow: hidden;">
<a href="http://seguros.eleconomista.es/" style="float: left;">Seguros</a><a href="http://seguros.eleconomista.es/"><img src="http://s04.s3c.es/imag3/logos/acierto-men.png" alt="Acierto" style="margin-left:10px; float:left"></a>
</li>
</ul></div>
</div>
</li>
<li id="eco-menu" class="sel" onmouseover="javascript:Patrocinio('eco-menu','eco-pat-b');" onmouseout="javascript:Patrocinio('eco-menu','eco-pat-b');"><a href="/economia/index.html"><span>Economía</span></a></li>
<li id="tecn-menu" class="tecno-pat" onmouseover="javascript:Patrocinio('tecn-menu','tecno-pat-b');" onmouseout="javascript:Patrocinio('tecn-menu','tecno-pat-b');">
<a class="drop" href="/tecnologia/index.html"><span>Tecnología</span></a><div class="dropdown_1column"><div class="col_1"><ul>
<li><a href="/tecnologia/index.html">Portada tecnología</a></li>
<li><a href="/CanalPDA">CanalPDA</a></li>
</ul></div></div>
</li>
<li id="viv-menu" class="viv-pat" onmouseover="javascript:Patrocinio('viv-menu','viv-pat-b');" onmouseout="javascript:Patrocinio('viv-menu','viv-pat-b');"><a href="/vivienda/index.html"><span>Vivienda</span></a></li>
<li id="opinion-menu">
<a class="drop" href="/opinion/index.html">Opinión/Blogs</a><div class="dropdown_3columns">
<div class="col_1">
<h3><a href="/opinion/">En opinión</a></h3>
<ul class="simple">
<li><a href="/opinion/index.html">Portada opinión</a></li>
<li><a href="/noticias/editoriales">Editoriales</a></li>
<li><a href="/noticias/opinion-blogs">Firmas</a></li>
<li><a href="/blogs/vineta-del-dia/">Viñeta del día</a></li>
</ul>
</div>
<div class="col_2 black_box" style="width: 240px;">
<h3><a href="/blogs/">Blogs</a></h3>
<ul style="width: auto; margin-left: 5px;">
<li style="width: 260px;"><a href="/blogs/desde-el-burladero">Desde el Burladero</a></li>
<li style="width: 260px;"><a href="/blogs/a-la-catalana">A la catalana</a></li>
<li style="width: 260px;"><a href="/blogs/naranjazos/">Naranjazos</a></li>
<li style="width: 260px;"><a href="/blogs/hablemos-de-empresa">Hablemos de empresa</a></li>
<li style="width: 260px;"><a href="/blogs/la-escuela-de-empresarios">Escuela de empresarios</a></li>
<li style="width: 260px;"><a href="/blogs/andanomiks/">Andanomiks</a></li>
<li style="width: 260px;"><a href="/blogs/la-conciencia-del-directivo/">La conciencia del directivo</a></li>
<li style="width: 260px;"><a href="/blogs/emprendedores">Emprendedores</a></li>
<li style="width: 260px;"><a href="/blogs/sumando-ideas/">Sumando ideas</a></li>
<li style="width: 260px;"><a href="/blogs/sensaciones">Sensaciones</a></li>
<li style="width: 260px;"><a href="/blogs">Ver todos</a></li>
</ul>
</div>
</div>
</li>
<li id="autonomias-menu">
<a class="drop" href="#">Autonomías</a><div class="dropdown_1column"><div class="col_1"><ul class="simple">
<li><a href="/catalunya">Cataluña</a></li>
<li><a href="/pais_vasco">País Vasco</a></li>
<li><a href="/andalucia">Andalucía</a></li>
<li><a href="/aragon">Aragón</a></li>
<li><a href="/valenciana">C. Valenciana</a></li>
<li><a href="/castilla_y_leon">Castilla y León</a></li>
</ul></div></div>
</li>
<li id="product-menu" class="menu_right">
<a class="drop" href="/productos/index.html">Diario y Revistas</a><div class="dropdown_3columns align_right">
<div class="col_3"><div class="drop-tit">Edición digital</div></div>
<div class="col_2_2"><ul class="greybox">
<li><a href="http://www.eleconomista.es/elsuperlunes/">elSuperLunes<span style="color: #c00; font-weight: normal; font-size: 11px;">GRATIS</span></a></li>
<li><a href="http://www.eleconomista.es/hemeroteca">Edición PDF</a></li>
</ul></div>
<div class="col_2_2"><ul class="greybox">
<li><a href="http://www.eleconomista.es/ecotablet">Ecotablet</a></li>
<li><a href="http://www.eleconomista.es/edicion-impresa/">Suscriptor periódico</a></li>
</ul></div>
<div class="col_3"><div class="drop-tit">Revistas elEconomista.es</div></div>
<div class="col_2_2"><ul class="greybox">
<li>
<a target="_blank" href="http://www.novartis.es/"><img src="http://s02.s3c.es/imag3/logos/novartis.png" alt="novartis" style="float: right; margin-top: -2px;"></a><a href="http://www.eleconomista.es/ecosanidad/index.html">EcoSanidad</a>
</li>
<li><a href="http://www.eleconomista.es/ecoley/">Iuris &amp; Lex</a></li>
<li><a href="http://www.eleconomista.es/empresas-finanzas/energia/">Energía</a></li>
<li><a href="http://www.eleconomista.es/empresas-finanzas/transporte/index.html">Transporte</a></li>
<li><a href="http://www.eleconomista.es/empresas-finanzas/seguros/">Seguros</a></li>
<li><a href="http://www.eleconomista.es/empresas-finanzas/consumo/index.html">Consumo</a></li>
<li><a href="http://www.eleconomista.es/tecnologia/index.html">Tecnología</a></li>
<li><a href="http://www.eleconomista.es/pymes/">Gestión empresarial</a></li>
<li><a href="http://www.eleconomista.es/andalucia/">Andalucía</a></li>
</ul></div>
<div class="col_2_2"><ul class="greybox">
<li><a href="http://www.eleconomista.es/especiales/ecobolsa/index.php">Ecobolsa</a></li>
<li><a href="http://www.eleconomista.es/ecomotor/pdf/">Ecomotor</a></li>
<li><a href="http://www.eleconomista.es/empresas-finanzas/distribucion-alimentacion/index.html"><img src="http://s04.s3c.es/imag3/logos/lidl-p.png" alt="Lidl" style="float:right; margin-top:-2px; width:18px;">Alimentación</a></li>
<li><a href="http://www.eleconomista.es/kiosco/economia-real/">Economía Real</a></li>
<li><a href="http://www.eleconomista.es/mercados-cotizaciones/index.html">Inversión</a></li>
<li><a href="http://www.eleconomista.es/kiosco/agua/">Agua</a></li>
<li><a href="http://www.eleconomista.es/kiosco/franquicias/">Franquicias y Emprendedores</a></li>
<li><a href="http://www.eleconomista.es/empresas-finanzas/fundaciones/">Fundaciones</a></li>
</ul></div>
<div class="col_3"><div class="drop-tit"><a style="font-size:18px; font-weight:400; color:#000" href="http://tienda.eleconomista.es">Tienda elEconomista</a></div></div>
<div class="col_2_2"><ul class="greybox">
<li><a href="http://tienda.eleconomista.es/products.aspx?categoryid=1114">Todo en Informática</a></li>
<li><a href="http://tienda.eleconomista.es/products.aspx?categoryid=8611">Imagen y sonido</a></li>
</ul></div>
<div class="col_2_2"><ul class="greybox">
<li><a href="http://tienda.eleconomista.es/products.aspx?categoryid=8577">Pequeño electrodoméstico</a></li>
<li><a href="http://tienda.eleconomista.es/products.aspx?categoryid=23294">Los mejores precios en tabletas</a></li>
</ul></div>
<div class="col_3"><h2>Para iPad</h2></div>
<div class="col_2_2"><ul class="greybox"><li><a target="_blank" href="http://www.eleconomista.es/ecotablet/">el<span style="color: #f60;">EcoTablet</span></a></li></ul></div>
<div class="col_2_2"><ul class="greybox"><li><a target="_blank" href="http://itunes.apple.com/us/app/eleconomista-edicion-impresa/id491306100?l=es&amp;ls=1&amp;mt=8">elEconomista Edición Impresa</a></li></ul></div>
</div>
</li>
<li id="servinv-menu" class="menu_right">
<a class="drop" href="/servicios-inversor/index.html">Servicios</a><div class="dropdown_3columns align_right">
<div class="col_2_2"><ul class="greybox">
<li><a href="http://www.eleconomista.es/fichas-de-empresas/">Fichas valor</a></li>
<li><a href="http://www.eleconomista.es/hechos-relevantes/">Últimos hechos CNMV</a></li>
<li><a href="http://www.eleconomista.es/foros">Foro</a></li>
<li><a href="/english/">English Edition</a></li>
<li style="width: auto;overflow:hidden"><a style="float:left" href="http://www.eleconomista.es/conferencias/">Conferencias</a></li>
<li><a href="http://www.eleconomista.es/observatorios">Observatorios</a></li>
</ul></div>
<div class="col_2_2"><ul class="greybox">
<li><a href="http://www.eleconomista.es/analisis-tecnico">Análisis técnico</a></li>
<li><a href="http://listas.eleconomista.es">Listas</a></li>
<li><a href="http://www.eleconomista.es/formacion/">Escuela de inversión</a></li>
<li><a href="http://coleccion.eleconomista.es">Ecoleccionista</a></li>
<li><a href="http://ecoapuestas.eleconomista.es" title="Ecoapuestas">Apuestas</a></li>
<li><a href="http://www.eleconomista.es/entradas/" title="Entradas">Entradas</a></li>
</ul></div>
<div class="col_3"><div class="drop-tit">Servicios para invertir</div></div>
<div class="col_3">
<a href="http://www.eleconomista.es/ecotrader/"><img src="http://s01.s3c.es/imag3/v5/menu/ecotrader.png" width="70" height="70" class="img_left imgshadow" alt=""></a><h3 style="border-bottom: none; margin-bottom: 5px; padding-bottom: 0;"><a href="http://www.eleconomista.es/ecotrader/"><span style="color: #f60;">Ecotrader</span></a></h3>
<p>Información exclusiva sobre el mercado financiero en tiempo real con las trece herramientas financieras más innovadoras del mercado. <a href="http://www.eleconomista.es/ecotrader/">Suscribirse</a></p>
<a href="http://www.eleconomista.es/monitor/"><img src="http://s03.s3c.es/imag3/v5/menu/monitor.png" width="70" height="70" class="img_left imgshadow" alt="elMonitor"></a><h3 style="border-bottom: none; margin-bottom: 5px; padding-bottom: 0;"><a href="http://www.eleconomista.es/monitor/">el<span style="color: #06c;">Monitor</span></a></h3>
<p>Suscríbase a la herramienta para los ahorradores en bolsa y acceda gratis. <a href="http://www.eleconomista.es/monitor/">Suscribirse</a></p>
</div>
</div>
</li>
</ul></div>
<div id="sub_menu" class="submenu-desp"></div>
<div id="banners-superiores"><div class="b928">
<script language="javascript">//<![CDATA[
		sz = "728x90" ;
		st = "eleconomista.es" ;
		std = "http://ad.es.doubleclick.net/" ;
		kw = "";
		sect="6181497";
		document.write( '<SCR'+'IPT language="JavaScript" SRC="'+std+'adj/'+st+'/economista_noticias_indicadores_espana;sect='+sect+';tile=3;kw='+kw+';sz='+sz+';ord='+randnum+'?"></SCR'+'IPT>' ) ;
	//]]></script><noscript><a href="http://ad.doubleclick.net/jump/eleconomista.es/economista_noticias_indicadores_espana;sect=6181497;tile=3;kw=;sz=728x90;ord=123456789?" target="_blank" rel="nofollow"><img src="http://ad.doubleclick.net/ad/eleconomista.es/economista_noticias_indicadores_espana;sect=6181497;tile=3;kw=;sz=728x90;ord=123456789?" width="728" height="90" border="0"></a></noscript>
</div></div>
<div id="mis_favoritos_reducido" class="cot-home cot-indices"></div>
<img style="display:none" src="http://s01.s3c.es/imag3/quotes/balloon-0.png"><img style="display:none" src="http://s01.s3c.es/imag3/quotes/balloon-1.png"><img style="display:none" src="http://s01.s3c.es/imag3/quotes/balloon-2.png"><img style="display:none" src="http://s01.s3c.es/imag3/quotes/balloon-3.png"><img style="display:none" src="http://s01.s3c.es/imag3/quotes/balloon.png"><img style="display:none" src="http://s01.s3c.es/imag3/si.png"><img style="display:none" src="http://s01.s3c.es/imag3/no.png"><img style="display:none" src="http://s01.s3c.es/imag3/twitter/button.png"><script type="text/javascript">
 
 //<![CDATA[
 	
 	var mivariable='login-menu';
 	var configuracioninicial=1;
	var cookieUsuario = getCookie('USU') ;
	var cookiePassword = getCookie('PASS') ;
	var delay = 10000;
	var secs = 0;
 	
	if ( (cookieUsuario != null) && (cookieUsuario != '') && (cookiePassword != null) && (cookiePassword != '') ) {
		Carga_Pagina(); /* incluido en cajaLogin.js*/
	}
 
  /* incluido en cajaLogin.js*/
 	 	    
	   	Event.observe($('signin_submit'),'click', Captura_mouseClick_desconectar);
	   	Event.observe($('login-superior'),'click', Captura_mouseClick);
	   	Event.observe($('login-enviar'),'click', Captura_mouseClick_enviar);
	   	Event.observe($('login-superior'),'mouseup', Captura_mouseUp);
		Event.observe($('login-menu'),'mouseup', Captura_mouseUp);
		Event.observe($('login-menu2'),'mouseup', Captura_mouseUp);
        Event.observe(window, 'mouseup', Captura_mouseUpGeneral);
   //]]>
   
   
</script>
</div>
<div id="webslice_ultima_hora" class="hslice entry-content">
<a style="display: none;" rel="entry-content" href="/slices/web-slice.php?modulo=392"></a><span class="entry-title" style="display: none;">Última hora en elEconomista.es</span><span class="ttl" style="display: none;">15</span><div mod="392"></div>
</div>
<div id="webslice_ultima_hora" class="hslice entry-content">
<a style="display: none;" rel="entry-content" href="/slices/web-slice.php?modulo=392"></a><span class="entry-title" style="display: none;">Última hora en elEconomista.es</span><span class="ttl" style="display: none;">15</span><div mod="3562">
<script type="text/javascript" src="http://s01.s3c.es/js/compressed/ticker.js"></script><div class="urgente aviso urgente3">
<small>En EcoDiario.es</small><h5 id="output_3562"><ul id="list_ul_text_3562">
<li><a href="http://ecodiario.eleconomista.es/sociedad/noticias/6182506/10/14/Teresa-Romero-tiene-un-bajon-emocional-por-su-aislamiento-y-la-muerte-de-Excalibur.html">Teresa Romero pide justicia por el sacrificio de su perro: "Me siento atropellada"</a></li>
<li><a href="http://ecodiario.eleconomista.es/futbol/noticias/6183330/10/14/Alex-Fbregas-Jugue-con-Espana-sin-sentirme-espanol-pero-me-deje-la-piel.html">"Jugué con España sin sentirme español"</a></li>
<li><a href="http://ecodiario.eleconomista.es/futbol/noticias/6181392/10/14/Los-dardos-de-Cristiano-Ronaldo-y-Ancelotti-al-Bara-Messi-y-Luis-Enrique.html">Los dardos de <b>Cristiano Ronaldo</b> y Ancelotti al Barça, Messi y Luis Enrique</a></li>
<li><a href="http://ecodiario.eleconomista.es/espana/noticias/6181988/10/14/La-exnovia-de-Jordi-Pujol-Ferrusol-Hoy-es-un-gran-dia-para-todos-los-espanoles.html">La exnovia de <B>Jordi Pujol junior</B>: "Hoy es un gran día para todos los españoles"</a></li>
<li><a href="http://ecodiario.eleconomista.es/politica/noticias/6182599/10/14/Rajoy-llamo-a-Acebes-y-le-traslado-que-es-una-injusticia-su-imputacion-por-los-papeles-de-Barcenas.html"><b>Rajoy</b> pudo llamar a Acebes para decirle que "es una injusticia" su imputación</a></li>
</ul></h5>
<div class="botones-uh"><p class="buh"><a href="#" onclick="ticker_3562.previousCurrentNews(); return( false );">L</a><a href="#" onclick="ticker_3562.play_pause_Ticker('tecla_pause_play_3562'); return( false );"><span id="tecla_pause_play_3562">0</span></a><a href="#" onclick="ticker_3562.nextCurrentNews(); return( false );">I</a></p></div>
</div>
<script type="text/javascript">//<![CDATA[
					ticker_3562 = new news_Ticker( 'output_3562', 'list_ul_text_3562',{typeSpeed:1,typing:false,tickSpeed:7500} ) ;
				//]]></script>
</div>
</div>
<div mod="1647" class="b930t">
<script language="javascript">//<![CDATA[
			sz = "960x30" ;
			st = "eleconomista.es" ;
			std = "http://ad.es.doubleclick.net/" ;
			kw = "";
			 sect ="6181497";
			document.write( '<SCR'+'IPT language="JavaScript" SRC="'+std+'adj/'+st+'/economista_noticias_indicadores_espana;sect='+sect+';tile=3;kw='+kw+';sz='+sz+';ord='+randnum+'?"></SCR'+'IPT>' ) ;
		//]]></script><noscript><a href="http://ad.doubleclick.net/jump/eleconomista.es/economista_noticias_indicadores_espana;sect=6181497;tile=3;kw=;sz=960x30;ord=123456789?" target="_blank" rel="nofollow"><img src="http://ad.doubleclick.net/ad/eleconomista.es/economista_noticias_indicadores_espana;sect=6181497;tile=3;kw=;sz=960x30;ord=123456789?" width="960" height="30" border="0"></a></noscript>
</div>
<div id="banners-superiores_2">
<script type="text/javascript" language="javascript">//<![CDATA[
		var time = new Date() ;
		randnum= (time.getTime()) ;
	//]]></script><script language="javascript">//<![CDATA[
		sz = "1x2" ;
		st = "eleconomista.es" ;
		std = "http://ad.es.doubleclick.net/" ;
		isnt = "";
		
		document.write( '<SCR'+'IPT language="JavaScript" SRC="'+std+'adj/'+st+'/economista_noticias_indicadores_espana;tile=11;dcopt=ist;sz='+sz+';isnt='+isnt+';ord='+randnum+'?"></SCR'+'IPT>' ) ;
	//]]></script><noscript><a href="http://ad.doubleclick.net/jump/eleconomista.es/economista_noticias_indicadores_espana;tile=11;sz=1x2;ord=123456789?" target="_blank" rel="nofollow"><img src="http://ad.doubleclick.net/ad/eleconomista.es/economista_noticias_indicadores_espana;sz=1x2;ord=123456789?" width="1" height="1" border="0"></a></noscript>
</div>

		<div class="cols-i noticia-des"><div mod="7">
<script type="text/javascript">
	//<![CDATA[
		miLista = new miListaValores( 'a' ) ;
	//]]>
	</script><script type="text/javascript">//<![CDATA[
	function abrirDelicious()
	{
		window.open('http://del.icio.us/post?v=4&noui&jump=close&url='+encodeURIComponent(location.href) + '&title=' + encodeURIComponent(document.title),'delicious','toolbar=0,width=700,height=400');
		return false;
	}
	
	function abrirTuenti()
	{
		window.open('http://www.tuenti.com/share?url='+encodeURIComponent(location.href));
		return false;
	}
	
	function abrirDigg(titulo, texto)
	{
	
		window.open('http://www.digg.com/submit?phase=2&url=' + encodeURIComponent(location.href) + '&title=' + titulo + '&bodytext=' + texto + '&topic=business_finance', 'digg','toolbar=no, scrollbars=yes,resizable=yes');
		return false;
		
	}
	
	function abrirMeneame()
	{
		window.open('http://meneame.net/submit.php?url=%20'+location.href ,'meneame','toolbar=yes, scrollbars=yes,resizable=yes');
		return false;
	}

	function abrirTwitter()
	{
		window.open('http://twitter.com/home?status='+location.href ,'Twitter','toolbar=yes, scrollbars=yes,resizable=yes');
		return false;
	}

	function abrirFacebook()
	{
		window.open('http://www.facebook.com/share.php?u='+location.href ,'Facebook','toolbar=yes, scrollbars=yes,resizable=yes');
		return false;
	}
	
	var tam_actual = 2;
	function actualizarTamano (signo)
	{
		tam = tam_actual;
		if ( ((signo==1) && (tam_actual < 4)) || ((signo==-1) && (tam_actual > 1))){
			tam_actual = tam_Siguiente = tam + signo;

			$$('div#cuerpo_noticia > p').each(
					function(s) {
						if (s.hasClassName('tam'+tam)) 
							s.removeClassName('tam'+tam);
						s.addClassName('tam'+tam_Siguiente)
					});

			$$('div#cuerpo_noticia > h2').each(
					function(s) {
						if (s.hasClassName('tam'+tam)) 
							s.removeClassName('tam'+tam);
						s.addClassName('tam'+tam_Siguiente)
					});
		}
	}
	
	
			enviarNoticiaLeida_Site( 6181497, 1 ) ;
		//]]></script><div id="cuerpo_noticia" class="noticiacuerpo">
<h1>El paro se redujo en 195.200 personas en el tercer trimestre y la tasa cae al 23,67%</h1>
<div class="s-horizontal">
<div class="s-h-c">
<script src="http://platform.twitter.com/widgets.js" type="text/javascript"></script><a href="http://twitter.com/share" class="twitter-share-button" data-via="eleconomistaes" data-url="http://www.eleconomista.es/indicadores-espana/noticias/6181497/10/14/El-paro-baja-en-195200-personas-en-el-tercer-trimestre-la-tasa-de-desempleo-desciende-hasta-el-2367.html" data-text="El paro se redujo en 195.200 personas en el tercer trimestre y la tasa cae al 23,67%" data-lang="en" data-count="horizontal">Tweet</a>
</div>
<div class="s-h-c2">
<div id="fb-root"></div>
<div class="fb-share-button" data-width="150" data-type="button_count" tyle="position: relative; display: inline;"></div>
</div>
<div class="s-h-c">
<div class="g-plusone" data-size="medium"></div>
<script type="text/javascript">
		  window.___gcfg = {lang: 'es'};

		  (function() {
		    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
		    po.src = 'https://apis.google.com/js/plusone.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
		  })();
		</script>
</div>
<div class="s-h-c">
<script src="//platform.linkedin.com/in.js" type="text/javascript"></script><script type="IN/Share" data-counter="right"></script>
</div>
<div class="wfavicon" sharecount="right" data-tooltip="false"></div>
<script type="text/javascript">(function(d, t, i, l) { var g = d.createElement(t), s = d.getElementsByTagName(t)[0]; g.src = "http://static.womenalia.net/media/wfaviconstyles/wfavicon.min.js"; g.id=i; g.lang=l; s.parentNode.insertBefore(g, s); }(document, "script", "wow-wfavicon", "es-ES"));</script>
</div>
<div class="firma">
<div class="f-autor">           Europa Press</div>
<div class="f-fecha">23/10/2014 - 9:03</div>
</div>
<div class="firma" style="border-bottom: none;">
<div class="com-not"><small class="num-coment"><a href="#Comentarios">99	comentarios</a></small></div>
<div class="slider">
<div id="gr_not_puntos_vote_6181497">
<div style="font-size: 11px; float: left; overflow: hidden; margin-left: 55px; _margin-left: 15px;">
<span id="texto_6181497">Puntúa la noticia</span> : </div>
<img src="http://s01.s3c.es/imagenes/menos-rojo.png" style="margin-top: 2px; margin-left: 15px; float: left;"><div id="track_6181497" class="track1" style="width:70px; height:13px; *height: 18px; float: left; margin-top: 5px;">
<div class="track-left1"></div>
<div id="handle_6181497" style="width:19px; height:20px;background: none;"><img src="http://s01.s3c.es/imagenes/slider/slider-images-handle1.png"></div>
</div>
<img src="http://s01.s3c.es/imagenes/mas-verde.png" style="margin-top: 2px; float: left;">
</div>
<div id="gr_not_puntos_result_6181497"><div style="font-size: 11px; float: left; margin: -5px 0 5px 5px; overflow: hidden; *margin-top:0;">Nota de los usuarios: 
			<span id="not_puntos_6181497" style="font-size: 14px; font-weight: bold;">3,6</span>
			(<span id="not_puntos_votes_6181497">71</span>votos)
		</div></div>
<script>
				not_6181497 = new ValoracionSlider_Noticia(6181497,259,71);
			</script>
</div>
</div>
<div class="not-res"><ul>
<li>El número total de parados alcanza la cifra de 5.427.700 personas</li>
<li>La tasa de paro bajó ocho décimas hasta situarse en el actual 23,67%</li>
<li>Entre los meses de julio y septiembre se crearon 151.000 empleos</li>
</ul></div>
<div class="p-tag" style="background:none;  border-top: 1px solid #999; width:auto; margin:0;" id="lista_tags">
<img id="abrir_tags" src="http://s01.s3c.es/imag3/ft-a.png" width="16" alt="tags" style="float:right; margin:15px 2px 0 2px; position: relative; right: 0;" onClick="$('lista_tags').style.height='auto';$('abrir_tags').style.display='none';$('cerrar_tags').style.display='block'"><span style="background:none; width:auto; font-weight:bold; padding:16px 0 0 3px;">Más noticias sobre:</span><ul>
<li><a href="/tags/espana">España</a></li>
<li><a href="/tags/ine">Ine</a></li>
<li><a href="/tags/EPA">EPA</a></li>
<li><a href="/tags/Tasa-paro">Tasa paro</a></li>
</ul>
<img id="cerrar_tags" src="http://s01.s3c.es/imag3/ft-c.png" width="16" alt="tags" style="float:right; margin:15px 2px 0 2px; display:none;" onClick="$('lista_tags').style.height='35px';$('abrir_tags').style.display='block';$('cerrar_tags').style.display='none'">
</div>
<div id="video_7" style="width: 640px; height: 360px;"></div>
<script language="javascript" type="text/javascript">
	//<![CDATA[
		

	var playlist_7 = new Object();
		
	
	playlist_7[0] = [{ 'file': 'http://s02.s3c.es/imag/video/1048x576/6/e/6/640x450_record-caida-paro.jpg'}, {'file': 'http://s04.s3c.es/fVideo/video/c/9/4/record-caida-paro.flv', 'duration': '00:00:36', 'idVideo': '6181497' }];
	
	var adv_7 = new Object();
	adv_7.enabled = true;
	adv_7.preroll = escape("http://ad.doubleclick.net/pfadx/eleconomista.es/;sz=200x95;ord=?");

	adv_7.postroll = escape("http://ad.doubleclick.net/pfadx/eleconomista.es/;sz=200x95;ord=[random]?");

	var style_7 = new Object();
	style_7.backgroundColor = "#996600";
	style_7.mainColor = "#FF6633";
	style_7.secondColor = "#FF6633";
	
	var autoplay_7 = false;
	var autoHide_7 = true;
	
	var pl = "total=1";
	for(var a in playlist_7){
		pl+="&th_" + a + "=" + playlist_7[a][0].file;
		for(var e in playlist_7[a][1]){
			pl+= "&" + e + "_" + a + "=" + playlist_7[a][1][e];
		}
	}
	var adData = adv_7;
	var ads="";
	for(var a in adData){
		ads+= "&" + a + "=" + adData[a];
	}

	var stData = style_7;
	var stl="";
	for(var a in adData){
		stl+= "&" + a + "=" + adData[a];
			
	}
	
	var flashvars_7 = {
		idPlayer : "video_7",
		prefix : "_7",
		autoplay:autoplay_7,
		autoHide:autoHide_7,
		playlist : escape(pl),
		
				advertisement : escape(ads),
			
		style : escape(stl)
	};

	var params_7 = {
		allowscriptaccess:"always",
		allowfullscreen:true,
		wmode:"transparent"
	};
	var attributes_7 = {id:'video_7'};

	if (navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i)){


		$('video_7').update('<video src="http://s04.s3c.es/fVideo/video/c/9/4/record-caida-paro.mp4" poster="http://s04.s3c.es/imag/video/1048x576/6/e/6/record-caida-paro.jpg" width="'+$('video_7').getWidth()+'" height="'+$('video_7').getHeight()+'" controls="controls"></video>');

	}
	else {
		swfobject.embedSWF( 'http://s04.s3c.es/vVideos/smartclip.v4.swf', 'video_7', 
			$('video_7').getWidth(), $('video_7').getHeight(), "9.0.0", "expressInstall.swf",
			flashvars_7, params_7, attributes_7);
	}


	//]]>
	</script><div class="i-not">
<div class="relacionados">
<h3>Enlaces relacionados</h3>
<h2><a href="http://www.eleconomista.es/indicadores-espana/noticias/5964185/07/14/El-paro-registra-una-caida-de-310400-personas-y-se-crean-402400-empleos.html"><img src='/imagenes/iconos/ico-general.gif' /> Así fue la EPA del segundo trimestre</a></h2>
</div>
<div class="caja mods" style="border-top: none; margin:0; border-bottom:none;"><a href="/kiosco/"><img src="http://s04.s3c.es/imag3/botones/kiosco225.png" alt="kiosco"></a></div>
<div class="controles"><ul>
<li class="noti-send">
<img src="http://s03.s3c.es//imag3/iconos/newsletter30x32.png"><a href="/noticias-email/6181497/El-paro-baja-en-195200-personas-en-el-tercer-trimestre-la-tasa-de-desempleo-desciende-hasta-el-2367" id="enviar" rel="nofollow">Enviar por e-mail</a>
</li>
<li><a href="javascript:window.print();" id="imprimir">Imprimir</a></li>
<li><a href="#" id="aumentar" onclick="return actualizarTamano(1);">Aumentar texto</a></li>
<li><a href="#" id="reducir" onclick="return actualizarTamano(-1);">Reducir texto</a></li>
<li>
<img style="float:left" src="http://s01.s3c.es/imag3/iconos/dixio.gif" alt="Dixio">Haga doble click sobre una palabra para ver su signifcado</li>
<li class="foro-rel"><a href="/servicios/sudoku/"><img style="float:left" src="http://s01.s3c.es/imag3/botones/sudoku.gif" alt="sudoku"><b>Sudoku:</b> Juega cada día a uno nuevo</a></li>
<li class="foro-rel"><a href="http://ecodiario.eleconomista.es/el-tiempo/"><img style="float:left" src="http://s01.s3c.es/imag3/botones/tiempo.gif" alt="Tiempo"><b>El tiempo:</b> Consulta la previsión para tu ciudad</a></li>
</ul></div>
</div>
<p><p><a href="http://www.ine.es/daco/daco42/daco4211/epa0314.pdf">El paro bajó en 195.200 personas</a> en el tercer trimestre del año respecto al trimestre anterior, un 3,5% menos, registrando su mayor descenso en un tercer trimestre dentro la serie histórica, según datos de la Encuesta de Población Activa (EPA) publicados este jueves por el Instituto Nacional de Estadística (INE). <a href="http://www.eleconomista.es/economia/noticias/6182335/10/14/Otra-perspectiva-de-la-EPA-30-curiosidades-mas-alla-del-dato-del-paro.html">La otra cara de la EPA: un retrato de España a través de 30 datos curiosos.</a></p></p><p>Tras este recorte del desempleo en el periodo julio-septiembre, el número total de parados alcanzó la cifra de 5.427.700 personas, su nivel más bajo desde el cuarto trimestre de 2011.</p>
<p>Así, la tasa de paro bajó ocho décimas en el tercer trimestre, hasta situarse en el 23,67%, experimentando esta tasa su mayor descenso en un tercer trimestre desde 2005. En el último año, el paro se ha reducido un 8,7%, con 515.700 desempleados menos.</p><p align="left"> Entre julio y septiembre se crearon 151.000 empleos (+0,87%), el mayor incremento de la ocupación desde el tercer trimestre de 2007. En el último año, la ocupación ha aumentado en 274.000 personas, a un ritmo del 1,59%. Al finalizar septiembre, el número total de ocupados alcanzaba los 17.504.000, nivel que no se registraba desde el tercer trimestre de 2012.</p>
<p><img src="http://s01.s3c.es/imag/_v0/630x425/c/6/7/Ocupados-Spain.jpg" /></p>
<p>Todos los empleos creados entre julio y septiembre pertenecían al sector privado, que registró un avance de la ocupación de 154.900 personas (+1,07%), mientras que el empleo público se redujo en este trimestre en 3.900 personas (-0,13%). El sector público acumula un ajuste de 17.700 puestos de trabajo en el último año (-0,6%), mientras que el privado ha creado 291.600 empleos (+2%).</p>
<p>En el tercer trimestre de 2014, el número de asalariados se incrementó en 95.700 personas (+0,7%), tras reducirse los contratados con carácter indefinido en 26.700 personas (-0,25%) y aumentar en 122.400 los asalariados con contrato temporal (+3,6%).</p>  <h2>Los hogares con todos sus miembros en paro bajan un 2,4%</h2>   <p>Los hogares con todos sus miembros en paro bajaron en el tercer trimestre del año en 44.600, lo que supone <a href="http://www.eleconomista.es/indicadores-espana/noticias/6181651/10/14/Los-hogares-con-todos-sus-miembros-en-paro-bajan-un-24-en-el-tercer-trimestre-hasta-los-178-millones.html">un 2,4% menos que en el trimestre anterior</a>, hasta situarse en 1.789.400, según datos de la EPA. En el último año los hogares con todos sus miembros en paro se han reducido en 104.200, un 5,5% menos. </p>
<p>Por su parte, los hogares con todos sus integrantes ocupados aumentaron en 165.400 entre julio y septiembre de este año, un 1,8% respecto al trimestre anterior, hasta un total de 9.073.100 hogares. En el último año, las familias con todos sus miembros ocupados han subido en 353.700 (+4%). </p>
<p>Los hogares con al menos un activo descendieron en 9.700 (-0,07%) en el tercer trimestre y suman 13.365.100 hogares, cifra superior en 27.600 a la del tercer trimestre de 2013 (+0,2%), mientras los hogares en los que no hay ningún activo aumentaron en 34.000 en el tercer trimestre, hasta superar los 4,99 millones. En comparación con el tercer trimestre de 2013, los hogares en los que no hay ningún activo crecieron en 107.700 (+2,2%).</p>  <h2>La tasa de paro juvenil baja al 52,4% pese a registrar más desempleados</h2>   <p>El número de jóvenes en paro menores de 25 años subió en 27.000 personas en el tercer trimestre, lo que supone un 3,2% más que en el trimestre anterior, situándose la cifra total de jóvenes en situación de desempleo en 867.600 al finalizar el pasado mes de septiembre, de acuerdo con los datos de la EPA de este tercer trimestre. </p>
<p>No obstante, <a href="http://www.eleconomista.es/indicadores-espana/noticias/6181601/10/14/La-tasa-de-paro-juvenil-baja-al-524-en-el-tercer-trimestre-pese-a-registrar-27000-desempleados-mas.html">la tasa de paro de este colectivo bajó hasta el 52,38%</a> al término del tercer trimestre, lo que supone siete décimas menos que en el trimestre anterior, cuando el desempleo de los jóvenes menores de 25 años se situó en el 53,12%. </p>
<p>El hecho de que el paro entre los jóvenes aumente en este trimestre pero descienda al mismo tiempo su tasa de desempleo se explica por la evolución de los activos. En concreto, el número de jóvenes activos se incrementó en 73.700 personas en el tercer trimestre (+4,6%), hasta situarse el total de activos en 1.656.100 jóvenes. </p>
<p>En el último año, el número de jóvenes en paro ha descendido en 94.000 desempleados (-9,7%), mientras que el de activos se ha reducido en 120.000 (-6,7%). La tasa de paro actual de los menores de 25 años es casi 1,8 puntos inferior a la existente en el tercer trimestre del año pasado (54,14%). </p>  <h2>La mitad de los parados lleva más de un año desempleado </h2> <p>De los más de 5,4 millones de parados contabilizados en España al finalizar septiembre, el 16% son jóvenes menores de 25 años y el 50,6% son parados de larga duración (más de un año en el desempleo). </p>
<p>El número de parados de larga duración descendió en 122.300 personas en el tercer trimestre, un 4,2% menos que en el trimestre anterior, hasta situarse en 2.746.600. En el último año, los parados de larga duración se han reducido en 165.600 personas (-5,7%).</p>
<p><iframe frameborder="0" src="http://www.porcentual.es/backoffice/widget.html#/charts/0yiSXXJqgvbcnxC901XNQQ/widget" style="width: 630px; height: 400px;">&amp;amp;amp;amp;lt;/p&amp;amp;amp;amp;gt;         	&amp;amp;amp;amp;lt;/body&amp;amp;amp;amp;gt;&amp;amp;amp;lt;/p&amp;amp;amp;gt;&amp;amp;amp;lt;p&amp;amp;amp;gt;&amp;amp;amp;amp;lt;/html&amp;amp;amp;amp;gt;</iframe></p><div class="canales"><ul>
<li><a href="/noticias/flash" id="categoria_12" onclick="return false;">Flash</a></li>
<script type="text/javascript">create_Balloon(12,1, 'categoria_12');</script><li><a href="/noticias/economia" id="categoria_14" onclick="return false;">Economía</a></li>
<script type="text/javascript">create_Balloon(14,1, 'categoria_14');</script><li><a href="/noticias/seleccion-ee" id="categoria_35" onclick="return false;">Seleccion eE</a></li>
<script type="text/javascript">create_Balloon(35,1, 'categoria_35');</script><li><a href="/noticias/indicadores-espana" id="categoria_46" onclick="return false;">Indicadores España</a></li>
<script type="text/javascript">create_Balloon(46,1, 'categoria_46');</script>
</ul></div>
<div class="sep" style="height: 10px;"></div>
<div style="margin: 5px 0pt 10px;" class="sep punteado"></div>
<p style="margin:0; font-size:8px;margin-top:0;">PUBLICIDAD</p>
<iframe width="634" height="28" src="http://www.idealista.com/news/widget/get/57579/2"></iframe><div style="background:#eee;">
<img style="float:left; padding:2px 5px 2px 2px" src="http://s03.s3c.es/imag3/banners/selfbank-peq.gif"><p style="font-size:11px; padding:5px">- <a target="_blank" rel="nofollow" href="http://pubads.g.doubleclick.net/gampad/clk?id=204740422&amp;iu=/4275/eleconomista.es">Opera con CFDs y Divisas apalancando tu inversión hasta 100 veces.</a></p>
</div>
<div style="background:#eee;">
<img style="float:left;padding: 4px 5px 2px 2px;" src="http://s03.s3c.es/imag3/banners/xtb-peq.png"><p style="font-size:11px; padding:5px">- <a target="_blank" rel="nofollow" href="http://track.adform.net/C/?bn=4415702">¿Eres buen inversor? Demuestra lo que vales y gana un PORSCHE </a></p>
</div>
<div class="pub-ads">
<div id="cX-root" style="display:none"></div>
<div id="panComboAds_630x240" style="display:none"></div>
<script type="text/javascript">

			var cX = cX || {}; 
			cX.callQueue = cX.callQueue || [];

			cX.callQueue.push(['insertAdSpace', { adSpaceId: '00000000013a878d',
												insertBeforeElementId: 'panComboAds_630x240',
												adUnitWidth: 630, 
												adUnitHeight: 240,
												k: 'panbackfill',
												initialHorizontalAdUnits: 1, 
												initialVerticalAdUnits: 1,
												renderTemplateUrl: 'http://cdn.cxpublic.com/PAN_Combo_630x240_ElEconomista.html'

								} ]);
		  cX.callQueue.push(['setAccountId', '9222287489163341896']);
		  cX.callQueue.push(['setSiteId', '9222287489163341897']);
		  cX.callQueue.push(['sendPageViewEvent']);

		</script><script type="text/javascript">
			(function() { try { var scriptEl = document.createElement('script');
								scriptEl.type = 'text/javascript'; 
								scriptEl.async = 'async';
								scriptEl.src = ('https:' == document.location.protocol) ?
										'https://scdn.cxense.com/cx.js' : 'http://cdn.cxense.com/cx.js';
								var targetEl = document.getElementsByTagName('script')[0];
								targetEl.parentNode.insertBefore(scriptEl, targetEl); 
							} 
						catch (e) {};} ());

	</script>
</div>
<div class="fb-like" data-send="true" data-width="450" data-show-faces="false"></div>
<a href="https://twitter.com/share" class="twitter-share-button" data-lang="es">Twittear</a><div style="margin: 10px 0pt 10px;"></div>
<div id="recsTargetEl" style="display:none"></div>
<script type="text/javascript">
        var cX = cX || {}; cX.callQueue = cX.callQueue || [];
        cX.callQueue.push(['insertWidget',{
            width: 630, height: 290,
            resizeToContentSize: true,
            insertBeforeElementId: 'recsTargetEl',
            renderTemplateUrl: 'http://cdn.cxpublic.com/ee-hor.html'
        }]);
    </script><script type="text/javascript">
        (function() { try { var scriptEl = document.createElement('script'); scriptEl.type = 'text/javascript'; scriptEl.async = 'async';
        scriptEl.src = ('https:' == document.location.protocol) ? 'https://scdn.cxense.com/cx.js' : 'http://cdn.cxense.com/cx.js';
        var targetEl = document.getElementsByTagName('script')[0]; targetEl.parentNode.insertBefore(scriptEl, targetEl); } catch (e) {};} ());
    </script><div class="sep" style="height: 10px;"></div>
</div>
</div>
<div mod="1649">
<a id="Comentarios"></a><div id="comm">
<h2>Comentarios 99</h2>
<a name="comment-2754671"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">1</div>23-10-2014 / 09:10<br><div class="nombre-usuario">Juan</div>
<div class="puntos">
<script language="javascript">
			mkarma_2754671 = new comment_karma('2754671', '-15');
		</script>
		Puntuación <span id="karma_2754671">-15</span><a id="karma_accion_subir_2754671" onclick="return false;"></a><a id="karma_accion_bajar_2754671" onclick="return false;"></a><script language="javascript">mkarma_2754671.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>Muy buena noticia.</p></div>
</div>
<a name="comment-2754681"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">2</div>23-10-2014 / 09:15<br><div class="nombre-usuario">k</div>
<div class="puntos">
<script language="javascript">
			mkarma_2754681 = new comment_karma('2754681', '19');
		</script>
		Puntuación <span id="karma_2754681">19</span><a id="karma_accion_subir_2754681" onclick="return false;"></a><a id="karma_accion_bajar_2754681" onclick="return false;"></a><script language="javascript">mkarma_2754681.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>Peperos ¿Dónde están los tres millones y medio de empleos que ibais a crear?.... En el cajón de los olvidos y las mentiras criminales con las que deshonestamente ganasteis las elecciones.<br /><br/>Si algo habéis demostrado peperos de la mierda es que no servís ni para lo que estáis, ni para nada.<br /><br/>Solo servís para mangonear, engañar y tomarnos el pelo incluso a los que nunca nos fiaríamos de gentuza ladrona y mentirosa como vosotros, so ppuercos.</p></div>
</div>
<a name="comment-2754686"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">3</div>23-10-2014 / 09:17<br><div class="nombre-usuario">elena</div>
<div class="puntos">
<script language="javascript">
			mkarma_2754686 = new comment_karma('2754686', '-59');
		</script>
		Puntuación <span id="karma_2754686">-59</span><a id="karma_accion_subir_2754686" onclick="return false;"></a><a id="karma_accion_bajar_2754686" onclick="return false;"></a><script language="javascript">mkarma_2754686.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>POR QUÉ EL MADRID DESCANSA SOLO DOS DIAS Y MEDIO Y EL BARCA DESCANSA CUATRO??????<br /><br/>MANIPULACIÓN DE LA LIGA??????</p></div>
</div>
<a name="comment-2754688"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">4</div>23-10-2014 / 09:19<br><div class="nombre-usuario">jbrochado</div>
<div class="puntos">
<script language="javascript">
			mkarma_2754688 = new comment_karma('2754688', '41');
		</script>
		Puntuación <span id="karma_2754688">41</span><a id="karma_accion_subir_2754688" onclick="return false;"></a><a id="karma_accion_bajar_2754688" onclick="return false;"></a><script language="javascript">mkarma_2754688.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>Si es cierta, pues todo lo que diga este gobierno hay que ponerlo en cuarentena...</p></div>
</div>
<a name="comment-2754693"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">5</div>23-10-2014 / 09:21<br><div class="nombre-usuario">carlos</div>
<div class="puntos">
<script language="javascript">
			mkarma_2754693 = new comment_karma('2754693', '34');
		</script>
		Puntuación <span id="karma_2754693">34</span><a id="karma_accion_subir_2754693" onclick="return false;"></a><a id="karma_accion_bajar_2754693" onclick="return false;"></a><script language="javascript">mkarma_2754693.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>EL SISTEMA ES INSOSTENIBLE- LA evolución nos lleva a menos personal sub-clases véanse película in-time</p></div>
</div>
<a name="comment-2754695"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">6</div>23-10-2014 / 09:22<br><div class="nombre-usuario">Blanco</div>
<div class="puntos">
<script language="javascript">
			mkarma_2754695 = new comment_karma('2754695', '61');
		</script>
		Puntuación <span id="karma_2754695">61</span><a id="karma_accion_subir_2754695" onclick="return false;"></a><a id="karma_accion_bajar_2754695" onclick="return false;"></a><script language="javascript">mkarma_2754695.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>El paro baja en aproximadamente 200.000 personas pero solo se crean 150.000 puestos de trabajo.<br /><br/>Y apuesto que de esos 150.000, 100.000 son contratos de horas.<br /><br/>Mentiras tras mentiras</p></div>
</div>
<a name="comment-2754699"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">7</div>23-10-2014 / 09:23<br><div class="nombre-usuario">Buena noticia se mire como se mire</div>
<div class="puntos">
<script language="javascript">
			mkarma_2754699 = new comment_karma('2754699', '-16');
		</script>
		Puntuación <span id="karma_2754699">-16</span><a id="karma_accion_subir_2754699" onclick="return false;"></a><a id="karma_accion_bajar_2754699" onclick="return false;"></a><script language="javascript">mkarma_2754699.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>Esta claro que esta es una gran noticia para todos y que parece que la recuperacion esta en marcha,aunque a todos nos gustaria que fuese mas rapida.</p></div>
</div>
<a name="comment-2754700"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">8</div>23-10-2014 / 09:23<br><div class="nombre-usuario">Español</div>
<div class="puntos">
<script language="javascript">
			mkarma_2754700 = new comment_karma('2754700', '-65');
		</script>
		Puntuación <span id="karma_2754700">-65</span><a id="karma_accion_subir_2754700" onclick="return false;"></a><a id="karma_accion_bajar_2754700" onclick="return false;"></a><script language="javascript">mkarma_2754700.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>Gracias Se. Rajoy eres nuestro salvador, sigue así esto esta funcionando, estábamos en la ruina y nos has salvado de morir.</p></div>
</div>
<a name="comment-2754705"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">9</div>23-10-2014 / 09:25<br><div class="nombre-usuario">elder</div>
<div class="puntos">
<script language="javascript">
			mkarma_2754705 = new comment_karma('2754705', '38');
		</script>
		Puntuación <span id="karma_2754705">38</span><a id="karma_accion_subir_2754705" onclick="return false;"></a><a id="karma_accion_bajar_2754705" onclick="return false;"></a><script language="javascript">mkarma_2754705.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>como nos llevan a la confusion...A numeros grandes ; Cuanta gente trabaja en numero : 14.000.000 ? y cuantos estan buscando trabajo 5.000.000 ?<br /><br/>Cuantos somos en España? 40.000.000 de personas... pues miren mas de la mitad no cotizan, y cada trabajador con un sueldo bajo tiene que cotizar para su futura jubilacion y para mantener a uno que no trabaja</p></div>
</div>
<a name="comment-2754706"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">10</div>23-10-2014 / 09:26<br><div class="nombre-usuario">Saliendo de esta</div>
<div class="puntos">
<script language="javascript">
			mkarma_2754706 = new comment_karma('2754706', '-35');
		</script>
		Puntuación <span id="karma_2754706">-35</span><a id="karma_accion_subir_2754706" onclick="return false;"></a><a id="karma_accion_bajar_2754706" onclick="return false;"></a><script language="javascript">mkarma_2754706.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>La inercia de caida que llevavamos se ha frenado y se ha invertido la tendencia.Ahora a reducir el endeudamiento vestial que tenemos y a todos nos ira mucho mejor en el futuro.</p></div>
</div>
<a name="comment-2754707"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">11</div>23-10-2014 / 09:26<br><div class="nombre-usuario">adrian</div>
<div class="puntos">
<script language="javascript">
			mkarma_2754707 = new comment_karma('2754707', '-10');
		</script>
		Puntuación <span id="karma_2754707">-10</span><a id="karma_accion_subir_2754707" onclick="return false;"></a><a id="karma_accion_bajar_2754707" onclick="return false;"></a><script language="javascript">mkarma_2754707.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>En el último año, el paro se ha reducido un 8,7%, con 515.700 desempleados menos<br /><br/>Dejaros de enfrentamientos por partidos politicos, lo importante es que medio millon de personas (familias) han dejado de pasarlo mal.</p></div>
</div>
<a name="comment-2754716"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">12</div>23-10-2014 / 09:33<br><div class="nombre-usuario">Antuan</div>
<div class="puntos">
<script language="javascript">
			mkarma_2754716 = new comment_karma('2754716', '58');
		</script>
		Puntuación <span id="karma_2754716">58</span><a id="karma_accion_subir_2754716" onclick="return false;"></a><a id="karma_accion_bajar_2754716" onclick="return false;"></a><script language="javascript">mkarma_2754716.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>Tengamos clara la perspectiva. En este periodo han abandonado españa más de 240.000 personas, obligadas por la necesidad de llevarse algo a la boca.<br /><br/>Esto no es un éxito, es un fracaso rotundo del país.</p></div>
</div>
<a name="comment-2754719"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">13</div>23-10-2014 / 09:34<br><div class="nombre-usuario">Otro parado</div>
<div class="puntos">
<script language="javascript">
			mkarma_2754719 = new comment_karma('2754719', '62');
		</script>
		Puntuación <span id="karma_2754719">62</span><a id="karma_accion_subir_2754719" onclick="return false;"></a><a id="karma_accion_bajar_2754719" onclick="return false;"></a><script language="javascript">mkarma_2754719.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>Todo mentira, no se trata de creación de empleo, sino de comienzo de cursos de formación tras los cuales te dan de baja del paro, pero no se está creando empleo, se estan aumentando los curso ¿así piensan acabar con el paro?</p></div>
</div>
<a name="comment-2754723"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">14</div>23-10-2014 / 09:36<br><div class="nombre-usuario">c</div>
<div class="puntos">
<script language="javascript">
			mkarma_2754723 = new comment_karma('2754723', '43');
		</script>
		Puntuación <span id="karma_2754723">43</span><a id="karma_accion_subir_2754723" onclick="return false;"></a><a id="karma_accion_bajar_2754723" onclick="return false;"></a><script language="javascript">mkarma_2754723.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>Increíble que haya tarados mentales de tal calibre que hablen de Rajoy como de un salvador en este foro, auténticos psicópatas tienen que ser para pensar así, por cierto menos trabajadores que en 2011 lo demás propaganda peperra.....</p></div>
</div>
<a name="comment-2754727"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">15</div>23-10-2014 / 09:37<br><div class="nombre-usuario">La politica por la politica nos destruye</div>
<div class="puntos">
<script language="javascript">
			mkarma_2754727 = new comment_karma('2754727', '-20');
		</script>
		Puntuación <span id="karma_2754727">-20</span><a id="karma_accion_subir_2754727" onclick="return false;"></a><a id="karma_accion_bajar_2754727" onclick="return false;"></a><script language="javascript">mkarma_2754727.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>Me alegro mucho por tanta gente que tiene la oportunidad de volver a trabajar,ojala que siga la buena racha y salgamos de esta cuanto antes.Quedarte sin trabajo es terrible y por contra encontrar uno es una vendicion,mas si tienes hijos que criar.Ojala que los politicos se den cuenta de que juegan con la vida de nuestras familias y hagan su trabajo por este pais y no por ganar otra legislatura;la historia les juzgara por ello.</p></div>
</div>
<a name="comment-2754733"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">16</div>23-10-2014 / 09:39<br><div class="nombre-usuario">Ex-votante</div>
<div class="puntos">
<script language="javascript">
			mkarma_2754733 = new comment_karma('2754733', '11');
		</script>
		Puntuación <span id="karma_2754733">11</span><a id="karma_accion_subir_2754733" onclick="return false;"></a><a id="karma_accion_bajar_2754733" onclick="return false;"></a><script language="javascript">mkarma_2754733.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>Y eso que no paran de subir las cotizaciones sociales. Pensemos cómo podría de haber bajado el paro si el PP de Montoro y de Báñez no hubiese expoliado a las empresas y trabajadores a manos llenas.</p></div>
</div>
<a name="comment-2754738"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">17</div>23-10-2014 / 09:41<br><div class="nombre-usuario">PArado que se alegra por el bien ajeno</div>
<div class="puntos">
<script language="javascript">
			mkarma_2754738 = new comment_karma('2754738', '-36');
		</script>
		Puntuación <span id="karma_2754738">-36</span><a id="karma_accion_subir_2754738" onclick="return false;"></a><a id="karma_accion_bajar_2754738" onclick="return false;"></a><script language="javascript">mkarma_2754738.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>A los que parece enfurecerlos que tanta gente encuentre trabajo creo que estan envenenados por la politica y lejos del sufrimiento de las familias que lo pasamos mal.</p></div>
</div>
<a name="comment-2754740"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">18</div>23-10-2014 / 09:41<br><div class="nombre-usuario">Pablo</div>
<div class="puntos">
<script language="javascript">
			mkarma_2754740 = new comment_karma('2754740', '18');
		</script>
		Puntuación <span id="karma_2754740">18</span><a id="karma_accion_subir_2754740" onclick="return false;"></a><a id="karma_accion_bajar_2754740" onclick="return false;"></a><script language="javascript">mkarma_2754740.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>Ver para creer? En cordoba subió en 9000 personas, en total hay 132000 desempleados.</p></div>
</div>
<a name="comment-2754750"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">19</div>23-10-2014 / 09:45<br><div class="nombre-usuario"></div>
<div class="puntos">
<script language="javascript">
			mkarma_2754750 = new comment_karma('2754750', '-6');
		</script>
		Puntuación <span id="karma_2754750">-6</span><a id="karma_accion_subir_2754750" onclick="return false;"></a><a id="karma_accion_bajar_2754750" onclick="return false;"></a><script language="javascript">mkarma_2754750.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>1º.- Cuando la Administración deje de saquer esto si que mejoraría de verdad<br /><br/>2º.- Haber cuando algún partido, sindicato, agentes sociales, ONGs, etc.  crean trabajo del bueno: bien remunerado, con buen horario, que permita la conciliación laboral con la personal/familar, etc. y todo esto sin sacarle un euro a nadie.<br /><br/>3º.-  Cuando dejaran los prejubilados de contar como parados ? o por lo menos que especifiquen cuantos son.</p></div>
</div>
<a name="comment-2754751"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">20</div>23-10-2014 / 09:46<br><div class="nombre-usuario">Juan</div>
<div class="puntos">
<script language="javascript">
			mkarma_2754751 = new comment_karma('2754751', '23');
		</script>
		Puntuación <span id="karma_2754751">23</span><a id="karma_accion_subir_2754751" onclick="return false;"></a><a id="karma_accion_bajar_2754751" onclick="return false;"></a><script language="javascript">mkarma_2754751.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>jajaja<br /><br/>El paro SIEMPRE baja en el TERCER trimestre. El dato de paro de octubre casi siempre destruye empleo; cuando salga va a dar miedo ver cómo está el panorama.</p></div>
</div>
<a name="comment-2754754"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">21</div>23-10-2014 / 09:47<br><div class="nombre-usuario">agu</div>
<div class="puntos">
<script language="javascript">
			mkarma_2754754 = new comment_karma('2754754', '-9');
		</script>
		Puntuación <span id="karma_2754754">-9</span><a id="karma_accion_subir_2754754" onclick="return false;"></a><a id="karma_accion_bajar_2754754" onclick="return false;"></a><script language="javascript">mkarma_2754754.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>Los datos son buenos, no por si mismos sino porque si en este entorno de estancamiento conseguimos estas cifras que no pasará cuando la cosa mejore, lo de que se vaya la gente sobre todo extranjeros es lógico lo ilógico es que siguieran en españa mientras no había trabajo para nadie via subsidios mas economía sumergida y destrozando al resto de la economía.<br /><br/>Vivo en almería y esto es un desastre el binomio extranjeros mas subsidios ha llevado la economía sumergida del 18% al 37% y el paro del 8% al 38% siendo la que tiene mas paro y mas economía sumergida mientras los españoles malvivimos comidos por una competencia feroz y desleal.</p></div>
</div>
<a name="comment-2754757"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">22</div>23-10-2014 / 09:47<br><div class="nombre-usuario">k</div>
<div class="puntos">
<script language="javascript">
			mkarma_2754757 = new comment_karma('2754757', '26');
		</script>
		Puntuación <span id="karma_2754757">26</span><a id="karma_accion_subir_2754757" onclick="return false;"></a><a id="karma_accion_bajar_2754757" onclick="return false;"></a><script language="javascript">mkarma_2754757.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>10#<br /><br/>Bestia pperro...bestial se escribe con b, no con v.<br /><br/>Claro, que de un bestia, analfabeto del pp ¿Qué vamos a esperar? ¿Qué podemos esperar de este gobierno de golfos y ladrones que están hundiendo todos los días el país?<br /><br/>Por cierto, los votos ya están manipulados como es costumbre en este medio.</p></div>
</div>
<a name="comment-2754765"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">23</div>23-10-2014 / 09:50<br><div class="nombre-usuario">Jerezano</div>
<div class="puntos">
<script language="javascript">
			mkarma_2754765 = new comment_karma('2754765', '39');
		</script>
		Puntuación <span id="karma_2754765">39</span><a id="karma_accion_subir_2754765" onclick="return false;"></a><a id="karma_accion_bajar_2754765" onclick="return false;"></a><script language="javascript">mkarma_2754765.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>Que vengan a Jerez de la Frontera a medirlo que estamos la mitad de la ciudad en paro</p></div>
</div>
<a name="comment-2754784"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">24</div>23-10-2014 / 09:59<br><div class="nombre-usuario">esturion</div>
<div class="puntos">
<script language="javascript">
			mkarma_2754784 = new comment_karma('2754784', '31');
		</script>
		Puntuación <span id="karma_2754784">31</span><a id="karma_accion_subir_2754784" onclick="return false;"></a><a id="karma_accion_bajar_2754784" onclick="return false;"></a><script language="javascript">mkarma_2754784.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>Bajando con contratos basura y trabajando 1 o 2 dias al mes te quitan del paro y con sueldos que no dan ni para comer.PURO MAQUILLAJE DE DATOS. Por otra parte la DEUDA DEL PAIS SUBIENDO DE FORMA DESCONTROLADA.</p></div>
</div>
<a name="comment-2754786"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">25</div>23-10-2014 / 10:00<br><div class="nombre-usuario">A la mierda con España</div>
<div class="puntos">
<script language="javascript">
			mkarma_2754786 = new comment_karma('2754786', '25');
		</script>
		Puntuación <span id="karma_2754786">25</span><a id="karma_accion_subir_2754786" onclick="return false;"></a><a id="karma_accion_bajar_2754786" onclick="return false;"></a><script language="javascript">mkarma_2754786.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>ME QUEDO ALUCINANDO COMO OS TRAGAIS TANTA MANIPULACION.<br /><br/>UNA ENCUESTA? LA NOTICIA LA DA UNA ENCUESTA?<br /><br/>COMO NO VA A IR ESPAÑA COMO VA SI NO TENEMOS NADA MAS QUE PALETOS QUE MIRAN TODO EL DIA WEBS DE NOTICIAS MANIPULADAS POR "ENCUESTAS"<br /><br/>SEGUID SEGUID... CUANDO SE OS VENGA ENCIMA TODO LO QUE ESTA POR LLEGAR LOS UNICOS CULPABLES SEREIS VOSOTROS MISMOS POR VUESTRA IGNORANCIA Y COMPLACENCIA.<br /><br/>ALE, A MAMARLA ESPAÑOLITOS PALETOS.</p></div>
</div>
<div class="bott"><ul>
<li><a href="/indicadores-espana/noticias/6181497/10/14/El-paro-baja-en-195200-personas-en-el-tercer-trimestre-la-tasa-de-desempleo-desciende-hasta-el-2367.html#Comentarios" class="select">1</a></li>
<li><a href="/indicadores-espana/noticias/6181497/10/14/2/El-paro-baja-en-195200-personas-en-el-tercer-trimestre-la-tasa-de-desempleo-desciende-hasta-el-2367.html#Comentarios">2</a></li>
<li><a href="/indicadores-espana/noticias/6181497/10/14/3/El-paro-baja-en-195200-personas-en-el-tercer-trimestre-la-tasa-de-desempleo-desciende-hasta-el-2367.html#Comentarios">3</a></li>
<li><a href="/indicadores-espana/noticias/6181497/10/14/4/El-paro-baja-en-195200-personas-en-el-tercer-trimestre-la-tasa-de-desempleo-desciende-hasta-el-2367.html#Comentarios">4</a></li>
</ul></div>
<a name="Formulario"></a><form action="http://www.eleconomista.es/blogs/en-portada/wp-comments-post_ab3da3.php" method="post" id="commentform" name="commentform"><fieldset>
<legend>Deja tu comentario</legend>
<div>
<p class="peq">Comenta las noticias de elEconomista.es como usuario genérico o utiliza tus cuentas de Facebook o Google Friend Connect para garantizar la identidad de tus comentarios:</p>
<div class="form-row" id="form-comentario-terceros-generico" style="display: none;"></div>
<div class="form-row" id="form-comentario-terceros-facebook" style="display: none;">
<div id="fb-login-box"></div>
<div id="fb-login-button" style="text-align: left; margin-top: 10px;">
						<fb:login-button size="medium" background="white" length="long" onlogin="fbLoginUser();"></fb:login-button>
					</div>
</div>
<div class="form-row" id="form-comentario-terceros-google-plus" style="display: none;">
<div id="gp-login-box"></div>
<div id="gp-login-button" style="text-align: left; margin-top: 10px;"></div>
</div>
<p class="botones-tipo-usuarios"><img id="tipo-generico" src="http://s01.s3c.es/imag3/iconos/boton-generico-activo.gif" onclick="comentariosUsuariosTercerosObj.cambiarTipo('generico');"><img id="tipo-facebook" src="http://s01.s3c.es/imag3/iconos/boton-facebook-inactivo.gif" onclick="comentariosUsuariosTercerosObj.cambiarTipo('facebook');"><img id="tipo-google-plus" src="http://s01.s3c.es/imag3/iconos/boton-google-plus-inactivo.gif" onclick="comentariosUsuariosTercerosObj.cambiarTipo('google-plus');"></p>
</div>
<div id="gf-login-button" style="display: none;"></div>
<div class="form-row" id="form-comentario-comentario">
<div class="field-label">
<label for="comment">Comentario</label>:</div>
<div class="field-widget"><textarea name="comment" title="Introduce el comentario" id="comment" class="required" type="text" rows="5" cols="60" tabindex="1"></textarea></div>
</div>
<div class="form-row" id="form-comentario-nombre">
<div class="field-label">
<label for="author">Nombre</label>:</div>
<div class="field-widget">
<input name="author" id="author" class="required" title="Introduce tu nombre" tabindex="2"><input name="authorId" id="authorId" type="hidden"><input name="authorTipo" id="authorTipo" type="hidden">
</div>
</div>
<div class="form-row" id="form-comentario-email">
<div class="field-label">
<label for="email">e-mail</label>:</div>
<div class="field-widget"><input name="email" id="email" class="required validate-email" title="Introduce tu email" tabindex="3"></div>
</div>
<div class="form-row" id="form-comentario-website">
<div class="field-label">
<label for="url">Website</label>:</div>
<div class="field-widget"><input name="url" id="url" class="validate-url" title="Por favor, introduce la URL de tu pagina web" tabindex="4"></div>
</div>
<div class="form-row"><div class="field-label">
<label for="comterms">Acepto la <a href="http://www.eleconomista.es/politica-de-privacidad/">política de privacidad</a></label>:<input type="checkbox" value="1" title="acepta las condiciones" class="validate-selection required" name="mivariable_checkbox" tabindex="5">
</div></div>
<p class="peq">elEconomista no se hace responsable de las opiniones expresadas en los comentarios y las mismos no constituyen la opinión de elEconomista. No obstante, elEconomista no tiene obligación de controlar la utilización de éstos por los usuarios y no garantiza que se haga un uso diligente o prudente de los mismos. Tampoco tiene la obligación de verificar y no verifica la identidad de los usuarios, ni la veracidad, vigencia, exhaustividad y/o autenticidad de los datos que los usuarios proporcionan y excluye cualquier responsabilidad por los daños y perjuicios de toda naturaleza que pudieran deberse a la utilización de los mismos o que puedan deberse a la ilicitud, carácter lesivo, falta de veracidad, vigencia, exhaustividad y/o autenticidad de la información proporcionada.</p>
<input name="comment_post_ID" value="684197" type="hidden"><input name="redirect_to" value="http://www.eleconomista.es/indicadores-espana/noticias/6181497/10/14/5/El-paro-baja-en-195200-personas-en-el-tercer-trimestre-la-tasa-de-desempleo-desciende-hasta-el-2367.html" type="hidden"><input type="button" value="Enviar" onclick="return comentariosUsuariosTercerosObj.validate();" tabindex="6"><input type="button" value="Borrar" onclick="valid.reset(); return false" tabindex="7">
</fieldset></form>
<script type="text/javascript">
		function formCallback(result, form) {
			return;
			//window.status = "valiation callback for form '" + form.id + "': result = " + result;
		}
		var valid = new Validation('commentform', {immediate : true, onFormValidate : formCallback});
	</script><script type="text/javascript">
		function comentariosUsuariosTerceros_cargarEstadoInicial() {
			var tipo = getCookie('COMENTARIOS_TIPO_USUARIO');
			if ( (tipo == null) || (tipo == '')) tipo = 'generico';
			comentariosUsuariosTercerosObj.cambiarTipo(tipo);
		}
		var comentariosUsuariosTercerosObj = new comentariosUsuariosTerceros();
		comentariosUsuariosTercerosObj.validation(valid);
		Event.observe(window, 'load', comentariosUsuariosTerceros_cargarEstadoInicial);
	</script><script type="text/javascript">
		window.___gcfg = {
			lang: 'es',
		};

      (function() {
       var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
       po.src = 'http://apis.google.com/js/client:plusone.js';
       var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
     })();
    </script>
</div>
</div>

		</div>
		<div class="col">
			<div class="cont-flash" style="margin-bottom:10px;"><div mod="1246"><div id="webslice_flash_del_mercado" class="hslice entry-content">
<a style="display: none;" rel="entry-content" href="/slices/web-slice.php?modulo=1246"></a><span class="ttl" style="display: none;">15</span><div class="flash caja" style="border-top:1px solid #666666;">
<h3 style="margin-top:-5px; margin-bottom:10px; padding:0; ">
<a href="/flash/index.html">El Flash del mercado</a><span class="entry-title" style="display: none;">en elEconomista.es</span><a href="https://www.bancsabadell.com/" rel="nofollow" target="_blank"><img src="http://s01.s3c.es/imag3/logos/sabadell.gif" alt="Sabadell Atlantico" style="border: none; margin-left: 35px;"></a>
</h3>
<ul>
<li>
<span class="hora">17:31</span><a href="http://www.eleconomista.es/emprendedores-pymes/noticias/6183701/10/14/Guudjob-una-aplicacion-para-buscar-y-ofrecer-servicios-profesionales.html">Guudjob, una aplicación para buscar y ofrecer servicios profesionales</a>
</li>
<li>
<span class="hora">17:28</span><a href="http://www.eleconomista.es/mercados-cotizaciones/noticias/6183682/10/14/Los-planes-de-pensiones-solo-son-rentables-para-los-que-ganen-mas-de-60000-euros-anuales.html">Los planes de pensiones solo son rentables para los que ganen más de 60.000 euros anuales</a>
</li>
<li>
<span class="hora">17:12</span><a href="http://www.eleconomista.es/nutricion/noticias/6183624/10/14/Una-cadena-de-supermercados-suiza-retira-miles-de-envases-de-nata-con-la-cara-de-Hitler-y-Mussolini.html">Una cadena de supermercados suiza retira miles de envases de nata con la cara de Hitler y Mussolini</a>
</li>
<li class="mas">
<span class="sigue" style="display: inline; margin-right: 20px;"><a href="http://www.eleconomista.es/flash/index.html">Ver todos</a></span><span class="sigue sigue2" style="display: inline;"><a href="http://www.eleconomista.es/fichas-de-empresas/index.html">Nuevas fichas empresas</a></span>
</li>
</ul>
</div>
</div></div>

			</div><div mod="5004">
<script language="javascript">//<![CDATA[
        
if( typeof( modulosSite) == "object") modulosSite.push("5011");
            
if( typeof( modulosReemplazados) == "object") modulosReemplazados.push("5004_1");
                
if( typeof( modulosSite) == "object") modulosSite.push("5027");
            
if( typeof( modulosReemplazados) == "object") modulosReemplazados.push("5004_2");
                
//]]></script><div mod="5004_1"></div>
<div mod="5004_2"></div>
</div>
<div mod="1273" class="b300">
<script language="javascript">//<![CDATA[
						sz = "300x250" ;
						st = "eleconomista.es" ;
						std = "http://ad.es.doubleclick.net/" ;
						kw = "";
				//		randnum = old_randnum;
						 sect ="6181497";
						document.write( '<SCR'+'IPT language="JavaScript" SRC="'+std+'adj/'+st+'/economista_noticias_indicadores_espana;sect='+sect+';tile=5;kw='+kw+';sz='+sz+';ord='+randnum+'?"></SCR'+'IPT>' ) ;
					//]]></script><noscript><a href="http://ad.doubleclick.net/jump/eleconomista.es/economista_noticias_indicadores_espana;sect=6181497;tile=5;kw=;sz=300x250;ord=123456789?" target="_blank" rel="nofollow"><img src="http://ad.doubleclick.net/ad/eleconomista.es/economista_noticias_indicadores_espana;sect=6181497;tile=5;kw=;sz=300x250;ord=123456789?" width="300" height="250" border="0"></a></noscript>
</div>

		<div class="e-eu"><div></div>

		</div><div></div>
 
			<div class="caja mods">
				<div style="overflow: hidden;background: #fff;padding: 10px;">
				<img src="http://s01.s3c.es/imag3/logos/eE3.png" alt="" style="float: left; padding-right: 10px;border-right: 2px solid #eee;margin-right: 10px;margin-top: 6px;">

				<!--twitter-->
				<iframe id="twitter-widget-0" scrolling="no" frameborder="0" allowtransparency="true" src="http://platform.twitter.com/widgets/follow_button.1404859412.html#_=1405593639981&amp;id=twitter-widget-0&amp;lang=es&amp;screen_name=elEconomistaes&amp;show_count=false&amp;show_screen_name=true&amp;size=m" class="twitter-follow-button twitter-follow-button" title="Twitter Follow Button" data-twttr-rendered="true" style="width: 171px; height: 20px;"></iframe>
			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?"http":"https";if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document, "script", "twitter-wjs");</script>
				<!--facebook-->
				<iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FelEconomista.es&amp;width=130px&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21&amp;appId=396195810399162" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:130px; height:21px;margin:5px 0;" allowtransparency="true"></iframe>
				<!--google-->
				<!-- Inserta esta etiqueta donde quieras que aparezca widget. -->
				<div id="___follow_0" style="text-indent: 0px; margin: 0px; padding: 0px; border-style: none; float: none; line-height: normal; font-size: 1px; vertical-align: baseline; display: inline-block; width: 91px; height: 20px; background: transparent;"><iframe frameborder="0" hspace="0" marginheight="0" marginwidth="0" scrolling="no" style="position: static; top: 0px; width: 91px; margin: 0px; border-style: none; left: 0px; visibility: visible; height: 20px;" tabindex="0" vspace="0" width="100%" id="I0_1405593638317" name="I0_1405593638317" src="https://apis.google.com/u/0/_/widget/render/follow?usegapi=1&amp;annotation=none&amp;height=20&amp;rel=publisher&amp;hl=es&amp;origin=http%3A%2F%2Fdesarrollo.eleconomista.es&amp;url=https%3A%2F%2Fplus.google.com%2F113527166384132333126&amp;gsrc=3p&amp;ic=1&amp;jsh=m%3B%2F_%2Fscs%2Fapps-static%2F_%2Fjs%2Fk%3Doz.gapi.es.kgwtPUNfqKs.O%2Fm%3D__features__%2Fam%3DAQ%2Frt%3Dj%2Fd%3D1%2Ft%3Dzcms%2Frs%3DAItRSTMD3J2AR92dCXMjh4MTmaKFMQ247g#_methods=onPlusOne%2C_ready%2C_close%2C_open%2C_resizeMe%2C_renderstart%2Concircled%2Cdrefresh%2Cerefresh%2Conload&amp;id=I0_1405593638317&amp;parent=http%3A%2F%2Fdesarrollo.eleconomista.es&amp;pfname=&amp;rpctoken=38912371" data-gapiattached="true"></iframe></div>

				<!-- Inserta esta etiqueta después de la última etiqueta de widget. -->
				<script type="text/javascript">
				  window.___gcfg = {lang: "es"};

				  (function() {
				    var po = document.createElement("script"); po.type = "text/javascript"; po.async = true;
				    po.src = "https://apis.google.com/js/platform.js";
				    var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(po, s);
				  })();
				</script>
			</div>
			</div>
			
				<div>
					<table cellspacing="0" class="tablapeq" style="margin:0 0 10px 0">
						<caption style="background: #f60; color: #fff;">elMonitor</caption>
						<tbody>
							<tr>
								<td style="text-align: left;">
									<h3 style="font-weight: bold; margin: 5px 0;">
										<a style="color: #333; text-decoration: none;" href="http://www.eleconomista.es/monitor">La herramienta para el ahorrador en Bolsa
										</a>
									</h3>
									<a href="http://www.eleconomista.es/monitor">
										<img style="float: left; margin-right: 5px;" alt="Monitor" src="http://s04.s3c.es/imag3/monitor/monitor50.png">
									</a>

									<p>
										<a style="color: #333; text-decoration: none;" href="http://www.eleconomista.es/monitor">Construya su cartera de inversión de forma clara y sencilla con las recomendaciones de elMonitor. <span style="color: #f60; font-weight: bold;">¡Regístrese y pruébelo GRATIS!</span>
										</a>
									</p>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="sep"></div>
			
<!-- CAJA DE REGISTRO ALERTAS -->
<div class="caja c-alerta" style="background: #fff;margin-bottom: 0; border-bottom:none;">
	<h3 style="margin-bottom: 5px; padding-left: 5px;">Alertas por email</h3>
	<img src="http://s04.s3c.es/imag3/iconos/alerta3.png" alt="alerta" style="float: left;" />
		<form action="http://www.eleconomista.es/boletines/ultima-hora.php" method="post">
			<label for="alerta-email" style="font-weight: normal;">Suscríbete y recibe <b>las noticias de última hora gratis</b> en tu email.</label>
			<div style="margin-top: 5px;float: left;">
			<input type="text" value="" name="email" class="email" id="" placeholder="Dirección de email" /></div>
			<div style="margin-top: 5px; text-align: left; padding-left: 61px;">
			<input type="submit" value="Subscribirse" name="alta" id="alta" class="c-a-boton" />
			</div>
		</form>
</div>
<!-- FIN CAJA DE REGISTRO ALERTAS -->

<div class="caja c-alerta" style="background: #fff;border-top: 3px solid #666;">
	<h3 style="margin-bottom: 5px; padding-left: 5px;font-family: Georgia,Times,serif;">el<span style="color:#f60;">Resumen</span>Diario</h3>
	<!--<img src="http://s04.s3c.es/imag3/iconos/alerta3.png" alt="alerta" style="float: left;" />-->
		<form action="http://www.eleconomista.es/boletines/alta_ok.php" method="post">
			<label for="alerta-email" style="font-weight: normal;margin: 6px 5px;display: block;line-height: 14px;">Suscríbete y recibe <b>el resumen de las noticias más importantes del día</b> en tu email.</label>
			<div style="margin: 5px 0 0 5px;">
			<input style="width: 199px;"" type="text" value="" name="email" class="email" id="" placeholder="Dirección de email" />
			<input type="hidden" value="1" id="Optin_news1" name="Optin_news1">
			<input style="background: #f60;" type="submit" value="Subscribirse" name="alta" id="alta" class="c-a-boton" />
			</div>
		</form>
</div>
<div class="sep"></div><div class="sep"></div>
<div mod="2779">
<div class="uh uh-new">
<script type="text/javascript">
			//<![CDATA[
			var pestanas = new Array();
				
				
				pestanas[0] = {site:1, tag:'tmas1', id:'d-1'};
				pestanas[1] = {site:4, tag:'tmas2', id:'d-2'};
				pestanas[2] = {site:4, tag:'tmas3', id:'d-3'};
				pestanas[3] = {site:4, tag:'tmas4', id:'d-4'};
				
				
				
					vMasNoticiasP = new masNoticias (pestanas[0].id, pestanas[0].tag);
				
			//]]>
			</script><h5>El flash: toda la última hora</h5>
<ul class="pest pest2"><li id="tmas1"><a href="#" onclick="javascript:vMasNoticiasP.mostrar('tmas1','d-1');return false;" target="_top">Actualidad</a></li></ul>
<script type="text/javascript">
				//<![CDATA[
					$(pestanas[0].tag).addClassName('select');
					
				//]]>
				</script><div class="uh-cont  uh-cont2">
<div id="d-1" class="uh-not2">
<ul>
<div class="uh-not2">
<h2><a href="http://ecodiario.eleconomista.es/sociedad/noticias/6183680/10/14/Un-subsahariano-va-a-la-carcel-como-patron-de-una-patera-por-primera-vez.html?utm_source=crosslink&amp;utm_medium=flash" target="_top">Un subsahariano va a la cárcel como patrón de una patera por prim…</a></h2>
<div class="uh-hora2">
<span class="hora5">17:26</span>
										Ecodiario.es
									
								 - Sociedad<div class="sep"></div>
<img src="http://s01.s3c.es/imag/_v0/580x300/d/3/2/rescate-patera-efe.jpg"><p>Por primera vez un inmigrante subsahariano estará tres años en la cárcel como responsable del intento de entrada e...
							 
							 </p>
</div>
</div>
<div class="uh-not2">
<h2><a href="http://ecodiario.eleconomista.es/cine/noticias/6183675/10/14/Paco-Leon-La-gente-quiere-ser-Marlon-Brando-yo-Carmina.html?utm_source=crosslink&amp;utm_medium=flash" target="_top">Paco León: "La gente quiere ser Marlon Brando, yo Carmina"</a></h2>
<div class="uh-hora2">
<span class="hora5">17:25</span>
										Ecodiario.es
									
								 - Cine<div class="sep"></div>
<img src="http://s01.s3c.es/imag/_v0/800x543/8/d/b/Paco-Leon.jpg"><p>"La gente quiere ser Marlon Brando, yo quiero ser Carmina", afirma el actor y director Paco León, quien aún está p...
							 
							 </p>
</div>
</div>
<div class="uh-not2">
<h2><a href="http://ecodiario.eleconomista.es/terrorismo/noticias/6183623/10/14/La-Camara-vasca-acepta-como-parlamentario-a-Casanova-condenado-por-ser-de-ETA.html?utm_source=crosslink&amp;utm_medium=flash" target="_top">La Cámara vasca acepta como parlamentario a Casanova, condenado p…</a></h2>
<div class="uh-hora2">
<span class="hora5">17:12</span>
										Ecodiario.es
									
								 - Terrorismo <div class="sep"></div>
<p>El Parlamento vasco ha respaldado hoy la compatibilidad de Iker Casanova para ocupar un escaño en la Cámara por EH...
							 
							 </p>
</div>
</div>
<div class="uh-not2">
<h2><a href="http://ecodiario.eleconomista.es/politica/noticias/6183593/10/14/Romero-y-los-sanitarios-que-han-trabajado-en-la-crisis-del-ebola-seran-condecorados.html?utm_source=crosslink&amp;utm_medium=flash" target="_top">Romero y los sanitarios que han trabajado en la crisis del ébola …</a></h2>
<div class="uh-hora2">
<span class="hora5">17:04</span>
										Ecodiario.es
									
								 - Política<div class="sep"></div>
<img src="http://s01.s3c.es/imag/_v0/50x47/0/4/b/ebola-50.jpg"><p>El presidente de la Comunidad de Madrid, Ignacio González, ha anunciado que propondrá la concesión de "la mas alta...
							 
							 </p>
</div>
</div>
<div class="uh-not2">
<h2><a href="http://ecodiario.eleconomista.es/sociedad/noticias/6183578/10/14/La-Guardia-Civil-detiene-a-nueve-personas-por-un-alijo-de-cocaina-en-Pontevedra.html?utm_source=crosslink&amp;utm_medium=flash" target="_top">La Guardia Civil detiene a nueve personas por un alijo de cocaína…</a></h2>
<div class="uh-hora2">
<span class="hora5">17:00</span>
										Ecodiario.es
									
								 - Sociedad<div class="sep"></div>
<p>Nueve personas han sido detenidas en relación con un alijo de 50 kilos de cocaína localizado a lo largo de la maña...
							 
							 </p>
</div>
</div>
<div class="uh-not2">
<h2><a href="http://ecodiario.eleconomista.es/futbol/noticias/6183565/10/14/El-Sevilla-busca-medio-billete-para-los-cruces-en-Lieja.html?utm_source=crosslink&amp;utm_medium=flash" target="_top">El Sevilla busca medio billete para los cruces en Lieja</a></h2>
<div class="uh-hora2">
<span class="hora5">16:51</span>
										Ecodiario.es
									
								 - Fútbol<div class="sep"></div>
<img src="http://s01.s3c.es/imag/_v0/580x364/b/3/0/sevilla-standard-entreno-efe.jpg"><p>El Sevilla intentará sumar los tres puntos en su segunda salida consecutiva de la fase de grupos de la Europa Leag...
							 
							 </p>
</div>
</div>
<div class="uh-not2">
<h2><a href="http://ecodiario.eleconomista.es/ciencia/noticias/6183542/10/14/Una-rueda-infrarroja-de-vida-galactica-brilla-en-el-espacio.html?utm_source=crosslink&amp;utm_medium=flash" target="_top">Una rueda infrarroja de vida galáctica brilla en el espacio</a></h2>
<div class="uh-hora2">
<span class="hora5">16:47</span>
										Ecodiario.es
									
								 - Ciencia<div class="sep"></div>
<img src="http://s01.s3c.es/imag/_v0/580x300/3/b/c/rueda-brillante-galactica-nasa.jpg"><p>La NASA ha publicado una imagen del telescopio Spitzer que muestra la galaxia NGC 1291. A pesar de que es muy viej...
							 
							 </p>
</div>
</div>
<div class="uh-not2">
<h2><a href="http://ecodiario.eleconomista.es/flash/noticias/6183497/10/14/Acaba-el-registro-de-ocho-horas-en-la-casa-de-Oleguer-Pujol-que-queda-en-libertad-con-cargos.html?utm_source=crosslink&amp;utm_medium=flash" target="_top">Acaba el registro de ocho horas en la casa de Oleguer Pujol, que …</a></h2>
<div class="uh-hora2">
<span class="hora5">16:33</span>
										Ecodiario.es
									
								 - Flash<div class="sep"></div>
<p>El registro de la vivienda de Oleguer Pujol, ordenado por el juez de la Audiencia Nacional Santiago Pedraz, ha fin...
							 
							 </p>
</div>
</div>
<div class="uh-not2">
<h2><a href="http://ecodiario.eleconomista.es/ciencia/noticias/6183496/10/14/Microbios-de-la-superficie-congelada-artica-amplipifican-el-cambio-climatico-de-origen-humano.html?utm_source=crosslink&amp;utm_medium=flash" target="_top">Microbios de la superficie congelada ártica amplipifican el cambi…</a></h2>
<div class="uh-hora2">
<span class="hora5">16:33</span>
										Ecodiario.es
									
								 - Ciencia<div class="sep"></div>
<img src="http://s01.s3c.es/imag/_v0/580x300/c/7/1/superficie-congelada-artico-efe.jpg"><p>Los microbios del suelo helado en el Ártico se encuentran entre los mayores potenciales amplificadores del cambio ...
							 
							 </p>
</div>
</div>
<div class="uh-not2">
<h2><a href="http://ecodiario.eleconomista.es/cine/noticias/6183472/10/14/Confirmado-Christian-Bale-sera-Steve-Jobs.html?utm_source=crosslink&amp;utm_medium=flash" target="_top">Confirmado: Christian Bale será Steve Jobs</a></h2>
<div class="uh-hora2">
<span class="hora5">16:29</span>
										Ecodiario.es
									
								 - Cine<div class="sep"></div>
<img src="http://s01.s3c.es/imag/_v0/580x300/8/4/8/Jobs580.jpg"><p>Finalmente Sony y Danny Boyle verán sus sueños hechos realidad y Christian Bale encarnará a Steve Jobs en el biopi...
							 
							 </p>
</div>
</div>
</ul>
<div class="sep-mas1"></div>
<div class=""><a class="mas-noti" target="_top" href="http://ecodiario.eleconomista.es/flash">Más noticias</a></div>
</div>
<div id="d-2" class="uh-not2"></div>
</div>
<script type="text/javascript">
				//<![CDATA[
					for (i=1; i<pestanas.length; i++) {
						//if (i!=0 ) {
							vMasNoticiasP.ocultar(pestanas[i].id);
						//}
					}
					
	
				//]]>
				</script>
</div>
<div class="sep"></div>
</div>
<div mod="2479" class="masleidas-orus masleidas-orus2"><div class="popular">
<script type="text/javascript">
					//<![CDATA[
						var sites = new Array();
						
						//------------------------------Realizacion de For each -----------------------------
						
							sites[0] = {site:1, tag:'tmaseconomista' , id:'d-economista'};
						
							sites[1] = {site:4, tag:'tmasecodiario' , id:'d-ecodiario'};
						
							sites[2] = {site:18, tag:'tmasecoteuve' , id:'d-ecoteuve'};
						
							sites[3] = {site:8, tag:'tmasmotor' , id:'d-motor'};
						
							sites[4] = {site:16, tag:'tmasevasion' , id:'d-evasion'};
						
					
						idSite = $(document.body).readAttribute('s');
						if(idSite==null){
							idSite=16;
						}
						
		//				idSite = 16;
						
						for (i=0; i<sites.length;i++) {
							if (idSite == sites[i].site ) 
								break;
						}
						vMasNoticiasS = new masNoticias (sites[i].id, sites[i].tag);
						
					//]]>
					</script><div id="pestPopular"><ul class="pest">
<li id="tmaseconomista" style="font-size: 11px;"><a href="#" onclick="javascript:vMasNoticiasS.mostrar('tmaseconomista','d-economista');return false;" target="'_top'">el<span style="color: #000;">Eco</span>nomista</a></li>
<li id="tmasecodiario" style="font-size: 11px;"><a href="#" onclick="javascript:vMasNoticiasS.mostrar('tmasecodiario','d-ecodiario');return false;" target="'_top'"><span style="color: #000;">Eco</span>Diario</a></li>
<li id="tmasecoteuve" style="font-size: 11px;"><a href="#" onclick="javascript:vMasNoticiasS.mostrar('tmasecoteuve','d-ecoteuve');return false;" target="'_top'"><span style="color: #000;">Eco</span>teuve</a></li>
<li id="tmasmotor" style="font-size: 11px;"><a href="#" onclick="javascript:vMasNoticiasS.mostrar('tmasmotor','d-motor');return false;" target="'_top'">Motor</a></li>
<li id="tmasevasion" style="font-size: 11px;"><a href="#" onclick="javascript:vMasNoticiasS.mostrar('tmasevasion','d-evasion');return false;" target="'_top'">Evasión</a></li>
</ul></div>
<script type="text/javascript">
			//<![CDATA[
				$(sites[i].tag).addClassName('select');
				
			//]]>
			</script><div id="d-economista" class="masleidas">
<p class="mas-leidas-cint">Noticias más leidas</p>
<ul>
<li><a href="http://www.eleconomista.es/materias-primas/noticias/6182949/10/14/Arabia-Saudi-anuncia-que-recorta-la-produccion-de-petroleo-el-precio-del-barril-vuelve-a-subir.html" target="_top">Sorpresa en el mercado del petróleo: Arabia Saudí recortó la…</a></li>
<li><a href="http://www.eleconomista.es/economia/noticias/6182335/10/14/Otra-perspectiva-de-la-EPA-30-curiosidades-mas-alla-del-dato-del-paro.html" target="_top">La otra cara de la EPA: hasta 30 datos curiosos más allá de …</a></li>
<li><a href="http://www.eleconomista.es/economia/noticias/6183399/10/14/El-hombre-mas-barato-que-la-maquina-por-que-podrian-fracasar-los-coches-autonomos.html" target="_top">El hombre, más barato que la máquina: la nueva realidad que …</a></li>
<li><a href="http://www.eleconomista.es/economia/noticias/6181293/10/14/La-letra-pequena-de-la-reforma-tributaria-.html" target="_top">La letra pequeña de la reforma tributaria: más impuestos per…</a></li>
<li><a href="http://www.eleconomista.es/banca-finanzas/noticias/6182313/10/14/Dancausa-asegura-que-en-Caja-Madrid-se-ha-dado-una-mezcla-de-la-cultura-del-saqueo-y-de-la-chapuza.html" target="_top">Dancausa asegura que en Caja Madrid "se ha dado una mezcla d…</a></li>
</ul>
<div class="sep-mas1"></div>
<div class="sigue" style="margin:5px 0 8px"><a target="_top" href="http://www.eleconomista.es/">Más noticias</a></div>
</div>
<div id="d-ecodiario" class="masleidas">
<p class="mas-leidas-cint">Noticias más leidas</p>
<ul>
<li><a href="http://ecodiario.eleconomista.es/futbol/noticias/6183330/10/14/Alex-Fbregas-Jugue-con-Espana-sin-sentirme-espanol-pero-me-deje-la-piel.html" target="_top">Alex Fàbregas: "Jugué con España sin sentirme español pero m…</a></li>
<li><a href="http://ecodiario.eleconomista.es/espana/noticias/6181988/10/14/La-exnovia-de-Jordi-Pujol-Ferrusol-Hoy-es-un-gran-dia-para-todos-los-espanoles.html" target="_top">La exnovia de Jordi Pujol junior: "Hoy es un gran día para t…</a></li>
<li><a href="http://ecodiario.eleconomista.es/sociedad/noticias/6182506/10/14/Teresa-Romero-tiene-un-bajon-emocional-por-su-aislamiento-y-la-muerte-de-Excalibur.html" target="_top">Teresa Romero pide justicia por el sacrificio de su perro: "…</a></li>
<li><a href="http://ecodiario.eleconomista.es/sociedad/noticias/6181731/10/14/Isabel-Pantoja-pide-que-se-suspenda-su-entrada-en-prision-por-blanqueo.html" target="_top">Isabel Pantoja pide que se suspenda su entrada en prisión po…</a></li>
<li><a href="http://ecodiario.eleconomista.es/politica/cronicas/5175/10/14/EN-DIRECTO-Siga-en-EcoDiarioes-todo-lo-relativo-a-la-detencin-de-Oleguer-Pujol.html" target="_top">EN DIRECTO: Siga en EcoDiario.es todo lo relativo a la deten…</a></li>
</ul>
<div class="sep-mas1"></div>
<div class="sigue" style="margin:5px 0 8px"><a target="_top" href="http://ecodiario.eleconomista.es/index.html">Más noticias</a></div>
</div>
<div id="d-ecoteuve" class="masleidas">
<p class="mas-leidas-cint">Noticias más leidas</p>
<ul>
<li><a href="http://ecoteuve.eleconomista.es/ecoteuve/television/noticias/6182851/10/14/Cristina-Pedroche-abandona-una-entrevista-porque-mis-tetas-lo-han-decicido.html" target="_top">Cristina Pedroche abandona una entrevista "porque mis tetas …</a></li>
<li><a href="http://ecoteuve.eleconomista.es/programas/noticias/6182479/10/14/Paula-Echevarria-despierta-la-polemica-en-El-Hormiguero-con-su-vestido-de-streptease.html" target="_top">Paula Echevarría despierta la polémica en 'El Hormiguero' co…</a></li>
<li><a href="http://ecoteuve.eleconomista.es/programas/noticias/6181856/10/14/Jordi-Evole-revela-que-un-perro-estaba-empenado-en-la-maleta-azul-de-Pablo-Iglesias-.html" target="_top">Jordi Évole revela la "angustia" que vivió con la maleta de …</a></li>
<li><a href="http://ecoteuve.eleconomista.es/ecoteuve/radio/noticias/6181964/10/14/Paco-Gonzalez-y-Manolo-Lama-acercan-posturas-con-la-Cope-hay-acuerdo-a-falta-de-flecos.html" target="_top">Paco González y Manolo Lama acercan posturas con la Cope: ha…</a></li>
<li><a href="http://ecoteuve.eleconomista.es/programas/noticias/6183106/10/14/Isaac-y-Triana-dejan-sus-tronos-para-ser-la-nueva-pareja-de-Mujeres-y-hombres-y-viceversa.html" target="_top">Isaac y Triana dejan sus tronos para ser la nueva pareja de …</a></li>
</ul>
<div class="sep-mas1"></div>
<div class="sigue" style="margin:5px 0 8px"><a target="_top" href="http://ecoteuve.eleconomista.es/index.html">Más noticias</a></div>
</div>
<div id="d-motor" class="masleidas">
<p class="mas-leidas-cint">Noticias más leidas</p>
<ul>
<li><a href="http://www.eleconomista.es/ecomotor/motor/noticias/6181610/10/14/Como-se-debe-circular-por-una-rotonda-la-explicacion-definitiva.html" target="_top">Cómo se debe circular por una rotonda: la explicación defini…</a></li>
<li><a href="http://www.eleconomista.es/ecomotor/motor/noticias/6181518/10/14/Volkswagen-Passat-2015-renovacion-integral.html" target="_top">Nuevo Volkswagen Passat: renovación integral</a></li>
<li><a href="http://www.eleconomista.es/ecomotor/motor/noticias/6182222/10/14/Como-rellenar-un-parte-amistoso-de-accidente-sin-que-reine-el-caos.html" target="_top">Cómo rellenar un parte amistoso de accidente sin que reine e…</a></li>
<li><a href="http://www.eleconomista.es/ecomotor/motor/noticias/6181848/10/14/Apple-quiere-que-el-iPhone-sea-un-mando-multifuncon-a-distancia-para-el-coche.html" target="_top">Apple quiere que el iPhone sea un mando a distancia multifun…</a></li>
<li><a href="http://www.eleconomista.es/ecomotor/motor/noticias/6181965/10/14/Los-precios-de-la-gasolina-y-el-diesel-caen-a-niveles-de-verano-de-2011.html" target="_top">Los precios de la gasolina y el diésel caen a niveles de ver…</a></li>
</ul>
<div class="sep-mas1"></div>
<div class="sigue" style="margin:5px 0 8px"><a target="_top" href="http://www.eleconomista.es/ecomotor/index.html">Más noticias</a></div>
</div>
<div id="d-evasion" class="masleidas">
<p class="mas-leidas-cint">Noticias más leidas</p>
<ul>
<li><a href="http://www.eleconomista.es/evasion/gente-y-estilo/noticias/6179772/10/14/Michelle-Jenner-y-Javier-Camara-protagonizan-la-nueva-edicion-sonora-de-El-Quijote-en-RNE.html" target="_top">Michelle Jenner y Javier Cámara protagonizan la nueva edició…</a></li>
<li><a href="http://www.eleconomista.es/evasion/gente-y-estilo/noticias/6182280/10/14/Shakira-y-Pique-muestran-orgullosos-su-barriguita-por-primera-vez.html" target="_top">Shakira y Piqué muestran orgullosos 'su barriguita' por prim…</a></li>
<li><a href="http://www.eleconomista.es/evasion/gente-y-estilo/noticias/6183333/10/14/Selena-Gomez-y-Orlando-Bloom-Mas-cerca-que-nunca.html" target="_top">Selena Gómez y Orlando Bloom... ¿Más cerca que nunca?</a></li>
<li><a href="http://www.eleconomista.es/evasion/gente-y-estilo/noticias/6183606/10/14/Isabel-Pantoja-ha-pagado-parte-el-dinero-exacto-en-secreto-de-sumario-y-pedido-aplazamiento.html" target="_top">Isabel Pantoja ha pagado parte, el dinero exacto en "secreto…</a></li>
<li><a href="http://www.eleconomista.es/evasion/gente-y-estilo/noticias/6181622/10/14/Darek-Susana-es-la-mujer-de-mi-vida.html" target="_top">Darek: "Susana es la mujer de mi vida"</a></li>
</ul>
<div class="sep-mas1"></div>
<div class="sigue" style="margin:5px 0 8px"><a target="_top" href="http://www.eleconomista.es/evasion/index.html">Más noticias</a></div>
</div>
<script type="text/javascript">
			//<![CDATA[
				for (i=0; i<sites.length; i++) {
					if (sites[i].site!=idSite ) {
//						console.log (sites[i].id);
						vMasNoticiasS.ocultar(sites[i].id);
					}
				}

			//]]>
			</script>
</div></div>
<div mod="4586">
<div class="masleidas masleidas-ame">
<div class="ame-i"><a href="http://www.eleconomistaamerica.com"><img width="186" src="http://s01.s3c.es/imag3/logos/america/logo7.png" alt=""></a></div>
<ul class="pest-ame" style="padding-left: 35px !important;">
<li class="sel"><a href="http://www.eleconomistaamerica.com/todas-las-noticias/index.html?utm_source=crosslink&amp;utm_medium=ee_ultimas">Más leídas</a></li>
<li><a href="http://www.eleconomistaamerica.com/indices-latinoamericanos/index.html?utm_source=crosslink&amp;utm_medium=ee_ultimas">Bolsas Latam</a></li>
</ul>
<ul>
<li><div class="noticia-ult"><h2><a href="http://www.economiahoy.mx/nacional-eAm-mx/noticias/6183054/10/14/Por-que-han-desaparecido-los-43-estudiantes-normalistas-en-Iguala-.html?utm_source=crosslink&amp;utm_medium=ee_masleidas">¿Por qué han desaparecido los 43 estudiantes normalistas en …</a></h2></div></li>
<li><div class="noticia-ult"><h2><a href="http://www.eleconomistaamerica.co/empresas-eAm-colombia/noticias/6182123/10/14/Educacion-y-empleo-La-cara-mas-amable-de-Inditex.html?utm_source=crosslink&amp;utm_medium=ee_masleidas">Educación y empleo: La cara más amable de Inditex</a></h2></div></li>
<li><div class="noticia-ult"><h2><a href="http://www.economiahoy.mx/actualidad-eAm-mexico/noticias/6182980/10/14/Video-Asi-canta-Paulina-la-hija-mayor-de-Pena-Nieto.html?utm_source=crosslink&amp;utm_medium=ee_masleidas">Video: Así canta Paulina, la hija mayor de Peña Nieto</a></h2></div></li>
<li><div class="noticia-ult"><h2><a href="http://www.eleconomistaamerica.com.ar/sociedad-eAm-argentina/noticias/6182481/10/14/Argentina-Obispos-argentinos-se-oponen-a-la-ley-de-reproduccion-asistida-y-piden-promover-la-adopcion.html?utm_source=crosslink&amp;utm_medium=ee_masleidas">La Iglesia argentina se opone a la reproducción asistida y p…</a></h2></div></li>
<li><div class="noticia-ult"><h2><a href="http://www.eleconomistaamerica.pe/politica-eAm-pe/noticias/6183389/10/14/Un-alcalde-en-Peru-obtiene-el-cargo-tras-ganar-sorteo-con-una-moneda.html?utm_source=crosslink&amp;utm_medium=ee_masleidas">Un alcalde en Perú obtiene el cargo tras ganar sorteo con un…</a></h2></div></li>
</ul>
</div>
<script type="text/javascript">
			s=$(document.body).readAttribute('s');
			$("s_"+s).addClassName("active");
		</script>
</div>
<div mod="1633" class="b300">
<script language="javascript">//<![CDATA[
			sz = "300x251" ;
			st = "eleconomista.es" ;
			std = "http://ad.es.doubleclick.net/" ;
			kw = "";
	//		randnum = old_randnum;
			 sect ="6181497";
			document.write( '<SCR'+'IPT language="JavaScript" SRC="'+std+'adj/'+st+'/economista_noticias_indicadores_espana;sect='+sect+';tile=6;kw='+kw+';sz='+sz+';ord='+randnum+'?"></SCR'+'IPT>' ) ;
		//]]></script><noscript><a href="http://ad.doubleclick.net/jump/eleconomista.es/economista_noticias_indicadores_espana;sect=6181497;tile=6;kw=;sz=300x251;ord=123456789?" target="_blank" rel="nofollow"><img src="http://ad.doubleclick.net/ad/eleconomista.es/economista_noticias_indicadores_espana;sect=6181497;tile=6;kw=;sz=300x251;ord=123456789?" width="300" height="250" border="0"></a></noscript>
</div>
<div class="d4"><div id="contenedor_galeria_3100"><div class="foto-rot" id="caja_3100_1"><div mod="3100"><table class="tablapeq tabla-prima">
<caption>Prima de Riesgo</caption>
<tr>
<th>País</th>
<th>Precio</th>
<th>Puntos</th>
<th>%</th>
</tr>
<tr>
<td class="pri-esp"><a href="/prima-riesgo/espana" title="Prima de riesgo de España">España</a></td>
<td class="pri-dat">133,30</td>
<td class="cot1">-0,21</td>
<td class="cot1">-0,16%</td>
</tr>
<tr>
<td class="pri-fr"><a href="/prima-riesgo/francia" title="Prima de riesgo de Francia">Francia</a></td>
<td class="pri-dat">41,38</td>
<td class="cot0">0,00</td>
<td class="cot0">0,00%</td>
</tr>
<tr>
<td class="pri-it"><a href="/prima-riesgo/italia" title="Prima de riesgo de Italia">Italia</a></td>
<td class="pri-dat">163,94</td>
<td class="cot0">0,00</td>
<td class="cot0">0,00%</td>
</tr>
<tr>
<td class="pri-gr"><a href="/prima-riesgo/grecia" title="Prima de riesgo de Grecia">Grecia</a></td>
<td class="pri-dat">651,36</td>
<td class="cot-1">+3,07</td>
<td class="cot-1">+0,47%</td>
</tr>
<tr>
<td class="pri-pt"><a href="/prima-riesgo/portugal" title="Prima de riesgo de Portugal">Portugal</a></td>
<td class="pri-dat">245,56</td>
<td class="cot0">0,00</td>
<td class="cot0">0,00%</td>
</tr>
</table></div></div></div></div>
<div class="m-pago m-ecot">
<script type="text/javascript" src="http://www.eleconomista.es/js/vticker.js"></script><h3 style="width:298px; padding:5px 0px; margin-left:5px;">
<a href="http://www.eleconomista.es/ecotrader/"><img src="http://s01.s3c.es/imag3/etp/ecotrader-neg3.gif" alt="Ecotrader"></a><span style="float: right; padding-right: 5px; padding-top: 13px;"><a href="http://www.eleconomista.es/mercados-cotizaciones/noticias/2694285/12/10/Ecotraderes-da-un-paso-mas-y-anuncia-ofertas-increibles-por-su-gran-acogida.html" style="font-size: 12px; color: #fff; text-decoration: none;">
			· Vea precios</a></span>
</h3>
<div class="m-ecto-ts" style="background:none;margin:0 10px">
<img src="http://s01.s3c.es/imag3/etp/ico-flecha.png"><a style="color:#666" href="http://www.eleconomista.es/ecotrader/recomendaciones/">Tabla de seguimiento</a>
</div>
<div class="m-ecto-ts" style="padding:1px; margin:0 10px"></div>
<div class="sr-pago">
<h4>Principales noticias:</h4>
<div id="outputV" class="m-ecot-not" style="position: relative; overflow: hidden">
<div class="m-ecot-not-int" style="position: absolute; width: 100%" id="outputV1"></div>
<div class="m-ecot-not-int" style="position: absolute; width: 100%; visibility: hidden;" id="outputV2"></div>
<ul id="list_ul_text" style="display:none;">
<li>
<h5><a href="http://www.eleconomista.es/ecotrader-premium-analisis-tecnico/noticias/6183466/10/14/Vigilamos-a-Bed-Bath-Beyond.html">Vigilamos a Bed Bath & Beyond</a></h5>
<p> </p>
</li>
<li>
<h5><a href="http://www.eleconomista.es/ecotrader-tradings">Apertura alcista en EE.UU</a></h5>
<p>Nueva muestra de fortaleza de las bolsas al otro lado del Atlántico que de un plumazo han borrado las pérdidas vivi…</p>
</li>
<li>
<h5><a href="http://www.eleconomista.es/ecotrader-tradings">Futuros USA en positivo</a></h5>
<p>Los futuros de Estados Unidos a estas horas presentan subidas en torno al 0,70% que apuntan a una apertura al alza.…</p>
</li>
<li>
<h5><a href="http://www.eleconomista.es/ecotrader-premium-analisis-tecnico/noticias/6182476/10/14/Posible-agotamiento-alcista.html">Posible agotamiento alcista</a></h5>
<p> </p>
</li>
<li>
<h5><a href="http://www.eleconomista.es/ecotrader-tradings">Un paso más, aunque se desinflan las ganancias</a></h5>
<p>Las bolsas europeas dan un nuevo paso de cara a favorecer el cierre semanal por encima de los máximos de la semana …</p>
</li>
<li>
<h5><a href="http://www.eleconomista.es/ecotrader-premium-analisis-tecnico/noticias/6181816/10/14/Europac-nueva-oportunidad-de-incorporacion.html">Europac, nueva oportunidad de incorporac…</a></h5>
<p> </p>
</li>
<li>
<h5><a href="http://www.eleconomista.es/ecotrader-tradings">Brusco giro al alza</a></h5>
<p>Brusco giro al alza de las bolsas europeas tras la publicación de los datos PMI de la Eurozona mejor de lo esperado…</p>
</li>
<li>
<h5><a href="http://www.eleconomista.es/ecotrader-tradings">Apertura muy volátil</a></h5>
<p>Apertura bajista de los mercados europeos, aunque las pérdidas se han minimizado en gran medida en los últimos minu…</p>
</li>
<li>
<h5><a href="http://www.eleconomista.es/ecotrader-premium-analisis-tecnico/noticias/6181487/10/14/El-Nikkei-sigue-por-debajo-de-su-primera-resistencia.html">El Nikkei sigue por debajo de su primera…</a></h5>
<p> </p>
</li>
<li>
<h5><a href="http://www.eleconomista.es/ecotrader-renta-variable/noticias/6181411/10/14/Recortamos-exposicion-a-Espana-ahora-que-el-Ibex-ha-alcanzado-resistencias.html">Recortamos exposición a España ahora que…</a></h5>
<p>El Ibex ha conseguido recuperar casi la mitad de toda la caída iniciada en los 11.200 puntos y llega a una zona de …</p>
</li>
<li>
<h5><a href="http://www.eleconomista.es/ecotrader-renta-variable/noticias/6180924/10/14/Se-impone-la-presion-vendedora-en-Wall-Street-se-agotan-los-toros.html">Se impone la presión vendedora en Wall S…</a></h5>
<p>Tras haber recuperado un 61,80/66% de la caída previa, el mercado de renta variable estadounidense ha registrado un…</p>
</li>
<li>
<h5><a href="http://www.eleconomista.es/ecotrader-premium-divisas/noticias/6180896/10/14/Se-confirman-las-sensaciones-bajistas.html">Se confirman las sensaciones bajistas</a></h5>
<p> </p>
</li>
<li>
<h5><a href="http://www.eleconomista.es/ecotrader-premium-renta-fija/noticias/6180893/10/14/Tono-mixto-que-no-cambia-nada.html">Tono mixto que no cambia nada</a></h5>
<p> </p>
</li>
<li>
<h5><a href="http://www.eleconomista.es/ecotrader-renta-variable/noticias/6180892/10/14/Signos-de-agotamiento-comprador-a-corto.html">Signos de agotamiento comprador a corto</a></h5>
<p> </p>
</li>
<li>
<h5><a href="http://www.eleconomista.es/ecotrader-renta-variable/noticias/6180885/10/14/Hasta-aqui-un-rebote-normal.html">Hasta aquí un rebote normal</a></h5>
<p> </p>
</li>
</ul>
<div class="m-ecot-flechas" align="right">
<a href="" onclick="ticker.previousCurrentNews(); return( false );"><img src="http://s01.s3c.es/imag3/iconos/flecha-roja-arriba.gif" alt="Flecha Arriba"></a><a href="" onclick="ticker.nextCurrentNews(); return( false );"><img src="http://s01.s3c.es/imag3/iconos/flecha-roja-abajo.gif" alt="Flecha Aabajo"></a>
</div>
</div>
<h4 style="margin-top: 10px;">Invertir en:</h4>
<div style="text-align: center; font-size: 11px; border-bottom: 1px solid #df0d00; border-top: 1px solid #df0d00; margin: 0 0 10px 0; padding: 2px 0; background: #ff0f00; color: #fff;">
<a href="http://www.eleconomista.es/ecotrader/renta-variable/index.html" style="color: #fff; text-decoration: none;">Renta Variable</a> | <a href="http://www.eleconomista.es/ecotrader/renta-fija/index.html" style="color: #fff; text-decoration: none;">Renta Fija</a> | <a href="http://www.eleconomista.es/ecotrader/divisas/" style="color: #fff; text-decoration: none;">Divisas</a> | <a href="http://www.eleconomista.es/ecotrader/materias-primas/index.html" style="color: #fff; text-decoration: none;">M. Primas</a>
</div>
</div>
<div class="m-ecot-fin">
<a href="http://www.eleconomista.es/ecotrader/publico.php">Conozca Ecotrader</a><a href="http://www.eleconomista.es/ecotrader/publico.php">Dése de alta</a>
</div>
</div><script type="text/javascript">//<![CDATA[
		var ticker = new news_VTicker('outputV', 'list_ul_text', 3000);
	//]]></script>
<div mod="535"><div class="popular">
<script> 
				vMasCotizaciones = new masNoticias('masinteres','tmasinteres');
			</script><div id="pestPopular"><ul class="pest">
<li id="tmasvistas"><a href="#" onClick="javascript:vMasCotizaciones.mostrar('tmasvistas','masvistas');return false;">+ vistos</a></li>
<li id="tmasinteres" class="select"><a href="#" onClick="javascript:vMasCotizaciones.mostrar('tmasinteres','masinteres');return false;">interesa - no interesa</a></li>
</ul></div>
<div id="masvistas" class="mastock"><ul style="width:90%">
<li class="C7"><a href="/empresa/ACCIONA">ACCIONA</a></li>
<li class="C6"><a href="/empresa/ACS">ACS</a></li>
<li class="C6"><a href="/empresa/APPLE-COMPUTER">APPLE</a></li>
<li class="C7"><a href="/empresa/BANKIA">BANKIA</a></li>
<li class="C5"><a href="/empresa/BBVA">BBVA</a></li>
<li class="C7"><a href="/empresa/CAIXABANK">CAIXABANK</a></li>
<li class="C7"><a href="/indice/DAX-30">DAX</a></li>
<li class="C5"><a href="/indice/DOW-JONES">DOW JONES</a></li>
<li class="C6"><a href="/indice/EUROSTOXX-50">EURO STOXX 50®</a></li>
<li class="C7"><a href="/cruce/EURUSD">EURUSD</a></li>
<li class="C6"><a href="/indice/IGBM">
									IGBM
								</a></li>
<li class="C7"><a href="/empresa/IAG-IBERIA">IAG (IBERIA)</a></li>
<li class="C1"><a href="/indice/IBEX-35">IBEX 35</a></li>
<li class="C7"><a href="/empresa/POPULAR">POPULAR</a></li>
<li class="C7"><a href="/empresa/REPSOL">REPSOL</a></li>
<li class="C7"><a href="/indice/S-P-500">S P 500</a></li>
<li class="C7"><a href="/empresa/SABADELL">SABADELL</a></li>
<li class="C7"><a href="/empresa/SACYR">SACYR</a></li>
<li class="C5"><a href="/empresa/SANTANDER">SANTANDER</a></li>
<li class="C6"><a href="/empresa/TELEFONICA">TELEFONICA</a></li>
</ul></div>
<div id="masinteres" class="mastock">
<h5 style="text-align:center">Subidas y caídas en el más vistos:</h5>
<ul>
<li>
<a href="/indice/DOW-JONES">DOW JONES</a><small>(+6 puestos)<br>Sube del 8 al 2</small>
</li>
<li>
<a href="/indice/S-P-500">S P 500</a><small>(+9 puestos)<br>Sube del 24 al 15</small>
</li>
<li>
<a href="/empresa/FERREYROS">FERREYROS</a><small>(+227 puestos)<br>Sube del 362 al 135</small>
</li>
<li>
<a href="/empresa/REPSOL">REPSOL</a><small>(+4 puestos)<br>Sube del 14 al 10</small>
</li>
<li>
<a href="/empresa/Sonda">Sonda</a><small>(+192 puestos)<br>Sube del 358 al 166</small>
</li>
</ul>
<ul>
<li>
<a href="/empresa/SACYR">SACYR</a><small class="rojo">(-7 puestos)<br>Cae del 10 al 17</small>
</li>
<li>
<a href="/empresa/IBERDROLA">IBERDROLA</a><small class="rojo">(-10 puestos)<br>Cae del 18 al 28</small>
</li>
<li>
<a href="/empresa/APPLE-COMPUTER">APPLE</a><small class="rojo">(-4 puestos)<br>Cae del 3 al 7</small>
</li>
<li>
<a href="/empresa/ACCIONA">ACCIONA</a><small class="rojo">(-7 puestos)<br>Cae del 11 al 18</small>
</li>
<li>
<a href="/empresa/ENDESA">ENDESA</a><small class="rojo">(-7 puestos)<br>Cae del 13 al 20</small>
</li>
</ul>
</div>
<script>
				vMasCotizaciones.ocultar('masvistas');
			</script>
</div></div>
<div mod="2687" id="contenedor_galeria_2687">
<script type="text/javascript" language="javascript">
	//<![CDATA[
		//inicial = 1+Math.round(Math.random() * (3-1)) ;
		
		var inicial_2687 = 1+Math.round(Math.random() * 10)%1 ;
		galeria_2687 = new galeria(2687, 1,inicial_2687,0);
	//]]>
	</script><div class="gal1">
<h3>Evasión</h3>
<div id="caja_2687_1">
<div id="imagen_2687_1"><a href="http://www.eleconomista.es/evasion/tendencias/noticias/6177117/10/14/Alquila-The-Highlander-el-yate-de-Malcom-Forbes.html#.Kku8vParM2CQNCR"><img src="http://s01.s3c.es/imag/_v0/310x230/9/7/8/Yate-TheHighlander.jpg" alt="¿Quieres alquilar The Highlander?" width="310" height="230" id="1848270"></a></div>
<div class="gal1-tit">
<h2><a href="http://www.eleconomista.es/evasion/tendencias/noticias/6177117/10/14/Alquila-The-Highlander-el-yate-de-Malcom-Forbes.html#.Kku8vParM2CQNCR">¿Quieres alquilar 'The Highlander'?</a></h2>
<p>Se trata de uno de los yates más emblemáticos.</p>
</div>
</div>
<script type="text/javascript" language="javascript">
			//<![CDATA[
				var arraySize_2687 = new Array() ;
				for( i=1 ; i<=1 ; i++) {
					arraySize_2687[i-1] =$('imagen_2687_'+i).getHeight() ; 
					if ( i!=inicial_2687 )
						galeria_2687.eliminar(i) ;
				}
				galeria_2687.avanzar_horizontal( arraySize_2687 ); 
				
						
			//]]>
			</script>
</div>
</div><div class="sep"></div>

				<div class="sep" style="margin-top: 10px;"></div>
				<div class="popular"><div id="ultimas" class="flash-int-ev">
<h3>El Flash de Evasión</h3>
<ul>
<li style="margin-bottom: 2px;"><h4 style="font-size: 14px;">
<span class="hora">17:28</span><a href="http://www.eleconomista.es/evasion/noticias/6183690/10/14/Isabel-Pantoja-pide-la-suspension-de-la-condena-de-dos-anos-de-prision-y-abona-parte-de-la-multa.html">Isabel Pantoja pide la suspensión de la condena de dos años de prisión y abona parte de la multa</a>
</h4></li>
<li style="margin-bottom: 2px;"><h4 style="font-size: 14px;">
<span class="hora">14:15</span><a href="http://www.eleconomista.es/evasion/noticias/6182861/10/14/Esmeralda-Moya-y-Carlos-Garcia-se-reconciliacion-un-ano-despues.html">Esmeralda Moya y Carlos García se reconciliación un año después</a>
</h4></li>
<li style="margin-bottom: 2px;"><h4 style="font-size: 14px;">
<span class="hora">13:28</span><a href="http://www.eleconomista.es/evasion/moda-y-complementos/noticias/6182547/10/14/Paula-Echevarria-y-su-visita-El-Hormiguero-y-causa-sensacion-y-estupor.html">Paula Echevarría y su visita El Hormiguero y causa sensación y estupor</a>
</h4></li>
<li style="margin-bottom: 2px;"><h4 style="font-size: 14px;">
<span class="hora">12:40</span><a href="http://www.eleconomista.es/evasion/gente-y-estilo/noticias/6182280/10/14/Shakira-y-Pique-muestran-orgullosos-su-barriguita-por-primera-vez.html">Shakira y Piqué muestran orgullosos 'su barriguita' por primera vez</a>
</h4></li>
<li style="margin-bottom: 2px;"><h4 style="font-size: 14px;">
<span class="hora">10:16</span><a href="http://www.eleconomista.es/evasion/gente-y-estilo/noticias/6181718/10/14/La-solidaridad-de-Caritina-Goyanes.html">La solidaridad de Caritina Goyanes</a>
</h4></li>
</ul>
<div class="sep"></div>
<div class="sigue" style="margin: 10px 10px 5px 0;"><a href="/noticias/categoria/noticias/fuente/europapress-chance" class="mas-b">Más noticias</a></div>
</div></div>

		</div>
		<div class="sep"></div><div mod="1502"><div class="cat"><ul>
<li class="d">Categorías:</li>
<li><a href="/noticias/flash">Flash</a></li>
<li><a href="/noticias/economia">Economía</a></li>
<li><a href="/noticias/seleccion-ee">Seleccion eE</a></li>
<li><a href="/noticias/indicadores-espana">Indicadores España</a></li>
<li><a href="/noticias/ultima-hora-urgente">Envio SMS Ultima Hora</a></li>
<li><a href="/noticias/videos">Video Extendido</a></li>
<li class="d">Hemeroteca:</li>
<li><a href="http://www.eleconomista.es/noticias/fuente/el-economista/2014/octubre/2quin">El Economista 16-31 Octubre 2014</a></li>
</ul></div></div>
<div id="publicidad_120" style="position:absolute; width:120px; height:600px; z-index:1000; top: 32px; margin-left: 1000px">
<script type="text/javascript">//<![CDATA[
		sz = "120x600" ;
		st = "eleconomista.es" ;
		std = "http://ad.es.doubleclick.net/" ;
		kw = "";
		 sect ="6181497";
		document.write( '<SCR'+'IPT language="JavaScript" SRC="'+std+'adj/'+st+'/economista_noticias_indicadores_espana;sect='+sect+';tile=7;kw='+kw+';sz='+sz+';ord='+randnum+'?"></SCR'+'IPT>' ) ;
		
		/*
		s.eVar21="120x600";
		*/
		function comprobarTamano()
		{
			if ( (screen.width > 1024) && (document.body.clientWidth > 1144) ) {
				document.getElementById('publicidad_120').style.visibility = 'visible';
			}
			else {
				document.getElementById('publicidad_120').style.visibility = 'hidden';
			}
		}
		
		window.onresize = comprobarTamano ;
		comprobarTamano() ;
		//]]></script><noscript><a href="http://ad.doubleclick.net/jump/eleconomista.es/economista_noticias_indicadores_espana;sect=6181497;tile=7;kw=;sz=120x600;ord=123456789?" target="_blank" rel="nofollow"><img src="http://ad.doubleclick.net/ad/eleconomista.es/economista_noticias_indicadores_espana;sect=6181497;tile=7;kw=;sz=120x600;ord=123456789?" width="120" height="600" border="0"></a></noscript>
</div>
<div class="sep"></div><div id="pie">
<div class="pie">
<div class="p-logo">
<img style="float:left" src="http://s01.s3c.es/imag3/logo-h1.gif"><ul style="width:auto; margin:0; float:right; text-align:left">
<li class="descrip-pie">¿Es usuario de elEconomista.es?</li>
<li style="display:inline"><a href="http://www.eleconomista.es/registro/alta.php">Regístrese aquí</a></li> | <li style="display:inline"><a href="http://www.eleconomista.es/registro/alta.php">Dése de alta</a></li>
</ul>
</div>
<div class="sep" style="margin:0; border:none; border-bottom:1px solid #ccc"></div>
<ul style="margin-top: 20px;">
<li class="descrip-pie"><a class="img-l" href="http://www.eleconomista.es"><img src="http://s01.s3c.es/imag3/logos/america/eees.png" alt="eleconomista.es"></a></li>
<li><a href="http://ecoteuve.eleconomista.es/">Ecoteuve</a></li>
<li><a href="http://ecodata.eleconomista.es">Datos Macro</a></li>
<li><a href="http://ecodiario.eleconomista.es">Información general</a></li>
<li><a href="http://ecoaula.eleconomista.es">Formación y empleo</a></li>
<li><a href="http://ecodiario.eleconomista.es/ecomotor/">Información motor</a></li>
<li><a href="http://www.eleconomista.es/evasion/index.html">Estilo y Tendencias</a></li>
<li><a href="http://www.eleconomista.es/especiales/turismo-viajes/">Turismo y viajes</a></li>
</ul>
<ul style="width:175px; margin-top: 20px;">
<li class="descrip-pie"><a class="img-l" href="http://www.eleconomistaamerica.com"><img src="http://s02.s3c.es/imag3/logos/america/eeam.png" alt="elEconomistaamerica.com"></a></li>
<li><a href="http://www.eleconomistaamerica.com.ar">Argentina</a></li>
<li><a href="http://www.eleconomistaamerica.com.br">Brasil</a></li>
<li><a href="http://www.eleconomistaamerica.cl">Chile</a></li>
<li><a href="http://www.eleconomistaamerica.co">Colombia</a></li>
<li><a href="http://www.eleconomistaamerica.mx">México</a></li>
<li><a href="http://www.eleconomistaamerica.pe">Perú</a></li>
</ul>
<ul>
<li class="descrip-pie">Invierta con eE</li>
<li><a href="http://www.eleconomista.es/ecotrader/">Eco<span style="color:#f00;">trader.es </span></a></li>
<li><a href="http://www.eleconomista.es/monitor"><span style="color: #f60;">el</span>Monitor</a></li>
<li><a href="http://www.eleconomista.es/indice/eco10">Eco 10</a></li>
<li><a href="http://www.eleconomista.es/indice/eco30">Eco 30</a></li>
<li><a href="http://www.eleconomista.es/mercados-cotizaciones/ecodividendo/index.html">Ecodividendo</a></li>
<li style="margin-top: 20px;">
<li class="descrip-pie"><a href="http://www.eleconomista.es/kiosco/" style="font-weight:bold;">Diario y revistas</a></li>
<li><a href="http://www.eleconomista.es/kiosco/">Kiosco</a></li>
<li><a href="http://www.eleconomista.es/kiosco/">Revistas digitales</a></li>
<li><a href="http://www.eleconomista.es/tarifas/">Suscripción al diario</a></li>
<li><a href="http://www.eleconomista.es/elsuperlunes/">elSuperLunes</a></li>
<li><a href="http://www.eleconomista.es/hemeroteca/">Ed. PDF + Hemeroteca</a></li>
<li><a href="http://www.eleconomista.es/ecotablet/">Ecotablet</a></li>
</ul>
<ul style="width:110px">
<li class="descrip-pie">Redes sociales</li>
<li>
<img style="padding:2px 5px 0 0" width="15" height="15" src="http://s01.s3c.es/imag3/iconos/facebook.png"><a href="http://es-es.facebook.com/elEconomista.es">Facebook</a>
</li>
<li>
<img style="padding:2px 5px 0 0" width="15" height="15" src="http://s01.s3c.es/imag3/iconos/twitter.png"><a href="http://twitter.com/eleconomistaes">Twitter</a>
</li>
<li>
<img style="padding:2px 5px 0 0" width="15" height="15" src="http://s02.s3c.es/imag3/iconos/gplus1.png"><a href="https://plus.google.com/+eleconomistaes/">Google+</a>
</li>
<li style="margin-top: 20px;">
<li class="descrip-pie"><a href="http://www.editorialecoprensa.es" style="font-weight:bold;">Editorial Ecoprensa</a></li>
<li><a href="http://www.eleconomista.es/quienes/somos.php">Quiénes somos</a></li>
<li><a href="http://www.eleconomista.es/publicidad/">Publicidad</a></li>
<li><a href="http://www.eleconomista.es/archivo-noticias/index.html">Archivo</a></li>
</ul>
<ul style="width:110px">
<li class="descrip-pie">Servicios</li>
<li><a href="http://www.eleconomista.es/alertas/"> Alertas móvil</a></li>
<li><a href="http://ecodiario.eleconomista.es/cartelera/">Cartelera</a></li>
<li>
<a href="http://www.eleconomista.es/el-tiempo/">El tiempo</a><a href="http://ad.doubleclick.net/clk;248020731;50676649;j?http://www.weatherpro.eu/es/home.html" rel="nofollow"><img src="http://s03.s3c.es/imag3/logos/wpro.png" alt="Weather Pro" style="margin-left: 3px;"></a>
</li>
<li><a href="http://www.eleconomista.es/evasion/libros/">Libros</a></li>
<li><a href="http://listas.eleconomista.es/">Listas</a></li>
<li><a href="http://www.eleconomista.es/rss/index.php"><img style="padding:2px 5px 0 0" width="12" height="12" src="http://s01.s3c.es/imag/rss.gif">RSS</a></li>
<li><a href="http://coleccion.eleconomista.es">Ecoleccionista</a></li>
</ul>
<ul style="width:175px; margin-right:0">
<li class="descrip-pie"><a style="font-weight:bold" href="http://www.eleconomista.es/especiales/index.html">Especiales</a></li>
<li><a href="http://ecodiario.eleconomista.es/deportes/mundial-baloncesto/index.php">Especial Baloncesto 2014</a></li>
<li><a href="http://ecodiario.eleconomista.es/deportes/mundial/index.php">Especial Mundial Brasil 2014</a></li>
<li><a href="http://www.eleconomista.es/especiales/oscars/">Gala de los Oscars 2014</a></li>
<li><a href="http://www.eleconomista.es/especiales/premios-goya/">Premios Goya 2014</a></li>
<li><a href="http://www.eleconomista.es/ecomotor/formula-1/index.html">Fórmula 1</a></li>
<li><a href="http://www.eleconomista.es/especiales/loteria-navidad/">Lotería de Navidad</a></li>
<li><a href="http://www.eleconomista.es/declaracion-renta/">Declaración de la Renta</a></li>
</ul>
<div class="sep" style="border:none; border-top:1px solid #ccc"></div>
<div class="pie-links">
<b style="color: #666;">Nuestros partners:</b><a style="font-weight:bold" href="http://www.eleconomista.es/CanalPDA/"> CanalPDA</a> | <a href="http://www.eleconomista.es/boxoffice/"><span style="color:#ba3b30; font-weight:bold">Boxoffice</span> - Industria del cine</a> | <a style="font-weight:bold" href="http://www.ilsole24ore.com/english">ilSole - English version</a> | 

<a style="font-weight:bold" href="http://empresite.eleconomista.es/" target="_blank">Empresite: España </a> - <a style="font-weight:bold" href="http://empresite.eleconomistaamerica.co/" target="_blank">Colombia </a> | <a style="font-weight:bold" href="http://administradores.eleconomista.es/" target="_blank">Administradores y Ejecutivos </a>
</div>
</div>
<div class="sep" style="border-color:#666; margin:2px 0"></div>
<div class="editorial"><p><a href="http://www.editorialecoprensa.es">Ecoprensa S.A.</a> - Todos los derechos reservados | <a href="http://www.eleconomista.es/politica-de-privacidad/">Nota Legal</a> | <a href="http://www.eleconomista.es/politica-de-privacidad/cookies.php">Política de cookies</a> | <a href="http://www.acens.com" target="_blank" rel="nofollow">Cloud Hosting en Acens</a> |<a href="http://www.paradigmatecnologico.com" rel="nofollow" target="_blank">Powered by Paradigma</a></p></div>
</div>
<div id="barra-nav" class="barra-inf">
<div mod="2086" class="bb">
<script language="javascript">//<![CDATA[
					sz = "550x20" ;
					st = "eleconomista.es" ;
					std = "http://ad.es.doubleclick.net/" ;
					kw = "";
					 sect ="6181497";
					document.write( '<SCR'+'IPT language="JavaScript" SRC="'+std+'adj/'+st+'/economista_noticias_indicadores_espana;sect='+sect+';tile=9;kw='+kw+';sz='+sz+';ord='+randnum+'?"></SCR'+'IPT>' ) ;
					
					
					document.write( '<SCR'+'IPT language="JavaScript" SRC="http://s01.s3c.es/js/eleconomista/cache/dockinbar.js"></SCR'+'IPT>' ) ;
					
	 				/* document.write('<link rel="stylesheet" href="/css3/barra.css" type="text/css"/>'); */
				//]]></script><noscript><a href="http://ad.doubleclick.net/jump/eleconomista.es/economista_noticias_indicadores_espana;sect=6181497;tile=9;kw=;sz=550x20;ord=123456789?" target="_blank" rel="nofollow"><img src="http://ad.doubleclick.net/ad/eleconomista.es/economista_noticias_indicadores_espana;sect=6181497;tile=9;kw=;sz=550x20;ord=123456789?" width="365" height="60" border="0"></a></noscript>
</div>
<ul class="bi">
<li id="ATecnico"><a href="http://www.eleconomista.es/analisis-tecnico" class="barra-enlace">A. Técnico</a></li>
<li id="Cartera"><a href="http://www.eleconomista.es/cartera/portada.php" class="barra-enlace">Su Cartera</a></li>
<li id="UltNoticias">
<a href="#" class="barra-enlace" onClick="return false;">Noticias</a><div class="barra-ventana">
<div class="ventana-cerrar"><img src="http://s01.s3c.es/imag3/iconos/i-cerrar4.gif" class="imagen-cerrar"></div>
<div class="ventana-contenido" id="contUltNoticias"><div class="loading"><p>Cargando Ultimas Noticias...</p></div></div>
</div>
</li>
<li id="Cotizaciones">
<a href="#" class="barra-enlace" onClick="return false;">Cotizaciones</a><div class="barra-ventana">
<div class="ventana-cerrar"><img src="http://s01.s3c.es/imag3/iconos/i-cerrar4.gif" class="imagen-cerrar"></div>
<div class="ventana-contenido">
<div class="ventana-contenido-g" id="contCotizaciones"><div class="loading"><p>Cargando Cotizaciones...</p></div></div>
<div class="ventana-contenido-e">
<a href="/ecotrader/"><span style="color:#000">· Eco</span><span style="color:#FF0000">trader.es</span> herramientas, estrategias de inversión</a><a href="/fichas-de-empresas/index.html">· Vea todas la cotizaciones de empresas</a><a href="/mercados-cotizaciones/index.html">· Todas las noticias de Bolsa y Mercados</a>
</div>
</div>
</div>
</li>
</ul>
</div><script type="text/javascript">
	 
	 			 
			if (/MSIE (\d+\.\d+);/.test(navigator.userAgent))
			{ 
			 var ieversion=new Number(RegExp.$1) // capture x.x portion and store as a number
			 if (ieversion==6)
			  document.getElementById("barra-nav").style.visibility = 'hidden';
			 }
			else if (navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i))
				document.getElementById("barra-nav").style.visibility = 'hidden';
			
	
	</script><script type="text/javascript">

		var UltNoticias = new dockElement('UltNoticias', {'title':'Ultimas Noticias', url_data:"/slices/bar-slice.php?modulo=1246", refreshPeriod:60, refreshDate:'empty'});

//		var Blogs = new dockElement('Blogs', {'title':'BLogs', url_data:"/portada/index.php?ModoAjax=1856,1124&AjaxJS=1", refreshPeriod:9000, refreshDate:'empty'});
		
		//var Blogs = new dockElement('Blogs', {'title':'BLogs', url_data:"/slices/bar-slice.php?modulo=1856", refreshPeriod:9000, refreshDate:'empty'});
		var Cotizaciones = new dockElement("Cotizaciones", {'title':'Cotizaciones', url_data:'/slices/bar-slice.php?modulo=2171', refreshPeriod:60, refreshDate:'empty'});	
//		var elementos = {'UltNoticias':UltNoticias, 'Blogs':Blogs, 'Cotizaciones':Cotizaciones};
		var elementos = {'UltNoticias':UltNoticias, 'Cotizaciones':Cotizaciones};
		dockBar = new dockBar(elementos);
	
		dockBar.setObservers();

</script>

	</div><script type="text/javascript"> var g_cPartnerId = "eleconomista.es-01";var g_cTheme = "eleconomista_1_0";</script><script type="text/javascript" charset="UTF-8" src="http://www.dixio.com/ifaces/dixioforyoursite/jsgic.js"></script>
<script type="text/javascript">// <![CDATA[
var _qq=document.createElement('script'),
_qf = 'Kku8', _qc = [],
_qh = document.getElementsByTagName('script')[0];
_qq.type = 'text/javascript';
_qq.setAttribute('src','http'+
(('https:'==document.location.protocol)?'s':'')+
'://www.intentshare.com/is/?f='+_qf);
_qh.parentNode.insertBefore(_qq, _qh);
// ]]></script>
					<script type="text/javascript">
					//<![CDATA[
						var eco = new ecoQuotes() ;eco.Anadir( "espana" , 0 ) ; eco.Anadir( "francia" , 0 ) ; eco.Anadir( "italia" , 0 ) ; eco.Anadir( "grecia" , 0 ) ; eco.Anadir( "portugal" , 0 ) ; 
						eco.Leer(0);
						eco.Leer(2);
						setInterval( function(){eco.Leer(2);}, 90000 ) ; //90 segundos
						setInterval( function(){eco.Leer(0);}, 90000 ) ; //90 segundos
					//]]>
					</script>
</body></html>