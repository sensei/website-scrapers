<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
					"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
					<html xmlns="http://www.w3.org/199/xhtml" xml:lang="es" xmlns:fb="http://www.facebook.com/2008/fbml">
					<head><title>"El autónomo trabaja mejor porque está en juego su nombre, no la empresa" - elEconomista.es</title>
					<meta property= "og:title" name="title" content="''El autónomo trabaja mejor porque está en juego su nombre, no la empresa'' - elEconomista.es" />

<meta property="og:description" name="description" content="Stephen Philipps está al frente de una empresa &quot;que da el mismo trato a sus clientes que a sus empleados&quot;. En este sentido, han dotado a su personal de las últimas tecnologías, a través de tabletas que les permiten tanto facilitar su tarea como optimizar los procesos de cara a la comodidad de sus clientes. Más noticias en la revista gratuita Gestión Empresarial" />

<meta property="og:image" name="image" content="http://s01.s3c.es/imag/_v0/225x250/8/c/f/h-stephen-phillips.jpg" />

<meta name="image_thumbnail" content="http://s01.s3c.es/imag/_v0/225x250/8/c/f/50x50_h-stephen-phillips.jpg" />
<meta http-equiv='X-UA-Compatible' content='IE=Edge' /><meta name="keywords" content="EMPRESA,NOMBRE,MEJOR,PORQUE,AUTÓNOMO,JUEGO,TRABAJA" />
			<meta name="distribution" content="global" />
			<meta name="resource-type" content="document"/>
			<meta property="og:type" content="article" />
			<meta name="robots" content="all"/>
			<meta name="author" content="elEconomista.es" />
			<meta  name="organization" content="Editorial Ecoprensa S.A." />
			<meta  name="classification" content="World, Espanol, Paises, Espana, Noticias y medios, Periodicos, Economicos" />
			<meta  name="revisit-after" content="7 days"/><meta http-equiv="refresh" content="360" /><meta http-equiv="content-Type" content="text/html; charset=utf-8" /><meta name="date" content="2014-09-24T14:13:17+02:00" />
<link rel="stylesheet" type="text/css" href="http://s02.s3c.es/css3/eleconomista/cache/general,carousel,eM-dcha,etp.408.cache.css" media="screen" />
<link rel="stylesheet" type="text/css" href="http://s01.s3c.es/css3/eleconomista/cache/print.css" media="print" />
<script type='text/javascript' language='javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js'></script>
<script type='text/javascript' src='http://s01.s3c.es/js/cookiesdirective.v1.js' charset='utf-8'></script>
<script type='text/javascript' language='javascript' src='http://ajax.googleapis.com/ajax/libs/prototype/1.6.1/prototype.js'></script>
<script type='text/javascript' language='javascript' src='http://ajax.googleapis.com/ajax/libs/scriptaculous/1.8.3/scriptaculous.js'></script>
<script type='text/javascript' language='javascript' src='http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js'></script>
<script type='text/javascript' language='javascript' src='http://ajax.googleapis.com/ajax/libs/scriptaculous/1.8.3/effects.js'></script>
<script type='text/javascript' language='javascript' src='http://s01.s3c.es/js/eleconomista/cache/c18e2338813cf985712efbd86d0037e2.75.cache.js'></script>
<link href="http://www.eleconomista.es/rss/rss-flash-del-mercado.php" rel="alternate" type="application/rss+xml" title="elEconomista :: Flash del Mercado"/>
<link href="http://www.eleconomista.es/rss/rss-category.php?category=emprendedores-pymes" rel="alternate" type="application/rss+xml" title="elEconomista :: Emprendedores-Pymes"/><meta name="original-source" content="http://www.eleconomista.es/emprendedores-pymes/noticias/6103473/09/14/El-autonomo-trabaja-mejor-porque-esta-en-juego-su-nombre-no-la-empresa.html" /><meta name="cXenseParse:title" content="El autónomo trabaja mejor porque está en juego su nombre, no la empresa" /><meta name="cXenseParse:recs:articleid" content="6103473"><link rel="next" href="http://www.eleconomista.es/emprendedores-pymes/noticias/6103473/09/14/2/El-autonomo-trabaja-mejor-porque-esta-en-juego-su-nombre-no-la-empresa.html" /><script type="text/javascript" src="//apis.google.com/js/plusone.js">{lang: 'es'}</script></head>

	<body id="emprendedores-pymes" s="1"><div class="principal-cab">
<script>
  var _comscore = _comscore || [];
  _comscore.push({ c1: "2", c2: "7107810" });
  (function() {
    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
    s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
    el.parentNode.insertBefore(s, el);
  })();
</script><noscript><img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=7107810&amp;cv=2.0&amp;cj=1"></noscript>
<script language="JavaScript" type="text/javascript" src="http://s01.s3c.es/js/s_code.2014.pro.v2.js"></script><script language="JavaScript" type="text/javascript">//<![CDATA[
		s.pageName="";
		s.channel="Red ElEconomista";
		s.prop1="elEconomista.es";
		s.prop2="elEconomista.es";
		s.prop6="El Economista";s.prop8="elEconomista.es";s.prop9="eleconomista.es";

		s.campaign="";
		s.eVar1="";
		s.eVar1="";
		s.eVar2="";
		s.eVar3="";
		s.eVar4="";
		s.eVar5="";
		var s_code=s.t();if(s_code)document.write(s_code)
	//]]></script><script language="JavaScript" type="text/javascript"></script><noscript><img src="http://eleconomista.d1.sc.omtrdc.net/b/ss/eleconomista/1/H.25.4--NS/0" height="1" width="1" border="0" alt=""></noscript>
<script type="text/javascript" src="//secure-uk.imrworldwide.com/v60.js"></script><script type="text/javascript">
		 	var pvar = { cid: "es-eleconomista", content: "0", server: "secure-uk" };
		 	var trac = nol_t(pvar);
		 	 trac.record().post();
		</script><noscript><div><img src="//secure-uk.imrworldwide.com/cgi-bin/m?ci=es-eleconomista&amp;cg=0&amp;cc=1&amp;ts=noscript" width="1" height="1" alt=""></div></noscript>
<script language="javascript">//<![CDATA[
		var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
		document.write(unescape("%3Cscript src=\'" + gaJsHost + "google-analytics.com/ga.js\' type=\'text/javascript\'%3E%3C/script%3E"));
	//]]></script><script language="javascript">//<![CDATA[
		var pageTracker = _gat._getTracker('UA-1010908-1');	
		pageTracker._initData();
		pageTracker._trackPageview();

//]]></script><div id="banners-superiores_1">
<script type="text/javascript" language="javascript">//<![CDATA[
		var time = new Date() ;
		randnum= (time.getTime()) ;
	//]]></script><script language="javascript">//<![CDATA[
		sz = "1x1" ;
		st = "eleconomista.es" ;
		std = "http://ad.es.doubleclick.net/" ;
		isnt = "";
		sect="6103473";
		document.write( '<SCR'+'IPT language="JavaScript" SRC="'+std+'adj/'+st+'/economista_noticias_emprendedores_pymes;tile=1;dcopt=ist;sz='+sz+';isnt='+isnt+';ord='+randnum+'?"></SCR'+'IPT>' ) ;
	//]]></script><noscript><a href="http://ad.doubleclick.net/jump/eleconomista.es/economista_noticias_emprendedores_pymes;tile=1;sz=1x1;sect=6103473;ord=123456789?" target="_blank" rel="nofollow"><img src="http://ad.doubleclick.net/ad/eleconomista.es/economista_noticias_emprendedores_pymes;sz=1x1;sect=6103473;ord=123456789?" width="1" height="1" border="0"></a></noscript>
</div>
<div class="red3">
<div class="menu3" style="width:auto;"><ul>
<li class="sel"><a href="http://www.eleconomista.es">Portada</a></li>
<li class="ncab-ed"><a href="http://ecodiario.eleconomista.es">Eco<span style="color: #d5b036;">Diario</span></a></li>
<li class="ncab-etv" style="padding-top: 0; height: 30px;"><a href="http://ecoteuve.eleconomista.es/"><img src="http://s04.s3c.es/imag3/ecotv/etv-peq.png"></a></li>
<li class="ncab-em"><a href="http://www.eleconomista.es/ecomotor">Eco<span style="color: #0079c8;">Motor</span></a></li>
<li class="ncab-ea"><a href="http://ecoaula.eleconomista.es">Eco<span style="color: #f60;">Aula</span></a></li>
<li class="ncab-el"><a href="http://www.eleconomista.es/ecoley">Eco<span style="color: #9c6;">ley</span></a></li>
<li class="ncab-el"><a href="http://www.eleconomista.es/evasion">Evasión</a></li>
<li class="ncab-et"><a href="http://www.eleconomista.es/ecotrader">Eco<span style="color: #f00;">trader</span></a></li>
<li class="ncab-en"><a href="http://www.eleconomista.es/monitor"><span style="color: #f60;">el</span>Monitor</a></li>
<li onclick="window.location.href='http://www.eleconomista.es/pymes'" class="ncab-en patro-logo-py"><a href="http://www.eleconomista.es/pymes"><span>Eco</span><span style="color: #2A92BE;">pymes</span></a></li>
<li class="menu_right li-ame" style="z-index: 2300;">
<a class="drop desp-ame" href="http://www.eleconomistaamerica.com/" target="_blank">América</a><div class="dropdown_1column align_right" style="top:27px; z-index: 3000;">
<div class="col_1 new ">
<h2>Países</h2>
<ul class="greybox"><li><a href="http://www.eleconomistaamerica.com">Portada</a></li></ul>
<ul>
<li><a href="http://www.eleconomistaamerica.com.ar/"><img style="vertical-align:middle; margin-right:3px;" src="http://s01.s3c.es/imag3/latam/f-argentina.png">Argentina</a></li>
<li><a href="http://www.eleconomistaamerica.com.br/"><img style="vertical-align:middle; margin-right:3px;" src="http://s01.s3c.es/imag3/latam/f-brasil.png">Brasil</a></li>
<li><a href="http://www.eleconomistaamerica.cl/"><img style="vertical-align:middle; margin-right:3px;" src="http://s01.s3c.es/imag3/latam/f-chile.png">Chile</a></li>
<li><a href="http://www.eleconomistaamerica.co/"><img style="vertical-align:middle; margin-right:3px;" src="http://s01.s3c.es/imag3/latam/f-colombia.png">Colombia</a></li>
<li><a href="http://www.economiahoy.mx/"><img style="vertical-align:middle; margin-right:3px;" src="http://s01.s3c.es/imag3/latam/f-mexico.png">México</a></li>
<li><a href="http://www.eleconomistaamerica.pe/"><img style="vertical-align:middle; margin-right:3px;" src="http://s01.s3c.es/imag3/latam/f-peru.png">Perú</a></li>
</ul>
</div>
<div class="col_1 new black_box" style="margin-top: 10px; width: 120px;">
<h2 style="margin-bottom: 5px; padding-bottom: 5px; font-size: 16px;">Materias Primas</h2>
<ul class="dd-tt-s">
<li><a href="http://www.eleconomistaamerica.com/materia-prima/Crudo-wti">Crudo</a></li>
<li><a href="http://www.eleconomistaamerica.com/materia-prima/oro">Oro</a></li>
<li><a href="http://www.eleconomistaamerica.com/materia-prima/soja">Soja</a></li>
<li><a href="http://www.eleconomistaamerica.com/materias-primas/">Ver más</a></li>
</ul>
<h2 style="margin-bottom: 5px; padding-bottom: 5px; font-size: 16px;">Índices</h2>
<ul class="dd-tt-s">
<li><a href="http://www.economiahoy.mx/indice/IPC-MEXICO">IPC México</a></li>
<li><a href="http://www.eleconomistaamerica.com/indice/COL20">COL 20</a></li>
<li><a href="http://www.eleconomistaamerica.com/indice/BOVESPA">BOVESPA</a></li>
<li><a href="http://www.eleconomistaamerica.com/indices-latinoamericanos/index.html">Ver más</a></li>
</ul>
</div>
</div>
</li>
</ul></div>
<div class="cont2" style="width: 250px; float: right; margin-top: 0px;"><div id="login-cont">
<div id="login-superior" class="topnav"> ¿Usuario de elEconomista? <a id="login-boton" class="login-boton"><span>Conéctate</span></a>
</div>
<fieldset id="login-menu" class="login-menu">
<div class="field1"><form method="post" id="signin" onsubmit="Captura_mouseClick_enviar();">
<p id="mensaje-error"></p>
<p><label for="username">Dirección de email</label><input id="username" name="username" value="" title="username" tabindex="4" type="text" autocorrect="off" autocapitalize="off"></p>
<p><label for="password">Contraseña</label><input id="password" name="password" value="" title="password" tabindex="5" type="password"></p>
<p class="login-recordar"><span id="boton_registro"><input id="login-enviar" value="Entrar" tabindex="6" type="submit"></span><input id="remember" name="remember_me" value="1" tabindex="7" type="checkbox" CHECKED><label for="remember">Recordarme</label></p>
<p class="login-enlace"><a href="http://www.eleconomista.es/registro/perfil.php" id="resend_password_link">¿Olvidaste tu contraseña?</a></p>
<p class="login-enlace"><a href="http://www.eleconomista.es/registro/alta.php" id="resend_password_link">Registrarse</a></p>
</form></div>
<div class="field2">
<h4 style="color: #f60;">Servicios Premium</h4>
<p class="login-enlace"><a style="color:#000!important;" href="/ecotrader" id="resend_password_link">Eco<span style="color:#f00;">trader</span></a></p>
<p class="login-enlace"><a href="http://www.eleconomista.es/hemeroteca" id="resend_password_link">Edición PDF + Hemeroteca</a></p>
<p class="forgot"><a href="http://www.eleconomista.es/elsuperlunes" id="resend_password_link">El Superlunes</a></p>
<h4 style="margin-top: 15px; color: #f60;">Servicios gratuitos</h4>
<p class="login-enlace"><a href="http://listas.eleconomista.es" id="resend_password_link">Listas y rankings</a></p>
<p class="login-enlace"><a href="http://www.eleconomista.es/cartera/portada.php" id="resend_password_link">Cartera</a></p>
<p class="login-enlace"><a href="http://www.eleconomista.es/foros" id="resend_password_link">Foros</a></p>
</div>
</fieldset>
<fieldset id="login-menu2" class="login-menu" style="display: none">
<div class="field1">
<h4>Zona usuario</h4>
<p class="login-enlace"><a href="http://www.eleconomista.es/registro/datosPersonales.php" id="resend_password_link">Datos personales</a></p>
<p class="login-enlace"><a href="http://www.eleconomista.es/registro/perfil.php" id="resend_password_link">Editar perfil</a></p>
<p class="login-enlace"><a href="http://www.eleconomista.es/premium/mediospago.php" id="resend_password_link">Medios de pago</a></p>
<p class="login-enlace"><a href="http://www.eleconomista.es/premium/facturas.php" id="resend_password_link">Facturas</a></p>
<p class="login-enlace"><a href="http://www.eleconomista.es/premium/suscripciones.php" id="resend_password_link">Servicios Premium</a></p>
<form method="post" id="signin2"><p class="remember"><span id="boton_desconectar"><input id="signin_submit" class="desconexion" value="Desconectarme" tabindex="6" type="button"></span></p></form>
</div>
<div class="field2">
<h4 style="color: #f60;">Servicios Premium</h4>
<p class="forgot"><a href="http://www.eleconomista.es/ecotrader" id="resend_password_link">Ecotrader</a></p>
<p class="forgot"><a href="http://www.eleconomista.es/hemeroteca" id="resend_password_link">Edición PDF + Hemeroteca</a></p>
<p class="forgot"><a href="http://www.eleconomista.es/elsuperlunes" id="resend_password_link">El Superlunes</a></p>
<h4 style="margin-top: 15px; color: #f60;">Servicios gratuitos</h4>
<p class="forgot"><a href="http://listas.eleconomista.es" id="resend_password_link">Listas y rankings</a></p>
<p class="forgot"><a href="http://www.eleconomista.es/cartera/portada.php" id="resend_password_link">Cartera</a></p>
<p class="forgot"><a href="http://www.eleconomista.es/foros" id="resend_password_link">Foros</a></p>
</div>
</fieldset>
</div></div>
<div style="clear: both;display: block; margin: 10px 0;"></div>
</div>
<script type="text/javascript">//<![CDATA[
		if ( getCookie("Navegacion_Web") ) {
			var content = '<li><a href="#" onclick="return soloIPhone() ;">Volver a la version iPhone</a></li>' ;
			
			$("menuLink").insert( {bottom: content} ) ; 
		}
		
		function soloIPhone() {
			deleteCookie("Navegacion_Web", "/", "http://www.eleconomista.es") ;
			location = "http://iphone.eleconomista.mobi"
		}
		//]]></script>
</div>
<style>
            #localizacion_aviso_predefinido{
                padding: 10px;
                border-right: 1px solid #B9B7AC;
                width: 970px;
                margin: 0 auto;
            }
            .el-ed{
                padding: 10px;
                border: 1px solid #096490;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                background-image: linear-gradient(bottom, rgb(12,145,206) 42%, rgb(30,132,179) 71%, rgb(31,158,217) 86%);
                background-image: -o-linear-gradient(bottom, rgb(12,145,206) 42%, rgb(30,132,179) 71%, rgb(31,158,217) 86%);
                background-image: -moz-linear-gradient(bottom, rgb(12,145,206) 42%, rgb(30,132,179) 71%, rgb(31,158,217) 86%);
                background-image: -webkit-linear-gradient(bottom, rgb(12,145,206) 42%, rgb(30,132,179) 71%, rgb(31,158,217) 86%);
                background-image: -ms-linear-gradient(bottom, rgb(12,145,206) 42%, rgb(30,132,179) 71%, rgb(31,158,217) 86%);
                background-image: -webkit-gradient(
                linear,
                left bottom,
                left top,
                color-stop(0.42, rgb(12,145,206)),
                color-stop(0.71, rgb(30,132,179)),
                color-stop(0.86, rgb(31,158,217))
                );
                
                #background-color:rgb(12,145,206);
                #width:948px;
                /* For Internet Explorer 5.5 - 7 */
                filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#1f9ed9, endColorstr=#1e84b3);
                
                /* For Internet Explorer 8 */
                -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#1f9ed9, endColorstr=#1e84b3)";
                padding-bottom:5px\9;
            }
            
            .el-ed h4 {
                margin: 0;
                font-size: 13px;
                color: #fff;
                font-weight:bold;
            }
            .el-ed h4 a:link, .el-ed h4 a:active, .el-ed h4 a:visited {
                color: #fff;
                text-decoration: underline;
            }
            .cierre {
                float: right;
                margin-top: -20px;
            }
            
            .el-ed-flecha {
                position: absolute;
                top: 30px;
                left: 100px;
            }
            
            .edicion {
                color: #333;
                font-size: 12px;
                margin-top: 10px;
            }
            
			.eleccion-pais { 
				/*background: #C34E00; */
				margin: 5px 5px 8px 0; 
				padding: 15px 10px; 
				color: #fff; 
				font-size: 16px;
				overflow: hidden;
				/*background: #c00;*/
				background:#F60;
				font-family: 'Trebuchet MS', Helvetica, sans-serif;
            }
            .eleccion-pais a:link, .eleccion-pais a:active, .eleccion-pais a:visited {
                color: #fff;
                text-decoration:none;
            }
            .eleccion-pais a:hover { text-decoration: underline;}
            .eleccion-pais-cerrar { float: right;}
            .eleccion-pais img{ margin-right:5px; vertical-align: baseline; opacity: 0.8;}
            .eleccion-pais b { /*color: #f60;*/ text-transform:uppercase;}
            .eleccion-indice { 
				font-size: 12px; 
				color:#333;
				/*margin-left: 20px;*/ 
				margin-top: 10px; 
				height:23px;
				padding-top: 10px; 
				padding-left: 5px;
				background: #ffe0cc;
            }
            .eleccion-indice span { /*background: #0072bf; padding: 4px;*/ margin-right: 5px; text-transform: uppercase; color: #333; font-weight:bold;}
            .eleccion-indice a { color:#333; cursor:pointer;}
            .eleccion-indice a:hover { text-decoration:none;}
            .eleccion-indice a span{ color: #0c0; font-weight:normal;}
            .eleccion-indice a span.accion1{ color: #0c0;}
            .eleccion-indice a span.accion-1{ color: #f00;}
            .eleccion-indice a span.accion-1{ color: #f00;}
            .eleccion-indice a.cotizacion{ color: #000;}
        </style><div class="row" mod="4379">
<div id="localizacion_aviso_predefinido" style="display:none;"><div class="eleccion-pais">
<a href="#" onclick="redirigirASite();"><img src="http://s01.s3c.es/imag3/flechas-hem/final_d_org.gif"><span id="localizacion_aviso_pais"></span></a><div class="eleccion-pais-cerrar"><a href="#" onclick="jQuery('#localizacion_aviso_predefinido').hide();">X</a></div>
<div class="eleccion-indice" id="localizacion_aviso_cotizaciones"></div>
</div></div>
<script language="javascript">//<![CDATA[
                var siteprincipal = '';
                var siteprincipal=getCookie("siteprincipal");
                var siteprincipalConsulta=getCookie("siteprincipalConsulta");
                var siteprincipalRecomendado = getCookie("siteprincipalRecomendado");
                var modulosSite = new Array();
                var modulosReemplazados = new Array();
                
                jQuery(window).load(function(){
                    if((siteprincipal==null || siteprincipal=='' ) || ( siteprincipalConsulta==1 && siteprincipalRecomendado>1 )){
                        //Petición de datos
                        var ajax_data = {};
                        
                        var ajaxRequest = new Ajax.Request(
                        '/iplocalizacion/iplocalizacion.php',
                        {
                            method: 'post',
                            parameters: ajax_data,
                            //asynchronous: true,
                            onComplete: function(xmlHttpRequest, responseHeader){
                                if(xmlHttpRequest.responseJSON != null ){
                                    oRespuesta = xmlHttpRequest.responseJSON;
                                    if(oRespuesta.resultado == 1){
                                        siteprincipal = oRespuesta.datos.site_local;
										jQuery('#localizacion_aviso_pais').html(oRespuesta.datos.msg);
										jQuery('#localizacion_avisoclick_pais').html(oRespuesta.datos.msgclick);
                                        mostrarCotizacionesSiteAsociado(oRespuesta.datos.cotizaciones, '');
                                        //if(typeof(siteprincipalRecomendado) == "undefined") {
                                            setCookie("siteprincipalRecomendado", siteprincipal, 1,"/",".eleconomista.es");
                                            siteprincipalRecomendado = siteprincipal;
                                        //}
                                        jQuery('#localizacion_aviso_predefinido').show();
                                        //if(siteprincipal!='')alert(siteprincipal);
                                        recargarModulosPorLocalizacion();
                                    }else{
                                        //No hay site asociado a esa localizacion  
                                        setCookie("siteprincipalConsulta", 1,1,"/",".eleconomista.es");
                                        setCookie("siteprincipalRecomendado", 1,1,"/",".eleconomista.es");
                                    }
                                }else{
                                    //
                                }
                            }
                            
                        });
                    }else{
                        //Site por defecto definido. Dar opción a su eliminación
                        //alert('Definido - ' + siteprincipal);
                    }
                    
                });
                
                function recargarModulosPorLocalizacion(){
                    //console.log("prueba");
                    /* */
                    var ajaxRequest2 = new Ajax.Request(
                    "/iplocalizacion/noticiasSite.php"
                    ,{
                        method: "get"
                        , parameters: {'site': siteprincipalRecomendado, 'Modulos': modulosSite.join(",")}
                        , onComplete: function(xmlHttpRequest, responseHeader){
                            if(xmlHttpRequest.responseJSON != null ){
                                oRespuesta = xmlHttpRequest.responseJSON;
                                if(oRespuesta.resultado == 1){
                                    //captura el listado de datos
                                    for(k=0;k<oRespuesta.datos.length;k++){
                                        //alert("change-"+ k);
                                        if(modulosSite.indexOf(oRespuesta.datos[k].mod+"")>-1){
                                            moduloAReemplazar = modulosReemplazados[modulosSite.indexOf(oRespuesta.datos[k].mod+"")];
                                            //jQuery("#prueba_" + moduloAReemplazar ).html(oRespuesta.datos[k].codigoHTML);
                                            jQuery('div[mod="' + moduloAReemplazar +'"]' ).html(oRespuesta.datos[k].codigoHTML);
                                        }
                                    }
                                }else{
                                    
                                }
                            }else{
                                //
                            }
                        }
                        
                    }
                    );
                }
                
                function establecerSitePrincipal(){
                    if(siteprincipal!='') {setCookie("siteprincipal", siteprincipal,365,"/",".eleconomista.es");jQuery('#localizacion_aviso_predefinido').hide();}
                    else{setCookie("siteprincipal", '',-1,"/",".eleconomista.es");}
                }
                
                function mostrarCotizacionesSiteAsociado(aCotizaciones, sEnlace){
                    if(sEnlace=='') sEnlace ='http://www.eleconomistaamerica.com/indices-latinoamericanos/index.html';
                    var sHTML = '';
                    for(i=0;i<aCotizaciones.length;i++){
                        //sHTML += ((sHTML!='')? ',':'');
                        sHTML += '<a href="' + aCotizaciones[i].CotizacionURL + '" class="cotizacion">' + aCotizaciones[i].CotizacionNombre + ' <span class="ultimo_' + aCotizaciones[i].CotizacionId + '" table="mod_geo" item="item_' + aCotizaciones[i].CotizacionCodigoFinInfo + '_' + aCotizaciones[i].CotizacionQuality + '" field="precio_ultima_cotizacion" source="lightstreamer">' + aCotizaciones[i].CotizacionValor + ' </span><span class="ico_' + aCotizaciones[i].CotizacionId + '" table="mod_geo" item="item_' + aCotizaciones[i].CotizacionId + '_' + aCotizaciones[i].CotizacionId + '" field="#arrow" source="lighstreamer"><img height="11" width="11" src="' + aCotizaciones[i].CotizacionIcono + '"/></span><span class="accion' + aCotizaciones[i].CotizacionEstado + ' pDif_' + aCotizaciones[i].CotizacionId + ' estado_' + aCotizaciones[i].CotizacionId + '"  acc="' + aCotizaciones[i].CotizacionEstado + '" table="mod_geo" item="item_' + aCotizaciones[i].CotizacionCodigoFinInfo + '_' + aCotizaciones[i].CotizacionQuality + '" field="variacion_porcentual" source="lightstreamer">' + aCotizaciones[i].CotizacionVariacion + '</span></a> | ';
                    }
                    sHTML = '<span>Cotizaciones:</span>' + sHTML + '<a href="' + sEnlace + '" class="cotizacion">Ver todas</a>';
                    jQuery('#localizacion_aviso_cotizaciones').html(sHTML);
                    //Activación del lightstreamer
                    /*
					var mod_geo = new OverwriteTable( null, null, "MERGE" ) ;
                    mod_geo.setSnapshotRequired( true ) ;
                    mod_geo.setRequestedMaxFrequency( 1.0 ) ;
                    mod_geo.onItemUpdate = updateItem ;
                    mod_geo.setDataAdapter("FINFEED");
                    mod_geo.onChangingValues = formatValues ;
                    mod_geo.setPushedHtmlEnabled( true ) ;
                    pushPage.addTable( mod_geo, "mod_geo" ) ;
                    var decimales = new Hash() ;
                    for(i=0;i<aCotizaciones.length;i++){
                        decimales.set('item_' + aCotizaciones[i].CotizacionId + '_' + aCotizaciones[i].CotizacionId , 2) ;
                    }
                    mod_geo.decimales = decimales ;
					*/
                }
                
                
    function redirigirASite(){
        if(siteprincipal==21){ window.location.replace('http://www.eleconomistaamerica.co/');}
        if(siteprincipal==22){ window.location.replace('http://www.eleconomistaamerica.mx/');}
        if(siteprincipal==23){ window.location.replace('http://www.eleconomistaamerica.cl/');}
        if(siteprincipal==24){ window.location.replace('http://www.eleconomistaamerica.com.br/');}
        if(siteprincipal==25){ window.location.replace('http://www.eleconomistaamerica.com.ar/');}
        if(siteprincipal==26){ window.location.replace('http://www.eleconomistaamerica.pe/');}
    }
    //]]></script>
</div>

	<div class="principal"><div class="cabecera">
<div class="logo">
<a href="/"><img src="http://s01.s3c.es/imag3/logo-h1.gif" alt="elEconomista.es"></a><div class="lead izda"><span class="l-fecha">Miércoles, 24 de Septiembre de 2014 <span class="actualizado"> Actualizado a las 14:13</span></span></div>
</div>
<div class="ruta"><a href="/noticias/emprendedores-pymes">Emprendedores-Pymes</a></div>
<div class="rutap"><a href="http://www.bancopopular.es/popular-web/empresas" target="_blank" rel="nofollow"><img src="http://s01.s3c.es/imag3/logos/logo-banco-popular-cuadrado.png"></a></div>
<div class="buscador buscador-int"><form method="GET" action="/buscador/resultados.php" name="buscador" style="margin: 0px; padding: 0px; color:#fff" accept-charset="UTF-8">
<input type="hidden" value="partner-pub-2866707026894500:3x0zyny7y13" name="cx"><input type="hidden" value="FORID:10" name="cof"><input type="hidden" value="UTF-8" name="ie"><input type="hidden" value="0" name="recordatorio" style="margin: 0pt 3px 3px; padding: 0pt;"><input type="hidden" value="0" name="pagina" style="margin: 0pt 3px 3px; padding: 0pt;"><input type="text" id="texto_formulario" name="fondo" placeholder="Noticias, acciones..."><input type="submit" value="Buscar" name="Submit" class="boton-buscar">
</form></div>
<div style="background-color:#2a92be; height:40px; clear:both; padding:20px 0px 10px 20px;">
<h1 style=" color:#FFF; font-size:30px; font-family: 'Palatino Linotype', 'Book Antiqua', Palatino, serif; float:left;"><a href="/pymes/" style="color: #fff;">Emprendedores y Pymes</a></h1>
<div style="float:right; color: #fff; font-size: 11px; width: 520px;">
<span style="padding: 0 10px 10px 0;">Redes para emprendedores:</span><iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.eleconomista.es%2Fpymes&amp;send=false&amp;layout=button_count&amp;width=450&amp;show_faces=false&amp;font&amp;colorscheme=light&amp;action=like&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:121px; height:21px;" allowTransparency="true"></iframe><div class="g-plusone" data-size="medium"></div>
<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.eleconomista.es/pymes" data-lang="es">Twittear</a><script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</div>
</div>
<div id="sub_menu" class="submenu-desp" style="margin-bottom:10px; background-color:#2a92be; border-top: 2px solid #1B6FA1;"><ul id="s-emp-menu" class="submenu_emp sel">
<li id="actu2-menu" class="emp_emp" style="background:url('http://s01.s3c.es/imag3/cabecero2/ed-flecha-emprendedores-azul.gif') no-repeat right top #b6b6b6 !important; border-left:#8c8a8a 1px solid; border-right:none !important; padding-right:30px;"><a href="/pymes/" class="emp_emp"> Emprendedores </a></li>
<li id="home-menu" class="sel" style="border-left:#12698e 1px solid; border-right:#2192c2 1px solid;"><a href="/pymes/">Portada</a></li>
<li id="nove-menu" style="border-left:#12698e 1px solid; border-right:#2192c2 1px solid;"><a href="/pymes/pymes/">Pymes</a></li>
<li id="prue-menu" style="border-left:#12698e 1px solid; border-right:#2192c2 1px solid;"><a href="/pymes/franquicias/">Franquicias</a></li>
<li id="prue-menu" style="border-left:#12698e 1px solid; border-right:#2192c2 1px solid;"><a href="/pymes/innova/">Innova</a></li>
<li id="prue-menu" style="border-left:#12698e 1px solid; border-right:#2192c2 1px solid;"><a href="/pymes/ferias/">Ferias</a></li>
<li id="comp-menu" style="border-left:#12698e 1px solid; border-right:#2192c2 1px solid;"><a href="/pymes/ayuda/">Ayudas</a></li>
<div style="border:0; float:right; width:167px;">
<p style="font-style:italic; color:#FFF; font-size:11px;width: 75px; float: left; margin-top:5px;">Ofrecido por:</p>
<a target="_blank" href="http://empresa.lacaixa.es/home/empresas_es.html"><img style="margin-top: 2px;" src="http://s01.s3c.es/imag3/logos/caixa/caixa2-logo88x22.png" alt="Caixa"></a>
</div>
</ul></div>
<div id="banners-superiores"><div class="b928">
<script language="javascript">//<![CDATA[
		sz = "728x90" ;
		st = "eleconomista.es" ;
		std = "http://ad.es.doubleclick.net/" ;
		kw = "";
		sect="6103473";
		document.write( '<SCR'+'IPT language="JavaScript" SRC="'+std+'adj/'+st+'/economista_noticias_emprendedores_pymes;sect='+sect+';tile=3;kw='+kw+';sz='+sz+';ord='+randnum+'?"></SCR'+'IPT>' ) ;
	//]]></script><noscript><a href="http://ad.doubleclick.net/jump/eleconomista.es/economista_noticias_emprendedores_pymes;sect=6103473;tile=3;kw=;sz=728x90;ord=123456789?" target="_blank" rel="nofollow"><img src="http://ad.doubleclick.net/ad/eleconomista.es/economista_noticias_emprendedores_pymes;sect=6103473;tile=3;kw=;sz=728x90;ord=123456789?" width="728" height="90" border="0"></a></noscript>
</div></div>
<div id="mis_favoritos_reducido" class="cot-home cot-indices"></div>
<script type="text/javascript">
	//<![CDATA[
		miLista = new miListaValores( 'l' ) ;
	
						
	//]]>
	</script><img style="display:none" src="http://s01.s3c.es/imag3/quotes/balloon-0.png"><img style="display:none" src="http://s01.s3c.es/imag3/quotes/balloon-1.png"><img style="display:none" src="http://s01.s3c.es/imag3/quotes/balloon-2.png"><img style="display:none" src="http://s01.s3c.es/imag3/quotes/balloon-3.png"><img style="display:none" src="http://s01.s3c.es/imag3/quotes/balloon.png"><img style="display:none" src="http://s01.s3c.es/imag3/si.png"><img style="display:none" src="http://s01.s3c.es/imag3/no.png"><img style="display:none" src="http://s01.s3c.es/imag3/twitter/button.png"><script type="text/javascript">
 
 //<![CDATA[
 	
 	var mivariable='login-menu';
 	var configuracioninicial=1;
	var cookieUsuario = getCookie('USU') ;
	var cookiePassword = getCookie('PASS') ;
	var delay = 10000;
	var secs = 0;
 	
	if ( (cookieUsuario != null) && (cookieUsuario != '') && (cookiePassword != null) && (cookiePassword != '') ) {
		Carga_Pagina(); /* incluido en cajaLogin.js*/
	}
 
  /* incluido en cajaLogin.js*/
 	 	    
	   	Event.observe($('signin_submit'),'click', Captura_mouseClick_desconectar);
	   	Event.observe($('login-superior'),'click', Captura_mouseClick);
	   	Event.observe($('login-enviar'),'click', Captura_mouseClick_enviar);
	   	Event.observe($('login-superior'),'mouseup', Captura_mouseUp);
		Event.observe($('login-menu'),'mouseup', Captura_mouseUp);
		Event.observe($('login-menu2'),'mouseup', Captura_mouseUp);
        Event.observe(window, 'mouseup', Captura_mouseUpGeneral);
   //]]>
   
   
</script>
</div>
<div id="webslice_ultima_hora" class="hslice entry-content">
<a style="display: none;" rel="entry-content" href="/slices/web-slice.php?modulo=392"></a><span class="entry-title" style="display: none;">Última hora en elEconomista.es</span><span class="ttl" style="display: none;">15</span><div mod="392"></div>
</div>
<div id="webslice_ultima_hora" class="hslice entry-content">
<a style="display: none;" rel="entry-content" href="/slices/web-slice.php?modulo=392"></a><span class="entry-title" style="display: none;">Última hora en elEconomista.es</span><span class="ttl" style="display: none;">15</span><div mod="3562">
<script type="text/javascript" src="http://s01.s3c.es/js/compressed/ticker.js"></script><div class="urgente aviso urgente3">
<small>En EcoDiario.es</small><h5 id="output_3562"><ul id="list_ul_text_3562">
<li><a href="http://ecodiario.eleconomista.es/espana/noticias/6236160/11/14/Rajoy-comparecera-manana-para-hacer-una-declaracion-sobre-el-9N.html">Rajoy comparece hoy en Moncloa, tres días después de la pseudoconsulta</a></li>
<li><a href="http://ecodiario.eleconomista.es/politica/noticias/6236531/11/14/Manos-Limpias-denuncia-a-los-negociaciadores-secretos-del-9N-Pedro-Arriola-y-Jose-Enrique-Serrano-y-Joan-Rigol.html">Manos Limpias denuncia al asesor de Rajoy, Pedro Arriola</a></li>
<li><a href="http://ecodiario.eleconomista.es/futbol/noticias/6236452/11/14/El-Bara-molesto-con-el-reencuentro-de-Pique-con-Guardiola.html">El Barça, molesto con el reciente reencuentro entre Piqué y Guardiola</a></li>
<li><a href="http://ecodiario.eleconomista.es/noticias/noticias/6236304/11/14/La-Fiscalia-querella-a-Mas-y-varios-consellers-por-la-consulta-del-9N.html">La Fiscalía se querellará contra Mas y varios consellers por la consulta del 9-N</a></li>
<li><a href="http://ecodiario.eleconomista.es/futbol/noticias/6236615/11/14/La-posible-reunion-entre-Pique-y-Luis-Enrique-para-tratar-su-futuro.html">La posible reunión entre Piqué y Luis Enrique para tratar su futuro</a></li>
</ul></h5>
<div class="botones-uh"><p class="buh"><a href="#" onclick="ticker_3562.previousCurrentNews(); return( false );">L</a><a href="#" onclick="ticker_3562.play_pause_Ticker('tecla_pause_play_3562'); return( false );"><span id="tecla_pause_play_3562">0</span></a><a href="#" onclick="ticker_3562.nextCurrentNews(); return( false );">I</a></p></div>
</div>
<script type="text/javascript">//<![CDATA[
					ticker_3562 = new news_Ticker( 'output_3562', 'list_ul_text_3562',{typeSpeed:1,typing:false,tickSpeed:7500} ) ;
				//]]></script>
</div>
</div>
<div mod="1647" class="b930t">
<script language="javascript">//<![CDATA[
			sz = "960x30" ;
			st = "eleconomista.es" ;
			std = "http://ad.es.doubleclick.net/" ;
			kw = "";
			 sect ="6103473";
			document.write( '<SCR'+'IPT language="JavaScript" SRC="'+std+'adj/'+st+'/economista_noticias_emprendedores_pymes;sect='+sect+';tile=3;kw='+kw+';sz='+sz+';ord='+randnum+'?"></SCR'+'IPT>' ) ;
		//]]></script><noscript><a href="http://ad.doubleclick.net/jump/eleconomista.es/economista_noticias_emprendedores_pymes;sect=6103473;tile=3;kw=;sz=960x30;ord=123456789?" target="_blank" rel="nofollow"><img src="http://ad.doubleclick.net/ad/eleconomista.es/economista_noticias_emprendedores_pymes;sect=6103473;tile=3;kw=;sz=960x30;ord=123456789?" width="960" height="30" border="0"></a></noscript>
</div>
<div id="banners-superiores_2">
<script type="text/javascript" language="javascript">//<![CDATA[
		var time = new Date() ;
		randnum= (time.getTime()) ;
	//]]></script><script language="javascript">//<![CDATA[
		sz = "1x2" ;
		st = "eleconomista.es" ;
		std = "http://ad.es.doubleclick.net/" ;
		isnt = "";
		
		document.write( '<SCR'+'IPT language="JavaScript" SRC="'+std+'adj/'+st+'/economista_noticias_emprendedores_pymes;tile=11;dcopt=ist;sz='+sz+';isnt='+isnt+';ord='+randnum+'?"></SCR'+'IPT>' ) ;
	//]]></script><noscript><a href="http://ad.doubleclick.net/jump/eleconomista.es/economista_noticias_emprendedores_pymes;tile=11;sz=1x2;ord=123456789?" target="_blank" rel="nofollow"><img src="http://ad.doubleclick.net/ad/eleconomista.es/economista_noticias_emprendedores_pymes;sz=1x2;ord=123456789?" width="1" height="1" border="0"></a></noscript>
</div>

		<div class="cols-i noticia-des"><div mod="7">
<script type="text/javascript">
	//<![CDATA[
		miLista = new miListaValores( 'a' ) ;
	//]]>
	</script><script type="text/javascript">//<![CDATA[
	function abrirDelicious()
	{
		window.open('http://del.icio.us/post?v=4&noui&jump=close&url='+encodeURIComponent(location.href) + '&title=' + encodeURIComponent(document.title),'delicious','toolbar=0,width=700,height=400');
		return false;
	}
	
	function abrirTuenti()
	{
		window.open('http://www.tuenti.com/share?url='+encodeURIComponent(location.href));
		return false;
	}
	
	function abrirDigg(titulo, texto)
	{
	
		window.open('http://www.digg.com/submit?phase=2&url=' + encodeURIComponent(location.href) + '&title=' + titulo + '&bodytext=' + texto + '&topic=business_finance', 'digg','toolbar=no, scrollbars=yes,resizable=yes');
		return false;
		
	}
	
	function abrirMeneame()
	{
		window.open('http://meneame.net/submit.php?url=%20'+location.href ,'meneame','toolbar=yes, scrollbars=yes,resizable=yes');
		return false;
	}

	function abrirTwitter()
	{
		window.open('http://twitter.com/home?status='+location.href ,'Twitter','toolbar=yes, scrollbars=yes,resizable=yes');
		return false;
	}

	function abrirFacebook()
	{
		window.open('http://www.facebook.com/share.php?u='+location.href ,'Facebook','toolbar=yes, scrollbars=yes,resizable=yes');
		return false;
	}
	
	var tam_actual = 2;
	function actualizarTamano (signo)
	{
		tam = tam_actual;
		if ( ((signo==1) && (tam_actual < 4)) || ((signo==-1) && (tam_actual > 1))){
			tam_actual = tam_Siguiente = tam + signo;

			$$('div#cuerpo_noticia > p').each(
					function(s) {
						if (s.hasClassName('tam'+tam)) 
							s.removeClassName('tam'+tam);
						s.addClassName('tam'+tam_Siguiente)
					});

			$$('div#cuerpo_noticia > h2').each(
					function(s) {
						if (s.hasClassName('tam'+tam)) 
							s.removeClassName('tam'+tam);
						s.addClassName('tam'+tam_Siguiente)
					});
		}
	}
	
	
			enviarNoticiaLeida_Site( 6103473, 1 ) ;
		//]]></script><div id="cuerpo_noticia" class="noticiacuerpo">
<h1>"El autónomo trabaja mejor porque está en juego su nombre, no la empresa"</h1>
<div class="s-horizontal">
<div class="s-h-c">
<script src="http://platform.twitter.com/widgets.js" type="text/javascript"></script><a href="http://twitter.com/share" class="twitter-share-button" data-via="eleconomistaes" data-url="http://www.eleconomista.es/emprendedores-pymes/noticias/6103473/09/14/El-autonomo-trabaja-mejor-porque-esta-en-juego-su-nombre-no-la-empresa.html" data-text='"El autónomo trabaja mejor porque está en juego su nombre, no la empresa"' data-lang="en" data-count="horizontal">Tweet</a>
</div>
<div class="s-h-c2">
<div id="fb-root"></div>
<div class="fb-share-button" data-width="150" data-type="button_count" tyle="position: relative; display: inline;"></div>
</div>
<div class="s-h-c">
<div class="g-plusone" data-size="medium"></div>
<script type="text/javascript">
		  window.___gcfg = {lang: 'es'};

		  (function() {
		    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
		    po.src = 'https://apis.google.com/js/plusone.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
		  })();
		</script>
</div>
<div class="s-h-c">
<script src="//platform.linkedin.com/in.js" type="text/javascript"></script><script type="IN/Share" data-counter="right"></script>
</div>
<div class="wfavicon" sharecount="right" data-tooltip="false"></div>
<script type="text/javascript">(function(d, t, i, l) { var g = d.createElement(t), s = d.getElementsByTagName(t)[0]; g.src = "http://static.womenalia.net/media/wfaviconstyles/wfavicon.min.js"; g.id=i; g.lang=l; s.parentNode.insertBefore(g, s); }(document, "script", "wow-wfavicon", "es-ES"));</script>
</div>
<div class="firma">
<div class="f-autor">Camila Pan de Soraluce</div>
<div class="f-fecha">24/09/2014 - 14:13</div>
</div>
<div class="firma" style="border-bottom: none;">
<div class="com-not"><small class="num-coment"><a href="#Comentarios">10	comentarios</a></small></div>
<div class="slider">
<div id="gr_not_puntos_vote_6103473">
<div style="font-size: 11px; float: left; overflow: hidden; margin-left: 55px; _margin-left: 15px;">
<span id="texto_6103473">Puntúa la noticia</span> : </div>
<img src="http://s01.s3c.es/imagenes/menos-rojo.png" style="margin-top: 2px; margin-left: 15px; float: left;"><div id="track_6103473" class="track1" style="width:70px; height:13px; *height: 18px; float: left; margin-top: 5px;">
<div class="track-left1"></div>
<div id="handle_6103473" style="width:19px; height:20px;background: none;"><img src="http://s01.s3c.es/imagenes/slider/slider-images-handle1.png"></div>
</div>
<img src="http://s01.s3c.es/imagenes/mas-verde.png" style="margin-top: 2px; float: left;">
</div>
<div id="gr_not_puntos_result_6103473"><div style="font-size: 11px; float: left; margin: -5px 0 5px 5px; overflow: hidden; *margin-top:0;">Nota de los usuarios: 
			<span id="not_puntos_6103473" style="font-size: 14px; font-weight: bold;">8,9</span>
			(<span id="not_puntos_votes_6103473">9</span>votos)
		</div></div>
<script>
				not_6103473 = new ValoracionSlider_Noticia(6103473,80,9);
			</script>
</div>
</div>
<div class="not-res"><ul><li>"Tras una reparación llamamos al cliente para saber si ha quedado satisfecho"</li></ul></div>
<div class="p-tag" style="background:none;  border-top: 1px solid #999; width:auto; margin:0;" id="lista_tags">
<img id="abrir_tags" src="http://s01.s3c.es/imag3/ft-a.png" width="16" alt="tags" style="float:right; margin:15px 2px 0 2px; position: relative; right: 0;" onClick="$('lista_tags').style.height='auto';$('abrir_tags').style.display='none';$('cerrar_tags').style.display='block'"><span style="background:none; width:auto; font-weight:bold; padding:16px 0 0 3px;">Más noticias sobre:</span><ul>
<li><a href="/tags/empresas">Empresas</a></li>
<li><a href="/tags/tabletas">Tabletas</a></li>
<li><a href="/tags/tecnologias">Tecnologías</a></li>
</ul>
<img id="cerrar_tags" src="http://s01.s3c.es/imag3/ft-c.png" width="16" alt="tags" style="float:right; margin:15px 2px 0 2px; display:none;" onClick="$('lista_tags').style.height='35px';$('abrir_tags').style.display='block';$('cerrar_tags').style.display='none'">
</div>
<div class="i-not"><div class="foto-not-g"><img src="http://s01.s3c.es/imag/_v0/225x250/8/c/f/h-stephen-phillips.jpg" alt="h-stephen-phillips.jpg" id="1824902"><div class="foto-pie">H. Stephen Phillips, consejero delegado de Reparalia</div></div><div class="relacionados">
<h3>Enlaces relacionados</h3>
<h2><a href="http://www.eleconomista.es/economia/noticias/6098581/09/14/Montoro-acelerara-la-rebaja-fiscal-a-los-autonomos-en-la-retencion-por-IRPF.html#.Kku87yKZjEq0r4L"><img src='/imagenes/iconos/ico-general.gif' /> Montoro acelerará la reforma fiscal</a></h2>
</div>
<div class="caja mods" style="border-top: none; margin:0; border-bottom:none;"><a href="/kiosco/"><img src="http://s04.s3c.es/imag3/botones/kiosco225.png" alt="kiosco"></a></div>
<div class="controles"><ul>
<li class="noti-send">
<img src="http://s03.s3c.es//imag3/iconos/newsletter30x32.png"><a href="/noticias-email/6103473/El-autonomo-trabaja-mejor-porque-esta-en-juego-su-nombre-no-la-empresa" id="enviar" rel="nofollow">Enviar por e-mail</a>
</li>
<li><a href="javascript:window.print();" id="imprimir">Imprimir</a></li>
<li><a href="#" id="aumentar" onclick="return actualizarTamano(1);">Aumentar texto</a></li>
<li><a href="#" id="reducir" onclick="return actualizarTamano(-1);">Reducir texto</a></li>
<li>
<img style="float:left" src="http://s01.s3c.es/imag3/iconos/dixio.gif" alt="Dixio">Haga doble click sobre una palabra para ver su signifcado</li>
<li class="foro-rel"><a href="/servicios/sudoku/"><img style="float:left" src="http://s01.s3c.es/imag3/botones/sudoku.gif" alt="sudoku"><b>Sudoku:</b> Juega cada día a uno nuevo</a></li>
<li class="foro-rel"><a href="http://ecodiario.eleconomista.es/el-tiempo/"><img style="float:left" src="http://s01.s3c.es/imag3/botones/tiempo.gif" alt="Tiempo"><b>El tiempo:</b> Consulta la previsión para tu ciudad</a></li>
</ul></div>
</div>
<p><p>Stephen Philipps está al frente de una empresa &quot;que da el mismo trato a sus clientes que a sus empleados&quot;. En este sentido, han dotado a su personal de las últimas tecnologías, a través de tabletas que les permiten tanto facilitar su tarea como optimizar los procesos de cara a la comodidad de sus clientes. <b><a href="http://www.eleconomista.es/kiosco/gestion/">Más noticias en la revista gratuita Gestión Empresarial</a></b></p></p><p><b>¿A qué se dedica Reparalia?</b></p>
<p>Trabajamos con dos líneas de negocio: la venta de pequeños contratos de reparaciones, y la gestión de siniestros a través de aseguradoras. Hoy en día trabajamos con 18 grandes aseguradoras en España, y además hemos creado una cartera propia de un millón de contratos, trabajando con varias compañías eléctricas y de servicios de aguas.</p>
<p><b>¿De qué manera trabajáis con los profesionales que realizan las reparaciones?</b></p>
<p>Dentro de los 2.000 ó 2.500 con los que operamos, unos 200 llevan el logo de Reparalia, los cuales tienen preferencia en zonas de menos volumen y además pueden tener clientes propios. El 90% de nuestros profesionales son autónomos, ya que creemos que cuando se confía una tarea a una persona que responde con un apellido y una imagen, la calidad es mejor.</p>
<p><b>¿Y cómo controlan la calidad de esas reparaciones?</b></p>
<p>Después de una reparación llamamos al cliente para saber si ha quedado satisfecho. Si tiene alguna queja o reclamación, se baja de orden al profesional que hizo ese trabajo, y los siguientes avisos de reparaciones le llegan más tarde, lo cual puede costarle incluso el despido. Además, algunos directivos aparecen por sorpresa en el lugar en el que se está realizando un trabajo para vivir la experiencia del cliente.</p>
<p><b>Es otra forma de aplicar el salario variable a su trabajo, ¿no?</b></p>
<p>Sí, exactamente. Los profesionales no reciben la parte variable si no consiguen bajar el nivel de insatisfacción y aumentar la satisfacción, además de mantener un buen nivel de objetivos comparativos. Y es en este orden, porque si no hay clientes no existimos y si no funcionamos bien, no merecemos gestionar nosotros el negocio.</p><div class="canales"><ul>
<li><a href="/noticias/flash" id="categoria_12" onclick="return false;">Flash</a></li>
<script type="text/javascript">create_Balloon(12,1, 'categoria_12');</script><li><a href="/noticias/gestion-empresarial" id="categoria_20" onclick="return false;">Pymes y Emprendedores</a></li>
<script type="text/javascript">create_Balloon(20,1, 'categoria_20');</script><li><a href="/noticias/seleccion-ee" id="categoria_35" onclick="return false;">Seleccion eE</a></li>
<script type="text/javascript">create_Balloon(35,1, 'categoria_35');</script><li><a href="/noticias/flash-emprendedores" id="categoria_477" onclick="return false;">Flash Emprendedores</a></li>
<script type="text/javascript">create_Balloon(477,1, 'categoria_477');</script><li><a href="/noticias/emprendedores-pymes" id="categoria_478" onclick="return false;">Emprendedores-Pymes</a></li>
<script type="text/javascript">create_Balloon(478,1, 'categoria_478');</script>
</ul></div>
<div class="sep" style="height: 10px;"></div>
<div style="margin: 5px 0pt 10px;" class="sep punteado"></div>
<p style="margin:0; font-size:8px;margin-top:0;">PUBLICIDAD</p>
<iframe width="634" height="28" src="http://www.idealista.com/news/widget/get/57579/2"></iframe><div style="background:#eee;">
<img style="float:left; padding:2px 5px 2px 2px" src="http://s03.s3c.es/imag3/banners/selfbank-peq.gif"><p style="font-size:11px; padding:5px">- <a target="_blank" rel="nofollow" href="http://pubads.g.doubleclick.net/gampad/clk?id=198661102&amp;iu=/4275">300 euros en comisiones y Pro Real Time gratis hasta 2015. Abre ya cuenta de Bolsa en Self Bank.</a></p>
</div>
<div class="pub-ads">
<div id="cX-root" style="display:none"></div>
<div id="panComboAds_630x240" style="display:none"></div>
<script type="text/javascript">

			var cX = cX || {}; 
			cX.callQueue = cX.callQueue || [];

			cX.callQueue.push(['insertAdSpace', { adSpaceId: '00000000013a878d',
												insertBeforeElementId: 'panComboAds_630x240',
												adUnitWidth: 630, 
												adUnitHeight: 240,
												k: 'panbackfill',
												initialHorizontalAdUnits: 1, 
												initialVerticalAdUnits: 1,
												renderTemplateUrl: 'http://cdn.cxpublic.com/PAN_Combo_630x240_ElEconomista.html'

								} ]);
		  cX.callQueue.push(['setAccountId', '9222287489163341896']);
		  cX.callQueue.push(['setSiteId', '9222287489163341897']);
		  cX.callQueue.push(['sendPageViewEvent']);

		</script><script type="text/javascript">
			(function() { try { var scriptEl = document.createElement('script');
								scriptEl.type = 'text/javascript'; 
								scriptEl.async = 'async';
								scriptEl.src = ('https:' == document.location.protocol) ?
										'https://scdn.cxense.com/cx.js' : 'http://cdn.cxense.com/cx.js';
								var targetEl = document.getElementsByTagName('script')[0];
								targetEl.parentNode.insertBefore(scriptEl, targetEl); 
							} 
						catch (e) {};} ());

	</script>
</div>
<div class="fb-like" data-send="true" data-width="450" data-show-faces="false"></div>
<a href="https://twitter.com/share" class="twitter-share-button" data-lang="es">Twittear</a><div style="margin: 10px 0pt 10px;"></div>
<div id="recsTargetEl" style="display:none"></div>
<script type="text/javascript">
        var cX = cX || {}; cX.callQueue = cX.callQueue || [];
        cX.callQueue.push(['insertWidget',{
            width: 630, height: 290,
            resizeToContentSize: true,
            insertBeforeElementId: 'recsTargetEl',
            renderTemplateUrl: 'http://cdn.cxpublic.com/ee-hor.html'
        }]);
    </script><script type="text/javascript">
        (function() { try { var scriptEl = document.createElement('script'); scriptEl.type = 'text/javascript'; scriptEl.async = 'async';
        scriptEl.src = ('https:' == document.location.protocol) ? 'https://scdn.cxense.com/cx.js' : 'http://cdn.cxense.com/cx.js';
        var targetEl = document.getElementsByTagName('script')[0]; targetEl.parentNode.insertBefore(scriptEl, targetEl); } catch (e) {};} ());
    </script><div class="sep" style="height: 10px;"></div>
</div>
</div>
<div mod="1649">
<a id="Comentarios"></a><div id="comm">
<h2>Comentarios 10</h2>
<a name="comment-2705725"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">1</div>24-09-2014 / 14:19<br><div class="nombre-usuario">Monicote</div>
<div class="puntos">
<script language="javascript">
			mkarma_2705725 = new comment_karma('2705725', '-4');
		</script>
		Puntuación <span id="karma_2705725">-4</span><a id="karma_accion_subir_2705725" onclick="return false;"></a><a id="karma_accion_bajar_2705725" onclick="return false;"></a><script language="javascript">mkarma_2705725.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>Con Jefes así da gusto, motivar y valorar al empleado.</p></div>
</div>
<a name="comment-2705986"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">2</div>24-09-2014 / 16:36<br><div class="nombre-usuario">jua, jua, Reparalia, jua, jua,.....</div>
<div class="puntos">
<script language="javascript">
			mkarma_2705986 = new comment_karma('2705986', '5');
		</script>
		Puntuación <span id="karma_2705986">5</span><a id="karma_accion_subir_2705986" onclick="return false;"></a><a id="karma_accion_bajar_2705986" onclick="return false;"></a><script language="javascript">mkarma_2705986.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>Jua, Jua, jua,....<br /><br/>y lo dice Reparalia?<br /><br/><br /><br/>Reparalia trabaja con algunas compañias de seguro.<br /><br/><br /><br/>Si tienes un incidente en tu casa, y tu compañia<br /><br/>de seguros te manda Reparalia.....<br /><br/><br /><br/>date por jodido!!</p></div>
</div>
<a name="comment-2705991"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">3</div>24-09-2014 / 16:38<br><div class="nombre-usuario">jua, jua, Reparalia, jua, jua,.....</div>
<div class="puntos">
<script language="javascript">
			mkarma_2705991 = new comment_karma('2705991', '3');
		</script>
		Puntuación <span id="karma_2705991">3</span><a id="karma_accion_subir_2705991" onclick="return false;"></a><a id="karma_accion_bajar_2705991" onclick="return false;"></a><script language="javascript">mkarma_2705991.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>¿Y cómo controlan la calidad de esas reparaciones?<br /><br/><br /><br/>En mi caso, les mandé 1 burofax, después de más de 10<br /><br/>llamadas, hablando con inútiles (perdón Reparalia).<br /><br/>Todo para pintar una pared, después de las humedades <br /><br/>de un vecino.<br /><br/><br /><br/>Pd.: Si el seguro de tu vecino es Reparalia, date por jodido</p></div>
</div>
<a name="comment-2706003"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">4</div>24-09-2014 / 16:43<br><div class="nombre-usuario">Viano</div>
<div class="puntos">
<script language="javascript">
			mkarma_2706003 = new comment_karma('2706003', '7');
		</script>
		Puntuación <span id="karma_2706003">7</span><a id="karma_accion_subir_2706003" onclick="return false;"></a><a id="karma_accion_bajar_2706003" onclick="return false;"></a><script language="javascript">mkarma_2706003.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>Por la cuenta que les trae, claro que trabajan mejor.... <br /><br/>Y como los políticos no cuiden (que no cuidan) a los autónomos, sin avasallarlos a impuestos, impedimentos para emprender,papel para qui para alli, no se de que van a cobrar los funcionarios y políticos.... <br /><br/>Mas les vale cuidarlos o España ya falta poco para que haga catapuuun, solo hay que mirar al deficit</p></div>
</div>
<a name="comment-2706029"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">5</div>24-09-2014 / 16:50<br><div class="nombre-usuario">Pepe</div>
<div class="puntos">
<script language="javascript">
			mkarma_2706029 = new comment_karma('2706029', '5');
		</script>
		Puntuación <span id="karma_2706029">5</span><a id="karma_accion_subir_2706029" onclick="return false;"></a><a id="karma_accion_bajar_2706029" onclick="return false;"></a><script language="javascript">mkarma_2706029.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>bla bla bla Muchas palabras vacias pero lo que no saven los usuarios de Reparalia es lo que cobran los trabajadores  que se dejen de historias buscan Autonomos solo para avaratar costes.</p></div>
</div>
<a name="comment-2706174"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">6</div>24-09-2014 / 17:46<br><div class="nombre-usuario">repaso</div>
<div class="puntos">
<script language="javascript">
			mkarma_2706174 = new comment_karma('2706174', '4');
		</script>
		Puntuación <span id="karma_2706174">4</span><a id="karma_accion_subir_2706174" onclick="return false;"></a><a id="karma_accion_bajar_2706174" onclick="return false;"></a><script language="javascript">mkarma_2706174.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>Es verdad, el autónomo es para pasar el gasto a otro y no tenerlos en nómina, que deje de hacer el hipócrita.</p></div>
</div>
<a name="comment-2706349"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">7</div>24-09-2014 / 20:20<br><div class="nombre-usuario">trabajador autonomo</div>
<div class="puntos">
<script language="javascript">
			mkarma_2706349 = new comment_karma('2706349', '0');
		</script>
		Puntuación <span id="karma_2706349">0</span><a id="karma_accion_subir_2706349" onclick="return false;"></a><a id="karma_accion_bajar_2706349" onclick="return false;"></a><script language="javascript">mkarma_2706349.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>a mi ni me pagaron los servicios, estafadores. y te dicen te pagamos lo mismo lo arregles o no. ja mierda de pais</p></div>
</div>
<a name="comment-2706420"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">8</div>24-09-2014 / 21:10<br><div class="nombre-usuario">
<img style="padding-right: 5px; float: left;" src="http://s01.s3c.es/imag3/iconos/eE.gif" title="Usuario validado en elEconomista.es">mfb</div>
<div class="puntos">
<script language="javascript">
			mkarma_2706420 = new comment_karma('2706420', '4');
		</script>
		Puntuación <span id="karma_2706420">4</span><a id="karma_accion_subir_2706420" onclick="return false;"></a><a id="karma_accion_bajar_2706420" onclick="return false;"></a><script language="javascript">mkarma_2706420.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>El autónomo trabaja porque no le queda mas remedio. El funcionario que no trabaja se refugia en su impunidad. Vete mañana a ver como los de Hacienda de Guzmán el Bueno se van a comprar al Corte Inglés.</p></div>
</div>
<a name="comment-2706707"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">9</div>25-09-2014 / 08:07<br><div class="nombre-usuario"><a href="http://www.detodoporinternet.com/muestras-gratis-perfumes.html" rel="nofollow">muestras gratis</a></div>
<div class="puntos">
<script language="javascript">
			mkarma_2706707 = new comment_karma('2706707', '2');
		</script>
		Puntuación <span id="karma_2706707">2</span><a id="karma_accion_subir_2706707" onclick="return false;"></a><a id="karma_accion_bajar_2706707" onclick="return false;"></a><script language="javascript">mkarma_2706707.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>Hola,<br /><br/>Es un modelo que puede funcionar en según que personas ... otros se conforman con hacer lo justo y a casita ...<br /><br/>Un placer<br /><br/>NeyLo2011</p></div>
</div>
<a name="comment-2706743"></a><div class="coment">
<div class="mensaje-fecha">
<div class="numero-comentario">10</div>25-09-2014 / 08:26<br><div class="nombre-usuario">paquito</div>
<div class="puntos">
<script language="javascript">
			mkarma_2706743 = new comment_karma('2706743', '-1');
		</script>
		Puntuación <span id="karma_2706743">-1</span><a id="karma_accion_subir_2706743" onclick="return false;"></a><a id="karma_accion_bajar_2706743" onclick="return false;"></a><script language="javascript">mkarma_2706743.isCommentVoted(); </script>
</div>
</div>
<div class="mensaje"><p>Buscan autónomos para despedirlos de un instante al siguiente instante ¿ Qué nombre ni ocho cuartos ?</p></div>
</div>
</div>
</div>

		</div>
		<div class="col">
			<div class="cont-flash" style="margin-bottom:10px;"><div mod="1246"><div id="webslice_flash_del_mercado" class="hslice entry-content">
<a style="display: none;" rel="entry-content" href="/slices/web-slice.php?modulo=1246"></a><span class="ttl" style="display: none;">15</span><div class="flash caja" style="border-top:1px solid #666666;">
<h3 style="margin-top:-5px; margin-bottom:10px; padding:0; ">
<a href="/flash/index.html">El Flash del mercado</a><span class="entry-title" style="display: none;">en elEconomista.es</span><a href="https://www.bancsabadell.com/" rel="nofollow" target="_blank"><img src="http://s01.s3c.es/imag3/logos/sabadell.gif" alt="Sabadell Atlantico" style="border: none; margin-left: 35px;"></a>
</h3>
<ul>
<li>
<span class="hora">10:04</span><a href="http://ecodiario.eleconomista.es/politica/noticias/6236645/11/14/Interior-niega-que-la-Fiscalia-este-a-las-ordenes-del-Gobierno-por-la-querella-contra-el-9N.html">Interior niega que la Fiscalía esté a las órdenes del Gobierno por la querella contra el 9-N</a>
</li>
<li>
<span class="hora">10:00</span><a href="http://ecodiario.eleconomista.es/politica/noticias/6236631/11/14/Espadaler-cree-que-la-querella-contra-Artur-Mas-inhabilita-a-Rajoy-para-hacer-politica.html">El Govern cree que la querella contra Mas "inhabilita a Rajoy para hacer política"</a>
</li>
<li>
<span class="hora">09:56</span><a href="http://www.eleconomista.es/ecomotor/motor/noticias/6236630/11/14/Es-posible-ahorrar-6000-euros-al-ano-compartiendo-coche-en-vez-de-tener-uno.html">¿Es posible ahorrar 6.000 euros al año compartiendo coche en vez de tener uno?</a>
</li>
<li class="mas">
<span class="sigue" style="display: inline; margin-right: 20px;"><a href="http://www.eleconomista.es/flash/index.html">Ver todos</a></span><span class="sigue sigue2" style="display: inline;"><a href="http://www.eleconomista.es/fichas-de-empresas/index.html">Nuevas fichas empresas</a></span>
</li>
</ul>
</div>
</div></div>

			</div><div mod="5004">
<script language="javascript">//<![CDATA[
        
if( typeof( modulosSite) == "object") modulosSite.push("5011");
            
if( typeof( modulosReemplazados) == "object") modulosReemplazados.push("5004_1");
                
if( typeof( modulosSite) == "object") modulosSite.push("5027");
            
if( typeof( modulosReemplazados) == "object") modulosReemplazados.push("5004_2");
                
//]]></script><div mod="5004_1"></div>
<div mod="5004_2"></div>
</div>
<div mod="1273" class="b300">
<script language="javascript">//<![CDATA[
						sz = "300x250" ;
						st = "eleconomista.es" ;
						std = "http://ad.es.doubleclick.net/" ;
						kw = "";
				//		randnum = old_randnum;
						 sect ="6103473";
						document.write( '<SCR'+'IPT language="JavaScript" SRC="'+std+'adj/'+st+'/economista_noticias_emprendedores_pymes;sect='+sect+';tile=5;kw='+kw+';sz='+sz+';ord='+randnum+'?"></SCR'+'IPT>' ) ;
					//]]></script><noscript><a href="http://ad.doubleclick.net/jump/eleconomista.es/economista_noticias_emprendedores_pymes;sect=6103473;tile=5;kw=;sz=300x250;ord=123456789?" target="_blank" rel="nofollow"><img src="http://ad.doubleclick.net/ad/eleconomista.es/economista_noticias_emprendedores_pymes;sect=6103473;tile=5;kw=;sz=300x250;ord=123456789?" width="300" height="250" border="0"></a></noscript>
</div>

		<div class="e-eu"><div></div>

		</div>
				                                   <div style="border-top: 2px solid #E6E6E6;border-bottom: 2px solid #E6E6E6;">
				                                   <h2><a style="text-decoration: none;color: #816854;font-weight: bold;font-size: 19px;margin: 3px;display: block;text-align: left;font-family: Trebuchet MS, Verdana, Arial, sans-serif;" target="_blank" href="https://es.santanderadvance.com/" rel="nofollow">Santander con las pymes</a></h2>
				                                   <a target="_blank" href="https://es.santanderadvance.com/" rel="nofollow"><img src="http://s01.s3c.es/imag3/logos/santander/codigo_1.jpg"/></a></div>
				                        <div></div>
 
			<div class="caja mods">
				<div style="overflow: hidden;background: #fff;padding: 10px;">
				<img src="http://s01.s3c.es/imag3/logos/eE3.png" alt="" style="float: left; padding-right: 10px;border-right: 2px solid #eee;margin-right: 10px;margin-top: 6px;">

				<!--twitter-->
				<iframe id="twitter-widget-0" scrolling="no" frameborder="0" allowtransparency="true" src="http://platform.twitter.com/widgets/follow_button.1404859412.html#_=1405593639981&amp;id=twitter-widget-0&amp;lang=es&amp;screen_name=elEconomistaes&amp;show_count=false&amp;show_screen_name=true&amp;size=m" class="twitter-follow-button twitter-follow-button" title="Twitter Follow Button" data-twttr-rendered="true" style="width: 171px; height: 20px;"></iframe>
			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?"http":"https";if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document, "script", "twitter-wjs");</script>
				<!--facebook-->
				<iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2FelEconomista.es&amp;width=130px&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21&amp;appId=396195810399162" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:130px; height:21px;margin:5px 0;" allowtransparency="true"></iframe>
				<!--google-->
				<!-- Inserta esta etiqueta donde quieras que aparezca widget. -->
				<div id="___follow_0" style="text-indent: 0px; margin: 0px; padding: 0px; border-style: none; float: none; line-height: normal; font-size: 1px; vertical-align: baseline; display: inline-block; width: 91px; height: 20px; background: transparent;"><iframe frameborder="0" hspace="0" marginheight="0" marginwidth="0" scrolling="no" style="position: static; top: 0px; width: 91px; margin: 0px; border-style: none; left: 0px; visibility: visible; height: 20px;" tabindex="0" vspace="0" width="100%" id="I0_1405593638317" name="I0_1405593638317" src="https://apis.google.com/u/0/_/widget/render/follow?usegapi=1&amp;annotation=none&amp;height=20&amp;rel=publisher&amp;hl=es&amp;origin=http%3A%2F%2Fdesarrollo.eleconomista.es&amp;url=https%3A%2F%2Fplus.google.com%2F113527166384132333126&amp;gsrc=3p&amp;ic=1&amp;jsh=m%3B%2F_%2Fscs%2Fapps-static%2F_%2Fjs%2Fk%3Doz.gapi.es.kgwtPUNfqKs.O%2Fm%3D__features__%2Fam%3DAQ%2Frt%3Dj%2Fd%3D1%2Ft%3Dzcms%2Frs%3DAItRSTMD3J2AR92dCXMjh4MTmaKFMQ247g#_methods=onPlusOne%2C_ready%2C_close%2C_open%2C_resizeMe%2C_renderstart%2Concircled%2Cdrefresh%2Cerefresh%2Conload&amp;id=I0_1405593638317&amp;parent=http%3A%2F%2Fdesarrollo.eleconomista.es&amp;pfname=&amp;rpctoken=38912371" data-gapiattached="true"></iframe></div>

				<!-- Inserta esta etiqueta después de la última etiqueta de widget. -->
				<script type="text/javascript">
				  window.___gcfg = {lang: "es"};

				  (function() {
				    var po = document.createElement("script"); po.type = "text/javascript"; po.async = true;
				    po.src = "https://apis.google.com/js/platform.js";
				    var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(po, s);
				  })();
				</script>
			</div>
			</div>
			
				<div>
					<table cellspacing="0" class="tablapeq" style="margin:0 0 10px 0">
						<caption style="background: #f60; color: #fff;">elMonitor</caption>
						<tbody>
							<tr>
								<td style="text-align: left;">
									<h3 style="font-weight: bold; margin: 5px 0;">
										<a style="color: #333; text-decoration: none;" href="http://www.eleconomista.es/monitor">La herramienta para el ahorrador en Bolsa
										</a>
									</h3>
									<a href="http://www.eleconomista.es/monitor">
										<img style="float: left; margin-right: 5px;" alt="Monitor" src="http://s04.s3c.es/imag3/monitor/monitor50.png">
									</a>

									<p>
										<a style="color: #333; text-decoration: none;" href="http://www.eleconomista.es/monitor">Construya su cartera de inversión de forma clara y sencilla con las recomendaciones de elMonitor. <span style="color: #f60; font-weight: bold;">¡Regístrese y pruébelo GRATIS!</span>
										</a>
									</p>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="sep"></div>
			
<!-- CAJA DE REGISTRO ALERTAS -->
<div class="caja c-alerta" style="background: #fff;margin-bottom: 0; border-bottom:none;">
	<h3 style="margin-bottom: 5px; padding-left: 5px;">Alertas por email</h3>
	<img src="http://s04.s3c.es/imag3/iconos/alerta3.png" alt="alerta" style="float: left;" />
		<form action="http://www.eleconomista.es/boletines/ultima-hora.php" method="post">
			<label for="alerta-email" style="font-weight: normal;">Suscríbete y recibe <b>las noticias de última hora gratis</b> en tu email.</label>
			<div style="margin-top: 5px;float: left;">
			<input type="text" value="" name="email" class="email" id="" placeholder="Dirección de email" /></div>
			<div style="margin-top: 5px; text-align: left; padding-left: 61px;">
			<input type="submit" value="Subscribirse" name="alta" id="alta" class="c-a-boton" />
			</div>
		</form>
</div>
<!-- FIN CAJA DE REGISTRO ALERTAS -->

<div class="caja c-alerta" style="background: #fff;border-top: 3px solid #666;">
	<h3 style="margin-bottom: 5px; padding-left: 5px;font-family: Georgia,Times,serif;">el<span style="color:#f60;">Resumen</span>Diario</h3>
	<!--<img src="http://s04.s3c.es/imag3/iconos/alerta3.png" alt="alerta" style="float: left;" />-->
		<form action="http://www.eleconomista.es/boletines/alta_ok.php" method="post">
			<label for="alerta-email" style="font-weight: normal;margin: 6px 5px;display: block;line-height: 14px;">Suscríbete y recibe <b>el resumen de las noticias más importantes del día</b> en tu email.</label>
			<div style="margin: 5px 0 0 5px;">
			<input style="width: 199px;"" type="text" value="" name="email" class="email" id="" placeholder="Dirección de email" />
			<input type="hidden" value="1" id="Optin_news1" name="Optin_news1">
			<input style="background: #f60;" type="submit" value="Subscribirse" name="alta" id="alta" class="c-a-boton" />
			</div>
		</form>
</div>
<div class="sep"></div><div class="sep"></div>
<div mod="2779">
<div class="uh uh-new">
<script type="text/javascript">
			//<![CDATA[
			var pestanas = new Array();
				
				
				pestanas[0] = {site:1, tag:'tmas1', id:'d-1'};
				pestanas[1] = {site:4, tag:'tmas2', id:'d-2'};
				pestanas[2] = {site:4, tag:'tmas3', id:'d-3'};
				pestanas[3] = {site:4, tag:'tmas4', id:'d-4'};
				
				
				
					vMasNoticiasP = new masNoticias (pestanas[0].id, pestanas[0].tag);
				
			//]]>
			</script><h5>El flash: toda la última hora</h5>
<ul class="pest pest2"><li id="tmas1"><a href="#" onclick="javascript:vMasNoticiasP.mostrar('tmas1','d-1');return false;" target="_top">Actualidad</a></li></ul>
<script type="text/javascript">
				//<![CDATA[
					$(pestanas[0].tag).addClassName('select');
					
				//]]>
				</script><div class="uh-cont  uh-cont2">
<div id="d-1" class="uh-not2">
<ul>
<div class="uh-not2">
<h2><a href="http://ecodiario.eleconomista.es/politica/noticias/6236645/11/14/Interior-niega-que-la-Fiscalia-este-a-las-ordenes-del-Gobierno-por-la-querella-contra-el-9N.html?utm_source=crosslink&amp;utm_medium=flash" target="_top">Interior niega que la Fiscalía esté a las órdenes del Gobierno po…</a></h2>
<div class="uh-hora2">
<span class="hora5">10:04</span>
										Ecodiario.es
									
								 - Política<div class="sep"></div>
<img src="http://s01.s3c.es/imag/_v0/580x300/4/6/1/interior-fernandez-diaz-efe.jpg"><p>El ministro del Interior, Jorge Fernández Díaz, ha asegurado hoy que el Ejecutivo ha actuado en todo momento en re...
							 
							 </p>
</div>
</div>
<div class="uh-not2">
<h2><a href="http://ecodiario.eleconomista.es/politica/noticias/6236631/11/14/Espadaler-cree-que-la-querella-contra-Artur-Mas-inhabilita-a-Rajoy-para-hacer-politica.html?utm_source=crosslink&amp;utm_medium=flash" target="_top">El Govern cree que la querella contra Mas "inhabilita a Rajoy par…</a></h2>
<div class="uh-hora2">
<span class="hora5">09:58</span>
										Ecodiario.es
									
								 - Política<div class="sep"></div>
<img src="http://s01.s3c.es/imag/_v0/50x50/2/1/2/CONSULTA-9N.jpg"><p>El conseller de Interior y secretario general de UDC, Ramon Espadaler, ha considerado este miércoles que la presen...
							 
							 </p>
</div>
</div>
<div class="uh-not2">
<h2><a href="http://ecodiario.eleconomista.es/politica/cronicas/5092/11/14/EN-DIRECTO-Reacciones-y-hechos-a-la-firma-del-decreto-de-la-ley-de-consultas-y-su-convocatoria.html?utm_source=crosslink&amp;utm_medium=flash" target="_top">EN DIRECTO | 9-N: siga aquí la evolución del proceso soberanista</a></h2>
<div class="uh-hora2">
<span class="hora5">09:58</span>
										Ecodiario.es
									
								 - Política<div class="sep"></div>
<p>El conseller de Interior y secretario general de UDC, Ramon Espadaler, ha considerado este miércoles que la presen...
							 
							 </p>
</div>
</div>
<div class="uh-not2">
<h2><a href="http://ecodiario.eleconomista.es/ciencia/noticias/6236616/11/14/La-sonda-que-aterrizara-en-el-cometa-67P-sufre-un-problema-en-el-sistema-de-descenso.html?utm_source=crosslink&amp;utm_medium=flash" target="_top">La sonda Philae se separa con éxito de la nave Rosetta y pone rum…</a></h2>
<div class="uh-hora2">
<span class="hora5">09:48</span>
										Ecodiario.es
									
								 - Ciencia<div class="sep"></div>
<img src="http://s01.s3c.es/imag/_v0/580x300/5/1/8/philae-sonda.jpg"><p>La sonda Philae se ha separado con éxito de la nave Rosetta e inicia su viaje de siete horas hasta la superficie d...
							 
							 </p>
</div>
</div>
<div class="uh-not2">
<h2><a href="http://ecodiario.eleconomista.es/futbol/noticias/6236615/11/14/La-posible-reunion-entre-Pique-y-Luis-Enrique-para-tratar-su-futuro.html?utm_source=crosslink&amp;utm_medium=flash" target="_top">La posible reunión entre Piqué y Luis Enrique para tratar su futu…</a></h2>
<div class="uh-hora2">
<span class="hora5">09:45</span>
										Ecodiario.es
									
								 - Fútbol<div class="sep"></div>
<img src="http://s01.s3c.es/imag/_v0/580x387/e/7/1/Pique-Busquets-Bartra-lluvia-LasRozas-2014.jpg"><p>Gerard Piqué y Luis Enrique podrían mantener una reunión en breve para tratar el futuro del futbolista en el FC Ba...
							 
							 </p>
</div>
</div>
<div class="uh-not2">
<h2><a href="http://ecodiario.eleconomista.es/sociedad/noticias/6236574/11/14/La-momia-en-el-tejado-de-la-Facultad-de-Medicina-que-trae-de-cabeza-a-la-Complutense.html?utm_source=crosslink&amp;utm_medium=flash" target="_top">La momia en el tejado de la Facultad de Medicina que trae de cabe…</a></h2>
<div class="uh-hora2">
<span class="hora5">09:22</span>
										Ecodiario.es
									
								 - Sociedad<div class="sep"></div>
<img src="http://s01.s3c.es/imag/video/1048x576/b/9/2/momia-medicina.jpg"><p>La Facultad de Medicina de la Universidad Complutense de Madrid vuelve a estar envuelta en la polémica después de ...
							 
							 </p>
</div>
</div>
<div class="uh-not2">
<h2><a href="http://ecodiario.eleconomista.es/asia/noticias/6236566/11/14/Los-instrumentos-usados-en-la-esterilizacion-de-las-mujeres-muertas-en-India-estaban-oxidados.html?utm_source=crosslink&amp;utm_medium=flash" target="_top">Los instrumentos usados en la esterilización de las mujeres muert…</a></h2>
<div class="uh-hora2">
<span class="hora5">09:17</span>
										Ecodiario.es
									
								 - Asia<div class="sep"></div>
<img src="http://s01.s3c.es/imag/_v0/580x300/f/1/f/campo-esterilizacion-india-reuters.jpg"><p>Los instrumentos quirúrgicos utilizados en la campaña masiva de ligadura de trompas que ha acabado con la vida de ...
							 
							 </p>
</div>
</div>
<div class="uh-not2">
<h2><a href="http://ecodiario.eleconomista.es/africa/noticias/6236549/11/14/Africa-afronta-otro-desafio-tras-el-ebola-esta-al-borde-de-una-grave-crisis-alimentaria.html?utm_source=crosslink&amp;utm_medium=flash" target="_top">África afronta otro desafío tras el ébola: al borde de una grave …</a></h2>
<div class="uh-hora2">
<span class="hora5">09:08</span>
										Ecodiario.es
									
								 - Africa<div class="sep"></div>
<img src="http://s01.s3c.es/imag/_v0/580x300/7/b/9/pobreza-africa.jpg"><p>La relatora especial sobre derecho a la alimentación de Naciones Unidas, Hilal Elver, ha advertido este martes de ...
							 
							 </p>
</div>
</div>
<div class="uh-not2">
<h2><a href="http://ecodiario.eleconomista.es/futbol/noticias/6236546/11/14/Del-Bosque-se-revela-contra-los-clubs-Si-las-selecciones-sobran-que-las-quiten.html?utm_source=crosslink&amp;utm_medium=flash" target="_top">Del Bosque, contra las críticas: "Si las selecciones sobran, que …</a></h2>
<div class="uh-hora2">
<span class="hora5">09:06</span>
										Ecodiario.es
									
								 - Fútbol<div class="sep"></div>
<img src="http://s01.s3c.es/imag/_v0/580x395/f/8/5/DelBosque-serio-2014-rp-efe.jpg"><p>El seleccionador español, Vicente del Bosque, reconoció que le molesta que se hable de 'Virus FIFA' y que se le te...
							 
							 </p>
</div>
</div>
<div class="uh-not2">
<h2><a href="http://ecodiario.eleconomista.es/europa/noticias/6236533/11/14/Registrados-varios-bombardeos-en-Donetsk-a-pesar-del-alto-el-fuego.html?utm_source=crosslink&amp;utm_medium=flash" target="_top">Registrados varios bombardeos en Donetsk a pesar del alto el fueg…</a></h2>
<div class="uh-hora2">
<span class="hora5">08:59</span>
										Ecodiario.es
									
								 - Europa<div class="sep"></div>
<img src="http://s01.s3c.es/imag/_v0/580x300/b/0/2/soldados-ucranianos-coche-efe.jpg"><p>La ciudad de Donetsk, uno de los principales bastiones de los separatistas prorrusos en el este de Ucrania, ha suf...
							 
							 </p>
</div>
</div>
</ul>
<div class="sep-mas1"></div>
<div class=""><a class="mas-noti" target="_top" href="http://ecodiario.eleconomista.es/flash">Más noticias</a></div>
</div>
<div id="d-2" class="uh-not2"></div>
</div>
<script type="text/javascript">
				//<![CDATA[
					for (i=1; i<pestanas.length; i++) {
						//if (i!=0 ) {
							vMasNoticiasP.ocultar(pestanas[i].id);
						//}
					}
					
	
				//]]>
				</script>
</div>
<div class="sep"></div>
</div>
<div mod="2479" class="masleidas-orus masleidas-orus2"><div class="popular">
<script type="text/javascript">
					//<![CDATA[
						var sites = new Array();
						
						//------------------------------Realizacion de For each -----------------------------
						
							sites[0] = {site:1, tag:'tmaseconomista' , id:'d-economista'};
						
							sites[1] = {site:4, tag:'tmasecodiario' , id:'d-ecodiario'};
						
							sites[2] = {site:18, tag:'tmasecoteuve' , id:'d-ecoteuve'};
						
							sites[3] = {site:8, tag:'tmasmotor' , id:'d-motor'};
						
							sites[4] = {site:16, tag:'tmasevasion' , id:'d-evasion'};
						
					
						idSite = $(document.body).readAttribute('s');
						if(idSite==null){
							idSite=16;
						}
						
		//				idSite = 16;
						
						for (i=0; i<sites.length;i++) {
							if (idSite == sites[i].site ) 
								break;
						}
						vMasNoticiasS = new masNoticias (sites[i].id, sites[i].tag);
						
					//]]>
					</script><div id="pestPopular"><ul class="pest">
<li id="tmaseconomista" style="font-size: 11px;"><a href="#" onclick="javascript:vMasNoticiasS.mostrar('tmaseconomista','d-economista');return false;" target="'_top'">el<span style="color: #000;">Eco</span>nomista</a></li>
<li id="tmasecodiario" style="font-size: 11px;"><a href="#" onclick="javascript:vMasNoticiasS.mostrar('tmasecodiario','d-ecodiario');return false;" target="'_top'"><span style="color: #000;">Eco</span>Diario</a></li>
<li id="tmasecoteuve" style="font-size: 11px;"><a href="#" onclick="javascript:vMasNoticiasS.mostrar('tmasecoteuve','d-ecoteuve');return false;" target="'_top'"><span style="color: #000;">Eco</span>teuve</a></li>
<li id="tmasmotor" style="font-size: 11px;"><a href="#" onclick="javascript:vMasNoticiasS.mostrar('tmasmotor','d-motor');return false;" target="'_top'">Motor</a></li>
<li id="tmasevasion" style="font-size: 11px;"><a href="#" onclick="javascript:vMasNoticiasS.mostrar('tmasevasion','d-evasion');return false;" target="'_top'">Evasión</a></li>
</ul></div>
<script type="text/javascript">
			//<![CDATA[
				$(sites[i].tag).addClassName('select');
				
			//]]>
			</script><div id="d-economista" class="masleidas">
<p class="mas-leidas-cint">Noticias más leidas</p>
<ul>
<li><a href="http://www.eleconomista.es/empresas-finanzas/noticias/6236461/11/14/Economia-Empresas-Telefonica-gana-2849-millones-hasta-septiembre-un-94-menos.html" target="_top">Más de lo esperado: Telefónica gana 2.849 millones hasta sep…</a></li>
<li><a href="http://www.eleconomista.es/firmas/noticias/6236407/11/14/Un-Estado-lento-ineficiente-e-injusto.html" target="_top">El Gobierno ha saqueado a los españoles para tapar los aguje…</a></li>
<li><a href="http://www.eleconomista.es/mercados-cotizaciones/noticias/6236547/11/14/El-Ibex-35-arranca-con-un-descenso-del-03-sobre-los-10300-puntos-.html" target="_top">El Ibex 35 baja y cede los 10.300 puntos </a></li>
<li><a href="http://www.eleconomista.es/banca-finanzas/noticias/6236530/11/14/El-regulador-britanico-multa-a-5-bancos-por-falta-control-en-el-mercado-de-divisas.html" target="_top">Los reguladores de EEUU, Reino Unido y Suiza condenan a cinc…</a></li>
<li><a href="http://www.eleconomista.es/mercados-cotizaciones/noticias/6236469/11/14/Las-bolsas-dan-senales-de-agotamiento-a-las-puertas-de-sus-resistencias.html" target="_top">Las bolsas dan señales de agotamiento a las puertas de sus r…</a></li>
</ul>
<div class="sep-mas1"></div>
<div class="sigue" style="margin:5px 0 8px"><a target="_top" href="http://www.eleconomista.es/">Más noticias</a></div>
</div>
<div id="d-ecodiario" class="masleidas">
<p class="mas-leidas-cint">Noticias más leidas</p>
<ul>
<li><a href="http://ecodiario.eleconomista.es/futbol/noticias/6236452/11/14/El-Bara-molesto-con-el-reencuentro-de-Pique-con-Guardiola.html" target="_top">El Barça, molesto con el reciente reencuentro entre Gerard P…</a></li>
<li><a href="http://ecodiario.eleconomista.es/politica/noticias/6236531/11/14/Manos-Limpias-denuncia-a-los-negociaciadores-secretos-del-9N-Pedro-Arriola-y-Jose-Enrique-Serrano-y-Joan-Rigol.html" target="_top">Manos Limpias denuncia al asesor de Rajoy, Pedro Arriola</a></li>
<li><a href="http://ecodiario.eleconomista.es/futbol/noticias/6236500/11/14/Vicente-Del-Bosque-lleva-a-cabo-con-Espana-algo-mas-que-una-dulce-transicion.html" target="_top">Vicente Del Bosque lleva a cabo con España algo más que una …</a></li>
<li><a href="http://ecodiario.eleconomista.es/espana/noticias/6236389/11/14/Donde-esta-Rajoy-o-la-eterna-pregunta.html" target="_top">Dónde está Rajoy o la eterna pregunta</a></li>
<li><a href="http://ecodiario.eleconomista.es/sociedad/noticias/6236574/11/14/La-momia-en-el-tejado-de-la-Facultad-de-Medicina-que-trae-de-cabeza-a-la-Complutense.html" target="_top">La momia en el tejado de la Facultad de Medicina que trae de…</a></li>
</ul>
<div class="sep-mas1"></div>
<div class="sigue" style="margin:5px 0 8px"><a target="_top" href="http://ecodiario.eleconomista.es/index.html">Más noticias</a></div>
</div>
<div id="d-ecoteuve" class="masleidas">
<p class="mas-leidas-cint">Noticias más leidas</p>
<ul>
<li><a href="http://ecoteuve.eleconomista.es/programas/noticias/6236506/11/14/Bustamante-estrella-de-la-Navidad-de-TVE-.html" target="_top">Bustamante, estrella de la Navidad en TVE </a></li>
<li><a href="http://ecoteuve.eleconomista.es/programas/noticias/6236599/11/14/Pablo-Iglesias-reconoce-hablar-con-un-exceso-de-arrogancia-.html" target="_top">Pablo Iglesias reconoce hablar con un "exceso de arrogancia"…</a></li>
<li><a href="http://ecoteuve.eleconomista.es/audiencias/noticias/6236503/11/14/El-Rey-se-despide-de-Telecinco-ante-1931000-espectadores-un-107-de-la-audiencia.html" target="_top">'El Rey' se despide de Telecinco ante 1.931.000 espectadores…</a></li>
<li><a href="http://ecoteuve.eleconomista.es/ecoteuve/television/noticias/6236526/11/14/Telecinco-estrena-la-taquillera-Lo-imposible-el-martes-18-de-noviembre.html" target="_top">Telecinco estrena la taquillera 'Lo imposible' el martes 18 …</a></li>
<li><a href="http://ecoteuve.eleconomista.es/programas/noticias/6234806/11/14/RTVE-cierra-la-venta-de-los-Estudios-Bunuel-por-35-millones-de-euros.html" target="_top">RTVE cierra la venta de los Estudios Buñuel por 35 millones …</a></li>
</ul>
<div class="sep-mas1"></div>
<div class="sigue" style="margin:5px 0 8px"><a target="_top" href="http://ecoteuve.eleconomista.es/index.html">Más noticias</a></div>
</div>
<div id="d-motor" class="masleidas">
<p class="mas-leidas-cint">Noticias más leidas</p>
<ul>
<li><a href="http://www.eleconomista.es/ecomotor/motor/noticias/6236532/11/14/Los-diez-mejores-coches-de-segunda-mano-para-estudiantes-por-menos-de-2000-euros.html" target="_top">Diez coches de segunda mano para estudiantes por menos de 2.…</a></li>
<li><a href="http://www.eleconomista.es/ecomotor/motor-clasico/noticias/6234683/11/14/Buick-Streamliner-1948-el-tesoro-perdido-en-el-desierto-de-Arizona.html" target="_top">Buick Streamliner 1948: el tesoro perdido en el desierto de …</a></li>
<li><a href="http://www.eleconomista.es/ecomotor/novedades/noticias/6236573/11/14/Citron-DS-3-Racing-nuevo-diseno-y-207-CV-bajo-el-capo.html" target="_top">Citroën DS 3 Racing: nuevo diseño y 207 CV bajo el capó</a></li>
<li><a href="http://www.eleconomista.es/ecomotor/motor/noticias/6236630/11/14/Es-posible-ahorrar-6000-euros-al-ano-compartiendo-coche-en-vez-de-tener-uno.html" target="_top">¿Es posible ahorrar 6.000 euros al año compartiendo coche en…</a></li>
<li><a href="http://www.eleconomista.es/ecomotor/formula-1/noticias/6233347/11/14/Fernando-Alonso-y-McLaren-los-detalles-del-acuerdo-y-razones-para-el-optimismo.html" target="_top">Fernando Alonso y McLaren: los detalles del acuerdo y razone…</a></li>
</ul>
<div class="sep-mas1"></div>
<div class="sigue" style="margin:5px 0 8px"><a target="_top" href="http://www.eleconomista.es/ecomotor/index.html">Más noticias</a></div>
</div>
<div id="d-evasion" class="masleidas">
<p class="mas-leidas-cint">Noticias más leidas</p>
<ul>
<li><a href="http://www.eleconomista.es/evasion/tendencias/noticias/6223897/11/14/Dejate-bigote-en-noviembre.html" target="_top">Déjate bigote en noviembre</a></li>
<li><a href="http://www.eleconomista.es/evasion/tendencias/noticias/6235616/11/14/Un-avion-inconfundible-venido-del-futuro-el-Avanti-EVO.html" target="_top">Un avión inconfundible venido del futuro: el Avanti EVO</a></li>
<li><a href="http://www.eleconomista.es/evasion/moda-y-complementos/noticias/6228172/11/14/Saca-tu-lado-mas-sport.html" target="_top">Saca tu lado más sport</a></li>
<li><a href="http://www.eleconomista.es/evasion/gente-y-estilo/noticias/6235104/11/14/Que-le-ha-pasado-al-pelo-de-Robert-Pattinson.html" target="_top">¿Qué le ha pasado al pelo de Robert Pattinson?</a></li>
<li><a href="http://www.eleconomista.es/evasion/gente-y-estilo/noticias/6235160/11/14/Jennifer-Lawrence-lo-ensena-todo-por-su-dificil-vestuario.html" target="_top">Jennifer Lawrence lo 'enseña todo' por su difícil vestuario</a></li>
</ul>
<div class="sep-mas1"></div>
<div class="sigue" style="margin:5px 0 8px"><a target="_top" href="http://www.eleconomista.es/evasion/index.html">Más noticias</a></div>
</div>
<script type="text/javascript">
			//<![CDATA[
				for (i=0; i<sites.length; i++) {
					if (sites[i].site!=idSite ) {
//						console.log (sites[i].id);
						vMasNoticiasS.ocultar(sites[i].id);
					}
				}

			//]]>
			</script>
</div></div>
<div mod="4586">
<div class="masleidas masleidas-ame">
<div class="ame-i"><a href="http://www.eleconomistaamerica.com"><img width="186" src="http://s01.s3c.es/imag3/logos/america/logo7.png" alt=""></a></div>
<ul class="pest-ame" style="padding-left: 35px !important;">
<li class="sel"><a href="http://www.eleconomistaamerica.com/todas-las-noticias/index.html?utm_source=crosslink&amp;utm_medium=ee_ultimas">Más leídas</a></li>
<li><a href="http://www.eleconomistaamerica.com/indices-latinoamericanos/index.html?utm_source=crosslink&amp;utm_medium=ee_ultimas">Bolsas Latam</a></li>
</ul>
<ul>
<li><div class="noticia-ult"><h2><a href="http://www.economiahoy.mx/sociedad-eAm-mexico/noticias/6236014/11/14/Video-Una-chica-finge-estar-borracha-y-varios-hombres-quieren-aprovechar-la-situacion-.html?utm_source=crosslink&amp;utm_medium=ee_masleidas">Vídeo | Una chica finge estar borracha y varios hombres quie…</a></h2></div></li>
<li><div class="noticia-ult"><h2><a href="http://www.economiahoy.mx/nacional-eAm-mx/noticias/6236173/11/14/Peritos-argentinos-dicen-que-restos-de-las-fosas-no-son-de-normalistas.html?utm_source=crosslink&amp;utm_medium=ee_masleidas">Restos de algunas fosas de Iguala no son de normalistas: per…</a></h2></div></li>
<li><div class="noticia-ult"><h2><a href="http://www.economiahoy.mx/telecomunicacion-tecnologia-mx/noticias/6235868/11/14/Cuales-son-las-diferencias-entre-Apple-TV-y-el-Chromecast-de-Google.html?utm_source=crosslink&amp;utm_medium=ee_masleidas">¿Cuáles son las diferencias entre el Apple TV y el Chromecas…</a></h2></div></li>
<li><div class="noticia-ult"><h2><a href="http://www.economiahoy.mx/futbol-eAm-mx/noticias/6235641/11/14/Asi-llama-Cristiano-Ronaldo-a-Messi-entre-su-circulo-de-amigos-del-Madrid.html?utm_source=crosslink&amp;utm_medium=ee_masleidas">Así llama Cristiano Ronaldo a Messi entre su círculo de amig…</a></h2></div></li>
<li><div class="noticia-ult"><h2><a href="http://www.economiahoy.mx/life-style-eAm-mx/noticias/6235781/11/14/Las-gemelas-Olsen-ya-no-son-gemelas.html?utm_source=crosslink&amp;utm_medium=ee_masleidas">Las gemelas Olsen ya no son gemelas</a></h2></div></li>
</ul>
</div>
<script type="text/javascript">
			s=$(document.body).readAttribute('s');
			$("s_"+s).addClassName("active");
		</script>
</div>
<div mod="1633" class="b300">
<script language="javascript">//<![CDATA[
			sz = "300x251" ;
			st = "eleconomista.es" ;
			std = "http://ad.es.doubleclick.net/" ;
			kw = "";
	//		randnum = old_randnum;
			 sect ="6103473";
			document.write( '<SCR'+'IPT language="JavaScript" SRC="'+std+'adj/'+st+'/economista_noticias_emprendedores_pymes;sect='+sect+';tile=6;kw='+kw+';sz='+sz+';ord='+randnum+'?"></SCR'+'IPT>' ) ;
		//]]></script><noscript><a href="http://ad.doubleclick.net/jump/eleconomista.es/economista_noticias_emprendedores_pymes;sect=6103473;tile=6;kw=;sz=300x251;ord=123456789?" target="_blank" rel="nofollow"><img src="http://ad.doubleclick.net/ad/eleconomista.es/economista_noticias_emprendedores_pymes;sect=6103473;tile=6;kw=;sz=300x251;ord=123456789?" width="300" height="250" border="0"></a></noscript>
</div>
<div class="d4"><div id="contenedor_galeria_3100"><div class="foto-rot" id="caja_3100_1"><div mod="3100"><table class="tablapeq tabla-prima">
<caption>Prima de Riesgo</caption>
<tr>
<th>País</th>
<th>Precio</th>
<th>Puntos</th>
<th>%</th>
</tr>
<tr>
<td class="pri-esp"><a href="/prima-riesgo/espana" title="Prima de riesgo de España">España</a></td>
<td class="pri-dat">127,02</td>
<td class="cot1">-1,77</td>
<td class="cot1">-1,37%</td>
</tr>
<tr>
<td class="pri-fr"><a href="/prima-riesgo/francia" title="Prima de riesgo de Francia">Francia</a></td>
<td class="pri-dat">34,57</td>
<td class="cot1">-1,00</td>
<td class="cot1">-2,81%</td>
</tr>
<tr>
<td class="pri-it"><a href="/prima-riesgo/italia" title="Prima de riesgo de Italia">Italia</a></td>
<td class="pri-dat">150,82</td>
<td class="cot1">-0,78</td>
<td class="cot1">-0,51%</td>
</tr>
<tr>
<td class="pri-gr"><a href="/prima-riesgo/grecia" title="Prima de riesgo de Grecia">Grecia</a></td>
<td class="pri-dat">733,65</td>
<td class="cot-1">+11,08</td>
<td class="cot-1">+1,53%</td>
</tr>
<tr>
<td class="pri-pt"><a href="/prima-riesgo/portugal" title="Prima de riesgo de Portugal">Portugal</a></td>
<td class="pri-dat">240,34</td>
<td class="cot1">-5,95</td>
<td class="cot1">-2,42%</td>
</tr>
</table></div></div></div></div>
<div class="m-pago m-ecot">
<script type="text/javascript" src="http://www.eleconomista.es/js/vticker.js"></script><h3 style="width:298px; padding:5px 0px; margin-left:5px;">
<a href="http://www.eleconomista.es/ecotrader/"><img src="http://s01.s3c.es/imag3/etp/ecotrader-neg3.gif" alt="Ecotrader"></a><span style="float: right; padding-right: 5px; padding-top: 13px;"><a href="http://www.eleconomista.es/mercados-cotizaciones/noticias/2694285/12/10/Ecotraderes-da-un-paso-mas-y-anuncia-ofertas-increibles-por-su-gran-acogida.html" style="font-size: 12px; color: #fff; text-decoration: none;">
			· Vea precios</a></span>
</h3>
<div class="m-ecto-ts" style="background:none;margin:0 10px">
<img src="http://s01.s3c.es/imag3/etp/ico-flecha.png"><a style="color:#666" href="http://www.eleconomista.es/ecotrader/recomendaciones/">Tabla de seguimiento</a>
</div>
<div class="m-ecto-ts" style="padding:1px; margin:0 10px"></div>
<div class="sr-pago">
<h4>Principales noticias:</h4>
<div id="outputV" class="m-ecot-not" style="position: relative; overflow: hidden">
<div class="m-ecot-not-int" style="position: absolute; width: 100%" id="outputV1"></div>
<div class="m-ecot-not-int" style="position: absolute; width: 100%; visibility: hidden;" id="outputV2"></div>
<ul id="list_ul_text" style="display:none;">
<li>
<h5><a href="http://www.eleconomista.es/ecotrader-tradings">Números rojos</a></h5>
<p>Los principales índices de referencia europeos volvieron ayer a a aproximarse a la parte alta del rango lateral en …</p>
</li>
<li>
<h5><a href="http://www.eleconomista.es/ecotrader-premium-analisis-tecnico/noticias/6236538/11/14/El-Nikkei-busca-los-18300.html">El Nikkei busca los 18.300</a></h5>
<p> </p>
</li>
<li>
<h5><a href="http://www.eleconomista.es/ecotrader-tradings">Preapertura mercado europeo</a></h5>
<p>Se espera una apertura a la baja en Europa tras las ganancias de ayer. El futuro del Eurostoxx 50 cede un -0,29% y …</p>
</li>
<li>
<h5><a href="http://www.eleconomista.es/ecotrader-renta-variable/noticias/6236493/11/14/La-situacion-lateral-que-define-Europa-sigue-vigente.html">La situación lateral que define Europa s…</a></h5>
<p>Las ganancias no alteran la tendencia lateral que definen las curvas de precios de los selectivos de referencia en …</p>
</li>
<li>
<h5><a href="http://www.eleconomista.es/ecotrader-renta-variable/noticias/6236043/11/14/Wall-Street-se-enfrenta-a-un-agotamiento-comprador-en-plena-resistencia-clave.html">Wall Street se enfrenta a un agotamiento…</a></h5>
<p>Lo de EEUU es un suma y sigue constante. Desde hace tres semanas no se cansa de marcar nuevos y más altos máximos h…</p>
</li>
<li>
<h5><a href="http://www.eleconomista.es/ecotrader-premium-divisas/noticias/6235995/11/14/Intenta-rebotar.html">Intenta rebotar</a></h5>
<p> </p>
</li>
<li>
<h5><a href="http://www.eleconomista.es/ecotrader-renta-variable/noticias/6235993/11/14/Sin-signos-de-debilidad-mientras-este-sobre-15080.html">Sin signos de debilidad mientras esté so…</a></h5>
<p> </p>
</li>
<li>
<h5><a href="http://www.eleconomista.es/ecotrader-renta-variable/noticias/6235991/11/14/Signos-de-cierto-agotamiento-comprador.html">Signos de cierto agotamiento comprador</a></h5>
<p> </p>
</li>
<li>
<h5><a href="http://www.eleconomista.es/ecotrader-renta-variable/noticias/6235990/11/14/La-situacion-lateral-se-mantiene.html">La situación lateral se mantiene</a></h5>
<p> </p>
</li>
<li>
<h5><a href="http://www.eleconomista.es/ecotrader-renta-variable/noticias/6235982/11/14/Sigue-por-debajo-de-resistencias.html">Sigue por debajo de resistencias</a></h5>
<p> </p>
</li>
<li>
<h5><a href="http://www.eleconomista.es/ecotrader-renta-variable/noticias/6235981/11/14/Repsol-vender-si-pierde-los-1710-euros.html">Repsol: vender si pierde los 17,10 euros…</a></h5>
<p> </p>
</li>
<li>
<h5><a href="http://www.eleconomista.es/ecotrader-renta-variable/noticias/6235880/11/14/Claves-del-miercoles-produccion-industrial-de-la-eurozona-y-resultados-de-Telefonica-y-Macys.html">Claves del miércoles: producción industr…</a></h5>
<p>Mañana destaca la presentación de la producción industrial de la eurozona durante el mes de septiembre. Además, se …</p>
</li>
</ul>
<div class="m-ecot-flechas" align="right">
<a href="" onclick="ticker.previousCurrentNews(); return( false );"><img src="http://s01.s3c.es/imag3/iconos/flecha-roja-arriba.gif" alt="Flecha Arriba"></a><a href="" onclick="ticker.nextCurrentNews(); return( false );"><img src="http://s01.s3c.es/imag3/iconos/flecha-roja-abajo.gif" alt="Flecha Aabajo"></a>
</div>
</div>
<h4 style="margin-top: 10px;">Invertir en:</h4>
<div style="text-align: center; font-size: 11px; border-bottom: 1px solid #df0d00; border-top: 1px solid #df0d00; margin: 0 0 10px 0; padding: 2px 0; background: #ff0f00; color: #fff;">
<a href="http://www.eleconomista.es/ecotrader/renta-variable/index.html" style="color: #fff; text-decoration: none;">Renta Variable</a> | <a href="http://www.eleconomista.es/ecotrader/renta-fija/index.html" style="color: #fff; text-decoration: none;">Renta Fija</a> | <a href="http://www.eleconomista.es/ecotrader/divisas/" style="color: #fff; text-decoration: none;">Divisas</a> | <a href="http://www.eleconomista.es/ecotrader/materias-primas/index.html" style="color: #fff; text-decoration: none;">M. Primas</a>
</div>
</div>
<div class="m-ecot-fin">
<a href="http://www.eleconomista.es/ecotrader/publico.php">Conozca Ecotrader</a><a href="http://www.eleconomista.es/ecotrader/publico.php">Dése de alta</a>
</div>
</div><script type="text/javascript">//<![CDATA[
		var ticker = new news_VTicker('outputV', 'list_ul_text', 3000);
	//]]></script>
<div mod="535"><div class="popular">
<script> 
				vMasCotizaciones = new masNoticias('masinteres','tmasinteres');
			</script><div id="pestPopular"><ul class="pest">
<li id="tmasvistas"><a href="#" onClick="javascript:vMasCotizaciones.mostrar('tmasvistas','masvistas');return false;">+ vistos</a></li>
<li id="tmasinteres" class="select"><a href="#" onClick="javascript:vMasCotizaciones.mostrar('tmasinteres','masinteres');return false;">interesa - no interesa</a></li>
</ul></div>
<div id="masvistas" class="mastock"><ul style="width:90%">
<li class="C6"><a href="/empresa/BANKIA">BANKIA</a></li>
<li class="C6"><a href="/empresa/BBVA">BBVA</a></li>
<li class="C6"><a href="/indice/BOVESPA-SAO-PAOLO">BOVESPA SAO PAULO</a></li>
<li class="C7"><a href="/empresa/CAIXABANK">CAIXABANK</a></li>
<li class="C6"><a href="/indice/DAX-30">DAX</a></li>
<li class="C5"><a href="/indice/DOW-JONES">DOW JONES</a></li>
<li class="C7"><a href="/empresa/ENDESA">ENDESA</a></li>
<li class="C6"><a href="/cruce/EURJPY">EURJPY</a></li>
<li class="C5"><a href="/indice/EUROSTOXX-50">EURO STOXX 50®</a></li>
<li class="C6"><a href="/cruce/EURUSD">EURUSD</a></li>
<li class="C7"><a href="/empresa/IAG-IBERIA">IAG (IBERIA)</a></li>
<li class="C6"><a href="/empresa/IBERDROLA">IBERDROLA</a></li>
<li class="C1"><a href="/indice/IBEX-35">IBEX 35</a></li>
<li class="C6"><a href="/indice/NASDAQ-100">NASDAQ 100</a></li>
<li class="C5"><a href="/empresa/POPULAR">POPULAR</a></li>
<li class="C5"><a href="/empresa/REPSOL">REPSOL</a></li>
<li class="C7"><a href="/indice/S-P-500">S P 500</a></li>
<li class="C6"><a href="/empresa/SACYR">SACYR</a></li>
<li class="C5"><a href="/empresa/SANTANDER">SANTANDER</a></li>
<li class="C5"><a href="/empresa/TELEFONICA">TELEFONICA</a></li>
</ul></div>
<div id="masinteres" class="mastock">
<h5 style="text-align:center">Subidas y caídas en el más vistos:</h5>
<ul>
<li>
<a href="/empresa/ABENGOA-CLB">ABENGOA CL.B</a><small>(+101 puestos)<br>Sube del 135 al 34</small>
</li>
<li>
<a href="/cruce/EURJPY">EURJPY</a><small>(+12 puestos)<br>Sube del 22 al 10</small>
</li>
<li>
<a href="/indice/NASDAQ-100">NASDAQ 100</a><small>(+11 puestos)<br>Sube del 20 al 9</small>
</li>
<li>
<a href="/empresa/ENDESA">ENDESA</a><small>(+16 puestos)<br>Sube del 35 al 19</small>
</li>
<li>
<a href="/indice/DOW-JONES">DOW JONES</a><small>(+7 puestos)<br>Sube del 9 al 2</small>
</li>
</ul>
<ul>
<li>
<a href="/empresa/CAIXABANK">CAIXABANK</a><small class="rojo">(-7 puestos)<br>Cae del 10 al 17</small>
</li>
<li>
<a href="/indice/NIKKEI">NIKKEI 225</a><small class="rojo">(-10 puestos)<br>Cae del 18 al 28</small>
</li>
<li>
<a href="/indice/IGBM">
										IGBM
									</a><small class="rojo">(-9 puestos)<br>Cae del 16 al 25</small>
</li>
<li>
<a href="/empresa/Nutresa">Nutresa</a><small class="rojo">(-106 puestos)<br>Cae del 60 al 166</small>
</li>
<li>
<a href="/empresa/ALFA-SAB-DE-CV-21701">ALFA S.A.B. DE CV</a><small class="rojo">(-204 puestos)<br>Cae del 88 al 292</small>
</li>
</ul>
</div>
<script>
				vMasCotizaciones.ocultar('masvistas');
			</script>
</div></div>
<div class="sep"></div>

				<div class="sep" style="margin-top: 10px;"></div>
				<div class="popular"><div id="ultimas" class="flash-int-ev">
<h3>El Flash de Evasión</h3>
<ul>
<li style="margin-bottom: 2px;"><h4 style="font-size: 14px;">
<span class="hora">08:50</span><a href="http://www.eleconomista.es/evasion/noticias/6236528/11/14/La-periodista-Victoria-Prego-premiada-con-el-Ouresania-2014.html">La periodista Victoria Prego, premiada con el 'Ouresanía 2014'</a>
</h4></li>
<li style="margin-bottom: 2px;"><h4 style="font-size: 14px;">
<span class="hora">08:18</span><a href="http://www.eleconomista.es/evasion/noticias/6236498/11/14/La-Infanta-Pilar-recoge-el-Premio-Foro-de-Madrid-Tercer-Milenio.html">La Infanta Pilar recoge el Premio 'Foro de Madrid Tercer Milenio'</a>
</h4></li>
<li style="margin-bottom: 2px;"><h4 style="font-size: 14px;">
<span class="hora">11/11</span><a href="http://www.eleconomista.es/evasion/noticias/6235408/11/14/Tori-Spelling-poco-a-poco-va-recuperando-su-vida-cotidiana-tras-su-gran-susto-con-el-ebola.html">Tori Spelling poco a poco va recuperando su vida cotidiana tras su gran susto con el ébola</a>
</h4></li>
<li style="margin-bottom: 2px;"><h4 style="font-size: 14px;">
<span class="hora">11/11</span><a href="http://www.eleconomista.es/evasion/noticias/6235405/11/14/Britney-Spears-ha-encontrado-de-nuevo-el-amor-y-ha-querido-presentarlo-a-traves-de-Instagram.html">Britney Spears ha encontrado de nuevo el amor y ha querido presentarlo a través de Instagram</a>
</h4></li>
<li style="margin-bottom: 2px;"><h4 style="font-size: 14px;">
<span class="hora">11/11</span><a href="http://www.eleconomista.es/evasion/noticias/6235373/11/14/Pablo-Alboran-presenta-su-ultimo-disco-Terral.html">Pablo Alborán presenta su último disco: 'Terral'</a>
</h4></li>
</ul>
<div class="sep"></div>
<div class="sigue" style="margin: 10px 10px 5px 0;"><a href="/noticias/categoria/noticias/fuente/europapress-chance" class="mas-b">Más noticias</a></div>
</div></div>

		</div>
		<div class="sep"></div><div mod="1502"><div class="cat"><ul>
<li class="d">Categorías:</li>
<li><a href="/noticias/flash">Flash</a></li>
<li><a href="/noticias/gestion-empresarial">Pymes y Emprendedores</a></li>
<li><a href="/noticias/seleccion-ee">Seleccion eE</a></li>
<li><a href="/noticias/flash-emprendedores">Flash Emprendedores</a></li>
<li><a href="/noticias/emprendedores-pymes">Emprendedores-Pymes</a></li>
<li class="d">Hemeroteca:</li>
<li><a href="http://www.eleconomista.es/noticias/fuente/el-economista/2014/septiembre/2quin">El Economista 16-30 Septiembre 2014</a></li>
</ul></div></div>
<div id="publicidad_120" style="position:absolute; width:120px; height:600px; z-index:1000; top: 32px; margin-left: 1000px">
<script type="text/javascript">//<![CDATA[
		sz = "120x600" ;
		st = "eleconomista.es" ;
		std = "http://ad.es.doubleclick.net/" ;
		kw = "";
		 sect ="6103473";
		document.write( '<SCR'+'IPT language="JavaScript" SRC="'+std+'adj/'+st+'/economista_noticias_emprendedores_pymes;sect='+sect+';tile=7;kw='+kw+';sz='+sz+';ord='+randnum+'?"></SCR'+'IPT>' ) ;
		
		/*
		s.eVar21="120x600";
		*/
		function comprobarTamano()
		{
			if ( (screen.width > 1024) && (document.body.clientWidth > 1144) ) {
				document.getElementById('publicidad_120').style.visibility = 'visible';
			}
			else {
				document.getElementById('publicidad_120').style.visibility = 'hidden';
			}
		}
		
		window.onresize = comprobarTamano ;
		comprobarTamano() ;
		//]]></script><noscript><a href="http://ad.doubleclick.net/jump/eleconomista.es/economista_noticias_emprendedores_pymes;sect=6103473;tile=7;kw=;sz=120x600;ord=123456789?" target="_blank" rel="nofollow"><img src="http://ad.doubleclick.net/ad/eleconomista.es/economista_noticias_emprendedores_pymes;sect=6103473;tile=7;kw=;sz=120x600;ord=123456789?" width="120" height="600" border="0"></a></noscript>
</div>
<div class="sep"></div><div id="pie">
<div class="pie">
<div class="p-logo">
<img style="float:left" src="http://s01.s3c.es/imag3/logo-h1.gif"><ul style="width:auto; margin:0; float:right; text-align:left">
<li class="descrip-pie">¿Es usuario de elEconomista.es?</li>
<li style="display:inline"><a href="http://www.eleconomista.es/registro/alta.php">Regístrese aquí</a></li> | <li style="display:inline"><a href="http://www.eleconomista.es/registro/alta.php">Dése de alta</a></li>
</ul>
</div>
<div class="sep" style="margin:0; border:none; border-bottom:1px solid #ccc"></div>
<ul style="margin-top: 20px;">
<li class="descrip-pie"><a class="img-l" href="http://www.eleconomista.es"><img src="http://s01.s3c.es/imag3/logos/america/eees.png" alt="eleconomista.es"></a></li>
<li><a href="http://ecoteuve.eleconomista.es/">Ecoteuve</a></li>
<li><a href="http://ecodata.eleconomista.es">Datos Macro</a></li>
<li><a href="http://ecodiario.eleconomista.es">Información general</a></li>
<li><a href="http://ecoaula.eleconomista.es">Formación y empleo</a></li>
<li><a href="http://ecodiario.eleconomista.es/ecomotor/">Información motor</a></li>
<li><a href="http://www.eleconomista.es/evasion/index.html">Estilo y Tendencias</a></li>
<li><a href="http://www.eleconomista.es/especiales/turismo-viajes/">Turismo y viajes</a></li>
</ul>
<ul style="width:175px; margin-top: 20px;">
<li class="descrip-pie"><a class="img-l" href="http://www.eleconomistaamerica.com"><img src="http://s02.s3c.es/imag3/logos/america/eeam.png" alt="elEconomistaamerica.com"></a></li>
<li><a href="http://www.eleconomistaamerica.com.ar">Argentina</a></li>
<li><a href="http://www.eleconomistaamerica.com.br">Brasil</a></li>
<li><a href="http://www.eleconomistaamerica.cl">Chile</a></li>
<li><a href="http://www.eleconomistaamerica.co">Colombia</a></li>
<li><a href="http://www.eleconomistaamerica.mx">México</a></li>
<li><a href="http://www.eleconomistaamerica.pe">Perú</a></li>
</ul>
<ul>
<li class="descrip-pie">Invierta con eE</li>
<li><a href="http://www.eleconomista.es/ecotrader/">Eco<span style="color:#f00;">trader.es </span></a></li>
<li><a href="http://www.eleconomista.es/monitor"><span style="color: #f60;">el</span>Monitor</a></li>
<li><a href="http://www.eleconomista.es/indice/eco10">Eco 10</a></li>
<li><a href="http://www.eleconomista.es/indice/eco30">Eco 30</a></li>
<li><a href="http://www.eleconomista.es/mercados-cotizaciones/ecodividendo/index.html">Ecodividendo</a></li>
<li style="margin-top: 20px;">
<li class="descrip-pie"><a href="http://www.eleconomista.es/kiosco/" style="font-weight:bold;">Diario y revistas</a></li>
<li><a href="http://www.eleconomista.es/kiosco/">Kiosco</a></li>
<li><a href="http://www.eleconomista.es/kiosco/">Revistas digitales</a></li>
<li><a href="http://www.eleconomista.es/tarifas/">Suscripción al diario</a></li>
<li><a href="http://www.eleconomista.es/elsuperlunes/">elSuperLunes</a></li>
<li><a href="http://www.eleconomista.es/hemeroteca/">Ed. PDF + Hemeroteca</a></li>
<li><a href="http://www.eleconomista.es/ecotablet/">Ecotablet</a></li>
</ul>
<ul style="width:110px">
<li class="descrip-pie">Redes sociales</li>
<li>
<img style="padding:2px 5px 0 0" width="15" height="15" src="http://s01.s3c.es/imag3/iconos/facebook.png"><a href="http://es-es.facebook.com/elEconomista.es">Facebook</a>
</li>
<li>
<img style="padding:2px 5px 0 0" width="15" height="15" src="http://s01.s3c.es/imag3/iconos/twitter.png"><a href="http://twitter.com/eleconomistaes">Twitter</a>
</li>
<li>
<img style="padding:2px 5px 0 0" width="15" height="15" src="http://s02.s3c.es/imag3/iconos/gplus1.png"><a href="https://plus.google.com/+eleconomistaes/">Google+</a>
</li>
<li style="margin-top: 20px;">
<li class="descrip-pie"><a href="http://www.editorialecoprensa.es" style="font-weight:bold;">Editorial Ecoprensa</a></li>
<li><a href="http://www.eleconomista.es/quienes/somos.php">Quiénes somos</a></li>
<li><a href="http://www.eleconomista.es/publicidad/">Publicidad</a></li>
<li><a href="http://www.eleconomista.es/archivo-noticias/index.html">Archivo</a></li>
</ul>
<ul style="width:110px">
<li class="descrip-pie">Servicios</li>
<li><a href="http://www.eleconomista.es/alertas/"> Alertas móvil</a></li>
<li><a href="http://ecodiario.eleconomista.es/cartelera/">Cartelera</a></li>
<li>
<a href="http://www.eleconomista.es/el-tiempo/">El tiempo</a><a href="http://ad.doubleclick.net/clk;248020731;50676649;j?http://www.weatherpro.eu/es/home.html" rel="nofollow"><img src="http://s03.s3c.es/imag3/logos/wpro.png" alt="Weather Pro" style="margin-left: 3px;"></a>
</li>
<li><a href="http://www.eleconomista.es/evasion/libros/">Libros</a></li>
<li><a href="http://listas.eleconomista.es/">Listas</a></li>
<li><a href="http://www.eleconomista.es/rss/index.php"><img style="padding:2px 5px 0 0" width="12" height="12" src="http://s01.s3c.es/imag/rss.gif">RSS</a></li>
<li><a href="http://coleccion.eleconomista.es">Ecoleccionista</a></li>
</ul>
<ul style="width:175px; margin-right:0">
<li class="descrip-pie"><a style="font-weight:bold" href="http://www.eleconomista.es/especiales/index.html">Especiales</a></li>
<li><a href="http://ecodiario.eleconomista.es/deportes/mundial-baloncesto/index.php">Especial Baloncesto 2014</a></li>
<li><a href="http://ecodiario.eleconomista.es/deportes/mundial/index.php">Especial Mundial Brasil 2014</a></li>
<li><a href="http://www.eleconomista.es/especiales/oscars/">Gala de los Oscars 2014</a></li>
<li><a href="http://www.eleconomista.es/especiales/premios-goya/">Premios Goya 2014</a></li>
<li><a href="http://www.eleconomista.es/ecomotor/formula-1/index.html">Fórmula 1</a></li>
<li><a href="http://www.eleconomista.es/especiales/loteria-navidad/">Lotería de Navidad</a></li>
<li><a href="http://www.eleconomista.es/declaracion-renta/">Declaración de la Renta</a></li>
</ul>
<div class="sep" style="border:none; border-top:1px solid #ccc"></div>
<div class="pie-links">
<b style="color: #666;">Nuestros partners:</b><a style="font-weight:bold" href="http://www.eleconomista.es/CanalPDA/"> CanalPDA</a> | <a href="http://www.eleconomista.es/boxoffice/"><span style="color:#ba3b30; font-weight:bold">Boxoffice</span> - Industria del cine</a> | <a style="font-weight:bold" href="http://www.ilsole24ore.com/english">ilSole - English version</a> | 

<a style="font-weight:bold" href="http://empresite.eleconomista.es/" target="_blank">Empresite: España </a> - <a style="font-weight:bold" href="http://empresite.eleconomistaamerica.co/" target="_blank">Colombia </a> | <a style="font-weight:bold" href="http://administradores.eleconomista.es/" target="_blank">Administradores y Ejecutivos </a>
</div>
</div>
<div class="sep" style="border-color:#666; margin:2px 0"></div>
<div class="editorial"><p><a href="http://www.editorialecoprensa.es">Ecoprensa S.A.</a> - Todos los derechos reservados | <a href="http://www.eleconomista.es/politica-de-privacidad/">Nota Legal</a> | <a href="http://www.eleconomista.es/politica-de-privacidad/cookies.php">Política de cookies</a> | <a href="http://www.acens.com" target="_blank" rel="nofollow">Cloud Hosting en Acens</a> |<a href="http://www.paradigmatecnologico.com" rel="nofollow" target="_blank">Powered by Paradigma</a></p></div>
</div>
<div id="barra-nav" class="barra-inf">
<div mod="2086" class="bb">
<script language="javascript">//<![CDATA[
					sz = "550x20" ;
					st = "eleconomista.es" ;
					std = "http://ad.es.doubleclick.net/" ;
					kw = "";
					 sect ="6103473";
					document.write( '<SCR'+'IPT language="JavaScript" SRC="'+std+'adj/'+st+'/economista_noticias_emprendedores_pymes;sect='+sect+';tile=9;kw='+kw+';sz='+sz+';ord='+randnum+'?"></SCR'+'IPT>' ) ;
					
					
					document.write( '<SCR'+'IPT language="JavaScript" SRC="http://s01.s3c.es/js/eleconomista/cache/dockinbar.js"></SCR'+'IPT>' ) ;
					
	 				/* document.write('<link rel="stylesheet" href="/css3/barra.css" type="text/css"/>'); */
				//]]></script><noscript><a href="http://ad.doubleclick.net/jump/eleconomista.es/economista_noticias_emprendedores_pymes;sect=6103473;tile=9;kw=;sz=550x20;ord=123456789?" target="_blank" rel="nofollow"><img src="http://ad.doubleclick.net/ad/eleconomista.es/economista_noticias_emprendedores_pymes;sect=6103473;tile=9;kw=;sz=550x20;ord=123456789?" width="365" height="60" border="0"></a></noscript>
</div>
<ul class="bi">
<li id="ATecnico"><a href="http://www.eleconomista.es/analisis-tecnico" class="barra-enlace">A. Técnico</a></li>
<li id="Cartera"><a href="http://www.eleconomista.es/cartera/portada.php" class="barra-enlace">Su Cartera</a></li>
<li id="UltNoticias">
<a href="#" class="barra-enlace" onClick="return false;">Noticias</a><div class="barra-ventana">
<div class="ventana-cerrar"><img src="http://s01.s3c.es/imag3/iconos/i-cerrar4.gif" class="imagen-cerrar"></div>
<div class="ventana-contenido" id="contUltNoticias"><div class="loading"><p>Cargando Ultimas Noticias...</p></div></div>
</div>
</li>
<li id="Cotizaciones">
<a href="#" class="barra-enlace" onClick="return false;">Cotizaciones</a><div class="barra-ventana">
<div class="ventana-cerrar"><img src="http://s01.s3c.es/imag3/iconos/i-cerrar4.gif" class="imagen-cerrar"></div>
<div class="ventana-contenido">
<div class="ventana-contenido-g" id="contCotizaciones"><div class="loading"><p>Cargando Cotizaciones...</p></div></div>
<div class="ventana-contenido-e">
<a href="/ecotrader/"><span style="color:#000">· Eco</span><span style="color:#FF0000">trader.es</span> herramientas, estrategias de inversión</a><a href="/fichas-de-empresas/index.html">· Vea todas la cotizaciones de empresas</a><a href="/mercados-cotizaciones/index.html">· Todas las noticias de Bolsa y Mercados</a>
</div>
</div>
</div>
</li>
</ul>
</div><script type="text/javascript">
	 
	 			 
			if (/MSIE (\d+\.\d+);/.test(navigator.userAgent))
			{ 
			 var ieversion=new Number(RegExp.$1) // capture x.x portion and store as a number
			 if (ieversion==6)
			  document.getElementById("barra-nav").style.visibility = 'hidden';
			 }
			else if (navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i))
				document.getElementById("barra-nav").style.visibility = 'hidden';
			
	
	</script><script type="text/javascript">

		var UltNoticias = new dockElement('UltNoticias', {'title':'Ultimas Noticias', url_data:"/slices/bar-slice.php?modulo=1246", refreshPeriod:60, refreshDate:'empty'});

//		var Blogs = new dockElement('Blogs', {'title':'BLogs', url_data:"/portada/index.php?ModoAjax=1856,1124&AjaxJS=1", refreshPeriod:9000, refreshDate:'empty'});
		
		//var Blogs = new dockElement('Blogs', {'title':'BLogs', url_data:"/slices/bar-slice.php?modulo=1856", refreshPeriod:9000, refreshDate:'empty'});
		var Cotizaciones = new dockElement("Cotizaciones", {'title':'Cotizaciones', url_data:'/slices/bar-slice.php?modulo=2171', refreshPeriod:60, refreshDate:'empty'});	
//		var elementos = {'UltNoticias':UltNoticias, 'Blogs':Blogs, 'Cotizaciones':Cotizaciones};
		var elementos = {'UltNoticias':UltNoticias, 'Cotizaciones':Cotizaciones};
		dockBar = new dockBar(elementos);
	
		dockBar.setObservers();

</script>

	</div><script type="text/javascript"> var g_cPartnerId = "eleconomista.es-01";var g_cTheme = "eleconomista_1_0";</script><script type="text/javascript" charset="UTF-8" src="http://www.dixio.com/ifaces/dixioforyoursite/jsgic.js"></script>
<script type="text/javascript">// <![CDATA[
var _qq=document.createElement('script'),
_qf = 'Kku8', _qc = [],
_qh = document.getElementsByTagName('script')[0];
_qq.type = 'text/javascript';
_qq.setAttribute('src','http'+
(('https:'==document.location.protocol)?'s':'')+
'://www.intentshare.com/is/?f='+_qf);
_qh.parentNode.insertBefore(_qq, _qh);
// ]]></script>
					<script type="text/javascript">
					//<![CDATA[
						var eco = new ecoQuotes() ;eco.Anadir( "espana" , 0 ) ; eco.Anadir( "francia" , 0 ) ; eco.Anadir( "italia" , 0 ) ; eco.Anadir( "grecia" , 0 ) ; eco.Anadir( "portugal" , 0 ) ; 
						eco.Leer(0);
						eco.Leer(2);
						setInterval( function(){eco.Leer(2);}, 90000 ) ; //90 segundos
						setInterval( function(){eco.Leer(0);}, 90000 ) ; //90 segundos
					//]]>
					</script>
</body></html>