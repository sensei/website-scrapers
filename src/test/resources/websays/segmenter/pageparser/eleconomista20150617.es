<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<!-- saved from url=(0201)http://www.eleconomista.es/interstitial/volver/269617462/construccion-inmobiliario/noticias/6784723/06/15/La-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html#.Kku8pmt05Z0CK4x -->
<html xmlns="http://www.w3.org/199/xhtml" xml:lang="es" xmlns:fb="http://www.facebook.com/2008/fbml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><script src="./eco_files/count.php"></script><title>La Caixa cree que el pulso del sector inmobiliario comienza a recuperarse - elEconomista.es</title>
					<meta property="og:title" name="title" content="La Caixa cree que el pulso del sector inmobiliario comienza a recuperarse - elEconomista.es">
 
<meta property="og:description" name="description" content="Barcelona, 11 jun (EFE).- La Caixa Research, el servicio de estudios y anÃ¡lisis econÃ³mico de La Caixa, cree que &#39;el pulso del sector inmobiliario empieza a recuperarse&#39; y que &#39;el recorrido al alza de la demanda y la oferta (de vivienda) es amplio&#39;, aunque augura que la recuperaciÃ³n serÃ¡ &#39;gradual&#39; en ambos frentes.">

<meta property="og:image" name="image" content="http://s01.s3c.es/imag/efe/2015/06/11/20150611-10867172w.jpg">

<meta name="image_thumbnail" content="http://s01.s3c.es/imag/efe/2015/06/11/50x50_20150611-10867172w.jpg">
<meta http-equiv="X-UA-Compatible" content="IE=Edge"><meta name="keywords" content="CAIXA,SECTOR,INMOBILIARIO,COMIENZA,PULSO,RECUPERARSE">
			<meta name="distribution" content="global">
			<meta name="resource-type" content="document">
			<meta property="og:type" content="article">
			<meta name="robots" content="all">
			<meta name="author" content="elEconomista.es">
			<meta name="organization" content="Editorial Ecoprensa S.A.">
			<meta name="classification" content="World, Espanol, Paises, Espana, Noticias y medios, Periodicos, Economicos">
			<meta name="revisit-after" content="7 days"><meta http-equiv="refresh" content="360; url=/construccion-inmobiliario/noticias/6784723/06/15/La-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html"><meta name="date" content="2015-06-11T14:44:26+02:00">
<link rel="stylesheet" type="text/css" href="./eco_files/generales,carousel,eM-dcha,etp.471.cache.css" media="screen">
<link rel="stylesheet" type="text/css" href="./eco_files/print.css" media="print">
<script src="./eco_files/6809.js" async="" type="text/javascript"></script><script class="intentshare-asset" type="text/javascript">var img_settings={'cid':'Kku8','sid':'Kku829QDcmybcRGc86C36PtZ','image_services':'facebook,twitter,googleplus,linkedin,pinterest,email','image_sharing':!0,'image_sharing_exclude':'','container_restrict':'.principal','image_sharing_position':'tl','shorten_url':0}</script><script src="./eco_files/Kku8.js"></script><script src="./eco_files/teads-player.min.js"></script><script type="text/javascript" src="./eco_files/saved_resource"></script><script type="text/javascript" async="" src="./eco_files/lidar.js"></script><script type="text/javascript" async="" src="./eco_files/lidar.js"></script><script type="text/javascript" async="" src="./eco_files/lidar.js"></script><script type="text/javascript" async="" src="./eco_files/lidar.js"></script><script type="text/javascript" async="" src="./eco_files/lidar.js"></script><script type="text/javascript" async="" src="./eco_files/platform.js" gapi_processed="true"></script><script id="twitter-wjs" src="./eco_files/widgets.js"></script><script type="text/javascript" async="" src="./eco_files/lidar.js"></script><script type="text/javascript" async="" src="./eco_files/cx.js"></script><script type="text/javascript" async="" src="./eco_files/cx.js"></script><script src="./eco_files/cb=gapi.loaded_1" async=""></script><script src="./eco_files/wfavicon.min.js" id="wow-wfavicon" lang="es-ES"></script><script async="" src="./eco_files/all-v2.js"></script><script type="text/javascript" async="" src="./eco_files/plusone.js" gapi_processed="true"></script><script type="text/javascript" async="" src="./eco_files/lidar.js"></script><script async="" src="./eco_files/all-v1.js"></script><script type="text/javascript" async="" src="./eco_files/lidar.js"></script><script type="text/javascript" async="" src="./eco_files/lidar.js"></script><script async="" src="./eco_files/beacon.js"></script><script src="./eco_files/cb=gapi.loaded_0" async=""></script><script id="facebook-jssdk" src="./eco_files/sdk.js"></script><script type="text/javascript" language="javascript" src="./eco_files/jquery.min.js"></script>
<script type="text/javascript" src="./eco_files/cookiesdirective.v1.js" charset="utf-8"></script>
<script type="text/javascript" language="javascript" src="./eco_files/prototype.js"></script>
<script type="text/javascript" language="javascript" src="./eco_files/scriptaculous.js"></script><script type="text/javascript" src="./eco_files/builder.js"></script><script type="text/javascript" src="./eco_files/effects.js"></script><script type="text/javascript" src="./eco_files/dragdrop.js"></script><script type="text/javascript" src="./eco_files/controls.js"></script><script type="text/javascript" src="./eco_files/slider.js"></script><script type="text/javascript" src="./eco_files/sound.js"></script>
<script type="text/javascript" language="javascript" src="./eco_files/swfobject.js"></script>
<script type="text/javascript" language="javascript" src="./eco_files/effects.js"></script>
<script type="text/javascript" language="javascript" src="./eco_files/c18e2338813cf985712efbd86d0037e2.81.cache.js"></script>
<link href="http://www.eleconomista.es/rss/rss-flash-del-mercado.php" rel="alternate" type="application/rss+xml" title="elEconomista :: Flash del Mercado">
<link href="http://www.eleconomista.es/rss/rss-category.php?category=construccion-inmobiliario" rel="alternate" type="application/rss+xml" title="elEconomista :: Construccion Inmobiliario"><meta name="syndication-source" content="http://www.efe.com"><meta name="cXenseParse:title" content="La Caixa cree que el pulso del sector inmobiliario comienza a recuperarse"><meta name="cXenseParse:recs:articleid" content="6784723"><script type="text/javascript" src="./eco_files/plusone(1).js" gapi_processed="true">{lang: 'es'}</script><script src="./eco_files/secureAnonymousFramework"></script><style type="text/css">* html #li_ui_li_gen_1434535975073_0 a#li_ui_li_gen_1434535975073_0-link{height:1% !important;}#li_ui_li_gen_1434535975073_0{position:relative !important;overflow:visible !important;display:block !important;}#li_ui_li_gen_1434535975073_0 span{-webkit-box-sizing:content-box !important;-moz-box-sizing:content-box !important;box-sizing:content-box !important;}#li_ui_li_gen_1434535975073_0 a#li_ui_li_gen_1434535975073_0-link{border:0 !important;height:20px !important;text-decoration:none !important;padding:0 !important;margin:0 !important;display:inline-block !important;}#li_ui_li_gen_1434535975073_0 a#li_ui_li_gen_1434535975073_0-link:link, #li_ui_li_gen_1434535975073_0 a#li_ui_li_gen_1434535975073_0-link:visited, #li_ui_li_gen_1434535975073_0 a#li_ui_li_gen_1434535975073_0-link:hover, #li_ui_li_gen_1434535975073_0 a#li_ui_li_gen_1434535975073_0-link:active{border:0 !important;text-decoration:none !important;}#li_ui_li_gen_1434535975073_0 a#li_ui_li_gen_1434535975073_0-link:after{content:"." !important;display:block !important;clear:both !important;visibility:hidden !important;line-height:0 !important;height:0 !important;}#li_ui_li_gen_1434535975073_0 #li_ui_li_gen_1434535975073_0-logo{background-image:url(https://static.licdn.com/scds/common/u/images/apps/connect/sprites/sprite_connect_v14.png) !important;background-position:0px -593px !important;background-repeat:no-repeat !important;background-color:#0077b5 !important;background-size:initial !important;cursor:pointer !important;border:0 !important;border-right:1px solid #066094 !important;text-indent:-9999em !important;overflow:hidden !important;padding:0 !important;margin:0 !important;position:absolute !important;left:0px !important;top:0px !important;display:block !important;width:20px !important;height:20px !important;float:right !important;border-radius:2px !important;-webkit-border-radius:2px !important;border-top-right-radius:2px !important;border-bottom-right-radius:2px !important;-webkit-border-top-right-radius:2px !important;-webkit-border-bottom-right-radius:2px !important;}#li_ui_li_gen_1434535975073_0 #li_ui_li_gen_1434535975073_0-title{color:#fff !important;cursor:pointer !important;display:block !important;white-space:nowrap !important;float:left !important;margin-left:1px !important;vertical-align:top !important;overflow:hidden !important;text-align:center !important;height:18px !important;padding:0 4px 0 23px !important;border:1px solid #000 !important;border-top-color:#0077b5 !important;border-right-color:#0077b5 !important;border-bottom-color:#0077b5 !important;border-left-color:#0077b5 !important;text-shadow:0 -1px #005887 !important;line-height:20px !important;border-radius:2px !important;-webkit-border-radius:2px !important;border-top-right-radius:2px !important;border-bottom-right-radius:2px !important;-webkit-border-top-right-radius:2px !important;-webkit-border-bottom-right-radius:2px !important;background-color:#0077b5 !important;background-image:-webkit-gradient(linear, left top, left bottom, color-stop(0%,#0077b5), color-stop(100%,#0077b5)) !important; background-image: -webkit-linear-gradient(top, #0077b5 0%, #0077b5 100%) !important;}#li_ui_li_gen_1434535975073_0.hovered #li_ui_li_gen_1434535975073_0-title{border:1px solid #000 !important;border-top-color:#0369a0 !important;border-right-color:#0369a0 !important;border-bottom-color:#0369a0 !important;border-left-color:#0369a0 !important;background-color:#0369a0 !important;background-image:-webkit-gradient(linear, left top, left bottom, color-stop(0%,#0369a0), color-stop(100%,#0369a0)) !important; background-image: -webkit-linear-gradient(top, #0369a0 0%, #0369a0 100%) !important;}#li_ui_li_gen_1434535975073_0.clicked #li_ui_li_gen_1434535975073_0-title, #li_ui_li_gen_1434535975073_0.down #li_ui_li_gen_1434535975073_0-title{color:#fff !important;border:1px solid #000 !important;border-top-color:#066094 !important;border-right-color:#066094 !important;border-bottom-color:#066094 !important;border-left-color:#066094 !important;background-color:#066094 !important;background-image:-webkit-gradient(linear, left top, left bottom, color-stop(0%,#066094), color-stop(100%,#066094)) !important; background-image: -webkit-linear-gradient(top, #066094 0%, #066094 100%) !important;}.IN-shadowed #li_ui_li_gen_1434535975073_0 #li_ui_li_gen_1434535975073_0-title{}.IN-shadowed #li_ui_li_gen_1434535975073_0.hovered #li_ui_li_gen_1434535975073_0-title{}.IN-shadowed #li_ui_li_gen_1434535975073_0.clicked #li_ui_li_gen_1434535975073_0-title, .IN-shadowed #li_ui_li_gen_1434535975073_0.down #li_ui_li_gen_1434535975073_0-title{}#li_ui_li_gen_1434535975073_0 #li_ui_li_gen_1434535975073_0-title-text, #li_ui_li_gen_1434535975073_0 #li_ui_li_gen_1434535975073_0-title-text *{color:#fff !important;font-size:11px !important;font-family:Arial, sans-serif !important;font-weight:bold !important;font-style:normal !important;-webkit-font-smoothing:antialiased !important;display:inline-block !important;background:transparent none !important;vertical-align:top !important;height:18px !important;line-height:20px !important;float:none !important;}#li_ui_li_gen_1434535975073_0 #li_ui_li_gen_1434535975073_0-title-text strong{font-weight:bold !important;}#li_ui_li_gen_1434535975073_0 #li_ui_li_gen_1434535975073_0-title-text em{font-style:italic !important;}#li_ui_li_gen_1434535975073_0.hovered #li_ui_li_gen_1434535975073_0-title-text, #li_ui_li_gen_1434535975073_0.hovered #li_ui_li_gen_1434535975073_0-title-text *{color:#fff !important;}#li_ui_li_gen_1434535975073_0.clicked #li_ui_li_gen_1434535975073_0-title-text, #li_ui_li_gen_1434535975073_0.down #li_ui_li_gen_1434535975073_0-title-text, #li_ui_li_gen_1434535975073_0.clicked #li_ui_li_gen_1434535975073_0-title-text *, #li_ui_li_gen_1434535975073_0.down #li_ui_li_gen_1434535975073_0-title-text *{color:#fff !important;}#li_ui_li_gen_1434535975073_0 #li_ui_li_gen_1434535975073_0-title #li_ui_li_gen_1434535975073_0-mark{display:inline-block !important;width:0px !important;overflow:hidden !important;}.success #li_ui_li_gen_1434535975073_0 #li_ui_li_gen_1434535975073_0-title{color:#fff !important;border-top-color:#0077b5 !important;border-right-color:#0077b5 !important;border-bottom-color:#0077b5 !important;border-left-color:#0077b5 !important;background-color:#0077b5 !important;background-image:-webkit-gradient(linear, left top, left bottom, color-stop(0%,#0077b5), color-stop(100%,#0077b5)) !important; background-image: -webkit-linear-gradient(top, #0077b5 0%, #0077b5 100%) !important;}.success #li_ui_li_gen_1434535975073_0 #li_ui_li_gen_1434535975073_0-title-text, .success #li_ui_li_gen_1434535975073_0 #li_ui_li_gen_1434535975073_0-title-text *{color:#fff !important;}.IN-shadowed .success #li_ui_li_gen_1434535975073_0 #li_ui_li_gen_1434535975073_0-title{}.success #li_ui_li_gen_1434535975073_0.hovered #li_ui_li_gen_1434535975073_0-title{color:#fff !important;border-top-color:#0369a0 !important;border-right-color:#0369a0 !important;border-bottom-color:#0369a0 !important;border-left-color:#0369a0 !important;background-color:#0369a0 !important;background-image:-webkit-gradient(linear, left top, left bottom, color-stop(0%,#0369a0), color-stop(100%,#0369a0)) !important; background-image: -webkit-linear-gradient(top, #0369a0 0%, #0369a0 100%) !important;}.success #li_ui_li_gen_1434535975073_0.hovered #li_ui_li_gen_1434535975073_0-title-text, .success #li_ui_li_gen_1434535975073_0.hovered #li_ui_li_gen_1434535975073_0-title-text *{color:#fff !important;}.success #li_ui_li_gen_1434535975073_0.clicked #li_ui_li_gen_1434535975073_0-title, .success #li_ui_li_gen_1434535975073_0.down #li_ui_li_gen_1434535975073_0-title{color:#fff !important;border-top-color:#066094 !important;border-right-color:#066094 !important;border-bottom-color:#066094 !important;border-left-color:#066094 !important;background-color:#066094 !important;background-image:-webkit-gradient(linear, left top, left bottom, color-stop(0%,#066094), color-stop(100%,#066094)) !important; background-image: -webkit-linear-gradient(top, #066094 0%, #066094 100%) !important;}.success #li_ui_li_gen_1434535975073_0.clicked #li_ui_li_gen_1434535975073_0-title-text, .success #li_ui_li_gen_1434535975073_0.down #li_ui_li_gen_1434535975073_0-title-text, .success #li_ui_li_gen_1434535975073_0.clicked #li_ui_li_gen_1434535975073_0-title-text *, .success #li_ui_li_gen_1434535975073_0.down #li_ui_li_gen_1434535975073_0-title-text *{color:#fff !important;}.IN-shadowed .success #li_ui_li_gen_1434535975073_0.clicked #li_ui_li_gen_1434535975073_0-title, .IN-shadowed .success #li_ui_li_gen_1434535975073_0.down #li_ui_li_gen_1434535975073_0-title{}#li_ui_li_gen_1434535975095_1-container.IN-right {display:inline-block !important;float:left !important;overflow:visible !important;position:relative !important;height:20px !important;padding-left:2px !important;line-height:1px !important;cursor:pointer !important;}#li_ui_li_gen_1434535975095_1.IN-right {display:block !important;float:left !important;height:20px !important;margin-right:4px !important;padding-right:4px !important;background-image:url(https://static.licdn.com/scds/common/u/images/apps/connect/sprites/sprite_connect_v14.png) !important;background-color:transparent !important;background-repeat:no-repeat !important;background-position:right -447px !important;}#li_ui_li_gen_1434535975095_1-inner.IN-right {display:block !important;float:left !important;padding-left:8px !important;text-align:center !important;background-image:url(https://static.licdn.com/scds/common/u/images/apps/connect/sprites/sprite_connect_v14.png) !important;background-color:transparent !important;background-repeat:no-repeat !important;background-position:0px -426px !important;}#li_ui_li_gen_1434535975095_1-content.IN-right {display:inline !important;font-size:11px !important;color:#4e4e4e !important;font-weight:bold !important;font-family:Arial, sans-serif !important;line-height:20px !important;padding:0 5px 0 5px !important;}#li_ui_li_gen_1434535975095_1-container.IN-empty {display:none !important;}#li_ui_li_gen_1434535975095_1-container.IN-hidden #li_ui_li_gen_1434535975095_1 {display:none !important;}</style><style type="text/css">.fb_hidden{position:absolute;top:-10000px;z-index:10001}.fb_invisible{display:none}.fb_reset{background:none;border:0;border-spacing:0;color:#000;cursor:auto;direction:ltr;font-family:"lucida grande", tahoma, verdana, arial, sans-serif;font-size:11px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;line-height:1;margin:0;overflow:visible;padding:0;text-align:left;text-decoration:none;text-indent:0;text-shadow:none;text-transform:none;visibility:visible;white-space:normal;word-spacing:normal}.fb_reset>div{overflow:hidden}.fb_link img{border:none}
.fb_dialog{background:rgba(82, 82, 82, .7);position:absolute;top:-10000px;z-index:10001}.fb_reset .fb_dialog_legacy{overflow:visible}.fb_dialog_advanced{padding:10px;-moz-border-radius:8px;-webkit-border-radius:8px;border-radius:8px}.fb_dialog_content{background:#fff;color:#333}.fb_dialog_close_icon{background:url(http://static.ak.fbcdn.net/rsrc.php/v2/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 0 transparent;_background-image:url(http://static.ak.fbcdn.net/rsrc.php/v2/yL/r/s816eWC-2sl.gif);cursor:pointer;display:block;height:15px;position:absolute;right:18px;top:17px;width:15px}.fb_dialog_mobile .fb_dialog_close_icon{top:5px;left:5px;right:auto}.fb_dialog_padding{background-color:transparent;position:absolute;width:1px;z-index:-1}.fb_dialog_close_icon:hover{background:url(http://static.ak.fbcdn.net/rsrc.php/v2/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 -15px transparent;_background-image:url(http://static.ak.fbcdn.net/rsrc.php/v2/yL/r/s816eWC-2sl.gif)}.fb_dialog_close_icon:active{background:url(http://static.ak.fbcdn.net/rsrc.php/v2/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 -30px transparent;_background-image:url(http://static.ak.fbcdn.net/rsrc.php/v2/yL/r/s816eWC-2sl.gif)}.fb_dialog_loader{background-color:#f6f7f8;border:1px solid #606060;font-size:24px;padding:20px}.fb_dialog_top_left,.fb_dialog_top_right,.fb_dialog_bottom_left,.fb_dialog_bottom_right{height:10px;width:10px;overflow:hidden;position:absolute}.fb_dialog_top_left{background:url(http://static.ak.fbcdn.net/rsrc.php/v2/ye/r/8YeTNIlTZjm.png) no-repeat 0 0;left:-10px;top:-10px}.fb_dialog_top_right{background:url(http://static.ak.fbcdn.net/rsrc.php/v2/ye/r/8YeTNIlTZjm.png) no-repeat 0 -10px;right:-10px;top:-10px}.fb_dialog_bottom_left{background:url(http://static.ak.fbcdn.net/rsrc.php/v2/ye/r/8YeTNIlTZjm.png) no-repeat 0 -20px;bottom:-10px;left:-10px}.fb_dialog_bottom_right{background:url(http://static.ak.fbcdn.net/rsrc.php/v2/ye/r/8YeTNIlTZjm.png) no-repeat 0 -30px;right:-10px;bottom:-10px}.fb_dialog_vert_left,.fb_dialog_vert_right,.fb_dialog_horiz_top,.fb_dialog_horiz_bottom{position:absolute;background:#525252;filter:alpha(opacity=70);opacity:.7}.fb_dialog_vert_left,.fb_dialog_vert_right{width:10px;height:100%}.fb_dialog_vert_left{margin-left:-10px}.fb_dialog_vert_right{right:0;margin-right:-10px}.fb_dialog_horiz_top,.fb_dialog_horiz_bottom{width:100%;height:10px}.fb_dialog_horiz_top{margin-top:-10px}.fb_dialog_horiz_bottom{bottom:0;margin-bottom:-10px}.fb_dialog_iframe{line-height:0}.fb_dialog_content .dialog_title{background:#6d84b4;border:1px solid #3a5795;color:#fff;font-size:14px;font-weight:bold;margin:0}.fb_dialog_content .dialog_title>span{background:url(http://static.ak.fbcdn.net/rsrc.php/v2/yd/r/Cou7n-nqK52.gif) no-repeat 5px 50%;float:left;padding:5px 0 7px 26px}body.fb_hidden{-webkit-transform:none;height:100%;margin:0;overflow:visible;position:absolute;top:-10000px;left:0;width:100%}.fb_dialog.fb_dialog_mobile.loading{background:url(http://static.ak.fbcdn.net/rsrc.php/v2/ya/r/3rhSv5V8j3o.gif) white no-repeat 50% 50%;min-height:100%;min-width:100%;overflow:hidden;position:absolute;top:0;z-index:10001}.fb_dialog.fb_dialog_mobile.loading.centered{max-height:590px;min-height:590px;max-width:500px;min-width:500px}#fb-root #fb_dialog_ipad_overlay{background:rgba(0, 0, 0, .45);position:absolute;left:0;top:0;width:100%;min-height:100%;z-index:10000}#fb-root #fb_dialog_ipad_overlay.hidden{display:none}.fb_dialog.fb_dialog_mobile.loading iframe{visibility:hidden}.fb_dialog_content .dialog_header{-webkit-box-shadow:white 0 1px 1px -1px inset;background:-webkit-gradient(linear, 0% 0%, 0% 100%, from(#738ABA), to(#2C4987));border-bottom:1px solid;border-color:#1d4088;color:#fff;font:14px Helvetica, sans-serif;font-weight:bold;text-overflow:ellipsis;text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0;vertical-align:middle;white-space:nowrap}.fb_dialog_content .dialog_header table{-webkit-font-smoothing:subpixel-antialiased;height:43px;width:100%}.fb_dialog_content .dialog_header td.header_left{font-size:12px;padding-left:5px;vertical-align:middle;width:60px}.fb_dialog_content .dialog_header td.header_right{font-size:12px;padding-right:5px;vertical-align:middle;width:60px}.fb_dialog_content .touchable_button{background:-webkit-gradient(linear, 0% 0%, 0% 100%, from(#4966A6), color-stop(.5, #355492), to(#2A4887));border:1px solid #2f477a;-webkit-background-clip:padding-box;-webkit-border-radius:3px;-webkit-box-shadow:rgba(0, 0, 0, .117188) 0 1px 1px inset, rgba(255, 255, 255, .167969) 0 1px 0;display:inline-block;margin-top:3px;max-width:85px;line-height:18px;padding:4px 12px;position:relative}.fb_dialog_content .dialog_header .touchable_button input{border:none;background:none;color:#fff;font:12px Helvetica, sans-serif;font-weight:bold;margin:2px -12px;padding:2px 6px 3px 6px;text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0}.fb_dialog_content .dialog_header .header_center{color:#fff;font-size:16px;font-weight:bold;line-height:18px;text-align:center;vertical-align:middle}.fb_dialog_content .dialog_content{background:url(http://static.ak.fbcdn.net/rsrc.php/v2/y9/r/jKEcVPZFk-2.gif) no-repeat 50% 50%;border:1px solid #555;border-bottom:0;border-top:0;height:150px}.fb_dialog_content .dialog_footer{background:#f6f7f8;border:1px solid #555;border-top-color:#ccc;height:40px}#fb_dialog_loader_close{float:left}.fb_dialog.fb_dialog_mobile .fb_dialog_close_button{text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0}.fb_dialog.fb_dialog_mobile .fb_dialog_close_icon{visibility:hidden}
.fb_iframe_widget{display:inline-block;position:relative}.fb_iframe_widget span{display:inline-block;position:relative;text-align:justify}.fb_iframe_widget iframe{position:absolute}.fb_iframe_widget_fluid_desktop,.fb_iframe_widget_fluid_desktop span,.fb_iframe_widget_fluid_desktop iframe{max-width:100%}.fb_iframe_widget_fluid_desktop iframe{min-width:220px;position:relative}.fb_iframe_widget_lift{z-index:1}.fb_hide_iframes iframe{position:relative;left:-10000px}.fb_iframe_widget_loader{position:relative;display:inline-block}.fb_iframe_widget_fluid{display:inline}.fb_iframe_widget_fluid span{width:100%}.fb_iframe_widget_loader iframe{min-height:32px;z-index:2;zoom:1}.fb_iframe_widget_loader .FB_Loader{background:url(http://static.ak.fbcdn.net/rsrc.php/v2/y9/r/jKEcVPZFk-2.gif) no-repeat;height:32px;width:32px;margin-left:-16px;position:absolute;left:50%;z-index:4}</style><script src="./eco_files/is2.js" class="intentshare-asset"></script><script src="./eco_files/is3.js" class="intentshare-asset"></script><link class="intentshare-asset" rel="stylesheet" type="text/css" href="./eco_files/is3.css"><style type="text/css">.wfavicon .wfavicon-tooltip{line-height:14px;position:absolute;left:0;top:15px;color:#fff;font-size:11px;border:1px solid #cb306b;border-radius:4px;-o-border-radius:4px;-moz-border-radius:4px;-webkit-border-radius:4px;width:286px;margin:1em;padding:3px;background:#797878}.wfavicon a.wfavicon-short{padding-left:15px !important}.wfavicon a.wfavicon-popup{background:#eee url(http://static.womenalia.net/media/wfaviconstyles/wfavicon.png) no-repeat left center;padding:0 2px 0 23px;color:#333;border:1px solid #aaa;border-radius:4px;-o-border-radius:4px;-moz-border-radius:4px;-webkit-border-radius:4px;font-weight:bold;font-size:11px;opacity:.8;white-space:nowrap !important;font-family:arial;display:inline-block;height:18px;text-decoration:none;line-height:18px !important}.wfavicon a.wfavicon-popup:hover{background-color:#ddd;border-color:#666;color:#000;opacity:1;text-decoration:none;line-height:18px !important}</style><link href="./eco_files/dixio_1_0.css" rel="stylesheet" type="text/css"><script type="text/javascript" id="cS" charset="UTF-8" src="./eco_files/traces.js"></script></head>

	<body id="empresas" s="1" data-twttr-rendered="true"><div id="epd"></div><div class="principal-cab">
<script>
  var _comscore = _comscore || [];
  _comscore.push({ c1: "2", c2: "7107810" });
  (function() {
    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
    s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
    el.parentNode.insertBefore(s, el);
  })();
</script><noscript>&lt;img src="http://b.scorecardresearch.com/p?c1=2&amp;amp;c2=7107810&amp;amp;cv=2.0&amp;amp;cj=1"&gt;</noscript>
<script language="javascript">//<![CDATA[
		var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
		document.write(unescape("%3Cscript src=\'" + gaJsHost + "google-analytics.com/ga.js\' type=\'text/javascript\'%3E%3C/script%3E"));
	//]]></script><script src="./eco_files/ga.js" type="text/javascript"></script><script language="javascript">//<![CDATA[
		var pageTracker = _gat._getTracker('UA-1010908-1');	
		pageTracker._initData();
		pageTracker._trackPageview();
	//]]></script><div id="banners-superiores_1"><script type="text/javascript" language="javascript">//<![CDATA[
		var time = new Date() ;
		randnum= (time.getTime()) ;
	//]]></script></div>
<div class="red3-wrap"><div class="red3" style="z-index: 1005010;">
<div class="menu3"><ul id="menu-drop">
<li class="ncab-en"><a style="color:#333;" href="http://www.eleconomista.es/">Portada</a></li>
<li class="ncab-en"><a style="color:#333;" href="http://ecodiario.eleconomista.es/">
							Eco<span style="color: #d5b036;">Diario</span></a></li>
<li class="ncab-en"><a style="color:#333;" href="http://ecoteuve.eleconomista.es/">
							Eco<span style="color: #800;">teuve</span></a></li>
<li class="ncab-en"><a style="color:#333;" href="http://www.eleconomista.es/ecomotor">
							Eco<span style="color: #0079c8;">motor</span></a></li>
<li class="ncab-en"><a style="color:#333;" href="http://www.eleconomista.es/evasion/">Evasión</a></li>
<li class="ncab-en"><a href="http://www.eleconomista.es/ecotrader">Eco<span style="color: #f00;">trader</span></a></li>
<li class="ncab-en"><a href="http://www.eleconomista.es/ecoley/">Eco<span style="color: #9c6;">ley</span></a></li>
<li class="ncab-en"><a href="http://www.eleconomista.es/monitor"><span style="color: #f60;">el</span>Monitor
						</a></li>
<li class="ncab-en"><a href="http://www.economiahoy.mx/"><span style="color: #f60;">Eco</span>nomía<span style="color: #f60;">hoy.mx</span></a></li>
<li class="menu_right li-ame ncab-en" style="z-index: 2300;">
<a class="drop desp-ame" href="http://www.eleconomistaamerica.com/">Otras <i class="fa fa-angle-down"></i></a><div class="dropdown_1column align_left" style="top:27px; z-index: 3000;"><div class="col_1 new "><ul>
<li><a href="http://ecoaula.eleconomista.es/">Eco<span style="color: #f60;">Aula</span></a></li>
<li class="patro">
<a class="left" href="http://www.eleconomista.es/pymes/">Eco<span style="color: #3498DB; ">pymes</span></a><a class="right" style="margin:6px 0 0 0 !important; padding:0 !important;" href="http://www.bancopopular.es/personas"><img style="width:43px !important;" src="./eco_files/popular_hover50x32.png" alt="Banco Popular"></a>
</li>
</ul></div></div>
</li>
<li class="menu_right li-ame ncab-en" style="z-index: 2300;">
<a style="color:#f60;" class="drop desp-ame" href="http://www.eleconomistaamerica.com/" target="_blank">América
  
  <i class="fa fa-angle-down"></i></a><div class="dropdown_1column align_left" style="top:27px; z-index: 3000;">
<div class="col_1 new ">
<h2>Países</h2>
<ul>
<li><a href="http://www.eleconomistaamerica.com/"><img style="width: 100%;" src="./eco_files/logo-america-pie-mapa.png"></a></li>
<li><a href="http://www.economiahoy.mx/"><img style="vertical-align:middle; margin-right:3px;" src="./eco_files/MEX_1332_grande.gif">México</a></li>
<li><a href="http://www.eleconomistaamerica.com.ar/"><img style="vertical-align:middle; margin-right:3px;" src="./eco_files/ARG_667_grande.gif">Argentina</a></li>
<li><a href="http://www.eleconomistaamerica.cl/"><img style="vertical-align:middle; margin-right:3px;" src="./eco_files/CHI_680_grande.gif">Chile</a></li>
<li><a href="http://www.eleconomistaamerica.co/"><img style="vertical-align:middle; margin-right:3px;" src="./eco_files/COL_1086_grande.gif">Colombia</a></li>
<li><a href="http://www.eleconomistaamerica.pe/"><img style="vertical-align:middle; margin-right:3px;" src="./eco_files/PER_1090_grande.gif">Perú</a></li>
</ul>
</div>
<hr>
<div class="col_1 new ">
<h3>Materias Primas</h3>
<ul class="dd-tt-s">
<li><a href="http://www.eleconomistaamerica.com/materia-prima/Crudo-wti">Crudo</a></li>
<li><a href="http://www.eleconomistaamerica.com/materia-prima/oro">Oro</a></li>
<li><a href="http://www.eleconomistaamerica.com/materia-prima/soja">Soja</a></li>
<li class="ver-mas"><a href="http://www.eleconomistaamerica.com/materias-primas/">Ver más</a></li>
</ul>
<hr>
<h3>Índices</h3>
<ul class="dd-tt-s">
<li><a href="http://www.economiahoy.mx/indice/IPC-MEXICO">IPC México</a></li>
<li><a href="http://www.eleconomistaamerica.com/indice/COL20">COL 20</a></li>
<li><a href="http://www.eleconomistaamerica.com/indice/BOVESPA">BOVESPA</a></li>
<li class="ver-mas"><a href="http://www.eleconomistaamerica.com/indices-latinoamericanos/index.html">Ver más</a></li>
</ul>
</div>
</div>
</li>
</ul></div>
<div class="cont2" style="width: 250px; float: right; margin-top: 0px;"><div id="login-cont">
<div id="login-superior" class="topnav"><a id="login-boton" class="login-boton"><span>Iniciar sesión</span></a></div>
<fieldset id="login-menu" class="login-menu">
<div class="field1"><form method="post" id="signin" onsubmit="Captura_mouseClick_enviar();">
<p id="mensaje-error"></p>
<p><label for="username">Dirección de email</label><input id="username" name="username" value="" title="username" tabindex="4" type="text" autocorrect="off" autocapitalize="off"></p>
<p><label for="password">Contraseña</label><input id="password" name="password" value="" title="password" tabindex="5" type="password"></p>
<p class="login-recordar"><span id="boton_registro"><input id="login-enviar" value="Entrar" tabindex="6" type="submit"></span><input id="remember" name="remember_me" value="1" tabindex="7" type="checkbox" checked=""><label for="remember">Recordarme</label></p>
<p class="login-enlace"><a href="http://www.eleconomista.es/registro/perfil.php" id="resend_password_link">¿Olvidaste tu contraseña?</a></p>
<p class="login-enlace"><a href="http://www.eleconomista.es/registro/alta.php" id="resend_password_link">Registrarse</a></p>
</form></div>
<div class="field2">
<h4 style="color: #f60;">Servicios Premium</h4>
<p class="login-enlace"><a style="color:#000!important;" href="http://www.eleconomista.es/ecotrader" id="resend_password_link">Eco<span style="color:#f00;">trader</span></a></p>
<p class="login-enlace"><a href="http://www.eleconomista.es/hemeroteca" id="resend_password_link">Edición PDF + Hemeroteca</a></p>
<p class="forgot"><a href="http://www.eleconomista.es/elsuperlunes" id="resend_password_link">El Superlunes</a></p>
<h4 style="margin-top: 15px; color: #f60;">Servicios gratuitos</h4>
<p class="login-enlace"><a href="http://listas.eleconomista.es/" id="resend_password_link">Listas y rankings</a></p>
<p class="login-enlace"><a href="http://www.eleconomista.es/cartera/portada.php" id="resend_password_link">Cartera</a></p>
<p class="login-enlace"><a href="http://www.eleconomista.es/foros" id="resend_password_link">Foros</a></p>
</div>
</fieldset>
<fieldset id="login-menu2" class="login-menu" style="display: none">
<div class="field1">
<h4>Zona usuario</h4>
<p class="login-enlace"><a href="http://www.eleconomista.es/registro/datosPersonales.php" id="resend_password_link"><i class="fa fa-user"></i> Datos personales</a></p>
<p class="login-enlace"><a href="http://www.eleconomista.es/registro/perfil.php" id="resend_password_link"><i class="fa fa-edit"></i> Editar perfil</a></p>
<p class="login-enlace"><a href="http://www.eleconomista.es/premium/mediospago.php" id="resend_password_link"><i class="fa fa-credit-card"></i> Medios de pago</a></p>
<p class="login-enlace"><a href="http://www.eleconomista.es/premium/facturas.php" id="resend_password_link"><i class="fa fa-file-o"></i> Facturas</a></p>
<p class="login-enlace"><a href="http://www.eleconomista.es/premium/suscripciones.php" id="resend_password_link"><i class="fa fa-check-square-o"></i> Servicios Premium</a></p>
<form method="post" id="signin2"><p class="remember"><span id="boton_desconectar"><input id="signin_submit" class="desconexion" value="Desconectarme" tabindex="6" type="button"></span></p></form>
</div>
<div class="field2">
<h4 style="color: #f60;">Servicios Premium</h4>
<p class="forgot"><a href="http://www.eleconomista.es/ecotrader" id="resend_password_link">Ecotrader</a></p>
<p class="forgot"><a href="http://www.eleconomista.es/hemeroteca" id="resend_password_link">Edición PDF + Hemeroteca</a></p>
<p class="forgot"><a href="http://www.eleconomista.es/elsuperlunes" id="resend_password_link">El Superlunes</a></p>
<h4 style="margin-top: 15px; color: #f60;">Servicios gratuitos</h4>
<p class="forgot"><a href="http://listas.eleconomista.es/" id="resend_password_link">Listas y rankings</a></p>
<p class="forgot"><a href="http://www.eleconomista.es/cartera/portada.php" id="resend_password_link">Cartera</a></p>
<p class="forgot"><a href="http://www.eleconomista.es/foros" id="resend_password_link">Foros</a></p>
</div>
</fieldset>
</div></div>
<div style="clear: both;display: block; margin: 10px 0;"></div>
</div></div>
<script type="text/javascript">//<![CDATA[
		if ( getCookie("Navegacion_Web") ) {
			var content = '<li><a href="#" onclick="return soloIPhone() ;">Volver a la version iPhone</a></li>' ;
			
			$("menuLink").insert( {bottom: content} ) ; 
		}
		
		function soloIPhone() {
			deleteCookie("Navegacion_Web", "/", "http://www.eleconomista.es") ;
			location = "http://iphone.eleconomista.mobi"
		}
		//]]></script>
</div>
<style>
            #localizacion_aviso_predefinido{
                padding: 0;
                width: 1024px;
                margin: 0 auto;
            }
            .eleccion-pais { 
                margin: 20px 0 0 0;
                padding: 10px;
                font-size: 16px;
                overflow: hidden;
                border: 2px solid #ECF0F1;
            }
            .eleccion-pais img{ /*margin-right:5px; vertical-align: baseline; opacity: 0.8;*/display: none;}
            .eleccion-pais a:link, .eleccion-pais a:active, .eleccion-pais a:visited {
                text-decoration:none;
            }
            .eleccion-pais a:hover { }
            .eleccion-pais-cerrar { float: right;font-size: 16px;}
            .eleccion-pais-cerrar a{ color:#BDC3C7;}
            .eleccion-pais b {text-transform:uppercase;}
            .eleccion-pais a span{
                font-size: 26px;
                font-weight: 400;
            }
            .eleccion-indice { 
                font-size: 12px; 
                margin-top: 10px; 
                overflow: hidden;
                padding: 2px 8px;
                border: 1px solid #ECF0F1;
                color: #ECF0F1;
            }
            .eleccion-indice span {
                margin-right: 5px;
                text-transform: uppercase;
                font-weight: 500;
                color: #000;
            }
            .eleccion-indice a { 
              color:#333;
              cursor:pointer;
            }
            .eleccion-indice a:hover { text-decoration:none;}
            .eleccion-indice a span{ 
              color: #27AE60;
              font-weight: 400;
              font-size: 15px;
            }
            .eleccion-indice a span.accion1{ color: #27AE60;}
            .eleccion-indice a span.accion-1{ color: #E74C3C;}
            .eleccion-indice a span.accion-1{ color: #E74C3C;}
            .eleccion-indice a.cotizacion{
              margin: 0 5px 0 0;
              font-size: 15px;
            }
            .eleccion-indice a.cotizacion:hover{
              color:#08c;
            }
            .eleccion-indice a.cotizacion.ver{
              margin: 4px 0 0 0;
              background: #ECF0F1;
              padding: 1px 15px;
              color: #2C3E50;
              font-size: 11px;
              float: right;
              -webkit-border-radius: 3px;
              border-radius: 3px;
              line-height: 14px;
            }
            .eleccion-indice a.cotizacion.ver:hover{ color:#08c; background:#BDC3C7;}



            .el-ed{
                padding: 10px;
                border: 1px solid #096490;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                background-image: linear-gradient(bottom, rgb(12,145,206) 42%, rgb(30,132,179) 71%, rgb(31,158,217) 86%);
                background-image: -o-linear-gradient(bottom, rgb(12,145,206) 42%, rgb(30,132,179) 71%, rgb(31,158,217) 86%);
                background-image: -moz-linear-gradient(bottom, rgb(12,145,206) 42%, rgb(30,132,179) 71%, rgb(31,158,217) 86%);
                background-image: -webkit-linear-gradient(bottom, rgb(12,145,206) 42%, rgb(30,132,179) 71%, rgb(31,158,217) 86%);
                background-image: -ms-linear-gradient(bottom, rgb(12,145,206) 42%, rgb(30,132,179) 71%, rgb(31,158,217) 86%);
                background-image: -webkit-gradient(
                linear,
                left bottom,
                left top,
                color-stop(0.42, rgb(12,145,206)),
                color-stop(0.71, rgb(30,132,179)),
                color-stop(0.86, rgb(31,158,217))
                );
                
                #background-color:rgb(12,145,206);
                #width:948px;
                /* For Internet Explorer 5.5 - 7 */
                filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#1f9ed9, endColorstr=#1e84b3);
                
                /* For Internet Explorer 8 */
                -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#1f9ed9, endColorstr=#1e84b3)";
                padding-bottom:5px\9;
            }
            
            .el-ed h4 {
                margin: 0;
                font-size: 13px;
                color: #fff;
                font-weight:bold;
            }
            .el-ed h4 a:link, .el-ed h4 a:active, .el-ed h4 a:visited {
                color: #fff;
                text-decoration: underline;
            }
            .cierre {
                float: right;
                margin-top: -20px;
            }
            
            .el-ed-flecha {
                position: absolute;
                top: 30px;
                left: 100px;
            }
            
            .edicion {
                color: #333;
                font-size: 12px;
                margin-top: 10px;
            }
            

        </style><div class="row" mod="4379">
<div id="localizacion_aviso_predefinido" style="display:none;"><div class="eleccion-pais">
<a href="http://www.eleconomista.es/interstitial/volver/269617462/construccion-inmobiliario/noticias/6784723/06/15/La-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html#" onclick="redirigirASite();"><img src="./eco_files/final_d_org.gif"><span id="localizacion_aviso_pais"></span></a><div class="eleccion-pais-cerrar"><a href="http://www.eleconomista.es/interstitial/volver/269617462/construccion-inmobiliario/noticias/6784723/06/15/La-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html#" onclick="jQuery(&#39;#localizacion_aviso_predefinido&#39;).hide();"><i class="fa fa-times"></i></a></div>
<div class="eleccion-indice" id="localizacion_aviso_cotizaciones"></div>
</div></div>
<script language="javascript">//<![CDATA[
                var siteprincipal = '';
                var siteprincipal=getCookie("siteprincipal");
                var siteprincipalConsulta=getCookie("siteprincipalConsulta");
                var siteprincipalRecomendado = getCookie("siteprincipalRecomendado");
                var modulosSite = new Array();
                var modulosReemplazados = new Array();
                
                jQuery(window).load(function(){
                    if((siteprincipal==null || siteprincipal=='' ) || ( siteprincipalConsulta==1 && siteprincipalRecomendado>1 )){
                        //Petición de datos
                        var ajax_data = {};
                        
                        var ajaxRequest = new Ajax.Request(
                        '/iplocalizacion/iplocalizacion.php',
                        {
                            method: 'post',
                            parameters: ajax_data,
                            //asynchronous: true,
                            onComplete: function(xmlHttpRequest, responseHeader){
                                if(xmlHttpRequest.responseJSON != null ){
                                    oRespuesta = xmlHttpRequest.responseJSON;
                                    if(oRespuesta.resultado == 1){
                                        siteprincipal = oRespuesta.datos.site_local;
										jQuery('#localizacion_aviso_pais').html(oRespuesta.datos.msg);
										jQuery('#localizacion_avisoclick_pais').html(oRespuesta.datos.msgclick);
                                        mostrarCotizacionesSiteAsociado(oRespuesta.datos.cotizaciones, '');
                                        //if(typeof(siteprincipalRecomendado) == "undefined") {
                                            setCookie("siteprincipalRecomendado", siteprincipal, 1,"/",".eleconomista.es");
                                            siteprincipalRecomendado = siteprincipal;
                                        //}
                                        jQuery('#localizacion_aviso_predefinido').show();
                                        //if(siteprincipal!='')alert(siteprincipal);
                                        recargarModulosPorLocalizacion();
                                    }else{
                                        //No hay site asociado a esa localizacion  
                                        setCookie("siteprincipalConsulta", 1,1,"/",".eleconomista.es");
                                        setCookie("siteprincipalRecomendado", 1,1,"/",".eleconomista.es");
                                    }
                                }else{
                                    //
                                }
                            }
                            
                        });
                    }else{
                        //Site por defecto definido. Dar opción a su eliminación
                        //alert('Definido - ' + siteprincipal);
                    }
                    
                });
                
                function recargarModulosPorLocalizacion(){
                    //console.log("prueba");
                    /* */
                    var ajaxRequest2 = new Ajax.Request(
                    "/iplocalizacion/noticiasSite.php"
                    ,{
                        method: "get"
                        , parameters: {'site': siteprincipalRecomendado, 'Modulos': modulosSite.join(",")}
                        , onComplete: function(xmlHttpRequest, responseHeader){
                            if(xmlHttpRequest.responseJSON != null ){
                                oRespuesta = xmlHttpRequest.responseJSON;
                                if(oRespuesta.resultado == 1){
                                    //captura el listado de datos
                                    for(k=0;k<oRespuesta.datos.length;k++){
                                        //alert("change-"+ k);
                                        if(modulosSite.indexOf(oRespuesta.datos[k].mod+"")>-1){
                                            moduloAReemplazar = modulosReemplazados[modulosSite.indexOf(oRespuesta.datos[k].mod+"")];
                                            //jQuery("#prueba_" + moduloAReemplazar ).html(oRespuesta.datos[k].codigoHTML);
                                            jQuery('div[mod="' + moduloAReemplazar +'"]' ).html(oRespuesta.datos[k].codigoHTML);
                                        }
                                    }
                                }else{
                                    
                                }
                            }else{
                                //
                            }
                        }
                        
                    }
                    );
                }
                
                function establecerSitePrincipal(){
                    if(siteprincipal!='') {setCookie("siteprincipal", siteprincipal,365,"/",".eleconomista.es");jQuery('#localizacion_aviso_predefinido').hide();}
                    else{setCookie("siteprincipal", '',-1,"/",".eleconomista.es");}
                }
                
                function mostrarCotizacionesSiteAsociado(aCotizaciones, sEnlace){
                    if(sEnlace=='') sEnlace ='http://www.eleconomistaamerica.com/indices-latinoamericanos/index.html';
                    var sHTML = '';
                    for(i=0;i<aCotizaciones.length;i++){
                        //sHTML += ((sHTML!='')? ',':'');
                        sHTML += '<a href="' + aCotizaciones[i].CotizacionURL + '" class="cotizacion">' + aCotizaciones[i].CotizacionNombre + ' <span class="ultimo_' + aCotizaciones[i].CotizacionId + '" table="mod_geo" item="item_' + aCotizaciones[i].CotizacionCodigoFinInfo + '_' + aCotizaciones[i].CotizacionQuality + '" field="precio_ultima_cotizacion" source="lightstreamer">' + aCotizaciones[i].CotizacionValor + ' </span><span class="ico_' + aCotizaciones[i].CotizacionId + '" table="mod_geo" item="item_' + aCotizaciones[i].CotizacionId + '_' + aCotizaciones[i].CotizacionId + '" field="#arrow" source="lighstreamer"><img height="11" width="11" src="' + aCotizaciones[i].CotizacionIcono + '"/></span><span class="accion' + aCotizaciones[i].CotizacionEstado + ' pDif_' + aCotizaciones[i].CotizacionId + ' estado_' + aCotizaciones[i].CotizacionId + '"  acc="' + aCotizaciones[i].CotizacionEstado + '" table="mod_geo" item="item_' + aCotizaciones[i].CotizacionCodigoFinInfo + '_' + aCotizaciones[i].CotizacionQuality + '" field="variacion_porcentual" source="lightstreamer">' + aCotizaciones[i].CotizacionVariacion + '</span></a>  |  ';
                    }
                    sHTML = '<span>Cotizaciones:</span>' + sHTML + '<a href="' + sEnlace + '" class="cotizacion ver">Ver todas</a>';
                    jQuery('#localizacion_aviso_cotizaciones').html(sHTML);
                    //Activación del lightstreamer
                    /*
					var mod_geo = new OverwriteTable( null, null, "MERGE" ) ;
                    mod_geo.setSnapshotRequired( true ) ;
                    mod_geo.setRequestedMaxFrequency( 1.0 ) ;
                    mod_geo.onItemUpdate = updateItem ;
                    mod_geo.setDataAdapter("FINFEED");
                    mod_geo.onChangingValues = formatValues ;
                    mod_geo.setPushedHtmlEnabled( true ) ;
                    pushPage.addTable( mod_geo, "mod_geo" ) ;
                    var decimales = new Hash() ;
                    for(i=0;i<aCotizaciones.length;i++){
                        decimales.set('item_' + aCotizaciones[i].CotizacionId + '_' + aCotizaciones[i].CotizacionId , 2) ;
                    }
                    mod_geo.decimales = decimales ;
					*/
                }
                
                
    function redirigirASite(){
        if(siteprincipal==21){ window.location.replace('http://www.eleconomistaamerica.co/');}
        if(siteprincipal==22){ window.location.replace('http://www.economiahoy.mx/');}
        if(siteprincipal==23){ window.location.replace('http://www.eleconomistaamerica.cl/');}
        //if(siteprincipal==24){ window.location.replace('http://www.eleconomistaamerica.com.br/');}
        if(siteprincipal==25){ window.location.replace('http://www.eleconomistaamerica.com.ar/');}
        if(siteprincipal==26){ window.location.replace('http://www.eleconomistaamerica.pe/');}
    }
    //]]></script>
</div>

	<div class="principal intentshare-highlightable"><div class="cabecera">
<div class="wrap-header">
<div class="logo">
<a href="http://www.eleconomista.es/"><img src="./eco_files/logo-h1.gif" alt="elEconomista.es"></a><div class="lead izda"><span class="l-fecha">Jueves, 11 de Junio de 2015 Actualizado a las 14:44</span></div>
</div>
<div class="ruta" style=" width: 166px;"><a href="http://www.eleconomista.es/noticias/construccion-inmobiliario">Construccion Inmobiliario</a></div>
<div class="cont-c-d cont-c-d-int"><div class="buscador container-2"><form id="demo-2" method="GET" action="http://www.eleconomista.es/buscador/resultados.php" name="buscador" accept-charset="UTF-8">
<input type="hidden" value="partner-pub-2866707026894500:3x0zyny7y13" name="cx"><input type="hidden" value="FORID:10" name="cof"><input type="hidden" value="UTF-8" name="ie"><input type="hidden" value="0" name="recordatorio" style="margin: 0pt 3px 3px; padding: 0pt;"><input type="hidden" value="0" name="pagina" style="margin: 0pt 3px 3px; padding: 0pt;"><span class="icon"><i class="fa fa-search"></i></span><input type="search" id="texto_formulario" name="fondo" placeholder="Buscar...">
</form></div></div>
</div>
<script language="javascript">
	//<![CDATA[		
		function DesplegarMenu(menu_actual, menu_nuevo) {
			Element.removeClassName(menu_actual, 'sel');
			Element.removeClassName('s-'+menu_actual, 'sel');
			Element.addClassName('s-'+menu_actual, 'non-sel');
			
			Element.removeClassName('s-'+menu_nuevo,'non-sel');
			Element.addClassName('s-'+menu_nuevo,'sel');
			Element.addClassName(menu_nuevo,'sel');
			return (menu_nuevo);
			
		};
		
		function Patrocinio(id, clase) {
			$(id).toggleClassName(clase);
		}
			
		var menu_actual = 'emp-menu';
	//]]>
	</script><div class="menu-v2"><ul id="menu-drop">
<li id="home-menu"><a href="http://www.eleconomista.es/"><i class="fa fa-home"></i> Portada</a></li>
<li id="merc-menu" onmouseover="javascript:Patrocinio(&#39;merc-menu&#39;,&#39;merc-pat-b&#39;);" onmouseout="javascript:Patrocinio(&#39;merc-menu&#39;,&#39;merc-pat-b&#39;);">
<a class="drop" href="http://www.eleconomista.es/mercados-cotizaciones/index.html"><span>Mercados y Cotizaciones <i class="fa fa-angle-down"></i></span></a><div class="dropdown_4columns">
<div class="col_1">
<div class="drop-tit">Mercados</div>
<ul>
<li><a href="http://www.eleconomista.es/mercados-cotizaciones/index.html">Portada Mercados</a></li>
<li><a href="http://www.eleconomista.es/mercados-cotizaciones/renta-variable-fundamental/">Renta Variable</a></li>
<li><a href="http://www.eleconomista.es/mercados-cotizaciones/renta-fija/">Renta Fija</a></li>
<li><a href="http://www.eleconomista.es/mercados-cotizaciones/divisas/">Divisas</a></li>
<li><a href="http://www.eleconomista.es/mercados-cotizaciones/divisas/">Calculadora Divisas</a></li>
<li><a href="http://www.eleconomista.es/mercados-cotizaciones/materias-primas/">Materias Primas</a></li>
<li><a href="http://www.eleconomista.es/cfd/index.html">CFDs</a></li>
<li><a href="http://www.eleconomista.es/carteras/index.html">Carteras consenso</a></li>
<li><a href="http://www.eleconomista.es/fondos/buscador_fondos_avanzado.php">Buscador de fondos</a></li>
</ul>
</div>
<div class="col_3 black_box" style="width: 374px;">
<div class="drop-tit">Cotizaciones</div>
<ul>
<li class="patro">
<a class="left" href="http://www.eleconomista.es/indices-mundiales/index.html">Índices mundiales</a><a class="right" style="margin:1px 0 0 0 !important; padding:0 !important;" href="http://www.cepsa.com/" rel="nofollow" target="_blank"><img src="./eco_files/cepsa2.png" alt="cepsa"></a>
</li>
<li><a href="http://www.eleconomistaamerica.com/indices-latinoamericanos/index.html">Índices latinoamericanos</a></li>
<li><a href="http://www.eleconomista.es/stoxx/index.html">Índices Stoxx</a></li>
</ul>
<div class="col_1">
<h3>Mercado español</h3>
<ul>
<li><a href="http://www.eleconomista.es/indice/ibex-35">Ibex 35</a></li>
<li><a href="http://www.eleconomista.es/mercados/mercado-continuo">M. Continuo</a></li>
<li><a href="http://www.eleconomista.es/indice/IGBM">IGBM</a></li>
<li><a href="http://www.eleconomista.es/indice/eco10">Eco 10</a></li>
<li><a href="http://www.eleconomista.es/eco30">Eco 30</a></li>
<li><a href="http://www.eleconomista.es/indice/ibex-dividendo">Ibex dividendo</a></li>
</ul>
<h3>Divisas y tipos</h3>
<ul>
<li><a href="http://www.eleconomista.es/cruce/EURUSD">Euro/Dólar</a></li>
<li><a href="http://www.eleconomista.es/cruce/EURJPY">Euro/Yen</a></li>
<li><a href="http://www.eleconomista.es/cruce/EURGBP">Euro/Libra</a></li>
<li><a href="http://www.eleconomista.es/tipo-interbancario/euribor-1-year">Euribor 1 año</a></li>
<li><a href="http://www.eleconomista.es/tipo-interbancario/euribor-6-meses">Euribor 6 meses</a></li>
</ul>
<ul class="greybox"><li style="width: auto;overflow:hidden"><a class="dest" href="http://www.eleconomista.es/prima-de-riesgo/index.html" style="float:left">Primas de Riesgo</a></li></ul>
</div>
<div class="col_1 right" style="padding-right: 0;">
<h3>Europa</h3>
<ul>
<li><a href="http://www.eleconomista.es/indice/EUROSTOXX-50">Stoxx50</a></li>
<li><a href="http://www.eleconomista.es/indice/DAX-30">Dax30</a></li>
<li><a href="http://www.eleconomista.es/indice/CAC-40">Cac40</a></li>
<li><a href="http://www.eleconomista.es/indice/FTSE-100">FTSE</a></li>
</ul>
<h3>EEUU/Asia</h3>
<ul>
<li><a href="http://www.eleconomista.es/indice/DOW-JONES">DowJones</a></li>
<li><a href="http://www.eleconomista.es/indice/S-P-500">SP500</a></li>
<li><a href="http://www.eleconomista.es/indice/NASDAQ-100">Nasdaq</a></li>
<li><a href="http://www.eleconomista.es/indice/NASDAQ-COMPOSITE">Nasdaq Comp.</a></li>
<li><a href="http://www.eleconomista.es/indice/NIKKEI">Nikkei</a></li>
</ul>
</div>
</div>
</div>
</li>
<li id="emp-menu" class="sel" onmouseover="javascript:Patrocinio(&#39;emp-menu&#39;,&#39;emp-pat-b&#39;);" onmouseout="javascript:Patrocinio(&#39;emp-menu&#39;,&#39;emp-pat-b&#39;);">
<a class="drop" href="http://www.eleconomista.es/empresas-finanzas/index.html"><span>Empresas <i class="fa fa-angle-down"></i></span></a><div class="dropdown_4columns">
<div class="col_4" style="padding-bottom:0;"><div class="drop-tit">Sectores</div></div>
<div class="col_1"><ul>
<li><a href="http://www.eleconomista.es/empresas-finanzas/index.html">Portada empresas</a></li>
<li class="patro">
<a class="left" href="http://www.eleconomista.es/empresas-finanzas/distribucion-alimentacion/index.html">Alimentación</a><a class="right" style="margin: 6px 8px 0 0 !important; padding:0 !important;" href="http://www.bancopopular.es/personas"><img src="./eco_files/lidl-p.png" alt="Lidl"></a>
</li>
<li><a href="http://www.eleconomista.es/empresas-finanzas/energia/index.html">Energía
            </a></li>
<li><a href="http://www.eleconomista.es/empresas-finanzas/consumo/index.html">Consumo</a></li>
<li><a href="http://www.eleconomista.es/empresas-finanzas/fundaciones/">Fundaciones</a></li>
</ul></div>
<div class="col_1"><ul>
<li class="patro">
<a class="left" href="http://www.eleconomista.es/empresas-finanzas/infraestructuras-construccion-inmobiliario/index.html">Construcción</a><a class="right" style="margin: 0 0 0 0 !important;" target="_blank" href="http://www.fcc.es/fccweb/index.html"><img src="./eco_files/fcc81x17.png" alt="FCC"></a>
</li>
<li><a href="http://www.eleconomista.es/ecosanidad/index.html">Sanidad</a></li>
<li><a href="http://www.eleconomista.es/empresas-finanzas/finanzas-seguros/index.html">Finanzas </a></li>
<li><a href="http://www.eleconomista.es/tecnologia/index.html">Tecnología</a></li>
<li><a href="http://www.eleconomista.es/agro/index.html">Agro</a></li>
</ul></div>
<div class="col_1 right"><ul>
<li><a href="http://www.eleconomista.es/empresas-finanzas/telecomunicaciones-medios-comunicacion/index.html">Telecomunicaciones</a></li>
<li><a href="http://www.eleconomista.es/empresas-finanzas/transporte/index.html">Transporte</a></li>
<li><a href="http://www.eleconomista.es/especiales/turismo-viajes/">Turismo y viajes</a></li>
<li><a href="http://www.eleconomista.es/empresas-finanzas/agua-medioambiente/index.html">Agua</a></li>
</ul></div>
<div class="col_4"><ul class="greybox">
<li style="width: auto;"><a href="http://www.eleconomista.es/empresas-finanzas/seguros/">Seguros y Asegurados Magazine- <span style="color: #666666; font-weight: normal; font-size: 12px;float: right;">Vea la sección y lea la nueva revista</span></a></li>
<li class="patro">
<a class="left" href="http://www.eleconomista.es/pymes/index.html">Emprendedores y Pymes</a><a class="right" target="_blank" style="margin: 0 8px 0 0 !important;" href="http://www.bancopopular.es/popular-web"><img src="./eco_files/popular2.png" alt="Banco Popular"></a>
</li>
<li class="patro">
<a class="left" href="http://www.eleconomista.es/ecosanidad/index.html" style="float: left;">Sanidad - <span style="color: #666666; font-weight: normal; font-size: 12px;">Vea la sección y lea la nueva revista</span></a><a class="right" style="margin: 0 8px 0 0 !important;" target="_blank" href="http://www.novartis.es/"><img src="./eco_files/novartis.png" alt="Novartis"></a>
</li>
<li class="patro">
<a class="left" href="http://empresite.eleconomista.es/">Buscador de empresas - <span style="color: #666666; font-weight: normal; font-size: 12px;">no cotizadas</span></a><a class="right" style="margin: 4px 8px 0 0 !important;" target="_blank" href="http://empresite.eleconomista.es/"><img src="./eco_files/empresite-trans.png" alt="Empresite"></a>
</li>
<li class="patro">
<a class="left" href="http://seguros.eleconomista.es/">Seguros</a><a class="right" style="margin: 4px 8px 0 0 !important;" target="_blank" href="http://seguros.eleconomista.es/"><img src="./eco_files/acierto-trans.png" alt="Acierto"></a>
</li>
<li style="width: auto;overflow:hidden"><a href="http://ranking-empresas.eleconomista.es/">Ranking de empresas - <span style="color: #666; font-weight: normal; font-size: 12px;float:right;">Las principales empresas en España</span></a></li>
<li style="width: auto;overflow:hidden"><a href="http://administradores.eleconomista.es/">Administradores de empresas</a></li>
</ul></div>
</div>
</li>
<li id="eco-menu" class="eco-pat" onmouseover="javascript:Patrocinio(&#39;eco-menu&#39;,&#39;eco-pat-b&#39;);" onmouseout="javascript:Patrocinio(&#39;eco-menu&#39;,&#39;eco-pat-b&#39;);">
<a class="drop" href="http://www.eleconomista.es/economia/index.html"><span>Economía <i class="fa fa-angle-down"></i></span></a><div class="dropdown_2columns"><div class="col_2"><ul>
<li><a href="http://www.eleconomista.es/economia/index.html">Portada economía</a></li>
<li><a href="http://ecodata.eleconomista.es/">Datos macro</a></li>
<li><a href="http://ecodata.eleconomista.es/indicadores/">Indicadores</a></li>
<li><a href="http://ecodata.eleconomista.es/paises/">Países</a></li>
<li><a href="http://ecodata.eleconomista.es/calendario/">Calendario</a></li>
<li><a href="http://ecodata.eleconomista.es/notas_economicas/">Notas económicas</a></li>
</ul></div></div>
</li>
<li id="tecn-menu" class="tecno-pat" onmouseover="javascript:Patrocinio(&#39;tecn-menu&#39;,&#39;tecno-pat-b&#39;);" onmouseout="javascript:Patrocinio(&#39;tecn-menu&#39;,&#39;tecno-pat-b&#39;);">
<a class="drop" href="http://www.eleconomista.es/tecnologia/index.html"><span>Tecnología <i class="fa fa-angle-down"></i></span></a><div class="dropdown_1column"><div class="col_1"><ul>
<li><a href="http://www.eleconomista.es/tecnologia/index.html">Portada tecnología</a></li>
<li><a href="http://www.eleconomista.es/CanalPDA">CanalPDA</a></li>
<li><a href="http://www.eleconomista.es/tecnologia/apps/">The App Date</a></li>
</ul></div></div>
</li>
<li id="viv-menu" class="viv-pat" onmouseover="javascript:Patrocinio(&#39;viv-menu&#39;,&#39;viv-pat-b&#39;);" onmouseout="javascript:Patrocinio(&#39;viv-menu&#39;,&#39;viv-pat-b&#39;);"><a href="http://www.eleconomista.es/vivienda/index.html"><span>Vivienda</span></a></li>
<li id="opinion-menu">
<a class="drop" href="http://www.eleconomista.es/opinion/index.html">Opinión/Blogs <i class="fa fa-angle-down"></i></a><div class="dropdown_3columns">
<div class="col_1">
<div class="drop-tit">En opinión</div>
<ul class="simple">
<li><a href="http://www.eleconomista.es/opinion/index.html">Portada opinión</a></li>
<li><a href="http://www.eleconomista.es/noticias/editoriales">Editoriales</a></li>
<li><a href="http://www.eleconomista.es/noticias/opinion-blogs">Firmas</a></li>
<li><a href="http://www.eleconomista.es/blogs/vineta-del-dia/">Viñeta del día</a></li>
</ul>
</div>
<div class="col_2 black_box" style="width: 240px;">
<div class="drop-tit">Blogs</div>
<ul style="width: auto; margin-left: 5px;">
<li><a href="http://www.eleconomista.es/blogs/desde-el-burladero">Desde el Burladero</a></li>
<li><a href="http://www.eleconomista.es/blogs/a-la-catalana">A la catalana</a></li>
<li><a href="http://www.eleconomista.es/blogs/naranjazos/">Naranjazos</a></li>
<li><a href="http://www.eleconomista.es/blogs/hablemos-de-empresa">Hablemos de empresa</a></li>
<li><a href="http://www.eleconomista.es/blogs/la-escuela-de-empresarios">Escuela de empresarios</a></li>
<li><a href="http://www.eleconomista.es/blogs/andanomiks/">Andanomiks</a></li>
<li><a href="http://www.eleconomista.es/blogs/la-conciencia-del-directivo/">La conciencia del directivo</a></li>
<li><a href="http://www.eleconomista.es/blogs/emprendedores">Emprendedores</a></li>
<li><a href="http://www.eleconomista.es/blogs/sumando-ideas/">Sumando ideas</a></li>
<li><a href="http://www.eleconomista.es/blogs/sensaciones">Sensaciones</a></li>
<li class="sigue"><a href="http://www.eleconomista.es/blogs">Ver todos</a></li>
</ul>
</div>
</div>
</li>
<li id="autonomias-menu">
<a class="drop" href="http://www.eleconomista.es/interstitial/volver/269617462/construccion-inmobiliario/noticias/6784723/06/15/La-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html#">Autonomías <i class="fa fa-angle-down"></i></a><div class="dropdown_1column"><div class="col_1"><ul class="simple">
<li><a href="http://www.eleconomista.es/catalunya">Cataluña</a></li>
<li style="overflow:hidden;"><a style="float:left; marging-right:2px;" href="http://www.eleconomista.es/pais_vasco">País Vasco</a></li>
<li><a href="http://www.eleconomista.es/andalucia">Andalucía</a></li>
<li><a href="http://www.eleconomista.es/aragon">Aragón</a></li>
<li><a href="http://www.eleconomista.es/valenciana">C. Valenciana</a></li>
<li><a href="http://www.eleconomista.es/castilla_y_leon">Castilla y León</a></li>
<li><a href="http://www.eleconomista.es/madrid">Madrid</a></li>
<li><a href="http://www.eleconomista.es/castilla-la-mancha/">Castilla La Mancha</a></li>
</ul></div></div>
</li>
<li id="product-menu" class="menu_right">
<a class="drop" href="http://www.eleconomista.es/kiosco/">Kiosco eE <i class="fa fa-angle-down"></i></a><div class="dropdown_3columns align_right">
<div class="col_3"><div class="drop-tit">Edición digital</div></div>
<div class="col_2_2"><ul>
<li><a href="http://www.eleconomista.es/elsuperlunes/">elSuperLunes<span class="radius secondary label red right">GRATIS</span></a></li>
<li><a href="http://www.eleconomista.es/hemeroteca">Edición PDF</a></li>
</ul></div>
<div class="col_2_2"><ul>
<li><a href="http://www.eleconomista.es/ecotablet">Ecotablet</a></li>
<li><a href="http://www.eleconomista.es/edicion-impresa/">Suscriptor periódico</a></li>
</ul></div>
<hr>
<div class="col_3"><div class="drop-tit">Revistas elEconomista.es</div></div>
<div class="col_2_2"><ul>
<li><a href="http://www.eleconomista.es/ecoley/">Iuris &amp; Lex</a></li>
<li class="patro">
<a class="left" href="http://www.eleconomista.es/empresas-finanzas/distribucion-alimentacion/index.html">Alimentación</a><a class="right" style="margin: 0 !important; padding:0 !important;"><img src="./eco_files/lidl-p.png" alt="Lidl"></a>
</li>
<li><a href="http://www.eleconomista.es/tecnologia/index.html">Tecnología</a></li>
<li><a href="http://www.eleconomista.es/mercados-cotizaciones/index.html">Inversión</a></li>
<li><a href="http://www.eleconomista.es/empresas-finanzas/energia/">Energía</a></li>
<li><a href="http://www.eleconomista.es/kiosco/agua/">Agua</a></li>
<li><a href="http://www.eleconomista.es/kiosco/franquicias/">Franquicias y Emprendedores</a></li>
<li class="patro">
<a class="left" href="http://www.eleconomista.es/ecosanidad/index.html">EcoSanidad</a><a class="right" style="margin: 0 0 0 0 !important; padding:0 !important;" target="_blank" href="http://www.novartis.es/"><img style="width: 80px;" src="./eco_files/novartis(1).png" alt="novartis"></a>
</li>
<li><a href="http://www.eleconomista.es/kiosco/madrid/">Madrid</a></li>
<li><a href="http://www.eleconomista.es/kiosco/paisvasco/">País Vasco</a></li>
</ul></div>
<div class="col_2_2"><ul>
<li><a href="http://www.eleconomista.es/empresas-finanzas/transporte/index.html">Transporte</a></li>
<li><a href="http://www.eleconomista.es/empresas-finanzas/consumo/index.html">Consumo</a></li>
<li><a href="http://www.eleconomista.es/empresas-finanzas/fundaciones/">Fundaciones</a></li>
<li><a href="http://www.eleconomista.es/pymes/">Gestión empresarial</a></li>
<li><a href="http://www.eleconomista.es/empresas-finanzas/seguros/">Seguros</a></li>
<li><a href="http://www.eleconomista.es/andalucia/">Andalucía</a></li>
<li><a href="http://www.eleconomista.es/catalunya/">Catalunya</a></li>
<li><a href="http://www.eleconomista.es/valenciana/">Valencia</a></li>
<li><a href="http://www.eleconomista.es/kiosco/elite-sport">Elite Sport</a></li>
</ul></div>
<hr>
<div class="col_3"><h2 class="cint-normal">Para iPad</h2></div>
<div class="col_2_2"><ul class="greybox"><li style="margin:0;"><a style="font-size:16px; font-weight:700;line-height: 1.2em;" target="_blank" href="http://www.eleconomista.es/ecotablet/">el<span style="color: #f60;">Eco</span>Tablet</a></li></ul></div>
<div class="col_2_2"><ul class="greybox"><li style="margin:0;"><a style="font-size: 16px;font-weight: 700;line-height: 1.2em;" target="_blank" href="http://itunes.apple.com/us/app/eleconomista-edicion-impresa/id491306100?l=es&ls=1&mt=8">elEconomista Edición Impresa</a></li></ul></div>
</div>
</li>
<li id="servinv-menu" class="menu_right">
<a class="drop" href="http://www.eleconomista.es/servicios-inversor/index.html">Servicios <i class="fa fa-angle-down"></i></a><div class="dropdown_3columns align_right">
<div class="col_2_2"><ul>
<li><a href="http://www.eleconomista.es/fichas-de-empresas/">Fichas valor</a></li>
<li><a href="http://www.eleconomista.es/hechos-relevantes/">Últimos hechos CNMV</a></li>
<li><a href="http://www.eleconomista.es/agora/">Agora</a></li>
<li class="patro">
<a class="left" href="http://www.eleconomista.es/conferencias/">Conferencias</a><a class="right" style="margin: 2px 8px 0 0 !important;padding: 0 !important;" href="http://www.kpmg.com/es/es/paginas/default.aspx" target="_blank" rel="nofollow"><img src="./eco_files/kpmg.png" alt="kpmg"></a>
</li>
<li><a href="http://www.eleconomista.es/observatorios">Observatorios</a></li>
<li><a href="http://ecodata.eleconomista.es/">Datos macro - <span style="color: #c00;">Nuevo</span></a></li>
</ul></div>
<div class="col_2_2 right"><ul>
<li><a href="http://www.eleconomista.es/analisis-tecnico">Análisis técnico</a></li>
<li><a href="http://listas.eleconomista.es/">Listas</a></li>
<li><a href="http://www.eleconomista.es/formacion/">Escuela de inversión</a></li>
<li><a href="http://coleccion.eleconomista.es/">Ecoleccionista</a></li>
<li><a href="http://ecoapuestas.eleconomista.es/" title="Ecoapuestas">Apuestas</a></li>
<li><a href="http://www.eleconomista.es/entradas/" title="Entradas">Entradas</a></li>
</ul></div>
<hr>
<div class="col_3"><div class="drop-tit">Servicios para invertir</div></div>
<div class="col_3 serv_trader">
<a href="http://www.eleconomista.es/ecotrader/"><img src="./eco_files/ecotrader-transparente.png" alt="Ecotrader.es"></a><p>Información exclusiva sobre el mercado financiero en tiempo real con las trece herramientas financieras más innovadoras del mercado.</p>
<a class="button radius small trader" href="http://www.eleconomista.es/ecotrader/">Suscribirse</a>
</div>
<div class="col_3 serv_monitor">
<a href="http://www.eleconomista.es/monitor/"><img src="./eco_files/logo-grande-degradado.png" alt="elMonitor"></a><p>Suscríbase a la herramienta para los ahorradores en bolsa y acceda gratis.</p>
<a class="button radius small monitor" href="http://www.eleconomista.es/monitor/">Suscribirse</a>
</div>
</div>
</li>
</ul></div>
<div class="sub-wrap"><div id="sub_menu" class="submenu-desp"><ul id="s-emp-menu" class="submenu2 sel">
<li><a href="http://www.eleconomista.es/empresas-finanzas/distribucion-alimentacion/index.html">Alimentación</a></li>
<li><a href="http://www.eleconomista.es/empresas-finanzas/consumo/index.html">Consumo</a></li>
<li id="s-home-menu-eon" class="eon" onmouseover="javascript:Patrocinio(&#39;s-home-menu-eon&#39;,&#39;eon-b&#39;);" onmouseout="javascript:Patrocinio(&#39;s-home-menu-eon&#39;,&#39;eon-b&#39;);"><a href="http://www.eleconomista.es/empresas-finanzas/energia/index.html"><span>Energía</span></a></li>
<li><a href="http://www.eleconomista.es/empresas-finanzas/finanzas-seguros/index.html">Finanzas y seguros</a></li>
<li><a href="http://www.eleconomista.es/empresas-finanzas/infraestructuras-construccion-inmobiliario/index.html">Infraestructuras y construcción</a></li>
<li><a href="http://www.eleconomista.es/ecosanidad/index.html">Sanidad</a></li>
<li><a href="http://www.eleconomista.es/empresas-finanzas/telecomunicaciones-medios-comunicacion/index.html">Telecomunicaciones</a></li>
<li><a href="http://www.eleconomista.es/empresas-finanzas/transporte/index.html">Transporte</a></li>
</ul></div></div>
<div id="mis_favoritos_reducido" class="cot-home cot-indices"><div id="mis_favoritos_reducido_visitadas"><div class="cot-home-una" id="mini_117"><dl><dt><a href="http://www.eleconomista.es/indice/IBEX-35">IBEX 35</a></dt><dd><span class="ico_117" acc="1"><img src="./eco_files/fsube2.gif"></span><span class="accion1 pDif_117 estado_117" acc="1">+0,19%</span></dd></dl></div><div class="cot-home-una" id="mini_321"><dl><dt><a href="http://www.eleconomista.es/cruce/EURUSD">EURUSD</a></dt><dd><span class="ico_321" acc="1"><img src="./eco_files/fsube2.gif"></span><span class="accion1 pDif_321 estado_321" acc="1">+0,39%</span></dd></dl></div><div class="cot-home-una" id="mini_323"><dl><dt><a href="http://www.eleconomista.es/indice/IGBM">I. GENERAL DE MADRID</a></dt><dd><span class="ico_323" acc="1"><img src="./eco_files/fsube2.gif"></span><span class="accion1 pDif_323 estado_323" acc="1">+0,15%</span></dd></dl></div><div class="cot-home-una" id="mini_555"><dl><dt><a href="http://www.eleconomista.es/indice/DOW-JONES">DOW JONES</a></dt><dd><span class="ico_555" acc="1"><img src="./eco_files/fsube2.gif"></span><span class="accion1 pDif_555 estado_555" acc="1">+0,64%</span></dd></dl></div><div class="cot-home-una" id="mini_11167"><dl><dt><a href="http://www.eleconomista.es/indice/ECO10">ECO10</a></dt><dd><span class="ico_11167" acc="1"><img src="./eco_files/fsube2.gif"></span><span class="accion1 pDif_11167 estado_11167" acc="1">+0,29%</span></dd></dl></div><div class="cot-home-una" id="mini_21243"><dl><dt><a href="http://www.eleconomista.es/tipo-interbancario/EURIBOR-1-YEAR">EURIBOR</a></dt><dd><span class="ico_21243" acc="1"><img src="./eco_files/fsube2.gif"></span><span class="accion1 pDif_21243 estado_21243" acc="1">+0,91%</span></dd></dl></div><div class="cot-home-una" id="mini_21299"><dl><dt><a href="http://www.eleconomista.es/materia-prima/BRENT">BRENT</a></dt><dd><span class="ico_21299" acc="-1"><img src="./eco_files/fbaja2.gif"></span><span class="accion-1 pDif_21299 estado_21299" acc="-1">-2,17%</span></dd></dl></div></div><div class="cot-home-fav"><a id="edi_favoritos" href="http://www.eleconomista.es/favoritos">Editar</a></div></div>
<script type="text/javascript">
	//<![CDATA[
		miLista = new miListaValores( 'l' ) ;
	
						
	//]]>
	</script><div id="banners-superiores">
<div class="b928">
<script language="javascript">//<![CDATA[
		sz = "728x90" ;
		st = "eleconomista.es" ;
		std = "http://ad.es.doubleclick.net/" ;
		kw = "269617462";
		sect="6784723";
		document.write( '<SCR'+'IPT language="JavaScript" SRC="'+std+'adj/'+st+'/economista_noticias_construcion;sect='+sect+';tile=3;kw='+kw+';sz='+sz+';ord='+randnum+'?"></SCR'+'IPT>' ) ;
	//]]></script><script language="JavaScript" src="./eco_files/economista_noticias_construcion"></script><div class="GoogleActiveViewClass" id="DfpVisibilityIdentifier_6704415148124099"><script>(function(){var g=this,l=function(a,b){var c=a.split("."),d=g;c[0]in d||!d.execScript||d.execScript("var "+c[0]);for(var e;c.length&&(e=c.shift());)c.length||void 0===b?d=d[e]?d[e]:d[e]={}:d[e]=b},m=function(a,b,c){return a.call.apply(a.bind,arguments)},n=function(a,b,c){if(!a)throw Error();if(2<arguments.length){var d=Array.prototype.slice.call(arguments,2);return function(){var c=Array.prototype.slice.call(arguments);Array.prototype.unshift.apply(c,d);return a.apply(b,c)}}return function(){return a.apply(b,arguments)}},p=function(a,b,c){p=Function.prototype.bind&&-1!=Function.prototype.bind.toString().indexOf("native code")?m:n;return p.apply(null,arguments)},q=Date.now||function(){return+new Date};var r=document,s=window;var t=function(a,b){for(var c in a)Object.prototype.hasOwnProperty.call(a,c)&&b.call(null,a[c],c,a)},w=function(a,b){a.google_image_requests||(a.google_image_requests=[]);var c=a.document.createElement("img");c.src=b;a.google_image_requests.push(c)};var x=function(a){return{visible:1,hidden:2,prerender:3,preview:4}[a.webkitVisibilityState||a.mozVisibilityState||a.visibilityState||""]||0},y=function(a){var b;a.mozVisibilityState?b="mozvisibilitychange":a.webkitVisibilityState?b="webkitvisibilitychange":a.visibilityState&&(b="visibilitychange");return b};var C=function(){this.g=r;this.k=s;this.j=!1;this.i=null;this.h=[];this.o={};if(z)this.i=q();else if(3==x(this.g)){this.i=q();var a=p(this.q,this);A&&(a=A("di::vch",a));this.p=a;var b=this.g,c=y(this.g);b.addEventListener?b.addEventListener(c,a,!1):b.attachEvent&&b.attachEvent("on"+c,a)}else B(this)},A;C.m=function(){return C.n?C.n:C.n=new C};var D=/^([^:]+:\/\/[^/]+)/m,G=/^\d*,(.+)$/m,z=!1,B=function(a){if(!a.j){a.j=!0;for(var b=0;b<a.h.length;++b)a.l.apply(a,a.h[b]);a.h=[]}};C.prototype.s=function(a,b){var c=b.target.u();(c=G.exec(c))&&(this.o[a]=c[1])};C.prototype.l=function(a,b){this.k.rvdt=this.i?q()-this.i:0;var c;if(c=this.t)t:{try{var d=D.exec(this.k.location.href),e=D.exec(a);if(d&&e&&d[1]==e[1]&&b){var f=p(this.s,this,b);this.t(a,f);c=!0;break t}}catch(u){}c=!1}c||w(this.k,a)};C.prototype.q=function(){if(3!=x(this.g)){B(this);var a=this.g,b=y(this.g),c=this.p;a.removeEventListener?a.removeEventListener(b,c,!1):a.detachEvent&&a.detachEvent("on"+b,c)}};var H=/^true$/.test("")?!0:!1;var I={},J=function(a){var b=a.toString();a.name&&-1==b.indexOf(a.name)&&(b+=": "+a.name);a.message&&-1==b.indexOf(a.message)&&(b+=": "+a.message);if(a.stack){a=a.stack;var c=b;try{-1==a.indexOf(c)&&(a=c+"\n"+a);for(var d;a!=d;)d=a,a=a.replace(/((https?:\/..*\/)[^\/:]*:\d+(?:.|\n)*)\2/,"$1");b=a.replace(/\n */g,"\n")}catch(e){b=c}}return b},M=function(a,b,c,d){var e=K,f,u=!0;try{f=b()}catch(h){try{var N=J(h);b="";h.fileName&&(b=h.fileName);var E=-1;h.lineNumber&&(E=h.lineNumber);var v;t:{try{v=c?c():"";break t}catch(S){}v=""}u=e(a,N,b,E,v)}catch(k){try{var O=J(k);a="";k.fileName&&(a=k.fileName);c=-1;k.lineNumber&&(c=k.lineNumber);K("pAR",O,a,c,void 0,void 0)}catch(F){L({context:"mRE",msg:F.toString()+"\n"+(F.stack||"")},void 0)}}if(!u)throw h;}finally{if(d)try{d()}catch(T){}}return f},K=function(a,b,c,d,e,f){a={context:a,msg:b.substring(0,512),eid:e&&e.substring(0,40),file:c,line:d.toString(),url:r.URL.substring(0,512),ref:r.referrer.substring(0,512)};P(a);L(a,f);return!0},L=function(a,b){try{if(Math.random()<(b||.01)){var c="/pagead/gen_204?id=jserror"+Q(a),d="http"+("https:"==s.location.protocol?"s":"")+"://pagead2.googlesyndication.com"+c,d=d.substring(0,2E3);w(s,d)}}catch(e){}},P=function(a){var b=a||{};t(I,function(a,d){b[d]=s[a]})},R=function(a,b,c,d,e){return function(){var f=arguments;return M(a,function(){return b.apply(c,f)},d,e)}},Q=function(a){var b="";t(a,function(a,d){if(0===a||a)b+="&"+d+"="+("function"==typeof encodeURIComponent?encodeURIComponent(a):escape(a))});return b};A=function(a,b,c,d){return R(a,b,void 0,c,d)};z=H;l("vu",R("vu",function(a,b){var c=a.replace("&amp;","&"),d=/(google|doubleclick).*\/pagead\/adview/.test(c),e=C.m();if(d){d="&vis="+x(e.g);b&&(d+="&ve=1");var f=c.indexOf("&adurl"),c=-1==f?c+d:c.substring(0,f)+d+c.substring(f)}e.j?e.l(c,b):e.h.push([c,b])}));l("vv",R("vv",function(){z&&B(C.m())}));})();</script><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" id="google_flash_obj" height="90" width="728"><param name="MOVIE" value="http://pagead2.googlesyndication.com/pagead/imgad?id=CICAgKDT-I-mCBDYBRhaMgj-zfghqgTgZg"><param name="quality" value="high"><param name="AllowScriptAccess" value="never"><param name="wmode" value="opaque"><param name="FlashVars" value="clickTAG=http://googleads.g.doubleclick.net/aclk%3Fsa%3DL%26ai%3DBN3orJUiBVYvpLdPBiQamtIHoD57V7oIIAAAAEAEgADgAWMaOr872AWDVjdOCvAiCARdjYS1wdWItODAxODk0MzI3NTc5OTg3MbIBE3d3dy5lbGVjb25vbWlzdGEuZXO6AQlnZnBfaW1hZ2XIAQTaAbgBaHR0cDovL3d3dy5lbGVjb25vbWlzdGEuZXMvaW50ZXJzdGl0aWFsL3ZvbHZlci8yNjk2MTc0NjIvY29uc3RydWNjaW9uLWlubW9iaWxpYXJpby9ub3RpY2lhcy82Nzg0NzIzLzA2LzE1L0xhLUNhaXhhLWNyZWUtcXVlLWVsLXB1bHNvLWRlbC1zZWN0b3ItaW5tb2JpbGlhcmlvLWNvbWllbnphLWEtcmVjdXBlcmFyc2UuaHRtbKkCqr7Amm3osj7AAgLgAgDqAjQ0Mjc1L2VsZWNvbm9taXN0YS5lcy9lY29ub21pc3RhX25vdGljaWFzX2NvbnN0cnVjaW9u-AL_0R6QA8gGmAPIBqgDAdAEkE7gBAGQBgGgBhXYBwA%26num%3D0%26cid%3D5GjcZJRUGsisf67uiPBn7pxa%26sig%3DAOD64_2FDWseF12Y70Ga6NkQ9aDcdXyrlA%26client%3Dca-pub-8018943275799871%26adurl%3Dhttp://www.contenidos-selfbank.es/promocion/fondos%253Futm_source%253Deleconomista%2526utm_medium%253Ddisplay728%2526utm_campaign%253Dfondos"><object data="http://pagead2.googlesyndication.com/pagead/imgad?id=CICAgKDT-I-mCBDYBRhaMgj-zfghqgTgZg" id="google_flash_embed" width="728" height="90" wmode="opaque" flashvars="clickTAG=http://googleads.g.doubleclick.net/aclk%3Fsa%3DL%26ai%3DBN3orJUiBVYvpLdPBiQamtIHoD57V7oIIAAAAEAEgADgAWMaOr872AWDVjdOCvAiCARdjYS1wdWItODAxODk0MzI3NTc5OTg3MbIBE3d3dy5lbGVjb25vbWlzdGEuZXO6AQlnZnBfaW1hZ2XIAQTaAbgBaHR0cDovL3d3dy5lbGVjb25vbWlzdGEuZXMvaW50ZXJzdGl0aWFsL3ZvbHZlci8yNjk2MTc0NjIvY29uc3RydWNjaW9uLWlubW9iaWxpYXJpby9ub3RpY2lhcy82Nzg0NzIzLzA2LzE1L0xhLUNhaXhhLWNyZWUtcXVlLWVsLXB1bHNvLWRlbC1zZWN0b3ItaW5tb2JpbGlhcmlvLWNvbWllbnphLWEtcmVjdXBlcmFyc2UuaHRtbKkCqr7Amm3osj7AAgLgAgDqAjQ0Mjc1L2VsZWNvbm9taXN0YS5lcy9lY29ub21pc3RhX25vdGljaWFzX2NvbnN0cnVjaW9u-AL_0R6QA8gGmAPIBqgDAdAEkE7gBAGQBgGgBhXYBwA%26num%3D0%26cid%3D5GjcZJRUGsisf67uiPBn7pxa%26sig%3DAOD64_2FDWseF12Y70Ga6NkQ9aDcdXyrlA%26client%3Dca-pub-8018943275799871%26adurl%3Dhttp://www.contenidos-selfbank.es/promocion/fondos%253Futm_source%253Deleconomista%2526utm_medium%253Ddisplay728%2526utm_campaign%253Dfondos" type="application/x-shockwave-flash" allowscriptaccess="never"><param name="FlashVars" value="clickTAG=http://googleads.g.doubleclick.net/aclk%3Fsa%3DL%26ai%3DBN3orJUiBVYvpLdPBiQamtIHoD57V7oIIAAAAEAEgADgAWMaOr872AWDVjdOCvAiCARdjYS1wdWItODAxODk0MzI3NTc5OTg3MbIBE3d3dy5lbGVjb25vbWlzdGEuZXO6AQlnZnBfaW1hZ2XIAQTaAbgBaHR0cDovL3d3dy5lbGVjb25vbWlzdGEuZXMvaW50ZXJzdGl0aWFsL3ZvbHZlci8yNjk2MTc0NjIvY29uc3RydWNjaW9uLWlubW9iaWxpYXJpby9ub3RpY2lhcy82Nzg0NzIzLzA2LzE1L0xhLUNhaXhhLWNyZWUtcXVlLWVsLXB1bHNvLWRlbC1zZWN0b3ItaW5tb2JpbGlhcmlvLWNvbWllbnphLWEtcmVjdXBlcmFyc2UuaHRtbKkCqr7Amm3osj7AAgLgAgDqAjQ0Mjc1L2VsZWNvbm9taXN0YS5lcy9lY29ub21pc3RhX25vdGljaWFzX2NvbnN0cnVjaW9u-AL_0R6QA8gGmAPIBqgDAdAEkE7gBAGQBgGgBhXYBwA%26num%3D0%26cid%3D5GjcZJRUGsisf67uiPBn7pxa%26sig%3DAOD64_2FDWseF12Y70Ga6NkQ9aDcdXyrlA%26client%3Dca-pub-8018943275799871%26adurl%3Dhttp://www.contenidos-selfbank.es/promocion/fondos%253Futm_source%253Deleconomista%2526utm_medium%253Ddisplay728%2526utm_campaign%253Dfondos"><param name="wmode" value="opaque"><div align="center"><a href="http://googleads.g.doubleclick.net/aclk?sa=L&ai=BN3orJUiBVYvpLdPBiQamtIHoD57V7oIIAAAAEAEgADgAWMaOr872AWDVjdOCvAiCARdjYS1wdWItODAxODk0MzI3NTc5OTg3MbIBE3d3dy5lbGVjb25vbWlzdGEuZXO6AQlnZnBfaW1hZ2XIAQTaAbgBaHR0cDovL3d3dy5lbGVjb25vbWlzdGEuZXMvaW50ZXJzdGl0aWFsL3ZvbHZlci8yNjk2MTc0NjIvY29uc3RydWNjaW9uLWlubW9iaWxpYXJpby9ub3RpY2lhcy82Nzg0NzIzLzA2LzE1L0xhLUNhaXhhLWNyZWUtcXVlLWVsLXB1bHNvLWRlbC1zZWN0b3ItaW5tb2JpbGlhcmlvLWNvbWllbnphLWEtcmVjdXBlcmFyc2UuaHRtbKkCqr7Amm3osj7AAgLgAgDqAjQ0Mjc1L2VsZWNvbm9taXN0YS5lcy9lY29ub21pc3RhX25vdGljaWFzX2NvbnN0cnVjaW9u-AL_0R6QA8gGmAPIBqgDAdAEkE7gBAGQBgGgBhXYBwA&num=0&cid=5GjcZJRUGsisf67uiPBn7pxa&sig=AOD64_2FDWseF12Y70Ga6NkQ9aDcdXyrlA&client=ca-pub-8018943275799871&adurl=http://www.contenidos-selfbank.es/promocion/fondos%3Futm_source%3Deleconomista%26utm_medium%3Ddisplay728%26utm_campaign%3Dfondos" target="_blank"><img src="./eco_files/imgad" height="90" width="728"></a></div></object></object><script>vu("http://pubads.g.doubleclick.net/pagead/adview?ai\x3dBN3orJUiBVYvpLdPBiQamtIHoD57V7oIIAAAAEAEgADgAWMaOr872AWDVjdOCvAiCARdjYS1wdWItODAxODk0MzI3NTc5OTg3MbIBE3d3dy5lbGVjb25vbWlzdGEuZXO6AQlnZnBfaW1hZ2XIAQTaAbgBaHR0cDovL3d3dy5lbGVjb25vbWlzdGEuZXMvaW50ZXJzdGl0aWFsL3ZvbHZlci8yNjk2MTc0NjIvY29uc3RydWNjaW9uLWlubW9iaWxpYXJpby9ub3RpY2lhcy82Nzg0NzIzLzA2LzE1L0xhLUNhaXhhLWNyZWUtcXVlLWVsLXB1bHNvLWRlbC1zZWN0b3ItaW5tb2JpbGlhcmlvLWNvbWllbnphLWEtcmVjdXBlcmFyc2UuaHRtbKkCqr7Amm3osj7AAgLgAgDqAjQ0Mjc1L2VsZWNvbm9taXN0YS5lcy9lY29ub21pc3RhX25vdGljaWFzX2NvbnN0cnVjaW9u-AL_0R6QA8gGmAPIBqgDAdAEkE7gBAGQBgGgBhXYBwA\x26sigh\x3dAmQgbywmuw8\x26cid\x3d5GjcZJRUGsisf67uiPBn7pxa")</script></div><noscript>&lt;a href="http://ad.doubleclick.net/jump/eleconomista.es/economista_noticias_construcion;sect=6784723;tile=3;kw=269617462;sz=728x90;ord=123456789?" target="_blank" rel="nofollow"&gt;&lt;img src="http://ad.doubleclick.net/ad/eleconomista.es/economista_noticias_construcion;sect=6784723;tile=3;kw=269617462;sz=728x90;ord=123456789?" width="728" height="90" border="0"&gt;&lt;/a&gt;</noscript>
</div>
<hr>
</div>
<img style="display:none" src="./eco_files/balloon-0.png"><img style="display:none" src="./eco_files/balloon-1.png"><img style="display:none" src="./eco_files/balloon-2.png"><img style="display:none" src="./eco_files/balloon-3.png"><img style="display:none" src="./eco_files/balloon.png"><img style="display:none" src="./eco_files/si.png"><img style="display:none" src="./eco_files/no.png"><img style="display:none" src="./eco_files/button.png"><script type="text/javascript">
 
 //<![CDATA[
 	
 	var mivariable='login-menu';
 	var configuracioninicial=1;
	var cookieUsuario = getCookie('USU') ;
	var cookiePassword = getCookie('PASS') ;
	var delay = 10000;
	var secs = 0;
 	
	if ( (cookieUsuario != null) && (cookieUsuario != '') && (cookiePassword != null) && (cookiePassword != '') ) {
		Carga_Pagina(); /* incluido en cajaLogin.js*/
	}
 
  /* incluido en cajaLogin.js*/
 	 	    
	   	Event.observe($('signin_submit'),'click', Captura_mouseClick_desconectar);
	   	Event.observe($('login-superior'),'click', Captura_mouseClick);
	   	Event.observe($('login-enviar'),'click', Captura_mouseClick_enviar);
	   	Event.observe($('login-superior'),'mouseup', Captura_mouseUp);
		Event.observe($('login-menu'),'mouseup', Captura_mouseUp);
		Event.observe($('login-menu2'),'mouseup', Captura_mouseUp);
        Event.observe(window, 'mouseup', Captura_mouseUpGeneral);
   //]]>
   
   
</script>
</div>
<div id="webslice_ultima_hora" class="hslice entry-content">
<a style="display: none;" rel="entry-content" href="http://www.eleconomista.es/slices/web-slice.php?modulo=392"></a><span class="entry-title" style="display: none;">Última hora en elEconomista.es</span><span class="ttl" style="display: none;">15</span><div mod="392"><div class="urgente">
<small>Destacamos</small><h5><a href="http://www.eleconomista.es/firmas/noticias/6799514/06/15/La-politica-y-las-redes-sociales.html">¿Cómo será la política del futuro según vayan evolucionando las redes sociales? </a></h5>
</div></div>
</div>
<div id="webslice_ultima_hora" class="hslice entry-content">
<a style="display: none;" rel="entry-content" href="http://www.eleconomista.es/slices/web-slice.php?modulo=392"></a><span class="entry-title" style="display: none;">Última hora en elEconomista.es</span><span class="ttl" style="display: none;">15</span><div mod="3562">
<script type="text/javascript" src="./eco_files/ticker.js"></script><div class="urgente aviso urgente3">
<small>En EcoDiario.es</small><h5 id="output_3562"><a href="http://ecodiario.eleconomista.es/futbol/noticias/6799553/06/15/La-vulgaridad-de-Messi-con-Argentina-Aguero-y-Pastore-tiran-de-la-Albiceleste.html">Se perpetúa la maldición de la vulgaridad de Messi con Argentina</a></h5>
<div class="botones-uh"><p class="buh"><a href="http://www.eleconomista.es/interstitial/volver/269617462/construccion-inmobiliario/noticias/6784723/06/15/La-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html#" onclick="ticker_3562.previousCurrentNews(); return( false );"><i class="fa fa-angle-left"></i></a><a href="http://www.eleconomista.es/interstitial/volver/269617462/construccion-inmobiliario/noticias/6784723/06/15/La-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html#" onclick="ticker_3562.nextCurrentNews(); return( false );"><i class="fa fa-angle-right"></i></a></p></div>
</div>
<script type="text/javascript">//<![CDATA[
					ticker_3562 = new news_Ticker( 'output_3562', 'list_ul_text_3562',{typeSpeed:1,typing:false,tickSpeed:7500} ) ;
				//]]></script>
</div>
</div>
<div mod="1647" class="b930t">
<script language="javascript">//<![CDATA[
			sz = "960x30" ;
			st = "eleconomista.es" ;
			std = "http://ad.es.doubleclick.net/" ;
			kw = "269617462";
			 sect ="6784723";
			document.write( '<SCR'+'IPT language="JavaScript" SRC="'+std+'adj/'+st+'/economista_noticias_construcion;sect='+sect+';tile=3;kw='+kw+';sz='+sz+';ord='+randnum+'?"></SCR'+'IPT>' ) ;
		//]]></script><script language="JavaScript" src="./eco_files/economista_noticias_construcion(1)"></script><div class="GoogleActiveViewClass" id="DfpVisibilityIdentifier_2108607625123113"><a target="_blank" href="http://googleads.g.doubleclick.net/aclk?sa=L&ai=BriCiJUiBVeaqOI7GiQbmq67wDY6HookIAAAAEAEgADgAWK7g7bWAAmDVjdOCvAiCARdjYS1wdWItODAxODk0MzI3NTc5OTg3MbIBE3d3dy5lbGVjb25vbWlzdGEuZXO6AQlnZnBfaW1hZ2XIAQLaAbgBaHR0cDovL3d3dy5lbGVjb25vbWlzdGEuZXMvaW50ZXJzdGl0aWFsL3ZvbHZlci8yNjk2MTc0NjIvY29uc3RydWNjaW9uLWlubW9iaWxpYXJpby9ub3RpY2lhcy82Nzg0NzIzLzA2LzE1L0xhLUNhaXhhLWNyZWUtcXVlLWVsLXB1bHNvLWRlbC1zZWN0b3ItaW5tb2JpbGlhcmlvLWNvbWllbnphLWEtcmVjdXBlcmFyc2UuaHRtbJgCwgOpAqq-wJpt6LI-wAIC4AIA6gI0NDI3NS9lbGVjb25vbWlzdGEuZXMvZWNvbm9taXN0YV9ub3RpY2lhc19jb25zdHJ1Y2lvbvgC_9EekAPIBpgDyAaoAwHgBAGQBgGgBhTYBwE&num=0&cid=5GilLKqnWbTI01_-8B003Tyk&sig=AOD64_2p0yyq2wVVstzcAKL6IhDal2gjiQ&client=ca-pub-8018943275799871&adurl=http://www.acierto.com/ads/clk/%3Fa%3D14093%26e%3D1"><img src="./eco_files/18027090726938548968" width="960" height="30" alt="" border="0"></a><script>(function(){var g=this,l=function(a,b){var c=a.split("."),d=g;c[0]in d||!d.execScript||d.execScript("var "+c[0]);for(var e;c.length&&(e=c.shift());)c.length||void 0===b?d=d[e]?d[e]:d[e]={}:d[e]=b},m=function(a,b,c){return a.call.apply(a.bind,arguments)},n=function(a,b,c){if(!a)throw Error();if(2<arguments.length){var d=Array.prototype.slice.call(arguments,2);return function(){var c=Array.prototype.slice.call(arguments);Array.prototype.unshift.apply(c,d);return a.apply(b,c)}}return function(){return a.apply(b,arguments)}},p=function(a,b,c){p=Function.prototype.bind&&-1!=Function.prototype.bind.toString().indexOf("native code")?m:n;return p.apply(null,arguments)},q=Date.now||function(){return+new Date};var r=document,s=window;var t=function(a,b){for(var c in a)Object.prototype.hasOwnProperty.call(a,c)&&b.call(null,a[c],c,a)},w=function(a,b){a.google_image_requests||(a.google_image_requests=[]);var c=a.document.createElement("img");c.src=b;a.google_image_requests.push(c)};var x=function(a){return{visible:1,hidden:2,prerender:3,preview:4}[a.webkitVisibilityState||a.mozVisibilityState||a.visibilityState||""]||0},y=function(a){var b;a.mozVisibilityState?b="mozvisibilitychange":a.webkitVisibilityState?b="webkitvisibilitychange":a.visibilityState&&(b="visibilitychange");return b};var C=function(){this.g=r;this.k=s;this.j=!1;this.i=null;this.h=[];this.o={};if(z)this.i=q();else if(3==x(this.g)){this.i=q();var a=p(this.q,this);A&&(a=A("di::vch",a));this.p=a;var b=this.g,c=y(this.g);b.addEventListener?b.addEventListener(c,a,!1):b.attachEvent&&b.attachEvent("on"+c,a)}else B(this)},A;C.m=function(){return C.n?C.n:C.n=new C};var D=/^([^:]+:\/\/[^/]+)/m,G=/^\d*,(.+)$/m,z=!1,B=function(a){if(!a.j){a.j=!0;for(var b=0;b<a.h.length;++b)a.l.apply(a,a.h[b]);a.h=[]}};C.prototype.s=function(a,b){var c=b.target.u();(c=G.exec(c))&&(this.o[a]=c[1])};C.prototype.l=function(a,b){this.k.rvdt=this.i?q()-this.i:0;var c;if(c=this.t)t:{try{var d=D.exec(this.k.location.href),e=D.exec(a);if(d&&e&&d[1]==e[1]&&b){var f=p(this.s,this,b);this.t(a,f);c=!0;break t}}catch(u){}c=!1}c||w(this.k,a)};C.prototype.q=function(){if(3!=x(this.g)){B(this);var a=this.g,b=y(this.g),c=this.p;a.removeEventListener?a.removeEventListener(b,c,!1):a.detachEvent&&a.detachEvent("on"+b,c)}};var H=/^true$/.test("")?!0:!1;var I={},J=function(a){var b=a.toString();a.name&&-1==b.indexOf(a.name)&&(b+=": "+a.name);a.message&&-1==b.indexOf(a.message)&&(b+=": "+a.message);if(a.stack){a=a.stack;var c=b;try{-1==a.indexOf(c)&&(a=c+"\n"+a);for(var d;a!=d;)d=a,a=a.replace(/((https?:\/..*\/)[^\/:]*:\d+(?:.|\n)*)\2/,"$1");b=a.replace(/\n */g,"\n")}catch(e){b=c}}return b},M=function(a,b,c,d){var e=K,f,u=!0;try{f=b()}catch(h){try{var N=J(h);b="";h.fileName&&(b=h.fileName);var E=-1;h.lineNumber&&(E=h.lineNumber);var v;t:{try{v=c?c():"";break t}catch(S){}v=""}u=e(a,N,b,E,v)}catch(k){try{var O=J(k);a="";k.fileName&&(a=k.fileName);c=-1;k.lineNumber&&(c=k.lineNumber);K("pAR",O,a,c,void 0,void 0)}catch(F){L({context:"mRE",msg:F.toString()+"\n"+(F.stack||"")},void 0)}}if(!u)throw h;}finally{if(d)try{d()}catch(T){}}return f},K=function(a,b,c,d,e,f){a={context:a,msg:b.substring(0,512),eid:e&&e.substring(0,40),file:c,line:d.toString(),url:r.URL.substring(0,512),ref:r.referrer.substring(0,512)};P(a);L(a,f);return!0},L=function(a,b){try{if(Math.random()<(b||.01)){var c="/pagead/gen_204?id=jserror"+Q(a),d="http"+("https:"==s.location.protocol?"s":"")+"://pagead2.googlesyndication.com"+c,d=d.substring(0,2E3);w(s,d)}}catch(e){}},P=function(a){var b=a||{};t(I,function(a,d){b[d]=s[a]})},R=function(a,b,c,d,e){return function(){var f=arguments;return M(a,function(){return b.apply(c,f)},d,e)}},Q=function(a){var b="";t(a,function(a,d){if(0===a||a)b+="&"+d+"="+("function"==typeof encodeURIComponent?encodeURIComponent(a):escape(a))});return b};A=function(a,b,c,d){return R(a,b,void 0,c,d)};z=H;l("vu",R("vu",function(a,b){var c=a.replace("&amp;","&"),d=/(google|doubleclick).*\/pagead\/adview/.test(c),e=C.m();if(d){d="&vis="+x(e.g);b&&(d+="&ve=1");var f=c.indexOf("&adurl"),c=-1==f?c+d:c.substring(0,f)+d+c.substring(f)}e.j?e.l(c,b):e.h.push([c,b])}));l("vv",R("vv",function(){z&&B(C.m())}));})();</script><script>vu("http://pubads.g.doubleclick.net/pagead/adview?ai\x3dBSuBQJUiBVeaqOI7GiQbmq67wDY6HookIAAAAEAEgADgAWK7g7bWAAmDVjdOCvAiCARdjYS1wdWItODAxODk0MzI3NTc5OTg3MbIBE3d3dy5lbGVjb25vbWlzdGEuZXO6AQlnZnBfaW1hZ2XIAQLaAbgBaHR0cDovL3d3dy5lbGVjb25vbWlzdGEuZXMvaW50ZXJzdGl0aWFsL3ZvbHZlci8yNjk2MTc0NjIvY29uc3RydWNjaW9uLWlubW9iaWxpYXJpby9ub3RpY2lhcy82Nzg0NzIzLzA2LzE1L0xhLUNhaXhhLWNyZWUtcXVlLWVsLXB1bHNvLWRlbC1zZWN0b3ItaW5tb2JpbGlhcmlvLWNvbWllbnphLWEtcmVjdXBlcmFyc2UuaHRtbJgCwgOpAqq-wJpt6LI-wAIC4AIA6gI0NDI3NS9lbGVjb25vbWlzdGEuZXMvZWNvbm9taXN0YV9ub3RpY2lhc19jb25zdHJ1Y2lvbvgC_9EekAPIBpgDyAaoAwHgBAGSBQsIBxABGAEgts6AEZAGAaAGFNgHAQ\x26sigh\x3da0hfGlEuJu4\x26cid\x3d5GilLKqnWbTI01_-8B003Tyk")</script><script>vu("http://www.acierto.com/ads/imp/?a\x3d14093\x26e\x3d8")</script></div><noscript>&lt;a href="http://ad.doubleclick.net/N4275/jump/eleconomista.es/economista_noticias_construcion;sect=6784723;tile=3;kw=269617462;sz=960x30;ord=123456789?" target="_blank" rel="nofollow"&gt;&lt;img src="http://ad.doubleclick.net/N4275/ad/eleconomista.es/economista_noticias_construcion;sect=6784723;tile=3;kw=269617462;sz=960x30;ord=123456789?" width="960" height="30" border="0"&gt;&lt;/a&gt;</noscript>
</div>
<div id="banners-superiores_2">
<script type="text/javascript" language="javascript">//<![CDATA[
		var time = new Date() ;
		randnum= (time.getTime()) ;
	//]]></script><script language="javascript">//<![CDATA[
		sz = "1x2" ;
		st = "eleconomista.es" ;
		std = "http://ad.es.doubleclick.net/" ;
		isnt = "";
		
		document.write( '<SCR'+'IPT language="JavaScript" SRC="'+std+'adj/'+st+'/economista_noticias_construcion;tile=11;dcopt=ist;sz='+sz+';isnt='+isnt+';ord='+randnum+'?"></SCR'+'IPT>' ) ;
	//]]></script><script language="JavaScript" src="./eco_files/economista_noticias_construcion(2)"></script><div class="GoogleActiveViewClass" id="DfpVisibilityIdentifier_7368837958201766"><script>(function(){var g=this,l=function(a,b){var c=a.split("."),d=g;c[0]in d||!d.execScript||d.execScript("var "+c[0]);for(var e;c.length&&(e=c.shift());)c.length||void 0===b?d=d[e]?d[e]:d[e]={}:d[e]=b},m=function(a,b,c){return a.call.apply(a.bind,arguments)},n=function(a,b,c){if(!a)throw Error();if(2<arguments.length){var d=Array.prototype.slice.call(arguments,2);return function(){var c=Array.prototype.slice.call(arguments);Array.prototype.unshift.apply(c,d);return a.apply(b,c)}}return function(){return a.apply(b,arguments)}},p=function(a,b,c){p=Function.prototype.bind&&-1!=Function.prototype.bind.toString().indexOf("native code")?m:n;return p.apply(null,arguments)},q=Date.now||function(){return+new Date};var r=document,s=window;var t=function(a,b){for(var c in a)Object.prototype.hasOwnProperty.call(a,c)&&b.call(null,a[c],c,a)},w=function(a,b){a.google_image_requests||(a.google_image_requests=[]);var c=a.document.createElement("img");c.src=b;a.google_image_requests.push(c)};var x=function(a){return{visible:1,hidden:2,prerender:3,preview:4}[a.webkitVisibilityState||a.mozVisibilityState||a.visibilityState||""]||0},y=function(a){var b;a.mozVisibilityState?b="mozvisibilitychange":a.webkitVisibilityState?b="webkitvisibilitychange":a.visibilityState&&(b="visibilitychange");return b};var C=function(){this.g=r;this.k=s;this.j=!1;this.i=null;this.h=[];this.o={};if(z)this.i=q();else if(3==x(this.g)){this.i=q();var a=p(this.q,this);A&&(a=A("di::vch",a));this.p=a;var b=this.g,c=y(this.g);b.addEventListener?b.addEventListener(c,a,!1):b.attachEvent&&b.attachEvent("on"+c,a)}else B(this)},A;C.m=function(){return C.n?C.n:C.n=new C};var D=/^([^:]+:\/\/[^/]+)/m,G=/^\d*,(.+)$/m,z=!1,B=function(a){if(!a.j){a.j=!0;for(var b=0;b<a.h.length;++b)a.l.apply(a,a.h[b]);a.h=[]}};C.prototype.s=function(a,b){var c=b.target.u();(c=G.exec(c))&&(this.o[a]=c[1])};C.prototype.l=function(a,b){this.k.rvdt=this.i?q()-this.i:0;var c;if(c=this.t)t:{try{var d=D.exec(this.k.location.href),e=D.exec(a);if(d&&e&&d[1]==e[1]&&b){var f=p(this.s,this,b);this.t(a,f);c=!0;break t}}catch(u){}c=!1}c||w(this.k,a)};C.prototype.q=function(){if(3!=x(this.g)){B(this);var a=this.g,b=y(this.g),c=this.p;a.removeEventListener?a.removeEventListener(b,c,!1):a.detachEvent&&a.detachEvent("on"+b,c)}};var H=/^true$/.test("")?!0:!1;var I={},J=function(a){var b=a.toString();a.name&&-1==b.indexOf(a.name)&&(b+=": "+a.name);a.message&&-1==b.indexOf(a.message)&&(b+=": "+a.message);if(a.stack){a=a.stack;var c=b;try{-1==a.indexOf(c)&&(a=c+"\n"+a);for(var d;a!=d;)d=a,a=a.replace(/((https?:\/..*\/)[^\/:]*:\d+(?:.|\n)*)\2/,"$1");b=a.replace(/\n */g,"\n")}catch(e){b=c}}return b},M=function(a,b,c,d){var e=K,f,u=!0;try{f=b()}catch(h){try{var N=J(h);b="";h.fileName&&(b=h.fileName);var E=-1;h.lineNumber&&(E=h.lineNumber);var v;t:{try{v=c?c():"";break t}catch(S){}v=""}u=e(a,N,b,E,v)}catch(k){try{var O=J(k);a="";k.fileName&&(a=k.fileName);c=-1;k.lineNumber&&(c=k.lineNumber);K("pAR",O,a,c,void 0,void 0)}catch(F){L({context:"mRE",msg:F.toString()+"\n"+(F.stack||"")},void 0)}}if(!u)throw h;}finally{if(d)try{d()}catch(T){}}return f},K=function(a,b,c,d,e,f){a={context:a,msg:b.substring(0,512),eid:e&&e.substring(0,40),file:c,line:d.toString(),url:r.URL.substring(0,512),ref:r.referrer.substring(0,512)};P(a);L(a,f);return!0},L=function(a,b){try{if(Math.random()<(b||.01)){var c="/pagead/gen_204?id=jserror"+Q(a),d="http"+("https:"==s.location.protocol?"s":"")+"://pagead2.googlesyndication.com"+c,d=d.substring(0,2E3);w(s,d)}}catch(e){}},P=function(a){var b=a||{};t(I,function(a,d){b[d]=s[a]})},R=function(a,b,c,d,e){return function(){var f=arguments;return M(a,function(){return b.apply(c,f)},d,e)}},Q=function(a){var b="";t(a,function(a,d){if(0===a||a)b+="&"+d+"="+("function"==typeof encodeURIComponent?encodeURIComponent(a):escape(a))});return b};A=function(a,b,c,d){return R(a,b,void 0,c,d)};z=H;l("vu",R("vu",function(a,b){var c=a.replace("&amp;","&"),d=/(google|doubleclick).*\/pagead\/adview/.test(c),e=C.m();if(d){d="&vis="+x(e.g);b&&(d+="&ve=1");var f=c.indexOf("&adurl"),c=-1==f?c+d:c.substring(0,f)+d+c.substring(f)}e.j?e.l(c,b):e.h.push([c,b])}));l("vv",R("vv",function(){z&&B(C.m())}));})();</script><script>vu("http://pubads.g.doubleclick.net/pagead/adview?ai\x3dB_58iJkiBVcHUB4TCiQaqlYCwCtbE5OUFAAAAEAEgADgAWNaWlY-6AWDVjdOCvAiCARdjYS1wdWItODAxODk0MzI3NTc5OTg3MbIBE3d3dy5lbGVjb25vbWlzdGEuZXO6AQlnZnBfaW1hZ2XIAQnaAbgBaHR0cDovL3d3dy5lbGVjb25vbWlzdGEuZXMvaW50ZXJzdGl0aWFsL3ZvbHZlci8yNjk2MTc0NjIvY29uc3RydWNjaW9uLWlubW9iaWxpYXJpby9ub3RpY2lhcy82Nzg0NzIzLzA2LzE1L0xhLUNhaXhhLWNyZWUtcXVlLWVsLXB1bHNvLWRlbC1zZWN0b3ItaW5tb2JpbGlhcmlvLWNvbWllbnphLWEtcmVjdXBlcmFyc2UuaHRtbKkCqr7Amm3osj7AAgLgAgDqAjQ0Mjc1L2VsZWNvbm9taXN0YS5lcy9lY29ub21pc3RhX25vdGljaWFzX2NvbnN0cnVjaW9u-AL_0R6AAwGQA8gGmAPIBqgDAeAEAZAGAaAGFtgHAA\x26sigh\x3dipQ4HQO0k5c\x26cid\x3d5Gh2cQuVi1jQHyI9RNHXq9do")</script><script type="text/javascript">
	window._ttf = window._ttf || []
		,doc = window.top.document
		,_tt_slot = 'div#cuerpo_noticia > p, .principal p'
		,_tt_before = false
		,_tt_BTF = true
		,_tt_minSlot = ''
		,_tt_check = doc.querySelectorAll('.tabla').length > 0;
	if (_tt_check) {
		_tt_slot = '.tabla';
		_tt_before = true;
		_tt_minSlot = 0;
		_tt_BTF = false;
	}
	_ttf.push({
		pid: 18062
		,lang: 'es'
		,slot: _tt_slot
		,minSlot: _tt_minSlot
		,BTF: _tt_BTF
		,before: _tt_before
		,format: 'inread'
		,css: 'padding-bottom: 10px; padding-top: 10px;'

		,filter: function (obj) {return obj['wrappedSizes']['width'] > 450;}
	});
	(function(d){
		var js, s = d.getElementsByTagName('script')[0];
		js = d.createElement('script'); js.async = true;
		js.src = "http://cdn.teads.tv/js/all-v1.js";
		s.parentNode.insertBefore(js, s);
	})(window.document);
</script>
</div><noscript>&lt;a href="http://ad.doubleclick.net/N4275/jump/eleconomista.es/economista_noticias_construcion;tile=11;sz=1x2;ord=123456789?" target="_blank" rel="nofollow"&gt;&lt;img src="http://ad.doubleclick.net/N4275/ad/eleconomista.es/economista_noticias_construcion;sz=1x2;ord=123456789?" width="1" height="1" border="0"&gt;&lt;/a&gt;</noscript>
</div>
<div class="wrap-columns-int"><div id="cuerpo_noticia" class="noticiacuerpo"><div class="cols-i noticia-des"><div mod="7">
<script type="text/javascript">
	//<![CDATA[
		miLista = new miListaValores( 'a' ) ;
	//]]>
	</script><script type="text/javascript">//<![CDATA[
	function abrirDelicious()
	{
		window.open('http://del.icio.us/post?v=4&noui&jump=close&url='+encodeURIComponent(location.href) + '&title=' + encodeURIComponent(document.title),'delicious','toolbar=0,width=700,height=400');
		return false;
	}
	
	function abrirTuenti()
	{
		window.open('http://www.tuenti.com/share?url='+encodeURIComponent(location.href));
		return false;
	}
	
	function abrirDigg(titulo, texto)
	{
	
		window.open('http://www.digg.com/submit?phase=2&url=' + encodeURIComponent(location.href) + '&title=' + titulo + '&bodytext=' + texto + '&topic=business_finance', 'digg','toolbar=no, scrollbars=yes,resizable=yes');
		return false;
		
	}
	
	function abrirMeneame()
	{
		window.open('http://meneame.net/submit.php?url=%20'+location.href ,'meneame','toolbar=yes, scrollbars=yes,resizable=yes');
		return false;
	}

	function abrirTwitter()
	{
		window.open('http://twitter.com/home?status='+location.href ,'Twitter','toolbar=yes, scrollbars=yes,resizable=yes');
		return false;
	}

	function abrirFacebook()
	{
		window.open('http://www.facebook.com/share.php?u='+location.href ,'Facebook','toolbar=yes, scrollbars=yes,resizable=yes');
		return false;
	}
	
	var tam_actual = 2;
	function actualizarTamano (signo)
	{
		tam = tam_actual;
		if ( ((signo==1) && (tam_actual < 4)) || ((signo==-1) && (tam_actual > 1))){
			tam_actual = tam_Siguiente = tam + signo;

			$$('div#cuerpo_noticia > p').each(
					function(s) {
						if (s.hasClassName('tam'+tam)) 
							s.removeClassName('tam'+tam);
						s.addClassName('tam'+tam_Siguiente)
					});

			$$('div#cuerpo_noticia > h2').each(
					function(s) {
						if (s.hasClassName('tam'+tam)) 
							s.removeClassName('tam'+tam);
						s.addClassName('tam'+tam_Siguiente)
					});
		}
	}
	
	
			enviarNoticiaLeida_Site( 6784723, 1 ) ;
		//]]></script><div id="cuerpo_noticia" class="noticiacuerpo">
<h1>La Caixa cree que el pulso del sector inmobiliario comienza a recuperarse</h1>
<div class="firma efe">
<div class="f-autor"><a href="http://www.efe.com/" target="_blank"><img src="./eco_files/efe-p.png" alt="EFE"></a></div>
<div class="f-fecha">11/06/2015 - 14:44</div>
</div>
<div class="s-horizontal">
<div class="s-h-c">
<script src="./eco_files/widgets.js" type="text/javascript"></script><iframe id="twitter-widget-0" scrolling="no" frameborder="0" allowtransparency="true" src="./eco_files/tweet_button.c197f64a14a7434e6e58ca9722b54406.en.html" class="twitter-share-button twitter-tweet-button twitter-share-button twitter-count-horizontal" title="Twitter Tweet Button" data-twttr-rendered="true" style="position: static; visibility: visible; width: 78px; height: 20px;"></iframe>
</div>
<div class="s-h-c2">
<div id="fb-root"></div>
<div class="fb-share-button fb_iframe_widget" data-width="150" data-type="button_count" tyle="position: relative; display: inline;" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=132138756987&amp;container_width=130&amp;href=http%3A%2F%2Fwww.eleconomista.es%2Finterstitial%2Fvolver%2F269617462%2Fconstruccion-inmobiliario%2Fnoticias%2F6784723%2F06%2F15%2FLa-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html%23.Kku89JqkvVqPDB9&amp;locale=es_ES&amp;sdk=joey&amp;type=button_count&amp;width=150"><span style="vertical-align: bottom; width: 107px; height: 20px;"><iframe name="f31f49910" width="150px" height="1000px" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" title="fb:share_button Facebook Social Plugin" src="./eco_files/share_button.html" style="border: none; visibility: visible; width: 107px; height: 20px;" class=""></iframe></span></div>
</div>
<div class="s-h-c">
<div id="___plusone_0" style="text-indent: 0px; margin: 0px; padding: 0px; border-style: none; float: none; line-height: normal; font-size: 1px; vertical-align: baseline; display: inline-block; width: 90px; height: 20px; background: transparent;"><iframe frameborder="0" hspace="0" marginheight="0" marginwidth="0" scrolling="no" style="position: static; top: 0px; width: 90px; margin: 0px; border-style: none; left: 0px; visibility: visible; height: 20px;" tabindex="0" vspace="0" width="100%" id="I0_1434535974695" name="I0_1434535974695" src="./eco_files/fastbutton.html" data-gapiattached="true" title="+1"></iframe></div>
<script type="text/javascript">
		  window.___gcfg = {lang: 'es'};

		  (function() {
		    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
		    po.src = 'https://apis.google.com/js/plusone.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
		  })();
		</script>
</div>
<div class="s-h-c2">
<script src="./eco_files/in.js" type="text/javascript"></script><span class="IN-widget" style="line-height: 1; vertical-align: baseline; display: inline-block; text-align: center;"><span style="padding: 0px !important; margin: 0px !important; text-indent: 0px !important; display: inline-block !important; vertical-align: baseline !important; font-size: 1px !important;"><span id="li_ui_li_gen_1434535975073_0"><a id="li_ui_li_gen_1434535975073_0-link" href="javascript:void(0);"><span id="li_ui_li_gen_1434535975073_0-logo">in</span><span id="li_ui_li_gen_1434535975073_0-title"><span id="li_ui_li_gen_1434535975073_0-mark"></span><span id="li_ui_li_gen_1434535975073_0-title-text">Share</span></span></a></span></span><span style="padding: 0px !important; margin: 0px !important; text-indent: 0px !important; display: inline-block !important; vertical-align: baseline !important; font-size: 1px !important;"><span id="li_ui_li_gen_1434535975095_1-container" class="IN-right IN-hidden"><span id="li_ui_li_gen_1434535975095_1" class="IN-right"><span id="li_ui_li_gen_1434535975095_1-inner" class="IN-right"><span id="li_ui_li_gen_1434535975095_1-content" class="IN-right">0</span></span></span></span></span></span><script type="IN/Share+init" data-counter="right"></script>
</div>
<div class="wfavicon" sharecount="right" data-tooltip="false"><span class="wfavicon-tooltip" style="display: none;">Comparte esta noticia en Womenalia.com con un solo clic. Date de alta gratis en la 1ª Red Social de Networking Mundial de mujeres profesionales. Solo te llevará 15 segundos.</span><a class="wfavicon-popup" href="http://www.womenalia.com/wfavicon?url=http%3A%2F%2Fwww.eleconomista.es%2Finterstitial%2Fvolver%2F269617462%2Fconstruccion-inmobiliario%2Fnoticias%2F6784723%2F06%2F15%2FLa-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html%23.Kku8pmt05Z0CK4x" title="Wow!">Wow!</a><div class="wShareRight" style="box-sizing: border-box;max-width: 100%;margin: 0 0 0 5px;position: relative;border: #bbb solid 1px;border-radius: 3px;min-height: 18px;min-width: 15px;display: inline-block;vertical-align: top;zoom: 1; height: 21px;"><i></i><span style="padding-left: 3px;font-size: 12px;">0</span></div></div>
<script type="text/javascript">(function(d, t, i, l) { var g = d.createElement(t), s = d.getElementsByTagName(t)[0]; g.src = "http://static.womenalia.net/media/wfaviconstyles/wfavicon.min.js"; g.id=i; g.lang=l; s.parentNode.insertBefore(g, s); }(document, "script", "wow-wfavicon", "es-ES"));</script>
</div>
<div class="p-tag" id="lista_tags">
<i style="float:right; margin:0 2px 0 2px; position: relative; right: 0;font-size: 25px;" id="abrir_tags" alt="tags" class="fa fa-angle-down" onclick="$(&#39;lista_tags&#39;).style.height=&#39;auto&#39;;$(&#39;abrir_tags&#39;).style.display=&#39;none&#39;;$(&#39;cerrar_tags&#39;).style.display=&#39;block&#39;"></i><span style="background:none; width:auto;">Más noticias sobre:</span><ul>
<li><a href="http://www.eleconomista.es/tags/mercado-inmobiliario">Mercado inmobiliario</a></li>
<li><a href="http://www.eleconomista.es/tags/Banca">Banca</a></li>
<li><a href="http://www.eleconomista.es/tags/stock-de-viviendas-">Stock de viviendas</a></li>
<li><a href="http://www.eleconomista.es/tags/compraventa">Compraventa</a></li>
<li><a href="http://www.eleconomista.es/tags/pib">Pib</a></li>
</ul>
<i id="cerrar_tags" alt="tags" style="float:right; margin:15px 2px 0 2px; display:none;font-size: 25px;" onclick="$(&#39;lista_tags&#39;).style.height=&#39;35px&#39;;$(&#39;abrir_tags&#39;).style.display=&#39;block&#39;;$(&#39;cerrar_tags&#39;).style.display=&#39;none&#39;" class="fa fa-angle-up"></i>
</div>
<div class="i-not"><div class="foto-not-g"><img src="./eco_files/20150611-10867172w.jpg" alt="/imag/efe/2015/06/11/20150611-10867172w.jpg - 225x250" id="2040854"></div><div class="relacionados">
<h5>Enlaces relacionados</h5>
<h2 class="ico-general"><a href="http://www.eleconomista.es/economia/noticias/6784698/06/15/Economia-Vivienda-El-Banco-de-Espana-ve-incertidumbre-en-la-recuperacion-del-sector-inmobiliario.html">Economía/Vivienda.- El Banco de España ve "incertidumbre" en la recuperación del sector inmobiliario (11/06)</a></h2>
<h2 class="ico-general"><a href="http://www.eleconomista.es/economia/noticias/6784715/06/15/El-Banco-de-Espana-ve-incertidumbre-en-la-recuperacion-del-sector-inmobiliario.html">El Banco de España ve "incertidumbre" en la recuperación del sector inmobiliario (11/06)</a></h2>
<h2 class="ico-general"><a href="http://www.eleconomista.es/economia/noticias/6775177/06/15/La-socimi-Trajano-saldra-al-MAB-e-invertira-190-millones-en-el-sector-inmobiliario-espanol.html">La socimi Trajano saldrá al MAB e invertirá 190 millones en el sector inmobiliario español (8/06)</a></h2>
<h2 class="ico-general"><a href="http://www.eleconomista.es/economia/noticias/6759793/06/15/Vivienda-bbva-asegura-que-el-sector-inmobiliario-se-estabiliza-sobre-cimientos-solidos.html">Vivienda. bbva asegura que el sector inmobiliario se estabiliza sobre cimientos sólidos (2/06)</a></h2>
<h2 class="ico-general"><a href="http://www.eleconomista.es/construccion-inmobiliario/noticias/6750814/05/15/El-sector-inmobiliario-uno-de-los-pocos-que-aun-ofrece-rentabilidad-atractiva.html">El sector inmobiliario, uno de los pocos que aún ofrece rentabilidad atractiva (29/05)</a></h2>
</div>
<hr>
<div class="controles2"><ul>
<li class="noti-send"><a href="http://www.eleconomista.es/noticias-email/6784723/La-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse" id="enviar" rel="nofollow"><i class="fa fa-envelope"></i></a></li>
<li><a href="javascript:window.print();" id="imprimir"><i class="fa fa-print"></i></a></li>
<li><a href="http://www.eleconomista.es/interstitial/volver/269617462/construccion-inmobiliario/noticias/6784723/06/15/La-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html#" id="aumentar" onclick="return actualizarTamano(1);"><i class="fa fa-font"></i><i class="fa fa-plus"></i></a></li>
<li><a href="http://www.eleconomista.es/interstitial/volver/269617462/construccion-inmobiliario/noticias/6784723/06/15/La-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html#" id="reducir" onclick="return actualizarTamano(-1);"><i class="fa fa-font"></i><i class="fa fa-minus"></i></a></li>
</ul></div>
<hr>
</div>
<p>Barcelona, 11 jun (EFE).- La Caixa Research, el servicio de estudios y análisis económico de La Caixa, cree que "el pulso del sector inmobiliario empieza a recuperarse" y que "el recorrido al alza de la demanda y la oferta (de vivienda) es amplio", aunque augura que la recuperación será "gradual" en ambos frentes.</p><p>Así lo asegura en un análisis correspondiente al mes de mayo dedicado al sector inmobiliario, en el cual apunta que, pese a que "el nuevo ciclo empieza con unas constantes vitales saludables", será "imprescindible" ir tomando la temperatura al sector para certificar que no se comenten "errores del pasado".</p>
<p>La Caixa Research extrae estas conclusiones a partir del termómetro inmobiliario que elabora, y que se nutre de tres subindicadores: uno dedicado a la oferta, otro a la demanda y un tercero a los precios.</p>
<p>El servicio de estudios y análisis de La Caixa sostiene, respecto a la demanda de vivienda, que a pesar de anotarse "un ligero aumento" en los últimos meses, "todavía se encuentra por debajo de los niveles considerados de equilibrio".</p>
<p>La nota comenta que, de momento, los extranjeros impulsan este "avance" en la compraventa de viviendas, pero que para alcanzar esos niveles de equilibrio "es imprescindible que la demanda doméstica se sume al crecimiento".</p><div id="tt-wrapper68070f4" class="tt-wrapper inread " style="padding-bottom: 10px; padding-top: 10px; height: 0px; text-align: center; max-width: 100%; min-width: 230px; overflow: hidden; margin-top: 0px; position: absolute; left: -999em; -webkit-transition: all 0.4s cubic-bezier(0.65, 0.52, 0.73, 0.43); transition: all 0.4s cubic-bezier(0.65, 0.52, 0.73, 0.43); -webkit-transform: translateZ(0px); transform: translateZ(0px); -webkit-backface-visibility: hidden; backface-visibility: hidden; -webkit-perspective: 1000;"> <div id="tt-mention68070f4" class="tt-mention inread " style="; ;">PUBLICIDAD</div><div id="tt-container68070f4" class="tt-container inread " style=";"><div id="tt-player68070f4" class="tt-player inread " style=";"></div> <div id="tt-controls68070f4" class="tt-controls inread " style=";"></div></div> <div id="tt-viewport68070f4" class="tt-viewport inread " style="height: 0px; margin-top: 64.5px;"></div></div>
<p>En este punto, La Caixa espera que la demanda doméstica de vivienda "gane vigor en 2015, gracias a un entorno económico más propicio, con un crecimiento del PIB y del empleo cercano al 3 % y una mejora de las condiciones de financiación".</p>
<p>Respecto a la oferta, La Caixa augura "un buen 2015" gracias al aumento de la inversión residencial, de las peticiones de visados para nueva construcción y de la ocupación en el sector", unas tendencias que "deberían consolidarse" este año, a pesar de que el stock de viviendas es todavía "muy elevado en algunas regiones", apunta.</p>
<p>En cuanto a los precios, el informe sí cree que han alcanzado "niveles de equilibrio" tras estabilizarse en el último trimestre de 2014.</p>
<p>No obstante, desde el primer trimestre de 2008, el punto álgido de la burbuja inmobiliaria, los precios han caído un 37 % en términos reales, un 30 % en términos nominales, según La Caixa.</p>
<p>Asimismo, el servicio de estudios de La Caixa asegura que, de cara a aumentar la demanda de vivienda, será clave la consolidación del mercado laboral, mientras que la recuperación de la oferta "sigue estando muy condicionada por el stock de viviendas", considera.</p><div class="canales"><ul>
<li><a href="http://www.eleconomista.es/noticias/empresas-finanzas" id="categoria_9" onclick="return false;">Empresas y finanzas</a></li>
<script type="text/javascript">create_Balloon(9,1, 'categoria_9');</script><li><a href="http://www.eleconomista.es/noticias/construccion-inmobiliario" id="categoria_40" onclick="return false;">Construccion Inmobiliario</a></li>
<script type="text/javascript">create_Balloon(40,1, 'categoria_40');</script>
</ul></div>
<hr>
<hr>
<h5 style="font-size: 11px;margin: 0 0 3px 0;padding: 2px 7px 2px 0;">PUBLICIDAD</h5>
<div style="background: #EEE;height: 28px;overflow: hidden;"><iframe width="655" height="28" src="./eco_files/widgets.html"></iframe></div>
<hr>
<div class="pub-ads">
<div id="cX-root" style="display:none"></div>
<iframe id="cxWidget_0.4829791320953518" name="cxWidget_0.4829791320953518" width="630" height="240" src="./eco_files/PAN_Combo_630x240_ElEconomista.html" style="display: block;" scrolling="no" frameborder="0"></iframe><div id="panComboAds_630x240" style="display:none"></div>
<script type="text/javascript">

			var cX = cX || {}; 
			cX.callQueue = cX.callQueue || [];

			cX.callQueue.push(['insertAdSpace', { adSpaceId: '00000000013a878d',
												insertBeforeElementId: 'panComboAds_630x240',
												adUnitWidth: 630, 
												adUnitHeight: 240,
												k: 'panbackfill',
												initialHorizontalAdUnits: 1, 
												initialVerticalAdUnits: 1,
												renderTemplateUrl: 'http://cdn.cxpublic.com/PAN_Combo_630x240_ElEconomista.html'

								} ]);
		  cX.callQueue.push(['setAccountId', '9222287489163341896']);
		  cX.callQueue.push(['setSiteId', '9222287489163341897']);
		  cX.callQueue.push(['sendPageViewEvent']);

		</script><script type="text/javascript">
			(function() { try { var scriptEl = document.createElement('script');
								scriptEl.type = 'text/javascript'; 
								scriptEl.async = 'async';
								scriptEl.src = ('https:' == document.location.protocol) ?
										'https://scdn.cxense.com/cx.js' : 'http://cdn.cxense.com/cx.js';
								var targetEl = document.getElementsByTagName('script')[0];
								targetEl.parentNode.insertBefore(scriptEl, targetEl); 
							} 
						catch (e) {};} ());

	</script>
</div>
<div class="fb-like fb_iframe_widget" data-send="true" data-width="450" data-show-faces="false" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=132138756987&amp;container_width=665&amp;href=http%3A%2F%2Fwww.eleconomista.es%2Finterstitial%2Fvolver%2F269617462%2Fconstruccion-inmobiliario%2Fnoticias%2F6784723%2F06%2F15%2FLa-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html%23.Kku89JqkvVqPDB9&amp;locale=es_ES&amp;sdk=joey&amp;send=true&amp;show_faces=false&amp;width=450"><span style="vertical-align: bottom; width: 450px; height: 20px;"><iframe name="f2c8455e74" width="450px" height="1000px" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" title="fb:like Facebook Social Plugin" src="./eco_files/like(1).html" style="border: none; visibility: visible; width: 450px; height: 20px;" class=""></iframe></span></div>
<iframe id="twitter-widget-1" scrolling="no" frameborder="0" allowtransparency="true" src="./eco_files/tweet_button.c197f64a14a7434e6e58ca9722b54406.es.html" class="twitter-share-button twitter-tweet-button twitter-share-button twitter-count-horizontal" title="Twitter Tweet Button" data-twttr-rendered="true" style="position: static; visibility: visible; width: 89px; height: 20px;"></iframe><hr>
<iframe id="cxWidget_0.36661816551350057" name="cxWidget_0.36661816551350057" width="630" height="290" src="./eco_files/ee-hor.html" style="display: block;" scrolling="no" frameborder="0"></iframe><div id="recsTargetEl" style="display:none"></div>
<script type="text/javascript">
        var cX = cX || {}; cX.callQueue = cX.callQueue || [];
        cX.callQueue.push(['insertWidget',{
            width: 630, height: 290,
            resizeToContentSize: true,
            insertBeforeElementId: 'recsTargetEl',
            renderTemplateUrl: 'http://cdn.cxpublic.com/ee-hor.html'
        }]);
    </script><script type="text/javascript">
        (function() { try { var scriptEl = document.createElement('script'); scriptEl.type = 'text/javascript'; scriptEl.async = 'async';
        scriptEl.src = ('https:' == document.location.protocol) ? 'https://scdn.cxense.com/cx.js' : 'http://cdn.cxense.com/cx.js';
        var targetEl = document.getElementsByTagName('script')[0]; targetEl.parentNode.insertBefore(scriptEl, targetEl); } catch (e) {};} ());
    </script>
</div>
</div>
</div></div>
		<div class="col">
			<div class="cont-flash" style="margin-bottom:10px;"><div mod="1246"><div id="webslice_flash_del_mercado" class="hslice entry-content">
<a style="display: none;" rel="entry-content" href="http://www.eleconomista.es/slices/web-slice.php?modulo=1246"></a><span class="ttl" style="display: none;">15</span><div class="flash caja" style="border-top:1px solid #666666;">
<h3 style="margin-top:-5px; margin-bottom:10px; padding:0; ">
<a href="http://www.eleconomista.es/flash/index.html">El Flash del mercado</a><span class="entry-title" style="display: none;">en elEconomista.es</span><a href="https://www.bancsabadell.com/" rel="nofollow" target="_blank"><img src="./eco_files/sabadell-PNG.png" alt="Sabadell Atlantico - " style="border: none; margin-left: 35px;"></a>
</h3>
<ul>
<li>
<span class="hora">11:57</span><a href="http://ecodiario.eleconomista.es/espana/noticias/6799582/06/15/Piden-la-dimision-de-la-directora-de-la-carcel-de-tras-conocerse-los-privilegios.html">Piden la dimisión de la directora de la cárcel de Isabel Pantoja tras conocerse los privilegios</a>
</li>
<li>
<span class="hora">12:08</span><a href="http://www.eleconomista.es/firmas/noticias/6799609/06/15/Nueve-claves-para-emprender-un-negocio-con-exito-empenzando-de-cero.html">Nueve claves para emprender un negocio con éxito empenzando de cero</a>
</li>
<li>
<span class="hora">12:08</span><a href="http://www.eleconomista.es/tecnologia/noticias/6799607/06/15/Ha-llegado-el-momento-de-debatir-sobre-la-neutralidad-de-la-red.html">Ha llegado el momento de debatir sobre la neutralidad de la red</a>
</li>
<li class="mas"><span class="sigue" style="display: inline; margin-right: 20px;"><a href="http://www.eleconomista.es/flash/index.html">Ver todos</a></span></li>
</ul>
</div>
<hr>
</div></div>

			</div>
			<hr>
			<div mod="7133">
<script language="javascript">//<![CDATA[
        
if( typeof( modulosSite) == "object") modulosSite.push("7134");
            
if( typeof( modulosReemplazados) == "object") modulosReemplazados.push("7133_1");
                
//]]></script><div mod="7133_1"></div>
</div>
<div mod="1273" class="b300">
<script language="javascript">//<![CDATA[
						sz = "300x250" ;
						st = "eleconomista.es" ;
						std = "http://ad.es.doubleclick.net/" ;
						kw = "269617462";
				//		randnum = old_randnum;
						 sect ="6784723";
						document.write( '<SCR'+'IPT language="JavaScript" SRC="'+std+'adj/'+st+'/economista_noticias_construcion;sect='+sect+';tile=5;kw='+kw+';sz='+sz+';ord='+randnum+'?"></SCR'+'IPT>' ) ;
					//]]></script><script language="JavaScript" src="./eco_files/economista_noticias_construcion(3)"></script><div class="GoogleActiveViewClass" id="DfpVisibilityIdentifier_9432513096835464"><script>(function(){var g=this,l=function(a,b){var c=a.split("."),d=g;c[0]in d||!d.execScript||d.execScript("var "+c[0]);for(var e;c.length&&(e=c.shift());)c.length||void 0===b?d=d[e]?d[e]:d[e]={}:d[e]=b},m=function(a,b,c){return a.call.apply(a.bind,arguments)},n=function(a,b,c){if(!a)throw Error();if(2<arguments.length){var d=Array.prototype.slice.call(arguments,2);return function(){var c=Array.prototype.slice.call(arguments);Array.prototype.unshift.apply(c,d);return a.apply(b,c)}}return function(){return a.apply(b,arguments)}},p=function(a,b,c){p=Function.prototype.bind&&-1!=Function.prototype.bind.toString().indexOf("native code")?m:n;return p.apply(null,arguments)},q=Date.now||function(){return+new Date};var r=document,s=window;var t=function(a,b){for(var c in a)Object.prototype.hasOwnProperty.call(a,c)&&b.call(null,a[c],c,a)},w=function(a,b){a.google_image_requests||(a.google_image_requests=[]);var c=a.document.createElement("img");c.src=b;a.google_image_requests.push(c)};var x=function(a){return{visible:1,hidden:2,prerender:3,preview:4}[a.webkitVisibilityState||a.mozVisibilityState||a.visibilityState||""]||0},y=function(a){var b;a.mozVisibilityState?b="mozvisibilitychange":a.webkitVisibilityState?b="webkitvisibilitychange":a.visibilityState&&(b="visibilitychange");return b};var C=function(){this.g=r;this.k=s;this.j=!1;this.i=null;this.h=[];this.o={};if(z)this.i=q();else if(3==x(this.g)){this.i=q();var a=p(this.q,this);A&&(a=A("di::vch",a));this.p=a;var b=this.g,c=y(this.g);b.addEventListener?b.addEventListener(c,a,!1):b.attachEvent&&b.attachEvent("on"+c,a)}else B(this)},A;C.m=function(){return C.n?C.n:C.n=new C};var D=/^([^:]+:\/\/[^/]+)/m,G=/^\d*,(.+)$/m,z=!1,B=function(a){if(!a.j){a.j=!0;for(var b=0;b<a.h.length;++b)a.l.apply(a,a.h[b]);a.h=[]}};C.prototype.s=function(a,b){var c=b.target.u();(c=G.exec(c))&&(this.o[a]=c[1])};C.prototype.l=function(a,b){this.k.rvdt=this.i?q()-this.i:0;var c;if(c=this.t)t:{try{var d=D.exec(this.k.location.href),e=D.exec(a);if(d&&e&&d[1]==e[1]&&b){var f=p(this.s,this,b);this.t(a,f);c=!0;break t}}catch(u){}c=!1}c||w(this.k,a)};C.prototype.q=function(){if(3!=x(this.g)){B(this);var a=this.g,b=y(this.g),c=this.p;a.removeEventListener?a.removeEventListener(b,c,!1):a.detachEvent&&a.detachEvent("on"+b,c)}};var H=/^true$/.test("")?!0:!1;var I={},J=function(a){var b=a.toString();a.name&&-1==b.indexOf(a.name)&&(b+=": "+a.name);a.message&&-1==b.indexOf(a.message)&&(b+=": "+a.message);if(a.stack){a=a.stack;var c=b;try{-1==a.indexOf(c)&&(a=c+"\n"+a);for(var d;a!=d;)d=a,a=a.replace(/((https?:\/..*\/)[^\/:]*:\d+(?:.|\n)*)\2/,"$1");b=a.replace(/\n */g,"\n")}catch(e){b=c}}return b},M=function(a,b,c,d){var e=K,f,u=!0;try{f=b()}catch(h){try{var N=J(h);b="";h.fileName&&(b=h.fileName);var E=-1;h.lineNumber&&(E=h.lineNumber);var v;t:{try{v=c?c():"";break t}catch(S){}v=""}u=e(a,N,b,E,v)}catch(k){try{var O=J(k);a="";k.fileName&&(a=k.fileName);c=-1;k.lineNumber&&(c=k.lineNumber);K("pAR",O,a,c,void 0,void 0)}catch(F){L({context:"mRE",msg:F.toString()+"\n"+(F.stack||"")},void 0)}}if(!u)throw h;}finally{if(d)try{d()}catch(T){}}return f},K=function(a,b,c,d,e,f){a={context:a,msg:b.substring(0,512),eid:e&&e.substring(0,40),file:c,line:d.toString(),url:r.URL.substring(0,512),ref:r.referrer.substring(0,512)};P(a);L(a,f);return!0},L=function(a,b){try{if(Math.random()<(b||.01)){var c="/pagead/gen_204?id=jserror"+Q(a),d="http"+("https:"==s.location.protocol?"s":"")+"://pagead2.googlesyndication.com"+c,d=d.substring(0,2E3);w(s,d)}}catch(e){}},P=function(a){var b=a||{};t(I,function(a,d){b[d]=s[a]})},R=function(a,b,c,d,e){return function(){var f=arguments;return M(a,function(){return b.apply(c,f)},d,e)}},Q=function(a){var b="";t(a,function(a,d){if(0===a||a)b+="&"+d+"="+("function"==typeof encodeURIComponent?encodeURIComponent(a):escape(a))});return b};A=function(a,b,c,d){return R(a,b,void 0,c,d)};z=H;l("vu",R("vu",function(a,b){var c=a.replace("&amp;","&"),d=/(google|doubleclick).*\/pagead\/adview/.test(c),e=C.m();if(d){d="&vis="+x(e.g);b&&(d+="&ve=1");var f=c.indexOf("&adurl"),c=-1==f?c+d:c.substring(0,f)+d+c.substring(f)}e.j?e.l(c,b):e.h.push([c,b])}));l("vv",R("vv",function(){z&&B(C.m())}));})();</script><script>vu("http://pubads.g.doubleclick.net/pagead/adview?ai\x3dBDl4fJkiBVfarOdCaigagmoGAC_bJ3NgGAAAAEAEgADgAWIbHw-XQAWDVjdOCvAiCARdjYS1wdWItODAxODk0MzI3NTc5OTg3MbIBE3d3dy5lbGVjb25vbWlzdGEuZXO6AQlnZnBfaW1hZ2XIAQnaAbgBaHR0cDovL3d3dy5lbGVjb25vbWlzdGEuZXMvaW50ZXJzdGl0aWFsL3ZvbHZlci8yNjk2MTc0NjIvY29uc3RydWNjaW9uLWlubW9iaWxpYXJpby9ub3RpY2lhcy82Nzg0NzIzLzA2LzE1L0xhLUNhaXhhLWNyZWUtcXVlLWVsLXB1bHNvLWRlbC1zZWN0b3ItaW5tb2JpbGlhcmlvLWNvbWllbnphLWEtcmVjdXBlcmFyc2UuaHRtbKkCqr7Amm3osj7AAgLgAgDqAjQ0Mjc1L2VsZWNvbm9taXN0YS5lcy9lY29ub21pc3RhX25vdGljaWFzX2NvbnN0cnVjaW9u-AL_0R6AAwGQA8gGmAPIBqgDAeAEAZIFCwgHEAEYASDO5N4OkAYBoAYW2AcA\x26sigh\x3dESb99kKk8w4\x26cid\x3d5GhD_bMo4ZGYnIuUGYsXTclv")</script><script type="text/javascript">
<!--//<![CDATA[
   document.MAX_ct0 ='INSERT_CLICK_URL';
   var m3_u = (location.protocol=='https:'?'https://cas.criteo.com/delivery/ajs.php?':'http://cas.criteo.com/delivery/ajs.php?');
   var m3_r = Math.floor(Math.random()*99999999999);
   document.write ("<scr"+"ipt type='text/javascript' src='"+m3_u);
   document.write ("zoneid=179777");
   document.write ('&amp;cb=' + m3_r);
   if (document.MAX_used != ',') document.write ("&amp;exclude=" + document.MAX_used);
   document.write (document.charset ? '&amp;charset='+document.charset : (document.characterSet ? '&amp;charset='+document.characterSet : ''));
   document.write ("&amp;loc=" + escape(window.location));
   if (document.referrer) document.write ("&amp;referer=" + escape(document.referrer));
   if (document.context) document.write ("&context=" + escape(document.context));
   if ((typeof(document.MAX_ct0) != 'undefined') && (document.MAX_ct0.substring(0,4) == 'http')) {
       document.write ("&amp;ct0=" + escape(document.MAX_ct0));
   }
   if (document.mmm_fo) document.write ("&amp;mmm_fo=1");
   document.write ("'></scr"+"ipt>");
//]]>--></script><script type="text/javascript" src="./eco_files/ajs.php"></script><iframe id="cto_iframe_37679bcec3" frameborder="0" allowtransparency="true" hspace="0" marginwidth="0" marginheight="0" scrolling="no" vspace="0" width="300px" height="250px"></iframe>

</div><noscript>&lt;a href="http://ad.doubleclick.net/jump/eleconomista.es/economista_noticias_construcion;sect=6784723;tile=5;kw=269617462;sz=300x250;ord=123456789?" target="_blank" rel="nofollow"&gt;&lt;img src="http://ad.doubleclick.net/ad/eleconomista.es/economista_noticias_construcion;sect=6784723;tile=5;kw=269617462;sz=300x250;ord=123456789?" width="300" height="250" border="0"&gt;&lt;/a&gt;</noscript>
</div><hr>
<div class="col-loteria"></div>
		<div class="e-eu"><div></div>

		</div><div></div>
 
				<div style="overflow: hidden;padding:0 10px;">
				<img src="./eco_files/eE3.png" alt="" style="float: left; padding-right: 13px;border-right: 2px solid #eee;margin-right: 14px;margin-top: 4px;">

				<!--twitter-->
				<iframe id="twitter-widget-0" scrolling="no" frameborder="0" allowtransparency="true" src="./eco_files/follow_button.1404859412.html" class="twitter-follow-button twitter-follow-button" title="Twitter Follow Button" data-twttr-rendered="true" style="width: 171px; height: 20px;"></iframe>
			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?"http":"https";if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document, "script", "twitter-wjs");</script>
				<!--facebook-->
				<iframe src="./eco_files/like.html" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:130px; height:21px;margin:5px 0;" allowtransparency="true"></iframe>
				<!--google-->
				<!-- Inserta esta etiqueta donde quieras que aparezca widget. -->
				<div id="___follow_0" style="text-indent: 0px; margin: 0px; padding: 0px; border-style: none; float: none; line-height: normal; font-size: 1px; vertical-align: baseline; display: inline-block; width: 91px; height: 20px; background: transparent;"><iframe frameborder="0" hspace="0" marginheight="0" marginwidth="0" scrolling="no" style="position: static; top: 0px; width: 91px; margin: 0px; border-style: none; left: 0px; visibility: visible; height: 20px;" tabindex="0" vspace="0" width="100%" id="I0_1405593638317" name="I0_1405593638317" src="./eco_files/follow.html" data-gapiattached="true"></iframe></div>

				<!-- Inserta esta etiqueta después de la última etiqueta de widget. -->
				<script type="text/javascript">
				  window.___gcfg = {lang: "es"};

				  (function() {
				    var po = document.createElement("script"); po.type = "text/javascript"; po.async = true;
				    po.src = "https://apis.google.com/js/platform.js";
				    var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(po, s);
				  })();
				</script>
			</div>
			<hr>
			<div mod="2779">
<div class="uh uh-new">
<script type="text/javascript">
			//<![CDATA[
			var pestanas = new Array();
				
				
				pestanas[0] = {site:1, tag:'tmas1', id:'d-1'};
				pestanas[1] = {site:4, tag:'tmas2', id:'d-2'};
				pestanas[2] = {site:4, tag:'tmas3', id:'d-3'};
				pestanas[3] = {site:4, tag:'tmas4', id:'d-4'};
				
				
				
					vMasNoticiasP = new masNoticias (pestanas[0].id, pestanas[0].tag);
				
			//]]>
			</script><h5>El flash: toda la última hora</h5>
<ul class="pest pest2"><li id="tmas1" class="select"><a href="http://www.eleconomista.es/interstitial/volver/269617462/construccion-inmobiliario/noticias/6784723/06/15/La-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html#" onclick="javascript:vMasNoticiasP.mostrar(&#39;tmas1&#39;,&#39;d-1&#39;);return false;" target="_top">Actualidad</a></li></ul>
<script type="text/javascript">
				//<![CDATA[
					$(pestanas[0].tag).addClassName('select');
					
				//]]>
				</script><div class="uh-cont  uh-cont2">
<div id="d-1" class="uh-not2">
<ul>
<div class="uh-not2">
<h2><a href="http://ecodiario.eleconomista.es/futbol/noticias/6799617/06/15/La-Premier-League-amenaza-con-llevarse-al-otro-Odegaard-del-Real-Madrid.html?utm_source=crosslink&utm_medium=flash" target="_top">La Premier League amenaza con llevarse al otro 'Odegaard' del Real Madrid</a></h2>
<div class="uh-hora2">
<span class="hora5">12:10</span>
										Ecodiario.es
									
								 - Fútbol<div class="sep"></div>
<img src="./eco_files/250x_Camiseta-Odegaard-2015-reuters.jpg"><p>El pasado mes de enero el Real Madrid dio un golpe en el mercado internacional del fichaje de 'perlas' con la llegada de Martin Odegaard a …</p>
</div>
</div>
<div class="uh-not2">
<h2><a href="http://ecodiario.eleconomista.es/politica/noticias/6799584/06/15/Wert-utiliza-el-Colegio-de-Espana-en-Paris-a-unos-dias-de-trabajar-en-la-capital-del-Sena.html?utm_source=crosslink&utm_medium=flash" target="_top">Wert utiliza el Colegio de España en París a unos días de trabajar en la …</a></h2>
<div class="uh-hora2">
<span class="hora5">11:59</span>
										Ecodiario.es
									
								 - Política<div class="sep"></div>
<img src="./eco_files/250x_gomendio-wert-efe.jpg"><p>El todavía ministro de Educación y Cultura, José Ignacio Wert, se ha alojado este último fin de semana en el Colegio de España en …</p>
</div>
</div>
<div class="uh-not2">
<h2><a href="http://ecodiario.eleconomista.es/espana/noticias/6799582/06/15/Piden-la-dimision-de-la-directora-de-la-carcel-de-tras-conocerse-los-privilegios.html?utm_source=crosslink&utm_medium=flash" target="_top">Piden la dimisión de la directora de la cárcel de Isabel Pantoja tras conocerse …</a></h2>
<div class="uh-hora2">
<span class="hora5">11:57</span>
										Ecodiario.es
									
								 - España<div class="sep"></div>
<img src="./eco_files/250x_pantoja-saluda-efe.jpg"><p>La Agrupación de los Cuerpos de la Administración de Instituciones Penitenciarias (Acaip) prepara un comunicado que piensan difundir esta …</p>
</div>
</div>
<div class="uh-not2">
<h2><a href="http://ecodiario.eleconomista.es/futbol/noticias/6799553/06/15/La-vulgaridad-de-Messi-con-Argentina-Aguero-y-Pastore-tiran-de-la-Albiceleste.html?utm_source=crosslink&utm_medium=flash" target="_top">La 'vulgaridad' de Messi con Argentina: Agüero y Pastore tiran de la …</a></h2>
<div class="uh-hora2">
<span class="hora5">11:47</span>
										Ecodiario.es
									
								 - Fútbol<div class="sep"></div>
<img src="./eco_files/250x_Messi-lamento-Argentina-2015-EFE.jpg"><p>Leo Messi se encuentra disputando la Copa América. El delantero del FC Barcelona quiere tomarse en serio el torneo, en primer lugar, para …</p>
</div>
</div>
<div class="uh-not2">
<h2><a href="http://ecodiario.eleconomista.es/cine/noticias/6799522/06/15/Disney-prepara-la-segunda-parte-de-Malefica.html?utm_source=crosslink&utm_medium=flash" target="_top">Disney prepara la segunda parte de 'Maléfica'</a></h2>
<div class="uh-hora2">
<span class="hora5">11:38</span>
										Ecodiario.es
									
								 - Cine<div class="sep"></div>
<img src="./eco_files/malefica-disney.jpg"><p>Disney ya está pensando en la secuela de 'Maléfica' y ya ha contratado a Linda Woolverton para escribir el guión de la cinta, según ha …</p>
</div>
</div>
<div class="uh-not2">
<h2><a href="http://ecodiario.eleconomista.es/inmigracion/noticias/6799519/06/15/Espana-ha-recibido-en-30-anos-menos-peticiones-de-asilo-que-Alemania-solo-el-ano-pasado.html?utm_source=crosslink&utm_medium=flash" target="_top">España ha recibido en 30 años menos peticiones de asilo que Alemania sólo en …</a></h2>
<div class="uh-hora2">
<span class="hora5">11:34</span>
										Ecodiario.es
									
								 - Inmigración<div class="sep"></div>
<img src="./eco_files/250x_demandante-asilo-efe.jpg"><p>España ha recibido en los últimos 30 años menos peticiones de asilo que Alemania sólo en 2014, conforme detalla el informe anual de CEAR …</p>
</div>
</div>
<div class="uh-not2">
<h2><a href="http://ecodiario.eleconomista.es/internacional/noticias/6799473/06/15/Nace-Engage-Cuba-la-gran-coalicion-de-Estados-Unidos-para-el-aperturismo-a-la-isla.html?utm_source=crosslink&utm_medium=flash" target="_top">Nace Engage Cuba, la gran coalición de Estados Unidos para el aperturismo a la …</a></h2>
<div class="uh-hora2">
<span class="hora5">11:25</span>
										Ecodiario.es
									
								 - Global<div class="sep"></div>
<img src="./eco_files/250x_engage-cuba-youtube.jpg"><p>La coalición bipartidista Engage Cuba, una organización a favor del fin del embargo de Estados Unidos hacia la isla, hizo este martes su …</p>
</div>
</div>
<div class="uh-not2">
<h2><a href="http://ecodiario.eleconomista.es/futbol/noticias/6799431/06/15/Bartra-cerca-de-convertirse-en-otro-caso-Thiago-para-salir-del-Bara-a-bajo-coste.html?utm_source=crosslink&utm_medium=flash" target="_top">Bartra, cerca de convertirse en otro caso Thiago para salir del Barça a bajo …</a></h2>
<div class="uh-hora2">
<span class="hora5">11:13</span>
										Ecodiario.es
									
								 - Fútbol<div class="sep"></div>
<img src="./eco_files/250x_Bartra-salto-Rayo-2015-reuters.jpg"><p>Marc Bartra amenaza con convertirse en otro caso Thiago Alcántara. El hispano brasileño dejó el FC Barcelona hace ya dos años después de …</p>
</div>
</div>
<div class="uh-not2">
<h2><a href="http://ecodiario.eleconomista.es/politica/noticias/6799357/06/15/Santiago-Cervera-absuelto-del-chantaje-al-expresidente-de-Caja-Navarra.html?utm_source=crosslink&utm_medium=flash" target="_top">El exdiputado del PP Cervera, absuelto del chantaje al expresidente de Caja …</a></h2>
<div class="uh-hora2">
<span class="hora5">10:42</span>
										Ecodiario.es
									
								 - Política<div class="sep"></div>
<img src="./eco_files/250x_santiago-cervera-efe.jpg"><p>La titular del Juzgado de lo Penal número 3 de Pamplona ha absuelto al exdiputado del PP Santiago Cervera del delito de amenazas que le …</p>
</div>
</div>
<div class="uh-not2">
<h2><a href="http://ecodiario.eleconomista.es/politica/noticias/6799195/06/15/Rajoy-acusa-a-Pedro-Sanchez-de-saltarse-la-voluntad-democratica-de-los-espanoles-dando-el-poder-a-grupos-extremistas.html?utm_source=crosslink&utm_medium=flash" target="_top">Rajoy acusa a Sánchez de dar el "poder a grupos extremistas que ya están dando …</a></h2>
<div class="uh-hora2">
<span class="hora5">09:38</span>
										Ecodiario.es
									
								 - Política<div class="sep"></div>
<img src="./eco_files/250x_rajoyacusasanchez.jpg"><p>El presidente del Gobierno, Mariano Rajoy, ha acusado este miércoles al secretario general del PSOE, Pedro Sánchez, de actuar "saltándose …</p>
</div>
</div>
</ul>
<div class="sep-mas1"></div>
<div class=""><a class="mas-noti" target="_top" href="http://ecodiario.eleconomista.es/flash">Más noticias</a></div>
</div>
<div id="d-2" class="uh-not2" style="display: none;"></div>
</div>
<script type="text/javascript">
				//<![CDATA[
					for (i=1; i<pestanas.length; i++) {
						//if (i!=0 ) {
							vMasNoticiasP.ocultar(pestanas[i].id);
						//}
					}
					
	
				//]]>
				</script>
</div>
<hr>
<div class="sep"></div>
</div>

					<table cellspacing="0" class="tablapeq tablemonitor" style="margin:0 0 10px 0">
						<tbody>
							<tr>
								<td>
									<a href="http://www.eleconomista.es/monitor"><img src="./eco_files/logo-grande-degradado(1).png"></a>
									<h3>
										<a href="http://www.eleconomista.es/monitor">La herramienta para el ahorrador en Bolsa</a>
									</h3>
									<p>
										<a href="http://www.eleconomista.es/monitor">¡Regístrese y pruébelo GRATIS!</a>
									</p>
								</td>
							</tr>
						</tbody>
					</table>
				<hr>

			
			<div class="caja c-alerta c-resumen">
				<h4>el<span>Resumen</span>Diario</h4>
					<form action="http://www.eleconomista.es/boletines/alta_ok.php" method="post">
						<label for="alerta-email">Suscríbete y recibe el resumen de las noticias más importantes del día en tu email.</label>
						<div style="margin: 5px 0 0 5px;">
						<input style="width: 199px;" "="" type="text" value="" name="email" class="email" id="" placeholder="Dirección de email">
						<input type="hidden" value="1" id="Optin_news1" name="Optin_news1">
						<input style="background: #f60;" type="submit" value="Enviar" name="alta" id="alta" class="c-a-boton">
						</div>
					</form>
			</div>
			<hr>
			<div class="sep"></div><div class="sep"></div>
<div mod="2479" class="masleidas-orus masleidas-orus2"><div class="popular">
<script type="text/javascript">
					//<![CDATA[
						var sites = new Array();
						
						//------------------------------Realizacion de For each -----------------------------
						
							sites[0] = {site:1, tag:'tmaseconomista' , id:'d-economista'};
						
							sites[1] = {site:4, tag:'tmasecodiario' , id:'d-ecodiario'};
						
							sites[2] = {site:18, tag:'tmasecoteuve' , id:'d-ecoteuve'};
						
							sites[3] = {site:8, tag:'tmasmotor' , id:'d-motor'};
						
							sites[4] = {site:16, tag:'tmasevasion' , id:'d-evasion'};
						
					
						idSite = $(document.body).readAttribute('s');
						if(idSite==null){
							idSite=16;
						}
						
		//				idSite = 16;
						
						for (i=0; i<sites.length;i++) {
							if (idSite == sites[i].site ) 
								break;
						}
						vMasNoticiasS = new masNoticias (sites[i].id, sites[i].tag);
						
					//]]>
					</script><div id="pestPopular"><ul class="pest">
<li id="tmaseconomista" style="font-size: 11px;" class="select"><a href="http://www.eleconomista.es/interstitial/volver/269617462/construccion-inmobiliario/noticias/6784723/06/15/La-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html#" onclick="javascript:vMasNoticiasS.mostrar(&#39;tmaseconomista&#39;,&#39;d-economista&#39;);return false;" target="&#39;_top&#39;">el<span style="color: #000;">Eco</span>nomista</a></li>
<li id="tmasecodiario" style="font-size: 11px;"><a href="http://www.eleconomista.es/interstitial/volver/269617462/construccion-inmobiliario/noticias/6784723/06/15/La-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html#" onclick="javascript:vMasNoticiasS.mostrar(&#39;tmasecodiario&#39;,&#39;d-ecodiario&#39;);return false;" target="&#39;_top&#39;"><span style="color: #000;">Eco</span>Diario</a></li>
<li id="tmasecoteuve" style="font-size: 11px;"><a href="http://www.eleconomista.es/interstitial/volver/269617462/construccion-inmobiliario/noticias/6784723/06/15/La-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html#" onclick="javascript:vMasNoticiasS.mostrar(&#39;tmasecoteuve&#39;,&#39;d-ecoteuve&#39;);return false;" target="&#39;_top&#39;"><span style="color: #000;">Eco</span>teuve</a></li>
<li id="tmasmotor" style="font-size: 11px;"><a href="http://www.eleconomista.es/interstitial/volver/269617462/construccion-inmobiliario/noticias/6784723/06/15/La-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html#" onclick="javascript:vMasNoticiasS.mostrar(&#39;tmasmotor&#39;,&#39;d-motor&#39;);return false;" target="&#39;_top&#39;">Motor</a></li>
<li id="tmasevasion" style="font-size: 11px;"><a href="http://www.eleconomista.es/interstitial/volver/269617462/construccion-inmobiliario/noticias/6784723/06/15/La-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html#" onclick="javascript:vMasNoticiasS.mostrar(&#39;tmasevasion&#39;,&#39;d-evasion&#39;);return false;" target="&#39;_top&#39;">Evasión</a></li>
</ul></div>
<script type="text/javascript">
			//<![CDATA[
				$(sites[i].tag).addClassName('select');
				
			//]]>
			</script><div id="d-economista" class="masleidas">
<p class="mas-leidas-cint">Noticias más leidas</p>
<ul>
<li><a href="http://www.eleconomista.es/economia/noticias/6798997/06/15/Los-empresarios-piden-reducir-los-ayuntamientos-a-3000.html" target="_top">Los empresarios piden reducir un 63% el número de ayuntamientos, a …</a></li>
<li><a href="http://www.eleconomista.es/mercados-cotizaciones/noticias/6799135/06/15/El-Ibex-35-arranca-con-subidas-del-02-sobre-los-10890-puntos.html" target="_top">Contraataque alcista: el Ibex 35 sube el 0,5% y supera los 10.900 …</a></li>
<li><a href="http://www.eleconomista.es/espana/cronicas/5723/06/15/EN-DIRECTO-Siga-aqu-la-sesin-de-control-al-Gobierno-en-el-Congreso.html" target="_top">EN DIRECTO | Siga aquí la sesión de control al Gobierno en el …</a></li>
<li><a href="http://www.eleconomista.es/mercados-cotizaciones/noticias/6798439/06/15/Las-letras-del-Tesoro-compiten-con-los-depositos-por-primera-vez-desde-2012.html" target="_top">Las letras del Tesoro compiten con los depósitos por primera vez desde …</a></li>
<li><a href="http://www.eleconomista.es/construccion-inmobiliario/noticias/6798944/06/15/Sacyr-OHL-y-FCC-estudian-contratos-en-autopistas-rusas-por-4000-millones.html" target="_top">Sacyr, OHL y FCC estudian contratos en autopistas rusas por 4.000 …</a></li>
</ul>
<div class="sep-mas1"></div>
<div class="sigue" style="margin:5px 0 8px"><a target="_top" href="http://www.eleconomista.es/">Más noticias</a></div>
</div>
<div id="d-ecodiario" class="masleidas" style="display: none;">
<p class="mas-leidas-cint">Noticias más leidas</p>
<ul>
<li><a href="http://ecodiario.eleconomista.es/politica/noticias/6799106/06/15/La-mano-derecha-de-Carmena-en-el-Ayuntamiento-No-podemos-parar-los-desahucios.html" target="_top">La 'mano derecha' de Carmena en el Ayuntamiento: "No podemos parar los …</a></li>
<li><a href="http://ecodiario.eleconomista.es/sociedad/noticias/6799153/06/15/Denuncian-el-escandaloso-trato-de-favor-que-recibe-Isabel-Pantoja-en-la-carcel.html" target="_top">Denuncian el "escandaloso trato de favor" que recibe Isabel Pantoja en …</a></li>
<li><a href="http://ecodiario.eleconomista.es/internacional/noticias/6799348/06/15/Alerta-en-Europa-por-la-llegada-del-MERS-que-sintomas-tiene-y-como-se-transmite.html" target="_top">Alerta en Europa por la llegada del MERS: ¿qué síntomas tiene y …</a></li>
<li><a href="http://ecodiario.eleconomista.es/futbol/noticias/6799126/06/15/El-Real-Madrid-necesita-vender-un-peso-pesado-si-quiere-fichar-un-galactico.html" target="_top">El Real Madrid necesita vender un peso pesado si quiere fichar un …</a></li>
<li><a href="http://ecodiario.eleconomista.es/politica/noticias/6798246/06/15/Un-consejo-a-Begona-Villacis-Sonrie-menos.html" target="_top">El último consejo que recibe Begoña Villacís, de Ciudadanos: …</a></li>
</ul>
<div class="sep-mas1"></div>
<div class="sigue" style="margin:5px 0 8px"><a target="_top" href="http://ecodiario.eleconomista.es/index.html">Más noticias</a></div>
</div>
<div id="d-ecoteuve" class="masleidas" style="display: none;">
<p class="mas-leidas-cint">Noticias más leidas</p>
<ul>
<li><a href="http://ecoteuve.eleconomista.es/programas/noticias/6799111/06/15/Zapata-rechaza-una-broma-de-Wyoming-porque-no-quiero-frivolizar.html" target="_top">Zapata rechaza una broma de Wyoming "porque no quiero frivolizar"</a></li>
<li><a href="http://ecoteuve.eleconomista.es/programas/noticias/6799123/06/15/Mila-cuelga-por-segunda-vez-el-delantal-y-es-expulsada-de-las-cocinas-de-Masterchef.html" target="_top">Mila, expulsada de 'Masterchef' tras ser repescada</a></li>
<li><a href="http://ecoteuve.eleconomista.es/programas/noticias/6799552/06/15/TVE-suspende-el-programa-de-Los-Morancos-una-semana-despues-del-estreno.html" target="_top">TVE suspende el programa de Los Morancos una semana después del …</a></li>
<li><a href="http://ecoteuve.eleconomista.es/audiencias/noticias/6799131/06/15/El-Principe-231-se-impone-tras-su-paron-con-la-llegada-de-Jesus-Castro.html" target="_top">'El Príncipe' (23,1%) se impone tras su parón con la llegada de …</a></li>
<li><a href="http://ecoteuve.eleconomista.es/ecoteuve/television/noticias/6799234/06/15/Rajoy-asegura-que-el-apoyo-del-presidente-de-RTVE-al-PP-no-afecta-su-independencia.html" target="_top">Rajoy asegura que el apoyo del presidente de RTVE al PP "no afecta a su …</a></li>
</ul>
<div class="sep-mas1"></div>
<div class="sigue" style="margin:5px 0 8px"><a target="_top" href="http://ecoteuve.eleconomista.es/index.html">Más noticias</a></div>
</div>
<div id="d-motor" class="masleidas" style="display: none;">
<p class="mas-leidas-cint">Noticias más leidas</p>
<ul>
<li><a href="http://www.eleconomista.es/ecomotor/novedades/noticias/6799091/06/15/Bultaco-Brinco-bici-o-moto.html" target="_top">Bultaco Brinco: ¿bici o moto?</a></li>
<li><a href="http://www.eleconomista.es/ecomotor/motor/noticias/6799203/06/15/Ducati-Leggero-BC-para-amantes-del-diseno-retro.html" target="_top">Ducati Leggero BC: para amantes del diseño retro</a></li>
<li><a href="http://www.eleconomista.es/ecomotor/motor/noticias/6799226/06/15/Seat-y-Samsung-pareja-al-volante-leer-whatsapps-conduciendo-ya-no-es-un-problema.html" target="_top">Seat y Samsung, pareja al volante: leer whatsapps conduciendo ya no es …</a></li>
<li><a href="http://www.eleconomista.es/ecomotor/motor/noticias/6799136/06/15/Llegan-a-Europa-los-proveedores-asiaticos-Renault-y-PSA-recurren-a-fabricantes-indios-y-chinos.html" target="_top">Llegan a Europa los proveedores asiáticos: Renault y PSA recurren a …</a></li>
<li><a href="http://www.eleconomista.es/ecomotor/motor/noticias/6799165/06/15/-Japon-inspeccionara-directamente-a-empresas-de-recambios-tras-el-caso-Takata.html" target="_top"> Japón inspeccionará directamente a empresas de recambios tras el …</a></li>
</ul>
<div class="sep-mas1"></div>
<div class="sigue" style="margin:5px 0 8px"><a target="_top" href="http://www.eleconomista.es/ecomotor/index.html">Más noticias</a></div>
</div>
<div id="d-evasion" class="masleidas" style="display: none;">
<p class="mas-leidas-cint">Noticias más leidas</p>
<ul>
<li><a href="http://www.eleconomista.es/evasion/tendencias/noticias/6799139/06/15/Capitol-Grand-South-Yarra-primer-complejo-residencial-de-6-estrellas-de-Melbourne.html" target="_top">Melbourne ya disfruta de su primer complejo residencial de seis …</a></li>
<li><a href="http://www.eleconomista.es/evasion/moda-y-complementos/noticias/6796468/06/15/Las-listas-de-espera-de-Birkin-la-mejor-estrategia-de-marketing-o-el-mito-mas-rentable.html" target="_top">Las listas de espera de Birkin, ¿la mejor estrategia de marketing o el …</a></li>
<li><a href="http://www.eleconomista.es/evasion/gente-y-estilo/noticias/6799255/06/15/Numerosos-rostros-conocidos-se-dan-cita-junto-a-Dani-Rovira-Clara-Lago-y-Maria-Valverde-en-el-estreno-de-Ahora-o-Nunca.html" target="_top">Dani Rovira, Clara Lago y María Valverde estrenan Ahora o Nunca</a></li>
<li><a href="http://www.eleconomista.es/evasion/gente-y-estilo/noticias/6799400/06/15/Jose-Ortega-Cano-libre.html" target="_top">José Ortega Cano libre</a></li>
<li><a href="http://www.eleconomista.es/evasion/gente-y-estilo/noticias/6799332/06/15/La-bella-Cara-Delevingne-presenta-pelicula-en-Madrid.html" target="_top">La bella Cara Delevingne presenta película en Madrid</a></li>
</ul>
<div class="sep-mas1"></div>
<div class="sigue" style="margin:5px 0 8px"><a target="_top" href="http://www.eleconomista.es/evasion/index.html">Más noticias</a></div>
</div>
<script type="text/javascript">
			//<![CDATA[
				for (i=0; i<sites.length; i++) {
					if (sites[i].site!=idSite ) {
//						console.log (sites[i].id);
						vMasNoticiasS.ocultar(sites[i].id);
					}
				}

			//]]>
			</script>
</div></div><hr>
<div mod="4586">
<div class="masleidas masleidas-ame">
<div class="ame-i"><a href="http://www.eleconomistaamerica.com/"><img width="186" src="./eco_files/logo7.png" alt=""></a></div>
<ul class="pest-ame">
<li class="sel"><a href="http://www.eleconomistaamerica.com/todas-las-noticias/index.html?utm_source=crosslink&utm_medium=ee_ultimas">Más leídas</a></li>
<li><a href="http://www.eleconomistaamerica.com/indices-latinoamericanos/index.html?utm_source=crosslink&utm_medium=ee_ultimas">Bolsas Latam</a></li>
</ul>
<ul>
<li><div class="noticia-ult"><h2><a href="http://www.economiahoy.mx/sociedad-eAm-mexico/noticias/6798706/06/15/Cinco-trucos-para-dormir-cuando-hace-demasiado-calor.html?utm_source=crosslink&utm_medium=ee_masleidas">Cinco trucos para dormir cuando hace demasiado calor</a></h2></div></li>
<li><div class="noticia-ult"><h2><a href="http://www.economiahoy.mx/empresas-eAm-mexico/noticias/6798803/06/15/Pemex-y-su-sindicato-inicia-revision-de-Contrato-Colectivo-de-Trabajo.html?utm_source=crosslink&utm_medium=ee_masleidas">Pemex y su sindicato inician revisión de Contrato Colectivo de …</a></h2></div></li>
<li><div class="noticia-ult"><h2><a href="http://www.economiahoy.mx/deportes-eAm-mx/noticias/6798714/06/15/La-presentadora-Yuvi-Pallares-se-desnuda-en-television-tras-el-triunfo-de-Venezuela-a-Colombia.html?utm_source=crosslink&utm_medium=ee_masleidas">La presentadora Yuvi Pallares se desnuda en televisión tras el triunfo …</a></h2></div></li>
<li><div class="noticia-ult"><h2><a href="http://www.economiahoy.mx/empresas-eAm-mexico/noticias/6798722/06/15/Ordenan-a-Banobras-mostrar-contrato-de-4500-millones-de-pesos-con-OHL-Mexico-.html?utm_source=crosslink&utm_medium=ee_masleidas">Ordenan a Banobras mostrar contrato de 4,500 millones de pesos con OHL …</a></h2></div></li>
<li><div class="noticia-ult"><h2><a href="http://www.economiahoy.mx/economia-eAm-mexico/noticias/6799606/06/15/El-Gobierno-de-Brasil-podria-conseguir-cerca-de-1000-millones-de-dolares-con-un-acuerdo-fiscal.html?utm_source=crosslink&utm_medium=ee_masleidas">El Gobierno de Brasil podría conseguir cerca de 1.000 millones de …</a></h2></div></li>
</ul>
</div>
<hr>
<script type="text/javascript">
			s=$(document.body).readAttribute('s');
			if(s != null){$("s_"+s).addClassName("active");}
		</script>
</div>
<div mod="1633" class="b300">
<script language="javascript">//<![CDATA[
			sz = "300x251" ;
			st = "eleconomista.es" ;
			std = "http://ad.es.doubleclick.net/" ;
			kw = "269617462";
	//		randnum = old_randnum;
			 sect ="6784723";
			document.write( '<SCR'+'IPT language="JavaScript" SRC="'+std+'adj/'+st+'/economista_noticias_construcion;sect='+sect+';tile=6;kw='+kw+';sz='+sz+';ord='+randnum+'?"></SCR'+'IPT>' ) ;
		//]]></script><script language="JavaScript" src="./eco_files/economista_noticias_construcion(4)"></script><div class="GoogleActiveViewClass" id="DfpVisibilityIdentifier_916925529018044"><div id="google_ads_div_950418"><iframe id="google_ads_iframe_950418" name="google_ads_iframe_950418" src="./eco_files/La-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html" width="300" height="250" scrolling="no" marginwidth="0" marginheight="0" frameborder="0" style="border: 0px;"></iframe></div></div><noscript>&lt;a href="http://ad.doubleclick.net/N4275/jump/eleconomista.es/economista_noticias_construcion;sect=6784723;tile=6;kw=269617462;sz=300x251;ord=123456789?" target="_blank" rel="nofollow"&gt;&lt;img src="http://ad.doubleclick.net/N4275/ad/eleconomista.es/economista_noticias_construcion;sect=6784723;tile=6;kw=269617462;sz=300x251;ord=123456789?" width="300" height="250" border="0"&gt;&lt;/a&gt;</noscript>
</div><hr>
<div class="primariesgo_col"><div id="contenedor_galeria_3100"><div class="foto-rot" id="caja_3100_1"><div mod="3100"><table class="tablapeq tabla-prima">
<caption>Prima de Riesgo</caption>
<tbody><tr>
<th>País</th>
<th>Precio</th>
<th>Puntos</th>
<th>%</th>
</tr>
<tr>
<td class="pri-esp"><a href="http://www.eleconomista.es/prima-riesgo/espana" title="Prima de riesgo de España">ESP</a></td>
<td class="pri-dat">1,56</td>
<td class="cot1">-0,15</td>
<td class="cot1">-6,66%</td>
</tr>
<tr>
<td class="pri-fr"><a href="http://www.eleconomista.es/prima-riesgo/francia" title="Prima de riesgo de Francia">FRA</a></td>
<td class="pri-dat">0,47</td>
<td class="cot-1">+0,02</td>
<td class="cot-1">+1,59%</td>
</tr>
<tr>
<td class="pri-it"><a href="http://www.eleconomista.es/prima-riesgo/italia" title="Prima de riesgo de Italia">ITA</a></td>
<td class="pri-dat">1,76</td>
<td class="cot1">0,01</td>
<td class="cot1">-0,29%</td>
</tr>
<tr>
<td class="pri-gr"><a href="http://www.eleconomista.es/prima-riesgo/grecia" title="Prima de riesgo de Grecia">GRE</a></td>
<td class="pri-dat">1.214,63</td>
<td class="cot-1">+65,56</td>
<td class="cot-1">+5,71%</td>
</tr>
<tr>
<td class="pri-pt"><a href="http://www.eleconomista.es/prima-riesgo/portugal" title="Prima de riesgo de Portugal">POR</a></td>
<td class="pri-dat">241,54</td>
<td class="cot1">-1,31</td>
<td class="cot1">-0,54%</td>
</tr>
</tbody></table></div></div></div></div><hr>
<div class="m-pago m-ecot">
<script type="text/javascript" src="./eco_files/vticker.js"></script><h3><a href="http://www.eleconomista.es/ecotrader/"><img src="./eco_files/ecotrader-transparente.png" alt="Ecotrader"></a></h3>
<div class="sr-pago"><div id="outputV" class="m-ecot-not" style="position: relative; overflow: hidden">
<div class="m-ecot-not-int" style="position: absolute; width: 286px; top: 118px;" id="outputV1">
<h5><a href="http://www.eleconomista.es/ecotrader-premium-analisis-tecnico/noticias/6799554/06/15/Gas-Natural-vuelve-a-martillear-su-zona-de-soporte.html">Gas Natural vuelve a martillear su zona …</a></h5>
<p> </p>
</div>
<div class="m-ecot-not-int" style="position: absolute; width: 286px; visibility: visible; top: 5px;" id="outputV2">
<h5><a href="http://www.eleconomista.es/ecotrader-tradings">Tono mixto en las bolsas europeas</a></h5>
<p>Tono mixto en las bolsas europeas, destacando el selectivo español Ibex 35 que está en terreno positivo superando …</p>
</div>
<ul id="list_ul_text" style="display:none;"><li>
<h5><a href="http://www.eleconomista.es/ecotrader-premium-analisis-tecnico/noticias/6799554/06/15/Gas-Natural-vuelve-a-martillear-su-zona-de-soporte.html">Gas Natural vuelve a martillear su zona …</a></h5>
<p> </p>
</li><li>
<h5><a href="http://www.eleconomista.es/ecotrader-tradings">Tono mixto en las bolsas europeas</a></h5>
<p>Tono mixto en las bolsas europeas, destacando el selectivo español Ibex 35 que está en terreno positivo superando …</p>
</li><li>
<h5><a href="http://www.eleconomista.es/ecotrader-tradings">Retroceso en las bolsas europeas</a></h5>
<p>Moderadas pérdidas en las bolsas europeas como ajuste a las subidas de la sesión de ayer que por el momento no ponen …</p>
</li><li>
<h5><a href="http://www.eleconomista.es/ecotrader-premium-analisis-tecnico/noticias/6799163/06/15/BMW-intenta-cambiar-de-marcha.html">BMW intenta cambiar de marcha</a></h5>
<p> </p>
</li><li>
<h5><a href="http://www.eleconomista.es/ecotrader-premium-analisis-tecnico/noticias/6799128/06/15/El-Nikkei-sigue-consolidando-.html">El Nikkei sigue consolidando  </a></h5>
<p> </p>
</li><li>
<h5><a href="http://www.eleconomista.es/ecotrader-tradings">Preapertura mercado europeo</a></h5>
<p>Se espera una apertura alcista en las bolsas europeas como continuidad al contraataque alcista visto en la sesión de …</p>
</li><li>
<h5><a href="http://www.eleconomista.es/ecotrader-renta-variable/noticias/6799068/06/15/El-Dax-gana-peso-para-aprovechar-el-reversal.html">El Dax gana peso para aprovechar el …</a></h5>
<p>Si bien es cierto que las negociaciones entre Grecia y la zona euro se recrudecen y que en las últimas horas han …</p>
</li><li>
<h5><a href="http://www.eleconomista.es/ecotrader-premium-analisis-tecnico/noticias/6798558/06/15/Tambien-gira-desde-soportes.html">También gira desde soportes</a></h5>
<p> </p>
</li><li>
<h5><a href="http://www.eleconomista.es/ecotrader-premium-divisas/noticias/6798418/06/15/Sigue-reacio-a-continuar-subiendo.html">Sigue reacio a continuar subiendo</a></h5>
<p> </p>
</li><li>
<h5><a href="http://www.eleconomista.es/ecotrader-premium-renta-fija/noticias/6798416/06/15/Prosigue-el-rebote.html">Prosigue el rebote</a></h5>
<p> </p>
</li><li>
<h5><a href="http://www.eleconomista.es/ecotrader-premium-analisis-tecnico/noticias/6798412/06/15/Contraataque-alcista-en-zonas-de-soporte-clave.html">Contraataque alcista en zonas de soporte …</a></h5>
<p> </p>
</li><li>
<h5><a href="http://www.eleconomista.es/ecotrader-premium-analisis-tecnico/noticias/6798411/06/15/Reversal-en-zona-de-soporte-clave.html">Reversal en zona de soporte clave</a></h5>
<p> </p>
</li><li>
<h5><a href="http://www.eleconomista.es/ecotrader-premium-analisis-tecnico/noticias/6798408/06/15/Endesa-recuperaria-el-tono-alcista-si-supera-17-euros.html">Endesa recuperaría el tono alcista si …</a></h5>
<p> </p>
</li></ul>
<div class="m-ecot-flechas" align="right">
<a href="" onclick="ticker.previousCurrentNews(); return( false );"><i class="fa fa-angle-up" alt="Flecha Arriba"></i></a><a href="" onclick="ticker.nextCurrentNews(); return( false );"><i class="fa fa-angle-down" alt="Flecha Aabajo"></i></a>
</div>
</div></div>
</div><hr><script type="text/javascript">//<![CDATA[
		var ticker = new news_VTicker('outputV', 'list_ul_text', 3000);
	//]]></script>
<div mod="535">
<div class="popular">
<script> 
				vMasCotizaciones = new masNoticias('masinteres','tmasinteres');
			</script><div id="pestPopular"><ul class="pest">
<li id="tmasvistas"><a href="http://www.eleconomista.es/interstitial/volver/269617462/construccion-inmobiliario/noticias/6784723/06/15/La-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html#" onclick="javascript:vMasCotizaciones.mostrar(&#39;tmasvistas&#39;,&#39;masvistas&#39;);return false;">+ vistos</a></li>
<li id="tmasinteres" class="select"><a href="http://www.eleconomista.es/interstitial/volver/269617462/construccion-inmobiliario/noticias/6784723/06/15/La-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html#" onclick="javascript:vMasCotizaciones.mostrar(&#39;tmasinteres&#39;,&#39;masinteres&#39;);return false;">interesa - no interesa</a></li>
</ul></div>
<div id="masvistas" class="mastock" style="display: none;"><ul style="width:90%">
<li class="C7"><a href="http://www.eleconomista.es/empresa/ACS">ACS</a></li>
<li class="C7"><a href="http://www.eleconomista.es/empresa/AMPER">AMPER</a></li>
<li class="C6"><a href="http://www.eleconomista.es/empresa/APPLE-COMPUTER">APPLE</a></li>
<li class="C5"><a href="http://www.eleconomista.es/empresa/BANKIA">BANKIA</a></li>
<li class="C5"><a href="http://www.eleconomista.es/empresa/BBVA">BBVA</a></li>
<li class="C6"><a href="http://www.eleconomista.es/indice/BOVESPA-SAO-PAOLO">BOVESPA SAO PAULO</a></li>
<li class="C6"><a href="http://www.eleconomista.es/empresa/CAIXABANK">CAIXABANK</a></li>
<li class="C6"><a href="http://www.eleconomista.es/indice/DAX-30">DAX</a></li>
<li class="C5"><a href="http://www.eleconomista.es/indice/DOW-JONES">DOW JONES</a></li>
<li class="C5"><a href="http://www.eleconomista.es/indice/EUROSTOXX-50">EURO STOXX 50®</a></li>
<li class="C6"><a href="http://www.eleconomista.es/cruce/EURUSD">EURUSD</a></li>
<li class="C7"><a href="http://www.eleconomista.es/empresa/GAMESA">GAMESA</a></li>
<li class="C6"><a href="http://www.eleconomista.es/indice/IGBM">
									IGBM
								</a></li>
<li class="C1"><a href="http://www.eleconomista.es/indice/IBEX-35">IBEX 35</a></li>
<li class="C6"><a href="http://www.eleconomista.es/empresa/OHL">OHL</a></li>
<li class="C7"><a href="http://www.eleconomista.es/empresa/POPULAR">POPULAR</a></li>
<li class="C7"><a href="http://www.eleconomista.es/empresa/REPSOL">REPSOL</a></li>
<li class="C6"><a href="http://www.eleconomista.es/indice/S-P-500">S P 500</a></li>
<li class="C6"><a href="http://www.eleconomista.es/empresa/SANTANDER">SANTANDER</a></li>
<li class="C6"><a href="http://www.eleconomista.es/empresa/TELEFONICA">TELEFONICA</a></li>
</ul></div>
<div id="masinteres" class="mastock">
<h5 style="text-align:center">Subidas y caídas en el más vistos:</h5>
<ul>
<li>
<a href="http://www.eleconomista.es/indice/BOVESPA-SAO-PAOLO">BOVESPA SAO PAULO</a><small>(+20 puestos)<br>Sube del 34 al 14</small>
</li>
<li>
<a href="http://www.eleconomista.es/empresa/Itauunibanco">Itauunibanco</a><small>(+259 puestos)<br>Sube del 313 al 54</small>
</li>
<li>
<a href="http://www.eleconomista.es/empresa/Sid-Nacional">Sid Nacional</a><small>(+259 puestos)<br>Sube del 314 al 55</small>
</li>
<li>
<a href="http://www.eleconomista.es/empresa/METROGAS-B">METROGAS-B</a><small>(+254 puestos)<br>Sube del 313 al 59</small>
</li>
<li>
<a href="http://www.eleconomista.es/empresa/BCO-SANTANDER-RIO-B">BCO SANTANDER RIO-B</a><small>(+253 puestos)<br>Sube del 304 al 51</small>
</li>
</ul>
<ul>
<li>
<a href="http://www.eleconomista.es/empresa/GAMESA">GAMESA</a><small class="rojo">(-13 puestos)<br>Cae del 7 al 20</small>
</li>
<li>
<a href="http://www.eleconomista.es/empresa/SACYR">SACYR</a><small class="rojo">(-16 puestos)<br>Cae del 11 al 27</small>
</li>
<li>
<a href="http://www.eleconomista.es/empresa/ACS">ACS</a><small class="rojo">(-6 puestos)<br>Cae del 10 al 16</small>
</li>
<li>
<a href="http://www.eleconomista.es/empresa/IBERDROLA">IBERDROLA</a><small class="rojo">(-8 puestos)<br>Cae del 16 al 24</small>
</li>
<li>
<a href="http://www.eleconomista.es/empresa/ECORODOVIAS-NM">ECORODOVIAS NM</a><small class="rojo">(-244 puestos)<br>Cae del 75 al 319</small>
</li>
</ul>
</div>
<script>
				vMasCotizaciones.ocultar('masvistas');
			</script>
</div>
<hr>
</div>
<div mod="2687" id="contenedor_galeria_2687" style="overflow: hidden; height: 219px;">
<script type="text/javascript" language="javascript">
	//<![CDATA[
		//inicial = 1+Math.round(Math.random() * (3-1)) ;
		
		var inicial_2687 = 1+Math.round(Math.random() * 10)%1 ;
		galeria_2687 = new galeria(2687, 1,inicial_2687,0);
	//]]>
	</script><div class="gal1">
<h3>Evasión</h3>
<div id="caja_2687_1" style="">
<div id="imagen_2687_1"><a href="http://www.eleconomista.es/evasion/tendencias/noticias/6795904/06/15/Suites-espectaculares-el-lujo-y-la-exclusividad-estan-en-la-dimension-de-sus-espacios.html#.Kku8eywj2hUsyKX"><img src="./eco_files/Azure-16junio.jpg" alt="Las mejores suites del mundo - 640x450" width="640" height="450" id="2044389"></a></div>
<div class="gal1-tit">
<h2><a href="http://www.eleconomista.es/evasion/tendencias/noticias/6795904/06/15/Suites-espectaculares-el-lujo-y-la-exclusividad-estan-en-la-dimension-de-sus-espacios.html#.Kku8eywj2hUsyKX">Las mejores suites del mundo</a></h2>
<p>Las mejores suites del mundo</p>
</div>
</div>
<script type="text/javascript" language="javascript">
			//<![CDATA[
				var arraySize_2687 = new Array() ;
				for( i=1 ; i<=1 ; i++) {
					arraySize_2687[i-1] =$('imagen_2687_'+i).getHeight() ; 
					if ( i!=inicial_2687 )
						galeria_2687.eliminar(i) ;
				}
				galeria_2687.avanzar_horizontal( arraySize_2687 ); 
				
						
			//]]>
			</script>
</div>
</div><hr><div class="sep"></div>

				<div class="sep" style="margin-top: 10px;"></div>
				
			<hr>
			
		</div>
		<div class="sep"></div><div mod="1322" id="publicidad_120" style="position: fixed; width: 120px; height: 600px; z-index: 1000; top: 32px; margin-left: 1035px; visibility: visible;">
<script type="text/javascript">//<![CDATA[
		sz = "120x600" ;
		st = "eleconomista.es" ;
		std = "http://ad.es.doubleclick.net/" ;
		kw = "269617462";
		 sect ="6784723";
		document.write( '<SCR'+'IPT language="JavaScript" SRC="'+std+'adj/'+st+'/economista_noticias_construcion;sect='+sect+';tile=7;kw='+kw+';sz='+sz+';ord='+randnum+'?"></SCR'+'IPT>' ) ;
		
		/*
		s.eVar21="120x600";
		*/
		function comprobarTamano()
		{
			if ( (screen.width > 1024) && (document.body.clientWidth > 1144) ) {
				document.getElementById('publicidad_120').style.visibility = 'visible';
			}
			else {
				document.getElementById('publicidad_120').style.visibility = 'hidden';
			}
		}
		
		window.onresize = comprobarTamano ;
		comprobarTamano() ;
		//]]></script><script language="JavaScript" src="./eco_files/economista_noticias_construcion(5)"></script><div class="GoogleActiveViewClass" id="DfpVisibilityIdentifier_2344094947911799"><div id="google_ads_div_768461"><iframe id="google_ads_iframe_768461" name="google_ads_iframe_768461" src="./eco_files/La-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html" width="120" height="600" scrolling="no" marginwidth="0" marginheight="0" frameborder="0" style="border: 0px;"></iframe></div></div><noscript>&lt;a href="http://ad.doubleclick.net/jump/eleconomista.es/economista_noticias_construcion;sect=6784723;tile=7;kw=269617462;sz=120x600;ord=123456789?" target="_blank" rel="nofollow"&gt;&lt;img src="http://ad.doubleclick.net/ad/eleconomista.es/economista_noticias_construcion;sect=6784723;tile=7;kw=269617462;sz=120x600;ord=123456789?" width="120" height="600" border="0"&gt;&lt;/a&gt;</noscript>
</div>
<div mod="1648" id="publicidad_120_601" style="position: fixed; width: 120px; height: 600px; z-index: 1000; top: 32px; margin-left: -130px; visibility: visible;" class="publicidad_120">
<script type="text/javascript">//<![CDATA[
		sz = "120x601" ;
		st = "eleconomista.es" ;
		std = "http://ad.es.doubleclick.net/" ;
		kw = "269617462";
		 sect ="6784723";
		document.write( '<SCR'+'IPT language="JavaScript" SRC="'+std+'adj/'+st+'/economista_noticias_construcion;sect='+sect+';tile=8;kw='+kw+';sz='+sz+';ord='+randnum+'?"></SCR'+'IPT>' ) ;
		
		function comprobarTamano()
		{
			if ( (screen.width > 1024) && (document.body.clientWidth > 1144) ) {
				document.getElementById('publicidad_120_601').style.visibility = 'visible';
			}
			else {
				document.getElementById('publicidad_120_601').style.visibility = 'hidden';
			}
		}
		
		window.onresize = comprobarTamano ;
		comprobarTamano() ;
		//]]></script><script language="JavaScript" src="./eco_files/economista_noticias_construcion(6)"></script><div class="GoogleActiveViewClass" id="DfpVisibilityIdentifier_5428998272400349"><a target="_blank" href="http://googleads.g.doubleclick.net/aclk?sa=L&ai=BTExIKUiBVbzYFuXCigaP75LoCJbw8Z0EAAAAEAEgADgAWN6Nk7Z4YNWN04K8CIIBF2NhLXB1Yi04MDE4OTQzMjc1Nzk5ODcxsgETd3d3LmVsZWNvbm9taXN0YS5lc7oBCWdmcF9pbWFnZcgBAtoBuAFodHRwOi8vd3d3LmVsZWNvbm9taXN0YS5lcy9pbnRlcnN0aXRpYWwvdm9sdmVyLzI2OTYxNzQ2Mi9jb25zdHJ1Y2Npb24taW5tb2JpbGlhcmlvL25vdGljaWFzLzY3ODQ3MjMvMDYvMTUvTGEtQ2FpeGEtY3JlZS1xdWUtZWwtcHVsc28tZGVsLXNlY3Rvci1pbm1vYmlsaWFyaW8tY29taWVuemEtYS1yZWN1cGVyYXJzZS5odG1sqQKqvsCabeiyPsACAuACAOoCNDQyNzUvZWxlY29ub21pc3RhLmVzL2Vjb25vbWlzdGFfbm90aWNpYXNfY29uc3RydWNpb274Av_RHpADyAaYA8gGqAMB0ASQTuAEAZAGAaAGFNgHAQ&num=0&cid=5GhjKNtwx0OR3JlndIlQoSZc&sig=AOD64_2qnAliRcOi7ezSgPdLPkIjIa3MFg&client=ca-pub-8018943275799871&adurl=http://www.eleconomista.es"><img src="./eco_files/13143437097638293659" width="1" height="1" alt="" border="0"></a><script>(function(){var g=this,l=function(a,b){var c=a.split("."),d=g;c[0]in d||!d.execScript||d.execScript("var "+c[0]);for(var e;c.length&&(e=c.shift());)c.length||void 0===b?d=d[e]?d[e]:d[e]={}:d[e]=b},m=function(a,b,c){return a.call.apply(a.bind,arguments)},n=function(a,b,c){if(!a)throw Error();if(2<arguments.length){var d=Array.prototype.slice.call(arguments,2);return function(){var c=Array.prototype.slice.call(arguments);Array.prototype.unshift.apply(c,d);return a.apply(b,c)}}return function(){return a.apply(b,arguments)}},p=function(a,b,c){p=Function.prototype.bind&&-1!=Function.prototype.bind.toString().indexOf("native code")?m:n;return p.apply(null,arguments)},q=Date.now||function(){return+new Date};var r=document,s=window;var t=function(a,b){for(var c in a)Object.prototype.hasOwnProperty.call(a,c)&&b.call(null,a[c],c,a)},w=function(a,b){a.google_image_requests||(a.google_image_requests=[]);var c=a.document.createElement("img");c.src=b;a.google_image_requests.push(c)};var x=function(a){return{visible:1,hidden:2,prerender:3,preview:4}[a.webkitVisibilityState||a.mozVisibilityState||a.visibilityState||""]||0},y=function(a){var b;a.mozVisibilityState?b="mozvisibilitychange":a.webkitVisibilityState?b="webkitvisibilitychange":a.visibilityState&&(b="visibilitychange");return b};var C=function(){this.g=r;this.k=s;this.j=!1;this.i=null;this.h=[];this.o={};if(z)this.i=q();else if(3==x(this.g)){this.i=q();var a=p(this.q,this);A&&(a=A("di::vch",a));this.p=a;var b=this.g,c=y(this.g);b.addEventListener?b.addEventListener(c,a,!1):b.attachEvent&&b.attachEvent("on"+c,a)}else B(this)},A;C.m=function(){return C.n?C.n:C.n=new C};var D=/^([^:]+:\/\/[^/]+)/m,G=/^\d*,(.+)$/m,z=!1,B=function(a){if(!a.j){a.j=!0;for(var b=0;b<a.h.length;++b)a.l.apply(a,a.h[b]);a.h=[]}};C.prototype.s=function(a,b){var c=b.target.u();(c=G.exec(c))&&(this.o[a]=c[1])};C.prototype.l=function(a,b){this.k.rvdt=this.i?q()-this.i:0;var c;if(c=this.t)t:{try{var d=D.exec(this.k.location.href),e=D.exec(a);if(d&&e&&d[1]==e[1]&&b){var f=p(this.s,this,b);this.t(a,f);c=!0;break t}}catch(u){}c=!1}c||w(this.k,a)};C.prototype.q=function(){if(3!=x(this.g)){B(this);var a=this.g,b=y(this.g),c=this.p;a.removeEventListener?a.removeEventListener(b,c,!1):a.detachEvent&&a.detachEvent("on"+b,c)}};var H=/^true$/.test("")?!0:!1;var I={},J=function(a){var b=a.toString();a.name&&-1==b.indexOf(a.name)&&(b+=": "+a.name);a.message&&-1==b.indexOf(a.message)&&(b+=": "+a.message);if(a.stack){a=a.stack;var c=b;try{-1==a.indexOf(c)&&(a=c+"\n"+a);for(var d;a!=d;)d=a,a=a.replace(/((https?:\/..*\/)[^\/:]*:\d+(?:.|\n)*)\2/,"$1");b=a.replace(/\n */g,"\n")}catch(e){b=c}}return b},M=function(a,b,c,d){var e=K,f,u=!0;try{f=b()}catch(h){try{var N=J(h);b="";h.fileName&&(b=h.fileName);var E=-1;h.lineNumber&&(E=h.lineNumber);var v;t:{try{v=c?c():"";break t}catch(S){}v=""}u=e(a,N,b,E,v)}catch(k){try{var O=J(k);a="";k.fileName&&(a=k.fileName);c=-1;k.lineNumber&&(c=k.lineNumber);K("pAR",O,a,c,void 0,void 0)}catch(F){L({context:"mRE",msg:F.toString()+"\n"+(F.stack||"")},void 0)}}if(!u)throw h;}finally{if(d)try{d()}catch(T){}}return f},K=function(a,b,c,d,e,f){a={context:a,msg:b.substring(0,512),eid:e&&e.substring(0,40),file:c,line:d.toString(),url:r.URL.substring(0,512),ref:r.referrer.substring(0,512)};P(a);L(a,f);return!0},L=function(a,b){try{if(Math.random()<(b||.01)){var c="/pagead/gen_204?id=jserror"+Q(a),d="http"+("https:"==s.location.protocol?"s":"")+"://pagead2.googlesyndication.com"+c,d=d.substring(0,2E3);w(s,d)}}catch(e){}},P=function(a){var b=a||{};t(I,function(a,d){b[d]=s[a]})},R=function(a,b,c,d,e){return function(){var f=arguments;return M(a,function(){return b.apply(c,f)},d,e)}},Q=function(a){var b="";t(a,function(a,d){if(0===a||a)b+="&"+d+"="+("function"==typeof encodeURIComponent?encodeURIComponent(a):escape(a))});return b};A=function(a,b,c,d){return R(a,b,void 0,c,d)};z=H;l("vu",R("vu",function(a,b){var c=a.replace("&amp;","&"),d=/(google|doubleclick).*\/pagead\/adview/.test(c),e=C.m();if(d){d="&vis="+x(e.g);b&&(d+="&ve=1");var f=c.indexOf("&adurl"),c=-1==f?c+d:c.substring(0,f)+d+c.substring(f)}e.j?e.l(c,b):e.h.push([c,b])}));l("vv",R("vv",function(){z&&B(C.m())}));})();</script><script>vu("http://pubads.g.doubleclick.net/pagead/adview?ai\x3dBTExIKUiBVbzYFuXCigaP75LoCJbw8Z0EAAAAEAEgADgAWN6Nk7Z4YNWN04K8CIIBF2NhLXB1Yi04MDE4OTQzMjc1Nzk5ODcxsgETd3d3LmVsZWNvbm9taXN0YS5lc7oBCWdmcF9pbWFnZcgBAtoBuAFodHRwOi8vd3d3LmVsZWNvbm9taXN0YS5lcy9pbnRlcnN0aXRpYWwvdm9sdmVyLzI2OTYxNzQ2Mi9jb25zdHJ1Y2Npb24taW5tb2JpbGlhcmlvL25vdGljaWFzLzY3ODQ3MjMvMDYvMTUvTGEtQ2FpeGEtY3JlZS1xdWUtZWwtcHVsc28tZGVsLXNlY3Rvci1pbm1vYmlsaWFyaW8tY29taWVuemEtYS1yZWN1cGVyYXJzZS5odG1sqQKqvsCabeiyPsACAuACAOoCNDQyNzUvZWxlY29ub21pc3RhLmVzL2Vjb25vbWlzdGFfbm90aWNpYXNfY29uc3RydWNpb274Av_RHpADyAaYA8gGqAMB0ASQTuAEAZAGAaAGFNgHAQ\x26sigh\x3d-SJGi44-kDw\x26cid\x3d5GhjKNtwx0OR3JlndIlQoSZc")</script></div><noscript>&lt;a href="http://ad.doubleclick.net/jump/eleconomista.es/economista_noticias_construcion;sect=6784723;tile=8;kw=269617462;sz=120x601;ord=123456789?" target="_blank" rel="nofollow"&gt;&lt;img src="http://ad.doubleclick.net/ad/eleconomista.es/economista_noticias_construcion;sect=6784723;tile=8;kw=269617462;sz=120x601;ord=123456789?" width="120" height="600" border="0"&gt;&lt;/a&gt;</noscript>
</div>
<div mod="2885" class="b960n">
<script language="javascript">//<![CDATA[
			sz = "728x91" ;
			st = "eleconomista.es" ;
			std = "http://ad.es.doubleclick.net/" ;
			kw = "269617462";
			 sect ="6784723";
			document.write( '<SCR'+'IPT language="JavaScript" SRC="'+std+'adj/'+st+'/economista_noticias_construcion;sect='+sect+';tile=9;kw='+kw+';sz='+sz+';ord='+randnum+'?"></SCR'+'IPT>' ) ;
		//]]></script><script language="JavaScript" src="./eco_files/economista_noticias_construcion(7)"></script><div class="GoogleActiveViewClass" id="DfpVisibilityIdentifier_69011081941425"><div id="google_ads_div_415467"><iframe id="google_ads_iframe_415467" name="google_ads_iframe_415467" src="./eco_files/La-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html" width="960" height="90" scrolling="no" marginwidth="0" marginheight="0" frameborder="0" style="border: 0px;"></iframe></div></div><noscript>&lt;a href="http://ad.doubleclick.net/N4275/jump/eleconomista.es/economista_noticias_construcion;sect=6784723;tile=9;kw=269617462;sz=728x91;ord=123456789?" target="_blank" rel="nofollow"&gt;&lt;img src="http://ad.doubleclick.net/N4275/ad/eleconomista.es/economista_noticias_construcion;sect=6784723;tile=9;kw=269617462;sz=728x91;ord=123456789?" width="728" height="90" border="0"&gt;&lt;/a&gt;</noscript>
</div><hr>
</div>
	</div><div id="fb-root" class=" fb_reset"><div style="position: absolute; top: -10000px; height: 0px; width: 0px;"><div><iframe name="fb_xdm_frame_http" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" id="fb_xdm_frame_http" aria-hidden="true" title="Facebook Cross Domain Communication Frame" tabindex="-1" src="./eco_files/1ldYU13brY_.html" style="border: none;"></iframe><iframe name="fb_xdm_frame_https" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" id="fb_xdm_frame_https" aria-hidden="true" title="Facebook Cross Domain Communication Frame" tabindex="-1" src="./eco_files/1ldYU13brY_(1).html" style="border: none;"></iframe></div></div><div style="position: absolute; top: -10000px; height: 0px; width: 0px;"><div></div></div></div><iframe name="oauth2relay587345256" id="oauth2relay587345256" src="./eco_files/postmessageRelay.html" tabindex="-1" style="width: 1px; height: 1px; position: absolute; top: -100px;"></iframe><iframe width="0" height="0" src="./eco_files/iframe.html" style="position:absolute;bottom:0;display:none"></iframe><style type="text/css" id="tt-inread">.tt-wrapper {font-family:"HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, sans-serif;margin:0 0 20px 0;padding:0;overflow:hidden;width:auto;height:0;left:-999em;position:absolute}.tt-container {font-family:"HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, sans-serif;width:100%!important;height:auto!important;overflow:hidden;padding-top:56.25%;position:relative}.tt-viewport {width:5px;position:absolute;top:0;left:-999em}.tt-mention {height:15px;font-size:10px !important;color:#999;margin-bottom:3px;text-transform:uppercase;width:100%}.tt-player {width:100%;height:100%;opacity:1;position:absolute;top:0;left:0;right:0;bottom:0}.tt-whatthis {font:10px helvetica-light, sans-serif;color:#333;text-align:right;display:inline-block;float:right;margin-top:3px;position:relative}.tt-whatthis .on {text-decoration:none;color:#333;position:absolute;bottom:0;white-space:nowrap;background-color:white;right:-150px;transition:right .5s}.tt-whatthis:hover .on {right:0}.tt-whatthis .off {width:19px;background-position:-82px 0}.tt-whatthis .on span {color:#3c8abb}.tt-whatthis .on sup {font-size:6px}.tt-player object {margin:0 !important}</style><style type="text/css" id="tt-custom-68070f4"></style><div id="pie">
<div class="pie">
<div class="p-logo">
<img style="float:left" src="./eco_files/logo-h1(1).gif"><ul style="width: 330px !important; margin:0; float:right;">
<li class="descrip-pie">¿Es usuario de elEconomista.es?</li>
<li style="display:inline"><a href="http://www.eleconomista.es/registro/alta.php">Regístrese aquí</a></li> | <li style="display:inline"><a href="http://www.eleconomista.es/registro/alta.php">Dése de alta</a></li>
</ul>
</div>
<div class="sep" style="margin:0; border:none; border-bottom:1px solid #ccc"></div>
<ul style="margin-top: 20px;">
<li class="descrip-pie"><a class="img-l" href="http://www.eleconomista.es/"><img style="width: 147px;" src="./eco_files/logo-h1(1).gif" alt="eleconomista.es"></a></li>
<li><a href="http://ecoteuve.eleconomista.es/">Ecoteuve</a></li>
<li><a href="http://ecodiario.eleconomista.es/">Información general</a></li>
<li><a href="http://ecoaula.eleconomista.es/">Formación y empleo</a></li>
<li><a href="http://ecodiario.eleconomista.es/ecomotor/">Información motor</a></li>
<li><a href="http://www.eleconomista.es/evasion/index.html">Estilo y Tendencias</a></li>
<li><a href="http://www.eleconomista.es/especiales/turismo-viajes/">Turismo y viajes</a></li>
<li><a href="http://www.eleconomista.es/diccionario-de-economia/">Diccionario de economía</a></li>
</ul>
<ul style="width:175px; margin-top: 20px;">
<li class="descrip-pie"><a class="img-l" href="http://www.eleconomistaamerica.com/"><img src="./eco_files/eeam.png" alt="elEconomistaamerica.com"></a></li>
<li><a href="http://www.eleconomistaamerica.com.ar/">Argentina</a></li>
<li><a href="http://www.eleconomistaamerica.cl/">Chile</a></li>
<li><a href="http://www.eleconomistaamerica.co/">Colombia</a></li>
<li><a href="http://www.economiahoy.mx/">México</a></li>
<li><a href="http://www.eleconomistaamerica.pe/">Perú</a></li>
</ul>
<ul>
<li class="descrip-pie">Invierta con eE</li>
<li><a href="http://www.eleconomista.es/ecotrader/">Eco<span style="color:#f00;">trader.es </span></a></li>
<li><a href="http://www.eleconomista.es/monitor"><span style="color: #f60;">el</span>Monitor</a></li>
<li><a href="http://www.eleconomista.es/indice/eco10">Eco 10</a></li>
<li><a href="http://www.eleconomista.es/eco30">Eco 30</a></li>
<li><a href="http://www.eleconomista.es/mercados-cotizaciones/ecodividendo/index.html">Ecodividendo</a></li>
<li style="margin-top: 20px;">
</li><li class="descrip-pie"><a href="http://www.eleconomista.es/kiosco/" style="font-weight:bold;">Diario y revistas</a></li>
<li><a href="http://www.eleconomista.es/kiosco/">Kiosco</a></li>
<li><a href="http://www.eleconomista.es/kiosco/">Revistas digitales</a></li>
<li><a href="http://www.eleconomista.es/tarifas/">Suscripción al diario</a></li>
<li><a href="http://www.eleconomista.es/elsuperlunes/">elSuperLunes</a></li>
<li><a href="http://www.eleconomista.es/hemeroteca/">Ed. PDF + Hemeroteca</a></li>
<li><a href="http://www.eleconomista.es/ecotablet/">Ecotablet</a></li>
</ul>
<ul style="width:110px">
<li class="descrip-pie">Redes sociales</li>
<li>
<i class="fa fa-facebook"></i><a href="http://es-es.facebook.com/elEconomista.es">Facebook</a>
</li>
<li>
<i class="fa fa-twitter"></i><a href="http://twitter.com/eleconomistaes">Twitter</a>
</li>
<li>
<i class="fa fa-google-plus"></i><a href="https://plus.google.com/+eleconomistaes/">Google+</a>
</li>
<li style="margin-top: 20px;">
</li><li class="descrip-pie"><a href="http://www.editorialecoprensa.es/" style="font-weight:bold;">Editorial Ecoprensa</a></li>
<li><a href="http://www.eleconomista.es/quienes/somos.php">Quiénes somos</a></li>
<li><a href="http://www.eleconomista.es/publicidad/">Publicidad</a></li>
<li><a href="http://www.eleconomista.es/archivo-noticias/index.html">Archivo</a></li>
</ul>
<ul style="width:110px">
<li class="descrip-pie">Servicios</li>
<li><a href="http://www.eleconomista.es/alertas/"> Alertas móvil</a></li>
<li><a href="http://ecodiario.eleconomista.es/cartelera/">Cartelera</a></li>
<li>
<a href="http://www.eleconomista.es/el-tiempo/">El tiempo</a><a href="http://ad.doubleclick.net/clk;248020731;50676649;j?http://www.weatherpro.eu/es/home.html" rel="nofollow"><img src="./eco_files/wpro2.png" alt="Weather Pro" style="margin: 0 0 0 3px;width: 70px;vertical-align: text-bottom;"></a>
</li>
<li><a href="http://www.eleconomista.es/evasion/libros/">Libros</a></li>
<li><a href="http://listas.eleconomista.es/">Listas</a></li>
<li><a href="http://www.eleconomista.es/rss/index.php"><i class="fa fa-rss"></i>RSS</a></li>
<li><a href="http://coleccion.eleconomista.es/">Ecoleccionista</a></li>
</ul>
<ul class="especiales">
<li class="descrip-pie"><a style="font-weight:bold" href="http://www.eleconomista.es/especiales/index.html">Especiales</a></li>
<li><a href="http://www.eleconomista.es/especiales/elecciones/2015/index.html">Eleciones Autonómicas</a></li>
<li><a href="http://www.eleconomista.es/especiales/elecciones/2015/index.html">Eleciones Municipales</a></li>
<li><a href="http://ecodiario.eleconomista.es/deportes/mundial-baloncesto/index.php">Especial Baloncesto 2014</a></li>
<li><a href="http://ecodiario.eleconomista.es/deportes/mundial/index.php">Especial Mundial Brasil 2014</a></li>
<li><a href="http://www.eleconomista.es/especiales/oscars/">Gala de los Oscars 2014</a></li>
<li><a href="http://www.eleconomista.es/especiales/premios-goya/">Premios Goya 2014</a></li>
<li><a href="http://www.eleconomista.es/ecomotor/formula-1/index.html">Fórmula 1</a></li>
<li><a href="http://www.eleconomista.es/especiales/loteria-navidad/">Lotería de Navidad</a></li>
<li><a href="http://www.eleconomista.es/declaracion-renta/">Declaración de la Renta</a></li>
</ul>
</div>
<div class="pie-links-cont"><div class="pie-links">
<b style="color: #666;">Nuestros partners: </b><a style="font-weight:bold" href="http://www.eleconomista.es/CanalPDA/"> CanalPDA </a> | <a href="http://www.eleconomista.es/boxoffice/"><span style="color:#ba3b30; font-weight:bold"> Boxoffice </span> - Industria del cine</a> | <a style="font-weight:bold" href="http://www.ilsole24ore.com/english"> ilSole - English version </a> | <a style="font-weight:bold" href="http://empresite.eleconomista.es/"> Empresite: España </a> - <a style="font-weight:bold" href="http://empresite.eleconomistaamerica.co/" target="_blank">Colombia </a> | <a style="font-weight:bold" href="http://administradores.eleconomista.es/" target="_blank"> Administradores y Ejecutivos </a> | <a style="font-weight:bold" href="http://ranking-empresas.eleconomista.es/" target="_blank">Ranking de Empresas</a>
</div></div>
<div class="editorial"><p><a href="http://www.editorialecoprensa.es/">Ecoprensa S.A.</a> - Todos los derechos reservados | <a href="http://www.eleconomista.es/politica-de-privacidad/">Nota Legal</a> | <a href="http://www.eleconomista.es/politica-de-privacidad/cookies.php">Política de cookies</a> | <a href="http://www.acens.com/" target="_blank" rel="nofollow">Cloud Hosting en Acens</a> | <a href="http://www.paradigmatecnologico.com/" rel="nofollow" target="_blank">Powered by Paradigma</a></p></div>
</div>
<div id="barra-nav" class="barra-inf">
<div mod="2086" class="bb">
<script language="javascript">//<![CDATA[
					sz = "550x20" ;
					st = "eleconomista.es" ;
					std = "http://ad.es.doubleclick.net/" ;
					kw = "269617462";
					 sect ="6784723";
					document.write( '<SCR'+'IPT language="JavaScript" SRC="'+std+'adj/'+st+'/economista_noticias_construcion;sect='+sect+';tile=10;kw='+kw+';sz='+sz+';ord='+randnum+'?"></SCR'+'IPT>' ) ;
					
					
					document.write( '<SCR'+'IPT language="JavaScript" SRC="http://s01.s3c.es/js/eleconomista/cache/dockinbar.js"></SCR'+'IPT>' ) ;
					
	 				/* document.write('<link rel="stylesheet" href="/css3/barra.css" type="text/css"/>'); */
				//]]></script><script language="JavaScript" src="./eco_files/economista_noticias_construcion(8)"></script><div class="GoogleActiveViewClass" id="DfpVisibilityIdentifier_4667299706488848"><script>(function(){var g=this,l=function(a,b){var c=a.split("."),d=g;c[0]in d||!d.execScript||d.execScript("var "+c[0]);for(var e;c.length&&(e=c.shift());)c.length||void 0===b?d=d[e]?d[e]:d[e]={}:d[e]=b},m=function(a,b,c){return a.call.apply(a.bind,arguments)},n=function(a,b,c){if(!a)throw Error();if(2<arguments.length){var d=Array.prototype.slice.call(arguments,2);return function(){var c=Array.prototype.slice.call(arguments);Array.prototype.unshift.apply(c,d);return a.apply(b,c)}}return function(){return a.apply(b,arguments)}},p=function(a,b,c){p=Function.prototype.bind&&-1!=Function.prototype.bind.toString().indexOf("native code")?m:n;return p.apply(null,arguments)},q=Date.now||function(){return+new Date};var r=document,s=window;var t=function(a,b){for(var c in a)Object.prototype.hasOwnProperty.call(a,c)&&b.call(null,a[c],c,a)},w=function(a,b){a.google_image_requests||(a.google_image_requests=[]);var c=a.document.createElement("img");c.src=b;a.google_image_requests.push(c)};var x=function(a){return{visible:1,hidden:2,prerender:3,preview:4}[a.webkitVisibilityState||a.mozVisibilityState||a.visibilityState||""]||0},y=function(a){var b;a.mozVisibilityState?b="mozvisibilitychange":a.webkitVisibilityState?b="webkitvisibilitychange":a.visibilityState&&(b="visibilitychange");return b};var C=function(){this.g=r;this.k=s;this.j=!1;this.i=null;this.h=[];this.o={};if(z)this.i=q();else if(3==x(this.g)){this.i=q();var a=p(this.q,this);A&&(a=A("di::vch",a));this.p=a;var b=this.g,c=y(this.g);b.addEventListener?b.addEventListener(c,a,!1):b.attachEvent&&b.attachEvent("on"+c,a)}else B(this)},A;C.m=function(){return C.n?C.n:C.n=new C};var D=/^([^:]+:\/\/[^/]+)/m,G=/^\d*,(.+)$/m,z=!1,B=function(a){if(!a.j){a.j=!0;for(var b=0;b<a.h.length;++b)a.l.apply(a,a.h[b]);a.h=[]}};C.prototype.s=function(a,b){var c=b.target.u();(c=G.exec(c))&&(this.o[a]=c[1])};C.prototype.l=function(a,b){this.k.rvdt=this.i?q()-this.i:0;var c;if(c=this.t)t:{try{var d=D.exec(this.k.location.href),e=D.exec(a);if(d&&e&&d[1]==e[1]&&b){var f=p(this.s,this,b);this.t(a,f);c=!0;break t}}catch(u){}c=!1}c||w(this.k,a)};C.prototype.q=function(){if(3!=x(this.g)){B(this);var a=this.g,b=y(this.g),c=this.p;a.removeEventListener?a.removeEventListener(b,c,!1):a.detachEvent&&a.detachEvent("on"+b,c)}};var H=/^true$/.test("")?!0:!1;var I={},J=function(a){var b=a.toString();a.name&&-1==b.indexOf(a.name)&&(b+=": "+a.name);a.message&&-1==b.indexOf(a.message)&&(b+=": "+a.message);if(a.stack){a=a.stack;var c=b;try{-1==a.indexOf(c)&&(a=c+"\n"+a);for(var d;a!=d;)d=a,a=a.replace(/((https?:\/..*\/)[^\/:]*:\d+(?:.|\n)*)\2/,"$1");b=a.replace(/\n */g,"\n")}catch(e){b=c}}return b},M=function(a,b,c,d){var e=K,f,u=!0;try{f=b()}catch(h){try{var N=J(h);b="";h.fileName&&(b=h.fileName);var E=-1;h.lineNumber&&(E=h.lineNumber);var v;t:{try{v=c?c():"";break t}catch(S){}v=""}u=e(a,N,b,E,v)}catch(k){try{var O=J(k);a="";k.fileName&&(a=k.fileName);c=-1;k.lineNumber&&(c=k.lineNumber);K("pAR",O,a,c,void 0,void 0)}catch(F){L({context:"mRE",msg:F.toString()+"\n"+(F.stack||"")},void 0)}}if(!u)throw h;}finally{if(d)try{d()}catch(T){}}return f},K=function(a,b,c,d,e,f){a={context:a,msg:b.substring(0,512),eid:e&&e.substring(0,40),file:c,line:d.toString(),url:r.URL.substring(0,512),ref:r.referrer.substring(0,512)};P(a);L(a,f);return!0},L=function(a,b){try{if(Math.random()<(b||.01)){var c="/pagead/gen_204?id=jserror"+Q(a),d="http"+("https:"==s.location.protocol?"s":"")+"://pagead2.googlesyndication.com"+c,d=d.substring(0,2E3);w(s,d)}}catch(e){}},P=function(a){var b=a||{};t(I,function(a,d){b[d]=s[a]})},R=function(a,b,c,d,e){return function(){var f=arguments;return M(a,function(){return b.apply(c,f)},d,e)}},Q=function(a){var b="";t(a,function(a,d){if(0===a||a)b+="&"+d+"="+("function"==typeof encodeURIComponent?encodeURIComponent(a):escape(a))});return b};A=function(a,b,c,d){return R(a,b,void 0,c,d)};z=H;l("vu",R("vu",function(a,b){var c=a.replace("&amp;","&"),d=/(google|doubleclick).*\/pagead\/adview/.test(c),e=C.m();if(d){d="&vis="+x(e.g);b&&(d+="&ve=1");var f=c.indexOf("&adurl"),c=-1==f?c+d:c.substring(0,f)+d+c.substring(f)}e.j?e.l(c,b):e.h.push([c,b])}));l("vv",R("vv",function(){z&&B(C.m())}));})();</script><script>vu("http://pubads.g.doubleclick.net/pagead/adview?ai\x3dBlr3UKUiBVbqNNc2sigb2nYGIDe7qsYAIAAAAEAEgADgAWPbH2-eKAmDVjdOCvAiCARdjYS1wdWItODAxODk0MzI3NTc5OTg3MbIBE3d3dy5lbGVjb25vbWlzdGEuZXO6AQlnZnBfaW1hZ2XIAQnaAbgBaHR0cDovL3d3dy5lbGVjb25vbWlzdGEuZXMvaW50ZXJzdGl0aWFsL3ZvbHZlci8yNjk2MTc0NjIvY29uc3RydWNjaW9uLWlubW9iaWxpYXJpby9ub3RpY2lhcy82Nzg0NzIzLzA2LzE1L0xhLUNhaXhhLWNyZWUtcXVlLWVsLXB1bHNvLWRlbC1zZWN0b3ItaW5tb2JpbGlhcmlvLWNvbWllbnphLWEtcmVjdXBlcmFyc2UuaHRtbJgCeKkCqr7Amm3osj7AAgLgAgDqAjQ0Mjc1L2VsZWNvbm9taXN0YS5lcy9lY29ub21pc3RhX25vdGljaWFzX2NvbnN0cnVjaW9u-AL_0R6QA8gGmAPIBqgDAeAEAZIFCwgHEAEYASDWiO4QkAYBoAYW2AcA\x26sigh\x3d5SlB9gGwtWg\x26cid\x3d5GhZ2xMyf7WGXhfU1sewX1km")</script><script src="./eco_files/engine"></script><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="550" height="20"><param name="movie" value="http://ads5.fxdepo.com/ads/03/80/33/71_385217BankerSecrets_br3_550x20_esp.swf?ClickTag=http%3A%2F%2Fpbid%2Efxdepo%2Ecom%2Fengine%3Fsite%3D139745%2Bpage%3D%24x%24%2Bspace%3D0%2Blink%3D%24129878%2D251013%2D0%24%2Bgoto%3D%24http%3A%2F%2Fwww%2Eiforex%2Ees%2Flanding%5Fpages%2Fbr3%2Fes%2FTraderGuideORGDynR%2F%3Fcontent%3DTraderGuideORGDynR%26sl%3DEspana%5Feu%26creg%3D3%26SID%3D385217%24"><param name="wmode" value="opaque"><param name="allowScriptAccess" value="always"><embed src="http://ads5.fxdepo.com/ads/03/80/33/71_385217BankerSecrets_br3_550x20_esp.swf?ClickTag=http%3A%2F%2Fpbid%2Efxdepo%2Ecom%2Fengine%3Fsite%3D139745%2Bpage%3D%24x%24%2Bspace%3D0%2Blink%3D%24129878%2D251013%2D0%24%2Bgoto%3D%24http%3A%2F%2Fwww%2Eiforex%2Ees%2Flanding%5Fpages%2Fbr3%2Fes%2FTraderGuideORGDynR%2F%3Fcontent%3DTraderGuideORGDynR%26sl%3DEspana%5Feu%26creg%3D3%26SID%3D385217%24" width="550" height="20" wmode="opaque" swliveconnect="true" allowscriptaccess="always" type="application/x-shockwave-flash"></object>
</div><script language="JavaScript" src="./eco_files/dockinbar.js"></script><noscript>&lt;a href="http://ad.doubleclick.net/N4275/jump/eleconomista.es/economista_noticias_construcion;sect=6784723;tile=10;kw=269617462;sz=550x20;ord=123456789?" target="_blank" rel="nofollow"&gt;&lt;img src="http://ad.doubleclick.net/N4275/ad/eleconomista.es/economista_noticias_construcion;sect=6784723;tile=10;kw=269617462;sz=550x20;ord=123456789?" width="365" height="60" border="0"&gt;&lt;/a&gt;</noscript>
</div>
<ul class="bi">
<li id="ATecnico"><a href="http://www.eleconomista.es/analisis-tecnico" class="barra-enlace">A. Técnico</a></li>
<li id="Cartera"><a href="http://www.eleconomista.es/cartera/portada.php" class="barra-enlace">Su Cartera</a></li>
<li id="UltNoticias">
<a href="http://www.eleconomista.es/interstitial/volver/269617462/construccion-inmobiliario/noticias/6784723/06/15/La-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html#" class="barra-enlace" onclick="return false;">Noticias</a><div class="barra-ventana">
<div class="ventana-cerrar"><img src="./eco_files/i-cerrar4.gif" class="imagen-cerrar"></div>
<div class="ventana-contenido" id="contUltNoticias"><div class="loading"><p>Cargando Ultimas Noticias...</p></div></div>
</div>
</li>
<li id="Cotizaciones">
<a href="http://www.eleconomista.es/interstitial/volver/269617462/construccion-inmobiliario/noticias/6784723/06/15/La-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html#" class="barra-enlace" onclick="return false;">Cotizaciones</a><div class="barra-ventana">
<div class="ventana-cerrar"><img src="./eco_files/i-cerrar4.gif" class="imagen-cerrar"></div>
<div class="ventana-contenido">
<div class="ventana-contenido-g" id="contCotizaciones"><div class="loading"><p>Cargando Cotizaciones...</p></div></div>
<div class="ventana-contenido-e">
<a href="http://www.eleconomista.es/ecotrader/"><span style="color:#000">· Eco</span><span style="color:#FF0000">trader.es</span> herramientas, estrategias de inversión</a><a href="http://www.eleconomista.es/fichas-de-empresas/index.html">· Vea todas la cotizaciones de empresas</a><a href="http://www.eleconomista.es/mercados-cotizaciones/index.html">· Todas las noticias de Bolsa y Mercados</a>
</div>
</div>
</div>
</li>
</ul>
</div><script type="text/javascript">
	 
	 			 
			if (/MSIE (\d+\.\d+);/.test(navigator.userAgent))
			{ 
			 var ieversion=new Number(RegExp.$1) // capture x.x portion and store as a number
			 if (ieversion==6)
			  document.getElementById("barra-nav").style.visibility = 'hidden';
			 }
			else if (navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i))
				document.getElementById("barra-nav").style.visibility = 'hidden';
			
	
	</script><script type="text/javascript">

		var UltNoticias = new dockElement('UltNoticias', {'title':'Ultimas Noticias', url_data:"/slices/bar-slice.php?modulo=1246", refreshPeriod:60, refreshDate:'empty'});

//		var Blogs = new dockElement('Blogs', {'title':'BLogs', url_data:"/portada/index.php?ModoAjax=1856,1124&AjaxJS=1", refreshPeriod:9000, refreshDate:'empty'});
		
		//var Blogs = new dockElement('Blogs', {'title':'BLogs', url_data:"/slices/bar-slice.php?modulo=1856", refreshPeriod:9000, refreshDate:'empty'});
		var Cotizaciones = new dockElement("Cotizaciones", {'title':'Cotizaciones', url_data:'/slices/bar-slice.php?modulo=2171', refreshPeriod:60, refreshDate:'empty'});	
//		var elementos = {'UltNoticias':UltNoticias, 'Blogs':Blogs, 'Cotizaciones':Cotizaciones};
		var elementos = {'UltNoticias':UltNoticias, 'Cotizaciones':Cotizaciones};
		dockBar = new dockBar(elementos);
	
		dockBar.setObservers();

</script>
<script type="text/javascript"> var g_cPartnerId = "eleconomista.es-01";var g_cTheme = "eleconomista_1_0";</script><script type="text/javascript" charset="UTF-8" src="./eco_files/jsgic.js"></script>
<script type="text/javascript">// <![CDATA[
var _qq=document.createElement('script'),
_qf = 'Kku8', _qc = [],
_qh = document.getElementsByTagName('script')[0];
_qq.type = 'text/javascript';
_qq.setAttribute('src','http'+
(('https:'==document.location.protocol)?'s':'')+
'://www.intentshare.com/is/?f='+_qf);
_qh.parentNode.insertBefore(_qq, _qh);
// ]]></script>
					<script type="text/javascript">
					//<![CDATA[
						var eco = new ecoQuotes() ;eco.Anadir( "espana" , 0 ) ; eco.Anadir( "francia" , 0 ) ; eco.Anadir( "italia" , 0 ) ; eco.Anadir( "grecia" , 0 ) ; eco.Anadir( "portugal" , 0 ) ; 
						eco.Leer(0);
						eco.Leer(2);
						setInterval( function(){eco.Leer(2);}, 90000 ) ; //90 segundos
						setInterval( function(){eco.Leer(0);}, 90000 ) ; //90 segundos
					//]]>
					</script><script type="text/javascript">
		jQuery(window).load(function(){var a=document.createElement("script");
		var b=document.getElementsByTagName("script")[0];
		a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0027/6809.js?"+Math.floor(new Date().getTime()/3600000);
		a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)});
		</script>
<div class="intentshare-image-wrapper intentshare-image-pos-tl intentshare-asset"><a href="http://www.eleconomista.es/interstitial/volver/269617462/construccion-inmobiliario/noticias/6784723/06/15/La-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html#" class="intentshare-share-btn intentshare-share-getintentshare" alt="Get IntentShare!" title="Get IntentShare!" onclick="window.setTimeout(function() { window.open(&#39;http://www.intentshare.com&#39;, &#39;_blank&#39;); }, 300); return false"></a><a href="http://www.eleconomista.es/interstitial/volver/269617462/construccion-inmobiliario/noticias/6784723/06/15/La-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html#" class="intentshare-share-btn intentshare-share-fb" alt="Share to Facebook" title="Share to Facebook" onclick="IntentShare.utils.share_img(&#39;facebook&#39;); return false"></a><a href="http://www.eleconomista.es/interstitial/volver/269617462/construccion-inmobiliario/noticias/6784723/06/15/La-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html#" class="intentshare-share-btn intentshare-share-tw" alt="Share to Twitter" title="Share to Twitter" onclick="IntentShare.utils.share_img(&#39;twitter&#39;); return false"></a><a href="http://www.eleconomista.es/interstitial/volver/269617462/construccion-inmobiliario/noticias/6784723/06/15/La-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html#" class="intentshare-share-btn intentshare-share-gp" alt="Share to Google+" title="Share to Google+" onclick="IntentShare.utils.share_img(&#39;googleplus&#39;); return false"></a><a href="http://www.eleconomista.es/interstitial/volver/269617462/construccion-inmobiliario/noticias/6784723/06/15/La-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html#" class="intentshare-share-btn intentshare-share-li" alt="Share to LinkedIn" title="Share to LinkedIn" onclick="IntentShare.utils.share_img(&#39;linkedin&#39;); return false"></a><a href="http://www.eleconomista.es/interstitial/volver/269617462/construccion-inmobiliario/noticias/6784723/06/15/La-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html#" class="intentshare-share-btn intentshare-share-pn" alt="Share to Pinterest" title="Share to Pinterest" onclick="IntentShare.utils.share_img(&#39;pinterest&#39;); return false"></a><a href="http://www.eleconomista.es/interstitial/volver/269617462/construccion-inmobiliario/noticias/6784723/06/15/La-Caixa-cree-que-el-pulso-del-sector-inmobiliario-comienza-a-recuperarse.html#" class="intentshare-share-btn intentshare-share-em" alt="Share to Email" title="Share to Email" onclick="IntentShare.utils.share_img(&#39;email&#39;); return false"></a><div class="clearer"></div></div><style type="text/css" class="intentshare-asset">.intentshare-arrow-box .intentshare-tip-icon, a.intentshare-share-btn { background-color: #undefined !important; }</style></body></html>