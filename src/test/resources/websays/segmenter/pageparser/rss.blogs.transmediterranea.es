<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:wfw="http://wellformedweb.org/CommentAPI/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
	>

<channel>
	<title>TrasmediterraneaTrasmediterranea</title>
	<atom:link href="http://blog.trasmediterranea.es/feed/" rel="self" type="application/rss+xml" />
	<link>http://blog.trasmediterranea.es</link>
	<description>me gusta gusta el mar</description>
	<lastBuildDate>Wed, 05 Feb 2014 09:36:46 +0000</lastBuildDate>
	<language>es-ES</language>
		<sy:updatePeriod>hourly</sy:updatePeriod>
		<sy:updateFrequency>1</sy:updateFrequency>
	<!--Theme by MyThemeShop.com-->
	<item>
		<title>¡Agüitaaa! ¡Mójate y disfruta del Carnaval de Santa Cruz de Tenerife con Trasmediterranea! Concurso CarnavalSCTrasme</title>
		<link>http://blog.trasmediterranea.es/2014/02/05/aguitaaa-mojate-y-disfruta-del-carnaval-de-santa-cruz-de-tenerife-con-trasmediterranea-concurso-carnavalsctrasme/</link>
		<comments>http://blog.trasmediterranea.es/2014/02/05/aguitaaa-mojate-y-disfruta-del-carnaval-de-santa-cruz-de-tenerife-con-trasmediterranea-concurso-carnavalsctrasme/#comments</comments>
		<pubDate>Wed, 05 Feb 2014 09:04:44 +0000</pubDate>
		<dc:creator><![CDATA[.]]></dc:creator>
				<category><![CDATA[Ofertas y promociones]]></category>
		<category><![CDATA[canarias]]></category>
		<category><![CDATA[Carnaval]]></category>
		<category><![CDATA[CarnavalSC]]></category>
		<category><![CDATA[CarnavalSCTrasme]]></category>
		<category><![CDATA[Santa Cruz de Tenerife]]></category>

		<guid isPermaLink="false">http://blog.trasmediterranea.es/?p=1316</guid>
		<description><![CDATA[<p><img width="150" height="150" src="http://blog.trasmediterranea.es/wp-content/uploads/FACE1-150x150.jpg" class="attachment-post-thumbnail wp-post-image" alt="FACE" /></p><a href="http://blog.trasmediterranea.es/wp-content/uploads/carnaval-tenerife-2014-cartel1.jpg"><img class="aligncenter size-full wp-image-1320" alt="carnaval-tenerife-2014-cartel" src="http://blog.trasmediterranea.es/wp-content/uploads/carnaval-tenerife-2014-cartel1.jpg" width="457" height="640" /></a>

Llega el mejor Carnaval del mundo... ¡llega el <a href="http://www.carnavaltenerife.es/" target="_blank">Carnaval de Tenerife</a>! Uno de los tres más famosos del mundo junto con el de Río de Janeiro y Venecia.

Pero eso ya lo sabéis, así que os puedo contar que éstas son las fechas en las que todos los amigos y conocidos que viven fuera de las islas me llaman para que les haga un hueco en casa. Nadie quiere perdérselo y no es para menos.

Prueba de ello es que mientras redactaba este texto las entradas para los principales actos del Carnaval volaban. La final de Murgas, por ejemplo, colgó el cartel de “no hay entradas” en media hora. Pero tengo una buena noticia para todos vosotros. <a href="http://www.trasmediterranea.es/" target="_blank">Trasmediterranea</a>, como patrocinador del Carnaval de Santa Cruz de Tenerife, te invita a disfrutar de las galas y actos del Carnaval. Así que ya pueden respirar tranquilos. Para calentar motores te presentamos cómo conseguir gratuitamente entradas para la <strong>Fase de Murgas Adultas</strong> que se celebra los días <strong>9, 10</strong><span style="line-height: 1.5em;"><strong>, 11 y 12</strong>. Verás que es muy fácil solo comenta este post y en el comentario deberás indicar tu nombre y primer apellido además de </span><strong style="line-height: 1.5em;">#CarnavalSCTrasme</strong><span style="line-height: 1.5em;">(*). Pincha </span><a style="line-height: 1.5em;" href="http://slidesha.re/1kRf6RN" target="_blank">aquí </a><span style="line-height: 1.5em;">y conocerás todos los detalles de cómo participar en este sorteo de entradas.</span>

<a href="http://blog.trasmediterranea.es/wp-content/uploads/FACE1.jpg"><img class="aligncenter size-full wp-image-1327" alt="FACE" src="http://blog.trasmediterranea.es/wp-content/uploads/FACE1.jpg" width="640" height="374" /></a>

El Carnaval de Tenerife es un carnaval no profesional donde los miles de componentes de las agrupaciones son personas anónimas que pasan todo el año preparando estos días. Hay muchos kilómetros de disfraces que tejer, coreografías de baile que montar y letras de canciones que ensayar. En Tenerife nos gusta decir que el nuestro es el Carnaval del pueblo.

Para escribir este artículo para los lectores de Me Gusta el Mar de Trasmediterranea me fui a hablar con el autor del cartel del Carnaval 2014.

Juan Pedro Hidalgo Sabina es un joven creativo publicitario que nos sorprendió este año con el personaje “¡¡Agüitaaa!!” que, además de tener forma de gota agua, recoge perfectamente una expresión muy canaria que hace referencia a una agradable sorpresa. Este año el cartel del Carnaval sale del papel y el personaje es fruto de un trabajo de escultura digital.  Al fin y al cabo el motivo del Carnaval 2014 son los dibujos animados.

Os puedo adelantar que en la Gala de Elección de la Reina habrá una sorpresa. Durante los primeros 15 minutos veremos una proyección de vídeo, donde el personaje del cartel acompañado de otros seres de su especie, se transforman una y otra vez para contarnos varias historias que se van a desarrollar sobre el escenario. Así será un espectáculo totalmente multimedia. El guión de todo este despliegue de dibujos animados en pantallas gigantes junto con es espectáculo del escenario es obra de Juan Carlos Armas, director de la Gala del Carnaval que está ilusionado con esta novedosa manera de darle vida al cartel. Se ha cuidado al detalle hasta la música, creándose una banda sonora original para la ocasión.  Contaros que en la edición del vídeo está trabajando el equipo de la película de animación “Hiroku”, que cuenta este año con 9 candidaturas a los premios Goya. Casi nada. El espectáculo está garantizado y vosotros puedes ser espectador de excepción.

Recuerda Trasmediterranea, te lleva y Santa Cruz te acoge con los brazos abiertos. ¡Ven al Carnaval!

Firma: César Sar

Periodista, Viajero y Gastrónomo.

(*) Se repartirán 20 packs (de 4 entradas cada uno, para cada uno de los días/ fases de las Murgas Adultas) entre los autores de los 20 primeros comentarios en este post. Para conseguir las entradas, en el comentario se deberá indicar el nombre y primer apellido del participante además de #CarnavalSCTrasme. Ejemplo: Pablo Rodríguez #CarnavalSCTrasme.

Nota: Los comentarios para entrar en el concurso serán moderados y no aparecerán automáticamente. Una vez finalizado el plazo de concurso, aparecerán los 20 ganadores.]]></description>
				<content:encoded><![CDATA[<p><img width="150" height="150" src="http://blog.trasmediterranea.es/wp-content/uploads/FACE1-150x150.jpg" class="attachment-post-thumbnail wp-post-image" alt="FACE" /></p><a href="http://blog.trasmediterranea.es/wp-content/uploads/carnaval-tenerife-2014-cartel1.jpg"><img class="aligncenter size-full wp-image-1320" alt="carnaval-tenerife-2014-cartel" src="http://blog.trasmediterranea.es/wp-content/uploads/carnaval-tenerife-2014-cartel1.jpg" width="457" height="640" /></a>

Llega el mejor Carnaval del mundo... ¡llega el <a href="http://www.carnavaltenerife.es/" target="_blank">Carnaval de Tenerife</a>! Uno de los tres más famosos del mundo junto con el de Río de Janeiro y Venecia.

Pero eso ya lo sabéis, así que os puedo contar que éstas son las fechas en las que todos los amigos y conocidos que viven fuera de las islas me llaman para que les haga un hueco en casa. Nadie quiere perdérselo y no es para menos.

Prueba de ello es que mientras redactaba este texto las entradas para los principales actos del Carnaval volaban. La final de Murgas, por ejemplo, colgó el cartel de “no hay entradas” en media hora. Pero tengo una buena noticia para todos vosotros. <a href="http://www.trasmediterranea.es/" target="_blank">Trasmediterranea</a>, como patrocinador del Carnaval de Santa Cruz de Tenerife, te invita a disfrutar de las galas y actos del Carnaval. Así que ya pueden respirar tranquilos. Para calentar motores te presentamos cómo conseguir gratuitamente entradas para la <strong>Fase de Murgas Adultas</strong> que se celebra los días <strong>9, 10</strong><span style="line-height: 1.5em;"><strong>, 11 y 12</strong>. Verás que es muy fácil solo comenta este post y en el comentario deberás indicar tu nombre y primer apellido además de </span><strong style="line-height: 1.5em;">#CarnavalSCTrasme</strong><span style="line-height: 1.5em;">(*). Pincha </span><a style="line-height: 1.5em;" href="http://slidesha.re/1kRf6RN" target="_blank">aquí </a><span style="line-height: 1.5em;">y conocerás todos los detalles de cómo participar en este sorteo de entradas.</span>

<a href="http://blog.trasmediterranea.es/wp-content/uploads/FACE1.jpg"><img class="aligncenter size-full wp-image-1327" alt="FACE" src="http://blog.trasmediterranea.es/wp-content/uploads/FACE1.jpg" width="640" height="374" /></a>

El Carnaval de Tenerife es un carnaval no profesional donde los miles de componentes de las agrupaciones son personas anónimas que pasan todo el año preparando estos días. Hay muchos kilómetros de disfraces que tejer, coreografías de baile que montar y letras de canciones que ensayar. En Tenerife nos gusta decir que el nuestro es el Carnaval del pueblo.

Para escribir este artículo para los lectores de Me Gusta el Mar de Trasmediterranea me fui a hablar con el autor del cartel del Carnaval 2014.

Juan Pedro Hidalgo Sabina es un joven creativo publicitario que nos sorprendió este año con el personaje “¡¡Agüitaaa!!” que, además de tener forma de gota agua, recoge perfectamente una expresión muy canaria que hace referencia a una agradable sorpresa. Este año el cartel del Carnaval sale del papel y el personaje es fruto de un trabajo de escultura digital.  Al fin y al cabo el motivo del Carnaval 2014 son los dibujos animados.

Os puedo adelantar que en la Gala de Elección de la Reina habrá una sorpresa. Durante los primeros 15 minutos veremos una proyección de vídeo, donde el personaje del cartel acompañado de otros seres de su especie, se transforman una y otra vez para contarnos varias historias que se van a desarrollar sobre el escenario. Así será un espectáculo totalmente multimedia. El guión de todo este despliegue de dibujos animados en pantallas gigantes junto con es espectáculo del escenario es obra de Juan Carlos Armas, director de la Gala del Carnaval que está ilusionado con esta novedosa manera de darle vida al cartel. Se ha cuidado al detalle hasta la música, creándose una banda sonora original para la ocasión.  Contaros que en la edición del vídeo está trabajando el equipo de la película de animación “Hiroku”, que cuenta este año con 9 candidaturas a los premios Goya. Casi nada. El espectáculo está garantizado y vosotros puedes ser espectador de excepción.

Recuerda Trasmediterranea, te lleva y Santa Cruz te acoge con los brazos abiertos. ¡Ven al Carnaval!

Firma: César Sar

Periodista, Viajero y Gastrónomo.

(*) Se repartirán 20 packs (de 4 entradas cada uno, para cada uno de los días/ fases de las Murgas Adultas) entre los autores de los 20 primeros comentarios en este post. Para conseguir las entradas, en el comentario se deberá indicar el nombre y primer apellido del participante además de #CarnavalSCTrasme. Ejemplo: Pablo Rodríguez #CarnavalSCTrasme.

Nota: Los comentarios para entrar en el concurso serán moderados y no aparecerán automáticamente. Una vez finalizado el plazo de concurso, aparecerán los 20 ganadores.]]></content:encoded>
			<wfw:commentRss>http://blog.trasmediterranea.es/2014/02/05/aguitaaa-mojate-y-disfruta-del-carnaval-de-santa-cruz-de-tenerife-con-trasmediterranea-concurso-carnavalsctrasme/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>Hollywood a bordo</title>
		<link>http://blog.trasmediterranea.es/2014/01/31/hollywood-a-bordo/</link>
		<comments>http://blog.trasmediterranea.es/2014/01/31/hollywood-a-bordo/#comments</comments>
		<pubDate>Fri, 31 Jan 2014 10:54:24 +0000</pubDate>
		<dc:creator><![CDATA[.]]></dc:creator>
				<category><![CDATA[Cultura y Mar:]]></category>
		<category><![CDATA[cine]]></category>
		<category><![CDATA[Exodus]]></category>
		<category><![CDATA[Fuerteventura]]></category>
		<category><![CDATA[Hollywood]]></category>
		<category><![CDATA[La Gomera]]></category>
		<category><![CDATA[lanzarote]]></category>
		<category><![CDATA[logística]]></category>
		<category><![CDATA[película]]></category>
		<category><![CDATA[Ridley Scott]]></category>
		<category><![CDATA[transporte]]></category>

		<guid isPermaLink="false">http://blog.trasmediterranea.es/?p=1312</guid>
		<description><![CDATA[<p><img width="150" height="150" src="http://blog.trasmediterranea.es/wp-content/uploads/290114_Fuerteventura-Jumilla-150x150.jpg" class="attachment-post-thumbnail wp-post-image" alt="290114_Fuerteventura (Jumilla)" /></p><p align="center"><a href="http://blog.trasmediterranea.es/wp-content/uploads/290114_Fuerteventura-Jumilla.jpg"><img class="aligncenter size-full wp-image-1313" alt="290114_Fuerteventura (Jumilla)" src="http://blog.trasmediterranea.es/wp-content/uploads/290114_Fuerteventura-Jumilla.jpg" width="640" height="361" /></a></p>
¿Qué hay detrás de una superproducción? Hollywood ha visitado España varias veces por las inmensas posibilidades de sus paisajes, y han sido muchos los directores que han repetido por la buena experiencia durante el rodaje. La magia del cine transforma de un modo espectacular las localizaciones, y es capaz de hacernos creer que Fuerteventura es Egipto. Pero para conseguir ese resultado, hace falta mucho trabajo que a simple vista puede que no reconozcamos, pero que es la clave para que la historia funcione.

Como ya sabéis, uno de los últimos en sucumbir a los encantos paisajísticos de España ha sido el director de cine <b>Ridley Scott</b>, que ha elegido <a href="http://visitfuerteventura.es/" target="_blank"><b>Fuerteventura</b></a>, <a href="http://www.lagomera.travel/islas-canarias/la-gomera/es/" target="_blank"><b>La Gomera</b></a> y <a href="http://www.turismolanzarote.com/" target="_blank"><b>Lanzarote</b></a> como escenarios para su último largometraje, <b><i>Exodus</i></b>. No es la primera vez que el director visita nuestro país, ya que varias escenas tanto de <b><i>El Reino de los Cielos</i></b> como de <b><i>Gladiator</i></b> se rodaron aquí. La nueva película de Ridley, que narra la historia de Moisés y la liberación del pueblo hebreo, ha encontrado su propio desierto del Sinaí en nuestro país.

<a href="http://w3.trasmediterranea.es/es/compania/noticias/Paginas/trasmediterranea-fuerteventura-superproducci%C3%B3n-Exodus.aspx" target="_blank"><b>Trasmediterranea </b></a>ha participado activamente en el rodaje de este film, ya que se ha ocupado del transporte del material desde Almería (donde se rodó parte de la película) hasta Fuerteventura. El buque Superfast Baleares fue el encargado de dicha travesía, que consistió en trasladar hasta <b>274 vehículos</b>, camiones y containers con infraestructura y medios técnicos, que se sumaron al <b>centenar de turismos y furgonetas</b> que la productora Volcano alquiló en la isla. Se eligió este ferry por su gran capacidad<b>, 3.400 metros lineales</b> y posibilidad de transportar hasta <b>100 vehículos</b>. Pero el buque también tuvo unos pasajeros muy especiales, <b>66 caballos de pura raza, 13 mulas, gansos, gallinas y perros</b>. Los inquilinos viajaron junto con una clínica veterinaria móvil, y fueron los primeros en desembarcar, tal como contó el diario <a href="http://www.laprovincia.es/fuerteventura/2013/11/24/pueblo-egipcio-majorero/573448.html" target="_blank"><b>La Provincia</b></a>. En total, se tardó más de <b>3 horas</b> en sacar todo el material del buque.

La superproducción de Fox, cuyos actores principales son <b>Christian Bale</b> y <b>Joel Edgerton</b>, cuenta con un presupuesto de <b>150 millones de dólares</b>. El equipo del director lo formaban un total de <b>475 personas</b>, y han estado rodando en las islas durante seis semanas, tres más que en las anteriores localizaciones, Londres y Almería.

Una vez concluido el rodaje, a comienzos de este mes, el Superfast Baleares ha vuelto a Almería, transportando de vuelta todas las infraestructuras y el equipo que el equipo de Scott ha utilizado. En la foto de este post podemos ver una de las localizaciones elegidas para la película, situada en Fuerteventura. La logística es una parte importante de todo lo que conlleva una superproducción, y su éxito consiste en la eficiencia del trabajo y el resultado final, el cual podremos ver en pantalla. Esperando a ese momento… Fuerteventura, nos vemos en Egipto.

Foto |<a href="http://www.flickr.com/photos/jumilla/" target="_blank"> Jumilla </a>]]></description>
				<content:encoded><![CDATA[<p><img width="150" height="150" src="http://blog.trasmediterranea.es/wp-content/uploads/290114_Fuerteventura-Jumilla-150x150.jpg" class="attachment-post-thumbnail wp-post-image" alt="290114_Fuerteventura (Jumilla)" /></p><p align="center"><a href="http://blog.trasmediterranea.es/wp-content/uploads/290114_Fuerteventura-Jumilla.jpg"><img class="aligncenter size-full wp-image-1313" alt="290114_Fuerteventura (Jumilla)" src="http://blog.trasmediterranea.es/wp-content/uploads/290114_Fuerteventura-Jumilla.jpg" width="640" height="361" /></a></p>
¿Qué hay detrás de una superproducción? Hollywood ha visitado España varias veces por las inmensas posibilidades de sus paisajes, y han sido muchos los directores que han repetido por la buena experiencia durante el rodaje. La magia del cine transforma de un modo espectacular las localizaciones, y es capaz de hacernos creer que Fuerteventura es Egipto. Pero para conseguir ese resultado, hace falta mucho trabajo que a simple vista puede que no reconozcamos, pero que es la clave para que la historia funcione.

Como ya sabéis, uno de los últimos en sucumbir a los encantos paisajísticos de España ha sido el director de cine <b>Ridley Scott</b>, que ha elegido <a href="http://visitfuerteventura.es/" target="_blank"><b>Fuerteventura</b></a>, <a href="http://www.lagomera.travel/islas-canarias/la-gomera/es/" target="_blank"><b>La Gomera</b></a> y <a href="http://www.turismolanzarote.com/" target="_blank"><b>Lanzarote</b></a> como escenarios para su último largometraje, <b><i>Exodus</i></b>. No es la primera vez que el director visita nuestro país, ya que varias escenas tanto de <b><i>El Reino de los Cielos</i></b> como de <b><i>Gladiator</i></b> se rodaron aquí. La nueva película de Ridley, que narra la historia de Moisés y la liberación del pueblo hebreo, ha encontrado su propio desierto del Sinaí en nuestro país.

<a href="http://w3.trasmediterranea.es/es/compania/noticias/Paginas/trasmediterranea-fuerteventura-superproducci%C3%B3n-Exodus.aspx" target="_blank"><b>Trasmediterranea </b></a>ha participado activamente en el rodaje de este film, ya que se ha ocupado del transporte del material desde Almería (donde se rodó parte de la película) hasta Fuerteventura. El buque Superfast Baleares fue el encargado de dicha travesía, que consistió en trasladar hasta <b>274 vehículos</b>, camiones y containers con infraestructura y medios técnicos, que se sumaron al <b>centenar de turismos y furgonetas</b> que la productora Volcano alquiló en la isla. Se eligió este ferry por su gran capacidad<b>, 3.400 metros lineales</b> y posibilidad de transportar hasta <b>100 vehículos</b>. Pero el buque también tuvo unos pasajeros muy especiales, <b>66 caballos de pura raza, 13 mulas, gansos, gallinas y perros</b>. Los inquilinos viajaron junto con una clínica veterinaria móvil, y fueron los primeros en desembarcar, tal como contó el diario <a href="http://www.laprovincia.es/fuerteventura/2013/11/24/pueblo-egipcio-majorero/573448.html" target="_blank"><b>La Provincia</b></a>. En total, se tardó más de <b>3 horas</b> en sacar todo el material del buque.

La superproducción de Fox, cuyos actores principales son <b>Christian Bale</b> y <b>Joel Edgerton</b>, cuenta con un presupuesto de <b>150 millones de dólares</b>. El equipo del director lo formaban un total de <b>475 personas</b>, y han estado rodando en las islas durante seis semanas, tres más que en las anteriores localizaciones, Londres y Almería.

Una vez concluido el rodaje, a comienzos de este mes, el Superfast Baleares ha vuelto a Almería, transportando de vuelta todas las infraestructuras y el equipo que el equipo de Scott ha utilizado. En la foto de este post podemos ver una de las localizaciones elegidas para la película, situada en Fuerteventura. La logística es una parte importante de todo lo que conlleva una superproducción, y su éxito consiste en la eficiencia del trabajo y el resultado final, el cual podremos ver en pantalla. Esperando a ese momento… Fuerteventura, nos vemos en Egipto.

Foto |<a href="http://www.flickr.com/photos/jumilla/" target="_blank"> Jumilla </a>]]></content:encoded>
			<wfw:commentRss>http://blog.trasmediterranea.es/2014/01/31/hollywood-a-bordo/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>De Fitur al mundo</title>
		<link>http://blog.trasmediterranea.es/2014/01/22/de-fitur-al-mundo/</link>
		<comments>http://blog.trasmediterranea.es/2014/01/22/de-fitur-al-mundo/#comments</comments>
		<pubDate>Wed, 22 Jan 2014 15:07:34 +0000</pubDate>
		<dc:creator><![CDATA[.]]></dc:creator>
				<category><![CDATA[General]]></category>
		<category><![CDATA[Cádiz]]></category>
		<category><![CDATA[Ceuta]]></category>
		<category><![CDATA[Fitur]]></category>
		<category><![CDATA[Ibiza]]></category>
		<category><![CDATA[Islas Canarias]]></category>
		<category><![CDATA[Marruecos]]></category>
		<category><![CDATA[Melilla]]></category>
		<category><![CDATA[turismo]]></category>

		<guid isPermaLink="false">http://blog.trasmediterranea.es/?p=1297</guid>
		<description><![CDATA[<p><img width="150" height="150" src="http://blog.trasmediterranea.es/wp-content/uploads/canarias_www.alternative-design.com_-150x150.jpg" class="attachment-post-thumbnail wp-post-image" alt="canarias_www.alternative-design.com" /></p><a href="http://blog.trasmediterranea.es/wp-content/uploads/canarias_www.alternative-design.com_.jpg"><img class="aligncenter size-full wp-image-1298" alt="canarias_www.alternative-design.com" src="http://blog.trasmediterranea.es/wp-content/uploads/canarias_www.alternative-design.com_.jpg" width="640" height="422" /></a>

Esta semana los amantes de los viajes tenemos una cita ineludible, la 34 edición de la <b><a href="http://www.ifema.es/fitur_01 " target="_blank">Feria Internacional de Turismo</a>, Fitur,</b> que se está desarrollando en Madrid hasta el 26 de enero. Fitur es un punto de encuentro para los profesionales del turismo, además de para los viajeros más curiosos. El año pasado 116.157 profesionales y 91.082 personas como público general visitaron la feria, confirmando el éxito del encuentro. Fitur es una de las ferias mundiales más importantes del turismo y marca las bases del futuro desarrollo del sector.

La feria alberga diversas iniciativas como <a href="http://www.ifema.es/fitur_01/FiturGreen/" target="_blank"><b>Fitur Green</b></a> que pretende fomentar la innovación y la sostenibilidad en el sector, impulsando su competitividad, y <a href="http://www.ifema.es/fitur_01/FiturTech/" target="_blank"><b>Fitur Tech</b></a> que nos trae la tecnología más puntera adaptada al sector y presenta el “turismo de tercera generación”. Otras iniciativas profesionales de la feria son<a href="http://www.ifema.es/fitur_01/Investour/index.htm " target="_blank"><b> Investour</b></a>, que apuesta por el desarrollo sostenible del turismo en África, <a href="http://www.ifema.es/fitur_01/FiturB2B/index.htm" target="_blank"><b>Fitur B2B</b></a>, con workshops, y <a href="http://www.ifema.es/fitur_01/FiturKnowHowExport/index.htm" target="_blank"><b>Know How and Export</b></a>, espacio donde las empresas turísticas españolas muestran su potencial a compradores de otros mercados.

Fitur además de ser un excelente escaparate para las empresas del sector turístico, también es una gran fuente de inspiración para los viajeros. Para estos últimos, la feria abre sus puertas los días 25 y 26 de enero, pudiendo conocer de primera mano las últimas tendencias en viajes y acceder a las mejores ofertas. Los visitantes podrán conocer, por ejemplo, la nueva campaña de las<a href="http://www.elmejorclimadelmundo.com/" target="_blank"> <b>Islas Canarias</b></a> que presentan “el mejor clima del mundo” en 1.428 metros cuadrados; visitar “<a href="http://www.youtube.com/watch?v=z3zjHnD4qVQ" target="_blank"><b>Melilla</b></a>, paraíso de los sentidos”; conocer una nueva parte de la <a href="http://turismo.cadiz.es/es/actualidad/el-c%C3%A1diz-fenicio-ser%C3%A1-presentado-en-fitur" target="_blank"><b>Ruta Cádiz Fenicia</b></a> por el yacimiento arqueológico Gadir; o los eventos deportivos que se llevarán a cabo en <a href="http://www.ibiza.travel/es/deporte.php" target="_blank"><b>Ibiza </b></a>en 2014 “… hasta que se ponga el sol”.

Los viajeros también podrán disfrutar de las actividades que muchas de las empresas participantes organizan. Este es el caso de <a href="http://www.minube.com/ " target="_blank"><b>Minube</b></a>, con apuestas muy interesantes como la “Quedada Viajera”, que es ya un clásico dentro de la feria, la “Gymkana viajera” y el “Taller de Fotografía”, además de un punto de información para viajeros disponible durante todo el fin de semana.

<a href="http://www.trasmediterranea.es/" target="_blank"><b>Trasmediterranea </b></a>está presente un año más en algunos de los espacios de sus destinos, como <a href="http://www.ceutaturistica.com/" target="_blank"><b>Ceuta</b></a>, <a href="http://www.melillaturismo.com/" target="_blank"><b>Melilla</b></a> y <a href="http://www.visitmorocco.com/index.php/esl/content/view/full/2" target="_blank"><b>Marruecos</b></a>, entre otros, donde presentamos novedades, promociones, colaboraciones, etc., dirigidas tanto a profesionales como a viajeros.

Estar en Fitur es imprescindible para los negocios del turismo, ya que es una ventana al mundo. Pero si simplemente os gusta viajar y buscáis nuevos destinos e inspiraciones, éste también es vuestro lugar. De Fitur al mundo y del mundo a Fitur.

Foto | <a href="http://www.flickr.com/photos/arternative-design/" target="_blank">www.alternative-design.com</a>]]></description>
				<content:encoded><![CDATA[<p><img width="150" height="150" src="http://blog.trasmediterranea.es/wp-content/uploads/canarias_www.alternative-design.com_-150x150.jpg" class="attachment-post-thumbnail wp-post-image" alt="canarias_www.alternative-design.com" /></p><a href="http://blog.trasmediterranea.es/wp-content/uploads/canarias_www.alternative-design.com_.jpg"><img class="aligncenter size-full wp-image-1298" alt="canarias_www.alternative-design.com" src="http://blog.trasmediterranea.es/wp-content/uploads/canarias_www.alternative-design.com_.jpg" width="640" height="422" /></a>

Esta semana los amantes de los viajes tenemos una cita ineludible, la 34 edición de la <b><a href="http://www.ifema.es/fitur_01 " target="_blank">Feria Internacional de Turismo</a>, Fitur,</b> que se está desarrollando en Madrid hasta el 26 de enero. Fitur es un punto de encuentro para los profesionales del turismo, además de para los viajeros más curiosos. El año pasado 116.157 profesionales y 91.082 personas como público general visitaron la feria, confirmando el éxito del encuentro. Fitur es una de las ferias mundiales más importantes del turismo y marca las bases del futuro desarrollo del sector.

La feria alberga diversas iniciativas como <a href="http://www.ifema.es/fitur_01/FiturGreen/" target="_blank"><b>Fitur Green</b></a> que pretende fomentar la innovación y la sostenibilidad en el sector, impulsando su competitividad, y <a href="http://www.ifema.es/fitur_01/FiturTech/" target="_blank"><b>Fitur Tech</b></a> que nos trae la tecnología más puntera adaptada al sector y presenta el “turismo de tercera generación”. Otras iniciativas profesionales de la feria son<a href="http://www.ifema.es/fitur_01/Investour/index.htm " target="_blank"><b> Investour</b></a>, que apuesta por el desarrollo sostenible del turismo en África, <a href="http://www.ifema.es/fitur_01/FiturB2B/index.htm" target="_blank"><b>Fitur B2B</b></a>, con workshops, y <a href="http://www.ifema.es/fitur_01/FiturKnowHowExport/index.htm" target="_blank"><b>Know How and Export</b></a>, espacio donde las empresas turísticas españolas muestran su potencial a compradores de otros mercados.

Fitur además de ser un excelente escaparate para las empresas del sector turístico, también es una gran fuente de inspiración para los viajeros. Para estos últimos, la feria abre sus puertas los días 25 y 26 de enero, pudiendo conocer de primera mano las últimas tendencias en viajes y acceder a las mejores ofertas. Los visitantes podrán conocer, por ejemplo, la nueva campaña de las<a href="http://www.elmejorclimadelmundo.com/" target="_blank"> <b>Islas Canarias</b></a> que presentan “el mejor clima del mundo” en 1.428 metros cuadrados; visitar “<a href="http://www.youtube.com/watch?v=z3zjHnD4qVQ" target="_blank"><b>Melilla</b></a>, paraíso de los sentidos”; conocer una nueva parte de la <a href="http://turismo.cadiz.es/es/actualidad/el-c%C3%A1diz-fenicio-ser%C3%A1-presentado-en-fitur" target="_blank"><b>Ruta Cádiz Fenicia</b></a> por el yacimiento arqueológico Gadir; o los eventos deportivos que se llevarán a cabo en <a href="http://www.ibiza.travel/es/deporte.php" target="_blank"><b>Ibiza </b></a>en 2014 “… hasta que se ponga el sol”.

Los viajeros también podrán disfrutar de las actividades que muchas de las empresas participantes organizan. Este es el caso de <a href="http://www.minube.com/ " target="_blank"><b>Minube</b></a>, con apuestas muy interesantes como la “Quedada Viajera”, que es ya un clásico dentro de la feria, la “Gymkana viajera” y el “Taller de Fotografía”, además de un punto de información para viajeros disponible durante todo el fin de semana.

<a href="http://www.trasmediterranea.es/" target="_blank"><b>Trasmediterranea </b></a>está presente un año más en algunos de los espacios de sus destinos, como <a href="http://www.ceutaturistica.com/" target="_blank"><b>Ceuta</b></a>, <a href="http://www.melillaturismo.com/" target="_blank"><b>Melilla</b></a> y <a href="http://www.visitmorocco.com/index.php/esl/content/view/full/2" target="_blank"><b>Marruecos</b></a>, entre otros, donde presentamos novedades, promociones, colaboraciones, etc., dirigidas tanto a profesionales como a viajeros.

Estar en Fitur es imprescindible para los negocios del turismo, ya que es una ventana al mundo. Pero si simplemente os gusta viajar y buscáis nuevos destinos e inspiraciones, éste también es vuestro lugar. De Fitur al mundo y del mundo a Fitur.

Foto | <a href="http://www.flickr.com/photos/arternative-design/" target="_blank">www.alternative-design.com</a>]]></content:encoded>
			<wfw:commentRss>http://blog.trasmediterranea.es/2014/01/22/de-fitur-al-mundo/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>Ir a esquiar en ferry. ¡Sí, es posible!</title>
		<link>http://blog.trasmediterranea.es/2014/01/16/ir-a-esquiar-en-ferry-si-es-posible/</link>
		<comments>http://blog.trasmediterranea.es/2014/01/16/ir-a-esquiar-en-ferry-si-es-posible/#comments</comments>
		<pubDate>Thu, 16 Jan 2014 10:24:21 +0000</pubDate>
		<dc:creator><![CDATA[.]]></dc:creator>
				<category><![CDATA[Ofertas y promociones]]></category>
		<category><![CDATA[Andorra]]></category>
		<category><![CDATA[Astún]]></category>
		<category><![CDATA[Baleares]]></category>
		<category><![CDATA[Barcelona]]></category>
		<category><![CDATA[Candanchú]]></category>
		<category><![CDATA[esquí]]></category>
		<category><![CDATA[La Molina]]></category>
		<category><![CDATA[Pal y Arinsal]]></category>
		<category><![CDATA[Port-Ainé]]></category>
		<category><![CDATA[snowboard]]></category>

		<guid isPermaLink="false">http://blog.trasmediterranea.es/?p=1281</guid>
		<description><![CDATA[<p><img width="150" height="150" src="http://blog.trasmediterranea.es/wp-content/uploads/Nieve_01_Denis-Messié-150x150.jpg" class="attachment-post-thumbnail wp-post-image" alt="Foto_Denis Messié" /></p><a href="http://blog.trasmediterranea.es/wp-content/uploads/Nieve_01_Denis-Messié.jpg"><img class="aligncenter size-full wp-image-1282" alt="Foto_Denis Messié" src="http://blog.trasmediterranea.es/wp-content/uploads/Nieve_01_Denis-Messié.jpg" width="640" height="427" /></a>

Estamos en plena temporada de esquí, y con la vuelta a la rutina que supone el fin de las Navidades… ¿Qué mejor que ir un fin de semana a la nieve? Es un planazo para estas fechas ¡no lo dejes pasar!

En <b>Trasmediterranea </b>te proponemos hacer un <i>road trip</i>. En nuestra <strong><a href="http://w3.trasmediterranea.es/es/guia-pasajeros/guias/Paginas/viaje-esquiar-baleares-peninsula.aspx" target="_blank">página web</a></strong> te planteamos diversos itinerarios para que elijas la estación y ruta que más te interesen. Los ferries de la compañía conectan las Islas Baleares con Barcelona, desde donde partimos con nuestro vehículo a cualquiera de las estaciones de esquí más cercanas (Astún, Candanchú, Pal y Arinsal, Port-Ainé y La Molina) y podemos así llegar de la playa a la nieve en menos de dos horas.

Un fin de semana de esquí no es sólo esquí. Disfrutar de las maravillosas vistas de los Pirineos haciendo deporte, salir por la noche a los pueblos de sus alrededores, disfrutar de la gastronomía local y de los diferentes paisajes… Es un viaje intenso y gratificante. Si eres de las Islas Baleares no te lo pienses, conectamos Palma, Ibiza y Menorca con Barcelona para que disfrutes de la nieve cuando y como quieras.

La estación más cercana al puerto de Barcelona es <b><a href="http://hivern.lamolina.cat/es/?" target="_blank">La Molina</a> </b>(Gerona), que está a tan sólo 162 kilómetros. Dispone de 53 pistas y un total de 61 kilómetros esquiables.

A 200 kilómetros tenemos <b>Pal y Arinsal</b> (Andorra). Se encuentra dentro del complejo <b><a href="http://www.vallnord.com/es" target="_blank">Vallnord</a> </b>y por lo tanto cerca de <b>Soldeu</b> y <b>Grandvalira</b>, 205 km de pistas que lo convierten en el mayor dominio esquiable de los Pirineos. Además, está a sólo 14 km del centro termolúdico de montaña más grande de Europa (baño islandés, aquamasaje, baño sirocco, saunas de luz, hamman, vaporización o luz de Wood… todo bajo el mismo techo): <b><a href="http://www.caldea.com/es/es.html" target="_blank">Caldea</a></b>, el cual merece la pena visitar, y más aún después de un duro día de esquí. Sin duda es la opción más apetecible ya que es la segunda más cercana y la que ofrece mayor oferta a los esquiadores, tanto expertos como amateurs.

La estación <a href="http://www.skipallars.cat/es/cms/estacion-port-aine-esqui_9/estacion-port-aine_9/" target="_blank"><b>Port Ainé</b></a> de Lleida tiene 24 km esquiables y casi 100 profesores, ¡ideal para comenzar tus andaduras en la nieve! Y por último tenemos las estaciones de <b><a href="https://www.astun.com/es/inicio" target="_blank">Astún</a> </b>y <b><a href="http://www.candanchu.com/" target="_blank">Candanchú</a> </b>(Huesca), a 342 kilómetros. Cerca de Jaca, son unas de las estaciones más frecuentadas, sobre todo, por familias. Candanchú es pionera en la enseñanza de esquí, a tener en cuenta al elegir destino.

La oferta es innegable, ahora sólo falta decidir destino, coger dos días y ¡animarse! No hay mejor modo de cargar las pilas que gastándolas haciendo deporte y alimentándolas después con buena gastronomía y cultura. De Baleares a la nieve, ¡con Trasme sí se puede!

Foto | <a href="http://www.flickr.com/photos/denismessie/" target="_blank">Denis Messié</a>]]></description>
				<content:encoded><![CDATA[<p><img width="150" height="150" src="http://blog.trasmediterranea.es/wp-content/uploads/Nieve_01_Denis-Messié-150x150.jpg" class="attachment-post-thumbnail wp-post-image" alt="Foto_Denis Messié" /></p><a href="http://blog.trasmediterranea.es/wp-content/uploads/Nieve_01_Denis-Messié.jpg"><img class="aligncenter size-full wp-image-1282" alt="Foto_Denis Messié" src="http://blog.trasmediterranea.es/wp-content/uploads/Nieve_01_Denis-Messié.jpg" width="640" height="427" /></a>

Estamos en plena temporada de esquí, y con la vuelta a la rutina que supone el fin de las Navidades… ¿Qué mejor que ir un fin de semana a la nieve? Es un planazo para estas fechas ¡no lo dejes pasar!

En <b>Trasmediterranea </b>te proponemos hacer un <i>road trip</i>. En nuestra <strong><a href="http://w3.trasmediterranea.es/es/guia-pasajeros/guias/Paginas/viaje-esquiar-baleares-peninsula.aspx" target="_blank">página web</a></strong> te planteamos diversos itinerarios para que elijas la estación y ruta que más te interesen. Los ferries de la compañía conectan las Islas Baleares con Barcelona, desde donde partimos con nuestro vehículo a cualquiera de las estaciones de esquí más cercanas (Astún, Candanchú, Pal y Arinsal, Port-Ainé y La Molina) y podemos así llegar de la playa a la nieve en menos de dos horas.

Un fin de semana de esquí no es sólo esquí. Disfrutar de las maravillosas vistas de los Pirineos haciendo deporte, salir por la noche a los pueblos de sus alrededores, disfrutar de la gastronomía local y de los diferentes paisajes… Es un viaje intenso y gratificante. Si eres de las Islas Baleares no te lo pienses, conectamos Palma, Ibiza y Menorca con Barcelona para que disfrutes de la nieve cuando y como quieras.

La estación más cercana al puerto de Barcelona es <b><a href="http://hivern.lamolina.cat/es/?" target="_blank">La Molina</a> </b>(Gerona), que está a tan sólo 162 kilómetros. Dispone de 53 pistas y un total de 61 kilómetros esquiables.

A 200 kilómetros tenemos <b>Pal y Arinsal</b> (Andorra). Se encuentra dentro del complejo <b><a href="http://www.vallnord.com/es" target="_blank">Vallnord</a> </b>y por lo tanto cerca de <b>Soldeu</b> y <b>Grandvalira</b>, 205 km de pistas que lo convierten en el mayor dominio esquiable de los Pirineos. Además, está a sólo 14 km del centro termolúdico de montaña más grande de Europa (baño islandés, aquamasaje, baño sirocco, saunas de luz, hamman, vaporización o luz de Wood… todo bajo el mismo techo): <b><a href="http://www.caldea.com/es/es.html" target="_blank">Caldea</a></b>, el cual merece la pena visitar, y más aún después de un duro día de esquí. Sin duda es la opción más apetecible ya que es la segunda más cercana y la que ofrece mayor oferta a los esquiadores, tanto expertos como amateurs.

La estación <a href="http://www.skipallars.cat/es/cms/estacion-port-aine-esqui_9/estacion-port-aine_9/" target="_blank"><b>Port Ainé</b></a> de Lleida tiene 24 km esquiables y casi 100 profesores, ¡ideal para comenzar tus andaduras en la nieve! Y por último tenemos las estaciones de <b><a href="https://www.astun.com/es/inicio" target="_blank">Astún</a> </b>y <b><a href="http://www.candanchu.com/" target="_blank">Candanchú</a> </b>(Huesca), a 342 kilómetros. Cerca de Jaca, son unas de las estaciones más frecuentadas, sobre todo, por familias. Candanchú es pionera en la enseñanza de esquí, a tener en cuenta al elegir destino.

La oferta es innegable, ahora sólo falta decidir destino, coger dos días y ¡animarse! No hay mejor modo de cargar las pilas que gastándolas haciendo deporte y alimentándolas después con buena gastronomía y cultura. De Baleares a la nieve, ¡con Trasme sí se puede!

Foto | <a href="http://www.flickr.com/photos/denismessie/" target="_blank">Denis Messié</a>]]></content:encoded>
			<wfw:commentRss>http://blog.trasmediterranea.es/2014/01/16/ir-a-esquiar-en-ferry-si-es-posible/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>Un nuevo comienzo</title>
		<link>http://blog.trasmediterranea.es/2014/01/02/un-nuevo-comienzo/</link>
		<comments>http://blog.trasmediterranea.es/2014/01/02/un-nuevo-comienzo/#comments</comments>
		<pubDate>Thu, 02 Jan 2014 15:55:16 +0000</pubDate>
		<dc:creator><![CDATA[.]]></dc:creator>
				<category><![CDATA[Guías viajeras]]></category>
		<category><![CDATA[Ceuta]]></category>
		<category><![CDATA[Gran Canaria]]></category>
		<category><![CDATA[Ibiza]]></category>
		<category><![CDATA[Islas Baleares]]></category>
		<category><![CDATA[Islas Canarias]]></category>
		<category><![CDATA[Mallorca]]></category>
		<category><![CDATA[Tanger]]></category>

		<guid isPermaLink="false">http://blog.trasmediterranea.es/?p=1260</guid>
		<description><![CDATA[<p><img width="150" height="150" src="http://blog.trasmediterranea.es/wp-content/uploads/royal-mansur_un-nuevo-comienzo3-150x150.png" class="attachment-post-thumbnail wp-post-image" alt="royal mansur_un nuevo comienzo" /></p><a href="http://blog.trasmediterranea.es/wp-content/uploads/royal-mansur_un-nuevo-comienzo1.png"><img class="aligncenter size-full wp-image-1261" alt="" /></a>
<p style="text-align: left;"><a href="http://blog.trasmediterranea.es/wp-content/uploads/royal-mansur_un-nuevo-comienzo4.png"><img class="aligncenter size-full wp-image-1275" alt="royal mansur_un nuevo comienzo" src="http://blog.trasmediterranea.es/wp-content/uploads/royal-mansur_un-nuevo-comienzo4.png" width="599" height="346" /></a></p>
<p style="text-align: left;">Ha llegado sin previo aviso, como siempre, pero aquí está el inicio de un nuevo año. ¿Qué nos deparará el 2014? Dejemos las cosas negativas a un lado y cambiemos el rumbo, miremos hacia delante y enfrentémonos a un futuro aún por recorrer, porque el destino está en nuestras manos y la suerte en nuestros actos.</p>
¿Cómo comenzar el año nuevo positivamente? Te proponemos varias ideas para que inicies esta nueva etapa con energías renovadas. Empezamos por las <a href="http://www.turismodecanarias.com/islas-canarias-espana/" target="_blank"><b>Islas Canarias</b></a>, muy demandadas en estas fechas, ya que ofrecen ecosistemas tan dispares como las <b><a href="http://www.minube.com/rincon/las-dunas-de-maspalomas-a83745" target="_blank">Dunas de Maspalomas</a> </b>(Gran Canaria) o el paisaje lunar de <a href="http://www.turismolanzarote.com/" target="_blank"><b>Lanzarote</b></a>, y todo ello con un clima que invita a relajarse y disfrutar.

El <a href="http://www.spain.info/es/que-quieres/ciudades-pueblos/comunidades-autonomas/islas_baleares.html" target="_blank"><b>Archipiélago Balear</b></a> es una opción más cercana pero no por ello menos especial. Menorca, Ibiza, Formentera… Son destinos tan diversos como interesantes. Deléitate con la magnífica puesta de sol frente a <a href="http://www.minube.com/rincon/es-vedra-_-cala-dhort-a95694" target="_blank"><b>Es Vedrá</b></a> (Ibiza) o goza de un paseo en barca, con concierto de música clásica incluido, en las<b> <a href="http://www.cuevasdeldrach.com/" target="_blank">Cuevas del Drach</a></b> (Mallorca).

Y para los más aventureros y curiosos, os recomendamos un viaje a otra cultura, otro mundo, tan cercano y a su vez tan distinto al nuestro. El norte de África es historia pura, que aún habiéndonos influido mucho, puede que no conozcamos tanto. Pasea por el <b><a href="http://www.turismotanger.com/zocos-tanger.html" target="_blank">Gran Zoco</a> </b>de Tánger y callejea en busca de los negocios más peculiares o camina sobre la <a href="http://www.conoceceuta.com/0050-07%20murallas_reales.htm" target="_blank"><b>Muralla de Ceuta</b></a> y descubre más sobre nuestro pasado.

La elección es amplia y excitante; mira, busca, lee, imagina… Déjate llevar. Elige tu destino y compártelo con quien desees. Empieza una nueva etapa llena de energía y fuerza, porque todo lo que no está hecho, está por hacer.

Date el comienzo que mereces.]]></description>
				<content:encoded><![CDATA[<p><img width="150" height="150" src="http://blog.trasmediterranea.es/wp-content/uploads/royal-mansur_un-nuevo-comienzo3-150x150.png" class="attachment-post-thumbnail wp-post-image" alt="royal mansur_un nuevo comienzo" /></p><a href="http://blog.trasmediterranea.es/wp-content/uploads/royal-mansur_un-nuevo-comienzo1.png"><img class="aligncenter size-full wp-image-1261" alt="" /></a>
<p style="text-align: left;"><a href="http://blog.trasmediterranea.es/wp-content/uploads/royal-mansur_un-nuevo-comienzo4.png"><img class="aligncenter size-full wp-image-1275" alt="royal mansur_un nuevo comienzo" src="http://blog.trasmediterranea.es/wp-content/uploads/royal-mansur_un-nuevo-comienzo4.png" width="599" height="346" /></a></p>
<p style="text-align: left;">Ha llegado sin previo aviso, como siempre, pero aquí está el inicio de un nuevo año. ¿Qué nos deparará el 2014? Dejemos las cosas negativas a un lado y cambiemos el rumbo, miremos hacia delante y enfrentémonos a un futuro aún por recorrer, porque el destino está en nuestras manos y la suerte en nuestros actos.</p>
¿Cómo comenzar el año nuevo positivamente? Te proponemos varias ideas para que inicies esta nueva etapa con energías renovadas. Empezamos por las <a href="http://www.turismodecanarias.com/islas-canarias-espana/" target="_blank"><b>Islas Canarias</b></a>, muy demandadas en estas fechas, ya que ofrecen ecosistemas tan dispares como las <b><a href="http://www.minube.com/rincon/las-dunas-de-maspalomas-a83745" target="_blank">Dunas de Maspalomas</a> </b>(Gran Canaria) o el paisaje lunar de <a href="http://www.turismolanzarote.com/" target="_blank"><b>Lanzarote</b></a>, y todo ello con un clima que invita a relajarse y disfrutar.

El <a href="http://www.spain.info/es/que-quieres/ciudades-pueblos/comunidades-autonomas/islas_baleares.html" target="_blank"><b>Archipiélago Balear</b></a> es una opción más cercana pero no por ello menos especial. Menorca, Ibiza, Formentera… Son destinos tan diversos como interesantes. Deléitate con la magnífica puesta de sol frente a <a href="http://www.minube.com/rincon/es-vedra-_-cala-dhort-a95694" target="_blank"><b>Es Vedrá</b></a> (Ibiza) o goza de un paseo en barca, con concierto de música clásica incluido, en las<b> <a href="http://www.cuevasdeldrach.com/" target="_blank">Cuevas del Drach</a></b> (Mallorca).

Y para los más aventureros y curiosos, os recomendamos un viaje a otra cultura, otro mundo, tan cercano y a su vez tan distinto al nuestro. El norte de África es historia pura, que aún habiéndonos influido mucho, puede que no conozcamos tanto. Pasea por el <b><a href="http://www.turismotanger.com/zocos-tanger.html" target="_blank">Gran Zoco</a> </b>de Tánger y callejea en busca de los negocios más peculiares o camina sobre la <a href="http://www.conoceceuta.com/0050-07%20murallas_reales.htm" target="_blank"><b>Muralla de Ceuta</b></a> y descubre más sobre nuestro pasado.

La elección es amplia y excitante; mira, busca, lee, imagina… Déjate llevar. Elige tu destino y compártelo con quien desees. Empieza una nueva etapa llena de energía y fuerza, porque todo lo que no está hecho, está por hacer.

Date el comienzo que mereces.]]></content:encoded>
			<wfw:commentRss>http://blog.trasmediterranea.es/2014/01/02/un-nuevo-comienzo/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>Baleares más allá del verano</title>
		<link>http://blog.trasmediterranea.es/2013/11/21/baleares-mas-alla-del-verano-2/</link>
		<comments>http://blog.trasmediterranea.es/2013/11/21/baleares-mas-alla-del-verano-2/#comments</comments>
		<pubDate>Thu, 21 Nov 2013 11:22:58 +0000</pubDate>
		<dc:creator><![CDATA[.]]></dc:creator>
				<category><![CDATA[General]]></category>
		<category><![CDATA[acantilado]]></category>
		<category><![CDATA[bosques]]></category>
		<category><![CDATA[calas]]></category>
		<category><![CDATA[Castell de Bellver]]></category>
		<category><![CDATA[catedral]]></category>
		<category><![CDATA[cuevas del Drach]]></category>
		<category><![CDATA[excursión]]></category>
		<category><![CDATA[murallas árabes]]></category>
		<category><![CDATA[Pollença]]></category>
		<category><![CDATA[pueblos del interior]]></category>
		<category><![CDATA[relajante]]></category>
		<category><![CDATA[Soller]]></category>
		<category><![CDATA[Valldemossa]]></category>

		<guid isPermaLink="false">http://blog.trasmediterranea.es/?p=1239</guid>
		<description><![CDATA[<p><img width="150" height="150" src="http://blog.trasmediterranea.es/wp-content/uploads/Foto-bici-formentera-1-150x150.jpg" class="attachment-post-thumbnail wp-post-image" alt="Foto bici formentera (1)" /></p><a href="http://blog.trasmediterranea.es/wp-content/uploads/Foto-bici-formentera-1.jpg"><img class="aligncenter size-full wp-image-1242" alt="Foto bici formentera (1)" src="http://blog.trasmediterranea.es/wp-content/uploads/Foto-bici-formentera-1.jpg" width="600" height="800" /></a>

Las islas <strong>Baleares en otoño y en invierno</strong> son el mismo paraíso que en verano. Una escapada en diciembre a sus <strong>bosques</strong>, sus <strong>calas</strong> o a sus <strong>preciosos pueblos del interior</strong>, son una efectiva cura de reposo y una potente recarga de energía.

En la isla de Mallorca hay rincones increíbles, calas rodeadas de pinos, la zona antigua de Palma, la lonja, la <strong>catedral</strong>, ¿sabías que posee el mayor <strong>rosetón gótico</strong> original de las catedrales europeas y que se comenzó a construir en el año 1.300? No te puedes perder maravillas como el impactante<strong> Castell de Bellver</strong>, el pueblo señorial de <strong>Valldemossa</strong>, <strong>Pollença</strong>, <strong>Soller</strong> o las <strong>cuevas del Drach</strong>.

Hay miles de planes y <strong>excursiones</strong> por los <strong>acantilados</strong>, por la <strong>Sierra Norte</strong> y la de la <strong>Tramontana</strong>. O sencillamente pasear por el puerto de <strong>Andratx</strong> con el mar de fondo, que es el mejor <strong>relajante</strong> del mundo.

Ibiza es mucho más que sol y playa. Sólo en la capital esperan al visitante sus <strong>murallas árabes</strong> del siglo XV; y en la isla aguardan los pueblos de <strong>San Antonio</strong>, <strong>San Miguel</strong>, <strong>Santa Eulalia</strong> o las cuevas de S<strong>es Fontelles</strong>, con sus antiquísimas <strong>pinturas murales</strong>.

También podemos darnos un baño de <strong>naturaleza,</strong> por algo <strong>Menorca</strong> es <strong>reserva de la Biosfera</strong> y <strong>Formentera</strong> un <strong>paraíso</strong> ideal para perderse.

Diciembre es una temporada ideal para escaparse a las Baleares. Aprovecha el <strong>puente de la Constitución</strong> y regálate unos días de descanso.]]></description>
				<content:encoded><![CDATA[<p><img width="150" height="150" src="http://blog.trasmediterranea.es/wp-content/uploads/Foto-bici-formentera-1-150x150.jpg" class="attachment-post-thumbnail wp-post-image" alt="Foto bici formentera (1)" /></p><a href="http://blog.trasmediterranea.es/wp-content/uploads/Foto-bici-formentera-1.jpg"><img class="aligncenter size-full wp-image-1242" alt="Foto bici formentera (1)" src="http://blog.trasmediterranea.es/wp-content/uploads/Foto-bici-formentera-1.jpg" width="600" height="800" /></a>

Las islas <strong>Baleares en otoño y en invierno</strong> son el mismo paraíso que en verano. Una escapada en diciembre a sus <strong>bosques</strong>, sus <strong>calas</strong> o a sus <strong>preciosos pueblos del interior</strong>, son una efectiva cura de reposo y una potente recarga de energía.

En la isla de Mallorca hay rincones increíbles, calas rodeadas de pinos, la zona antigua de Palma, la lonja, la <strong>catedral</strong>, ¿sabías que posee el mayor <strong>rosetón gótico</strong> original de las catedrales europeas y que se comenzó a construir en el año 1.300? No te puedes perder maravillas como el impactante<strong> Castell de Bellver</strong>, el pueblo señorial de <strong>Valldemossa</strong>, <strong>Pollença</strong>, <strong>Soller</strong> o las <strong>cuevas del Drach</strong>.

Hay miles de planes y <strong>excursiones</strong> por los <strong>acantilados</strong>, por la <strong>Sierra Norte</strong> y la de la <strong>Tramontana</strong>. O sencillamente pasear por el puerto de <strong>Andratx</strong> con el mar de fondo, que es el mejor <strong>relajante</strong> del mundo.

Ibiza es mucho más que sol y playa. Sólo en la capital esperan al visitante sus <strong>murallas árabes</strong> del siglo XV; y en la isla aguardan los pueblos de <strong>San Antonio</strong>, <strong>San Miguel</strong>, <strong>Santa Eulalia</strong> o las cuevas de S<strong>es Fontelles</strong>, con sus antiquísimas <strong>pinturas murales</strong>.

También podemos darnos un baño de <strong>naturaleza,</strong> por algo <strong>Menorca</strong> es <strong>reserva de la Biosfera</strong> y <strong>Formentera</strong> un <strong>paraíso</strong> ideal para perderse.

Diciembre es una temporada ideal para escaparse a las Baleares. Aprovecha el <strong>puente de la Constitución</strong> y regálate unos días de descanso.]]></content:encoded>
			<wfw:commentRss>http://blog.trasmediterranea.es/2013/11/21/baleares-mas-alla-del-verano-2/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>En barco, mejor que en avión</title>
		<link>http://blog.trasmediterranea.es/2013/10/29/en-barco-mejor-que-en-avion/</link>
		<comments>http://blog.trasmediterranea.es/2013/10/29/en-barco-mejor-que-en-avion/#comments</comments>
		<pubDate>Tue, 29 Oct 2013 14:35:58 +0000</pubDate>
		<dc:creator><![CDATA[.]]></dc:creator>
				<category><![CDATA[General]]></category>
		<category><![CDATA[coche]]></category>
		<category><![CDATA[comodidad]]></category>
		<category><![CDATA[delfines]]></category>
		<category><![CDATA[descando]]></category>
		<category><![CDATA[disfrute]]></category>
		<category><![CDATA[equipaje]]></category>
		<category><![CDATA[mascotas]]></category>
		<category><![CDATA[puesta de dol]]></category>
		<category><![CDATA[restaurante]]></category>
		<category><![CDATA[ventaja]]></category>

		<guid isPermaLink="false">http://blog.trasmediterranea.es/?p=1208</guid>
		<description><![CDATA[<p><img width="150" height="150" src="http://blog.trasmediterranea.es/wp-content/uploads/Mejor-en-barco-150x150.jpg" class="attachment-post-thumbnail wp-post-image" alt="Mejor en barco" /></p><a href="http://blog.trasmediterranea.es/wp-content/uploads/Mejor-en-barco1.jpg"><img class="size-full wp-image-1218" alt="Mejor en barco" src="http://blog.trasmediterranea.es/wp-content/uploads/Mejor-en-barco1.jpg" width="500" height="666" /></a>

Cuando subimos a un avión, a un autobús, a un tren o a un coche pensamos que ojalá el viaje se haga corto. Cuando subimos a un <strong>barco</strong>, sin embargo, la sensación es diferente porque a bordo comienza el <strong>descanso</strong> y el <strong>disfrute</strong>.

Viajar en barco es más que desplazarnos de un punto a otro, es <strong>pasarlo bien</strong> a bordo, subir a cubierta y ver cómo nos alejamos del punto de partida; es el placer de <strong>movernos libremente</strong> por el buque, de disfrutar de la <strong>puesta del sol</strong> tomando un refresco, de ver <strong>delfines</strong>, de quedarnos traspuestos con el frescor de <strong>la brisa marina</strong>.

Viajar en barco es mucho más <strong>cómodo</strong> que el avión. Para empezar, los puertos están más cerca que los aeropuertos: son parte de la ciudad, mientras que los aeropuertos están en las afueras; además podemos viajar con <strong>el equipaje</strong> que queramos, nuestros hijos pueden corretear a sus anchas; nos podemos llevar <strong>el coche</strong> y nuestras <strong>mascotas</strong> viajan con nosotros, cómodas y tranquilas.

Las posibilidades son infinitas: podemos bailar si estamos animados, comer en el <strong>restaurante</strong>, o si preferimos algo más informal, tomar unos bocadillos haciendo un <strong>picnic en cubierta</strong>, y asomarnos para ver cómo aparece en <strong>el horizonte</strong> el lugar al que nos dirigimos...

…O en palabras de Robert Louis Stevenson: “Yo no viajo para ir a alguna parte, sino por ir. Por el hecho de viajar. El asunto es moverse.”]]></description>
				<content:encoded><![CDATA[<p><img width="150" height="150" src="http://blog.trasmediterranea.es/wp-content/uploads/Mejor-en-barco-150x150.jpg" class="attachment-post-thumbnail wp-post-image" alt="Mejor en barco" /></p><a href="http://blog.trasmediterranea.es/wp-content/uploads/Mejor-en-barco1.jpg"><img class="size-full wp-image-1218" alt="Mejor en barco" src="http://blog.trasmediterranea.es/wp-content/uploads/Mejor-en-barco1.jpg" width="500" height="666" /></a>

Cuando subimos a un avión, a un autobús, a un tren o a un coche pensamos que ojalá el viaje se haga corto. Cuando subimos a un <strong>barco</strong>, sin embargo, la sensación es diferente porque a bordo comienza el <strong>descanso</strong> y el <strong>disfrute</strong>.

Viajar en barco es más que desplazarnos de un punto a otro, es <strong>pasarlo bien</strong> a bordo, subir a cubierta y ver cómo nos alejamos del punto de partida; es el placer de <strong>movernos libremente</strong> por el buque, de disfrutar de la <strong>puesta del sol</strong> tomando un refresco, de ver <strong>delfines</strong>, de quedarnos traspuestos con el frescor de <strong>la brisa marina</strong>.

Viajar en barco es mucho más <strong>cómodo</strong> que el avión. Para empezar, los puertos están más cerca que los aeropuertos: son parte de la ciudad, mientras que los aeropuertos están en las afueras; además podemos viajar con <strong>el equipaje</strong> que queramos, nuestros hijos pueden corretear a sus anchas; nos podemos llevar <strong>el coche</strong> y nuestras <strong>mascotas</strong> viajan con nosotros, cómodas y tranquilas.

Las posibilidades son infinitas: podemos bailar si estamos animados, comer en el <strong>restaurante</strong>, o si preferimos algo más informal, tomar unos bocadillos haciendo un <strong>picnic en cubierta</strong>, y asomarnos para ver cómo aparece en <strong>el horizonte</strong> el lugar al que nos dirigimos...

…O en palabras de Robert Louis Stevenson: “Yo no viajo para ir a alguna parte, sino por ir. Por el hecho de viajar. El asunto es moverse.”]]></content:encoded>
			<wfw:commentRss>http://blog.trasmediterranea.es/2013/10/29/en-barco-mejor-que-en-avion/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>Buscar el equilibrio entre todos</title>
		<link>http://blog.trasmediterranea.es/2013/09/30/buscar-el-equilibrio-entre-todos/</link>
		<comments>http://blog.trasmediterranea.es/2013/09/30/buscar-el-equilibrio-entre-todos/#comments</comments>
		<pubDate>Mon, 30 Sep 2013 11:31:20 +0000</pubDate>
		<dc:creator><![CDATA[.]]></dc:creator>
				<category><![CDATA[General]]></category>
		<category><![CDATA[ahorro de energía]]></category>
		<category><![CDATA[futuro]]></category>
		<category><![CDATA[medioambiente]]></category>
		<category><![CDATA[naturaleza]]></category>
		<category><![CDATA[planeta sano]]></category>
		<category><![CDATA[Sostenibilidad]]></category>

		<guid isPermaLink="false">http://blog.trasmediterranea.es/?p=1074</guid>
		<description><![CDATA[<p><img width="150" height="150" src="http://blog.trasmediterranea.es/wp-content/uploads/LA-FOTO-150x150.jpg" class="attachment-post-thumbnail wp-post-image" alt="LA FOTO" /></p><a href="http://blog.trasmediterranea.es/wp-content/uploads/LA-FOTO.jpg"><img class="alignleft size-full wp-image-1195" alt="LA FOTO" src="http://blog.trasmediterranea.es/wp-content/uploads/LA-FOTO.jpg" width="600" height="399" /></a>
Acabamos de regresar de nuestras vacaciones de verano, hemos disfrutado de unos paisajes preciosos, hemos gozado del mar, de playas maravillosas… Ahora hay que pensar en cómo preservar todo eso para que nuestros hijos y nuestros nietos también lo conozcan y lo disfruten.

La sostenibilidad, la búsqueda del equilibrio y el procurar que las generaciones futuras conozcan un planeta sano, es una cuestión de todos. Hasta en la tarea más sencilla y cotidiana de nuestras vidas, la sostenibilidad está presente: cuando derrochamos agua o consumimos productos especialmente contaminantes estamos perjudicando al medio ambiente.

Pensemos en qué podemos hacer para contribuir a un desarrollo sostenible: podemos ahorrar energía, reciclar nuestros desperdicios, utilizar menos el coche… A nosotros nos preocupa la sostenibilidad y nos hemos puesto en marcha: el año pasado, por ejemplo, evitamos la emisión a la atmósfera de 14 toneladas de CO2, que es lo que emiten el 80% de los hogares españoles en un año.

También tomamos medidas de ahorro: utilizamos perlizadores en los grifos para gastar menos agua, nuestras bombillas son de bajo consumo, y la pintura del casco de nuestros buques es especial ya que está ideada para disminuir el rozamiento con el agua y así gastar menos combustible.

¿Qué haces tú para contribuir al ahorro de energía y a la sostenibilidad de nuestro planeta azul?]]></description>
				<content:encoded><![CDATA[<p><img width="150" height="150" src="http://blog.trasmediterranea.es/wp-content/uploads/LA-FOTO-150x150.jpg" class="attachment-post-thumbnail wp-post-image" alt="LA FOTO" /></p><a href="http://blog.trasmediterranea.es/wp-content/uploads/LA-FOTO.jpg"><img class="alignleft size-full wp-image-1195" alt="LA FOTO" src="http://blog.trasmediterranea.es/wp-content/uploads/LA-FOTO.jpg" width="600" height="399" /></a>
Acabamos de regresar de nuestras vacaciones de verano, hemos disfrutado de unos paisajes preciosos, hemos gozado del mar, de playas maravillosas… Ahora hay que pensar en cómo preservar todo eso para que nuestros hijos y nuestros nietos también lo conozcan y lo disfruten.

La sostenibilidad, la búsqueda del equilibrio y el procurar que las generaciones futuras conozcan un planeta sano, es una cuestión de todos. Hasta en la tarea más sencilla y cotidiana de nuestras vidas, la sostenibilidad está presente: cuando derrochamos agua o consumimos productos especialmente contaminantes estamos perjudicando al medio ambiente.

Pensemos en qué podemos hacer para contribuir a un desarrollo sostenible: podemos ahorrar energía, reciclar nuestros desperdicios, utilizar menos el coche… A nosotros nos preocupa la sostenibilidad y nos hemos puesto en marcha: el año pasado, por ejemplo, evitamos la emisión a la atmósfera de 14 toneladas de CO2, que es lo que emiten el 80% de los hogares españoles en un año.

También tomamos medidas de ahorro: utilizamos perlizadores en los grifos para gastar menos agua, nuestras bombillas son de bajo consumo, y la pintura del casco de nuestros buques es especial ya que está ideada para disminuir el rozamiento con el agua y así gastar menos combustible.

¿Qué haces tú para contribuir al ahorro de energía y a la sostenibilidad de nuestro planeta azul?]]></content:encoded>
			<wfw:commentRss>http://blog.trasmediterranea.es/2013/09/30/buscar-el-equilibrio-entre-todos/feed/</wfw:commentRss>
		<slash:comments>1</slash:comments>
		</item>
		<item>
		<title>Asilah, un paraíso muy cercano</title>
		<link>http://blog.trasmediterranea.es/2013/09/11/893/</link>
		<comments>http://blog.trasmediterranea.es/2013/09/11/893/#comments</comments>
		<pubDate>Wed, 11 Sep 2013 09:24:51 +0000</pubDate>
		<dc:creator><![CDATA[.]]></dc:creator>
				<category><![CDATA[Guías viajeras]]></category>
		<category><![CDATA[Cultura]]></category>
		<category><![CDATA[Festival]]></category>
		<category><![CDATA[fotografía]]></category>
		<category><![CDATA[gastronomía]]></category>
		<category><![CDATA[Historia]]></category>
		<category><![CDATA[Marruecos]]></category>
		<category><![CDATA[medina]]></category>
		<category><![CDATA[mercado]]></category>
		<category><![CDATA[playa]]></category>
		<category><![CDATA[Tanger]]></category>
		<category><![CDATA[Teatro]]></category>

		<guid isPermaLink="false">http://blog.trasmediterranea.es/?p=893</guid>
		<description><![CDATA[<p><img width="150" height="150" src="http://blog.trasmediterranea.es/wp-content/uploads/1537516141-150x150.jpg" class="attachment-post-thumbnail wp-post-image" alt="153751614" /></p>La ciudad marroquí de Asilah, que se encuentra<strong> a solo 30 km de Tánger,</strong> es un paraíso: sus playas son kilómetros de arena fina y agua transparente. Darse un baño y después saborear un delicioso tajine de pescado o unas sardinas asadas en la misma playa puede ser una experiencia auténtica e inolvidable. <a href="http://blog.trasmediterranea.es/wp-content/uploads/114406722-2.jpg"><img class="alignleft size-medium wp-image-898" alt="114406722-2" src="http://blog.trasmediterranea.es/wp-content/uploads/114406722-2-300x197.jpg" width="300" height="197" /></a> Esta preciosa población ha atraído a los fenicios, griegos, cartagineses, romanos, portugueses y españoles (la Corona española la convirtió en una fortaleza entre los siglos XV y XVI). Conserva la seducción propia de la mezcla de culturas: <strong>murallas del siglo XV</strong> que rodean la ciudad, edificios de la época del protectorado español próximos a la medina, casas blancas, bulliciosos puestos de mercado... <strong>la</strong> <strong>cultura y el arte</strong> irradian por todos los rincones. <a href="http://blog.trasmediterranea.es/wp-content/uploads/1537516141.jpg"><img class="alignleft size-medium wp-image-894" alt="153751614" src="http://blog.trasmediterranea.es/wp-content/uploads/1537516141-300x199.jpg" width="300" height="199" /></a> Y en verano se celebra el prestigioso <strong>Festival Internacional de Asilah</strong> al que acuden artistas e intelectuales de todo el mundo. Hay conciertos, música en directo, teatro, conferencias, exposiciones, concursos de fotografías… y un sinfín de actividades divertidas e interesantes para disfrutar de una estancia agradable en esta ciudad marítima. Asilah es un destino excelente. Ofrece cultura, <strong>historia, playas y hospitalidad.</strong> Esta ciudad de casitas blancas, pescadores y gente encantadora es un lugar perfecto para desconectar de las preocupaciones, para vivir unas maravillosas vacaciones. ¡No os la perdáis!

]]></description>
				<content:encoded><![CDATA[<p><img width="150" height="150" src="http://blog.trasmediterranea.es/wp-content/uploads/1537516141-150x150.jpg" class="attachment-post-thumbnail wp-post-image" alt="153751614" /></p>La ciudad marroquí de Asilah, que se encuentra<strong> a solo 30 km de Tánger,</strong> es un paraíso: sus playas son kilómetros de arena fina y agua transparente. Darse un baño y después saborear un delicioso tajine de pescado o unas sardinas asadas en la misma playa puede ser una experiencia auténtica e inolvidable. <a href="http://blog.trasmediterranea.es/wp-content/uploads/114406722-2.jpg"><img class="alignleft size-medium wp-image-898" alt="114406722-2" src="http://blog.trasmediterranea.es/wp-content/uploads/114406722-2-300x197.jpg" width="300" height="197" /></a> Esta preciosa población ha atraído a los fenicios, griegos, cartagineses, romanos, portugueses y españoles (la Corona española la convirtió en una fortaleza entre los siglos XV y XVI). Conserva la seducción propia de la mezcla de culturas: <strong>murallas del siglo XV</strong> que rodean la ciudad, edificios de la época del protectorado español próximos a la medina, casas blancas, bulliciosos puestos de mercado... <strong>la</strong> <strong>cultura y el arte</strong> irradian por todos los rincones. <a href="http://blog.trasmediterranea.es/wp-content/uploads/1537516141.jpg"><img class="alignleft size-medium wp-image-894" alt="153751614" src="http://blog.trasmediterranea.es/wp-content/uploads/1537516141-300x199.jpg" width="300" height="199" /></a> Y en verano se celebra el prestigioso <strong>Festival Internacional de Asilah</strong> al que acuden artistas e intelectuales de todo el mundo. Hay conciertos, música en directo, teatro, conferencias, exposiciones, concursos de fotografías… y un sinfín de actividades divertidas e interesantes para disfrutar de una estancia agradable en esta ciudad marítima. Asilah es un destino excelente. Ofrece cultura, <strong>historia, playas y hospitalidad.</strong> Esta ciudad de casitas blancas, pescadores y gente encantadora es un lugar perfecto para desconectar de las preocupaciones, para vivir unas maravillosas vacaciones. ¡No os la perdáis!

]]></content:encoded>
			<wfw:commentRss>http://blog.trasmediterranea.es/2013/09/11/893/feed/</wfw:commentRss>
		<slash:comments>0</slash:comments>
		</item>
		<item>
		<title>Trucos básicos para una maleta bien hecha</title>
		<link>http://blog.trasmediterranea.es/2013/09/10/trucos-basicos-para-una-maleta-bien-hecha/</link>
		<comments>http://blog.trasmediterranea.es/2013/09/10/trucos-basicos-para-una-maleta-bien-hecha/#comments</comments>
		<pubDate>Tue, 10 Sep 2013 09:28:36 +0000</pubDate>
		<dc:creator><![CDATA[.]]></dc:creator>
				<category><![CDATA[Manual de viajes]]></category>
		<category><![CDATA[equipaje]]></category>
		<category><![CDATA[Equipaje de mano]]></category>
		<category><![CDATA[Maleta]]></category>
		<category><![CDATA[Neceser]]></category>
		<category><![CDATA[Prendas delicadas]]></category>

		<guid isPermaLink="false">http://blog.trasmediterranea.es/?p=620</guid>
		<description><![CDATA[<p><img width="150" height="150" src="http://blog.trasmediterranea.es/wp-content/uploads/Maleta72-150x150.jpg" class="attachment-post-thumbnail wp-post-image" alt="Maleta72" /></p><a href="http://blog.trasmediterranea.es/wp-content/uploads/20130807-231251.jpg"><img class="alignleft size-full wp-image-984" alt="20130807-231251.jpg" src="http://blog.trasmediterranea.es/wp-content/uploads/20130807-231251.jpg" width="300" height="249" /></a> ¡Llevar todo lo necesario, sin que se arrugue demasiado y sin necesidad de cargar con un pesado baúl es posible! Hay una serie de <strong>trucos</strong> que permiten empaquetar de una manera práctica todo lo que necesitamos para disfrutar de unas estupendas vacaciones. Es fácil preparar un equipaje completo, y que nos quepa todo sin tener que sentarnos encima de la maleta para cerrarla. Comencemos poniendo sobre la cama todo lo que necesitamos. Así sabemos si nos estamos quedando cortos o nos estamos llevando el armario entero. Una vez hecha la selección empezamos la maleta situando <strong>las prendas más pesadas en el fondo,</strong> como los pantalones, y <strong>arriba lo más delicado,</strong> o lo que más se arrugue, como vestidos o blusas. Los <strong>vaqueros, las camisetas de algodón y los jerséis de punto es mejor que vayan enrollados</strong> para ocupar menos. ¡Hay que aprovechar lo huecos! <strong>Las camisas</strong> conviene colocarlas enfrentadas y con los cuellos hacia arriba para que no se aplasten. Las <strong>prendas delicadas</strong> hay que separarlas entre sí con papel de cebolla porque evitará las temibles arrugas. Y si aún así la ropa no ha llegado en perfectas condiciones de plancha, lo arreglamos mientras nos duchamos: solo hay que introducir en el cuarto de baño las prendas arrugadas bien colgadas en una percha, el vapor se encarga de alisarlas. <strong>Los zapatos</strong> requieren un tratamiento especial: como pueden manchar al resto de compañeros de maleta, lo mejor es meterlos en bolsas individuales, y para economizar sitio, una buena solución es rellenarlos de calcetines o complementos (collares, pulseras, etc.) También conviene ocupar las esquinas, en ellas se adaptan bien la ropa interior y las prendas pequeñas. ¡hay que aprovechar el espacio al máximo! <strong>Los cinturones</strong> enrollados ocupan más, así que es más práctico colocarlos estirados de manera que sigan la forma de la maleta. <strong>El neceser</strong> puede provocar accidentes inoportunos, así que lo ideal es aislarlo envuelto en una bolsa de plástico o con cada producto precintado individualmente para evitar que los líquidos se salgan y estropeen el resto del equipaje: una colonia derramada puede destruir el vestido que teníamos reservado para una noche especial. ¡Ah!, esta es una de las muchas ventajas de viajar en barco, ¡no hay limitaciones con los líquidos! Puedes viajar con lo que quieras y como quieras. ¡Ahora, a disfrutar de las vacaciones!]]></description>
				<content:encoded><![CDATA[<p><img width="150" height="150" src="http://blog.trasmediterranea.es/wp-content/uploads/Maleta72-150x150.jpg" class="attachment-post-thumbnail wp-post-image" alt="Maleta72" /></p><a href="http://blog.trasmediterranea.es/wp-content/uploads/20130807-231251.jpg"><img class="alignleft size-full wp-image-984" alt="20130807-231251.jpg" src="http://blog.trasmediterranea.es/wp-content/uploads/20130807-231251.jpg" width="300" height="249" /></a> ¡Llevar todo lo necesario, sin que se arrugue demasiado y sin necesidad de cargar con un pesado baúl es posible! Hay una serie de <strong>trucos</strong> que permiten empaquetar de una manera práctica todo lo que necesitamos para disfrutar de unas estupendas vacaciones. Es fácil preparar un equipaje completo, y que nos quepa todo sin tener que sentarnos encima de la maleta para cerrarla. Comencemos poniendo sobre la cama todo lo que necesitamos. Así sabemos si nos estamos quedando cortos o nos estamos llevando el armario entero. Una vez hecha la selección empezamos la maleta situando <strong>las prendas más pesadas en el fondo,</strong> como los pantalones, y <strong>arriba lo más delicado,</strong> o lo que más se arrugue, como vestidos o blusas. Los <strong>vaqueros, las camisetas de algodón y los jerséis de punto es mejor que vayan enrollados</strong> para ocupar menos. ¡Hay que aprovechar lo huecos! <strong>Las camisas</strong> conviene colocarlas enfrentadas y con los cuellos hacia arriba para que no se aplasten. Las <strong>prendas delicadas</strong> hay que separarlas entre sí con papel de cebolla porque evitará las temibles arrugas. Y si aún así la ropa no ha llegado en perfectas condiciones de plancha, lo arreglamos mientras nos duchamos: solo hay que introducir en el cuarto de baño las prendas arrugadas bien colgadas en una percha, el vapor se encarga de alisarlas. <strong>Los zapatos</strong> requieren un tratamiento especial: como pueden manchar al resto de compañeros de maleta, lo mejor es meterlos en bolsas individuales, y para economizar sitio, una buena solución es rellenarlos de calcetines o complementos (collares, pulseras, etc.) También conviene ocupar las esquinas, en ellas se adaptan bien la ropa interior y las prendas pequeñas. ¡hay que aprovechar el espacio al máximo! <strong>Los cinturones</strong> enrollados ocupan más, así que es más práctico colocarlos estirados de manera que sigan la forma de la maleta. <strong>El neceser</strong> puede provocar accidentes inoportunos, así que lo ideal es aislarlo envuelto en una bolsa de plástico o con cada producto precintado individualmente para evitar que los líquidos se salgan y estropeen el resto del equipaje: una colonia derramada puede destruir el vestido que teníamos reservado para una noche especial. ¡Ah!, esta es una de las muchas ventajas de viajar en barco, ¡no hay limitaciones con los líquidos! Puedes viajar con lo que quieras y como quieras. ¡Ahora, a disfrutar de las vacaciones!]]></content:encoded>
			<wfw:commentRss>http://blog.trasmediterranea.es/2013/09/10/trucos-basicos-para-una-maleta-bien-hecha/feed/</wfw:commentRss>
		<slash:comments>1</slash:comments>
		</item>
	</channel>
</rss>
